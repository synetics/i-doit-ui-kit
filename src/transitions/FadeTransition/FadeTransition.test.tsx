import { render, screen } from '@testing-library/react';
import React from 'react';
import { FadeTransition } from './FadeTransition';

describe('FadeTransition', () => {
  it("doesn't render content to the document", () => {
    render(
      <FadeTransition>
        <div>content</div>
      </FadeTransition>,
    );

    expect(screen.queryByText('content')).not.toBeInTheDocument();
  });

  it('renders given content to the document', () => {
    render(
      <FadeTransition inProp>
        <div>content</div>
      </FadeTransition>,
    );

    expect(screen.getByText('content')).toBeInTheDocument();
  });
});
