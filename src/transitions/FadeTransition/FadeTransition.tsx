import { FC, ReactElement } from 'react';
import { CSSTransition } from 'react-transition-group';
import { transitionConfig } from '../transition.config';
import styles from './FadeTransition.module.scss';

type FadeTransitionProps = {
  /**
   * Triggers the enter or exit states to start or stop transitions
   */
  inProp?: boolean;
  /**
   * Content to apply transitions
   */
  children: ReactElement;
};

const FadeTransition: FC<FadeTransitionProps> = ({ inProp, children }) => (
  <CSSTransition
    in={inProp}
    timeout={transitionConfig.duration}
    classNames={{
      enter: styles.fadeEnter,
      enterActive: styles.fadeEnterActive,
      exit: styles.fadeExit,
      exitActive: styles.fadeExitActive,
    }}
    unmountOnExit
  >
    {children}
  </CSSTransition>
);

export { FadeTransition, FadeTransitionProps };
