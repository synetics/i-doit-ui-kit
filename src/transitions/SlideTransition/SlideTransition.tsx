import { FC, ReactElement, useState } from 'react';
import { CSSTransition } from 'react-transition-group';
import { transitionConfig } from '../transition.config';
import styles from './SlideTransition.module.scss';

type SlideTransitionProps = {
  /**
   * Triggers the enter or exit states to start or stop transitions
   */
  inProp?: boolean;
  /**
   * Content to apply transitions
   */
  children: ReactElement;
};

const SlideTransition: FC<SlideTransitionProps> = ({ inProp, children }) => {
  const [height, setHeight] = useState<number>(0);

  return (
    <CSSTransition
      in={inProp}
      timeout={transitionConfig.duration}
      classNames={{
        enter: styles.slideEnter,
        enterActive: styles.slideEnterActive,
        exit: styles.slideExit,
        exitActive: styles.slideExitActive,
      }}
      unmountOnExit
      onEnter={(el: HTMLElement) => {
        setHeight(el.clientHeight);
        el.setAttribute('style', 'height: 0');
      }}
      onEntering={(el: HTMLElement) => {
        el.setAttribute('style', `height: ${height}px`);
      }}
      onExiting={(el: HTMLElement) => {
        el.setAttribute('style', 'height: 0');
      }}
    >
      {children}
    </CSSTransition>
  );
};

export { SlideTransition, SlideTransitionProps };
