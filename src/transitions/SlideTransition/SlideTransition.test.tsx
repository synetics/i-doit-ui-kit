import { render, screen } from '@testing-library/react';
import React from 'react';
import { SlideTransition } from './SlideTransition';

describe('SlideTransition', () => {
  it('does not render content to the document', () => {
    render(
      <SlideTransition>
        <div>content</div>
      </SlideTransition>,
    );

    expect(screen.queryByText('content')).not.toBeInTheDocument();
  });

  it('renders given content to the document', () => {
    render(
      <SlideTransition inProp>
        <div>content</div>
      </SlideTransition>,
    );

    expect(screen.getByText('content')).toBeInTheDocument();
  });
});
