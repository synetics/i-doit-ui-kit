import { ReactNode, useState } from 'react';
import { Collapse, CollapseButton, CollapseDetails, CollapseSummary } from '../Collapse';
import { Text } from '../../Text';

export default {
  title: 'Components/Molecules/Collapse',
  component: Collapse,
};

export const Uncontrolled = (): ReactNode => (
  <Collapse>
    <CollapseSummary>
      <Text tag="span">Summary</Text>
      <CollapseButton />
    </CollapseSummary>
    <CollapseDetails>
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse, porro veniam! Accusamus hic impedit labore maxime
      sit! Dolor impedit iste magni odit sit voluptas. Architecto earum ex obcaecati rem sint. Lorem ipsum dolor sit
      amet, consectetur adipisicing elit. Esse, porro veniam! Accusamus hic impedit labore maxime sit! Dolor impedit
      iste magni odit sit voluptas. Architecto earum ex obcaecati rem sint. Lorem ipsum dolor sit amet, consectetur
      adipisicing elit. Esse, porro veniam! Accusamus hic impedit labore maxime sit! Dolor impedit iste magni odit sit
      voluptas. Architecto earum ex obcaecati rem sint.
    </CollapseDetails>
  </Collapse>
);

export const Controlled = (): ReactNode => {
  const [isOpen, setIsOpen] = useState(false);

  return (
    <Collapse isOpen={isOpen} onChange={setIsOpen}>
      <CollapseSummary>
        <Text tag="span">Summary</Text>
        <CollapseButton />
      </CollapseSummary>
      <CollapseDetails>
        <div>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse, porro veniam! Accusamus hic impedit labore
          maxime sit! Dolor impedit iste magni odit sit voluptas. Architecto earum ex obcaecati rem sint. Lorem ipsum
          dolor sit amet, consectetur adipisicing elit. Esse, porro veniam! Accusamus hic impedit labore maxime sit!
          Dolor impedit iste magni odit sit voluptas. Architecto earum ex obcaecati rem sint. Lorem ipsum dolor sit
          amet, consectetur adipisicing elit. Esse, porro veniam! Accusamus hic impedit labore maxime sit! Dolor impedit
          iste magni odit sit voluptas. Architecto earum ex obcaecati rem sint.
        </div>
      </CollapseDetails>
    </Collapse>
  );
};
