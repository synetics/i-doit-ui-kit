import {
  cloneElement,
  forwardRef,
  isValidElement,
  ReactElement,
  ReactNode,
  useCallback,
  useMemo,
  useState,
} from 'react';
import classnames from 'classnames';
import { SlideTransition } from '../../transitions';
import { useControlledState } from '../../hooks';
import { createLocalContext } from '../../utils';
import { chevron_down } from '../../icons';
import { Button, ButtonProps } from '../Button';
import { Icon } from '../Icon';
import styles from './Collapse.module.scss';

type CollapseContextValue = {
  isOpen: boolean;
  open: () => void;
  close: () => void;
  onChange?: (isOpen: boolean) => void;
  isControlled: boolean;
};

const [CollapseContextProvider, useCollapseContext] = createLocalContext<CollapseContextValue>('Collapse');

const useCollapse = (forwardedIsOpen?: boolean) => {
  const [isOpen, setIsOpen] = useControlledState(forwardedIsOpen || false);

  const open = useCallback(() => {
    setIsOpen(true);
  }, [setIsOpen]);

  const close = useCallback(() => {
    setIsOpen(false);
  }, [setIsOpen]);

  return {
    isOpen,
    open,
    close,
  } as const;
};

type CollapseProps = {
  /**
   * Content that holds short summary and details
   */
  children: ReactElement[];

  /**
   * Defines visibility of the section with details
   */
  isOpen?: boolean;

  /**
   * A callback function that fires at the moment when isOpen value of the component is changed
   */
  onChange?: (isOpen: boolean) => void;
};

const Collapse = ({ children, isOpen: forwardedIsOpen, onChange }: CollapseProps) => {
  const [isControlled] = useState(forwardedIsOpen !== undefined && typeof onChange === 'function');
  const { isOpen, open, close } = useCollapse(forwardedIsOpen);

  const contextValue = useMemo(
    () => ({
      close,
      isControlled,
      isOpen,
      onChange,
      open,
    }),
    [close, isControlled, isOpen, onChange, open],
  );

  return (
    <CollapseContextProvider value={contextValue}>
      <div className={styles.collapse} data-testid="idoit-collapse">
        {children}
      </div>
    </CollapseContextProvider>
  );
};

type CollapseSummaryProps = {
  /**
   * Content that holds details
   */
  children: ReactNode | ReactNode[];
  /**
   * Class of the container
   */
  className?: string;
};

const CollapseSummary = ({ children, className }: CollapseSummaryProps) => (
  <div className={classnames(styles.collapseSummary, className)}>{children}</div>
);

const CollapseButton = forwardRef<HTMLButtonElement, ButtonProps>(({ className, ...props }, ref) => {
  const { isControlled, isOpen, open, close, onChange } = useCollapseContext();

  const handleOnClick = () => {
    if (!isControlled) {
      if (isOpen) {
        close();
      } else {
        open();
      }
    }

    if (onChange) {
      onChange(!isOpen);
    }
  };

  return (
    <Button
      onClick={handleOnClick}
      variant="text"
      icon={
        <Icon
          src={chevron_down}
          className={classnames(styles.triggerIcon, {
            [styles.active]: isOpen,
          })}
        />
      }
      iconPosition="right"
      className={classnames(styles.collapseButton, className)}
      {...props}
      ref={ref}
      activeState={isOpen}
    >
      {isOpen ? 'Hide' : 'Show'}
    </Button>
  );
});

type CollapseDetailsProps = {
  /**
   * Content that holds details
   */
  children: ReactNode;

  /**
   * An extra className to be added to the element that holds details
   */
  className?: string;
};

const CollapseDetails = ({ children, className }: CollapseDetailsProps) => {
  const { isOpen } = useCollapseContext();
  const child = isValidElement(children) ? children : <div>{children}</div>;

  return (
    <SlideTransition inProp={isOpen}>
      {cloneElement(child, {
        className: classnames(styles.collapseDetails, className),
      })}
    </SlideTransition>
  );
};

export { Collapse, CollapseProps, CollapseSummary, CollapseDetails, CollapseButton };
