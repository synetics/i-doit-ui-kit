import { render, screen } from '@testing-library/react';
import user from '@testing-library/user-event';
import { Collapse, CollapseButton, CollapseDetails, CollapseSummary } from './Collapse';

describe('Collapse', () => {
  it('renders without crashing', () => {
    render(
      <Collapse>
        <CollapseSummary>summary</CollapseSummary>
        <CollapseDetails>details</CollapseDetails>
      </Collapse>,
    );

    expect(screen.getByTestId('idoit-collapse')).toBeInTheDocument();
  });

  it('renders summary without details', () => {
    render(
      <Collapse>
        <CollapseSummary>summary</CollapseSummary>
        <CollapseDetails>details</CollapseDetails>
      </Collapse>,
    );

    expect(screen.queryByText('summary')).toBeInTheDocument();
    expect(screen.queryByText('details')).not.toBeInTheDocument();
  });

  it('renders summary and details', () => {
    render(
      <Collapse isOpen>
        <CollapseSummary>summary</CollapseSummary>
        <CollapseDetails>details</CollapseDetails>
      </Collapse>,
    );

    expect(screen.queryByText('summary')).toBeInTheDocument();
    expect(screen.queryByText('details')).toBeInTheDocument();
  });

  it('shows the section with details by click on the action button', async () => {
    render(
      <Collapse>
        <CollapseSummary>
          <div>summary</div>
          <CollapseButton />
        </CollapseSummary>
        <CollapseDetails>details</CollapseDetails>
      </Collapse>,
    );
    const actionButton = screen.getByRole('button', { name: 'Show' });
    expect(screen.queryByText('details')).not.toBeInTheDocument();

    await user.click(actionButton);

    expect(screen.queryByText('details')).toBeInTheDocument();
    expect(actionButton).toHaveTextContent('Hide');
  });

  it('hides the section with details by click on the action button', async () => {
    render(
      <Collapse>
        <CollapseSummary>
          <div>summary</div>
          <CollapseButton />
        </CollapseSummary>
        <CollapseDetails>details</CollapseDetails>
      </Collapse>,
    );
    const actionButton = screen.getByRole('button', { name: 'Show' });
    await user.click(actionButton);
    expect(actionButton).toHaveTextContent('Hide');

    await user.click(actionButton);

    expect(screen.queryByText('details')).not.toBeInTheDocument();
    expect(actionButton).toHaveTextContent('Show');
  });

  it('emits onChange event with payload of opened state (uncontrolled)', async () => {
    const handleChangeMock = jest.fn();
    render(
      <Collapse onChange={handleChangeMock}>
        <CollapseSummary>
          <div>summary</div>
          <CollapseButton />
        </CollapseSummary>
        <CollapseDetails>details</CollapseDetails>
      </Collapse>,
    );

    await user.click(screen.getByRole('button', { name: 'Show' }));

    expect(handleChangeMock).toHaveBeenNthCalledWith(1, true);
  });

  it('emits onChange event with payload of closed state (uncontrolled)', async () => {
    const handleChangeMock = jest.fn();
    render(
      <Collapse onChange={handleChangeMock}>
        <CollapseSummary>
          <div>summary</div>
          <CollapseButton />
        </CollapseSummary>
        <CollapseDetails>details</CollapseDetails>
      </Collapse>,
    );
    const actionButton = screen.getByRole('button', { name: 'Show' });
    await user.click(actionButton);

    await user.click(actionButton);

    expect(handleChangeMock).toHaveBeenNthCalledWith(2, false);
  });

  it('only emits onChange event with payload of opened state (controlled)', async () => {
    const handleChangeMock = jest.fn();
    render(
      <Collapse isOpen={false} onChange={handleChangeMock}>
        <CollapseSummary>
          <div>summary</div>
          <CollapseButton />
        </CollapseSummary>
        <CollapseDetails>details</CollapseDetails>
      </Collapse>,
    );

    await user.click(screen.getByRole('button', { name: 'Show' }));

    expect(handleChangeMock).toHaveBeenNthCalledWith(1, true);
  });

  it('only emits onChange event with payload of closed state (controlled)', async () => {
    const handleChangeMock = jest.fn();
    render(
      <Collapse isOpen onChange={handleChangeMock}>
        <CollapseSummary>
          <div>summary</div>
          <CollapseButton />
        </CollapseSummary>
        <CollapseDetails>details</CollapseDetails>
      </Collapse>,
    );

    await user.click(screen.getByRole('button', { name: 'Hide' }));

    expect(handleChangeMock).toHaveBeenNthCalledWith(1, false);
  });

  it('performs transition effect on react nodes', () => {
    render(
      <Collapse isOpen>
        <CollapseSummary>summary</CollapseSummary>
        <CollapseDetails>details</CollapseDetails>
      </Collapse>,
    );

    expect(screen.getByText('details').parentNode).toHaveClass('enter', 'enterActive', 'exit', 'exitActive');
  });

  it('performs transition effect on react elements', () => {
    render(
      <Collapse isOpen>
        <CollapseSummary>summary</CollapseSummary>
        <CollapseDetails>
          <div>details</div>
        </CollapseDetails>
      </Collapse>,
    );

    expect(screen.getByText('details').parentNode).toHaveClass('enter', 'enterActive', 'exit', 'exitActive');
  });

  it('adds extra className to the CollapseDetails', () => {
    render(
      <Collapse isOpen>
        <CollapseSummary>summary</CollapseSummary>
        <CollapseDetails className="extraClassName">details</CollapseDetails>
      </Collapse>,
    );

    expect(screen.getByText('details')).toHaveClass('extraClassName');
  });

  it('adds extra className to the CollapseSummary', () => {
    render(
      <Collapse isOpen>
        <CollapseSummary className="extraClassName">summary</CollapseSummary>
        <CollapseDetails>details</CollapseDetails>
      </Collapse>,
    );

    expect(screen.getByText('summary')).toHaveClass('extraClassName');
  });
});
