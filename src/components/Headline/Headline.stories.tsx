import { lorem } from 'faker';
import { ReactNode } from 'react';
import { Headline } from './Headline';

export default {
  title: 'Components/Atoms/Headline',
  component: Headline,
};

export const Regular = (): ReactNode => (
  <>
    <Headline tag="h1">H1 - {lorem.sentences(1)}</Headline>
    <hr />
    <Headline tag="h2">H2 - {lorem.sentences(1)}</Headline>
    <hr />
    <Headline tag="h3">H3 - {lorem.sentences(1)}</Headline>
    <hr />
    <Headline tag="h4">H4 - {lorem.sentences(1)}</Headline>
    <hr />
    <Headline tag="h5">H5 - {lorem.sentences(1)}</Headline>
    <hr />
    <Headline tag="h6">H6 - {lorem.sentences(1)}</Headline>
  </>
);

export const Display = (): ReactNode => (
  <>
    <Headline as="h1">
      <code>div</code> as <code>h1</code> - {lorem.sentences(1)}
    </Headline>
    <hr />
    <Headline as="h2">
      <code>div</code> as <code>h2</code> - {lorem.sentences(1)}
    </Headline>
    <hr />
    <Headline as="h3">
      <code>div</code> as <code>h3</code> - {lorem.sentences(1)}
    </Headline>
    <hr />
    <Headline as="h4">
      <code>div</code> as <code>h4</code> - {lorem.sentences(1)}
    </Headline>
    <hr />
    <Headline as="h5">
      <code>div</code> as <code>h5</code> - {lorem.sentences(1)}
    </Headline>
    <hr />
    <Headline as="h6">
      <code>div</code> as <code>h6</code> - {lorem.sentences(1)}
    </Headline>
  </>
);

export const Semantic = (): ReactNode => (
  <>
    <Headline tag="h1" as="h2">
      <code>h1</code> as <code>h2</code> - {lorem.sentences(1)}
    </Headline>
    <hr />
    <Headline tag="h1" as="h3">
      <code>h1</code> as <code>h3</code> - {lorem.sentences(1)}
    </Headline>
    <hr />
    <Headline tag="h2" as="h1">
      <code>h2</code> as <code>h1</code> - {lorem.sentences(1)}
    </Headline>
    <hr />
    <Headline tag="h2" as="h3">
      <code>h2</code> as <code>h3</code> - {lorem.sentences(1)}
    </Headline>
    <hr />
    <Headline tag="h3" as="h1">
      <code>h3</code> as <code>h1</code> - {lorem.sentences(1)}
    </Headline>
    <hr />
    <Headline tag="h3" as="h2">
      <code>h3</code> as <code>h2</code> - {lorem.sentences(1)}
    </Headline>
  </>
);

export const Examples = (): ReactNode => (
  <>
    <Headline tag="h2">H2 Heading</Headline>
  </>
);

/**
 *
 * Sass Variables
 *
 */
export const Variables = (): ReactNode => (
  <table className="table table-striped">
    <thead>
      <tr>
        <th>Variable Name</th>
        <th>Default value</th>
        <th>Computed value</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td colSpan={3} className="text-center">
          <strong>All headlines</strong>
        </td>
      </tr>
      <tr>
        <td>$idoit-headings-margin-bottom</td>
        <td>1rem * 0.4</td>
        <td>4px</td>
      </tr>
      <tr>
        <td>$idoit-headings-font-weight</td>
        <td>$idoit-font-weight-semi-bold</td>
        <td>600</td>
      </tr>
      <tr>
        <td colSpan={3} className="text-center">
          <strong>H1</strong>
        </td>
      </tr>
      <tr>
        <td>$idoit-h1-font-size</td>
        <td>2rem</td>
        <td>20px</td>
      </tr>
      <tr>
        <td>$idoit-h1-line-height</td>
        <td>2.4rem</td>
        <td>24px</td>
      </tr>
      <tr>
        <td>$idoit-h1-font-weight</td>
        <td>$idoit-headings-font-weight</td>
        <td>600</td>
      </tr>
      <tr>
        <td colSpan={3} className="text-center">
          <strong>H2</strong>
        </td>
      </tr>
      <tr>
        <td>$idoit-h2-font-size</td>
        <td>1.6rem</td>
        <td>16px</td>
      </tr>
      <tr>
        <td>$idoit-h2-line-height</td>
        <td>2rem</td>
        <td>20px</td>
      </tr>
      <tr>
        <td>$idoit-h2-font-weight</td>
        <td>$idoit-headings-font-weight</td>
        <td>600</td>
      </tr>
      <tr>
        <td colSpan={3} className="text-center">
          <strong>H3</strong>
        </td>
      </tr>
      <tr>
        <td>$idoit-h3-font-size</td>
        <td>$idoit-h2-font-size</td>
        <td>16px</td>
      </tr>
      <tr>
        <td>$idoit-h3-line-height</td>
        <td>$idoit-h2-line-height</td>
        <td>20px</td>
      </tr>
      <tr>
        <td>$idoit-h3-font-weight</td>
        <td>$idoit-font-weight-normal</td>
        <td>400</td>
      </tr>
      <tr>
        <td colSpan={3} className="text-center">
          <strong>H4</strong>
        </td>
      </tr>
      <tr>
        <td>$idoit-h4-font-size</td>
        <td>$idoit-font-size-regular</td>
        <td>13px</td>
      </tr>
      <tr>
        <td>$idoit-h4-line-height</td>
        <td>$idoit-line-height-regular</td>
        <td>1.23076923077 or 16px</td>
      </tr>
      <tr>
        <td>$idoit-h4-font-weight</td>
        <td>$idoit-font-weight-normal</td>
        <td>400</td>
      </tr>
      <tr>
        <td colSpan={3} className="text-center">
          <strong>H5</strong>
        </td>
      </tr>
      <tr>
        <td>$idoit-h5-font-size</td>
        <td>$idoit-font-size-regular</td>
        <td>13px</td>
      </tr>
      <tr>
        <td>$idoit-h5-line-height</td>
        <td>$idoit-line-height-regular</td>
        <td>1.23076923077 or 16px</td>
      </tr>
      <tr>
        <td>$idoit-h5-font-weight</td>
        <td>$idoit-font-weight-normal</td>
        <td>400</td>
      </tr>
      <tr>
        <td colSpan={3} className="text-center">
          <strong>H6</strong>
        </td>
      </tr>
      <tr>
        <td>$idoit-h6-font-size</td>
        <td>$idoit-font-size-regular</td>
        <td>13px</td>
      </tr>
      <tr>
        <td>$idoit-h6-line-height</td>
        <td>$idoit-line-height-regular</td>
        <td>1.23076923077 or 16px</td>
      </tr>
      <tr>
        <td>$idoit-h6-font-weight</td>
        <td>$idoit-font-weight-normal</td>
        <td>400</td>
      </tr>
    </tbody>
  </table>
);
