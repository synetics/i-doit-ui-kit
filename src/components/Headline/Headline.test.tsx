import { screen } from '@testing-library/react';
import { createRenderer } from '../../../test/utils/renderer';
import { Headline, HeadlineTag, HeadlineType } from './Headline';

const render = createRenderer(Headline, { children: 'Test' });

describe('Headline', () => {
  it('renders without crashing', () => {
    const { children } = render();

    expect(screen.getByText(children as string)).toBeInTheDocument();
  });

  describe('tag name', () => {
    it.each<[HeadlineTag, number]>([
      ['h1', 1],
      ['h2', 2],
      ['h3', 3],
      ['h4', 4],
      ['h5', 5],
      ['h6', 6],
    ])('renders %s tag name', (tag, level) => {
      render({ tag });

      expect(screen.getByRole('heading', { level })).toBeInTheDocument();
    });

    it('renders default tag name', () => {
      const { children } = render();

      expect(screen.getByText(children as string).tagName).toBe('DIV');
    });
  });

  it('renders with a classname', () => {
    const props = { className: 'className' };

    const { children } = render(props);

    expect(screen.getByText(children as string)).toHaveClass(props.className);
  });

  it.each<[HeadlineType]>([['h1'], ['h2'], ['h3'], ['h4'], ['h5'], ['h6']])('displays the heading as %s', (as) => {
    render({ as, tag: 'h1' });

    expect(screen.getByRole('heading')).toHaveClass(as);
  });
});
