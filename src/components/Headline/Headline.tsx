import { createElement, forwardRef, PropsWithChildren } from 'react';
import classnames from 'classnames';
import styles from './Headline.module.scss';

type HeadlineType = 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'h6';

type HeadlineTag = HeadlineType | 'div';

type HeadlineProps = PropsWithChildren<{
  /**
   * Defines view of the content
   */
  as?: HeadlineType;

  /**
   * An extra className to be added to the root element.
   */
  className?: string;

  /**
   * Represents the root element
   */
  tag?: HeadlineTag;
}>;

const Headline = forwardRef<HTMLHeadingElement, HeadlineProps>(
  ({ as, children, className, tag = 'div', ...props }, ref) =>
    createElement(tag, { className: classnames(as && styles[as], className), ref, ...props }, children),
);

export { Headline, HeadlineTag, HeadlineType, HeadlineProps };
