import { ReactNode } from 'react';
import { Tags } from '../Tags';
import { Tag } from '../Tag';
import tagClasses from '../Tag.module.scss';
import tagsClasses from '../Tags.module.scss';

const items = [
  { value: '1', label: 'Label1' },
  { value: '2', label: 'Label2' },
];

export default {
  title: 'Components/Atoms/Tags/Tags',
  component: Tags,
};

/**
 *
 * Tags Item needed for multiple tags view and adjusting of keyboard handling for it:
 *
 */
export const Active = (): ReactNode => (
  <Tags className={tagsClasses.tags} enabled>
    {items.map((a) => (
      <Tag disabled={false} className={tagClasses.tag} key={a.value} onClose={() => {}}>
        {a.label}
      </Tag>
    ))}
  </Tags>
);

export const Disabled = (): ReactNode => (
  <Tags className={tagsClasses.tags} enabled={false}>
    {items.map((a) => (
      <Tag disabled className={tagClasses.tag} key={a.value} onClose={() => {}}>
        {a.label}
      </Tag>
    ))}
  </Tags>
);
