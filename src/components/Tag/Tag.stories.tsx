import { ReactNode, useState } from 'react';
import { placeholder, open_tab, call } from '../../icons';
import { Icon } from '../Icon';
import { FormGroup, Label } from '../Form/Infrastructure';
import { Input } from '../Form/Control';
import { Tag } from './Tag';

export default {
  title: 'Components/Atoms/Tags/Tag',
  component: Tag,
};

/**
 *
 * Tags are used in fields where user can discard the tag and it’s value.
 *
 * They are compact elements that display input of a user, e.g. in multi value fields.
 *
 */

export const Introduction = (): ReactNode => {
  const [tagContent, setTagContent] = useState(`{Tag}`);
  const text = tagContent || '{Tag}';
  return (
    <div>
      <div style={{ width: 300 }}>
        <FormGroup className="pb-2">
          <Label id="text">Tag content</Label>
          <Input placeholder="{Tag}" value={tagContent} onChange={(v) => setTagContent(`${v}`)} id="text" />
        </FormGroup>
      </div>
      <table className="table">
        <thead>
          <tr>
            <th scope="col">&nbsp;</th>
            <th scope="col">Text</th>
            <th scope="col">Icon-Text</th>
            <th scope="col">Icon-Text-Action</th>
            <th scope="col">Text-Action</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Active</td>
            <td>
              <Tag>{text}</Tag>
            </td>
            <td>
              <Tag prefix={<Icon src={placeholder} wrapper="span" width={16} />}>{text}</Tag>
            </td>
            <td>
              <Tag prefix={<Icon src={placeholder} wrapper="span" width={16} />} onClose={() => alert('Removed')}>
                {text}
              </Tag>
            </td>
            <td>
              <Tag onClose={() => alert('Removed')}>{text}</Tag>
            </td>
          </tr>
          <tr>
            <td>Disabled</td>
            <td>
              <Tag disabled>{text}</Tag>
            </td>
            <td>
              <Tag disabled prefix={<Icon src={placeholder} wrapper="span" width={16} />}>
                {text}
              </Tag>
            </td>
            <td>
              <Tag
                disabled
                prefix={<Icon src={placeholder} wrapper="span" width={16} />}
                onClose={() => alert('Removed')}
              >
                {text}
              </Tag>
            </td>
            <td>
              <Tag disabled onClose={() => alert('Removed')}>
                {text}
              </Tag>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  );
};

export const Examples = (): ReactNode => (
  <>
    <p className="mb-6">Sometimes, tags are used for links:</p>

    <Tag link="https://i-doit.com" prefix={<Icon src={open_tab} wrapper="span" width={16} />}>
      i-doit.com
    </Tag>

    <p className="my-6">or for telephone numbers:</p>

    <Tag
      link="tel://+491234567890"
      tooltipProps={{
        contentTooltipLabel: 'Call',
      }}
      onClick={() => window.open('tel://+491234567890', '_blank')}
      prefix={<Icon src={call} wrapper="span" width={16} />}
    >
      +491234567890
    </Tag>
  </>
);
