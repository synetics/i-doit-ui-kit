import { fireEvent, render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { Icon } from '../Icon';
import { Tag } from './Tag';
import { Tags } from './Tags';

describe('Tag', () => {
  const icon = <Icon src="image/src" />;
  const onClose = jest.fn();
  const tagContent = 'Test';

  it('renders regular tag', () => {
    render(<Tag>{tagContent}</Tag>);

    expect(screen.getByText(tagContent)).toBeInTheDocument();
  });

  it('renders tag if no valid', () => {
    render(<Tags enabled>{true}</Tags>);

    expect(screen.queryByText(tagContent)).not.toBeInTheDocument();
  });

  it('renders with prefix icon', () => {
    render(<Tag prefix={icon}>{tagContent}</Tag>);

    expect(screen.getByText(tagContent)).toBeInTheDocument();
    expect(screen.getByTestId('icon')).toBeInTheDocument();
  });

  it('renders with close icon', () => {
    render(<Tag onClose={onClose}>{tagContent}</Tag>);

    expect(screen.getByText(tagContent)).toBeInTheDocument();
    expect(screen.getByTestId('icon')).toBeInTheDocument();
  });

  it('renders with valid link', () => {
    const validLink = 'https://i-doit.com';

    render(
      <Tag onClose={onClose} link={validLink}>
        {tagContent}
      </Tag>,
    );

    expect(screen.getByText(tagContent)).toBeInTheDocument();
    expect(screen.getByText(tagContent).closest('a')).toHaveAttribute('href', validLink);
  });

  it('renders with invalid link', () => {
    const invalidLink = '/';

    render(
      <Tag onClose={onClose} link={invalidLink}>
        {tagContent}
      </Tag>,
    );

    expect(screen.getByText(tagContent)).toBeInTheDocument();
    expect(screen.getByText(tagContent).closest('a')).not.toBeInTheDocument();
  });

  it('renders disabled tag', () => {
    // Close and prefix icons
    const numberOfIcons = 2;

    const { container } = render(
      <Tag onClose={onClose} disabled prefix={icon}>
        {tagContent}
      </Tag>,
    );

    expect(container.getElementsByClassName('tag-content-disabled').length).toBe(1);
    expect(container.getElementsByClassName('tag-icon-disabled').length).toBe(numberOfIcons);
  });

  it('renders tag with tooltip', async () => {
    const tooltipContent = 'Test tooltip content';

    render(
      <Tag onClose={onClose} prefix={icon} tooltipProps={{ contentTooltipLabel: tooltipContent }}>
        {tagContent}
      </Tag>,
    );

    const trigger = screen.getByText(tagContent);

    await userEvent.hover(trigger);
    expect(screen.getByText(tooltipContent)).toBeInTheDocument();
  });
  it('call callback on onKeydown', () => {
    const cb = jest.fn();
    render(
      <Tag onKeyDown={cb} onClose={() => {}}>
        {tagContent}
      </Tag>,
    );

    const trigger = screen.getByTestId('tag-remove-button');

    fireEvent.keyDown(trigger);
    expect(cb).toHaveBeenCalled();
  });

  it('stopPropagation on menu click', async () => {
    const cb = jest.fn();
    render(
      <Tag onClick={cb} onClose={() => {}}>
        {tagContent}
      </Tag>,
    );

    const trigger = screen.getByRole('menu');

    await userEvent.click(trigger);
    expect(cb).toHaveBeenCalled();
  });

  it('stopPropagation on menu click', async () => {
    const cb = jest.fn();
    render(
      <Tag onClose={() => {}}>
        <button onClick={cb}>{tagContent}</button>
      </Tag>,
    );

    const trigger = screen.getByRole('menu');

    await userEvent.click(trigger);
    expect(cb).not.toHaveBeenCalled();
  });
  it('handles enter keydown', async () => {
    render(
      <Tag onClose={onClose} prefix={icon}>
        {tagContent}
      </Tag>,
    );

    const button = screen.getByTestId('tag-remove-button');

    await userEvent.tab();

    fireEvent.keyDown(button, {
      key: 'Enter',
      code: 'Enter',
      keyCode: 13,
      charCode: 13,
    });

    expect(onClose.mock.calls.length).toBe(1);
  });
});
