import classnames from 'classnames';
import React, { BaseSyntheticEvent, forwardRef, HTMLAttributes, useCallback, useEffect, useMemo } from 'react';
import { close } from '../../icons';
import { isValidUrl } from '../../utils/valid-url';
import { KeyboardKeys } from '../../constants/constants';
import { useGlobalizedRef } from '../../hooks';
import { Icon } from '../Icon';
import { Tooltip } from '../Tooltip';
import { OverflowWithTooltip } from '../OverflowWithTooltip';
import styles from './Tag.module.scss';

export type TooltipProps = {
  /**
   * Force this label to be in tooltip popper when user hover on close icon
   */
  closeTooltipLabel?: string;
  /**
   * Force this label to be in tooltip popper when user hover on tag's content
   */
  contentTooltipLabel?: string;
};
export type TagProps = {
  /**
   * Class name for tag
   */
  className?: string;
  /**
   * Action for onClose icon
   */
  onClose?: (e?: React.BaseSyntheticEvent) => void;
  /**
   * Will transform content to link
   */
  link?: string;
  /**
   * Component which renders before tag content
   */
  prefix?: React.ReactNode;
  /**
   * Disables tag
   */
  disabled?: boolean;
  /**
   * Prop for controlling tag's tooltip
   */
  tooltipProps?: TooltipProps;
  /**
   * Prop to handle click event on item
   */
  onClick?: () => void;
  /**
   * Prop to handle onKeyDown event on item
   */
  onKeyDown?: (event: React.KeyboardEvent | React.BaseSyntheticEvent<Event>) => void;
  /**
   * Prop to define that should recieve a focus
   */
  autoFocus?: boolean;
} & Omit<HTMLAttributes<HTMLElement>, 'prefix'>;

const Tag = forwardRef<HTMLDivElement, TagProps>(
  (
    { children, autoFocus, tabIndex = 0, className, onClose, link, prefix, disabled, tooltipProps, onClick, onKeyDown },
    ref,
  ) => {
    const { localRef, setRefsCallback } = useGlobalizedRef(ref);
    const contentLabel = tooltipProps && tooltipProps.contentTooltipLabel;
    const tooltipContent = useMemo(() => {
      if (contentLabel) return contentLabel;
      if (link) return 'Open';
      return children as React.ReactElement;
    }, [contentLabel, children, link]);

    const content = (
      <OverflowWithTooltip
        containerClassName={styles['tag-label-wrapper']}
        tooltipContent={tooltipContent}
        forceTooltip={Boolean(link) || Boolean(contentLabel)}
      >
        {children}
      </OverflowWithTooltip>
    );

    const contentText =
      link && isValidUrl(link) ? (
        <a href={link} rel="noopener noreferrer" tabIndex={-1} target="_blank">
          {content}
        </a>
      ) : (
        content
      );

    const onInnerKeyDown = (event: React.KeyboardEvent): void => {
      if (typeof onKeyDown === 'function') onKeyDown(event);
      if (onClose && event.key === KeyboardKeys.Enter) {
        event.preventDefault();
        event.stopPropagation();
        onClose(event);
      }
    };
    const onInnerClick = (event: BaseSyntheticEvent): void => {
      event.preventDefault();
      event.stopPropagation();
      if (onClose) {
        onClose(event);
      }
    };
    useEffect(() => {
      if (autoFocus && localRef.current && document.activeElement !== localRef.current) {
        localRef.current.focus();
      }
    }, [autoFocus, localRef]);
    const handleClickOnWrapper = useCallback(
      (e: React.MouseEvent | React.KeyboardEvent) => {
        e.stopPropagation();
        if (onClick) {
          onClick();
        }
      },
      [onClick],
    );
    // We want to turn off focus when tag is disabled and eslint can't parse it
    /* eslint-disable jsx-a11y/interactive-supports-focus */
    return (
      <div
        className={classnames(styles['tag-wrapper'], className)}
        onClick={handleClickOnWrapper}
        onKeyDown={handleClickOnWrapper}
        role="button"
      >
        <div
          className={classnames(
            styles['tag-content'],
            disabled && styles['tag-content-disabled'],
            onClose && styles['tag-content-with-close'],
          )}
          onClick={onClick ? undefined : (e: React.MouseEvent) => e.stopPropagation()}
          onKeyDown={() => false}
          role="menu"
        >
          {prefix && <span className={classnames(styles['tag-prefix'])}>{prefix}</span>}
          {contentText}
        </div>
        {onClose && (
          <Tooltip content={contentLabel || 'Remove'} placement="bottom">
            <span
              data-testid="tag-remove-button"
              onKeyDown={disabled ? undefined : onInnerKeyDown}
              onClick={disabled ? undefined : onInnerClick}
              onMouseDown={disabled ? undefined : onInnerClick}
              tabIndex={disabled ? undefined : tabIndex}
              role="button"
              className={classnames(
                styles['tag-icon-wrapper'],
                styles['tag-close'],
                disabled && styles['tag-icon-disabled'],
              )}
              ref={setRefsCallback}
            >
              <Icon
                src={close}
                className={classnames(styles['tag-icon'], disabled && styles['tag-icon-disabled'])}
                height={16}
                wrapper="span"
              />
            </span>
          </Tooltip>
        )}
      </div>
    );
  },
);

export { Tag };
