import React, { ForwardedRef, forwardRef, isValidElement, ReactNode } from 'react';
import classnames from 'classnames';
import { CSSTransition } from 'react-transition-group';
import TransitionGroup from 'react-transition-group/TransitionGroup';
import { useGlobalizedRef } from '../../hooks';
import { Container, ContainerProps } from '../Container';
import classes from './Tags.module.scss';

type TagsType = ContainerProps & {
  enabled: boolean;
  autoFocus?: boolean;
  focusIndex?: number;
};

export type TagItemProps = {
  label: string;
  link?: string;
  value: string;
  icon?: string;
  view?: ReactNode;
};

const hasClassName = (props: unknown): props is { className: string } =>
  typeof props === 'object' && Object.hasOwnProperty.call(props, 'className');

const hasTabIndex = (props: unknown): props is { tabIndex: number } =>
  typeof props === 'object' && Object.hasOwnProperty.call(props, 'tabIndex');

const Tags = forwardRef(
  ({ className, children, enabled, autoFocus, focusIndex, ...rest }: TagsType, ref: ForwardedRef<unknown>) => {
    const { setRefsCallback } = useGlobalizedRef(ref);

    return (
      <Container {...rest} ref={setRefsCallback} className={classnames(className, classes.tags)}>
        <TransitionGroup component={null}>
          {React.Children.map(children, (child, i) => {
            if (!isValidElement(child)) {
              return null;
            }

            let tabIndex = i === focusIndex ? 0 : -1;
            if (hasTabIndex(child.props)) {
              tabIndex = child.props.tabIndex;
            }

            return (
              <CSSTransition
                key={child.key || i.toString()}
                timeout={200}
                classNames={{
                  enter: classes.tagItemEnterActive,
                  exitActive: classes.tagItemExitDone,
                }}
                onExit={() => {
                  /* istanbul ignore next */
                  setTimeout(() => {
                    const activeElement = document?.activeElement as HTMLElement;
                    if (activeElement.getAttribute('role') === 'button') {
                      activeElement?.blur();
                      activeElement?.focus();
                    }
                  }, 250);
                }}
                unmountOnExit
              >
                {React.cloneElement(child, {
                  autoFocus: enabled && autoFocus && i === focusIndex,
                  tabIndex,
                  className: classnames([classes.tagItem, hasClassName(child.props) ? child.props.className : '']),
                })}
              </CSSTransition>
            );
          })}
        </TransitionGroup>
      </Container>
    );
  },
);

export { Tags, TagsType };
