import { fireEvent, render, screen } from '@testing-library/react';
import user from '@testing-library/user-event';
import { Table } from '../../Table';
import { WithContextMenu } from './WithContextMenu';

type CellPropTypes = { className?: string; value: string };

const Cell = WithContextMenu<CellPropTypes>(
  ({ className, value }) => <div className={className}>{value}</div>,
  () => <div>Children</div>,
);

const columns = [{ accessor: 'value' as const, Cell }];

describe('WithContextMenu', () => {
  it('renders the table with a cell', () => {
    render(<Table columns={columns} data={[{ value: 'value' }]} />);

    expect(screen.getByText('value')).toBeInTheDocument();
    expect(screen.queryByText('Children')).not.toBeInTheDocument();
    fireEvent.contextMenu(screen.getByText('value'));
    expect(screen.queryByText('Children')).toBeInTheDocument();
  });

  it('reacts on focus', async () => {
    render(<Table columns={columns} data={[{ value: 'value' }]} />);

    const icons = screen.queryAllByTestId('icon');
    expect(screen.getByText('value')).toBeInTheDocument();
    expect(screen.queryByText('Children')).not.toBeInTheDocument();
    screen.getByText('value').focus();
    await user.tab();
    await user.type(icons[icons.length - 1], '{Enter}');
    expect(screen.queryByText('Children')).toBeInTheDocument();
  });
});
