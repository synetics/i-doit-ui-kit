import { ComponentType, FC, ReactNode } from 'react';
import classnames from 'classnames';
import { useHasFocus } from '../../../hooks/use-focus-within';
import { useCallbackRef } from '../../../hooks';
import { useMenu } from '../hooks';
import { CellWrapper } from '../../Table';
import { Trigger } from '../Trigger';

type WithClassType = {
  className?: string;
};

type ChildrenFunctionType = (close: () => void) => ReactNode | null;

export const WithContextMenu = <T extends WithClassType>(
  Cell: ComponentType<T>,
  children: ChildrenFunctionType,
): FC<T> => {
  const WithContextMenuCell: FC<T> = (props: T) => {
    const { className } = props;
    const [ref, setRef] = useCallbackRef<HTMLDivElement>();
    const hasFocus = useHasFocus(ref?.parentElement || null);
    const { menu, show, api } = useMenu(ref, (close) => children(close));
    return (
      <CellWrapper ref={setRef}>
        <Cell {...props} className={classnames('flex-fill', className)} />
        {menu}
        <Trigger
          activeState={show}
          onFocus={(e) => e.stopPropagation()}
          onClick={(e) => {
            e.stopPropagation();
            api.enable();
          }}
          tabIndex={hasFocus ? 0 : -1}
        />
      </CellWrapper>
    );
  };

  return WithContextMenuCell;
};
