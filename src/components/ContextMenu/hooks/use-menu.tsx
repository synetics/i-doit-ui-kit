import { ReactNode, SyntheticEvent, useCallback } from 'react';
import { useEventListener, useToggle } from '../../../hooks';
import { ContextMenu } from '../ContextMenu';

type UseMenuResult = {
  show: boolean;
  api: {
    enable: () => void;
    disable: () => void;
    toggle: () => void;
  };
  menu: ReactNode | null;
};

export const useMenu = (trigger: Element | null, Children: (close: () => void) => ReactNode | null): UseMenuResult => {
  const [show, showApi] = useToggle(false);
  const handleOpen = useCallback(
    (e: SyntheticEvent) => {
      e.preventDefault();
      e.stopPropagation();
      showApi.enable();
    },
    [showApi],
  );
  const menu = show ? (
    <ContextMenu onClose={showApi.disable} trigger={trigger}>
      {Children(showApi.disable)}
    </ContextMenu>
  ) : null;
  useEventListener('contextmenu', handleOpen, trigger, false);

  return {
    show,
    api: showApi,
    menu,
  };
};
