import { render, screen } from '@testing-library/react';
import user from '@testing-library/user-event';
import { useCallbackRef } from '../../../hooks';
import { useMenu } from './use-menu';

describe('use-menu', () => {
  it('opens the menu on context menu', async () => {
    const Test = () => {
      const [ref, setRef] = useCallbackRef<HTMLDivElement>();
      const { menu } = useMenu(ref, () => <div>Children</div>);
      return (
        <>
          <div ref={setRef}>Trigger</div>
          {menu}
        </>
      );
    };

    render(<Test />);

    const trigger = screen.getByText('Trigger');
    expect(trigger).toBeInTheDocument();
    expect(screen.queryByText('Children')).not.toBeInTheDocument();
    await user.pointer({ keys: '[MouseRight]', target: trigger });
    expect(screen.queryByText('Children')).toBeInTheDocument();
  });
});
