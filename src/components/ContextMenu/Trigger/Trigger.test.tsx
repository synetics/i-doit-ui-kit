import { render, screen } from '@testing-library/react';
import { Trigger } from './Trigger';

describe('Trigger', () => {
  it('renders with content inside without crashing', () => {
    render(<Trigger data-testid="trigger" />);

    expect(screen.getByTestId('trigger')).toBeInTheDocument();
    expect(screen.getByTestId('icon')).toBeInTheDocument();
  });
});
