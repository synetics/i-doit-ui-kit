import { forwardRef } from 'react';
import more from '../../../../assets/icons/actions/more-vertical.svg';
import { IconButton, IconButtonProps } from '../../IconButton';
import { Icon } from '../../Icon';

type TriggerProps = Omit<IconButtonProps, 'label' | 'icon'> & Partial<Pick<IconButtonProps, 'label' | 'icon'>>;

export const Trigger = forwardRef<HTMLButtonElement, TriggerProps>((props, ref) => (
  <IconButton label="More" variant="text" icon={<Icon src={more} />} {...props} ref={ref} />
));
