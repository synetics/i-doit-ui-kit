import { ReactNode, useRef } from 'react';
import { VirtualElement } from '@popperjs/core';
import FocusTrap from 'focus-trap-react';
import classnames from 'classnames';
import { Menu } from '../../Menu';
import { Popper } from '../../Popper';
import classes from './ContextMenu.module.scss';

type ContextMenuProps = {
  trigger?: Element | VirtualElement | null;
  className?: string;
  onClose: () => void;
  children?: ReactNode;
};

export const ContextMenu = ({ className, trigger, children, onClose }: ContextMenuProps) => {
  const ref = useRef<HTMLElement>(null);

  return (
    <Popper
      className={classnames(className, classes.menu)}
      ref={ref}
      referenceElement={trigger}
      open
      options={{ placement: 'bottom-end' }}
    >
      <FocusTrap active focusTrapOptions={{ onDeactivate: onClose, clickOutsideDeactivates: true }}>
        <div>
          <Menu onCloseRequest={onClose} trigger={ref}>
            <div role="menu" tabIndex={0} />
            {children}
          </Menu>
        </div>
      </FocusTrap>
    </Popper>
  );
};
