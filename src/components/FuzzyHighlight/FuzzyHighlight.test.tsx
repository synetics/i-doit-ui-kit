import { render } from '@testing-library/react';
import React from 'react';
import { FuzzyHighlight, FuzzyHighlightType } from './FuzzyHighlight';

describe('FuzzyHighlight', () => {
  it('renders correctly with highlight and custom className', () => {
    const props: FuzzyHighlightType = {
      text: 'Some text for test',
      searchValue: 'for test',
      highlightClassName: 'test',
    };
    const { container } = render(<FuzzyHighlight {...props} />);
    const spans = container.querySelectorAll('span');
    spans.forEach((span) => {
      if (props.searchValue.includes(span.innerHTML)) {
        expect(span).toHaveClass('highlight test');
      }
    });
  });
  it('check class not highlighted elements', () => {
    const props: FuzzyHighlightType = {
      text: 'Some text for test',
      searchValue: 'for test',
      notHighlightClassName: 'test',
    };
    const { container } = render(<FuzzyHighlight {...props} />);
    const spans = container.querySelectorAll('span');
    spans.forEach((span) => {
      if (!props.searchValue.includes(span.innerHTML.toLowerCase())) {
        expect(span).toHaveClass('test');
      }
    });
  });
});
