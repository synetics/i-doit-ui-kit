import React, { useMemo } from 'react';
import classnames from 'classnames';
import classes from './FuzzyHighlight.module.scss';

export type FuzzyHighlightType = {
  text: string;
  searchValue: string;
  highlightClassName?: string;
  notHighlightClassName?: string;
};

export const FuzzyHighlight = ({
  text,
  searchValue,
  highlightClassName,
  notHighlightClassName,
}: FuzzyHighlightType) => {
  const highlightedLettersSet = useMemo(() => new Set(searchValue.toLowerCase()), [searchValue]);

  return (
    <>
      {text.split('').map((char, index) => {
        const isHighlighted = highlightedLettersSet.has(char.toLowerCase());
        return (
          <span
            className={
              isHighlighted ? classnames(classes.highlight, highlightClassName) : classnames(notHighlightClassName)
            }
            /* eslint-disable-next-line react/no-array-index-key */
            key={`${char}-${index}`}
          >
            {char}
          </span>
        );
      })}
    </>
  );
};
