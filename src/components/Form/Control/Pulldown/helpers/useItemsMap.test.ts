import { renderHook } from '@testing-library/react-hooks';
import { ItemType } from '../types';
import { useItemsMap } from './useItemsMap';

const createItem = (key: string): ItemType => ({ value: key, label: key });

describe('useItemsMap', () => {
  const expecetdMapB = new Map<string, ItemType>([
    ['1', createItem('1')],
    ['a', createItem('a')],
    ['b', createItem('b')],
    ['2', createItem('2')],
  ]);

  it.each([
    [[], new Map<string, ItemType>()],
    [[createItem('1')], new Map<string, ItemType>([['1', createItem('1')]])],
    [[createItem('1'), createItem('a'), createItem('b'), createItem('2')], expecetdMapB],
  ])('converts the items to the correct shape', (items: ItemType[], expected: Map<string, ItemType>) => {
    const { result } = renderHook(() => useItemsMap(items));
    expect(result.current).toStrictEqual(expected);
  });
});
