import { ItemType } from '../types';

export const sortSelected = <T extends ItemType>({ items, value }: { items: T[]; value: T[] }): T[] => {
  const selectedItems: T[] = [];
  const notSelectedItems = items.filter((item) => {
    const selectedValue = value.find((it) => it.value === item.value);
    if (selectedValue) {
      selectedItems.push(selectedValue);
      return false;
    }
    return value.find((it) => it.value !== item.value);
  });
  return [...selectedItems, ...notSelectedItems];
};
