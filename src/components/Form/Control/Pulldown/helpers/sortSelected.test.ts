import { TagItemProps } from '../../../../Tag';
import { sortSelected } from './sortSelected';

describe('sortSelected function', () => {
  it('should return sorted array with selected items first', () => {
    const items: TagItemProps[] = [
      { label: 'Label1', value: 'value1' },
      { label: 'Label2', value: 'value2' },
      { label: 'Label3', value: 'value3' },
    ];

    const value: TagItemProps[] = [
      { label: 'Label2', value: 'value2' },
      { label: 'Label3', value: 'value3' },
    ];

    const sortedArray = sortSelected({ items, value });

    expect(sortedArray[0].label).toBe('Label2');
    expect(sortedArray[1].label).toBe('Label3');
  });
});
