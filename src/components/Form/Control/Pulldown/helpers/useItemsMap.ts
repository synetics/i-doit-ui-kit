import { useMemo } from 'react';
import { ItemType } from '../types';

const useItemsMap = <T extends ItemType = ItemType>(items: T[]): Map<string, T> =>
  useMemo(() => {
    const map = new Map<string, T>();

    items.forEach((item) => map.set(item.value, item));

    return map;
  }, [items]);

export { useItemsMap };
