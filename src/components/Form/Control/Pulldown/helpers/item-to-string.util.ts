export const getItemLabelFromMap =
  <T extends { label: string }>(itemsMap: Map<string, T>) =>
  (key: string | null): string => {
    if (key === null) {
      return '';
    }
    const item = itemsMap.get(key);

    if (!item) {
      return '';
    }

    return item.label;
  };
