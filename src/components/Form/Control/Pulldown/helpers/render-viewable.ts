import { ItemType } from '../types';

export const renderViewable = (
  { view, label }: ItemType,
  highlighted?: boolean,
  searchValue?: string,
  highlightedSearchResult?: boolean,
) => {
  if (typeof view === 'function') {
    return view(highlighted || false, searchValue, highlightedSearchResult);
  }
  if (view) {
    return view;
  }
  return label;
};
