import Fuse from 'fuse.js';
import { ItemType } from './types';

export const filterOptions = <T extends ItemType>(options: T[], filters = { searchTerm: '' }, threshold = 1): T[] => {
  const fuseOptions = {
    minMatchCharLength: threshold,
    keys: ['label'],
  };
  if (!filters.searchTerm || filters.searchTerm.length < threshold) {
    return options;
  }
  const fuse = new Fuse(options, fuseOptions);
  return fuse.search(filters.searchTerm).map((el) => el.item);
};
