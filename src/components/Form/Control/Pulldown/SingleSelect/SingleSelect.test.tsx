import { render, screen } from '@testing-library/react';
import user from '@testing-library/user-event';
import { SingleSelect } from './SingleSelect';

describe('SingleSelect', () => {
  const items = [
    { value: '1', label: 'One' },
    { value: '2', label: 'Two' },
  ];

  it('renders without crashing', () => {
    render(<SingleSelect onChange={() => {}} value={null} items={[]} />);

    const trigger = screen.getByTestId('pulldown-trigger');
    expect(trigger).toBeInTheDocument();
    const menu = screen.queryByRole('menu');
    expect(menu).not.toBeInTheDocument();
  });

  it('renders with placeholder', () => {
    render(<SingleSelect onChange={() => {}} value={null} items={[]} placeholder="Placeholder" />);

    expect(screen.getByText('Placeholder')).toBeInTheDocument();
  });

  it('renders with the value', () => {
    render(
      <SingleSelect
        onChange={() => {}}
        items={[]}
        value={{ value: 'value', label: 'Label' }}
        placeholder="Placeholder"
      />,
    );

    expect(screen.getByText('Label')).toBeInTheDocument();
  });

  it('renders with the custom value', () => {
    const view = <div data-testid="view">View</div>;
    render(
      <SingleSelect
        onChange={() => {}}
        items={[]}
        value={{ value: 'value', label: 'Label', view }}
        placeholder="Placeholder"
      />,
    );

    expect(screen.getByTestId('view')).toBeInTheDocument();
    expect(screen.getByTestId('view')).toHaveTextContent('View');
  });

  it('opens the menu with items', async () => {
    const handleChange = jest.fn();
    render(<SingleSelect items={items} value={null} onChange={handleChange} />);

    expect(screen.queryByText('One')).not.toBeVisible();
    expect(screen.queryByText('Two')).not.toBeVisible();

    await user.click(screen.getByTestId('pulldown-trigger'));

    expect(screen.queryByText('One')).toBeInTheDocument();
    expect(screen.queryByText('Two')).toBeInTheDocument();

    await user.click(screen.getByText('One'));

    expect(screen.queryByText('One')).not.toBeVisible();
    expect(screen.queryByText('Two')).not.toBeVisible();
    expect(handleChange).toHaveBeenCalledTimes(1);
    expect(handleChange).toHaveBeenCalledWith(items[0]);
  });

  it('reacts with keyboard', async () => {
    const handleChange = jest.fn();
    render(<SingleSelect items={items} value={null} onChange={handleChange} />);
    const trigger = screen.getByTestId('pulldown-trigger');

    await user.type(trigger, '{arrowdown}');
    expect(screen.getByText('One')).toBeInTheDocument();
    await user.type(trigger, 'Two');
    await user.type(trigger, '{enter}');
    expect(screen.queryByText('One')).not.toBeVisible();

    expect(handleChange).toHaveBeenCalledWith(items[1]);
  });

  it('does not render reset button', () => {
    render(<SingleSelect onChange={() => {}} items={items} value={items[0]} />);

    expect(screen.queryByTestId('reset-button')).not.toBeInTheDocument();
  });

  it('renders reset button if it is allowed', async () => {
    const handleChange = jest.fn();
    render(<SingleSelect items={items} onChange={handleChange} value={items[0]} canReset />);

    const button = screen.queryByTestId('reset-button');
    expect(button).toBeInTheDocument();

    if (!button) {
      return;
    }

    await user.click(button);
    expect(handleChange).toHaveBeenCalledWith(null);
  });

  it('renders reset button with custom label', () => {
    const handleChange = jest.fn();
    render(
      <SingleSelect items={items} onChange={handleChange} value={items[0]} canReset resetButtonLabel="Test label" />,
    );

    const button = screen.queryAllByTestId('idoit-tooltip');
    expect(button[1]).toBeInTheDocument();
    expect(button[1]).toHaveTextContent('Test label');
  });
});
