import React, { HTMLProps, ReactNode, useCallback, useMemo, useRef } from 'react';
import { VirtualElement } from '@popperjs/core';
import { useSelect, UseSelectStateChange } from 'downshift';
import classnames from 'classnames';
import { arrow_down, arrow_up } from '../../../../../icons';
import { ScrollListOptions } from '../../../../ScrollList';
import { OverflowWithTooltip } from '../../../../OverflowWithTooltip';
import { Popper, SameWidthModifier, ScrollParentModifier } from '../../../../Popper';
import { Icon } from '../../../../Icon';
import { FieldProps, FormField, useActive, useFieldGroup, ValueProps } from '../../../Infrastructure';
import { Items } from '../Items/Items';
import { ItemType } from '../types';
import { useItemsMap } from '../helpers/useItemsMap';
import { ResetButton } from '../ResetButton';
import { getItemLabelFromMap } from '../helpers/item-to-string.util';
import { renderViewable } from '../helpers/render-viewable';
import classes from './SingleSelect.module.scss';

const popperOptions = {
  modifiers: [SameWidthModifier, ScrollParentModifier],
};

type SingleSelectProps = Required<ValueProps<ItemType | null>> &
  FieldProps & {
    /**
     * Available items for selection
     */
    items: ItemType[];
    /**
     * Placeholder to be shown when nothing is selected
     */
    placeholder?: string;
    /**
     * Is the value optional and can be resetted
     */
    canReset?: boolean;
    /**
     * Reference element for pulldown
     */
    pulldownContainer?: Element | VirtualElement | null;
    /**
     * Label of the reset button
     */
    resetButtonLabel?: string;
    /**
     * selected item show as label
     */
    isSelectedAsLabel?: boolean;
    /**
     * Options for ScrollList component
     */
    options?: ScrollListOptions;
    children?: ReactNode;
    /**
     * Id for input
     */
    id?: string;
  };

const SingleSelect = ({
  active: passedActive,
  placeholder,
  disabled,
  readonly,
  canReset = false,
  items,
  children,
  value,
  onChange,
  resetButtonLabel,
  setActive: passedSetActive,
  pulldownContainer,
  isSelectedAsLabel = false,
  options = {},
  id,
  ...rest
}: SingleSelectProps) => {
  const ref = useRef(null);
  const trigger = useRef<HTMLButtonElement>(null);
  const focus = useCallback(() => {
    if (trigger.current) {
      trigger.current.focus();
    }
  }, []);
  const { value: active, setValue: setActive } = useActive(passedActive, passedSetActive);
  const { readonly: derivedReadonly, disabled: derivedDisabled } = useFieldGroup({ disabled, readonly });
  const itemsMap = useItemsMap(items);
  const itemToString = useMemo(() => getItemLabelFromMap(itemsMap), [itemsMap]);
  const onIsOpenChange = useCallback(
    ({ isOpen: open, type }: UseSelectStateChange<string>) => {
      setActive(open || false);
      if (
        type === useSelect.stateChangeTypes.MenuKeyDownEscape ||
        type === useSelect.stateChangeTypes.MenuKeyDownEnter
      ) {
        focus();
      }
    },
    [focus, setActive],
  );
  const values = useMemo(() => Array.from(itemsMap.keys()), [itemsMap]);
  const onSelectedItemChange = useCallback(
    ({ selectedItem: newValue }: UseSelectStateChange<string>) => {
      onChange((typeof newValue === 'string' && itemsMap.get(newValue)) || null);
      focus();
    },
    [focus, itemsMap, onChange],
  );

  const {
    selectedItem: selected,
    isOpen,
    highlightedIndex,
    getToggleButtonProps,
    getMenuProps,
    getItemProps,
    reset,
  } = useSelect<string>({
    items: values,
    itemToString,
    selectedItem: value?.value || null,
    onIsOpenChange,
    onSelectedItemChange,
  });
  const handleReset = useCallback(() => {
    reset();
    setTimeout(focus, 0);
  }, [reset, focus]);
  const displayedValue = isSelectedAsLabel ? value?.label : value && renderViewable(value);

  const { onKeyDown, ...menuProps } = getMenuProps() as HTMLProps<HTMLDivElement>;
  const handleKeyDownMenu = useCallback(
    (e: React.KeyboardEvent<HTMLDivElement>) => {
      e.stopPropagation();
      if (onKeyDown) {
        onKeyDown(e);
      }
    },
    [onKeyDown],
  );

  return (
    <>
      <FormField {...rest} ref={ref} active={active} readonly={derivedReadonly} disabled={derivedDisabled}>
        <button
          data-testid="pulldown-trigger"
          type="button"
          {...getToggleButtonProps({ disabled: derivedReadonly, ref: trigger, id })}
          className={classnames(classes.trigger, {
            [classes.open]: isOpen,
          })}
          disabled={derivedDisabled}
        >
          <div className={classes.content}>
            {value ? (
              <OverflowWithTooltip>{displayedValue}</OverflowWithTooltip>
            ) : (
              <span className={classes.placeholder}>{placeholder}</span>
            )}
          </div>
          {canReset && value && <ResetButton onClick={handleReset} label={resetButtonLabel} />}
          {!derivedReadonly && <Icon src={isOpen ? arrow_up : arrow_down} className={classes.triggerIcon} />}
        </button>
      </FormField>
      <Popper open={isOpen} options={popperOptions} referenceElement={pulldownContainer || ref.current}>
        <div {...menuProps} onKeyDown={handleKeyDownMenu} role="menu" tabIndex={-1}>
          <Items
            className={classes.menu}
            itemsMap={itemsMap}
            selected={selected}
            highlightedIndex={highlightedIndex}
            getItemProps={getItemProps}
            options={options}
          >
            {children}
          </Items>
        </div>
      </Popper>
    </>
  );
};

export { SingleSelect, SingleSelectProps };
