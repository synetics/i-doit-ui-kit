import { ReactElement } from 'react';
import { GroupItemType } from '../types';
import { Item } from '../Items';
import { renderViewable } from '../helpers/render-viewable';
import { Group } from './Group';

const groupItems = (
  items: GroupItemType[],
  searchValue?: string,
  highlightedSearchResult?: boolean,
  highlightedIndex?: number,
): ReactElement[] => {
  let last: string | undefined;
  const result = [];
  for (let i = 0; i < items.length; i += 1) {
    const { group, value } = items[i];
    if (last !== group) {
      result.push(<Group name={group} key={`group-${group || 'hr'}`} />);
      last = group;
    }

    result.push(
      <Item value={value} key={`item-${value}`} shouldScrollIntoView>
        {renderViewable(items[i], highlightedIndex === i, searchValue, highlightedSearchResult)}
      </Item>,
    );
  }
  return result;
};

export { groupItems };
