import { render, screen } from '@testing-library/react';
import { GroupItemType } from '../types';
import { GroupedItems } from './GroupedItems';

const createItem = (group: string, key: string): GroupItemType => ({
  group,
  value: key,
  label: key,
});

describe('GroupedItems', () => {
  it('renders empty without crashing', () => {
    render(<GroupedItems items={[]} />);
  });

  it('renders with items', () => {
    render(<GroupedItems items={[createItem('one', '1'), createItem('one', '2')]} />);

    expect(screen.getByText('one')).toBeInTheDocument();
    expect(screen.getByText('1')).toBeInTheDocument();
    expect(screen.getByText('2')).toBeInTheDocument();
  });
  it('renders with multiple groups', () => {
    render(
      <GroupedItems
        items={[createItem('one', '1'), createItem('one', '2'), createItem('two', 'a'), createItem('two', 'b')]}
      />,
    );

    expect(screen.getByText('one')).toBeInTheDocument();
    expect(screen.getByText('1')).toBeInTheDocument();
    expect(screen.getByText('2')).toBeInTheDocument();
    expect(screen.getByText('two')).toBeInTheDocument();
    expect(screen.getByText('a')).toBeInTheDocument();
    expect(screen.getByText('b')).toBeInTheDocument();
  });
  it('renders without group', () => {
    render(<GroupedItems items={[createItem('one', '1'), createItem('', '2')]} />);

    expect(screen.getByText('one')).toBeInTheDocument();
    expect(screen.getByText('1')).toBeInTheDocument();
    expect(screen.getByTestId('delimiter')).toBeInTheDocument();
    expect(screen.getByText('2')).toBeInTheDocument();
  });
});
