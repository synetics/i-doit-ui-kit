import classes from './GroupItem.module.scss';

type GroupType = {
  name?: string;
};

const Group = ({ name }: GroupType) => {
  if (!name) {
    return <hr data-testid="delimiter" className={classes.line} />;
  }

  return <div className={classes.group}>{name}</div>;
};

export { Group, GroupType };
