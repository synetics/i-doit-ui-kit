import { useMemo } from 'react';
import { GroupItemType } from '../types';
import styles from '../MultiSelectComboBox/MultiSelectComboBox.module.scss';
import { groupItems } from './groupItems';

type GroupedItemsType = {
  items: GroupItemType[];
  userInput?: string;
  highlightedSearchResult?: boolean;
  highlightedIndex?: number;
};

const GroupedItems = ({ items, userInput, highlightedSearchResult, highlightedIndex }: GroupedItemsType) => {
  const result = useMemo(
    () => groupItems(items, userInput, highlightedSearchResult, highlightedIndex),
    [items, userInput, highlightedSearchResult, highlightedIndex],
  );
  return <div className={styles.menu}>{result}</div>;
};

export { GroupedItems, GroupedItemsType };
