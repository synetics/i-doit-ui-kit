import { useCallback, useMemo } from 'react';
import { UseSelectGetItemPropsOptions } from 'downshift';
import { GroupItemType, ItemType } from '../types';
import { groupItems } from './groupItems';

type GroupedItemsType = {
  items: GroupItemType[];
  selectedItems: string[];
  onChange: (id: string) => void;
  highlightedIndex: number | null;
  getItemProps: (options: UseSelectGetItemPropsOptions<string>) => any;
  itemsMap: Map<string, ItemType>;
  userInput?: string;
  highlightedSearchResult?: boolean;
};
const GroupedItemsMultiSelect = ({
  items,
  itemsMap,
  selectedItems,
  highlightedIndex,
  getItemProps,
  onChange,
  userInput,
  highlightedSearchResult,
}: GroupedItemsType) => {
  const isSelected = useCallback((item: string) => selectedItems.includes(item), [selectedItems]);
  const isHighlighted = useCallback((index: number) => index === highlightedIndex, [highlightedIndex]);
  const context = useMemo(
    () => ({ isSelected, isHighlighted, getItemProps, itemsMap, highlightedIndex }),
    [getItemProps, itemsMap, isHighlighted, isSelected, highlightedIndex],
  );
  return useMemo(
    () => groupItems(items, selectedItems, itemsMap, onChange, context, userInput, highlightedSearchResult),
    [items, itemsMap, selectedItems, onChange, context, userInput, highlightedSearchResult],
  );
};

export { GroupedItemsMultiSelect, GroupedItemsType };
