import { render, screen } from '@testing-library/react';
import { Group } from './Group';

describe('GroupedItems', () => {
  it('renders empty without crashing', () => {
    render(<Group />);
    expect(screen.getByTestId('delimiter')).toBeInTheDocument();
  });
});
