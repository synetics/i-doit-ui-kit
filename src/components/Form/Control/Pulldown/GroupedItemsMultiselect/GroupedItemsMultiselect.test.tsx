import { render } from '@testing-library/react';
import { GroupItemType } from '../types';
import { GroupedItemsMultiSelect } from './GroupedItemsMultiSelect';

const createItem = (group: string, key: string): GroupItemType => ({
  group,
  value: key,
  label: key,
});

describe('GroupedItems', () => {
  it('renders empty without crashing', () => {
    render(
      <GroupedItemsMultiSelect
        selectedItems={['1', '2']}
        items={[createItem('one', '1'), createItem('one', '2')]}
        onChange={() => {}}
        itemsMap={new Map()}
        getItemProps={() => {}}
        highlightedIndex={1}
      />,
    );
  });
});
