import { UseSelectGetItemPropsOptions } from 'downshift';
import React from 'react';
import { SelectorItem } from '../../../../List/Item';
import { ScrollList } from '../../../../ScrollList';
import { GroupItemType, ItemType } from '../types';
import { ItemsContext } from '../Items';
import { renderViewable } from '../helpers/render-viewable';
import { Group } from './Group';
import classes from './GroupItemMultiselect.module.scss';

const groupItems = (
  items: GroupItemType[],
  selectedItems: string[],
  itemsMap: Map<string, ItemType>,
  onChange: { (id: string): void; (arg0: string): void },
  context: {
    isSelected: ((item: string) => boolean) | ((item: string) => boolean);
    isHighlighted: ((index: number) => boolean) | ((index: number) => boolean);
    getItemProps:
      | ((options: UseSelectGetItemPropsOptions<string>) => any)
      | ((options: UseSelectGetItemPropsOptions<string>) => Record<string, any>);
    itemsMap: Map<string, ItemType>;
  },
  userInput?: string,
  highlightedSearchResult?: boolean,
): JSX.Element => {
  let last: string | undefined;

  const allRenderedItems = Array.from(itemsMap, ([id, item], index) => {
    const { group, value } = items[index];
    const renderedItem = (
      <SelectorItem
        key={value}
        selected={selectedItems.includes(id)}
        onSelect={() => onChange(id)}
        aria-label={item.label}
        value={value}
      >
        {renderViewable(item, context.isHighlighted(index), userInput, highlightedSearchResult)}
      </SelectorItem>
    );
    if (last !== group) {
      last = group;
      return [<Group name={group} key={`group-${group || 'hr'}`} />, renderedItem];
    }
    return [renderedItem];
  }).flatMap((a) => a);

  return (
    <ItemsContext.Provider value={context}>
      <ScrollList size={allRenderedItems.length} itemSize={40} className={classes.scrollList}>
        {(i) => allRenderedItems[i]}
      </ScrollList>
    </ItemsContext.Provider>
  );
};

export { groupItems };
