import React, {
  BaseSyntheticEvent,
  ForwardedRef,
  forwardRef,
  HTMLProps,
  ReactNode,
  useCallback,
  useMemo,
  useState,
} from 'react';
import {
  useMultipleSelection,
  UseMultipleSelectionStateChange,
  useSelect,
  UseSelectState,
  UseSelectStateChange,
  UseSelectStateChangeOptions,
} from 'downshift';
import classnames from 'classnames';
import { VirtualElement } from '@popperjs/core';
import { arrow_down, arrow_up } from '../../../../../icons';
import { Tag, Tags } from '../../../../Tag';
import { Popper, SameWidthModifierAndSizeChange, ScrollParentModifier } from '../../../../Popper';
import { Icon } from '../../../../Icon';
import {
  useCallbackRef,
  useControlledState,
  useEventListener,
  useFocusWithin,
  useGlobalizedRef,
  useTagItemsFocus,
} from '../../../../../hooks';
import { FieldProps, FormField, useActive, useFieldGroup, ValueProps } from '../../../Infrastructure';
import { ScrollListOptions } from '../../../../ScrollList';
import { useItemsMap } from '../helpers/useItemsMap';
import { getItemLabelFromMap } from '../helpers/item-to-string.util';
import { ResetButton } from '../ResetButton';
import { ItemType } from '../types';
import { sortSelected } from '../helpers/sortSelected';
import { ChildrenProps } from '../MultiSelectComboBox';
import classes from './MultiSelect.module.scss';
import { Items } from './items/Items';
import { customReducer } from './customReducer';
import { customStateChangeHandler } from './customStateChangeHandler';

const popperOptions = {
  modifiers: [SameWidthModifierAndSizeChange, ScrollParentModifier],
};

type MultiSelectValueType = ItemType[];

type MultiSelectProps = Required<ValueProps<MultiSelectValueType>> &
  FieldProps & {
    /**
     * Available items for selection
     */
    items: ItemType[];
    /**
     * Placeholder to be shown when nothing is selected
     */
    placeholder?: string;
    /**
     * Reference element for pulldown
     */
    pulldownContainer?: Element | VirtualElement | null;
    /**
     * If user can reset the value
     */
    canReset?: boolean;
    /**
     * Options for ScrollList component
     */
    options?: ScrollListOptions;
    /**
     * Option to collapse tags
     */
    collapsedTags?: boolean;
    children?: ChildrenProps | ReactNode;
    /**
     * Id for input
     */
    id?: string;
  };

const MultiSelect = forwardRef(
  (
    {
      active: passedActive,
      canReset,
      placeholder,
      pulldownContainer,
      disabled,
      readonly,
      items,
      children,
      value,
      onChange,
      setActive: passedSetActive,
      options = {},
      collapsedTags = true,
      id: inputId,
      ...rest
    }: MultiSelectProps,
    forwardedRef: ForwardedRef<HTMLButtonElement>,
  ) => {
    const { localRef: ref, setRefsCallback: setRef } = useGlobalizedRef(forwardedRef);
    const [tagsRef, setTagsRef] = useCallbackRef<HTMLElement>();
    const { value: active, setValue: setActive } = useActive(passedActive, passedSetActive);
    const { readonly: derivedReadonly, disabled: derivedDisabled } = useFieldGroup({ disabled, readonly });
    const [itemState, setItemState] = useControlledState<ItemType[]>(items);
    const itemsMap = useItemsMap(itemState);
    const itemToString = useMemo(() => getItemLabelFromMap(itemsMap), [itemsMap]);

    const focus = useCallback(() => ref.current?.focus(), [ref]);
    const onIsOpenChange = useCallback(
      ({ isOpen: open }: UseSelectStateChange<string>) => setActive(open || false),
      [setActive],
    );
    const values = useMemo(() => Array.from(itemsMap.keys()), [itemsMap]);
    const selectedValues = useMemo(() => value?.map((item) => item.value), [value]);

    // this state we need to reset autofocus, after menu closing
    const onSelectedItemsChange = useCallback(
      ({ selectedItems: newValue }: UseMultipleSelectionStateChange<string>) => {
        if (Array.isArray(newValue)) {
          onChange(newValue.map((item) => itemsMap.get(item) as ItemType));
        }
      },
      [itemsMap, onChange],
    );

    const { getDropdownProps, addSelectedItem, removeSelectedItem, selectedItems } = useMultipleSelection<string>({
      selectedItems: selectedValues || [],
      onSelectedItemsChange,
      stateReducer: (state, action) => ({
        ...action.changes,
        selectedItems: Array.from(new Set(action.changes.selectedItems)),
      }),
    });
    const handlerChange = useCallback(
      (id: string | null) => {
        if (!id) {
          return;
        }
        if (!selectedItems.includes(id)) {
          addSelectedItem(id);
        } else removeSelectedItem(id);
      },
      [selectedItems, addSelectedItem, removeSelectedItem],
    );
    const stateReducer = useCallback(
      (state: UseSelectState<string>, changes: UseSelectStateChangeOptions<string>) =>
        customReducer(values)(state, changes),
      [values],
    );
    const onStateChange = useCallback(
      (changes: UseSelectStateChange<string>) => customStateChangeHandler(handlerChange, focus)(changes),
      [focus, handlerChange],
    );

    const { isOpen, highlightedIndex, getToggleButtonProps, getMenuProps, getItemProps, openMenu } = useSelect<string>({
      items: values,
      itemToString,
      selectedItem: null,
      onIsOpenChange,
      defaultHighlightedIndex: selectedValues?.length > 0 ? values.indexOf(selectedValues[0]) : 0,
      stateReducer,
      onStateChange,
    });
    const notEditable = derivedReadonly || derivedDisabled;

    const tagItems = useMemo<ItemType[]>(
      () =>
        selectedItems.map((selectedItem) => ({
          value: selectedItem,
          label: itemToString(selectedItem),
          icon: itemsMap.get(selectedItem)?.icon || items.find((el) => el.value === selectedItem)?.icon,
        })),
      [selectedItems, itemToString, itemsMap, items],
    );
    const handleRemoveSelectedItem = (id: number) => {
      removeSelectedItem(selectedItems[id]);
      if (selectedItems.length <= 1) {
        setTimeout(focus, 0);
      }
    };

    const handleRemoveEvent = (id: number) => (event?: BaseSyntheticEvent) => {
      handleRemoveSelectedItem(id);

      if (event && event.type === 'mousedown') {
        setTimeout(focus, 0);
      }
    };

    const [index, setIndex] = useState(0);
    useEventListener('focus', () => setIndex(0), ref.current);
    const focusIndex = useTagItemsFocus(tagsRef, tagItems.length, handleRemoveSelectedItem, index, setIndex);
    const focusWithin = useFocusWithin(tagsRef);
    const focusWithinBar = useFocusWithin(ref.current);
    const handleReset = useCallback(() => {
      onChange([]);
      setTimeout(focus, 0);
    }, [focus, onChange]);

    const { onKeyDown, ...menuProps } = getMenuProps({
      onMouseUpCapture: (e) => {
        e.preventDefault();
        e.stopPropagation();
      },
    }) as HTMLProps<HTMLDivElement>;

    const handleKeyDownMenu = useCallback(
      (e: React.KeyboardEvent<HTMLDivElement>) => {
        e.stopPropagation();
        if (onKeyDown) {
          onKeyDown(e);
        }
      },
      [onKeyDown],
    );
    const itemsQuantity = tagItems.length - 1;

    const handleOpenMore = useCallback(() => {
      openMenu();
      if (value && value.length > 0) {
        setItemState(sortSelected({ items, value }));
      }
    }, [openMenu, items, value, setItemState]);

    const passedChildren =
      typeof children === 'function'
        ? children({
            items: itemState,
            itemsMap,
            selectedItems,
            highlightedIndex,
            getItemProps,
            onChange: handlerChange,
          })
        : children;

    return (
      <>
        <FormField
          {...rest}
          {...getToggleButtonProps(
            getDropdownProps({
              id: inputId,
              ref: setRef,
              disabled: notEditable,
              onKeyDownCapture: (e: React.KeyboardEvent) => {
                if (e.key === 'Escape' || e.key === 'Tab') return;
                e.preventDefault();
              },
            }),
          )}
          type="button"
          component="button"
          className={classes.trigger}
          data-testid="multipulldown-trigger"
          active={active}
          setActive={() => null}
          disabled={derivedDisabled}
          readonly={derivedReadonly}
        >
          <div className={classes.content}>
            {tagItems.length === 0 ? (
              <span className={classes.placeholder}>{placeholder}</span>
            ) : (
              <Tags
                ref={setTagsRef}
                focusIndex={focusWithinBar ? focusIndex : -1}
                autoFocus={focusWithin}
                enabled={!notEditable}
                className={classes.tags}
              >
                {tagItems.length > 2 && collapsedTags ? (
                  <>
                    <Tag onClick={handleOpenMore} className={classes.tag}>
                      {itemsQuantity} more
                    </Tag>
                    <Tag
                      prefix={tagItems[itemsQuantity]?.icon}
                      onClose={!notEditable ? handleRemoveEvent(itemsQuantity) : undefined}
                      className={classes.tag}
                    >
                      {tagItems[itemsQuantity].label}
                    </Tag>
                  </>
                ) : (
                  tagItems.map(({ value: val, label, icon }, i) => (
                    <Tag
                      key={val}
                      prefix={icon || null}
                      onClose={!notEditable ? handleRemoveEvent(i) : undefined}
                      className={classes.collapsedTags}
                    >
                      {label}
                    </Tag>
                  ))
                )}
              </Tags>
            )}
          </div>
          {canReset && !derivedReadonly && !derivedDisabled && tagItems.length > 0 && (
            <ResetButton tabIndex={0} onClick={handleReset} />
          )}
          {!derivedReadonly && (
            <Icon
              src={isOpen ? arrow_up : arrow_down}
              className={classnames(classes.triggerIcon, { [classes.open]: isOpen })}
            />
          )}
        </FormField>
        <Popper
          dataTestId="multiselect-popper"
          open={isOpen}
          options={popperOptions}
          referenceElement={pulldownContainer || ref.current}
        >
          <div {...menuProps} onKeyDown={handleKeyDownMenu} role="menu" tabIndex={-1}>
            <Items
              className={classes.menu}
              itemsMap={itemsMap}
              selectedItems={selectedItems}
              highlightedIndex={highlightedIndex}
              getItemProps={getItemProps}
              onChange={handlerChange}
              options={options}
            >
              {passedChildren}
            </Items>
          </div>
        </Popper>
      </>
    );
  },
);

export { MultiSelect, MultiSelectProps };
