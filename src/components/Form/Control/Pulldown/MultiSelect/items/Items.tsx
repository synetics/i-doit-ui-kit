import React, { ReactNode, useCallback, useMemo } from 'react';
import { UseSelectGetItemPropsOptions } from 'downshift';
import { SelectorItem } from '../../../../../List/Item';
import { OverflowWithTooltip } from '../../../../../OverflowWithTooltip';
import { ScrollListOptions } from '../../../../../ScrollList';
import { FuzzyHighlight } from '../../../../../FuzzyHighlight';
import { ItemType } from '../../types';
import { ItemsContext } from '../../Items';
import { DefaultItemsContent } from '../../Items/DefaultItemsContent';
import classes from './Item.module.scss';

type ItemsProps = {
  className?: string;
  selectedItems: string[];
  onChange: (id: string) => void;
  highlightedIndex: number | null;
  getItemProps: (options: UseSelectGetItemPropsOptions<string>) => any;
  itemsMap: Map<string, ItemType>;
  options?: ScrollListOptions;
  highlightSearchResult?: boolean;
  searchValue?: string;
  children?: ReactNode;
};

const Items = ({
  className,
  itemsMap,
  selectedItems,
  highlightedIndex,
  getItemProps,
  children,
  onChange,
  options,
  highlightSearchResult = false,
  searchValue = '',
}: ItemsProps) => {
  const isSelected = useCallback((item: string) => selectedItems.includes(item), [selectedItems]);
  const isHighlighted = useCallback((index: number) => index === highlightedIndex, [highlightedIndex]);
  const context = useMemo(
    () => ({ isSelected, isHighlighted, getItemProps, itemsMap }),
    [getItemProps, itemsMap, isHighlighted, isSelected],
  );
  const inner = children || (
    <DefaultItemsContent
      className={className}
      highlightedIndex={highlightedIndex}
      itemsMap={itemsMap}
      options={options}
      renderer={(id, item, index) => (
        <SelectorItem
          key={id}
          selected={selectedItems.includes(id)}
          onSelect={() => onChange(id)}
          aria-label={item.label}
          value={id}
        >
          {(typeof item.view === 'function'
            ? item.view(index === highlightedIndex, searchValue, highlightSearchResult)
            : item.view) || (
            <OverflowWithTooltip
              showTooltip={index === highlightedIndex}
              popperClassName={classes.tooltip}
              tooltipContent={item.label}
            >
              <span className={classes.twoLines}>
                {highlightSearchResult ? <FuzzyHighlight text={item.label} searchValue={searchValue} /> : item.label}
              </span>
            </OverflowWithTooltip>
          )}
        </SelectorItem>
      )}
    />
  );

  return <ItemsContext.Provider value={context}>{inner}</ItemsContext.Provider>;
};

export { Items, ItemsProps };
