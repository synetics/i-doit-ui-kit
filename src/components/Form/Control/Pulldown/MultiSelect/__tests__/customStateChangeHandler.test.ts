import { useSelect, UseSelectStateChangeTypes } from 'downshift';
import { customStateChangeHandler } from '../customStateChangeHandler';

describe('Custom MultiSelect handler', () => {
  it.each([
    useSelect.stateChangeTypes.MenuKeyDownEnter,
    useSelect.stateChangeTypes.ItemClick,
    useSelect.stateChangeTypes.MenuKeyDownSpaceButton,
  ])('Calls selection', (type: UseSelectStateChangeTypes) => {
    const onSelect = jest.fn();
    const onClose = jest.fn();
    customStateChangeHandler(onSelect, onClose)({ type, selectedItem: 'item' });

    expect(onSelect).toHaveBeenCalledWith('item');
  });
  it('Calls closing', () => {
    const onSelect = jest.fn();
    const onClose = jest.fn();
    customStateChangeHandler(onSelect, onClose)({ type: useSelect.stateChangeTypes.MenuKeyDownEscape });

    expect(onClose).toHaveBeenCalledWith();
  });
});
