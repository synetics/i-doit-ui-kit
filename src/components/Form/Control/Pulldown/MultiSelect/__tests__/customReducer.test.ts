import { useSelect, UseSelectStateChangeTypes } from 'downshift';
import { customReducer } from '../customReducer';

describe('Custom MultiSelect reducer', () => {
  const items = ['a', 'b', 'c', 'd'];
  it.each([
    [useSelect.stateChangeTypes.MenuKeyDownEnter, 'a', 0],
    [useSelect.stateChangeTypes.MenuKeyDownEnter, 'c', 2],
    [useSelect.stateChangeTypes.MenuKeyDownSpaceButton, 'a', 0],
    [useSelect.stateChangeTypes.MenuKeyDownSpaceButton, 'c', 2],
    [useSelect.stateChangeTypes.ItemClick, 'a', 0],
    [useSelect.stateChangeTypes.ItemClick, 'c', 2],
  ])('reacts on change', (type: UseSelectStateChangeTypes, selectedItem: string, highlightedIndex: number) => {
    const result = customReducer(items)(
      {
        highlightedIndex: 0,
        isOpen: true,
        inputValue: 'test',
        selectedItem: null,
      },
      {
        type,
        changes: {
          highlightedIndex,
          selectedItem,
        },
      },
    );

    expect(result).toStrictEqual({
      isOpen: true,
      highlightedIndex,
      selectedItem,
    });
  });
});
