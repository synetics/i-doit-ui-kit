import * as icons from '../../../../../../icons';

const imported = icons as Record<string, string>;

export const getRandomIcon = () => {
  const iconNames = Object.keys(imported);
  const name = iconNames[Math.round(Math.random() * (iconNames.length - 1))];
  return imported[name];
};
