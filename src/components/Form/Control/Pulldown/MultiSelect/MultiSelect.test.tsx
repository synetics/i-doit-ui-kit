import { fireEvent, render, screen } from '@testing-library/react';
import user from '@testing-library/user-event';
import React, { FC, useState } from 'react';
import { act } from 'react-dom/test-utils';
import { ItemType } from '../types';
import { MultiSelect } from './MultiSelect';

const items = [
  { value: '1', label: 'One' },
  { value: '2', label: 'Two' },
];

const itemsWithoutId = [
  { value: '1', label: 'One' },
  { value: '', label: 'Two' },
];
const delay = (time: number): Promise<void> => new Promise<void>((resolve) => setTimeout(resolve, time));

const StatefulMultiSelect: FC<{ initialItems: ItemType[] }> = ({ initialItems }) => {
  const [value, setValue] = useState<ItemType[]>(initialItems);
  return <MultiSelect value={value} onChange={setValue} items={initialItems} />;
};

describe('MultiSelect', () => {
  it('renders without crashing', () => {
    render(<MultiSelect onChange={() => false} items={[]} value={[] as ItemType[]} />);

    const trigger = screen.getByTestId('multipulldown-trigger');
    expect(trigger).toBeInTheDocument();
    const menu = screen.queryByRole('menu');
    expect(menu).not.toBeInTheDocument();
    const reset = screen.queryByTestId('reset-button');
    expect(reset).not.toBeInTheDocument();
  });

  it('check collapsed tags', async () => {
    const moreItems = [...items, { value: '3', label: 'Three' }, { value: '4', label: 'Four', icon: 'icon' }];
    render(<MultiSelect onChange={() => {}} value={moreItems} items={moreItems} />);
    expect(screen.getByText('3 more')).toBeInTheDocument();
    expect(screen.getByText('Four')).toBeInTheDocument();
    expect(screen.getByText('icon')).toBeInTheDocument();
    const trigger = screen.getByText('3 more');
    await user.click(trigger);
    expect(screen.getAllByText('Two')[1]).toBeInTheDocument();
  });

  it('renders with placeholder', () => {
    render(<MultiSelect onChange={() => {}} value={[]} items={[]} placeholder="Placeholder" />);

    expect(screen.getByText('Placeholder')).toBeInTheDocument();
  });

  it('renders with the values', () => {
    render(<MultiSelect onChange={() => {}} items={items} value={items} placeholder="Placeholder" canReset />);

    expect(screen.getAllByText('One')[0]).toBeInTheDocument();

    const reset = screen.queryByTestId('reset-button');
    expect(reset).toBeInTheDocument();
  });

  it('reset button clears the selection', async () => {
    const onChange = jest.fn();
    render(<MultiSelect onChange={onChange} items={items} value={items} placeholder="Placeholder" canReset />);

    const trigger = screen.getByTestId('multipulldown-trigger');
    const reset = screen.getByTestId('reset-button');
    await user.click(reset);

    await delay(0);

    expect(onChange).toHaveBeenCalledWith([]);
    expect(trigger).toHaveFocus();
  });

  it('opens the menu with items', () => {
    const handleChange = jest.fn();
    render(<MultiSelect items={items} value={[]} onChange={handleChange} />);

    expect(screen.queryAllByText('One')[0]).not.toBeVisible();
    expect(screen.queryAllByText('Two')[0]).not.toBeVisible();
  });

  it('selects elems on click', async () => {
    const handleChange = jest.fn();
    render(<MultiSelect items={items} value={[]} onChange={handleChange} />);

    const trigger = screen.getByTestId('multipulldown-trigger');
    await user.click(trigger);

    const firstCheck = screen.getByRole('checkbox', { name: 'One' });
    const secondCheck = screen.getByRole('checkbox', { name: 'Two' });
    const firstOption = screen.getAllByRole('option')[0];

    expect(firstCheck).toBeInTheDocument();
    expect(firstCheck).not.toBeChecked();
    expect(secondCheck).toBeInTheDocument();
    expect(secondCheck).not.toBeChecked();

    await user.click(firstOption);

    expect(firstCheck).toBeInTheDocument();
    expect(secondCheck).toBeInTheDocument();
    expect(handleChange).toHaveBeenCalledWith([{ value: '1', label: 'One' }]);
  });

  it('selects elem with checkbox and stays opened', async () => {
    const handleChange = jest.fn();
    render(<MultiSelect items={items} value={[]} onChange={handleChange} />);

    const trigger = screen.getByTestId('multipulldown-trigger');
    await user.click(trigger);

    const firstCheck = screen.getAllByTestId('checkboxView')[0];

    await user.click(firstCheck);
    const menu = screen.getByRole('menu');

    expect(menu).toBeVisible();
  });

  it('selects elems on without id', async () => {
    const handleChange = jest.fn();
    render(<MultiSelect items={itemsWithoutId} value={[]} onChange={handleChange} />);

    const trigger = screen.getByTestId('multipulldown-trigger');
    await user.click(trigger);
    const secondCheck = screen.getByRole('checkbox', { name: 'Two' });

    await user.click(secondCheck);
    expect(handleChange).toHaveBeenCalled();
  });

  it('deletes element by clicking on option', async () => {
    const handleChange = jest.fn();
    render(<MultiSelect items={items} value={[items[0]]} onChange={handleChange} />);

    const trigger = screen.getByTestId('multipulldown-trigger');
    await user.click(trigger);

    const firstCheck = screen.getByRole('checkbox', { name: 'One' });
    const firstOption = screen.getAllByRole('option')[0];

    expect(firstCheck).toBeInTheDocument();
    expect(firstCheck).toBeChecked();

    await user.click(firstOption);

    expect(firstCheck).toBeInTheDocument();
    expect(handleChange).toHaveBeenCalledWith([]);
  });

  it('deletes elememt by clicking on tag', async () => {
    const handleChange = jest.fn();
    render(<MultiSelect items={items} value={items} onChange={handleChange} />);

    const remove = screen.getAllByTestId('tag-remove-button')[0];
    await user.click(remove);
    expect(handleChange).toHaveBeenCalledWith([items[1]]);
  });

  it('closes the menu on escape', async () => {
    render(<MultiSelect items={items} value={[]} onChange={() => {}} />);
    const trigger = screen.getByTestId('multipulldown-trigger');
    await user.click(trigger);
    const options = screen.queryAllByRole('option');
    expect(options).toHaveLength(2);
    await user.type(trigger, '{escape}');
    await delay(0);

    const newOptions = screen.queryAllByRole('option');
    expect(newOptions.length).toBe(0);
  });

  it('adds element', async () => {
    const handleChange = jest.fn();
    render(<MultiSelect items={items} value={[items[0]]} onChange={handleChange} />);

    await user.click(screen.getByTestId('multipulldown-trigger'));
    const firstCheck = screen.getByRole('checkbox', { name: 'One' });
    const secondCheck = screen.getByRole('checkbox', { name: 'Two' });

    expect(firstCheck).toBeInTheDocument();
    expect(firstCheck).toBeChecked();
    expect(secondCheck).toBeInTheDocument();
    expect(secondCheck).not.toBeChecked();
    await user.click(secondCheck);
    expect(handleChange).toHaveBeenCalledWith(items);
  });

  it('selects and unselect elements by click/keypress on close', async () => {
    const handleChange = jest.fn();
    render(<MultiSelect items={items} value={items} onChange={handleChange} />);

    await user.click(screen.getByTestId('multipulldown-trigger'));

    const firstCheck = screen.getByRole('checkbox', { name: 'One' });
    const secondCheck = screen.getByRole('checkbox', { name: 'Two' });

    expect(firstCheck).toBeInTheDocument();
    expect(secondCheck).toBeInTheDocument();

    expect(firstCheck).toBeChecked();
    const closeButton = screen.getAllByTestId('tag-remove-button')[0];
    expect(closeButton).toBeInTheDocument();
    await user.click(closeButton);
    expect(handleChange).toHaveBeenCalledWith([{ value: '2', label: 'Two' }]);
    expect(secondCheck).toBeInTheDocument();
    const closeButtonSecond = screen.getAllByTestId('tag-remove-button')[1];
    await user.click(closeButtonSecond);
    expect(handleChange).toHaveBeenCalledWith([{ value: '1', label: 'One' }]);
  });

  it('reacts with keyboard', async () => {
    const handleChange = jest.fn();
    render(<MultiSelect items={items} value={items} onChange={handleChange} />);
    const trigger = screen.getByTestId('multipulldown-trigger');

    await user.type(trigger, '{arrowdown}');

    const option = screen.getByRole('checkbox', { name: 'One' });

    expect(option).toBeInTheDocument();

    trigger.blur();

    await user.type(option, ' ');
    await user.type(option, '{enter}');
  });

  it('removes element without tooltip staying', async () => {
    render(<StatefulMultiSelect initialItems={items} />);
    const trigger = screen.getByTestId('multipulldown-trigger');

    trigger.focus();

    await user.tab();

    const tags = screen.getAllByTestId('tag-remove-button');

    expect(tags[0]).toHaveFocus();

    expect(tags).toHaveLength(2);
    await user.type(tags[0], '{enter}');

    await delay(300);

    fireEvent.transitionEnd(tags[0]);

    expect(tags[0]).not.toBeInTheDocument();
  });

  it('checks undefined or empty value in the list', () => {
    render(<MultiSelect items={[]} value={null as unknown as []} onChange={() => undefined} disabled />);
    render(
      <MultiSelect
        items={[{ value: '1', label: 'Tag 1' }]}
        value={[{ value: '', label: 'Tag 1' }]}
        onChange={() => undefined}
        disabled
      />,
    );
  });

  it.skip('focuses only one instance after choose option', async () => {
    const SeveralInstances: FC<{ instanceItems: ItemType[] }> = ({ instanceItems }) => {
      const [value, setValue] = useState<ItemType[]>([]);
      return (
        <>
          <MultiSelect value={value} onChange={setValue} items={instanceItems} />
          <MultiSelect value={value} onChange={setValue} items={instanceItems} />
        </>
      );
    };
    render(<SeveralInstances instanceItems={items} />);
    const triggerFirst = screen.getAllByTestId('multipulldown-trigger')[0];
    const triggerSecond = screen.getAllByTestId('multipulldown-trigger')[1];
    await user.type(triggerSecond, '{arrowdown}');

    const optionOne = screen.getByRole('checkbox', { name: 'One' });
    const optionTwo = screen.getByRole('checkbox', { name: 'Two' });
    await user.click(optionOne);
    await user.click(optionTwo);

    // expect(optionOne).toBeInTheDocument();

    await user.type(triggerSecond, '{escape}');

    expect(optionOne).not.toBeInTheDocument();

    const tags = screen.getAllByTestId('tag-remove-button');

    triggerSecond.focus();
    await user.tab();

    expect(tags[2]).toHaveFocus();

    await act(async () => user.type(tags[2], '{arrowright}'));

    expect(tags[3]).toHaveFocus();

    await user.type(triggerFirst, '{arrowdown}');
    const addSpy = spyOn(tags[0], 'focus');
    const getOption = screen.getByRole('checkbox', { name: 'One' });
    await user.click(getOption);

    expect(getOption).toBeInTheDocument();
    expect(addSpy).not.toHaveBeenCalled();
  });
});
