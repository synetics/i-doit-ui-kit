import { useSelect, UseSelectState, UseSelectStateChangeOptions } from 'downshift';

export const customReducer =
  (items: string[]) =>
  (
    state: UseSelectState<string>,
    actionAndChanges: UseSelectStateChangeOptions<string>,
  ): Partial<UseSelectState<string>> => {
    const { changes, type } = actionAndChanges;
    switch (type) {
      case useSelect.stateChangeTypes.MenuKeyDownEnter:
      case useSelect.stateChangeTypes.MenuKeyDownSpaceButton:
      case useSelect.stateChangeTypes.ItemClick:
        return {
          ...changes,
          highlightedIndex: items.indexOf(changes.selectedItem || ''),
          isOpen: true, // keep the menu open after selection.
        };
      default:
        break;
    }
    return changes;
  };
