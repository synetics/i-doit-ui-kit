import { useSelect, UseSelectStateChange } from 'downshift';

export const customStateChangeHandler =
  (onSelect: (item: string | null) => void, onClose: () => void) =>
  ({ type, selectedItem }: UseSelectStateChange<string>) => {
    switch (type) {
      case useSelect.stateChangeTypes.ItemClick:
      case useSelect.stateChangeTypes.MenuKeyDownEnter:
      case useSelect.stateChangeTypes.MenuKeyDownSpaceButton:
        onSelect(selectedItem || null);
        break;
      case useSelect.stateChangeTypes.MenuKeyDownEscape:
        onClose();
        break;
      default:
        break;
    }
  };
