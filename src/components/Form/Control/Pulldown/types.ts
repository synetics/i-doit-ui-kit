import { ReactNode } from 'react';

type ItemType = {
  value: string;
  label: string;
  view?: ReactNode | ((highlighted: boolean, searchValue?: string, highlightSearchResult?: boolean) => ReactNode);
  icon?: string | ReactNode;
};

type GroupItemType = ItemType & {
  group?: string;
};

export { GroupItemType, ItemType };
