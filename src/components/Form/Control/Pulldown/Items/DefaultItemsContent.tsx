import React, { ReactElement, useEffect, useRef } from 'react';
import { useEvent } from '../../../../../hooks';
import { ApiType, ScrollList, ScrollListOptions } from '../../../../ScrollList';
import { ItemType } from '../types';

type DefaultItemsContentProps = {
  className?: string;
  highlightedIndex: number | null;
  itemsMap: Map<string, ItemType>;
  options?: ScrollListOptions;
  renderer: (id: string, item: ItemType, index?: number) => ReactElement;
};

const DefaultItemsContent = ({
  className,
  itemsMap,
  highlightedIndex,
  options,
  renderer,
}: DefaultItemsContentProps) => {
  const itemArray = Array.from(itemsMap);
  const apiRef = useRef<ApiType>(null);
  const checkScroll = useEvent((itemIndex: number | null) => {
    const rowVirtualizer = apiRef.current;

    if (
      !rowVirtualizer ||
      itemIndex === null ||
      itemIndex < 0 ||
      rowVirtualizer.virtualItems.some((a) => a.index === itemIndex)
    ) {
      return;
    }

    rowVirtualizer.scrollToIndex(itemIndex, {
      align: 'center',
    });
  });

  useEffect(() => checkScroll(highlightedIndex), [checkScroll, highlightedIndex]);
  return (
    <ScrollList size={itemArray.length} itemSize={40} className={className} apiRef={apiRef} options={options}>
      {(i) => renderer(itemArray[i][0], itemArray[i][1], i)}
    </ScrollList>
  );
};

export { DefaultItemsContent, DefaultItemsContentProps };
