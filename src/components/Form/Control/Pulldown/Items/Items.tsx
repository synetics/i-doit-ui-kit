import React, { ReactNode, useCallback, useMemo } from 'react';
import { UseSelectGetItemPropsOptions } from 'downshift';
import { FuzzyHighlight } from '../../../../FuzzyHighlight';
import { OverflowWithTooltip } from '../../../../OverflowWithTooltip';
import { ScrollListOptions } from '../../../../ScrollList';
import { ItemType } from '../types';
import { DefaultItemsContent } from './DefaultItemsContent';
import { ItemsContext } from './context';
import { Item } from './Item';
import classes from './Item.module.scss';

type ItemsProps = {
  className?: string;
  children?: ReactNode;
  selected: string | null;
  highlightedIndex: number | null;
  getItemProps: (options: UseSelectGetItemPropsOptions<string>) => any;
  itemsMap: Map<string, ItemType>;
  options?: ScrollListOptions;
  highlightSearchResult?: boolean;
  searchValue?: string;
};

const Items = ({
  className,
  itemsMap,
  selected,
  highlightedIndex,
  getItemProps,
  children,
  options,
  highlightSearchResult,
  searchValue = '',
}: ItemsProps) => {
  const isSelected = useCallback((item: string) => item === selected, [selected]);
  const isHighlighted = useCallback((index: number) => index === highlightedIndex, [highlightedIndex]);
  const context = useMemo(
    () => ({ isSelected, isHighlighted, getItemProps, itemsMap }),
    [getItemProps, itemsMap, isHighlighted, isSelected],
  );

  const inner = children || (
    <DefaultItemsContent
      className={className}
      highlightedIndex={highlightedIndex}
      itemsMap={itemsMap}
      options={options}
      renderer={(id, item, i) => (
        <Item key={id} value={id}>
          {(typeof item.view === 'function'
            ? item.view(i === highlightedIndex, searchValue, highlightSearchResult)
            : item.view) || (
            <OverflowWithTooltip showTooltip={i === highlightedIndex} tooltipContent={item.label}>
              <span className={classes.twoLines}>
                {highlightSearchResult ? <FuzzyHighlight text={item.label} searchValue={searchValue} /> : item.label}
              </span>
            </OverflowWithTooltip>
          )}
        </Item>
      )}
    />
  );

  return <ItemsContext.Provider value={context}>{inner}</ItemsContext.Provider>;
};

export { Items, ItemsProps };
