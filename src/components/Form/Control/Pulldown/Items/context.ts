import { createContext, useContext, useMemo } from 'react';
import { UseSelectGetItemPropsOptions } from 'downshift';
import { ItemType } from '../types';

type ItemsContextType = {
  isSelected: (item: string) => boolean;
  isHighlighted: (index: number) => boolean;
  getItemProps: (options: UseSelectGetItemPropsOptions<string>) => Record<string, any>;
  itemsMap: Map<string, ItemType>;
};

const ItemsContext = createContext<ItemsContextType>({
  getItemProps: () => ({}),
  isSelected: () => false,
  isHighlighted: () => false,
  itemsMap: new Map(),
});

type RenderItemProps = {
  selected: boolean;
  highlighted: boolean;
  props: Record<string, any>;
};

const useRenderItem = (value: string): RenderItemProps => {
  const { itemsMap, isSelected, isHighlighted, getItemProps } = useContext(ItemsContext);
  const index = useMemo(() => Array.from(itemsMap.keys()).indexOf(value), [itemsMap, value]);
  const selected = useMemo(() => isSelected(value), [isSelected, value]);
  const highlighted = useMemo(() => isHighlighted(index), [index, isHighlighted]);
  const props = useMemo(() => getItemProps({ item: value, index }), [getItemProps, index, value]);

  return {
    selected,
    highlighted,
    props,
  } as const;
};

export { ItemsContext, useRenderItem };
