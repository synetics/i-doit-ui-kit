import { ReactNode, useEffect } from 'react';
import classnames from 'classnames';
import { useCallbackRef } from '../../../../../hooks';
import { useRenderItem } from './context';
import classes from './Item.module.scss';

type ItemProps = {
  value: string;
  children?: ReactNode;
  shouldScrollIntoView?: boolean;
};

const Item = ({ children, value, shouldScrollIntoView }: ItemProps) => {
  const { selected, highlighted, props } = useRenderItem(value);
  const [ref, setRef] = useCallbackRef<HTMLDivElement>();

  useEffect(() => {
    if (highlighted && shouldScrollIntoView) {
      ref?.scrollIntoView({ block: 'nearest' });
    }
  }, [highlighted, ref, shouldScrollIntoView]);
  return (
    <div
      tabIndex={-1}
      {...props}
      className={classnames(classes.menuItem, {
        [classes.hoverItem]: highlighted,
        [classes.selected]: selected,
      })}
      ref={setRef}
    >
      {children}
    </div>
  );
};

export { Item, ItemProps };
