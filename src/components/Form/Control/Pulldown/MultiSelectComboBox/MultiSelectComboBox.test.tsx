import { fireEvent, render, screen } from '@testing-library/react';
import React from 'react';
import user from '@testing-library/user-event';
import { uniqueId } from '../../../../../utils';
import { GroupedItemsMultiSelect } from '../GroupedItemsMultiselect';
import { GroupItemType } from '../types';
import { MultiSelectComboBox } from './MultiSelectComboBox';

describe('<MultiSelectComboBox />', () => {
  const items = [
    { value: '1', label: 'One' },
    { value: '2', label: 'Two' },
    { value: '3', label: 'Three' },
    { value: '4', label: 'Four', icon: 'icon' },
  ];

  it('renders without crashing', () => {
    render(<MultiSelectComboBox id={uniqueId()} onChange={() => {}} value={null} items={[]} />);

    const trigger = screen.getByTestId('pulldown-trigger');
    expect(trigger).toBeInTheDocument();
    const menu = screen.queryByRole('menu');
    expect(menu).not.toBeInTheDocument();
    const button = screen.queryByTestId('reset-button');
    expect(button).not.toBeInTheDocument();
  });

  it('checked collapsed tags', async () => {
    const moreItems = [...items, { value: '5', label: 'Five' }, { value: '6', label: 'Six' }];
    render(
      <MultiSelectComboBox
        id={uniqueId()}
        onChange={() => {}}
        value={items}
        items={moreItems}
        highlightSearchResult={false}
      />,
    );
    const collapsedTags = screen.getByText('3 more');
    expect(collapsedTags).toBeInTheDocument();
    expect(screen.getByText('Four')).toBeInTheDocument();
    expect(screen.getByText('icon')).toBeInTheDocument();
    await user.click(collapsedTags);
    expect(screen.getByRole('textbox')).toHaveFocus();
    expect(screen.getAllByText('One')[1]).toBeInTheDocument();
  });

  it('check collapsed tags readonly', () => {
    const handleChange = jest.fn();
    render(<MultiSelectComboBox id={uniqueId()} onChange={handleChange} value={items} items={items} readonly />);
    expect(screen.queryByTestId('tag-remove-button')).not.toBeInTheDocument();
  });

  it('renders with placeholder', () => {
    render(
      <MultiSelectComboBox id={uniqueId()} onChange={() => {}} value={null} items={[]} placeholder="Placeholder" />,
    );

    expect(screen.getByPlaceholderText('Placeholder')).toBeInTheDocument();
  });

  it('renders with children as Render props', async () => {
    render(
      <MultiSelectComboBox id={uniqueId()} onChange={() => {}} value={null} items={[]} placeholder="Placeholder">
        {() => <div>children</div>}
      </MultiSelectComboBox>,
    );
    await user.click(screen.getByTestId('pulldown-trigger'));
    expect(screen.getByText('children')).toBeInTheDocument();
  });

  it('check and uncheck elems by click', async () => {
    const handleChange = jest.fn();
    render(
      <MultiSelectComboBox
        id={uniqueId()}
        items={items}
        value={[{ value: '2', label: 'Two' }]}
        onChange={handleChange}
      />,
    );

    await user.click(screen.getByTestId('pulldown-trigger'));

    const firstCheckBox = screen.getByRole('checkbox', { name: 'One' });
    const secondCheckBox = screen.getByRole('checkbox', { name: 'Two' });
    const secondOption = screen.getAllByRole('option')[1];

    expect(firstCheckBox).toBeInTheDocument();
    expect(secondCheckBox).toBeInTheDocument();
    expect(secondCheckBox).toBeChecked();

    await user.click(secondOption);
    expect(handleChange).toHaveBeenCalledWith([]);
  });

  it('check the Input value after the selection', async () => {
    const handleChange = jest.fn();
    render(<MultiSelectComboBox id={uniqueId()} items={items} value={[]} onChange={handleChange} />);

    await user.click(screen.getByTestId('pulldown-trigger'));

    const firstCheckBox = screen.getByRole('checkbox', { name: 'One' });
    const secondCheckBox = screen.getByRole('checkbox', { name: 'Two' });

    expect(firstCheckBox).toBeInTheDocument();
    expect(secondCheckBox).toBeInTheDocument();

    const input = screen.getByRole('textbox');
    fireEvent.change(input, { target: { value: 'One' } });
    expect(input).toHaveValue('One');

    await user.click(firstCheckBox);
    expect(input).toHaveValue('');
  });

  it('reacts with keyboards and check focus on input', async () => {
    const handleChange = jest.fn();
    render(<MultiSelectComboBox items={items} value={null} id={uniqueId()} onChange={handleChange} />);
    const trigger = screen.getByTestId('pulldown-trigger');

    await user.type(trigger, '{arrowdown}');

    const option = screen.getByRole('checkbox', { name: 'One' });

    expect(option).toBeInTheDocument();
    option.focus();

    await user.type(trigger, '{enter}');
    expect(handleChange).toHaveBeenCalledWith([items[0]]);
    const input = screen.getByRole('textbox');
    expect(input).toHaveFocus();
  });

  it('set user selected order for values', async () => {
    const handleChange = jest.fn();
    render(<MultiSelectComboBox items={items} value={[items[1]]} id={uniqueId()} onChange={handleChange} />);
    const trigger = screen.getByTestId('pulldown-trigger');

    await user.type(trigger, '{arrowdown}');

    const optionOne = screen.getByRole('checkbox', { name: 'One' });
    await user.click(optionOne);

    expect(handleChange).toHaveBeenCalledWith([
      { label: 'Two', value: '2' },
      { label: 'One', value: '1' },
    ]);
  });

  it('deletes elem by clicking on option and tag', async () => {
    const handleChange = jest.fn();

    render(<MultiSelectComboBox id={uniqueId()} items={items} value={[items[0]]} onChange={handleChange} />);

    const trigger = screen.getByTestId('pulldown-trigger');
    await user.click(trigger);

    const firstCheck = screen.getByRole('checkbox', { name: 'One' });

    const firstOption = screen.getAllByRole('option')[0];

    expect(firstCheck).toBeInTheDocument();

    expect(firstCheck).toBeChecked();

    await user.click(firstOption);

    await user.type(trigger, '{enter}');

    // user.click(firstCheck);

    expect(firstCheck).toBeInTheDocument();

    expect(handleChange).toHaveBeenCalledWith([]);

    const closeButton = screen.getByTestId('tag-remove-button');

    expect(closeButton).toBeInTheDocument();

    await user.click(closeButton);

    expect(handleChange).toHaveBeenCalledWith([]);
  });
  it('renders reset button if selections is not empty', () => {
    render(<MultiSelectComboBox id={uniqueId()} items={items} value={items} onChange={() => {}} canReset />);

    const button = screen.queryByTestId('reset-button');
    expect(button).toBeInTheDocument();
  });

  it('click on reset button', async () => {
    const onChange = jest.fn();
    render(<MultiSelectComboBox id={uniqueId()} items={items} value={items} onChange={onChange} canReset />);
    const button = screen.getByTestId('reset-button');
    await user.click(button);

    expect(onChange).toHaveBeenCalledWith([]);
  });

  it('removes selection on backspace', async () => {
    const onChange = jest.fn();
    render(<MultiSelectComboBox id={uniqueId()} items={items} value={items} onChange={onChange} />);
    const input = screen.getByRole('textbox');

    await user.type(input, '{backspace}');
    expect(onChange).toHaveBeenCalledWith([items[0], items[1], items[2]]);
  });

  it('check searching result with default threshold', async () => {
    render(
      <MultiSelectComboBox
        id={uniqueId()}
        items={[
          { value: '1', label: 'One Item' },
          { value: '2', label: 'Two' },
        ]}
        value={null}
        onChange={() => {}}
      />,
    );
    const input = screen.getByRole('textbox');

    await user.type(input, '{arrowdown}');
    const secondCheckbox = screen.getByRole('checkbox', { name: 'Two' });
    expect(secondCheckbox).toBeInTheDocument();
    fireEvent.change(input, { target: { value: 'One Item' } });
    expect(screen.getByRole('checkbox', { name: 'One Item' })).toBeInTheDocument();
    expect(secondCheckbox).not.toBeInTheDocument();
  });

  it('check searching result without result', async () => {
    render(
      <MultiSelectComboBox
        id={uniqueId()}
        items={[
          { value: '1', label: 'One Item' },
          { value: '2', label: 'Two' },
        ]}
        value={null}
        onChange={() => {}}
        titleDefault="No results are found"
      />,
    );
    const input = screen.getByRole('textbox');

    await user.type(input, '{arrowdown}');

    fireEvent.change(input, { target: { value: 'Three' } });

    expect(screen.getByText('No results are found')).toBeInTheDocument();
  });

  it('check searching result with threshold value = 2', async () => {
    render(
      <MultiSelectComboBox
        id={uniqueId()}
        items={[
          { value: '1', label: 'One Item' },
          { value: '2', label: 'Two' },
        ]}
        value={null}
        threshold={2}
        onChange={() => {}}
      />,
    );
    const input = screen.getByRole('textbox');

    await user.type(input, '{arrowdown}');
    const firstCheckbox = screen.getByRole('checkbox', { name: 'One Item' });
    const secondCheckbox = screen.getByRole('checkbox', { name: 'Two' });
    expect(secondCheckbox).toBeInTheDocument();

    fireEvent.change(input, { target: { value: 'O' } });
    expect(firstCheckbox).toBeInTheDocument();
    expect(secondCheckbox).toBeInTheDocument();

    fireEvent.change(input, { target: { value: 'One Item' } });

    expect(firstCheckbox).toBeInTheDocument();
    expect(secondCheckbox).not.toBeInTheDocument();
  });

  it('check second item after correct search', async () => {
    const handleChange = jest.fn();
    render(
      <MultiSelectComboBox
        id={uniqueId()}
        items={items}
        value={[{ label: 'One', value: '1' }]}
        threshold={2}
        onChange={handleChange}
      />,
    );
    const input = screen.getByRole('textbox');

    await user.type(input, '{arrowdown}');
    const firstCheckbox = screen.getByRole('checkbox', { name: 'One' });

    await user.type(input, 'Two');

    expect(firstCheckbox).not.toBeInTheDocument();
    expect(screen.getByRole('checkbox', { name: 'Two' })).toBeInTheDocument();

    await user.click(screen.getByRole('checkbox', { name: 'Two' }));

    expect(handleChange).toHaveBeenCalledWith([
      { label: 'One', value: '1' },
      { label: 'Two', value: '2' },
    ]);

    expect(screen.getByTestId('tag-remove-button')).toBeInTheDocument();
  });

  it('renders with the custom value', async () => {
    const view = <div data-testid="view">View</div>;
    render(
      <MultiSelectComboBox
        id={uniqueId()}
        onChange={() => {}}
        items={[{ value: 'value', label: 'Label', view }]}
        value={null}
        placeholder="Placeholder"
      />,
    );

    const input = screen.getByRole('textbox');

    await user.type(input, '{arrowdown}');

    expect(screen.getByTestId('view')).toBeInTheDocument();
    expect(screen.getByTestId('view')).toHaveTextContent('View');
  });

  it('check control input', async () => {
    const onInput = jest.fn();
    render(
      <MultiSelectComboBox
        id={uniqueId()}
        items={items}
        value={items}
        onChange={() => {}}
        onInputChange={onInput}
        canReset
      />,
    );
    const input = screen.getByRole('textbox');
    fireEvent.change(input, { target: { value: 'input-change-1' } });
    expect(onInput).toHaveBeenCalledWith('input-change-1');
    const button = screen.queryByTestId('reset-button');

    if (button) {
      await user.click(button);
    }
    expect(onInput).toHaveBeenCalledWith('');
  });

  it('closes the menu on escape', async () => {
    render(<MultiSelectComboBox id={uniqueId()} items={items} value={[]} onChange={() => {}} />);
    const input = screen.getByRole('textbox');
    await user.click(input);
    const options = screen.queryAllByRole('option');
    expect(options).toHaveLength(2);
    await user.type(options[0], '{ESCAPE}');
    expect(screen.queryAllByRole('option').length).toBe(0);
  });

  it('does not open on click if disabled', async () => {
    render(<MultiSelectComboBox disabled id={uniqueId()} items={items} value={[]} onChange={() => {}} />);
    const input = screen.getByRole('textbox');
    await user.click(input);
    const options = screen.queryAllByRole('option');
    expect(options.length).toBe(0);
  });

  it('tags have no remove if disabled', () => {
    render(<MultiSelectComboBox disabled id={uniqueId()} items={items} value={[]} onChange={() => {}} />);
    const tags = screen.queryAllByTestId('tag-remove-button');
    expect(tags.length).toBe(0);
  });

  it('renders with children with Group as Render props', async () => {
    const createItem = (group: string, key: string): GroupItemType => ({
      group,
      value: key,
      label: key,
    });
    const onChange = jest.fn();
    render(
      <MultiSelectComboBox
        id={uniqueId()}
        onChange={() => {}}
        value={null}
        items={[createItem('one', '1'), createItem('one', '2')]}
        placeholder="Placeholder"
      >
        {({ items: passedItems, itemsMap, selectedItems, highlightedIndex, getItemProps }) => (
          <GroupedItemsMultiSelect
            items={passedItems}
            selectedItems={selectedItems}
            onChange={onChange}
            highlightedIndex={highlightedIndex}
            getItemProps={getItemProps}
            itemsMap={itemsMap}
          />
        )}
      </MultiSelectComboBox>,
    );
    await user.click(screen.getByTestId('pulldown-trigger'));
    expect(screen.getByText('one')).toBeInTheDocument();
    const links = screen.getAllByText('1');
    await user.click(links[0]);

    expect(onChange).toHaveBeenCalledTimes(1);
  });
});
