import React, {
  forwardRef,
  KeyboardEvent,
  ReactElement,
  ReactNode,
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react';
import {
  useCombobox,
  UseComboboxGetItemPropsOptions,
  UseComboboxStateChange,
  useMultipleSelection,
  UseMultipleSelectionStateChange,
} from 'downshift';
import { VirtualElement } from '@popperjs/core';
import classnames from 'classnames';
import { FieldGroup, FieldProps, useActive, useFieldGroup, ValueProps } from '../../../Infrastructure';
import { Tag, Tags } from '../../../../Tag';
import { Popper, SameWidthModifierAndSizeChange, ScrollParentModifier } from '../../../../Popper';
import { Icon } from '../../../../Icon';
import { arrow_down, arrow_up } from '../../../../../icons';
import {
  useCallbackRef,
  useEventListener,
  useFocusWithin,
  useGlobalizedRef,
  useTagItemsFocus,
} from '../../../../../hooks';
import { ScrollListOptions } from '../../../../ScrollList';
import { Input } from '../../Input';
import { Items } from '../MultiSelect/items/Items';
import { ItemType } from '../types';
import { useItemsMap } from '../helpers/useItemsMap';
import classesForDefault from '../Items/Item.module.scss';
import { filterOptions } from '../combobox.utils';
import { ResetButton } from '../ResetButton';
import { getItemLabelFromMap } from '../helpers/item-to-string.util';
import { sortSelected } from '../helpers/sortSelected';
import classes from './MultiSelectComboBox.module.scss';

const popperOptions = {
  modifiers: [SameWidthModifierAndSizeChange, ScrollParentModifier],
};
type MultiSelectValueType = ItemType[];

export type ChildrenProps = (props: {
  items: ItemType[];
  input?: string;
  itemsMap: Map<string, ItemType>;
  selectedItems: string[];
  highlightedIndex: number;
  getItemProps: (options: UseComboboxGetItemPropsOptions<string>) => any;
  onChange: (identifier: string) => void;
}) => ReactNode | string;

export type ComboBoxMultiSelectProps = Required<ValueProps<MultiSelectValueType | null>> &
  FieldProps & {
    /**
     * Id for input
     */
    id: string;
    /**
     * Available items for selection
     */
    items: ItemType[];
    /**
     * Placeholder to be shown when nothing is selected
     */
    placeholder?: string;
    /**
     * Threshold value for search
     */
    threshold?: number;
    /**
     * Title for default component
     */
    titleDefault?: string;

    /**
     * Children
     */
    children?: ChildrenProps | ReactElement;

    /**
     * Handler for the user input
     */
    onInputChange?: (value: string) => void;

    /**
     * Reference element for pulldown
     */
    pulldownContainer?: Element | VirtualElement | null;

    /**
     * if user can reset value
     */
    canReset?: boolean;
    /**
     * Options for ScrollList component
     */
    options?: ScrollListOptions;
    /**
     * Option to highlight search value in items
     */
    highlightSearchResult?: boolean;
    /**
     * Option to collapse tags
     */
    collapsedTags?: boolean;
  };

const MultiSelectComboBox = forwardRef<HTMLDivElement, ComboBoxMultiSelectProps>(
  (
    {
      id,
      active: passedActive,
      canReset,
      items,
      disabled,
      readonly,
      value,
      onChange,
      children,
      placeholder,
      setActive: passedSetActive,
      threshold,
      titleDefault,
      className,
      onInputChange,
      pulldownContainer,
      options = {},
      highlightSearchResult = true,
      collapsedTags = true,
      ...rest
    },
    forwardedRef,
  ) => {
    const [itemState, setItemState] = useState<ItemType[]>(items);
    const { localRef: ref, setRefsCallback: setRef } = useGlobalizedRef(forwardedRef);
    const inputRef = useRef<HTMLInputElement | null>(null);
    const [tagsRef, setTagsRef] = useCallbackRef<HTMLElement>();
    const { value: active, setValue: setActive } = useActive(passedActive, passedSetActive);
    const { readonly: derivedReadonly, disabled: derivedDisabled } = useFieldGroup({ disabled, readonly });
    const notEditable = derivedReadonly || derivedDisabled;
    const itemsMap = useItemsMap(itemState);
    const allItemsMap = useItemsMap(items);

    const focus = useCallback(() => {
      if (inputRef.current) {
        inputRef.current.focus();
      }
    }, []);
    const itemToString = useMemo(() => getItemLabelFromMap(itemsMap), [itemsMap]);
    const onIsOpenChange = useCallback(
      ({ isOpen: open, type }: UseComboboxStateChange<string>) => {
        setActive(open || false);

        if (!open && type !== useCombobox.stateChangeTypes.InputBlur) {
          focus();
        }
      },
      [focus, setActive],
    );
    const values = useMemo(() => Array.from(itemsMap.keys()), [itemsMap]);
    const selectedValues = useMemo(() => value?.map((item) => item.value), [value]);

    const onSelectedItemsChange = useCallback(
      ({ selectedItems: newValue }: UseMultipleSelectionStateChange<string>) => {
        if (Array.isArray(newValue)) {
          onChange(newValue.map((item) => allItemsMap.get(item) as ItemType));
        }
      },
      [onChange, allItemsMap],
    );

    const { getDropdownProps, addSelectedItem, removeSelectedItem, selectedItems } = useMultipleSelection({
      selectedItems: selectedValues || [],
      onSelectedItemsChange,
      stateReducer: (state, action) => {
        const uniqueItems = Array.from(new Set(action.changes.selectedItems));
        return {
          ...action.changes,
          selectedItems:
            action.changes.selectedItems?.length === uniqueItems.length ? action.changes.selectedItems : uniqueItems,
        };
      },
    });
    const {
      isOpen,
      getToggleButtonProps,
      getMenuProps,
      getInputProps,
      getComboboxProps,
      getItemProps,
      setInputValue,
      highlightedIndex,
      inputValue: inValue,
      reset,
      openMenu,
      closeMenu,
    } = useCombobox({
      items: values,
      itemToString,
      onIsOpenChange,
      defaultHighlightedIndex: (selectedValues || []).length > 0 ? values.indexOf((selectedValues || [])[0]) : 0,
      selectedItem: null,
      onInputValueChange: ({ inputValue }) => {
        if (!isOpen && inputValue?.length) {
          openMenu();
        }
        if (onInputChange && typeof inputValue === 'string') {
          onInputChange(inputValue);
        }
      },

      stateReducer: (state, actionAndChanges) => {
        const { changes, type } = actionAndChanges;
        switch (type) {
          case useCombobox.stateChangeTypes.InputKeyDownEnter:
          case useCombobox.stateChangeTypes.ItemClick:
            return {
              ...changes,
              highlightedIndex: values.indexOf(changes.selectedItem || ''),
              isOpen: true, // keep the menu open after selection.
            };
          default:
            break;
        }
        return changes;
      },
      onStateChange: ({ type, selectedItem: itemSelected }) => {
        switch (type) {
          case useCombobox.stateChangeTypes.ItemClick:
          case useCombobox.stateChangeTypes.InputKeyDownEnter:
            if (itemSelected) {
              if (itemSelected && !selectedItems.includes(itemSelected)) {
                addSelectedItem(itemSelected);
              } else {
                removeSelectedItem(itemSelected);
              }
            }
            break;
          default:
            break;
        }
      },
    });

    useEffect(() => {
      setItemState(filterOptions(items, { searchTerm: inValue || '' }, threshold));
    }, [inValue, items, threshold]);

    const handleReset = () => {
      onChange([]);
      if (inValue.length > 0) {
        setInputValue('');
      }
      reset();
      setTimeout(focus, 100);
    };

    const withoutSearchItemMap = useItemsMap(items);
    const withoutSearchItemMapToString = useMemo(
      () => getItemLabelFromMap(withoutSearchItemMap),
      [withoutSearchItemMap],
    );
    const tagItems = useMemo<ItemType[]>(
      () =>
        selectedItems.map((itemSelected) => ({
          value: itemSelected,
          label: withoutSearchItemMapToString(itemSelected),
          view: itemsMap.get(itemSelected)?.view,
          icon: itemsMap.get(itemSelected)?.icon || items.find((el) => el.value === itemSelected)?.icon,
        })),
      [selectedItems, withoutSearchItemMapToString, itemsMap, items],
    );

    const handleRemoveSelectedItem = (Id: number) => {
      removeSelectedItem(selectedItems[Id]);
    };

    const [index, setIndex] = useState(0);
    const focusIndex = useTagItemsFocus(tagsRef, tagItems.length, handleRemoveSelectedItem, index, setIndex);
    const defaultComponent = (
      <div className={classes.menu}>
        <div className={classesForDefault.menuItem}>{titleDefault}</div>
      </div>
    );

    const handlerChange = (identifier: string) => {
      if (!selectedItems.includes(identifier)) {
        addSelectedItem(identifier);
      } else removeSelectedItem(identifier);
      focus();
      setInputValue('');
    };

    const passedChildren =
      typeof children === 'function'
        ? children({
            items: itemState,
            input: inValue,
            itemsMap,
            selectedItems,
            highlightedIndex,
            getItemProps,
            onChange: handlerChange,
          })
        : children;

    const focusWithinBar = useFocusWithin(ref.current);
    const focusWithinInput = useFocusWithin(inputRef.current);
    const focusWithinTags = useFocusWithin(tagsRef);
    const handleEscape = useCallback(
      (e: KeyboardEvent) => {
        if (e.key === 'Escape' && isOpen) {
          closeMenu();
          e.stopPropagation();
        }
      },
      [closeMenu, isOpen],
    );
    useEventListener('keydown', handleEscape, ref.current);

    const toggleMenu = useCallback(() => {
      if (!notEditable) {
        if (isOpen) {
          closeMenu();
        } else {
          openMenu();
          focus();
        }
      }
    }, [notEditable, isOpen, closeMenu, openMenu, focus]);

    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
    const { onKeyDown: comboboxKeyDown, ...comboboxProps } = getComboboxProps({
      ref: setRef,
    });

    const handleKeyDownCombobox = useCallback(
      (e: React.KeyboardEvent<HTMLDivElement>) => {
        if (isOpen) {
          e.stopPropagation();
        }
        if (comboboxKeyDown) {
          // eslint-disable-next-line @typescript-eslint/no-unsafe-call
          comboboxKeyDown(e);
        }
      },
      [comboboxKeyDown, isOpen],
    );
    const handleOpenMore = useCallback(() => {
      openMenu();
      focus();
      if (value && value.length > 0) {
        setItemState(sortSelected({ items: itemState, value }));
      }
    }, [openMenu, itemState, value, focus]);
    const itemsQuantity = tagItems.length - 1;
    return (
      <>
        <FieldGroup
          {...rest}
          {...comboboxProps}
          onKeyDown={handleKeyDownCombobox}
          onClick={toggleMenu}
          active={active}
          setActive={() => {}}
          readonly={derivedReadonly}
          disabled={derivedDisabled}
          className={classnames(classes.container, className)}
          data-testid="pulldown-trigger"
        >
          <div ref={setRef} className={classnames(classes.containerTags, { [classes.resetButton]: inValue.length })}>
            <Tags
              enabled={!notEditable}
              autoFocus={focusWithinTags && !focusWithinInput}
              focusIndex={focusWithinBar ? focusIndex : -1}
              ref={setTagsRef}
              className={classes.tags}
            >
              {tagItems.length > 2 && collapsedTags ? (
                <>
                  <Tag className={classes.tag} onClick={handleOpenMore}>
                    {itemsQuantity} more
                  </Tag>
                  <Tag
                    prefix={tagItems[itemsQuantity].icon}
                    onClose={!notEditable ? () => handleRemoveSelectedItem(itemsQuantity) : undefined}
                    className={classes.tag}
                  >
                    {tagItems[itemsQuantity].label}
                  </Tag>
                </>
              ) : (
                tagItems.map(({ value: val, label, icon }, i) => (
                  <Tag
                    key={val}
                    prefix={icon || null}
                    onClose={!notEditable ? () => handleRemoveSelectedItem(i) : undefined}
                    className={classes.tag}
                  >
                    {label}
                  </Tag>
                ))
              )}

              <Input
                key="input"
                type="text"
                {...getInputProps(
                  getDropdownProps({
                    preventKeyAction: isOpen,
                    id,
                    ref: inputRef,
                    disabled: derivedDisabled,
                    onKeyDownCapture: (e: KeyboardEvent) => {
                      if (['Enter', 'Escape', 'ArrowUp', 'ArrowDown'].includes(e.key)) {
                        return;
                      }
                      if (e.key === 'Backspace' && inValue.length === 0 && tagItems.length > 0) {
                        handlerChange(tagItems[tagItems.length - 1].value);
                      }

                      e.stopPropagation();
                    },
                  }),
                )}
                tabIndex={0}
                placeholder={placeholder}
                onChange={setInputValue}
                className={classnames(classes.input, {
                  [classes.visible]: (focusWithinBar && !notEditable) || tagItems.length === 0,
                })}
                readonly={derivedReadonly}
              />
            </Tags>
          </div>
          {canReset && tagItems.length > 0 && !notEditable && <ResetButton onClick={handleReset} />}
          {!derivedReadonly && (
            <Icon
              src={isOpen ? arrow_up : arrow_down}
              {...getToggleButtonProps({ disabled: notEditable })}
              className={classnames(classes.trigger, {
                [classes.open]: isOpen,
              })}
            />
          )}
        </FieldGroup>
        <Popper
          open={isOpen}
          options={popperOptions}
          referenceElement={pulldownContainer || ref.current}
          className={classes.popper}
        >
          <div {...getMenuProps()}>
            <Items
              className={classes.menu}
              itemsMap={itemsMap}
              selectedItems={selectedItems}
              highlightedIndex={highlightedIndex}
              getItemProps={getItemProps}
              onChange={handlerChange}
              options={options}
              highlightSearchResult={highlightSearchResult}
              searchValue={inValue}
            >
              {!passedChildren && !itemState.length ? defaultComponent : passedChildren}
            </Items>
          </div>
        </Popper>
      </>
    );
  },
);

export { MultiSelectComboBox };
