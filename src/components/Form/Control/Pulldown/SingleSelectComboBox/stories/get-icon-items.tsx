import faker from 'faker';
import React from 'react';
import { Icon } from '../../../../../Icon';
import { getRandomIcon } from '../../MultiSelect/stories/get-random-icon';
import { FuzzyHighlight } from '../../../../../FuzzyHighlight';
import { OverflowWithTooltip } from '../../../../../OverflowWithTooltip';
import classes from './SingleSelectComboBox.module.scss';

export const items = new Array(20)
  .fill(0)
  .map((_, i) => ({
    label: faker.address.city(),
    value: `value${i}`,
  }))
  .sort(({ label: a }, { label: b }) => a.localeCompare(b));

export const getIconItems = (asString: boolean) =>
  items.map((item, i) => {
    const icon = getRandomIcon();
    return {
      ...item,
      icon: asString ? icon : <Icon src={icon} />,
      view: (highlightedIndex: number, searchValue: string, highlightSearchResult: boolean) => (
        <div className={classes.labelContainer}>
          <Icon src={icon} />
          <OverflowWithTooltip showTooltip={i === highlightedIndex} tooltipContent={item.label}>
            {highlightSearchResult ? <FuzzyHighlight text={item.label} searchValue={searchValue} /> : item.label}
          </OverflowWithTooltip>
        </div>
      ),
    };
  });
