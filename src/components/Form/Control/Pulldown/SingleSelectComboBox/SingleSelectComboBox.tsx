import {
  ForwardedRef,
  forwardRef,
  HTMLProps,
  KeyboardEvent,
  ReactElement,
  ReactNode,
  useCallback,
  useEffect,
  useMemo,
  useRef,
} from 'react';
import { useCombobox, UseComboboxStateChange } from 'downshift';
import classnames from 'classnames';
import { VirtualElement } from '@popperjs/core';
import { arrow_down, arrow_up } from '../../../../../icons';
import { Icon } from '../../../../Icon';
import { Popper, SameWidthModifierAndSizeChange, ScrollParentModifier } from '../../../../Popper';
import { ScrollListOptions } from '../../../../ScrollList';
import { useControlledState, useEvent, useGlobalizedRef } from '../../../../../hooks';
import { FieldGroup, FieldProps, useActive, useFieldGroup, ValueProps } from '../../../Infrastructure';
import { Input } from '../../Input';
import { ItemType } from '../types';
import { Items } from '../Items/Items';
import classesForDefault from '../Items/Item.module.scss';
import { useItemsMap } from '../helpers/useItemsMap';
import { filterOptions } from '../combobox.utils';
import { ResetButton } from '../ResetButton';
import { getItemLabelFromMap } from '../helpers/item-to-string.util';
import classes from './SingleSelectComboBox.module.scss';

const popperOptions = {
  modifiers: [SameWidthModifierAndSizeChange, ScrollParentModifier],
};

export type RenderChildrenProps<T extends ItemType> = {
  items: T[];
  itemsMap: Map<string, T>;
  input?: string;
  highlightedIndex: number;
  shouldHighlighted?: boolean;
};

type ComboBoxSingleSelectProps<T extends ItemType = ItemType> = Required<ValueProps<T | null>> &
  FieldProps & {
    /**
     * Id for input
     */
    id: string;
    /**
     * Available items for selection
     */
    items: T[];
    /**
     * Placeholder to be shown when nothing is selected
     */
    placeholder?: string;
    /**
     * Handler for the user input
     */
    onInputChange?: (value: string) => void;
    /**
     * Threshold value for search
     */
    threshold?: number;
    /**
     * Title for default component
     */
    titleDefault?: string;
    /**
     * Reference element for pulldown
     */
    pulldownContainer?: Element | VirtualElement | null;
    /**
     * Children
     */
    children?: (props: RenderChildrenProps<T>) => ReactNode | string;
    /**
     * Is the value optional and can be reset
     */
    canReset?: boolean;

    /**
     * Custom Filter function
     */
    filter?: (items: T[], search: { searchTerm: string }, threshold?: number) => T[];
    /**
     * Options for ScrollList component
     */
    options?: ScrollListOptions;
    /**
     * Options for input value
     */
    userInput?: string;
    /**
     * Options to delete input value after item selection
     */
    clearInputAfterSelection?: boolean;
    /**
     * set show menu
     */
    onToggleMenu?: (v: boolean) => void;
    /**
     * Option to highlight search value in items
     */
    highlightSearchResult?: boolean;
    /**
     * Is the input haves autosuggestion
     */
    autosuggestion?: boolean;
  };

const SingleSelectComboboxInner = <T extends ItemType>(
  {
    id,
    active: passedActive,
    items,
    disabled,
    readonly,
    value,
    onChange,
    children,
    placeholder,
    setActive: passedSetActive,
    threshold,
    onInputChange,
    titleDefault,
    className,
    pulldownContainer,
    filter = filterOptions,
    canReset = true,
    options = {},
    userInput,
    clearInputAfterSelection = true,
    onToggleMenu,
    autosuggestion = false,
    highlightSearchResult = true,
    ...rest
  }: ComboBoxSingleSelectProps<T>,
  forwardedRef: ForwardedRef<HTMLDivElement>,
): ReactElement => {
  const handleFilter = useEvent(filter);
  const [searchTerm, setSearchTerm] = useControlledState(userInput || (value ? value.label : ''), onInputChange);
  const hasChanges = useMemo(() => searchTerm !== value?.label, [value, searchTerm]);
  const shouldHighlighted = useMemo(() => highlightSearchResult && hasChanges, [highlightSearchResult, hasChanges]);
  const filteredItems = useMemo(
    () => (hasChanges ? handleFilter(items, { searchTerm }, threshold) : items),
    [handleFilter, hasChanges, threshold, items, searchTerm],
  );
  const { localRef: ref, setRefsCallback: setRef } = useGlobalizedRef(forwardedRef);
  const inputRef = useRef<HTMLInputElement>(null);

  const focus = useCallback(() => {
    if (inputRef.current) {
      inputRef.current.focus();
    }
  }, []);

  const { value: active, setValue: setActive } = useActive(passedActive, passedSetActive);
  const { readonly: derivedReadonly, disabled: derivedDisabled } = useFieldGroup({ disabled, readonly });
  const notEditable = derivedReadonly || derivedDisabled;
  const itemsMap = useItemsMap(filteredItems);
  const allItemsMap = useItemsMap(items);
  const itemToString = useMemo(() => getItemLabelFromMap(allItemsMap), [allItemsMap]);

  const onIsOpenChange = useCallback(
    ({ isOpen: open }: UseComboboxStateChange<string>) => setActive(open || false),
    [setActive],
  );
  const values = useMemo(() => filteredItems.map((a) => a.value), [filteredItems]);
  const selectedItem = useMemo(() => (value ? value.value : null), [value]);

  const {
    isOpen,
    openMenu,
    closeMenu,
    getToggleButtonProps,
    getMenuProps,
    getInputProps,
    getComboboxProps,
    getItemProps,
    setInputValue,
    highlightedIndex,
    setHighlightedIndex,
    reset,
  } = useCombobox({
    items: values,
    selectedItem: selectedItem || undefined,
    itemToString,
    inputValue: searchTerm,
    onIsOpenChange,
    stateReducer: (state, { type, changes }) => {
      switch (type) {
        case useCombobox.stateChangeTypes.InputKeyDownEscape:
          if (!state.isOpen) {
            return state;
          }
          return changes;
        case useCombobox.stateChangeTypes.FunctionSetInputValue:
          return {
            ...changes,
            isOpen: true,
            highlightedIndex: Math.max(changes.highlightedIndex || -1, 0),
          };
        case useCombobox.stateChangeTypes.ItemClick:
        case useCombobox.stateChangeTypes.InputKeyDownEnter:
        case useCombobox.stateChangeTypes.FunctionSelectItem:
        case useCombobox.stateChangeTypes.ControlledPropUpdatedSelectedItem:
          return clearInputAfterSelection ? { ...changes } : { ...changes, inputValue: userInput };
        default:
          return changes;
      }
    },
    onStateChange: (changes) => {
      switch (changes.type) {
        case useCombobox.stateChangeTypes.FunctionReset:
        case useCombobox.stateChangeTypes.FunctionSelectItem:
        case useCombobox.stateChangeTypes.InputKeyDownEscape:
        case useCombobox.stateChangeTypes.FunctionOpenMenu:
          focus();
          break;
        default:
      }
    },
    onSelectedItemChange: (change) => {
      const item =
        change.selectedItem && itemsMap.has(change.selectedItem) ? itemsMap.get(change.selectedItem) || null : null;

      onChange(item);
    },
    onInputValueChange: ({ inputValue }) => {
      setSearchTerm(inputValue || '');

      if (onInputChange) {
        onInputChange(inputValue || '');
      }

      if (!inputValue && selectedItem !== null) {
        onChange(null);
      }
    },
  });
  const autosuggestionValue = useMemo(() => {
    if (filteredItems.length > 0 && autosuggestion && searchTerm.length > 0) {
      const highlightedLabel = filteredItems[highlightedIndex]?.label || '';

      const isMatch = highlightedLabel.slice(0, searchTerm.length).toLowerCase() === searchTerm.toLowerCase();
      if (isMatch && highlightedLabel) {
        return highlightedLabel;
      }
    }
    return '';
  }, [searchTerm, autosuggestion, highlightedIndex, filteredItems]);

  const suggestViewableText = useMemo(
    () => autosuggestionValue.substring(autosuggestionValue.length - (autosuggestionValue.length - searchTerm.length)),
    [autosuggestionValue, searchTerm],
  );
  const handleEnter = useCallback(
    (e: KeyboardEvent) => {
      if (['ArrowRight'].includes(e.key) && autosuggestionValue) {
        e.stopPropagation();
        const { selectionStart } = e.target as HTMLInputElement;
        if (searchTerm?.length === selectionStart) {
          setHighlightedIndex(0);
          setSearchTerm(autosuggestionValue);
        }
      }
      if (active && ['Enter', 'Escape'].includes(e.key)) {
        e.stopPropagation();
        setActive(false);
      }
    },
    [searchTerm, autosuggestionValue, active, setSearchTerm, setActive, setHighlightedIndex],
  );

  useEffect(() => {
    if (onToggleMenu) {
      onToggleMenu(isOpen);
    }
  }, [isOpen, onToggleMenu]);
  const passedChildren =
    typeof children === 'function'
      ? children({
          items: filteredItems,
          itemsMap,
          input: searchTerm,
          highlightedIndex,
          shouldHighlighted,
        })
      : children;

  const defaultComponent = (
    <div className={classes.menu}>
      <div className={classesForDefault.menuItem}>{titleDefault}</div>
    </div>
  );

  const toggleMenu = useCallback(() => {
    if (!notEditable) {
      if (isOpen) {
        closeMenu();
      } else {
        openMenu();
        if (inputRef.current) {
          inputRef.current.select();
        }
      }
    }
  }, [notEditable, isOpen, closeMenu, openMenu]);

  const { onKeyDown, ...menuProps } = getMenuProps() as HTMLProps<HTMLDivElement>;

  const handleKeyDownMenu = useCallback(
    (e: KeyboardEvent<HTMLDivElement>) => {
      e.stopPropagation();
      if (onKeyDown) {
        onKeyDown(e);
      }
    },
    [onKeyDown],
  );

  return (
    <>
      <FieldGroup
        {...rest}
        {...getComboboxProps({ ref: setRef })}
        active={active}
        setActive={() => {}}
        readonly={derivedReadonly}
        disabled={derivedDisabled}
        className={classnames(className, classes.field)}
        onClick={toggleMenu}
      >
        <Input
          type="text"
          disabled={derivedDisabled}
          {...getInputProps({ id, onKeyDown: handleEnter, ref: inputRef })}
          placeholder={placeholder}
          onChange={setInputValue}
          className={classes.input}
          readonly={derivedReadonly}
        />
        {searchTerm.length > 0 && !notEditable && canReset && <ResetButton onClick={reset} />}
        {!derivedReadonly && (
          <Icon
            src={isOpen ? arrow_up : arrow_down}
            {...getToggleButtonProps({ disabled: notEditable })}
            className={classnames(classes.trigger, {
              [classes.open]: isOpen,
            })}
            dataTestId="triggerIcon"
          />
        )}
        {autosuggestion && !!suggestViewableText && (
          <div className={classes.autosuggestionWrapper} data-testid="autoSuggestions">
            <span className={classes.autosuggestionTransparent}>{searchTerm}</span>
            <span className={classes.autosuggestion}>{suggestViewableText}</span>
          </div>
        )}
      </FieldGroup>
      <Popper
        open={isOpen}
        options={popperOptions}
        referenceElement={pulldownContainer || ref.current}
        className={classes.popper}
      >
        <div {...menuProps} onKeyDown={handleKeyDownMenu} role="menu" tabIndex={-1}>
          <Items
            className={classes.menu}
            itemsMap={itemsMap}
            selected={selectedItem}
            highlightedIndex={highlightedIndex}
            getItemProps={getItemProps}
            options={options}
            highlightSearchResult={shouldHighlighted}
            searchValue={searchTerm}
          >
            {!passedChildren && filteredItems.length === 0 ? defaultComponent : passedChildren}
          </Items>
        </div>
      </Popper>
    </>
  );
};

const SingleSelectComboBox = forwardRef(SingleSelectComboboxInner) as <T extends ItemType>(
  props: ComboBoxSingleSelectProps<T> & { ref?: ForwardedRef<HTMLUListElement> },
) => ReturnType<typeof SingleSelectComboboxInner>;

export { SingleSelectComboBox };
