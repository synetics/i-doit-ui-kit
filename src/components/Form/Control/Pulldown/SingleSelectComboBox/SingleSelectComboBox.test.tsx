import { fireEvent, render, screen } from '@testing-library/react';
import React from 'react';
import user from '@testing-library/user-event';
import { act } from 'react-dom/test-utils';
import { uniqueId } from '../../../../../utils';
import { SingleSelectComboBox } from './SingleSelectComboBox';

const defaultItems = [
  { value: '1', label: 'One Item' },
  { value: '2', label: 'Two' },
];

describe('<SingleSelectComboBox />', () => {
  it('renders without crashing', () => {
    render(<SingleSelectComboBox id={uniqueId()} onChange={() => {}} value={null} items={[]} />);

    const trigger = screen.getByTestId('triggerIcon');
    expect(trigger).toBeInTheDocument();
    const menu = screen.queryByRole('menu');
    expect(menu).not.toBeInTheDocument();
  });

  it('renders with placeholder', () => {
    render(
      <SingleSelectComboBox id={uniqueId()} onChange={() => {}} value={null} items={[]} placeholder="Placeholder" />,
    );

    expect(screen.getByPlaceholderText('Placeholder')).toBeInTheDocument();
  });

  it('renders with children as Render props', async () => {
    render(
      <SingleSelectComboBox id={uniqueId()} onChange={() => {}} value={null} items={[]} placeholder="Placeholder">
        {() => <div>children</div>}
      </SingleSelectComboBox>,
    );
    await user.click(screen.getByTestId('triggerIcon'));
    expect(screen.getByText('children')).toBeInTheDocument();
  });

  it('opens  menu with items', async () => {
    const handleChange = jest.fn();
    render(
      <SingleSelectComboBox
        id={uniqueId()}
        highlightSearchResult={false}
        items={[
          { value: '1', label: 'One' },
          { value: '2', label: 'Two' },
        ]}
        value={null}
        onChange={handleChange}
        onInputChange={() => {}}
      />,
    );

    expect(screen.queryByText('One')).not.toBeVisible();
    expect(screen.queryByText('Two')).not.toBeVisible();
    const button = screen.getByTestId('triggerIcon');
    await user.click(button);

    expect(screen.queryByText('One')).toBeInTheDocument();
    expect(screen.queryByText('Two')).toBeInTheDocument();

    await user.click(screen.getByText('One'));

    expect(screen.queryByText('One')).not.toBeVisible();
    expect(screen.queryByText('Two')).not.toBeVisible();

    expect(handleChange).toHaveBeenCalledTimes(1);
    expect(handleChange).toHaveBeenCalledWith({ value: '1', label: 'One' });
  });

  it('reacts with keyboard', async () => {
    const handleChange = jest.fn();
    render(
      <SingleSelectComboBox
        highlightSearchResult={false}
        id={uniqueId()}
        items={[
          { value: '1', label: 'One' },
          { value: '2', label: 'Two' },
        ]}
        value={null}
        onChange={handleChange}
        onInputChange={() => {}}
      />,
    );
    const trigger = screen.getByTestId('triggerIcon');

    await user.type(trigger, '{ARROWDOWN}');
    expect(screen.getByText('One')).toBeInTheDocument();
    expect(screen.getByText('Two')).toBeInTheDocument();
    const li = screen.getByText('Two');
    await user.type(li, '{enter}');
    expect(screen.queryByText('One')).not.toBeVisible();
    expect(screen.queryByText('Two')).not.toBeVisible();

    expect(handleChange).toHaveBeenCalledWith({ value: '2', label: 'Two' });
  });

  it('renders reset button if input not empty', async () => {
    render(<SingleSelectComboBox id={uniqueId()} items={[]} value={null} onChange={() => {}} />);
    const input = screen.getByRole('textbox');

    fireEvent.change(input, { target: { value: 'input-change-1' } });
    expect(input).toHaveValue('input-change-1');

    const button = screen.queryByTestId('reset-button');
    expect(button).toBeInTheDocument();
  });

  it('no reset button if input not empty and canReset is set to false', () => {
    render(
      <SingleSelectComboBox
        id={uniqueId()}
        items={[
          { value: '1', label: 'One' },
          { value: '2', label: 'Two' },
        ]}
        value={{ value: '1', label: 'One' }}
        onChange={() => {}}
        canReset={false}
      />,
    );
    const input = screen.getByRole('textbox');

    fireEvent.change(input, { target: { value: '2' } });
    expect(input).toHaveValue('2');

    const button = screen.queryByTestId('reset-button');
    expect(button).not.toBeInTheDocument();
  });

  it('opens the pulldown on input', async () => {
    const handleChange = jest.fn();
    render(
      <SingleSelectComboBox
        id={uniqueId()}
        items={defaultItems}
        value={null}
        onChange={handleChange}
        highlightSearchResult={false}
      />,
    );
    const input = screen.getByRole('textbox');

    await user.type(input, 'one');

    expect(screen.queryByText('One Item')).toBeInTheDocument();
    expect(screen.queryByText('Two')).not.toBeInTheDocument();
  });

  it('opens the pulldown on click', async () => {
    const handleChange = jest.fn();
    render(
      <SingleSelectComboBox
        id={uniqueId()}
        items={defaultItems}
        value={null}
        onChange={handleChange}
        highlightSearchResult={false}
      />,
    );
    const input = screen.getByRole('textbox');

    await user.click(input);

    expect(screen.queryByText('One Item')).toBeInTheDocument();
    expect(screen.queryByText('Two')).toBeInTheDocument();
  });

  it('check if the text in the input is selected', async () => {
    const handleChange = jest.fn();

    render(
      <SingleSelectComboBox
        id="combobox"
        items={[
          { value: '1', label: 'One' },
          { value: '2', label: 'Two' },
        ]}
        value={{ value: '1', label: 'One' }}
        onChange={handleChange}
        titleDefault="No results are found"
      />,
    );
    const input: HTMLInputElement = screen.getByTestId('combobox');

    await user.click(input);

    expect(input).toHaveClass('active');

    const { selectionStart, selectionEnd } = input;

    expect(input).toHaveValue('One');
    expect(selectionStart).toBe(0);
    expect(selectionEnd).toBe('One'.length);
  });

  it('opens and closes the pulldown on trigger click', async () => {
    const handleChange = jest.fn();
    render(
      <SingleSelectComboBox
        id={uniqueId()}
        items={defaultItems}
        value={null}
        onChange={handleChange}
        highlightSearchResult={false}
      />,
    );
    const triggerIcon = screen.getByTestId('triggerIcon');

    await user.click(triggerIcon);

    expect(screen.queryByText('One Item')).toBeVisible();

    await user.click(triggerIcon);

    expect(screen.queryByText('One Item')).not.toBeVisible();
  });

  it('click on reset button', async () => {
    render(<SingleSelectComboBox id={uniqueId()} items={[]} value={null} onChange={() => {}} />);
    const input = screen.getByRole('textbox');

    await user.type(input, 'input-change-1');

    const button = screen.getByTestId('reset-button');

    await user.click(button);
    expect(input).toHaveValue('');
  });

  it('check searching result with default threshold', async () => {
    render(
      <SingleSelectComboBox
        id={uniqueId()}
        items={defaultItems}
        value={null}
        onChange={() => {}}
        highlightSearchResult={false}
      />,
    );
    const input = screen.getByRole('textbox');

    await user.type(input, '{ARROWDOWN}');
    const secondItem = screen.getByText('Two');
    expect(secondItem).toBeInTheDocument();
    await act(() => fireEvent.change(input, { target: { value: 'One Item' } }));
    expect(screen.getByText('One Item')).toBeInTheDocument();
    expect(secondItem).not.toBeInTheDocument();
  });

  it('check searching result without result', async () => {
    render(
      <SingleSelectComboBox
        id={uniqueId()}
        items={defaultItems}
        value={null}
        onChange={() => {}}
        titleDefault="No results are found"
      />,
    );
    const input = screen.getByRole('textbox');

    await user.type(input, '{ARROWDOWN}');

    await act(() => fireEvent.change(input, { target: { value: 'Three' } }));

    expect(screen.getByText('No results are found')).toBeInTheDocument();
  });

  it('renders preselected item', () => {
    render(
      <SingleSelectComboBox
        id={uniqueId()}
        items={defaultItems}
        value={defaultItems[0]}
        onChange={() => {}}
        titleDefault="No results are found"
      />,
    );
    const input = screen.getByRole('textbox');
    expect(input).toHaveValue(defaultItems[0].label);
  });

  it('resets value on clearing value', async () => {
    const onChange = jest.fn();
    const item = { value: 'one', label: 'One' };
    render(
      <SingleSelectComboBox
        id={uniqueId()}
        items={[item]}
        value={item}
        onChange={onChange}
        titleDefault="No results are found"
      />,
    );
    const input = screen.getByRole('textbox');

    await user.type(input, '{BACKSPACE}{BACKSPACE}{BACKSPACE}');
    expect(input).toHaveValue('');
    expect(onChange).toHaveBeenCalledWith(null);
  });

  it('check searching result with threshold value = 2', async () => {
    render(
      <SingleSelectComboBox
        id={uniqueId()}
        items={defaultItems}
        value={null}
        threshold={2}
        onChange={() => {}}
        highlightSearchResult={false}
      />,
    );
    const input = screen.getByRole('textbox');

    await user.type(input, '{ARROWDOWN}');
    const firstItem = screen.getByText('One Item');
    const secondItem = screen.getByText('Two');
    expect(secondItem).toBeInTheDocument();

    await act(() => fireEvent.change(input, { target: { value: 'O' } }));
    expect(firstItem).toBeInTheDocument();
    expect(secondItem).toBeInTheDocument();

    await act(() => fireEvent.change(input, { target: { value: 'One Item' } }));

    expect(firstItem).toBeInTheDocument();
    expect(secondItem).not.toBeInTheDocument();
  });

  it('renders with the custom value', async () => {
    const view = <div data-testid="view">View</div>;
    render(
      <SingleSelectComboBox
        id={uniqueId()}
        onChange={() => {}}
        items={[{ value: 'value', label: 'Label', view }]}
        value={null}
        placeholder="Placeholder"
      />,
    );

    const input = screen.getByRole('textbox');

    await user.type(input, '{ARROWDOWN}');

    expect(screen.getByTestId('view')).toBeInTheDocument();
    expect(screen.getByTestId('view')).toHaveTextContent('View');
  });

  it('closes menu with esc key and not open after next escape press', async () => {
    render(
      <SingleSelectComboBox
        id={uniqueId()}
        onChange={() => {}}
        items={[{ value: 'value', label: 'Label' }]}
        value={null}
        placeholder="Placeholder"
        highlightSearchResult={false}
      />,
    );

    const input = screen.getByRole('textbox');

    await user.type(input, '{ARROWDOWN}');

    expect(screen.getByText('Label')).toBeInTheDocument();

    await user.type(input, '{ESCAPE}');

    expect(screen.queryByText('Label')).not.toBeVisible();
    await user.type(input, '{ESCAPE}');

    expect(screen.queryByText('Label')).not.toBeVisible();
  });

  it('call onToggleMenu', async () => {
    const onToggleMenu = jest.fn();
    render(
      <SingleSelectComboBox
        id={uniqueId()}
        onChange={() => {}}
        items={[{ value: 'value', label: 'Label' }]}
        value={null}
        placeholder="Placeholder"
        onToggleMenu={onToggleMenu}
      />,
    );

    const input = screen.getByRole('textbox');
    await user.type(input, '{arrowdown}');
    expect(onToggleMenu).toHaveBeenCalled();
  });

  it('clearInputAfterSelection', async () => {
    render(
      <SingleSelectComboBox
        id={uniqueId()}
        onChange={() => {}}
        items={[{ value: 'value', label: 'Label' }]}
        value={null}
        placeholder="Placeholder"
        clearInputAfterSelection={false}
        highlightSearchResult={false}
      />,
    );
    const input = screen.getByRole('textbox');
    await user.type(input, 'Lab');
    expect(input).toHaveValue('Lab');
    await user.type(input, '{arrowdown}');
    await user.click(screen.getByText('Label'));
    expect(input).toHaveValue('Lab');
  });

  it('check autosuggestion', async () => {
    render(
      <SingleSelectComboBox
        id={uniqueId()}
        onChange={() => {}}
        items={[
          { value: 'value', label: 'One Item' },
          { value: 'value2', label: 'One Item2' },
        ]}
        value={null}
        placeholder="Placeholder"
        threshold={1}
        autosuggestion
      />,
    );
    const input = screen.getByRole('textbox');
    expect(screen.queryByTestId('autoSuggestions')).not.toBeInTheDocument();
    await user.type(input, 'One It');
    expect(input).toHaveValue('One It');
    expect(screen.getByText('One It')).toBeInTheDocument();
    expect(screen.getByText('em')).toBeInTheDocument();
    await user.type(input, '{arrowright}');
    expect(input).toHaveValue('One Item');
  });
});
