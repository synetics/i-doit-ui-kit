import React, { SyntheticEvent, useCallback } from 'react';
import { Icon } from '../../../../Icon';
import { close } from '../../../../../icons';
import { IconButton } from '../../../../IconButton';
import styles from './ResetButton.module.scss';

type ResetButtonType = {
  onClick: () => void;
  label?: string;
  tabIndex?: number;
};

const ResetButton = ({ label = 'Reset', tabIndex, onClick }: ResetButtonType) => {
  const handleClick = useCallback(
    (e: SyntheticEvent) => {
      onClick();
      e.stopPropagation();
    },
    [onClick],
  );
  return (
    <IconButton
      tabIndex={tabIndex}
      data-testid="reset-button"
      className="mx-1"
      iconClassName={styles.iconTrigger}
      label={label}
      variant="text"
      icon={<Icon src={close} />}
      onClick={handleClick}
    />
  );
};

export { ResetButton, ResetButtonType };
