import styles from './SelectedColor.module.scss';

type SelectedColorType = {
  color?: string | number;
};

const SelectedColor = ({ color = '000' }: SelectedColorType) => (
  <div className={styles.transparentContainer}>
    <span
      className={styles.selectedColor}
      style={{
        background: `#${color}`,
      }}
      data-testid="selected-color"
    />
  </div>
);

export default SelectedColor;
