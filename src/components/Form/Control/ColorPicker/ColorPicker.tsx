import React, {
  ComponentType,
  forwardRef,
  InputHTMLAttributes,
  MouseEvent,
  SyntheticEvent,
  useCallback,
  useRef,
  useState,
} from 'react';
import { ChromePicker, ColorResult } from 'react-color';
import { palette } from '../../../../icons';
import { useControlledState, useEventListener, useFocusOut, useGlobalizedRef } from '../../../../hooks';
import { handleKey, toHexCode, uniqueId } from '../../../../utils';
import { Popper, SameWidthInlineModifier } from '../../../Popper';
import { IconButton } from '../../../IconButton';
import { Icon } from '../../../Icon';
import { FieldExtra, FieldGroup, FieldProps, useActive, useFieldGroup, ValueProps } from '../../Infrastructure';
import { Input, InputProps } from '../Input';
import SelectedColor from './SelectedColor';
import styles from './ColorPicker.module.scss';

const MAX_HEX_COLOR_LENGTH = 6;

type ColorPickerProps = FieldProps &
  ValueProps<string> &
  Omit<InputHTMLAttributes<HTMLInputElement>, 'value' | 'onChange' | 'prefix' | 'type'> & {
    control?: ComponentType<Omit<InputProps, 'component'>>;
    openLabel?: string;
    closeLabel?: string;
    adoptiveParentsSelectors?: string[];
  };

const popperOptions = {
  modifiers: [
    {
      name: 'offset',
      options: {
        offset: [0, 4],
      },
    },
    SameWidthInlineModifier,
  ],
};

const ColorPicker = forwardRef<HTMLInputElement, ColorPickerProps>(
  (
    {
      active: passedActive,
      setActive: passedSetActive,
      disabled,
      readonly,
      onChange,
      id,
      className,
      value: initialColor = '000',
      placeholder = 'Choose a color',
      control,
      openLabel = 'Open color picker',
      closeLabel = 'Close color picker',
      adoptiveParentsSelectors = [],
      ...props
    },
    ref,
  ) => {
    const { setRefsCallback: setReferenceElement } = useGlobalizedRef<HTMLInputElement>(ref);
    const wrapperRef = useRef<HTMLDivElement | null>(null);
    const [innerId] = useState(id || uniqueId('idoit-colorPicker'));
    const { value: active } = useActive(passedActive, passedSetActive);
    const [show, setShow] = useState(false);
    const [color, setColor] = useControlledState<string>(initialColor, onChange);
    const { readonly: derivedReadonly, disabled: derivedDisabled } = useFieldGroup({ disabled, readonly });
    const Control = control || Input;

    const hidePopper = useCallback(() => {
      setShow(false);
    }, [setShow]);

    const showPopper = useCallback(() => {
      setShow(true);
    }, [setShow]);

    useFocusOut(wrapperRef, hidePopper, { adoptiveParentsSelectors: ['.flexbox-fix', ...adoptiveParentsSelectors] });

    useEventListener(
      'keydown',
      handleKey<SyntheticEvent>({
        Escape: hidePopper,
        ArrowDown: showPopper,
      }),
      wrapperRef.current as Element,
    );

    const handleChange = useCallback(
      (value: string | number) => {
        setColor(toHexCode(value.toString()));
      },
      [setColor],
    );

    const handleColorChange = useCallback(
      (value: ColorResult) => {
        handleChange(value.hex);
      },
      [handleChange],
    );

    const handleToggle = useCallback(
      (e: MouseEvent) => {
        setShow(!show);
        e.stopPropagation();
      },
      [setShow, show],
    );

    return (
      <>
        <FieldGroup
          {...props}
          className={className}
          ref={wrapperRef}
          disabled={derivedDisabled}
          readonly={derivedReadonly}
          active={active}
        >
          <FieldExtra>
            <SelectedColor color={color} />
          </FieldExtra>
          <Control
            {...props}
            ref={setReferenceElement}
            id={innerId}
            type="text"
            format={toHexCode}
            onChange={handleChange}
            maxLength={MAX_HEX_COLOR_LENGTH}
            value={color}
            placeholder={placeholder}
          />
          <FieldExtra>
            <IconButton
              disabled={derivedDisabled}
              icon={<Icon src={palette} />}
              onClick={handleToggle}
              onMouseDown={handleToggle}
              data-testid="icon-palette"
              label={!show ? openLabel : closeLabel}
              variant="text"
              activeState={show}
            />
          </FieldExtra>
        </FieldGroup>
        {show && (
          <Popper open={show} referenceElement={wrapperRef.current} dataTestId="colorPicker" options={popperOptions}>
            <ChromePicker
              color={`#${color}`}
              className={styles.iChromePicker}
              onChange={handleColorChange}
              disableAlpha
            />
          </Popper>
        )}
      </>
    );
  },
);

export { ColorPicker, ColorPickerProps };
