import { act, fireEvent, render as reactRender, screen } from '@testing-library/react';
import user from '@testing-library/user-event';
import { createRenderer } from '../../../../../test/utils/renderer';
import { ColorPicker } from './ColorPicker';

const render = createRenderer(ColorPicker, {});

const delay = async (time: number) => new Promise((resolve) => setTimeout(resolve, time));

describe('<ColorPicker />', () => {
  it('renders an input, a palette icon and a selected color (enabled)', () => {
    render();

    expect(screen.getByPlaceholderText('Choose a color')).toHaveValue('000');
    expect(screen.getByTestId('icon-palette')).toBeInTheDocument(); // test class name
    expect(screen.queryByTestId('selected-color')).toBeInTheDocument();
  });

  it('renders predefined value (regular)', () => {
    const props = { value: 'CCC' };
    render(props);

    expect(screen.getByDisplayValue('CCC')).toBeInTheDocument();
    expect(screen.getByTestId('selected-color')).toHaveStyle({ background: `#${props.value}` });
  });

  it('renders disabled', async () => {
    const props = { disabled: true };
    render(props);

    // click on button
    await user.click(screen.getByTestId('icon-palette'));
    expect(screen.queryByTestId('colorPicker')).not.toBeInTheDocument();

    // focus on the input
    act(() => screen.getByPlaceholderText('Choose a color').focus());
    await delay(100);
    expect(screen.queryByTestId('colorPicker')).not.toBeInTheDocument();
  });

  it('allows to type a hex value', async () => {
    render();

    const input = screen.getByPlaceholderText('Choose a color');

    await user.clear(input);

    await user.type(input, 'aaa');
    expect(input).toHaveValue('AAA');
    await user.clear(input);

    await user.type(input, 'aaaaaaa');
    expect(input).toHaveValue('AAAAAA');
    await user.clear(input);

    await user.type(input, '#aaa');
    expect(input).toHaveValue('AAA');
    await user.clear(input);

    await user.type(input, '#aZbZcZ');
    expect(input).toHaveValue('ABC');
    await user.clear(input);
  });

  it('allows to pick a value', async () => {
    render();
    const iconButton = screen.getByTestId('icon-palette');

    await act(() => fireEvent.mouseDown(iconButton));
    expect(screen.getByDisplayValue('#000000')).toBeInTheDocument();
  });

  it('renders popper inside element with class', async () => {
    reactRender(
      <div className="control-wrapper">
        <ColorPicker />
      </div>,
    );

    const iconButton = screen.getByTestId('icon-palette');

    await act(() => fireEvent.mouseDown(iconButton));
    expect(screen.getByDisplayValue('#000000')).toBeInTheDocument();
  });

  it('closes color picker by click outside of it', async () => {
    render();

    const iconButton = screen.getByTestId('icon-palette');

    await act(() => fireEvent.mouseDown(iconButton));

    const colorPicker = screen.getByTestId('colorPicker');
    expect(colorPicker).toHaveAttribute('data-open', 'true');

    await user.click(document.body);
    await delay(100);
    expect(colorPicker).not.toBeInTheDocument();
  });

  it('closes color picker by pressing the Escape key', async () => {
    render();
    const input = screen.getByPlaceholderText('Choose a color');
    const iconButton = screen.getByTestId('icon-palette');

    await act(() => fireEvent.mouseDown(iconButton));

    const colorPicker = screen.getByTestId('colorPicker');

    expect(colorPicker).toBeInTheDocument();
    await user.type(input, '{escape}');
    await delay(100);
    expect(colorPicker).not.toBeInTheDocument();
  });

  it('closes color picker when the user moves focus to prev element', async () => {
    render();
    const iconButton = screen.getByTestId('icon-palette');

    reactRender(<button>Click me</button>);

    await act(() => fireEvent.mouseDown(iconButton));

    const colorPicker = screen.getByTestId('colorPicker');

    expect(colorPicker).toHaveAttribute('data-open', 'true');

    act(() => screen.getByText('Click me').focus());
    await delay(100);
    expect(colorPicker).not.toBeInTheDocument();
  });

  it('toggles the color picker by click on the palette icon', async () => {
    render();
    const iconButton = screen.getByTestId('icon-palette');

    // opens by click
    await act(() => fireEvent.mouseDown(iconButton));

    const colorPicker = screen.getByTestId('colorPicker');

    expect(colorPicker).toBeInTheDocument();

    // closes by click
    await user.click(iconButton);
    expect(colorPicker).not.toBeInTheDocument();
  });

  it('opens by arrowDown', async () => {
    render();
    const input = screen.getByPlaceholderText('Choose a color');

    await user.type(input, 'aaa');
    await user.type(input, '{arrowdown}');

    const colorPicker = screen.getByTestId('colorPicker');
    expect(colorPicker).toBeInTheDocument();
  });

  it('picks a value with color picker', async () => {
    render();
    const input = screen.getByPlaceholderText('Choose a color');
    const iconButton = screen.getByTestId('icon-palette');
    await act(() => fireEvent.mouseDown(iconButton));
    const rcInput = screen.getByLabelText('hex');

    await user.clear(rcInput);
    await user.type(rcInput, '222222');
    expect(input).toHaveValue('222222');
  });

  it('renders custom placeholder and labels', async () => {
    const placeholder = 'Pick the color';
    const props = { placeholder, openLabel: 'Please open', closeLabel: 'Please close' };
    render(props);
    const iconButton = screen.getByTestId('icon-palette');

    screen.getByPlaceholderText(placeholder);

    await user.hover(iconButton);
    screen.getAllByText('Please open');

    await act(() => fireEvent.mouseDown(iconButton));
    await user.hover(iconButton);
    screen.getAllByText('Please close');
  });
});
