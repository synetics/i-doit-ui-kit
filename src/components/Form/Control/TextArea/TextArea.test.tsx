import { act, fireEvent, render, screen } from '@testing-library/react';
import { createRef } from 'react';
import { TextArea } from './TextArea';

const delay = (time: number): Promise<void> => new Promise<void>((resolve) => setTimeout(resolve, time));

describe('TextArea', () => {
  it('renders without crashing', () => {
    render(<TextArea />);

    expect(screen.getByTestId('textArea')).toBeInTheDocument();
  });

  it('renders uncontrolled', async () => {
    render(<TextArea />);
    const textArea = screen.getByTestId('textArea');

    await act(() => fireEvent.change(textArea, { target: { value: 'input-change-1' } }));
    expect(textArea).toHaveValue('input-change-1');

    await act(() => fireEvent.change(textArea, { target: { value: 'input-change-2' } }));
    expect(textArea).toHaveValue('input-change-2');
  });

  it('renders initialValue', () => {
    render(<TextArea value="initial-value" />);
    const textArea = screen.getByTestId('textArea');

    expect(textArea).toHaveValue('initial-value');
  });

  it('calls onChange callback', async () => {
    const onChange = jest.fn();
    render(<TextArea onChange={onChange} />);
    const textArea = screen.getByTestId('textArea');

    await act(() => fireEvent.change(textArea, { target: { value: 'input-change-1' } }));
    expect(onChange).toHaveBeenCalledWith('input-change-1');
  });

  it('renders with given attributes', () => {
    const attributes = {
      placeholder: 'initial-placeholder',
      name: 'name',
    };

    render(<TextArea {...attributes} />);
    const textArea = screen.getByTestId('textArea');

    Object.entries(attributes).forEach(([attribute, value]) => {
      expect(textArea).toHaveAttribute(attribute, value);
    });
  });

  it('renders with class', () => {
    render(<TextArea className="dummy-class" />);
    const textArea = screen.getByTestId('textArea');

    expect(textArea).toHaveAttribute('class');
    expect(textArea).toHaveClass('dummy-class');
  });

  it('renders with maxLength', () => {
    render(<TextArea maxLength={400} rows={3} />);
    const textArea = screen.getByTestId('textArea');

    expect(textArea).toHaveAttribute('maxLength', '400');
    expect(textArea).toHaveAttribute('rows', '3');
  });

  it('renders with noLength', () => {
    render(<TextArea maxLength={-1} />);
    const textArea = screen.queryByTestId('textAreaUnderline');

    expect(textArea).not.toBeInTheDocument();
  });

  it('uses ref', () => {
    const ref = createRef<HTMLTextAreaElement>();

    render(<TextArea type="text" ref={ref} />);
    expect(ref.current).toBeInstanceOf(HTMLTextAreaElement);
  });

  it('calls onFocus callback', async () => {
    const onFocus = jest.fn();
    render(<TextArea id="idoit-input" type="text" onFocus={onFocus} />);
    const textArea = screen.getByTestId('textArea');

    act(() => textArea.focus());

    expect(onFocus).toHaveBeenCalled();
  });

  it('calls onBlur callback', async () => {
    const onBlur = jest.fn();
    render(<TextArea type="text" onBlur={onBlur} />);
    const textArea = screen.getByTestId('textArea');

    await act(() => fireEvent.blur(textArea, { target: {} }));

    expect(onBlur).toHaveBeenCalled();
  });

  it('is active with autoFocus', async () => {
    render(<TextArea type="text" dataTestId="area" autoFocus />);
    const textArea = screen.getByTestId('area');

    await delay(0);

    expect(textArea).toHaveClass('active');
  });
});
