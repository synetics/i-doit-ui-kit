import React, {
  ChangeEvent,
  forwardRef,
  InputHTMLAttributes,
  RefAttributes,
  useCallback,
  useEffect,
  useState,
} from 'react';
import classnames from 'classnames';
import { useControlledState, useGlobalizedRef } from '../../../../hooks';
import { uniqueId } from '../../../../utils';
import { FieldProps, FormField, Underline, useActive, useFieldGroup, ValueProps } from '../../Infrastructure';
import styles from './TextArea.module.scss';

type TextAreaProps = FieldProps &
  ValueProps<string> &
  RefAttributes<HTMLTextAreaElement> &
  InputHTMLAttributes<HTMLTextAreaElement> & {
    fixed?: boolean;
    cols?: number;
    rows?: number;
    maxLength?: number;
    dataTestId?: string;
  };

const TextArea = forwardRef<HTMLTextAreaElement, TextAreaProps>(
  (
    {
      name,
      onChange,
      fixed,
      cols,
      value = '',
      required,
      disabled,
      active,
      setActive,
      onFocus,
      onBlur,
      className,
      readonly,
      rows = 1,
      type,
      maxLength = 300,
      placeholder = 'Type a description',
      id,
      autoFocus,
      dataTestId,
      ...props
    },
    ref,
  ) => {
    const { localRef: currentRef, setRefsCallback } = useGlobalizedRef<HTMLTextAreaElement>(ref);
    const [text, setText] = useControlledState<string>(value, onChange);
    const { value: derivedActive, setValue: setDerivedActive } = useActive(active || false, setActive);
    const {
      readonly: derivedReadonly,
      disabled: derivedDisabled,
      required: derivedRequired,
    } = useFieldGroup({
      disabled,
      readonly,
      required,
    });

    const [innerId] = useState(id || uniqueId('idoit-textarea'));
    const handleChange = useCallback(
      (event: ChangeEvent<HTMLTextAreaElement>) => {
        setText(event.target.value);
      },
      [setText],
    );

    useEffect(() => {
      if (autoFocus) {
        const timeOutId = setTimeout(() => {
          setDerivedActive(true);
          currentRef.current?.focus();
        }, 0);
        return () => clearTimeout(timeOutId);
      }
      return undefined;
    }, [autoFocus, setDerivedActive, currentRef]);

    const handleFocus = useCallback(
      (event: React.FocusEvent<HTMLTextAreaElement>) => {
        if (onFocus) {
          onFocus(event);
        }

        setDerivedActive(true);
      },
      [onFocus, setDerivedActive],
    );

    const handleBlur = useCallback(
      (event: React.FocusEvent<HTMLTextAreaElement>) => {
        if (onBlur) {
          onBlur(event);
        }

        setDerivedActive(false);
      },
      [onBlur, setDerivedActive],
    );

    const currentTextCounter = maxLength > 0 ? `${text.length} / ${maxLength}` : '';

    useEffect(() => {
      if (currentRef && currentRef.current) {
        currentRef.current.style.height = `${currentRef.current.scrollHeight}px`;
      }
    }, [value, currentRef]);

    return (
      <FormField
        {...props}
        className={classnames(className, styles.textAreaContainer)}
        active={!derivedReadonly && derivedActive}
        disabled={derivedDisabled}
        readonly={derivedReadonly}
        dataTestId={dataTestId}
      >
        <textarea
          ref={setRefsCallback}
          name={name}
          className={classnames(className, styles.textarea, { [styles.noResize]: fixed || derivedDisabled })}
          onChange={handleChange}
          onFocus={handleFocus}
          onBlur={handleBlur}
          placeholder={placeholder}
          maxLength={maxLength}
          cols={cols}
          rows={rows}
          value={text}
          required={derivedRequired}
          disabled={derivedDisabled}
          readOnly={derivedReadonly}
          data-testid="textArea"
        />
        {currentTextCounter && <Underline id={innerId} right={currentTextCounter} data-testid="textAreaUnderline" />}
      </FormField>
    );
  },
);

export { TextArea, TextAreaProps };
