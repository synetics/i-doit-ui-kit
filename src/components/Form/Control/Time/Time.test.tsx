import { fireEvent, render, screen } from '@testing-library/react';
import { Time } from './Time';

describe('Time', () => {
  it('renders without crashing', () => {
    render(<Time id="time" value="10:30" />);

    expect(screen.getByDisplayValue('10:30')).toBeInTheDocument();
  });

  it('renders as controlled', () => {
    const onChange = jest.fn();
    render(<Time id="time" value="10:30" onChange={onChange} />);

    const input = screen.getByDisplayValue('10:30');
    fireEvent.change(input, { target: { value: '11:55' } });
    expect(onChange).toHaveBeenCalledWith('11:55');
  });

  it('renders with right states', () => {
    render(<Time id="time" value="10:30" data-testid="time" />);

    const group = screen.getByTestId('time');
    const input = screen.getByDisplayValue('10:30');
    expect(group).toHaveClass('has-border');
    fireEvent.focus(input);
    expect(group).toHaveClass('active');
    fireEvent.blur(input);
    expect(group).not.toHaveClass('active');
  });
});
