import React, { forwardRef } from 'react';
import { ValueProps } from '../../Infrastructure';
import { Input, InputProps } from '../Input';

type TimeType = Omit<InputProps, 'type'> & ValueProps<string>;

const Time = forwardRef<HTMLInputElement, TimeType>((props, ref) => <Input {...props} ref={ref} type="time" />);

export { Time, TimeType };
