import React, { ForwardedRef, forwardRef, useCallback } from 'react';
import { FieldExtra, FieldGroup, ValueProps } from '../../Infrastructure';
import { Input, InputProps } from '../Input';
import { IconButton } from '../../../IconButton';
import { Icon } from '../../../Icon';
import { eye, eye_slash } from '../../../../icons';
import { useControlledState } from '../../../../hooks';

type PasswordType = Omit<InputProps, 'type'> &
  ValueProps<string> & {
    visible?: boolean;
    setVisible?: (v: boolean) => void;
    suffix?: string;
    /**
     * Class name for popper
     */
    popperClassName?: string;
  };

const Password = forwardRef(
  (
    {
      id,
      value,
      onChange,
      visible = false,
      setVisible,
      'aria-label': ariaLabel,
      disabled,
      autoFocus,
      required,
      name,
      className,
      readOnly,
      placeholder,
      onFocus,
      onBlur,
      rtl,
      format,
      suffix,
      popperClassName,
      ...props
    }: PasswordType,
    ref: ForwardedRef<HTMLInputElement>,
  ) => {
    const [derivedVisible, setDerivedVisible] = useControlledState(visible, setVisible);
    const toggle = useCallback(() => setDerivedVisible(!derivedVisible), [derivedVisible, setDerivedVisible]);
    const label = derivedVisible ? 'Hide password' : 'Show password';
    return (
      <FieldGroup {...props} data-testid="password-group">
        <Input
          {...props}
          id={id}
          autoFocus={autoFocus}
          aria-label={ariaLabel}
          ref={ref}
          className={className}
          disabled={disabled}
          readOnly={readOnly}
          required={required}
          name={name}
          type={derivedVisible ? 'text' : 'password'}
          value={value}
          placeholder={placeholder}
          onChange={onChange}
          onFocus={onFocus}
          onBlur={onBlur}
          rtl={rtl}
          format={format}
        />
        <FieldExtra>{suffix}</FieldExtra>
        <FieldExtra>
          <IconButton
            variant="text"
            type="button"
            label={label}
            aria-label={label}
            icon={<Icon src={derivedVisible ? eye_slash : eye} />}
            onClick={() => toggle()}
            disabled={disabled}
            popperClassName={popperClassName}
          />
        </FieldExtra>
      </FieldGroup>
    );
  },
);

export { Password, PasswordType };
