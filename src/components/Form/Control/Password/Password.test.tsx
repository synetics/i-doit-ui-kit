import { fireEvent, render, screen } from '@testing-library/react';
import { Password } from './Password';

describe('Password', () => {
  it('renders without crashing', () => {
    render(<Password id="password" value="mysecreptpassword" />);

    expect(screen.getByDisplayValue('mysecreptpassword')).toBeInTheDocument();
  });

  it('renders with button', () => {
    render(<Password id="password" value="mysecreptpassword" />);

    expect(screen.getByRole('button')).toBeInTheDocument();
  });

  it('renders with button and toggle feature', () => {
    render(<Password id="password" value="mysecreptpassword" />);

    const input = screen.getByDisplayValue('mysecreptpassword');
    const button = screen.getByRole('button');
    expect(input).toHaveAttribute('type', 'password');
    expect(button).toHaveAttribute('aria-label', 'Show password');
    fireEvent.click(button);
    expect(input).toHaveAttribute('type', 'text');
    expect(button).toHaveAttribute('aria-label', 'Hide password');
    fireEvent.click(button);
    expect(input).toHaveAttribute('type', 'password');
    expect(button).toHaveAttribute('aria-label', 'Show password');
  });

  it('renders as controlled', () => {
    const onChange = jest.fn();
    render(<Password id="password" value="mysecreptpassword" onChange={onChange} />);

    const input = screen.getByDisplayValue('mysecreptpassword');
    fireEvent.change(input, { target: { value: 'newpassword' } });
    expect(onChange).toHaveBeenCalledWith('newpassword');
  });

  it('renders as controlled for visibility capabilities', () => {
    const setVisible = jest.fn();
    render(<Password id="password" value="mysecreptpassword" visible={false} setVisible={setVisible} />);

    const button = screen.getByRole('button');
    fireEvent.click(button);
    expect(setVisible).toHaveBeenCalledWith(true);
  });

  it('renders with right states', () => {
    render(<Password id="password" value="mysecreptpassword" />);

    const group = screen.getByTestId('password-group');
    const input = screen.getByDisplayValue('mysecreptpassword');
    expect(group).toHaveClass('has-border');
    fireEvent.focus(input);
    expect(group).toHaveClass('active');
    fireEvent.blur(input);
    expect(group).not.toHaveClass('active');
  });
});
