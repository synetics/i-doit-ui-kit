import React, { forwardRef, InputHTMLAttributes, RefAttributes, useCallback, useState } from 'react';
import classnames from 'classnames';
import { useGlobalizedRef } from '../../../../hooks';
import { uniqueId } from '../../../../utils';
import { check } from '../../../../icons';
import { Icon } from '../../../Icon';
import styles from './Checkbox.module.scss';

type CheckboxProps = {
  /**
   * Defines an accessible name that labels the current element
   */
  'aria-label': string;

  /**
   * Defines label of the checkbox
   */
  label?: string;

  /**
   * Accent style of the component
   */
  variant?: 'regular' | 'error';

  onChangeAfter?: (value: boolean) => void;
} & InputHTMLAttributes<HTMLInputElement> &
  RefAttributes<HTMLInputElement>;

const Checkbox = forwardRef<HTMLInputElement, CheckboxProps>(
  (
    {
      'aria-label': ariaLabel,
      checked,
      label,
      onChange,
      onClick,
      onChangeAfter,
      onKeyDown,
      id,
      variant = 'regular',
      className,
      ...inputProps
    },
    ref,
  ) => {
    const [innerId] = useState(id || uniqueId('idoit-checkbox'));
    const derivedProps =
      checked !== undefined && typeof onChange === 'function' ? { checked } : { defaultChecked: checked };
    const { localRef, setRefsCallback } = useGlobalizedRef<HTMLInputElement>(ref);
    const onInternalChange = useCallback(
      (e: React.ChangeEvent<HTMLInputElement>) => {
        if (onChange) {
          onChange(e);
        }
        if (onChangeAfter) {
          onChangeAfter(e.target.checked);
        }
      },
      [onChange, onChangeAfter],
    );
    const handleKeyDown = useCallback(
      (e: React.KeyboardEvent<HTMLInputElement>) => {
        if (e.key === 'Enter' && localRef.current) {
          localRef.current.checked = !localRef.current.checked;
          onInternalChange({
            ...e,
            target: localRef.current,
          });
        }
        if (onKeyDown) {
          onKeyDown(e);
        }
      },
      [localRef, onInternalChange, onKeyDown],
    );
    const handleClick = useCallback(
      (e: React.MouseEvent<HTMLInputElement, MouseEvent>) => {
        if (onClick) {
          onClick(e);
          e.preventDefault();
          e.stopPropagation();
        }
        e.stopPropagation();
      },
      [onClick],
    );

    // eslint-disable jsx-a11y/click-events-have-key-events
    return (
      <div
        className={classnames(styles.checkbox, styles[variant], className)}
        data-testid="idoit-checkbox"
        role="presentation"
        onClick={handleClick}
      >
        <input
          tabIndex={0}
          {...inputProps}
          {...derivedProps}
          onChange={onInternalChange}
          onKeyDown={handleKeyDown}
          id={innerId}
          type="checkbox"
          ref={setRefsCallback}
        />
        <label htmlFor={innerId} data-testid="checkboxView">
          <span className={styles.pseudoCheckbox} role="button">
            <Icon src={check} wrapper="span" width={16} fill="white" className={styles.checkIcon} />
          </span>
          {label ? (
            <span className={styles.label}>{label}</span>
          ) : (
            <span className={styles.visuallyHidden}>{ariaLabel}</span>
          )}
        </label>
      </div>
    );
  },
);

export { Checkbox, CheckboxProps };
