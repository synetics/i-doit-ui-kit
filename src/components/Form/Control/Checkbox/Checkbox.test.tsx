import { fireEvent, render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { Checkbox } from './Checkbox';

describe('Checkbox', () => {
  it('renders without crashing', () => {
    render(<Checkbox aria-label="aria-label" />);

    expect(screen.getByTestId('idoit-checkbox')).toBeInTheDocument();
  });

  it('renders labeled', () => {
    render(<Checkbox aria-label="bar" />);

    expect(screen.getByText('bar')).toBeInTheDocument();
  });

  it('renders labeled and with visual label', () => {
    render(<Checkbox aria-label="bar" label="baz" />);

    expect(screen.getByText('baz')).toBeInTheDocument();
    expect(screen.queryByText('bar')).not.toBeInTheDocument();
  });

  it('links label and form control by default', () => {
    render(<Checkbox aria-label="bar" label="baz" />);

    const checkboxId = screen.getByRole('checkbox').getAttribute('id');
    const htmlFor = screen.getByText('baz').parentElement?.getAttribute('for');
    expect(checkboxId).toBe(htmlFor);
  });

  it('links label and form control by given id', () => {
    render(<Checkbox id="foo" aria-label="baz" />);

    const checkboxId = screen.getByRole('checkbox').getAttribute('id');
    const htmlFor = screen.getByText('baz').parentElement?.getAttribute('for');
    expect(checkboxId).toBe(htmlFor);
  });

  it('can be checked by clicking on pseudo element', async () => {
    render(<Checkbox aria-label="bar" />);
    const pseudoCheckbox = screen.getByText('bar');
    const checkbox = screen.getByRole('checkbox');

    await userEvent.click(pseudoCheckbox);

    expect(checkbox).toBeChecked();
  });

  it('can be checked by clicking on the label', async () => {
    render(<Checkbox aria-label="bar" label="baz" />);
    const label = screen.getByText('baz');
    const checkbox = screen.getByRole('checkbox');

    await userEvent.click(label);

    expect(checkbox).toBeChecked();
  });

  it('can be checked by keys', () => {
    render(<Checkbox aria-label="bar" />);
    const checkbox = screen.getByRole('checkbox');

    fireEvent.keyDown(checkbox, { key: 'Enter' });

    expect(checkbox).toBeChecked();

    fireEvent.keyDown(checkbox, { key: 'Space' });

    expect(checkbox).toBeChecked();
  });

  it('calls onChange event', async () => {
    const onChangeEventHandler = jest.fn();
    const onChangeAfterEventHandler = jest.fn();
    render(<Checkbox aria-label="bar" onChange={onChangeEventHandler} onChangeAfter={onChangeAfterEventHandler} />);
    const checkbox = screen.getByRole('checkbox');

    fireEvent.keyDown(checkbox, { key: 'Enter' });
    expect(onChangeEventHandler).toHaveBeenCalledTimes(1);
    expect(onChangeAfterEventHandler).toHaveBeenCalledTimes(1);

    await userEvent.click(checkbox);
    expect(onChangeEventHandler).toHaveBeenCalledTimes(2);
    expect(onChangeAfterEventHandler).toHaveBeenCalledTimes(2);
  });
});
