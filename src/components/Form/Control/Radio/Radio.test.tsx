import { render, screen } from '@testing-library/react';
import user from '@testing-library/user-event';
import { Radio } from './Radio';

describe('Radio', () => {
  it('renders without crashing', () => {
    render(<Radio aria-label="foo" />);

    expect(screen.getByRole('radio')).toBeInTheDocument();
  });

  it('renders labeled', () => {
    render(<Radio aria-label="bar" />);

    expect(screen.getByText('bar')).toBeInTheDocument();
    expect(screen.queryByText('baz')).not.toBeInTheDocument();
  });

  it('renders with custom className', () => {
    render(<Radio aria-label="bar" className="test" />);

    expect(screen.getByTestId('radio')).toHaveClass('test');
  });

  it('renders labeled and with visual label', () => {
    render(<Radio aria-label="bar" label="baz" />);

    expect(screen.getByText('baz')).toBeInTheDocument();
    expect(screen.queryByText('bar')).not.toBeInTheDocument();
  });

  it('links label and radio', () => {
    render(<Radio aria-label="bar" label="baz" />);

    expect(screen.queryByLabelText('baz')).toBeInTheDocument();
  });

  it('links label and radio by given id', () => {
    render(<Radio id="foo" aria-label="baz" />);

    expect(screen.queryByLabelText('baz')).toBeInTheDocument();
  });

  it('can be selected by click on the label', async () => {
    render(<Radio aria-label="bar" label="baz" />);
    const label = screen.getByText('baz');
    const radio = screen.getByLabelText('baz');

    await user.click(label);

    expect(radio).toBeChecked();
  });

  it('renders checked (uncontrolled)', () => {
    render(<Radio aria-label="bar" checked />);
    const radio = screen.getByLabelText('bar');

    expect(radio).toBeChecked();
  });

  it('renders controlled', () => {
    const onChangeMock = jest.fn();
    render(<Radio aria-label="bar" checked onChange={onChangeMock} />);

    expect(screen.getByLabelText('bar')).toBeChecked();
  });

  it('gives the ability to link the RadioButton with an error message', () => {
    render(<Radio aria-label="bar" aria-describedby="describedby" hasError />);
    const radio = screen.queryByLabelText('bar');

    expect(radio).toHaveAttribute('aria-describedby', 'describedby');
  });
});
