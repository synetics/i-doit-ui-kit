import { forwardRef, InputHTMLAttributes, RefAttributes, useState } from 'react';
import classnames from 'classnames';
import { uniqueId } from '../../../../utils';
import { useGlobalizedRef } from '../../../../hooks';
import styles from './Radio.module.scss';

type RadioProps = {
  /**
   * Defines an accessible name that labels the current element.
   */
  'aria-label': string;

  /**
   * Establishes a relationship between form field and text (error message) that described them.
   */
  'aria-describedby'?: string;

  /**
   * Defines additionat custom className
   */
  className?: string;

  /**
   * Defines the error state of the form field
   */
  hasError?: boolean;

  /**
   * Defines a unique identifier (ID) in the whole document to identify the element when linking.
   */
  id?: string;

  /**
   * Represents a caption for the radio button.
   */
  label?: string;
} & InputHTMLAttributes<HTMLInputElement> &
  RefAttributes<HTMLInputElement>;

const Radio = forwardRef<HTMLInputElement, RadioProps>(
  (
    {
      'aria-label': ariaLabel,
      'aria-describedby': ariaDescribedby,
      checked,
      hasError,
      label,
      onChange,
      id,
      className,
      ...inputProps
    },
    ref,
  ) => {
    const [innerId] = useState(id || uniqueId('idoit-radio'));
    const derivedProps =
      checked !== undefined && typeof onChange === 'function' ? { checked } : { defaultChecked: checked };
    const { setRefsCallback } = useGlobalizedRef<HTMLInputElement>(ref);

    return (
      <div data-testid="radio" className={classnames(styles.radio, className)}>
        <input
          id={innerId}
          ref={setRefsCallback}
          tabIndex={0}
          data-testid="idoit-radio"
          {...inputProps}
          {...derivedProps}
          onChange={onChange}
          aria-describedby={hasError ? ariaDescribedby : undefined}
          type="radio"
        />
        <label
          htmlFor={innerId}
          className={classnames({
            [styles.error]: hasError,
            [styles.labeled]: label,
          })}
        >
          {label || <span className={styles.visuallyHidden}>{ariaLabel}</span>}
        </label>
      </div>
    );
  },
);

export { Radio, RadioProps };
