import { fireEvent, render, screen } from '@testing-library/react';
import { createRef } from 'react';
import { FieldGroup } from '../../Infrastructure';
import { Input } from './Input';

describe('Input', () => {
  it('renders without crashing', () => {
    render(<Input id="idoit-input" type="text" />);

    expect(screen.getByTestId('idoit-input')).toBeInTheDocument();
  });

  it('renders without crashing', async () => {
    render(<Input id="idoit-input" type="number" />);
    const input = screen.getByTestId('idoit-input');

    fireEvent.change(input, { target: { value: '1' } });
    expect(input).toHaveValue(1);
    fireEvent.change(input, { target: { value: ' ' } });
    expect(input).toHaveValue(null);
  });

  it('renders without crashing with default type', () => {
    render(<Input id="idoit-input" />);

    expect(screen.getByTestId('idoit-input')).toBeInTheDocument();
    expect(screen.getByTestId('idoit-input')).toHaveAttribute('type', 'text');
  });

  it('renders uncontrolled', () => {
    render(<Input id="idoit-input" type="text" />);
    const input = screen.getByTestId('idoit-input');

    fireEvent.change(input, { target: { value: 'input-change-1' } });
    expect(input).toHaveValue('input-change-1');

    fireEvent.change(input, { target: { value: 'input-change-2' } });
    expect(input).toHaveValue('input-change-2');
  });

  it('renders initialValue', () => {
    render(<Input id="idoit-input" type="text" value="initial-value" />);
    const input = screen.getByTestId('idoit-input');

    expect(input).toHaveValue('initial-value');
  });

  it('calls onChange callback', () => {
    const onChange = jest.fn();
    render(<Input id="idoit-input" type="text" onChange={onChange} />);
    const input = screen.getByTestId('idoit-input');

    fireEvent.change(input, { target: { value: 'input-change-1' } });

    expect(onChange).toHaveBeenCalledWith('input-change-1');
  });

  it('calls onFocus callback', () => {
    const onFocus = jest.fn();
    render(<Input id="idoit-input" type="text" onFocus={onFocus} />);
    const input = screen.getByTestId('idoit-input');

    fireEvent.focus(input, { target: {} });

    expect(onFocus).toHaveBeenCalled();
  });

  it('calls onBlur callback', () => {
    const onBlur = jest.fn();
    render(<Input id="idoit-input" type="text" onBlur={onBlur} />);
    const input = screen.getByTestId('idoit-input');

    fireEvent.blur(input, { target: {} });

    expect(onBlur).toHaveBeenCalled();
  });

  it('is default after render', () => {
    render(<Input id="idoit-input" type="text" />);
    const input = screen.getByTestId('idoit-input');

    fireEvent.focus(input, { target: {} });

    expect(input).toHaveClass('has-border');
  });

  it('is active after focus', () => {
    render(<Input id="idoit-input" type="text" />);
    const input = screen.getByTestId('idoit-input');

    fireEvent.focus(input, { target: {} });

    expect(input).toHaveClass('active');
  });

  it('is active with autoFocus', () => {
    render(<Input id="idoit-input" type="text" autoFocus />);
    const input = screen.getByTestId('idoit-input');

    expect(input).toHaveClass('active');
  });

  it('has error variant', () => {
    render(<Input id="idoit-input" type="text" variant="has-error" />);
    const input = screen.getByTestId('idoit-input');

    expect(input).toHaveClass('has-error');
  });

  it('has success variant', () => {
    render(<Input id="idoit-input" type="text" variant="success" />);
    const input = screen.getByTestId('idoit-input');

    expect(input).toHaveClass('success');
  });

  it('is disabled', () => {
    render(<Input id="idoit-input" type="text" disabled value="initial-value" />);
    const input = screen.getByTestId('idoit-input');

    expect(input).toHaveClass('disabled');
    expect(input).toBeDisabled();
  });

  it('is readonly', () => {
    render(<Input id="idoit-input" type="text" readonly value="initial-value" />);
    const input = screen.getByTestId('idoit-input');

    expect(input).toHaveClass('readonly');
    expect(input).toHaveAttribute('readonly');
  });

  it('is required', () => {
    render(<Input id="idoit-input" type="text" required value="initial-value" />);
    const input = screen.getByTestId('idoit-input');

    expect(input).toBeRequired();
  });

  it('calls format', () => {
    const format = jest.fn((e: string): string => e);
    render(<Input id="idoit-input" type="text" format={format} />);
    const input = screen.getByTestId('idoit-input');

    fireEvent.change(input, { target: { value: 'input-change-1', selectionStart: 3 } });

    expect(format).toHaveBeenCalledWith('ut-change-1');
    expect(format).toHaveBeenCalledWith('inp');
  });

  it('renders with given attributes', () => {
    const attributes = {
      placeholder: 'initial-placeholder',
      name: 'name',
      value: 'value',
    };

    render(<Input type="text" id="idoit-input" {...attributes} />);
    const input = screen.getByTestId('idoit-input');

    Object.entries(attributes).forEach(([attribute, value]) => {
      expect(input).toHaveAttribute(attribute, value);
    });
  });

  it('renders with class', () => {
    render(<Input type="text" id="idoit-input" className="dummy-class" />);
    const input = screen.getByTestId('idoit-input');

    expect(input).toHaveAttribute('class');
    expect(input).toHaveClass('dummy-class');
  });

  it('uses ref', () => {
    const ref = createRef<HTMLInputElement>();

    render(<Input type="text" id="idoit-input" ref={ref} />);
    expect(ref.current).toBeInstanceOf(HTMLInputElement);
  });

  it('uses distributed "disabled" property', () => {
    render(
      <FieldGroup disabled>
        <Input type="text" id="idoit-input" />
      </FieldGroup>,
    );

    const input = screen.getByTestId('idoit-input');

    expect(input).toBeDisabled();
  });

  it('uses distributed "readonly" property', () => {
    render(
      <FieldGroup readonly>
        <Input type="text" id="idoit-input" />
      </FieldGroup>,
    );

    const input = screen.getByTestId('idoit-input');

    expect(input).toHaveAttribute('readonly');
  });

  it('uses distributed "required" property', () => {
    render(
      <FieldGroup required>
        <Input type="text" id="idoit-input" />
      </FieldGroup>,
    );

    const input = screen.getByTestId('idoit-input');

    expect(input).toBeRequired();
  });

  it('uses distributed "active" property', () => {
    const setActive = jest.fn();
    render(
      <FieldGroup active setActive={setActive}>
        <Input type="text" id="idoit-input" />
      </FieldGroup>,
    );

    const input = screen.getByTestId('idoit-input');
    expect(input).toHaveClass('active');
    fireEvent.focus(input);
    expect(setActive).toHaveBeenCalled();
  });

  it('can input the negative number', () => {
    render(
      <FieldGroup>
        <Input type="number" id="idoit-input" />
      </FieldGroup>,
    );

    const input = screen.getByTestId('idoit-input');

    fireEvent.change(input, { target: { value: '1' } });
    expect(input).toHaveValue(1);

    fireEvent.change(input, { target: { value: '' } });
    fireEvent.change(input, { target: { value: '-1' } });
    expect(input).toHaveValue(-1);
  });
});
