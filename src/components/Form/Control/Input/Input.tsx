import React, { InputHTMLAttributes, useCallback, useEffect, useMemo } from 'react';
import classnames from 'classnames';
import { useControlledState, useGlobalizedRef } from '../../../../hooks';
import {
  FieldProps,
  getFieldClasses,
  IdentifiableProps,
  useActive,
  useFieldGroup,
  ValueProps,
} from '../../Infrastructure';
import { transformWithCursorPosition } from './utils';

type Value = string | number;
type InputProps = FieldProps &
  Omit<InputHTMLAttributes<HTMLInputElement>, 'value' | 'onChange'> &
  ValueProps<Value> &
  IdentifiableProps & {
    type?: 'text' | 'number' | 'date' | 'datetime-local' | 'time' | 'color' | 'month' | 'password' | 'week';
    format?: (v: string) => string;
    rtl?: boolean;
    maxLength?: number;
  };

const Input = React.forwardRef<HTMLInputElement, InputProps>(
  (
    {
      'aria-label': ariaLabel,
      id,
      type = 'text',
      name,
      value: initialValue = '',
      format = (v: string): string => v,
      rtl,
      placeholder,
      autoFocus,
      maxLength,

      active,
      setActive,
      disabled,
      readonly,
      required,

      onChange,
      onFocus,
      onBlur,
      className,
      variant,
      initial,
      autoComplete = 'off',
      ...inputProps
    },
    ref,
  ) => {
    const [value, setValue] = useControlledState<Value>(initialValue, onChange);
    const { value: derivedActive, setValue: setDerivedActive } = useActive(active || autoFocus || false, setActive);
    const {
      readonly: derivedReadonly,
      disabled: derivedDisabled,
      required: derivedRequired,
      ariaLabel: derivedAriaLabel,
    } = useFieldGroup({ disabled, readonly, required, ariaLabel });

    const { localRef, setRefsCallback } = useGlobalizedRef<HTMLInputElement>(ref);

    // Recheck this in app because it does not work in storybook
    useEffect(() => {
      if (autoFocus) {
        const timeOutId = setTimeout(() => {
          setDerivedActive(true);
          localRef.current?.focus();
        }, 0);
        return () => clearTimeout(timeOutId);
      }
      return undefined;
    }, [autoFocus, setDerivedActive, localRef]);

    const handleChange = useCallback(
      (e: React.ChangeEvent<HTMLInputElement>) => {
        const { target: input } = e;
        const { value: nextValue, position: cursorPosition } = transformWithCursorPosition(input, format);

        setValue(nextValue);

        if (typeof input.selectionStart === 'number') {
          window.requestAnimationFrame(() => {
            input.selectionStart = cursorPosition;
            input.selectionEnd = cursorPosition;
          });
        }
      },
      [format, setValue],
    );

    const handleFocus = useCallback(
      (event: React.FocusEvent<HTMLInputElement>) => {
        if (onFocus) {
          onFocus(event);
        }

        setDerivedActive(true);
      },
      [onFocus, setDerivedActive],
    );

    const handleBlur = useCallback(
      (event: React.FocusEvent<HTMLInputElement>) => {
        if (onBlur) {
          onBlur(event);
        }

        setDerivedActive(false);
      },
      [onBlur, setDerivedActive],
    );

    const classNames = useMemo<string>(
      () =>
        classnames(
          getFieldClasses({
            active: !derivedReadonly && derivedActive,
            disabled: derivedDisabled,
            readonly: derivedReadonly,
            variant,
            className,
            initial,
          }),
          { 'text-right': rtl },
        ),
      [className, derivedActive, derivedDisabled, derivedReadonly, rtl, variant, initial],
    );

    return (
      <input
        {...inputProps}
        id={id}
        data-testid={id}
        /* eslint-disable-next-line jsx-a11y/no-autofocus */
        autoFocus={autoFocus}
        aria-label={derivedAriaLabel}
        ref={setRefsCallback}
        className={classNames}
        disabled={derivedDisabled}
        readOnly={derivedReadonly}
        required={derivedRequired}
        name={name}
        type={type}
        value={value}
        placeholder={placeholder}
        onKeyDown={!derivedReadonly ? inputProps.onKeyDown : undefined}
        onChange={handleChange}
        onFocus={handleFocus}
        onBlur={handleBlur}
        maxLength={maxLength}
        autoComplete={autoComplete}
      />
    );
  },
);

export { Input, InputProps };
