type CursorPositon = {
  value: string;
  position: number;
};

export const transformWithCursorPosition = (
  input: HTMLInputElement,
  format: (value: string) => string,
): CursorPositon => {
  const { value, selectionStart: cursor } = input;

  if (!cursor) {
    return {
      value: input.value,
      position: 0,
    };
  }
  const beforeCursor = input.value.slice(0, cursor);
  const afterCursor = value.slice(cursor, value.length);

  const filteredBeforeCursor = format(beforeCursor);
  const filteredAfterCursor = format(afterCursor);

  const newValue = filteredBeforeCursor + filteredAfterCursor;
  const newCursorPosition = filteredBeforeCursor.length;

  return { value: newValue, position: newCursorPosition };
};
