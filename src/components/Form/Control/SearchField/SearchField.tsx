import React, { FocusEvent, forwardRef, useRef } from 'react';
import { CSSTransition } from 'react-transition-group';
import classnames from 'classnames';
import { FieldExtra, FieldGroup, ValueProps } from '../../Infrastructure';
import { Input, InputProps } from '../Input';
import { Icon } from '../../../Icon';
import { search } from '../../../../icons';
import { useControlledState } from '../../../../hooks';
import { ResetButton } from '../Pulldown/ResetButton';
import { Spinner } from '../../../Spinner';
import styles from './SearchField.module.scss';

type SearchFieldType = Omit<InputProps, 'type'> &
  ValueProps<string> & {
    resetButtonLabel?: string;
    isLoading?: boolean;
    variant?: 'solid' | 'outline';
    flexible?: boolean;
    classMinimized?: string;
  };

const SearchField = forwardRef<unknown, SearchFieldType>(
  (
    {
      id,
      active = false,
      setActive,
      value,
      onChange,
      'aria-label': ariaLabel,
      disabled,
      autoFocus,
      required,
      name,
      className,
      readOnly,
      placeholder,
      onFocus,
      onBlur,
      rtl,
      format,
      resetButtonLabel = 'Clear',
      isLoading,
      flexible = false,
      variant = 'outline',
      classMinimized,
      ...props
    },
    ref,
  ) => {
    const [expanded, setExpanded] = useControlledState<boolean>(active, setActive);
    const inputRef = useRef<HTMLInputElement>(null);
    const handleExpanded = (e: FocusEvent<HTMLInputElement, Element>) => {
      if (onFocus) {
        onFocus(e);
      }
      setExpanded(true);
    };

    const handleBlur = (e: FocusEvent<HTMLInputElement, Element>) => {
      if (onBlur) {
        onBlur(e);
      }
      if (!value) {
        setExpanded(false);
      }
    };

    const handleReset = () => {
      if (onChange) {
        onChange('');
      }
      inputRef.current?.focus();
      setExpanded(false);
    };

    const handleKeyDown = (e: React.KeyboardEvent<HTMLInputElement>) => {
      if (e.key === 'Escape' && onChange) {
        onChange('');
      }
    };

    return (
      <FieldGroup
        {...props}
        ref={ref}
        data-testid="livesearch"
        className={classnames(className, flexible && !expanded && classMinimized, {
          [styles.searchMinimized]: flexible && !expanded,
          [styles.searchExpanded]: flexible && expanded,
          [styles.solid]: !value && !expanded && variant === 'solid',
        })}
      >
        <FieldExtra>
          <Icon src={search} />
        </FieldExtra>
        <Input
          id={id}
          autoFocus={autoFocus}
          autoComplete="off"
          aria-label={ariaLabel}
          ref={inputRef}
          className={className}
          disabled={disabled}
          readOnly={readOnly}
          required={required}
          name={name}
          value={value}
          placeholder={placeholder}
          onChange={onChange}
          onKeyDown={handleKeyDown}
          onFocus={handleExpanded}
          onBlur={handleBlur}
          rtl={rtl}
          format={format}
        />

        {isLoading && (
          <FieldExtra>
            <Spinner className={styles.spinner} />
          </FieldExtra>
        )}
        <CSSTransition
          in={!!value}
          timeout={300}
          classNames={{
            enter: styles.resetButtonEnterActive,
            exitActive: styles.resetButtonExitDone,
            enterDone: styles.resetButtonEnterDone,
          }}
          unmountOnExit
        >
          <FieldExtra className={styles.resetButton}>
            <ResetButton onClick={handleReset} label={resetButtonLabel} />
          </FieldExtra>
        </CSSTransition>
      </FieldGroup>
    );
  },
);

export { SearchField };
