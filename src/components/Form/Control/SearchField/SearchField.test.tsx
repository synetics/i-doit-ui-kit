import { fireEvent, render, screen } from '@testing-library/react';
import user from '@testing-library/user-event';
import { SearchField } from './SearchField';

describe('SearchField', () => {
  it('renders without crashing', async () => {
    render(<SearchField id="livesearch" />);

    expect(screen.getAllByTestId('livesearch')[0]).toBeInTheDocument();
  });

  it('renders with close button if value', () => {
    render(<SearchField id="livesearch" value="mylivesearch" resetButtonLabel="reset" />);

    expect(screen.getByTestId('reset-button')).toBeInTheDocument();
  });

  it('renders with loading icon if true', () => {
    render(<SearchField id="livesearch" value="mylivesearch" isLoading />);

    expect(screen.getByTestId('idoit-spinner')).toBeInTheDocument();
  });

  it('Passed minimized classname if exists', () => {
    render(<SearchField id="livesearch" value="mylivesearch" flexible classMinimized="search" />);

    expect(document.getElementsByClassName('search').length).toBe(1);
  });

  it('get expanded when focused', async () => {
    const handleFocus = jest.fn();

    render(<SearchField id="livesearch" value="mylivesearch" flexible onFocus={handleFocus} />);
    const liveSearch = screen.getAllByTestId('livesearch')[1];
    await user.click(liveSearch);

    expect(handleFocus).toHaveBeenCalled();
    expect(document.getElementsByClassName('searchExpanded').length).toBe(1);
  });

  it('get minimized when blured', () => {
    const handleBlur = jest.fn();
    render(<SearchField id="livesearch" onBlur={handleBlur} flexible />);
    const liveSearch = screen.getAllByTestId('livesearch')[1];

    fireEvent.focus(liveSearch);

    expect(document.getElementsByClassName('searchExpanded').length).toBe(1);

    fireEvent.blur(liveSearch);

    expect(handleBlur).toHaveBeenCalled();
    expect(document.getElementsByClassName('searchExpanded').length).toBe(0);
  });

  it('resets value by clicking on reset button', async () => {
    const handleChange = jest.fn();
    render(<SearchField id="livesearch" onChange={handleChange} value="mylivesearch" />);
    const reset = screen.getByTestId('reset-button');
    await user.click(reset);

    expect(handleChange).toHaveBeenCalledWith('');
  });

  it('clears value and minimized on Esc pressed', async () => {
    const handleChange = jest.fn();

    render(<SearchField id="livesearch" onChange={handleChange} />);
    const liveSearch = screen.getAllByTestId('livesearch')[1];

    await user.type(liveSearch, 'A');
    expect(handleChange).toHaveBeenCalledWith('A');

    await user.type(liveSearch, '{escape}');
    expect(handleChange).toHaveBeenCalledWith('');
  });
});
