import { ChangeEvent, DragEvent, forwardRef, ReactNode, RefAttributes, useCallback } from 'react';
import { useControlledState, useGlobalizedRef } from '../../../../hooks';
import { FieldProps, FormField, useActive, ValueProps } from '../../Infrastructure';
import { Button } from '../../../Button';
import styles from './FileUpload.module.scss';
import { FileType } from './types';

type FileUploadProps = RefAttributes<HTMLInputElement> &
  FieldProps &
  ValueProps<FileType> & {
    /**
     * Types that component can accept
     */
    accept?: string;
    /**
     * Content that appears in upload container
     */
    children?: ReactNode | ReactNode[];
  };

const FileUpload = forwardRef<HTMLInputElement, FileUploadProps>(
  ({ children, value = null, accept, onChange, ...props }, ref) => {
    const [file, setFile] = useControlledState<FileType>(value, onChange);
    const { value: active, setValue: setActive } = useActive(props.active, props.setActive);
    const { localRef: currentRef, setRefsCallback } = useGlobalizedRef<HTMLInputElement>(ref);

    const onChangeFileUpload = useCallback(
      (e: ChangeEvent<HTMLInputElement>) => {
        const { files } = e.target;
        if (files && files.length) {
          setFile(files[0]);
        }
      },
      [setFile],
    );

    const fileDrop = useCallback(
      (e: DragEvent) => {
        const { files } = e.dataTransfer;
        e.preventDefault();
        if (files.length) {
          setFile(files[0]);
        }
        setActive(false);
      },
      [setActive, setFile],
    );

    const onDragEnter = useCallback(
      (e: DragEvent) => {
        e.preventDefault();
        setActive(true);
      },
      [setActive],
    );

    const onDragLeave = useCallback(
      (e: DragEvent) => {
        e.preventDefault();
        setActive(false);
      },
      [setActive],
    );

    return (
      <FormField
        {...props}
        onDragOver={onDragEnter}
        onDragEnter={onDragEnter}
        onDragLeave={onDragLeave}
        onDrop={fileDrop}
        active={active}
        data-testid="idoit-upload-container"
      >
        <input
          className={styles.fileInput}
          onChange={onChangeFileUpload}
          type="file"
          ref={setRefsCallback}
          accept={accept}
          data-testid="idoit-upload-field"
        />
        {children || (
          <>
            <Button
              onClick={() => currentRef.current && currentRef.current.click()}
              variant="secondary"
              className={styles.marginRight}
            >
              Choose file
            </Button>
            {file && file.name}
          </>
        )}
      </FormField>
    );
  },
);

export { FileUpload, FileUploadProps };
