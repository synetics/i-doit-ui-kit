import { fireEvent, render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { FileUpload } from './FileUpload';

const fileUploadElem = <FileUpload onChange={() => jest.fn}>Test content</FileUpload>;

const file = new File(['test content'], 'test_file.csv', {
  type: 'text/csv',
});

describe('FileUpload', () => {
  it('renders without crashing', () => {
    render(fileUploadElem);
  });

  it('allows to drag over upload', () => {
    render(fileUploadElem);

    const fileUpload = screen.getByTestId('idoit-upload-container');
    fireEvent.dragOver(fileUpload);
    expect(fileUpload).toHaveClass('active');

    fireEvent.dragLeave(fileUpload);

    expect(fileUpload).not.toHaveClass('active');
  });

  it('adds file by drag n drop', () => {
    const mock = jest.fn();
    render(<FileUpload onChange={mock}>Test content</FileUpload>);
    const fileUpload = screen.getByTestId('idoit-upload-field');

    fireEvent.dragOver(fileUpload);
    fireEvent.drop(fileUpload, { dataTransfer: { files: [file] } });

    expect(mock).toHaveBeenNthCalledWith(1, file);
  });

  it('adds file by onChange input', async () => {
    render(<FileUpload />);

    const fileUpload = screen.getByTestId('idoit-upload-field');
    await userEvent.upload(fileUpload, file);

    expect(screen.queryByText('test_file.csv')).toBeInTheDocument();
  });
  it('should trigger file input click when "Choose file" button is clicked', async () => {
    const { getByText, getByTestId } = render(<FileUpload />);

    const fileInput = getByTestId('idoit-upload-field');
    const chooseFileButton = getByText('Choose file');

    const clickSpy = jest.spyOn(fileInput, 'click');

    await userEvent.click(chooseFileButton);

    expect(clickSpy).toHaveBeenCalledTimes(1);
  });
});
