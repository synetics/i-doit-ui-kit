import { FC } from 'react';
import { FileUpload } from '../FileUpload';

const UncontrolledFileUpload: FC = () => <FileUpload accept=".csv" />;

export default UncontrolledFileUpload;
