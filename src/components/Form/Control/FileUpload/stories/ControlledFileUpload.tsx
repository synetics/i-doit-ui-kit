import { FC, useRef, useState } from 'react';
import { FileUpload } from '../FileUpload';
import { Error } from '../../../Infrastructure/Error';
import { Headline } from '../../../../Headline';
import { Button } from '../../../../Button';
import { FileType } from '../types';
import styles from './ControlledFileUpload.module.scss';

const MAX_FILE_SIZE_IN_BYTES = 500000;

const ControlledFileUpload: FC = () => {
  const [file, setFile] = useState<FileType>(null);
  const [error, setError] = useState<boolean>(false);
  const fileInput = useRef<HTMLInputElement | null>(null);

  const handleErrorState = (fileToCheck: File) => {
    if (fileToCheck.size > MAX_FILE_SIZE_IN_BYTES || fileToCheck.type !== 'text/csv') {
      setError(true);
    } else {
      setFile(fileToCheck);
      setError(false);
    }
  };

  const onChangeFileUpload = (value: FileType) => {
    if (value) {
      handleErrorState(value);
    }
  };

  const handleUploadClick = () => fileInput.current && fileInput.current.click();

  return (
    <FileUpload accept=".csv" value={file} onChange={onChangeFileUpload} ref={fileInput}>
      {!file ? (
        <div className={styles.controlContainer}>
          <Headline tag="h2">Drag & drop to upload or</Headline>
          <Button onClick={handleUploadClick} className={styles.uploadButton} variant="secondary">
            Choose file
          </Button>
          {error && <Error id="file_upload">File must be .csv type and below 5 Mb</Error>}
        </div>
      ) : (
        <div className={styles.controlContainer}>
          <Headline tag="h2">Your file was uploaded successfully!</Headline>
          <div className="uploadContainer">{file && file.name}</div>
          <Button onClick={() => setFile(null)} className={styles.uploadButton} variant="secondary">
            Delete
          </Button>
        </div>
      )}
    </FileUpload>
  );
};

export default ControlledFileUpload;
