import { render, screen } from '@testing-library/react';
import classes from './Error.module.scss';
import { Error } from './Error';

describe('Error', () => {
  it('has needed attributes', () => {
    const content = 'Error';

    render(<Error id="test">{content}</Error>);

    const element = screen.getByText(content);
    expect(element).toBeInTheDocument();
    expect(element).toHaveClass(classes.error, classes.sticky);
    expect(element).toHaveAttribute('for', 'test');
  });

  it('disables sticky', () => {
    const content = 'Error';

    render(
      <Error id="test" sticky={false}>
        {content}
      </Error>,
    );

    const element = screen.getByText(content);
    expect(element).not.toHaveClass(classes.sticky);
  });

  it('passes the class', () => {
    const className = 'test-class';
    const content = 'Error';

    render(
      <Error className={className} id="test">
        {content}
      </Error>,
    );

    const element = screen.getByText(content);
    expect(element).toHaveClass(classes.error, classes.sticky, className);
  });
});
