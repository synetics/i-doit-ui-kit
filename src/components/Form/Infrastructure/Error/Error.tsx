import classnames from 'classnames';
import classes from './Error.module.scss';

type ErrorProps = {
  /**
   * Id of the field
   */
  id: string;
  /**
   * Container class name
   */
  className?: string;
  /**
   * Error text
   */
  children: string;
  /**
   * Is it sticky
   */
  sticky?: boolean;
};

const Error = ({ id, sticky = true, className, children }: ErrorProps) => (
  <label
    className={classnames(classes.error, className, {
      [classes.sticky]: sticky,
    })}
    htmlFor={id}
  >
    {children}
  </label>
);

export { Error };
