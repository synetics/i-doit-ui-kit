import classnames from 'classnames';
import classes from './Underline.module.scss';

type UnderlineProps = {
  /**
   * Id of the field
   */
  id: string;
  /**
   * Container class name
   */
  className?: string;
  /**
   * Left text
   */
  left?: string;
  /**
   * Right text
   */
  right?: string;
};

const Underline = ({ id, className, left, right }: UnderlineProps) => (
  <label className={classnames(classes.underline, className)} htmlFor={id}>
    <div className={classes.underlineLeft}>{left}</div>
    {right && <div>{right}</div>}
  </label>
);

export { Underline };
