import { render, screen } from '@testing-library/react';
import classes from './Underline.module.scss';
import { Underline } from './Underline';

describe('Underline', () => {
  it('has needed attributes', () => {
    render(<Underline id="test" left="Child" right="Right child" />);

    const element = screen.getByText('Child');
    expect(element).toBeInTheDocument();
    expect(element).toHaveClass(classes.underlineLeft);
    const right = screen.getByText('Right child');
    expect(right).toBeInTheDocument();
    expect(element.parentElement).toHaveAttribute('for', 'test');
    expect(element.parentElement).toHaveClass(classes.underline);
  });

  it('passes the class', () => {
    render(<Underline className="test-class" id="test" left="Child" />);

    const element = screen.getByText('Child');
    expect(element.parentElement).toHaveClass(classes.underline, 'test-class');
  });

  it('renders right hint', () => {
    render(<Underline right="Right" id="test" />);

    const element = screen.getByText('Right');
    expect(element).toBeInTheDocument();
  });
});
