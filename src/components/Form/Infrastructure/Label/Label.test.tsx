import { render, screen } from '@testing-library/react';
import classes from './Label.module.scss';
import { Label } from './Label';

describe('Label', () => {
  it('has needed attributes', () => {
    render(<Label id="test">Child</Label>);

    expect(screen.queryByText('Required')).not.toBeInTheDocument();
    const element = screen.getByText('Child');
    expect(element).toHaveClass(classes.label);
    expect(element.parentElement).toHaveAttribute('for', 'test');
    expect(element.parentElement).toHaveClass(classes.labelContainer);
  });

  it('passes the class', () => {
    render(
      <Label className="test-class" id="test">
        Child
      </Label>,
    );

    const element = screen.getByText('Child');
    expect(element.parentElement).toHaveClass(classes.labelContainer, 'test-class');
  });

  it('renders required', () => {
    render(
      <Label required id="test">
        Child
      </Label>,
    );

    const element = screen.getByText('Required');
    expect(element).toBeInTheDocument();
    expect(element).toHaveClass(classes.required);
  });

  it('renders custom text of required', () => {
    render(
      <Label required id="test" requiredText="Pflichtfeld">
        Child
      </Label>,
    );

    const element = screen.getByText('Pflichtfeld');
    expect(element).toBeInTheDocument();
    expect(element).toHaveClass(classes.required);
  });

  it('Does not render custom required text', () => {
    render(
      <Label id="test" requiredText="Pflichtfeld">
        Child
      </Label>,
    );

    expect(screen.queryByText('Pflichtfeld')).not.toBeInTheDocument();
  });
});
