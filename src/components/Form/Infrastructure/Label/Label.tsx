import classnames from 'classnames';
import { ReactNode } from 'react';
import classes from './Label.module.scss';

type LabelProps = {
  /**
   * Id of the field
   */
  id: string;
  /**
   * Container class name
   */
  className?: string;
  /**
   * Content of the label
   */
  children: string | ReactNode;
  /**
   * Is it required
   */
  required?: boolean;

  /**
   * Text of the required field
   */
  requiredText?: string;
};

const Label = ({ id, className, children, required, requiredText = 'Required' }: LabelProps) => (
  <label className={classnames(classes.labelContainer, className)} htmlFor={id}>
    <span className={classes.label}>{children}</span>
    {required && <div className={classes.required}>{requiredText}</div>}
  </label>
);

export { Label };
