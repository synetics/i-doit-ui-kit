import { createContext } from 'react';

type FieldGroupContextProps = {
  readonly?: boolean;
  disabled?: boolean;
  required?: boolean;
  ariaLabel?: string;
};

const FieldGroupContext = createContext<FieldGroupContextProps>({});

export { FieldGroupContext, FieldGroupContextProps };
