import { useContext } from 'react';
import { getByCriteria, isBoolean, isString } from '../../../../utils';
import { FieldGroupContext, FieldGroupContextProps } from './context';

const useFieldGroup = ({
  disabled,
  readonly,
  required,
  ariaLabel,
}: FieldGroupContextProps): Required<FieldGroupContextProps> => {
  const value = useContext<FieldGroupContextProps>(FieldGroupContext);

  return {
    disabled: getByCriteria<boolean>(isBoolean, false, disabled, value.disabled),
    readonly: getByCriteria<boolean>(isBoolean, false, readonly, value.readonly),
    required: getByCriteria<boolean>(isBoolean, false, required, value.required),
    ariaLabel: getByCriteria<string>(isString, '', ariaLabel, value.ariaLabel),
  };
};

export { useFieldGroup };
