import { fireEvent, render, screen } from '@testing-library/react';
import classes from '../FormField/FormField.module.scss';
import { Input } from '../../Control';
import { FieldGroup } from './FieldGroup';

describe('FieldGroup', () => {
  it('renders without crashing', () => {
    render(<FieldGroup>Child</FieldGroup>);

    const element = screen.getByText('Child');
    expect(element).toBeInTheDocument();
    expect(element).toHaveClass(classes.fieldGroup);
  });

  it('distributes needed active state', () => {
    render(
      <FieldGroup>
        <Input id="idoit-control-input" type="text" />
        CHILD
      </FieldGroup>,
    );

    const element = screen.getByText('CHILD');
    const inputElement = screen.getByTestId('idoit-control-input');

    expect(element).toBeInTheDocument();
    expect(element).toHaveClass(classes.fieldGroup, 'has-border', classes.formField);
    fireEvent.focus(inputElement);
    expect(element).toHaveClass('active');
  });
});
