import React, { forwardRef } from 'react';
import classnames from 'classnames';
import { ContainerProps } from '../../../Container';
import { ActiveContext, useActive } from '../useActive';
import { FieldGroupContext } from '../useFieldGroup';
import { FieldProps, FormField } from '../FormField';
import classes from '../FormField/FormField.module.scss';

type FieldGroupProps = FieldProps & ContainerProps & { ariaLabel?: string; required?: boolean };

const FieldGroup = forwardRef<unknown, FieldGroupProps>(
  (
    {
      className,
      children,
      active,
      setActive,
      disabled = false,
      readonly = false,
      required = false,
      'aria-label': ariaLabel,
      ...props
    },
    ref,
  ) => {
    const contextValue = useActive(active, setActive);

    return (
      <FormField
        {...props}
        active={contextValue.value}
        readonly={readonly}
        disabled={disabled}
        className={classnames(className, classes.fieldGroup)}
        ref={ref}
      >
        <FieldGroupContext.Provider value={{ disabled, readonly, required, ariaLabel }}>
          <ActiveContext.Provider value={contextValue}>{children}</ActiveContext.Provider>
        </FieldGroupContext.Provider>
      </FormField>
    );
  },
);

export { FieldGroup, FieldGroupProps };
