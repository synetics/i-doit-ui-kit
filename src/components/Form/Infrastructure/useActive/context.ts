import { createContext } from 'react';
import { ContextValue } from '../../../../hooks/use-hierarchical-value';

export const ActiveContext = createContext<ContextValue<boolean> | null>(null);
