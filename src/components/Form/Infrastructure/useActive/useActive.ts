import { ContextValue, useHierarchicalValue } from '../../../../hooks/use-hierarchical-value';
import { ActiveContext } from './context';

const useActive = (active = false, setActive?: (value: boolean) => void): ContextValue<boolean> =>
  useHierarchicalValue<boolean>(ActiveContext, active, setActive);

export { useActive };
