import classnames from 'classnames';
import { Container, ContainerProps } from '../../../Container';
import classes from './FormField.module.scss';

type FieldContentType = ContainerProps & {
  /**
   * How to overflow content
   */
  overflow?: 'wrap' | 'ellipsis';
};

const FieldContent = ({ className, children, overflow = 'ellipsis', ...props }: FieldContentType) => (
  <Container {...props} className={classnames(classes.content, overflow && classes[overflow], className)}>
    {children}
  </Container>
);

export { FieldContent };
