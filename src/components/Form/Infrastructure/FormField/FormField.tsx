import { forwardRef } from 'react';
import { Container, ContainerProps } from '../../../Container';
import { getFieldClasses } from './helpers';
import { FieldProps } from './types';

type FormFieldProps = FieldProps & ContainerProps;

const FormField = forwardRef<unknown, FormFieldProps>(
  ({ children, active, className, variant, initial, ...props }: FormFieldProps, ref) => {
    const { disabled, readonly } = props;
    return (
      <Container
        {...props}
        className={getFieldClasses({
          active,
          disabled,
          readonly,
          className,
          variant,
          initial,
        })}
        ref={ref}
      >
        {children}
      </Container>
    );
  },
);

export { FormField, FormFieldProps };
