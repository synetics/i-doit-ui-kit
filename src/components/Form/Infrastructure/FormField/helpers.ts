import classnames from 'classnames';
import classes from './FormField.module.scss';
import { ViewProps, WithClassName } from './types';

export const getFieldClasses = ({
  active,
  className,
  disabled,
  readonly,
  variant = 'has-border',
  initial,
}: ViewProps & WithClassName): string =>
  classnames(classes.formField, variant && classes[variant], className, {
    [classes.disabled]: disabled,
    [classes.active]: active,
    [classes.readonly]: readonly,
    [classes.initial]: initial,
  });
