import { Icon } from '../../../Icon';
import { warning } from '../../../../icons';

type FieldErrorType = { className?: string };

const FieldError = ({ className }: FieldErrorType) => (
  <Icon className={className} dataTestId="field-error" src={warning} fill="error" />
);

export { FieldError };
