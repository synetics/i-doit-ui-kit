import classnames from 'classnames';
import { Container, ContainerProps } from '../../../Container';
import classes from './FormField.module.scss';

const FieldExtra = ({ children, className, ...props }: ContainerProps) => (
  <Container {...props} className={classnames(classes.extra, className)}>
    {children}
  </Container>
);

export { FieldExtra };
