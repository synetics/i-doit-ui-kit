import { render, screen } from '@testing-library/react';
import { FieldError } from '../FieldError';

describe('FieldError', () => {
  it('renders without crashing', () => {
    render(<FieldError />);

    const element = screen.getByTestId('field-error');
    expect(element).toBeInTheDocument();
  });
});
