import { render, screen } from '@testing-library/react';
import classes from '../FormField.module.scss';
import { FieldContent } from '../FieldContent';

describe('FieldContent', () => {
  it('renders without crashing', () => {
    render(<FieldContent>Child</FieldContent>);

    const element = screen.getByText('Child');
    expect(element).toBeInTheDocument();
    expect(element).toHaveClass(classes.content);
  });
  it('renders with passed class', () => {
    render(<FieldContent className="test-class">Child</FieldContent>);

    const element = screen.getByText('Child');
    expect(element).toHaveClass(classes.content, 'test-class');
  });
  it('uses overflow strategy', () => {
    render(<FieldContent overflow="ellipsis">Child</FieldContent>);

    const element = screen.getByText('Child');
    expect(element).toHaveClass(classes.ellipsis);
  });
});
