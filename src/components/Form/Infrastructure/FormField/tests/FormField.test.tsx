import { render, screen } from '@testing-library/react';
import classes from '../FormField.module.scss';
import { FormField } from '../FormField';
import { Variant } from '../types';

describe('FormField', () => {
  it('renders without crashing', () => {
    render(<FormField>Child</FormField>);

    const element = screen.getByText('Child');
    expect(element).toBeInTheDocument();
    expect(element).toHaveClass(classes.formField);
  });
  it.each<[Variant]>([['regular'], ['has-border'], ['has-error'], ['success']])(
    'renders with classes of variant %s',
    (variant: Variant) => {
      render(<FormField variant={variant}>Child</FormField>);

      const element = screen.getByText('Child');
      expect(element).toHaveClass(classes[variant]);
    },
  );
  it('renders with passed class', () => {
    render(<FormField className="test-class">Child</FormField>);

    const element = screen.getByText('Child');
    expect(element).toHaveClass(classes.formField, 'test-class');
  });
});
