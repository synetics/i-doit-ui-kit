import { render, screen } from '@testing-library/react';
import classes from '../FormField.module.scss';
import { FieldExtra } from '../FieldExtra';

describe('FieldExtra', () => {
  it('renders without crashing', () => {
    render(<FieldExtra>Child</FieldExtra>);

    const element = screen.getByText('Child');
    expect(element).toBeInTheDocument();
    expect(element).toHaveClass(classes.extra);
  });
  it('renders with passed class', () => {
    render(<FieldExtra className="test-class">Child</FieldExtra>);

    const element = screen.getByText('Child');
    expect(element).toHaveClass(classes.extra, 'test-class');
  });
});
