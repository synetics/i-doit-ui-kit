export type Variant = 'regular' | 'has-border' | 'has-error' | 'success';

export type WithClassName = {
  className?: string;
};

export type ActivatableProps = {
  /**
   * Is field active
   */
  active?: boolean;

  /**
   * Control field's active state
   */
  setActive?: (value: boolean) => void;
};

export type ViewProps = {
  /**
   * View variant of the field
   */
  variant?: Variant;

  /**
   * Is field active
   */
  active?: boolean;

  /**
   * Is field disabled
   */
  disabled?: boolean;

  /**
   * Is field read only
   */
  readonly?: boolean;
  /**
   * If field don't need  to unset the style
   */
  initial?: boolean;
};

export type IdentifiableProps = {
  /**
   * Id of field
   */
  id: string;
};

export type FieldProps = WithClassName & ViewProps & ActivatableProps;

export type ValueProps<T> = {
  /**
   * Value
   */
  value?: T;
  /**
   * Callback on change
   * @param {T} value
   */
  onChange?: (value: T) => void;
};
