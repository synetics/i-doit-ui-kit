import React from 'react';
import classnames from 'classnames';
import { ErrorType } from '../useFormFields/types';
import { FieldConfiguration } from './types';
import { Field } from './Field';
import classes from './FormFields.module.scss';

type FormFieldsType<T> = {
  className?: string;
  fieldClassName?: string;
  configuration: FieldConfiguration<T, string & keyof T>[];
  value: T;
  onChange: <K extends keyof T>(field: K, value: T[K]) => void;
  errors: ErrorType<T>;
  stickyError?: boolean;
  vertical?: boolean;
};

export const FormFields = function <T>({
  className,
  fieldClassName,
  configuration,
  errors,
  value,
  onChange,
  stickyError = false,
  vertical = false,
}: FormFieldsType<T>) {
  const handleChange =
    <K extends string & keyof T>(field: K) =>
    (v: T[K]) =>
      onChange(field, v);
  return (
    <div className={classnames(className, { [classes.vertical]: vertical })} data-testid="form-fields">
      {configuration.map((fieldConfiguration) => {
        const { key } = fieldConfiguration;
        const fieldValue = value[key];
        const error = errors[key];
        return (
          <Field
            key={key}
            configuration={fieldConfiguration}
            value={fieldValue}
            error={error}
            stickyError={stickyError}
            className={fieldClassName}
            onChange={handleChange(key)}
          />
        );
      })}
    </div>
  );
};
