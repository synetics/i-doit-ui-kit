import { render, screen } from '@testing-library/react';
import user from '@testing-library/user-event';
import { FormFields } from './FormFields';
import classes from './FormFields.module.scss';

describe('FormFields', () => {
  const configuration = [
    {
      key: 'one' as const,
      label: 'my label',
      renderControl: (v?: string, onChange?: (v: string) => void) => (
        <input data-testid="one" value={v} onChange={(e) => onChange?.(e.target.value)} />
      ),
    },
  ];

  it('renders without crashing', () => {
    render(<FormFields value={{}} errors={{}} configuration={[]} onChange={() => null} />);

    const element = screen.getByTestId('form-fields');
    expect(element).toBeInTheDocument();
  });

  it('renders with vertical layout', () => {
    render(<FormFields value={{}} errors={{}} configuration={[]} onChange={() => null} vertical />);

    const element = screen.getByTestId('form-fields');
    expect(element).toBeInTheDocument();
    expect(element).toHaveClass(classes.vertical);
  });

  it('renders without error', async () => {
    const handleChange = jest.fn();
    render(<FormFields value={{ one: 'default' }} errors={{}} configuration={configuration} onChange={handleChange} />);

    const field = screen.getByDisplayValue('default');
    expect(field).toBeInTheDocument();
    const label = screen.getByText('my label');
    expect(label).toBeInTheDocument();
    const error = screen.queryByTestId('field-error');
    expect(error).not.toBeInTheDocument();
    expect(handleChange).not.toHaveBeenCalled();
    await user.click(field);
    await user.paste('example');

    expect(handleChange).toHaveBeenCalledWith('one', 'defaultexample');
  });

  it('renders with error', () => {
    const handleChange = jest.fn();
    render(
      <FormFields
        value={{ one: 'default' }}
        errors={{ one: 'some error' }}
        configuration={configuration}
        onChange={handleChange}
      />,
    );

    const error = screen.getByText('some error');
    expect(error).toBeInTheDocument();
  });
  it('renders with sticky error', () => {
    const handleChange = jest.fn();
    render(
      <FormFields
        value={{ one: 'default' }}
        errors={{ one: 'some error' }}
        configuration={configuration}
        onChange={handleChange}
        stickyError
      />,
    );

    const error = screen.getByText('some error');
    expect(error).toBeInTheDocument();
  });
});
