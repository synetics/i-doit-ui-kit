import { useMemo, useState } from 'react';
import faker from 'faker';
import { Canvas, Meta, Story } from '@storybook/blocks';
import { warning } from '../../../../icons';
import { Button } from '../../../Button';
import { Headline } from '../../../Headline';
import { BannerHeader } from '../../../Banner/BannerHeader';
import { Input, Password, SingleSelectComboBox, TextArea } from '../../Control';
import { useFormFields } from '../useFormFields';
import { FormFields } from './FormFields';

<Meta title="Components/Molecules/Form/Infrastructure/FormFields" component={FormFields} />

# FormFields

Form Fields renders the form fields according to the configuration.

It renders the fields according to configuration and passes the value, handles the change of the complete data.

FormFields passes the corresponding field value, onChange and errors to the needed single form field and apply default form group styling.

## Configuration

Configuration is an array of field definitions.

Each field definition has a key, label and the `renderControl` method.

```tsx
[
  {
    key: 'email',
    label: 'E-mail',
    renderControl: (value: string, onChange: (v: string) => void) => <Input value={value} onChange={onChange} />,
  },
  {
    key: 'username',
    label: 'User name',
    required: true,
    renderControl: (value: string, onChange: (v: string) => void) => <Input value={value} onChange={onChange} />,
  },
];
```

export const items = new Array(20)
  .fill(0)
  .map((_, i) => ({
    label: faker.address.city(),
    value: `value${i}`,
  }))
  .sort(({ label: a }, { label: b }) => a.localeCompare(b));

In the following example, you can see how to use the form fields.

The errors in this case use simple strategy - they validate input during typing.

Requirements of the fields:

- email has to contain `@` symbol
- password should be longer than 8 symbols
- text is required

<Canvas>
  <Story name="Usage">
    {() => {
      const [value, setValue] = useState({
        email: 'predefined',
        password: 'password',
        text: '',
      });
      const errors = useMemo(
        () => ({
          email: value.email.includes('@') ? null : 'Invalid e-mail',
          password: value.password.length <= 8 ? 'Password should be longer that 8 characters' : null,
          text: value.text.length === 0 ? 'This field is required' : null,
        }),
        [value],
      );
      return (
        <FormFields
          value={value}
          onChange={(field, v) =>
            setValue((a) => ({
              ...a,
              [field]: v,
            }))
          }
          configuration={[
            {
              key: 'email',
              label: 'E-mail',
              renderControl: (value, onChange) => <Input value={value} onChange={onChange} />,
            },
            {
              key: 'password',
              label: 'Password',
              renderControl: (value, onChange) => <Password value={value} onChange={onChange} />,
            },
            {
              key: 'text',
              label: 'Description',
              required: true,
              renderControl: (value, onChange) => <TextArea value={value} onChange={onChange} />,
            },
            {
              key: 'combobox',
              label: 'Combobox',
              required: true,
              renderControl: (value, onChange, ref) => (
                <SingleSelectComboBox
                  pulldownContainer={ref}
                  value={items[0]}
                  onChange={onChange}
                  items={items}
                  id="combobox"
                />
              ),
            },
          ]}
          errors={errors}
        />
      );
    }}
  </Story>
</Canvas>

## useFormFields hook

useFormFields hook makes handling of form with multiple fields easier.

You can specify the shape of your data and receive an API to work with it.

Example of usage:

```tsx
const [value, errors, fieldsApi] = useFormFields({ value: 'test' }, { value: 'error' });
```

where `fieldsApi` is an API object that allows you to:

- change field using `fieldsApi.change(field)(fieldValue)`
- set error for field: `fieldsApi.setError(field)(error)`
- set all errors: `fieldsApi.setErrors({ value: 'new error', other: 'another' })`

<Canvas>
  <Story name="Usage with useFormFields">
    {() => {
      const [value, errors, { setError, change }] = useFormFields({
        email: 'predefined',
        password: 'password',
        text: '',
      });
      const handleSubmit = (e) => {
        e.preventDefault();
        if (!value.email.includes('@')) {
          setError('email')('Invalid e-mail');
        }
        if (value.password.length <= 8) {
          setError('password')('Password should be longer that 8 characters');
        }
        if (!value.text) {
          setError('text')('This field is required');
        }
      };
      return (
        <form onSubmit={handleSubmit}>
          <FormFields
            value={value}
            onChange={(field, v) => change(field)(v)}
            configuration={[
              {
                key: 'email',
                label: 'E-mail',
                renderControl: (value, onChange) => <Input value={value} onChange={onChange} />,
              },
              {
                key: 'password',
                label: 'Password',
                renderControl: (value, onChange) => <Password value={value} onChange={onChange} />,
              },
              {
                key: 'text',
                label: 'Description',
                required: true,
                renderControl: (value, onChange) => <TextArea value={value} onChange={onChange} />,
              },
            ]}
            errors={errors}
          />
          <Button type="submit" onSubmit={handleSubmit}>
            Submit
          </Button>
        </form>
      );
    }}
  </Story>
</Canvas>

## Multiple forms

You can define multiple form fields to create complex forms.

Vertical alignment allows you to place fields next to each other.

<Canvas>
  <Story name="Complex form">
    {() => {
      const [value, errors, { setError, change }] = useFormFields({
        firstname: '',
        lastname: '',
        password: 'password',
        email: '',
        phone: '',
        text: '',
      });
      const handleSubmit = (e) => {
        e.preventDefault();
        e.stopPropagation();
        if (!value.email.includes('@')) {
          setError('email')('Invalid e-mail');
        }
        if (value.password.length <= 8) {
          setError('password')('Password should be longer that 8 characters');
        }
        if (!value.firstname) {
          setError('firstname')('This field is required');
        }
        if (!value.lastname) {
          setError('lastname')('This field is required');
        }
      };
      return (
        <form onSubmit={handleSubmit}>
          <Headline as="h1" className="mb-6">
            User information
          </Headline>
          {Object.keys(errors).length > 0 && (
            <BannerHeader variant="error" headerIcon={warning} className="rounded my-6">
              An error occured please check the highlighted input fields
            </BannerHeader>
          )}
          <Headline as="h2" className="mb-6">
            Basic information
          </Headline>
          <FormFields
            value={value}
            onChange={(field, v) => change(field)(v)}
            configuration={[
              {
                key: 'firstname',
                label: 'First name',
                renderControl: (value, onChange) => (
                  <Input placeholder="Enter first name" value={value} onChange={onChange} />
                ),
              },
              {
                key: 'lastname',
                label: 'Last name',
                renderControl: (value, onChange) => (
                  <Input placeholder="Enter last name" value={value} onChange={onChange} />
                ),
              },
            ]}
            errors={errors}
            vertical
          />
          <Headline as="h2" className="mb-6">
            Contact information
          </Headline>
          <FormFields
            value={value}
            onChange={(field, v) => change(field)(v)}
            vertical
            configuration={[
              {
                key: 'email',
                label: 'E-mail',
                required: true,
                renderControl: (value, onChange) => (
                  <Input placeholder="Enter e-mail" value={value} onChange={onChange} />
                ),
              },
              {
                key: 'phone',
                label: 'Phone',
                renderControl: (value, onChange) => (
                  <Input placeholder="Enter phone" value={value} onChange={onChange} />
                ),
              },
              {
                key: 'businessPhone',
                label: 'Business phone',
                renderControl: (value, onChange) => (
                  <Input placeholder="Enter business phone" value={value} onChange={onChange} />
                ),
              },
            ]}
            errors={errors}
          />
          <Headline as="h2" className="mb-6">
            Security
          </Headline>
          <FormFields
            value={value}
            onChange={(field, v) => change(field)(v)}
            configuration={[
              {
                key: 'username',
                label: 'Username',
                renderControl: (value, onChange) => (
                  <Input placeholder="Enter username" value={value} onChange={onChange} />
                ),
              },
              {
                key: 'password',
                label: 'Password',
                renderControl: (value, onChange) => (
                  <Password placeholder="Enter password" value={value} onChange={onChange} />
                ),
              },
            ]}
            errors={errors}
          />
          <hr className="my-4" />
          <FormFields
            value={value}
            onChange={(field, v) => change(field)(v)}
            configuration={[
              {
                key: 'text',
                label: 'Description',
                renderControl: (value, onChange) => <TextArea value={value} onChange={onChange} />,
              },
            ]}
            errors={errors}
          />
          <Button type="submit" onSubmit={handleSubmit}>
            Submit
          </Button>
        </form>
      );
    }}
  </Story>
</Canvas>
