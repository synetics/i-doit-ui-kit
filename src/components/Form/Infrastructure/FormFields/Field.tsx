import React from 'react';
import classnames from 'classnames';
import { useCallbackRef } from '../../../../hooks/use-callback-ref';
import { FieldError, ValueProps, WithClassName } from '../FormField';
import { Label } from '../Label';
import { FieldGroup } from '../FieldGroup';
import { Error } from '../Error';
import { FormGroup } from '../FormGroup';
import { FieldConfiguration } from './types';
import classes from './FormFields.module.scss';

type FieldType<T, K extends string & keyof T> = ValueProps<T[K]> &
  WithClassName & {
    error?: string | null;
    configuration: FieldConfiguration<T, K>;
    stickyError: boolean;
  };

export const Field = <T, K extends string & keyof T>({
  value,
  error,
  onChange,
  className,
  stickyError,
  configuration: { key, required, label, renderControl },
}: FieldType<T, K>) => {
  const [ref, setRef] = useCallbackRef<Element>();
  return (
    <FormGroup className={classnames(classes.field, className)}>
      <Label id={key} required={required}>
        {label}
      </Label>
      <FieldGroup ref={setRef} variant={error ? 'has-error' : 'has-border'}>
        {renderControl(value, onChange, ref || undefined)}
        {error && <FieldError className={classes.icon} />}
      </FieldGroup>
      {error && (
        <Error id={key} sticky={stickyError} className={classes.error}>
          {error}
        </Error>
      )}
    </FormGroup>
  );
};
