import { ReactNode } from 'react';

export type FieldConfiguration<T, K extends string & keyof T> = {
  key: K;
  label: string;
  required?: boolean;
  renderControl: (value?: T[K], onChange?: (v: T[K]) => void, ref?: Element) => ReactNode;
};
