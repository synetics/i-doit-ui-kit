import { ActionType, StateType } from './types';
import { ValueReducer } from './reducer';

describe('ValueReducer', () => {
  it('check default case', () => {
    const initialState: StateType<{ name: string }> = {
      value: { name: '' },
      errors: {},
    };
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    const invalidAction: ActionType<{ name: string }> = {};
    expect(ValueReducer(initialState, invalidAction)).toEqual(initialState);
  });
});
