import { act, renderHook } from '@testing-library/react-hooks';
import { useFormFields } from './use-form-fields';

describe('use form fields', () => {
  const defaultValue = { one: 'value1', second: 'value2' };
  it('has correct initial state', () => {
    const { result } = renderHook(() => useFormFields<typeof defaultValue>(defaultValue, { one: 'error' }));
    const [value, setErrors] = result.current;
    expect(value).toStrictEqual(defaultValue);
    expect(setErrors).toStrictEqual({ one: 'error' });
  });

  it('changes field and clears error', () => {
    const { result } = renderHook(() => useFormFields<typeof defaultValue>(defaultValue, { one: 'error' }));
    const api = result.current[2];
    act(() => api.change('one')('new value'));
    const [value, setErrors] = result.current;
    expect(value).toStrictEqual({ one: 'new value', second: 'value2' });
    expect(setErrors).toStrictEqual({});
  });

  it('set error', () => {
    const { result } = renderHook(() => useFormFields<typeof defaultValue>(defaultValue));
    const api = result.current[2];
    act(() => api.setError('one')('new error'));
    const [value, setErrors] = result.current;
    expect(value).toStrictEqual(defaultValue);
    expect(setErrors).toStrictEqual({ one: 'new error' });
  });

  it('set multiple setErrors', () => {
    const { result } = renderHook(() => useFormFields<typeof defaultValue>(defaultValue));
    const api = result.current[2];
    act(() => api.setErrors({ one: 'new error', second: 'another error' }));
    const [value, setErrors] = result.current;
    expect(value).toStrictEqual(defaultValue);
    expect(setErrors).toStrictEqual({ one: 'new error', second: 'another error' });
  });

  it('set multiple setErrors and single', () => {
    const { result } = renderHook(() => useFormFields<typeof defaultValue>(defaultValue));
    const api = result.current[2];
    act(() => {
      api.setErrors({ one: 'new error', second: 'another error' });
      api.setError('second')('error');
    });
    const [value, setErrors] = result.current;
    expect(value).toStrictEqual(defaultValue);
    expect(setErrors).toStrictEqual({ one: 'new error', second: 'error' });
  });
});
