import { ActionType, ErrorType, StateType } from './types';

export const ValueReducer = <T>(v: StateType<T>, change: ActionType<T>): StateType<T> => {
  switch (change.type) {
    case 'set-field':
      return {
        ...v,
        value: {
          ...v.value,
          [change.key]: change.value,
        },
        errors: {} as ErrorType<T>,
      };
    case 'set-error':
      return {
        ...v,
        errors: {
          ...v.errors,
          [change.key]: change.value,
        },
      };
    case 'errors':
      return {
        ...v,
        errors: change.value,
      };
    default:
      return v;
  }
};
