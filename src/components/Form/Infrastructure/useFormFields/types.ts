export type ErrorType<T> = {
  [K in keyof T]?: string | null;
};

type ValueType<T> = {
  [K in keyof T]?: T[K];
};

export type StateType<T> = {
  value: T;
  errors: ErrorType<T>;
};

type SetValue<T, K extends keyof T> = {
  type: 'set-field';
  key: K;
  value: ValueType<T[K]>;
};

type SetError<K> = {
  type: 'set-error';
  key: K;
  value: string | null;
};

type SetErrors<T> = {
  type: 'errors';
  value: ErrorType<T>;
};

export type ActionType<T> = SetValue<T, keyof T> | SetErrors<T> | SetError<keyof T>;
