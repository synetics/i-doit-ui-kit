import { useMemo, useReducer } from 'react';
import { ActionType, ErrorType, StateType } from './types';
import { ValueReducer } from './reducer';

type FieldsApi<T> = {
  change: <K extends keyof T>(field: K) => (value: T[K]) => void;
  setError: <K extends keyof T>(field: K) => (error: string | null) => void;
  setErrors: (errors: ErrorType<T>) => void;
};

type UseFieldsReturnType<T> = [T, ErrorType<T>, FieldsApi<T>];

export const useFormFields = <T>(
  initialValue: T,
  initialErrors: ErrorType<T> = {} as ErrorType<T>,
): UseFieldsReturnType<T> => {
  const [{ value, errors }, dispatch] = useReducer(
    (v: StateType<T>, change: ActionType<T>) => ValueReducer(v, change),
    { value: initialValue, errors: initialErrors },
  );

  const api = useMemo(
    () => ({
      change:
        <K extends keyof T>(field: K) =>
        (v: T[K]) => {
          dispatch({
            type: 'set-field',
            key: field,
            value: v,
          });
        },
      setError:
        <K extends keyof T>(field: K) =>
        (v: string | null) =>
          dispatch({
            type: 'set-error',
            key: field,
            value: v,
          }),
      setErrors: (v: ErrorType<T>) =>
        dispatch({
          type: 'errors',
          value: v,
        }),
    }),
    [],
  );

  return [value, errors, api];
};
