import { forwardRef } from 'react';
import classnames from 'classnames';
import { Container, ContainerProps } from '../../../Container';
import classes from './FormGroup.module.scss';

const FormGroup = forwardRef<unknown, ContainerProps>(({ className, children, ...props }, ref) => (
  <Container {...props} ref={ref} className={classnames(classes.formGroup, className)}>
    {children}
  </Container>
));

export { FormGroup };
