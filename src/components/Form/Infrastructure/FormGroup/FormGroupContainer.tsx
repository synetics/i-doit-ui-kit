import React, { ReactNode } from 'react';
import classnames from 'classnames';
import styles from './FormGroup.module.scss';

type FormGroupContainerProps = {
  /**
   * Container class name
   */
  className?: string;
  /**
   * Vertical layout
   */
  vertical: boolean;
  /**
   * Content of the label
   */
  children: ReactNode;
};

const FormGroupContainer = ({ vertical, children, className }: FormGroupContainerProps) => (
  <div className={classnames(className, styles.formGroupContainer, { [styles.vertical]: vertical })}>{children}</div>
);

export { FormGroupContainer };
