import { render, screen } from '@testing-library/react';
import classes from './FormGroup.module.scss';
import { FormGroupContainer } from './FormGroupContainer';

describe('FormGroupContainer', () => {
  it('renders without crashing', () => {
    render(
      <FormGroupContainer vertical>
        <div>Child</div>
      </FormGroupContainer>,
    );

    const element = screen.getByText('Child');
    expect(element).toBeInTheDocument();
    expect(element.parentElement).toHaveClass(classes.formGroupContainer);
    expect(element.parentElement).toHaveClass(classes.vertical);
  });
});
