import { render, screen } from '@testing-library/react';
import classes from './FormGroup.module.scss';
import { FormGroup } from './FormGroup';

describe('FormGroup', () => {
  it('renders without crashing', () => {
    render(<FormGroup>Child</FormGroup>);

    const element = screen.getByText('Child');
    expect(element).toBeInTheDocument();
    expect(element).toHaveClass(classes.formGroup);
  });
  it('renders with passed class', () => {
    render(<FormGroup className="test-class">Child</FormGroup>);

    const element = screen.getByText('Child');
    expect(element).toHaveClass(classes.formGroup, 'test-class');
  });
});
