import { name } from 'faker';
import { ReactNode, useState } from 'react';
import { Item } from '../Menu/components';
import { Wrapper } from '../Modal';
import { AvatarPulldown } from './AvatarPulldown';

const items = new Array(70).fill(0).map((_, i) => ({
  label: name.firstName()[0] + name.lastName()[0],
  id: `id${i}`,
}));

export default {
  title: 'Components/Atoms/Buttons/AvatarPulldown',
  component: AvatarPulldown,
};

/**
 *
 * Regular
 *
 */

export const AvatarPulldownComponent = (): ReactNode => {
  const [menuLabel, setMenuLabel] = useState('AA');
  return (
    <div
      style={{
        minHeight: 600,
        padding: 20,
      }}
    >
      <AvatarPulldown label={menuLabel}>
        {(onClose) => (
          <>
            <Wrapper
              className="p-0 border-0"
              style={{
                maxHeight: 300,
                overflow: 'auto',
                display: 'flex',
                flexDirection: 'column',
              }}
            >
              {items.map(({ label, id }) => (
                <Item
                  id={id}
                  label={label}
                  onSelect={() => {
                    onClose();
                    setMenuLabel(label);
                  }}
                />
              ))}
            </Wrapper>
          </>
        )}
      </AvatarPulldown>
    </div>
  );
};
