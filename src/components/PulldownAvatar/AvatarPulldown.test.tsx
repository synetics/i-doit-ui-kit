import { render, screen } from '@testing-library/react';
import { AvatarPulldown } from './AvatarPulldown';

describe('SubMenu', () => {
  it('renders with content inside without crashing', () => {
    render(<AvatarPulldown label="Label">{() => <div>Test</div>}</AvatarPulldown>);
    expect(screen.getByText('Label')).toBeInTheDocument();
  });
});
