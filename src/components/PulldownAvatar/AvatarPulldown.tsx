import React, { forwardRef, MouseEventHandler, ReactNode } from 'react';
import classnames from 'classnames';
import { useGlobalizedRef } from '../../hooks';
import { ButtonPulldown } from '../ButtonPulldown';
import { Button } from '../Button';
import classes from './AvatarPulldown.module.scss';

type NavigationItemType = {
  children: (onClose: () => void) => ReactNode;
  label: string;
  className?: string;
};

type TriggerProps = {
  activeState?: boolean;
  hovered?: boolean;
  onClick?: MouseEventHandler;
  label?: string;
  className?: string;
};

const Trigger = forwardRef<HTMLButtonElement, TriggerProps>(
  ({ activeState, hovered, label, className, onClick, ...props }: TriggerProps, ref) => {
    const { setRefsCallback } = useGlobalizedRef(ref);
    return (
      <Button
        {...props}
        ref={setRefsCallback}
        onClick={onClick}
        activeState={activeState}
        className={classnames(classes.button, className, { [classes.open]: activeState })}
      >
        {label}
      </Button>
    );
  },
);

export const AvatarPulldown = ({ children, label, className, ...props }: NavigationItemType) => (
  <ButtonPulldown
    className={className}
    containerClassName={classes.container}
    component={Trigger}
    label={label}
    modifiers={[
      {
        name: 'offset',
        options: {
          offset: [-4, 4],
        },
      },
    ]}
    {...props}
  >
    {children}
  </ButtonPulldown>
);
