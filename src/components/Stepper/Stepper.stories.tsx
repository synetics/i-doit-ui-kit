import React, { useState } from 'react';
import { Input } from '../Form/Control';
import { FormGroup, Label } from '../Form/Infrastructure';
import { bug } from '../../icons';
import { Text } from '../Text';
import { Button, ButtonSpacer } from '../Button';
import { OverflowWithTooltip } from '../OverflowWithTooltip';
import { SubLine } from '../List/Item';
import { useSelectInValues } from '../../stories';
import { Stepper } from './Stepper';
import { IconStep, SimpleStep, Step } from './Steps';
import { LabelStep } from './Steps/LabelStep';

export default {
  title: 'Components/Molecules/Stepper',
  component: Stepper,
};

const StepSelector = ({
  max,
  value,
  onChange,
  label = 'Step',
}: {
  label?: string;
  max?: number;
  value: number;
  onChange: (value: number) => void;
}) => (
  <FormGroup className="mb-5 w-25">
    <Label id="step">{label}</Label>
    <Input id="step" value={value} onChange={(v) => onChange(+v)} type="number" min={0} max={max} />
  </FormGroup>
);

/**
 * Stepper shows user the progress within some process.
 *
 * Bullet points represent the steps needed to do to finish the process.
 *
 * Stepper is built using compound components. Stepper container takes care about displaying its items with
 * progress bars and applying the status of the step on each step.
 *
 * Child component receives the properties `current` and `done` and are able to style them accordingly.
 */
export const StepperWithDifferentSteps = () => {
  const [step, setStep] = useState(1);
  return (
    <div>
      <StepSelector value={step} onChange={setStep} />
      <Stepper progress={step}>
        <SimpleStep size="md" />
        <Text className="text-nowrap p-4" fontSize="large" fontWeight="semi-bold" tag="span">
          Step
        </Text>
        <LabelStep icon={bug}>
          <Text fontWeight="semi-bold" fontSize="large">
            Step 3
          </Text>
          <SubLine>More information</SubLine>
        </LabelStep>
        <IconStep icon={bug} size="md" />
      </Stepper>
    </div>
  );
};

const sizes = {
  xs: 'xs',
  sm: 'sm',
  md: 'md',
  lg: 'lg',
} as const;

/**
 * There are some standard implementations of steps.
 *
 * SimpleStep just shows the state of the state.
 *
 * IconStep displays the icon within the step.
 *
 * LabelStep allows us to show custom content.
 *
 * SimpleStep and IconStep are based on
 */
export const StepVariants = () => {
  const [state, stateView] = useSelectInValues({
    label: 'State',
    defaultValue: 'done',
    values: {
      Done: 'done',
      Active: 'active',
      'Not done': 'not done',
    },
  });
  const [size, sizeView] = useSelectInValues<string & keyof typeof sizes>({
    label: 'Size',
    defaultValue: 'md',
    values: sizes,
  });
  const current = state === 'active';
  const done = state === 'done';

  return (
    <>
      <ButtonSpacer gridSize="m">
        {stateView}
        {sizeView}
      </ButtonSpacer>
      <ButtonSpacer gridSize="l">
        <SimpleStep size={size ?? 'sm'} current={current} done={done} />
        <IconStep icon={bug} size={size ?? 'sm'} current={current} done={done} />
        <LabelStep icon={bug} current={current} done={done}>
          Step content
        </LabelStep>
        <LabelStep icon={bug} current={current} done={done}>
          <Text fontSize="large" fontWeight="semi-bold">
            Label
          </Text>
          <SubLine>Subline</SubLine>
        </LabelStep>
      </ButtonSpacer>
    </>
  );
};

/**
 * Stepper allows you to pass different step elements to configure best experience for the user
 */
export const StepperWithDifferentTypes = () => {
  const [count, setCount] = useState(5);
  const [step, setStep] = useState(1);
  return (
    <div className="d-flex flex-column">
      <StepSelector label="Number of steps" value={count} onChange={setCount} />
      <StepSelector max={count} value={step} onChange={setStep} />
      <Stepper progress={step} className="mb-5">
        {new Array(count).fill(0).map(() => (
          <SimpleStep size="sm" />
        ))}
      </Stepper>
      <Stepper progress={step} className="mb-5">
        {new Array(count).fill(0).map((_, i) => (
          <LabelStep icon={bug}>
            <>Step {i + 1}</>
            <div style={{ maxWidth: 100, overflow: 'hidden' }}>
              <SubLine>
                <OverflowWithTooltip>More information about step number {i + 1}</OverflowWithTooltip>
              </SubLine>
            </div>
          </LabelStep>
        ))}
      </Stepper>
      <Stepper progress={step} className="mb-5">
        {new Array(count).fill(0).map(() => (
          <IconStep icon={bug} size="lg" />
        ))}
      </Stepper>
      <Stepper progress={step} className="mb-5">
        {new Array(count).fill(0).map((_, i) => (
          <Step size="lg">
            <Text fontSize="large" fontWeight="semi-bold">
              Step {i}
            </Text>
          </Step>
        ))}
      </Stepper>
      <Stepper progress={step}>
        {new Array(count).fill(0).map((_, i) => (
          <Button onClick={() => setStep(i)} className="text-nowrap" variant={i <= step ? 'primary' : 'outline'}>
            Step {i}
          </Button>
        ))}
      </Stepper>
    </div>
  );
};

/**
 * The stepper might be used in vertical context.
 */
export const VerticalVariant = () => {
  const [count, setCount] = useState(5);
  const [step, setStep] = useState(1);
  return (
    <div className="d-flex flex-column">
      <StepSelector label="Number of steps" value={count} onChange={setCount} />
      <StepSelector value={step} onChange={setStep} />
      <Stepper style={{ height: 500, alignSelf: 'start' }} progress={step} vertical>
        {new Array(count).fill(0).map(() => (
          <SimpleStep size="sm" />
        ))}
      </Stepper>
    </div>
  );
};
