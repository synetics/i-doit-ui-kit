import { Children, cloneElement, isValidElement, ReactNode } from 'react';
import classnames from 'classnames';
import { Progress } from '../Progress';
import { Container, ContainerProps } from '../Container';
import classes from './Stepper.module.scss';

type NewStepperProps = ContainerProps & {
  progress: number;
  vertical?: boolean;
  className?: string;
  children: ReactNode[];
  size?: 'xs' | 'sm' | 'md' | 'lg';
};

export const Stepper = ({ size = 'xs', vertical, progress, className, children, ...rest }: NewStepperProps) => (
  <Container
    {...rest}
    className={classnames(className, classes.container, {
      [classes.vertical]: vertical,
      [classes.horizontal]: !vertical,
    })}
  >
    {Children.map(children, (a, i) => (
      <>
        {isValidElement(a) &&
          cloneElement(a, {
            current: i === progress,
            done: i < progress,
          })}
        {i !== children.length - 1 && (
          <Progress
            className={classes.progress}
            size={size}
            orientation={vertical ? 'vertical' : 'horizontal'}
            value={progress - i}
            total={1}
          />
        )}
      </>
    ))}
  </Container>
);
