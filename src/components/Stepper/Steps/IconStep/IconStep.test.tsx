import { render, screen } from '@testing-library/react';
import { bug } from '../../../../icons';
import { IconStep } from './IconStep';

describe('IconStep', () => {
  it('renders a step with children', () => {
    render(<IconStep size="sm" dataTestId="step" icon={bug} />);

    expect(screen.getByTestId('step')).toBeInTheDocument();
  });

  it('renders a step as current', () => {
    render(<IconStep dataTestId="step" icon={bug} current />);

    const step = screen.getByTestId('step');

    expect(step).toBeInTheDocument();
    expect(step).toHaveClass('active');
  });

  it('renders a step as done', () => {
    render(<IconStep dataTestId="step" icon={bug} done />);

    const step = screen.getByTestId('step');

    expect(step).toBeInTheDocument();
    expect(step).toHaveClass('completed');
  });
});
