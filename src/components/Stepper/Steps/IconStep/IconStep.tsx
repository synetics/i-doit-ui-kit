import React from 'react';
import classnames from 'classnames';
import { Icon } from '../../../Icon';
import { Step, StepProps } from '../Step';
import { sizes } from '../consts';
import classes from './IconStep.module.scss';

type IconStepProps = Omit<StepProps, 'children'> & {
  icon: string;
};

export const IconStep = ({ icon, className, ...props }: IconStepProps) => {
  const size = sizes[props.size ?? 'xs'];

  return (
    <Step
      {...props}
      className={classnames(classes.step, className, {
        [classes.current]: props.current,
        [classes.done]: props.done,
      })}
    >
      <Icon src={icon} width={size} height={size} className={classes.icon} />
    </Step>
  );
};
