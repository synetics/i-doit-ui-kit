import classnames from 'classnames';
import React, { ReactNode } from 'react';
import { Container, ContainerProps } from '../../../Container';
import classes from './Step.module.scss';

export type StepProps = Omit<ContainerProps, 'children'> & {
  current?: boolean;
  done?: boolean;
  size?: 'xs' | 'sm' | 'md' | 'lg';
  children?: ReactNode;
};

export const Step = ({ children, size = 'sm', className, current = false, done = false, ...props }: StepProps) => (
  <Container
    {...props}
    className={classnames(className, classes[size], classes.stepItem, {
      [classes.active]: current,
      [classes.completed]: done,
    })}
  >
    {children}
  </Container>
);
