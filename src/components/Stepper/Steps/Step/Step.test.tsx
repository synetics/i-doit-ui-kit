import { render, screen } from '@testing-library/react';
import { Step } from './Step';

describe('Step', () => {
  it('renders a step with children', () => {
    render(<Step>One</Step>);

    expect(screen.getByText('One')).toBeInTheDocument();
  });

  it('renders a step as current', () => {
    render(<Step current>One</Step>);

    const step = screen.getByText('One');

    expect(step).toBeInTheDocument();
    expect(step).toHaveClass('active');
  });

  it('renders a step as done', () => {
    render(<Step done>One</Step>);

    const step = screen.getByText('One');

    expect(step).toBeInTheDocument();
    expect(step).toHaveClass('completed');
  });
});
