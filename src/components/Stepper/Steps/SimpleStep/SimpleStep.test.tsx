import { render, screen } from '@testing-library/react';
import { SimpleStep } from './SimpleStep';

describe('SimpleStep', () => {
  it('renders a step with children', () => {
    render(<SimpleStep size="sm" dataTestId="stepper" />);

    expect(screen.getByTestId('stepper')).toBeInTheDocument();
  });

  it('renders a step as current', () => {
    render(<SimpleStep dataTestId="stepper" current />);

    const step = screen.getByTestId('stepper');

    expect(step).toBeInTheDocument();
    expect(step).toHaveClass('active');
  });

  it('renders a step as done', () => {
    render(<SimpleStep dataTestId="stepper" done />);

    const step = screen.getByTestId('stepper');

    expect(step).toBeInTheDocument();
    expect(step).toHaveClass('completed');
  });
});
