import React from 'react';
import classnames from 'classnames';
import { check } from '../../../../icons';
import { Icon } from '../../../Icon';
import { Step, StepProps } from '../Step';
import { sizes } from '../consts';
import classes from './SimpleStep.module.scss';

export const SimpleStep = ({ done, current, size = 'xs', className, ...props }: Omit<StepProps, 'children'>) => (
  <Step {...props} size={size} done={done} current={current} className={className}>
    <div
      className={classnames(classes.content, {
        [classes.current]: current,
        [classes.done]: done,
      })}
    >
      {done ? <Icon src={check} fill="white" width={sizes[size]} height={sizes[size]} /> : null}
    </div>
  </Step>
);
