export const sizes = {
  xs: 12,
  sm: 16,
  md: 20,
  lg: 32,
};
