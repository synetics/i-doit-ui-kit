import { render, screen } from '@testing-library/react';
import { bug } from '../../../../icons';
import { LabelStep } from './LabelStep';

describe('LabelStep', () => {
  it('renders a step with children', () => {
    render(<LabelStep dataTestId="step">One</LabelStep>);

    expect(screen.getByText('One')).toBeInTheDocument();
  });

  it('renders a step with icon', () => {
    render(
      <LabelStep dataTestId="step" icon={bug}>
        One
      </LabelStep>,
    );

    expect(screen.getByText('One')).toBeInTheDocument();
    expect(screen.getByTestId('icon')).toBeInTheDocument();
  });

  it('renders a step as current', () => {
    render(
      <LabelStep dataTestId="step" current>
        One
      </LabelStep>,
    );

    const step = screen.getByTestId('step');

    expect(step).toHaveClass('current');
  });

  it('renders with icon className', () => {
    render(
      <LabelStep dataTestId="step" iconClassName="iconClass" icon={bug}>
        One
      </LabelStep>,
    );

    const step = screen.getByTestId('icon');

    expect(step).toHaveClass('iconClass');
  });

  it('renders a step as done', () => {
    render(
      <LabelStep dataTestId="step" done>
        One
      </LabelStep>,
    );

    const step = screen.getByTestId('step');

    expect(step).toHaveClass('done');
  });
});
