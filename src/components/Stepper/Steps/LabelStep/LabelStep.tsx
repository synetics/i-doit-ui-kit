import { ReactNode } from 'react';
import classnames from 'classnames';
import { Icon } from '../../../Icon';
import { Container, ContainerProps } from '../../../Container';
import classes from './LabelStep.module.scss';

type LabelStepProps = Omit<ContainerProps, 'children'> & {
  current?: boolean;
  done?: boolean;
  icon?: string;
  iconClassName?: string;
  children: ReactNode;
};

export const LabelStep = ({ children, icon, current, done, className, iconClassName, ...rest }: LabelStepProps) => (
  <Container
    {...rest}
    className={classnames(classes.content, className, {
      [classes.current]: current,
      [classes.done]: done,
    })}
  >
    {icon && <Icon className={classnames(classes.icon, iconClassName)} src={icon} />}
    <div className={classes.text}>{children}</div>
  </Container>
);
