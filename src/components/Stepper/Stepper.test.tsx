import { render, screen } from '@testing-library/react';
import { Stepper } from './Stepper';
import { Step } from './Steps';

describe('Stepper', () => {
  it('renders a stepper with children', () => {
    render(
      <Stepper progress={1}>
        <Step>One</Step>
        <Step>Two</Step>
        <Step>Three</Step>
      </Stepper>,
    );

    expect(screen.getByText('One')).toBeInTheDocument();
    expect(screen.getByText('Two')).toBeInTheDocument();
    expect(screen.getByText('Three')).toBeInTheDocument();
    expect(screen.queryAllByRole('progressbar')).toHaveLength(2);
  });
  it('renders a stepper vertically', () => {
    render(
      <Stepper dataTestId="stepper" progress={1} vertical>
        <Step>One</Step>
        <Step>Two</Step>
        <Step>Three</Step>
      </Stepper>,
    );
    const stepper = screen.getByTestId('stepper');
    expect(stepper).toHaveClass('vertical');
  });
});
