import { render, screen } from '@testing-library/react';
import React from 'react';
import { Spinner } from './Spinner';

describe('Spinner', () => {
  it('renders without crashing', () => {
    render(<Spinner />);

    expect(screen.getByRole('status')).toBeInTheDocument();
  });

  it('displays a message to screen readers', () => {
    render(<Spinner />);

    expect(screen.getByText('Loading...')).toHaveClass('sr-only');
  });

  it('adds an extra className', () => {
    const className = 'className';
    render(<Spinner className={className} />);
    const spinner = screen.getByRole('status');

    expect(spinner).toHaveClass(className);
  });
});
