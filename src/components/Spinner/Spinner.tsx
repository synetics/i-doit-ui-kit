import React from 'react';
import classnames from 'classnames';

type SpinnerProps = {
  /**
   * An extra className to be added to the root element
   */
  className?: string;
};

const Spinner = ({ className }: SpinnerProps) => (
  <div className={classnames('spinner-border', className)} role="status" data-testid="idoit-spinner">
    <span className="sr-only">Loading...</span>
  </div>
);

export { Spinner, SpinnerProps };
