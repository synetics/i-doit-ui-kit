import { ReactNode } from 'react';
import { Spinner } from './Spinner';

export default {
  title: 'Components/Atoms/Spinner',
  component: Spinner,
};

export const Regular = (): ReactNode => <Spinner />;
