import { screen } from '@testing-library/react';
import { createRenderer } from '../../../../../test/utils/renderer';
import { LinkHoverCell } from './LinkHoverCell';

const render = createRenderer(LinkHoverCell, { value: 'text', link: 'www.example.com' });

describe('LinkHoverCell', () => {
  it('renders without crashing', () => {
    render();
    expect(screen.getByText('text')).toBeInTheDocument();
  });

  it('renders empty string', () => {
    render({ value: '', link: '/' });
    expect(screen.getByText('-')).toBeInTheDocument();
  });

  it('renders custom link', () => {
    render({ value: '', link: '/test' });
    const text = screen.getByText('-');
    const link = document.querySelector('a');
    expect(text).toBeInTheDocument();
    expect(link).toHaveAttribute('href', '/test');
  });
});
