import { ReactElement } from 'react';
import classnames from 'classnames';
import { IconButton } from '../../../IconButton';
import { OverflowWithTooltip } from '../../../OverflowWithTooltip';
import { Icon } from '../../../Icon';
import { open_tab } from '../../../../icons';
import { AlignmentProp, CellWrapper } from '../../CellWrapper';
import classes from './LinkHoverCell.module.scss';

type LinkCellProps = {
  value?: string;
  link: string;
  label?: string;
  className?: string;
  buttonClassName?: string;
  tooltipClassName?: string;
} & AlignmentProp;

export const LinkHoverCell = ({
  value,
  link,
  label = 'Open Link',
  align = 'between',
  className,
  buttonClassName,
  tooltipClassName,
}: LinkCellProps): ReactElement => (
  <CellWrapper align={align} className={className}>
    <OverflowWithTooltip tooltipContent={value}>{value || '-'}</OverflowWithTooltip>
    <a href={link} tabIndex={-1} target="_blank" rel="noreferrer">
      <IconButton
        variant="text"
        icon={<Icon src={open_tab} />}
        className={classnames(classes.iconButton, buttonClassName)}
        tooltipClassName={classnames(classes.tooltip, tooltipClassName)}
        label={label}
      />
    </a>
  </CellWrapper>
);
