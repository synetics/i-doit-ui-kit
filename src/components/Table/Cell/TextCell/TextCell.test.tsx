import { screen } from '@testing-library/react';
import { createRenderer } from '../../../../../test/utils/renderer';
import { TextCell } from './TextCell';

const render = createRenderer(TextCell, { value: 'text' });

describe('TextCell', () => {
  it('renders without crashing', () => {
    render();

    screen.getByText('text');
  });

  it('renders empty string', () => {
    render({ value: '' });

    screen.getByText('-');
  });

  it('gets left alignment css class', () => {
    render({ align: 'left', value: 'text' });

    expect(screen.getByTestId('cell-wrapper')).toHaveClass('left');
  });

  it('gets center alignment css class', () => {
    render({ align: 'center', value: 'text' });

    expect(screen.getByTestId('cell-wrapper')).toHaveClass('center');
  });

  it('gets right alignment css class', () => {
    render({ align: 'right', value: 'text' });

    expect(screen.getByTestId('cell-wrapper')).toHaveClass('right');
  });

  it('gets muted css class', () => {
    render({ muted: true, value: 'text' });

    expect(screen.getByTestId('cell-wrapper')).toHaveClass('muted');
  });
});
