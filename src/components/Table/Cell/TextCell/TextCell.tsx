import { ReactElement } from 'react';
import { Text } from '../../../Text';
import { OverflowWithTooltip } from '../../../OverflowWithTooltip';
import { AlignmentProp, CellWrapper } from '../../CellWrapper';

type TextProps = {
  muted?: boolean;
  value?: string;
} & AlignmentProp;

export const TextCell = ({ align = 'left', muted = false, value }: TextProps): ReactElement => (
  <CellWrapper align={align} muted={muted}>
    <OverflowWithTooltip>
      <Text tag="span">{value || '-'}</Text>
    </OverflowWithTooltip>
  </CellWrapper>
);
