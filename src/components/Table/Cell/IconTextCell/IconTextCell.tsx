import { ReactElement } from 'react';
import { Text } from '../../../Text';
import { Icon } from '../../../Icon';
import { OverflowWithTooltip } from '../../../OverflowWithTooltip';
import { AlignmentProp, CellWrapper } from '../../CellWrapper';

type IconTextProps = {
  src: string;
  value?: string;
  fill?: string;
} & AlignmentProp;

export const IconTextCell = ({ align = 'left', src, value, fill = 'neutral-700' }: IconTextProps): ReactElement => (
  <CellWrapper align={align}>
    {value && <Icon className="mr-2" src={src} fill={fill} />}
    <OverflowWithTooltip>
      <Text tag="span">{value || '-'}</Text>
    </OverflowWithTooltip>
  </CellWrapper>
);
