import { screen } from '@testing-library/react';
import { createRenderer } from '../../../../../test/utils/renderer';
import { location } from '../../../../icons';
import { IconTextCell } from './IconTextCell';

const render = createRenderer(IconTextCell, { src: location, value: 'text' });

describe('IconTextCell', () => {
  it('renders without crashing', () => {
    render();

    screen.getByText('text');
  });

  it('renders empty string', () => {
    render({ src: location, value: '' });

    screen.getByText('-');
  });

  it('renders with correct "left" alignment', () => {
    render({ align: 'left', src: location, value: '' });

    expect(screen.getByTestId('cell-wrapper')).toHaveClass('left');
  });

  it('renders with correct "center" alignment', () => {
    render({ align: 'center', src: location, value: '' });

    expect(screen.getByTestId('cell-wrapper')).toHaveClass('center');
  });

  it('renders with correct "right" alignment', () => {
    render({ align: 'right', src: location, value: '' });

    expect(screen.getByTestId('cell-wrapper')).toHaveClass('right');
  });
});
