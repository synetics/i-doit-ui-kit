import { createColumn } from './createColumn';

describe('createColumn', () => {
  it('creates column', () => {
    const column = createColumn();

    expect(column.id).toBe('selection');
    expect(column.disableSortBy).toBe(true);
  });
});
