import React, { FC } from 'react';
import { useSelection } from '../../../../hooks';
import { Checkbox } from '../../../Form/Control';
import classes from './SelectionCell.module.scss';

const SelectionCellHeader: FC = () => {
  const { isAllSelected, setAll } = useSelection();

  return (
    <Checkbox
      aria-label="Select all"
      className={classes.cell}
      checked={isAllSelected()}
      onChange={(e) => {
        setAll(e.target.checked);
      }}
    />
  );
};

export { SelectionCellHeader };
