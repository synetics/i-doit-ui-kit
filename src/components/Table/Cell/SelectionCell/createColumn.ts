import { Column } from 'react-table';
import { SelectionCellHeader } from './SelectionCellHeader';
import { SelectionCell } from './SelectionCell';

export const createColumn = (): Column & { unfocusable: boolean; overflow: boolean } => ({
  id: 'selection',
  disableSortBy: true,
  Header: SelectionCellHeader,
  Cell: SelectionCell,
  className: 'w-auto text-center p-0',
  unfocusable: true,
  overflow: false,
});
