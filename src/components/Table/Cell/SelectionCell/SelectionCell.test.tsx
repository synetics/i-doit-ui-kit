import { screen } from '@testing-library/react';
import user from '@testing-library/user-event';
import { act } from 'react-dom/test-utils';
import { createRenderer } from '../../../../../test/utils/renderer';
import { SelectionCell } from './SelectionCell';

const mock = {
  isSelected: jest.fn(),
  isAllSelected: jest.fn(),
  click: jest.fn(),
  setAll: jest.fn(),
};

jest.mock('../../../../hooks/use-multi-selection', () => ({
  useSelection: () => mock,
}));

const row = {
  id: 'row-id',
};
const render = createRenderer(SelectionCell, { row });

describe('SelectionCell', () => {
  it('renders without crashing', async () => {
    render();

    screen.getByTestId('idoit-checkbox');
  });

  it('sets checks', async () => {
    await act(async () => {
      render();
    });

    const checkbox = screen.getByRole('checkbox');
    expect(checkbox).not.toBeChecked();
    await user.click(checkbox);
    expect(mock.click).toHaveBeenCalledWith('row-id', true, false);
  });

  it('sets checks with shift', async () => {
    await act(async () => {
      render();
    });

    const checkbox = screen.getByRole('checkbox');
    expect(checkbox).not.toBeChecked();
    const session = user.setup();
    await session.keyboard('{shift>}');
    await session.click(checkbox);
    await session.keyboard('{/shift}');
    expect(mock.click).toHaveBeenCalledWith('row-id', true, true);
  });

  it('selects all', async () => {
    await act(async () => {
      render();
    });

    const checkbox = screen.getByRole('checkbox');
    await user.tab();
    await user.type(checkbox, '{Control>}a{/Control}');
    expect(mock.setAll).toHaveBeenCalledWith(true);
  });

  it('deselects all', async () => {
    mock.isAllSelected.mockReturnValueOnce(true);

    await act(async () => {
      render();
    });

    const checkbox = screen.getByRole('checkbox');
    await user.type(checkbox, '{Control>}a{/Control}');
    expect(mock.setAll).toHaveBeenCalledWith(false);

    await user.type(checkbox, '{Shift>}a{/Shift}');
    expect(mock.setAll).toHaveBeenCalledWith(false);
  });

  it('is checked', async () => {
    mock.isSelected.mockReturnValueOnce(true);

    await act(async () => {
      render();
    });

    const checkbox = screen.getByRole('checkbox');

    expect(checkbox).toBeChecked();
  });
});
