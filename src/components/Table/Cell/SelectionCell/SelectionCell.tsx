import React, { useCallback } from 'react';
import { Checkbox } from '../../../Form/Control';
import { handleKey } from '../../../../utils';
import { useKeyStatus, useSelection } from '../../../../hooks';
import classes from './SelectionCell.module.scss';

type SelectionCellType = {
  row: { id: string };
};

const SelectionCell = ({ row: { id } }: SelectionCellType) => {
  const { isSelected, isAllSelected, click, setAll } = useSelection();
  const keys = useKeyStatus();
  const handleSelectRange = useCallback(
    (e: React.MouseEvent) => {
      click(id, !isSelected(id) || e.shiftKey, e.shiftKey);
    },
    [click, id, isSelected],
  );

  return (
    <Checkbox
      onClick={handleSelectRange}
      checked={isSelected(id)}
      className={classes.cell}
      onChange={({ target: { checked } }) => {
        click(id, checked, keys.current.Shift || false);
      }}
      onKeyDown={handleKey({
        a: (e) => {
          if (e.metaKey || e.ctrlKey) {
            setAll(!isAllSelected());
          }
        },
      })}
      aria-label="Select"
    />
  );
};

export { SelectionCell };
