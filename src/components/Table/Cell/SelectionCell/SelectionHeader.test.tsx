import { screen } from '@testing-library/react';
import user from '@testing-library/user-event';
import { act } from 'react-dom/test-utils';
import { createRenderer } from '../../../../../test/utils/renderer';
import { SelectionCellHeader } from './SelectionCellHeader';

const mock = {
  isAllSelected: jest.fn(),
  setAll: jest.fn(),
};

jest.mock('../../../../hooks/use-multi-selection', () => ({
  useSelection: () => mock,
}));

const render = createRenderer(SelectionCellHeader, {});

describe('SelectionCellHeader', () => {
  it('renders without crashing', async () => {
    render();

    const checkbox = screen.getByRole('checkbox');
    expect(checkbox).not.toBeChecked();
  });

  it('is checked', async () => {
    mock.isAllSelected.mockReturnValueOnce(true);

    await act(async () => {
      render();
    });

    const checkbox = screen.getByRole('checkbox');
    expect(checkbox).toBeChecked();
  });

  it('sets checks', async () => {
    await act(async () => {
      render();
    });

    const checkbox = screen.getByRole('checkbox');
    expect(checkbox).not.toBeChecked();
    await user.click(checkbox);
    expect(mock.setAll).toHaveBeenCalled();
  });
});
