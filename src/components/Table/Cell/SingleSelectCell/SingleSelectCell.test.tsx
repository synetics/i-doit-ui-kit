import { screen } from '@testing-library/react';
import user from '@testing-library/user-event';
import { act } from 'react-dom/test-utils';
import { createRenderer } from '../../../../../test/utils/renderer';
import { SingleSelectCell } from './SingleSelectCell';

const mock = {
  isSelected: jest.fn(),
  set: jest.fn(),
};

jest.mock('../../../../hooks/use-single-selection', () => ({
  useSingleSelectionContext: () => mock,
}));

const row = {
  id: 'row-id',
};

const render = createRenderer(SingleSelectCell, { row });

describe('SingleSelectionCell', () => {
  it('renders without crashing', async () => {
    render();

    screen.getByTestId('idoit-radio');
  });

  it('sets checks', async () => {
    await act(async () => {
      render();
    });

    const radio = screen.getByRole('radio');
    expect(radio).not.toBeChecked();
    await user.click(radio);
    expect(mock.set).toHaveBeenCalledWith('row-id');
  });

  it('is checked', async () => {
    mock.isSelected.mockReturnValueOnce(true);

    await act(async () => {
      render();
    });

    const checkbox = screen.getByRole('radio');

    expect(checkbox).toBeChecked();
  });
});
