import React, { useCallback } from 'react';
import { Radio } from '../../../Form/Control';
import { useSingleSelectionContext } from '../../../../hooks';

type SelectionCellType = {
  row: { id: string };
};

const SingleSelectCell = ({ row: { id } }: SelectionCellType) => {
  const { isSelected, set } = useSingleSelectionContext();
  const handleSelect = useCallback(() => {
    set(id);
  }, [set, id]);

  return (
    <Radio
      onClick={handleSelect}
      checked={isSelected(id)}
      onChange={() => set(id)}
      aria-label="Select Radio"
      className="m-0 h-100"
    />
  );
};

export { SingleSelectCell };
