import { Column } from 'react-table';
import { SingleSelectCell } from './SingleSelectCell';

export const createColumn = (): Column & { unfocusable: boolean } => ({
  id: 'single-selection',
  disableSortBy: true,
  Header: '',
  Cell: SingleSelectCell,
  className: 'w-auto text-center py-0',
  unfocusable: true,
});
