import { ReactElement } from 'react';
import { Text } from '../../../Text';
import { OverflowWithTooltip } from '../../../OverflowWithTooltip';
import { CellWrapper } from '../../CellWrapper';

type CurrencyProps = {
  currency: string;
  value?: string;
};

export const CurrencyCell = ({ currency, value }: CurrencyProps): ReactElement => (
  <CellWrapper>
    {value && (
      <Text className="mr-2" tag="span">
        {currency}
      </Text>
    )}
    <OverflowWithTooltip>
      <Text tag="span">{value || '-'}</Text>
    </OverflowWithTooltip>
  </CellWrapper>
);
