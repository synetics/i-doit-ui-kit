import { screen } from '@testing-library/react';
import { createRenderer } from '../../../../../test/utils/renderer';
import { CurrencyCell } from './CurrencyCell';

const render = createRenderer(CurrencyCell, { currency: 'EUR', value: '1.000' });

describe('CurrencyCell', () => {
  it('renders without crashing', () => {
    render();

    screen.getByText('1.000');
  });

  it('renders empty string', () => {
    render({ currency: 'EUR', value: '' });

    screen.getByText('-');
  });
});
