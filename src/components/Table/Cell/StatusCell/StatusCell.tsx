import React, { ReactElement } from 'react';
import { Checkbox } from '../../../Form/Control/Checkbox';
import { AlignmentProp, CellWrapper } from '../../CellWrapper/CellWrapper';

type StatusCellType = {
  status: boolean;
  onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
} & AlignmentProp;

export const StatusCell = ({ align = 'left', status, onChange }: StatusCellType): ReactElement => (
  <CellWrapper align={align}>
    <Checkbox aria-label="checkbox" checked={status} onChange={onChange} />
  </CellWrapper>
);
