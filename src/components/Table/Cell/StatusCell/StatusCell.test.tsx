import { screen } from '@testing-library/react';
import { createRenderer } from '../../../../../test/utils/renderer';
import { StatusCell } from './StatusCell';

const render = createRenderer(StatusCell, { status: false });

describe('StatusCell', () => {
  it('renders without crashing with false', () => {
    render();

    expect(screen.getByRole('checkbox')).not.toBeChecked();
  });

  it('renders with specific range', () => {
    render({ status: true, align: 'center' });

    expect(screen.getByRole('checkbox')).toBeChecked();
  });
});
