import { ReactElement } from 'react';
import { Text } from '../../../Text';
import { OverflowWithTooltip } from '../../../OverflowWithTooltip';
import { AlignmentProp, CellWrapper } from '../../CellWrapper';

type SuffixProps = {
  suffix?: string | number;
  value?: string | number;
} & AlignmentProp;

export const SuffixTextCell = ({ align = 'right', suffix, value }: SuffixProps): ReactElement => (
  <CellWrapper align={align}>
    <OverflowWithTooltip>
      <Text className="mr-2 text-nowrap" tag="span">
        {value || '-'}
      </Text>
      {suffix && (
        <Text className="text-nowrap" tag="span">
          {suffix}
        </Text>
      )}
    </OverflowWithTooltip>
  </CellWrapper>
);
