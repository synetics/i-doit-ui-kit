import { screen } from '@testing-library/react';
import { createRenderer } from '../../../../../test/utils/renderer';
import { SuffixTextCell } from './SuffixTextCell';

const render = createRenderer(SuffixTextCell, { suffix: 'i-doit.com', value: 'text' });

describe('SuffixTextCell', () => {
  it('renders without crashing', () => {
    render();

    screen.getByText('text');
  });

  it('renders empty string', () => {
    render({ suffix: 'i-doit.com', value: '' });

    screen.getByText('-');
  });

  it('renders left css class', () => {
    render({ align: 'left' });

    expect(screen.getByTestId('cell-wrapper')).toHaveClass('left');
  });

  it('renders center css class', () => {
    render({ align: 'center' });

    expect(screen.getByTestId('cell-wrapper')).toHaveClass('center');
  });

  it('renders right css class', () => {
    render();

    expect(screen.getByTestId('cell-wrapper')).toHaveClass('right');
  });

  it('renders between css class', () => {
    render({ align: 'between' });

    expect(screen.getByTestId('cell-wrapper')).toHaveClass('between');
  });
});
