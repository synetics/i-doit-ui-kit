import { screen } from '@testing-library/react';
import { createRenderer } from '../../../../../test/utils/renderer';
import { ListTextCell } from './ListTextCell';

const render = createRenderer(ListTextCell, {});

describe('ListTextCell', () => {
  it('renders empty list', () => {
    render();

    screen.getByText('-');
  });

  it('renders list without tooltip with string arrays', () => {
    render({ value: ['item01', 'item02'] });

    screen.getByText('item01, item02');
  });

  it('renders list with tooltip with string arrays', () => {
    render({ value: ['item01', 'item02'], withTooltip: true });

    expect(screen.getByTestId('idoit-tooltip')).toHaveClass('tooltip');
    screen.getByText('item01, item02');
  });

  it('renders list without tooltip with string', () => {
    render({ value: 'item01, item02' });

    screen.getByText('item01, item02');
  });

  it('renders list with tooltip with string', () => {
    render({ value: 'item01, item02', withTooltip: true });

    expect(screen.getByTestId('idoit-tooltip')).toHaveClass('tooltip');
    screen.getByText('item01, item02');
  });

  it('renders list without tooltip with string array with separator ";" ', () => {
    render({ value: ['item01', 'item02'], separator: ';' });

    screen.getByText('item01; item02');
  });

  it('renders list with tooltip with string array with separator ";" ', () => {
    render({ value: ['item01', 'item02'], separator: ';', withTooltip: true });

    expect(screen.getByTestId('idoit-tooltip')).toHaveClass('tooltip');
    screen.getByText('item01; item02');
  });

  it('renders list without tooltip with string with separator ";" ', () => {
    render({ value: 'item01, item02', separator: ';' });

    screen.getByText('item01; item02');
  });

  it('renders list with tooltip with string with separator ";" ', () => {
    render({ value: 'item01, item02', separator: ';', withTooltip: true });

    expect(screen.getByTestId('idoit-tooltip')).toHaveClass('tooltip');
    screen.getByText('item01; item02');
  });
});
