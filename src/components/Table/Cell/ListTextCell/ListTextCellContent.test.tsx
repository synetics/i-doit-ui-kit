import { screen } from '@testing-library/react';
import { createRenderer } from '../../../../../test/utils/renderer';
import { ListTextCellContent } from './ListTextCellContent';

const render = createRenderer(ListTextCellContent, { value: 'content' });

describe('ListTextCellContent', () => {
  it('renders without crashing', () => {
    render();

    expect(screen.getByTestId('idoit-tooltip')).toHaveClass('tooltip');
    screen.getByText('content');
  });

  it('renders empty string', () => {
    render({ value: '' });

    screen.getByText('-');
  });
});
