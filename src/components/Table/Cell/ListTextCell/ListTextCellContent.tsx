import { ReactElement } from 'react';
import { TooltipCell } from '../TooltipCell';
import styles from './ListTextCell.module.scss';

type ListTextCellContentProps = {
  value?: string;
  withTooltip?: boolean;
};

export const ListTextCellContent = ({ value, withTooltip = false }: ListTextCellContentProps): ReactElement => (
  <TooltipCell containerClassName={styles.listCellContainer} content={value || ''} disableHoverAction={!withTooltip}>
    {value || '-'}
  </TooltipCell>
);
