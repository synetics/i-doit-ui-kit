import { ReactElement } from 'react';
import { Text } from '../../../Text';
import { isString } from '../../../../utils';
import { CellWrapper } from '../../CellWrapper';
import { ListTextCellContent } from './ListTextCellContent';

type ListTextProps = {
  value?: string | string[];
  separator?: string;
  withTooltip?: boolean;
};

export const ListTextCell = ({ value, separator = ',', withTooltip = false }: ListTextProps): ReactElement => (
  <CellWrapper>
    {(value && (
      <ListTextCellContent
        value={
          (Array.isArray(value) && value.join(`${separator} `)) ||
          ((isString(value) && value.replace(',', separator)) as string)
        }
        withTooltip={withTooltip}
      />
    )) || (
      <div>
        <Text tag="span" />-
      </div>
    )}
  </CellWrapper>
);
