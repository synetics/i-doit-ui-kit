import { screen } from '@testing-library/react';
import { createRenderer } from '../../../../../test/utils/renderer';
import { TooltipCell } from './TooltipCell';

const render = createRenderer(TooltipCell, { children: 'text' });

describe('TooltipCell', () => {
  it('renders without crashing', () => {
    render();

    screen.getByText('text');
  });

  it('renders empty string', () => {
    render({ children: '' });

    screen.getByText('-');
  });

  it('gets passed css class', () => {
    render({ className: 'css-class', value: 'text' });

    expect(screen.getByText('text')).toHaveClass('css-class');
  });
});
