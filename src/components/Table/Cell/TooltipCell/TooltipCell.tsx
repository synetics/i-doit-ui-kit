import classnames from 'classnames';
import { PropsWithChildren, ReactElement } from 'react';
import { Tooltip } from '../../../Tooltip';
import styles from './TooltipCell.module.scss';

type TooltipCellProps = PropsWithChildren<{
  value?: string;
  containerClassName?: string;
  content?: ReactElement | string;
  className?: string;
  disableHoverAction?: boolean;
}>;

export const TooltipCell = ({
  content = '',
  children,
  value = '',
  className,
  containerClassName,
  disableHoverAction = false,
}: TooltipCellProps): ReactElement => (
  <Tooltip
    placement="bottom-start"
    className={styles.tooltip}
    disableHoverAction={!children || disableHoverAction}
    tooltipClassName={containerClassName}
    content={content || value}
  >
    <div className={classnames(styles.tooltipCell, className)}>{children || '-'}</div>
  </Tooltip>
);
