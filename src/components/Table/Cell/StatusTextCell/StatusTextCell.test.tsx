import { screen } from '@testing-library/react';
import { createRenderer } from '../../../../../test/utils/renderer';
import { StatusTextCell } from './StatusTextCell';

const render = createRenderer(StatusTextCell, { value: 'text' });

describe('StatusTextCell', () => {
  it('renders without crashing', () => {
    render();

    screen.getByText('text');
  });

  it('renders empty string', () => {
    render({ value: '' });

    screen.getByText('-');
  });

  it('gets alignment css class', () => {
    render({ color: 'red', value: 'text' });

    expect(screen.getByTestId('color')).toHaveStyle({ backgroundColor: 'red' });
  });
});
