import { ReactElement } from 'react';
import { Text } from '../../../Text';
import { OverflowWithTooltip } from '../../../OverflowWithTooltip';
import { CellWrapper } from '../../CellWrapper';
import styles from './StatusTextCell.module.scss';

type StatusTextProps = {
  color?: string;
  value?: string;
  className?: string;
};

export const StatusTextCell = ({ color = '#aaa', value, className }: StatusTextProps): ReactElement => (
  <CellWrapper className={className}>
    {value && <div data-testid="color" className={styles['status-pin']} style={{ backgroundColor: color }} />}
    <OverflowWithTooltip>
      <Text tag="span">{value || '-'}</Text>
    </OverflowWithTooltip>
  </CellWrapper>
);
