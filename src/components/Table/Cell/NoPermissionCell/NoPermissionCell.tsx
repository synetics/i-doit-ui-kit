import React, { ReactElement } from 'react';
import classnames from 'classnames';
import { denied } from '../../../../icons';
import { Tooltip } from '../../../Tooltip';
import { AlignmentProp, CellWrapper } from '../../CellWrapper';
import { Icon } from '../../../Icon';
import { Text } from '../../../Text';
import styles from './NoPermissionCell.module.scss';

type NoPermissionProps = {
  tooltipValue?: string;
  value?: string;
} & AlignmentProp;

export const NoPermissionCell = ({
  align = 'left',
  tooltipValue = "You don't have permission to read",
  value = 'No permission',
}: NoPermissionProps): ReactElement => (
  <CellWrapper className="overflow-hidden" align={align} nowrap muted>
    <Tooltip
      className={styles.abbreviate}
      triggerClassName={classnames('align-items-center overflow-hidden')}
      content={tooltipValue}
      placement="top"
    >
      <Icon className="mr-2" src={denied} fill="neutral-700" />
      <Text className={styles.abbreviate} tag="span">
        {value}
      </Text>
    </Tooltip>
  </CellWrapper>
);
