import { screen } from '@testing-library/react';
import { act } from 'react-dom/test-utils';
import { createRenderer } from '../../../../../test/utils/renderer';
import { NoPermissionCell } from './NoPermissionCell';

const render = createRenderer(NoPermissionCell, {});

describe('NoPermissionCell', () => {
  it('renders without crashing', async () => {
    await act(async () => {
      render();
    });

    screen.getByText('No permission');
  });

  it('renders visibly', async () => {
    await act(async () => {
      render({ tooltipValue: 'tooltip-value', value: 'value' });
    });

    expect(screen.getByText('value')).toBeVisible();
  });

  it('renders with correct "left" alignment', async () => {
    await act(async () => {
      render({ align: 'left' });
    });

    expect(screen.getByTestId('cell-wrapper')).toHaveClass('left');
  });

  it('renders with correct "center" alignment', async () => {
    await act(async () => {
      render({ align: 'center' });
    });

    expect(screen.getByTestId('cell-wrapper')).toHaveClass('center');
  });

  it('renders with correct "right" alignment', async () => {
    await act(async () => {
      render({ align: 'right' });
    });

    expect(screen.getByTestId('cell-wrapper')).toHaveClass('right');
  });
});
