import { ReactElement } from 'react';
import { Text } from '../../../Text';
import { OverflowWithTooltip } from '../../../OverflowWithTooltip';
import { CellWrapper } from '../../CellWrapper';

type DateProps = {
  value?: string;
};

export const DateCell = ({ value }: DateProps): ReactElement => (
  <CellWrapper align="right">
    <OverflowWithTooltip>
      <Text tag="span">{value || '-'}</Text>
    </OverflowWithTooltip>
  </CellWrapper>
);
