import { screen } from '@testing-library/react';
import { createRenderer } from '../../../../../test/utils/renderer';
import { DateCell } from './DateCell';

const render = createRenderer(DateCell, {});

describe('DateCell', () => {
  it('renders without crashing', () => {
    render({ value: '31-12-1982' });

    screen.getByText('31-12-1982');
  });

  it('renders empty string without any parameters', () => {
    render();

    screen.getByText('-');
  });
});
