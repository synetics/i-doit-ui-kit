import { ReactElement, useState } from 'react';
import classnames from 'classnames';
import { Tooltip } from '../../../Tooltip';
import { IconButton } from '../../../IconButton';
import { Icon } from '../../../Icon';
import { open_tab } from '../../../../icons';
import { Text } from '../../../Text';
import { CellWrapper } from '../../CellWrapper';
import styles from './LinkCell.module.scss';

type LinkCellProps = {
  value?: string;
  link: string;
  label?: string;
};

export const LinkCell = ({ value, link, label = 'Open Link' }: LinkCellProps): ReactElement => {
  const [hovered, setHovered] = useState(false);

  const handleShow = () => {
    setTimeout(() => setHovered(true), 100);
  };

  const handleHide = () => {
    setTimeout(() => setHovered(false), 100);
  };

  return (
    <CellWrapper align="between" className={classnames(styles.maxSize, { [styles.cellWrapperStyle]: hovered })}>
      <div onMouseLeave={handleHide} className={styles.maxSize}>
        <Tooltip
          placement="right"
          className={styles.maxSize}
          triggerClassName={styles.tooltipCell}
          popperClassName={styles.negativeOffset}
          disableHoverAction={!value}
          tooltipClassName={styles.tooltipContainer}
          content={
            <IconButton
              variant="text"
              href={link}
              label={label}
              icon={<Icon src={open_tab} />}
              onMouseEnter={handleShow}
              onMouseLeave={handleHide}
            />
          }
        >
          <div onMouseEnter={handleShow} onFocus={handleShow}>
            <Text tag="span" className={styles.tooltipCell}>
              {value || '-'}
            </Text>
          </div>
        </Tooltip>
      </div>
    </CellWrapper>
  );
};
