import { act, fireEvent, screen } from '@testing-library/react';
import user from '@testing-library/user-event';
import { createRenderer } from '../../../../../test/utils/renderer';
import { LinkCell } from './LinkCell';

const render = createRenderer(LinkCell, { value: 'text', link: 'www.example.com' });

describe('LinkCell', () => {
  it('renders without crashing', () => {
    render();

    screen.getByText('text');
  });

  it('renders custom link title', async () => {
    render({ value: 'text', link: '/', label: 'Custom text' });

    const trigger = screen.getByText('text');
    expect(screen.queryByRole('tooltip')).not.toBeInTheDocument();

    await user.hover(trigger);
    expect(screen.getByTestId('idoit-popper')).toBeInTheDocument();

    await user.unhover(trigger);
    expect(screen.queryByTestId('idoit-popper')).not.toBeInTheDocument();
  });

  it('renders empty string', () => {
    render({ value: '', link: '/' });

    screen.getByText('-');
  });

  it('should show tooltip and open link on hover', async () => {
    jest.useFakeTimers();
    try {
      render({ value: 'text', link: '/', label: 'Custom text' });
      const trigger = screen.getByText('text');
      expect(screen.queryByRole('tooltip')).not.toBeInTheDocument();

      fireEvent.mouseEnter(trigger);
      act(() => {
        jest.advanceTimersByTime(100);
      });
      expect(screen.queryByRole('tooltip')).toBeInTheDocument();
      expect(screen.getByText('Custom text')).toBeInTheDocument();
      fireEvent.mouseLeave(trigger);
      act(() => {
        jest.advanceTimersByTime(100);
      });
      expect(screen.queryByRole('tooltip')).not.toBeInTheDocument();
      expect(screen.queryByText('Custom text')).not.toBeInTheDocument();
    } finally {
      jest.useRealTimers();
    }
  });
});
