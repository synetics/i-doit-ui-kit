import { screen } from '@testing-library/react';
import { createRenderer } from '../../../../../test/utils/renderer';
import { ColorIndicatorCell } from './ColorIndicatorCell';

const render = createRenderer(ColorIndicatorCell, { value: 'MyValue' });

describe('CurrencyCell', () => {
  it('renders without crashing', () => {
    render();

    screen.getByText('MyValue');
  });

  it('renders empty string and gray color', () => {
    render({ value: '' });

    const elem = document.getElementsByClassName('colorSection')[0];

    expect(elem).toHaveStyle({ 'background-color': '#CBCBCB' });
  });

  it('renders needed color', () => {
    render({ value: '', color: '#343434' });

    expect(document.getElementsByClassName('colorSection')[0]).toHaveStyle({ 'background-color': '#343434' });
  });
});
