import { ReactElement } from 'react';
import classnames from 'classnames';
import { Text } from '../../../Text';
import { OverflowWithTooltip } from '../../../OverflowWithTooltip';
import { CellWrapper } from '../../CellWrapper';
import styles from './ColorIndicatorCell.module.scss';

type ColorIndicatorCellProps = {
  color?: string;
  value?: string;
  className?: string;
};

export const ColorIndicatorCell = ({ color = '#CBCBCB', value, className }: ColorIndicatorCellProps): ReactElement => (
  <CellWrapper className={classnames(styles.colorCell, className)}>
    <div className={styles.colorSection} style={{ backgroundColor: color }} />
    <OverflowWithTooltip>
      <Text tag="span">{value || '-'}</Text>
    </OverflowWithTooltip>
  </CellWrapper>
);
