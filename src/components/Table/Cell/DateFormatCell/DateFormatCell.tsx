import { ReactElement } from 'react';
import { formatDate } from '../../../../utils';
import { DateCell } from '../DateCell';

type DateFormatProps = {
  value?: Date;
  format?: string;
};

export const DateFormatCell = ({ value, format = 'MM/dd/yyyy' }: DateFormatProps): ReactElement => (
  <DateCell value={value && formatDate(value, format)} />
);
