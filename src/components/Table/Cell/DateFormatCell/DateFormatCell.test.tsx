import { screen } from '@testing-library/react';
import { createRenderer } from '../../../../../test/utils/renderer';
import { DateFormatCell } from './DateFormatCell';

const render = createRenderer(DateFormatCell, {});

describe('DateFormatCell', () => {
  it('renders without crashing', () => {
    render({ format: 'dd-MM-yyyy', value: new Date(1982, 11, 31) });

    screen.getByText('31-12-1982');
  });

  it('renders default format', () => {
    render({ value: new Date(1982, 11, 31) });

    screen.getByText('12/31/1982');
  });

  it('renders time', () => {
    render({ format: 'hh:mm aaa', value: new Date(1982, 11, 31, 13, 13, 0) });

    screen.getByText('01:13 pm');
  });

  it('renders empty string without any parameters', () => {
    render();

    screen.getByText('-');
  });
});
