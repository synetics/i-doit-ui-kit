import { screen } from '@testing-library/react';
import { createRenderer } from '../../../../../test/utils/renderer';
import { RangeCell } from './RangeCell';

const render = createRenderer(RangeCell, {});

describe('SuffixTextCell', () => {
  it('renders without crashing', () => {
    render({ value: 99 });

    screen.getByText('99');
    screen.getByText('/100');
  });

  it('renders with specific range', () => {
    render({ value: 99, range: 1000 });

    screen.getByText('99');
    screen.getByText('/1000');
  });

  it('renders empty string when value is not set', () => {
    render();

    screen.getByText('-');
  });

  it('renders empty string when value is not set but range', () => {
    render({ range: 10000 });

    screen.getByText('-');
  });
});
