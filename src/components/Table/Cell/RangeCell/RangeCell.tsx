import { ReactElement } from 'react';
import { SuffixTextCell } from '../SuffixTextCell';

type RangeCellType = {
  value?: number;
  range?: number;
};

export const RangeCell = ({ value, range = 100 }: RangeCellType): ReactElement => (
  <SuffixTextCell suffix={range && value && `/${range}`} value={value} />
);
