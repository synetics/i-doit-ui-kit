import { fireEvent, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { createRenderer } from '../../../../../test/utils/renderer';
import { ToggleFavoriteCell } from './ToggleFavoriteCell';
import style from './ToggleFavoriteCell.module.scss';

const render = createRenderer(ToggleFavoriteCell, { status: false });

describe('ToggleFavoriteCell', () => {
  it('renders without crashing with false', async () => {
    render();

    const button = screen.getByRole('button');

    expect(button).toHaveClass(style.fillUnchosen);

    await userEvent.click(button);

    expect(button).toHaveClass(style.fillChosen);
  });

  it('renders with neede labels', async () => {
    render({ status: true, addLabel: 'Add', removeLabel: 'Remove' });

    const button = screen.getByRole('button');

    fireEvent.mouseEnter(button);
    expect(screen.getByTestId('idoit-popper')).toBeInTheDocument();

    await userEvent.click(button);

    fireEvent.mouseEnter(button);
    expect(screen.getByTestId('idoit-popper')).toBeInTheDocument();
  });
});
