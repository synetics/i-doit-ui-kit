import React, { HTMLProps, ReactElement } from 'react';
import classnames from 'classnames';
import { IconButton } from '../../../IconButton';
import { Icon } from '../../../Icon';
import { favorite } from '../../../../icons';
import { AlignmentProp, CellWrapper } from '../../CellWrapper/CellWrapper';
import { useControlledState } from '../../../../hooks';
import styles from './ToggleFavoriteCell.module.scss';

type ToggleCellType = {
  status: boolean;
  addLabel?: string;
  removeLabel?: string;
  onChange?: () => void;
} & AlignmentProp &
  Omit<HTMLProps<HTMLButtonElement>, 'onChange'>;

export const ToggleFavoriteCell = ({
  align = 'center',
  status,
  addLabel = 'Add to favorites',
  removeLabel = 'Remove from favorites',
  onChange,
}: ToggleCellType): ReactElement => {
  const [value, setValue] = useControlledState<boolean>(status, onChange);
  const handleClick = (e: React.MouseEvent) => {
    setValue(!value);
    e.stopPropagation();
  };
  return (
    <CellWrapper align={align}>
      <IconButton
        variant="text"
        onClick={handleClick}
        label={value ? removeLabel : addLabel}
        className={classnames({ [styles.fillChosen]: value, [styles.fillUnchosen]: !value })}
        icon={<Icon src={favorite} />}
      />
    </CellWrapper>
  );
};
