import { screen } from '@testing-library/react';
import { createRenderer } from '../../../../../test/utils/renderer';
import { NotAvailableCell } from './NotAvailableCell';

const render = createRenderer(NotAvailableCell, { value: 'text' });

describe('NotAvailableCell', () => {
  it('renders without crashing', () => {
    render();

    screen.getByText('Not available');
  });

  it('renders with correct css classes', () => {
    render();

    expect(screen.getByTestId('cell-wrapper')).toHaveClass('cell-wrapper', 'left', 'muted');
  });
});
