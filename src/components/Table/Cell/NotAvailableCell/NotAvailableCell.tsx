import React, { ReactElement } from 'react';
import { TextCell } from '../TextCell';

export const NotAvailableCell = (): ReactElement => <TextCell align="left" muted value="Not available" />;
