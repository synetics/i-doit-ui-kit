import { ReactElement, SyntheticEvent, useRef, useState } from 'react';
import { useCallbackRef, useFocusOut } from '../../../../hooks';
import { Icon } from '../../../Icon';
import { Popper } from '../../../Popper';
import { more_vertical } from '../../../../icons';
import { CellWrapper } from '../../CellWrapper';
import { Text } from '../../../Text';
import { IconButton } from '../../../IconButton';
import { Menu } from '../../../Menu';
import { Button } from '../../../Button';

type ActionProps = {
  mainIcon?: { name: string; icon: string };
  icons?: { name: string; icon: string }[];
};

export const ActionCell = ({ mainIcon, icons }: ActionProps): ReactElement => {
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const [referenceElement, setReferenceElement] = useCallbackRef<HTMLButtonElement>();
  const wrapperRef = useRef(null);
  const toggleOpen = (e: SyntheticEvent) => {
    e.stopPropagation();
    setIsOpen(!isOpen);
  };

  useFocusOut(wrapperRef, () => setIsOpen(false));

  if (mainIcon && icons) {
    return (
      <CellWrapper>
        <IconButton label={mainIcon.name} variant="text" icon={<Icon src={mainIcon.icon} />} />
        {icons.length > 2 ? (
          <div ref={wrapperRef}>
            <IconButton
              ref={setReferenceElement}
              label="More"
              variant="text"
              activeState={isOpen}
              onClick={toggleOpen}
              icon={<Icon src={more_vertical} />}
            />
            {isOpen && (
              <Popper dataTestId="action-cell-popper" open={isOpen} referenceElement={referenceElement}>
                <Menu>
                  {icons.map((icon) => (
                    <Button
                      key={icon.icon + icon.name}
                      className="justify-content-start"
                      variant="text"
                      icon={<Icon wrapper="span" src={icon.icon} />}
                    >
                      {icon.name}
                    </Button>
                  ))}
                </Menu>
              </Popper>
            )}
          </div>
        ) : (
          <>
            {icons.map((icon) => (
              <IconButton
                key={icon.icon + icon.name}
                ref={setReferenceElement}
                label={icon.name}
                variant="text"
                icon={<Icon src={icon.icon} />}
              />
            ))}
          </>
        )}
      </CellWrapper>
    );
  }

  return (
    <CellWrapper>
      {mainIcon ? (
        <IconButton label={mainIcon.name} variant="text" icon={<Icon src={mainIcon.icon} />} />
      ) : (
        <Text>-</Text>
      )}
    </CellWrapper>
  );
};
