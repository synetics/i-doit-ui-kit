import { render, screen } from '@testing-library/react';
import React from 'react';
import { act } from 'react-dom/test-utils';
import userEvent from '@testing-library/user-event';
import { createRenderer } from '../../../../../test/utils/renderer';
import { arrow_up, edit, more_vertical } from '../../../../icons';
import { ActionCell } from './ActionCell';

const renderActionCell = createRenderer(ActionCell, {});

const propsJustWithMainIcon = {
  mainIcon: { icon: arrow_up, name: 'arrow_up' },
};

const propsWithThreeIcons = {
  mainIcon: { icon: arrow_up, name: 'arrow_up' },
  icons: [
    { name: 'One', icon: edit },
    { name: 'Two', icon: edit },
  ],
};
const propsWithTwoIcons = {
  mainIcon: { icon: arrow_up, name: 'arrow_up' },
  icons: [{ name: 'One', icon: edit }],
};
const propsWithMoreThanThreeIcons = {
  mainIcon: { icon: arrow_up, name: 'arrow_up' },
  icons: [
    { name: 'One', icon: edit },
    { name: 'Two', icon: edit },
    { name: 'Three', icon: edit },
    { name: 'Four', icon: edit },
  ],
};

describe('<ActionCell/>', () => {
  it('renders without crashing', () => {
    renderActionCell({ ...propsJustWithMainIcon });

    const button = screen.getByRole('button');
    expect(button).toBeInTheDocument();
    const iconButton = screen.getByTestId('icon');
    expect(iconButton).toHaveAttribute('data-src', arrow_up);
  });

  it('renders with 2 buttons', () => {
    renderActionCell({ ...propsWithTwoIcons });

    const buttons = screen.getAllByRole('button');
    expect(buttons).toHaveLength(2);
    buttons.map((el) => expect(el).toBeInTheDocument());
  });

  it('renders with 3 buttons', () => {
    renderActionCell({ ...propsWithThreeIcons });

    const buttons = screen.getAllByRole('button');
    expect(buttons).toHaveLength(propsWithThreeIcons.icons.length + 1);
    buttons.map((el) => expect(el).toBeInTheDocument());
  });
  it('renders with more than 3 buttons', async () => {
    await act(async () => {
      renderActionCell({ ...propsWithMoreThanThreeIcons });
    });

    const buttons = screen.getAllByRole('button');
    expect(buttons).toHaveLength(2);
    buttons.map((el) => expect(el).toBeInTheDocument());
  });

  it('renders with  more than 3 buttons and open/close menu', async () => {
    await act(async () => {
      renderActionCell({ ...propsWithMoreThanThreeIcons });
    });
    const [, openButton] = screen.getAllByRole('button');
    await act(async () => userEvent.click(openButton));
    const buttonsNew = screen.getAllByRole('button');
    expect(buttonsNew).toHaveLength(propsWithMoreThanThreeIcons.icons.length + 3);
    const popper = screen.getByTestId('action-cell-popper');
    expect(popper).toBeVisible();
    propsWithMoreThanThreeIcons.icons.forEach((el): void => {
      const text = screen.getByText(el.name);
      expect(text).toBeVisible();
    });
    await act(async () => userEvent.click(openButton));
    expect(popper).not.toBeVisible();
    propsWithMoreThanThreeIcons.icons.forEach((el): void => {
      expect(screen.queryByText(el.name)).not.toBeInTheDocument();
    });
  });

  it('renders with more than 3 buttons, open menu and close by click outside', async () => {
    await act(async () => {
      renderActionCell({ ...propsWithMoreThanThreeIcons });
      render(React.createElement('div', { id: 'outside', role: 'outside' }));
    });
    const [, openButton] = screen.getAllByRole('button');

    await act(async () => userEvent.click(openButton));
    expect(screen.getByTestId('action-cell-popper')).toBeVisible();
    await act(async () => userEvent.click(openButton));
    expect(screen.queryByTestId('action-cell-popper')).not.toBeInTheDocument();
  });

  it('render as icon button', async () => {
    await act(async () => {
      renderActionCell({ ...propsWithMoreThanThreeIcons });
    });

    const iconButtons = screen.getAllByTestId('icon');
    expect(iconButtons[0]).toHaveAttribute('data-src', propsWithMoreThanThreeIcons.mainIcon.icon);
    expect(iconButtons[1]).toHaveAttribute('data-src', more_vertical);
    iconButtons.forEach((icon, i) => {
      if (i > 1) {
        expect(icon).toHaveAttribute('data-src', propsWithMoreThanThreeIcons.icons[i - 2].icon);
      }
    });
  });

  it('renders empty string without any parameters', () => {
    renderActionCell();

    screen.getByText('-');
  });
});
