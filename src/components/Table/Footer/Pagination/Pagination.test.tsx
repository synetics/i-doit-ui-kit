import { render, screen } from '@testing-library/react';
import React from 'react';
import user from '@testing-library/user-event';
import { Pagination } from './Pagination';

describe('Pagination', () => {
  const setPage = jest.fn();
  it('renders without crashing', () => {
    render(<Pagination page={0} setPage={setPage} totalPages={10} />);

    expect(screen.getByTestId('Pagination')).toBeInTheDocument();
  });

  it('to have a className', () => {
    render(<Pagination className="example" page={1} setPage={setPage} totalPages={10} />);

    expect(screen.getByTestId('Pagination')).toHaveClass('example');
  });

  it('Previous button is Disabled and Next is Enabled when The First Page is active', () => {
    render(<Pagination page={1} setPage={setPage} totalPages={10} />);

    expect(screen.getByTestId('Previous-btn')).toBeDisabled();
    expect(screen.getByTestId('Next-btn')).toBeEnabled();
  });

  it('Input have a Page Number and have the total Pages als suffix', () => {
    render(<Pagination page={1} setPage={setPage} totalPages={10} />);

    expect(screen.getByRole('textbox')).toHaveValue('1');
    expect(screen.getByTestId('FieldGroup')).toHaveTextContent('/ 10');
  });

  it('Previous button is Enabled and Next is Disabled when The Last Page is active', () => {
    render(<Pagination page={10} setPage={setPage} totalPages={10} />);

    expect(screen.getByTestId('Previous-btn')).toBeEnabled();
    expect(screen.getByTestId('Next-btn')).toBeDisabled();
  });

  it('Previous button is clicked', async () => {
    const onPageChange = jest.fn();
    render(<Pagination page={5} setPage={onPageChange} totalPages={10} />);

    await user.click(screen.getByTestId('Previous-btn'));

    expect(onPageChange).toHaveBeenCalledWith(4);
  });

  it('typing in the Input', async () => {
    const onPageChange = jest.fn();
    render(<Pagination page={5} setPage={onPageChange} totalPages={10} />);

    const trigger = screen.getByRole('textbox');

    await user.type(trigger, '{backspace}4{enter}');

    expect(onPageChange).toHaveBeenCalledWith(4);
  });

  it('click on Blur after typing in the Input', async () => {
    const onPageChange = jest.fn();
    render(<Pagination page={5} setPage={onPageChange} totalPages={10} />);

    const trigger = screen.getByRole('textbox');

    await user.type(trigger, '{backspace}4');
    await user.click(document.body);

    expect(onPageChange).toHaveBeenCalledWith(4);
  });

  it('Next button is clicked', async () => {
    const onPageChange = jest.fn();
    render(<Pagination page={5} setPage={onPageChange} totalPages={10} />);

    await user.click(screen.getByTestId('Next-btn'));

    expect(onPageChange).toHaveBeenCalledWith(6);
  });

  const page = 5;
  it.each([
    ['4asd', 4],
    ['', page],
    ['44', page],
    ['5', 5],
  ])('check the Input Value', async (userInput: string, expectedValue: number) => {
    const onPageChange = jest.fn();
    render(<Pagination page={page} setPage={onPageChange} totalPages={10} />);

    const trigger = screen.getByRole('textbox');

    await user.type(trigger, `{backspace}${userInput}{enter}`);

    expect(onPageChange).toHaveBeenCalledWith(expectedValue);
  });
});
