import React, { useState } from 'react';
import classNames from 'classnames';
import { Icon } from '../../../Icon';
import { IconButton } from '../../../IconButton';
import { arrow_left, arrow_right } from '../../../../icons';
import { FieldExtra, FieldGroup } from '../../../Form/Infrastructure';
import { Input } from '../../../Form/Control';
import style from './Pagination.module.scss';

type PaginationProps = {
  className?: string;
  page: number;
  setPage: (page: number) => void;
  totalPages: number;
};

const Pagination = ({ className, page, setPage, totalPages }: PaginationProps) => {
  const minPage = 1;
  const maxPage = totalPages;
  const [userValue, setUserValue] = useState<string | number>(page);

  const handleChange = (newPage: number) => {
    setPage(newPage);
    setUserValue(newPage);
  };

  const onEnter = () => {
    const value = typeof userValue === 'string' ? parseInt(userValue, 10) : userValue;
    const newPage = Math.max(minPage, Math.min(value, maxPage));
    if (Number.isNaN(newPage)) {
      handleChange(page);
    } else if (value > maxPage) {
      handleChange(page);
    } else {
      handleChange(newPage);
    }
  };

  const onKeyDownHandler = (e: React.KeyboardEvent<HTMLInputElement>) => {
    if (e.key === 'Enter') {
      e.preventDefault();
      onEnter();
      e.stopPropagation();
    }
  };

  const [paginationInputId] = useState<string>(`idoit-pagination-input-${Date.now()}`);

  return (
    <div data-testid="Pagination" className={classNames(className, style.pagination)}>
      <IconButton
        className={style.previousButton}
        data-testid="Previous-btn"
        disabled={page <= 1}
        variant="text"
        type="button"
        label="Previous"
        icon={<Icon src={arrow_left} />}
        onClick={() => handleChange(page - 1)}
      />
      <FieldGroup dataTestId="FieldGroup" className={style.paginationInput}>
        <Input
          className={style.input}
          id={paginationInputId}
          type="text"
          value={userValue}
          min={minPage}
          max={maxPage}
          onKeyDown={onKeyDownHandler}
          onChange={setUserValue}
          onFocus={() => setUserValue(page)}
          onBlur={onEnter}
        />
        <FieldExtra> / {totalPages}</FieldExtra>
      </FieldGroup>
      <IconButton
        className={style.nextButton}
        data-testid="Next-btn"
        disabled={page === totalPages}
        variant="text"
        type="button"
        label="Next"
        icon={<Icon src={arrow_right} />}
        onClick={() => handleChange(page + 1)}
      />
    </div>
  );
};

export { Pagination, PaginationProps };
