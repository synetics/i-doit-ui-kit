import { render, screen } from '@testing-library/react';
import React from 'react';
import { FooterInfo } from './FooterInfo';

describe('FooterInfo', () => {
  it('renders without crashing', () => {
    render(<FooterInfo text="Example" />);

    screen.getByText('Example');
  });
  it('to have a className', () => {
    render(<FooterInfo className="Example" text="Example" />);

    expect(screen.getByText('Example')).toHaveClass('Example');
  });
});
