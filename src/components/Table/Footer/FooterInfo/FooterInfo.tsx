import React from 'react';
import classNames from 'classnames';
import { Text } from '../../../Text';
import style from './FooterInfo.module.scss';

type FooterInfoProps = {
  className?: string;
  text: string;
};

const FooterInfo = ({ className, text }: FooterInfoProps) => (
  <Text className={classNames(className, style.footerInfo)} tag="p" fontSize="small">
    {text}
  </Text>
);
export { FooterInfo, FooterInfoProps };
