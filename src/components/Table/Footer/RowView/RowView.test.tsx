import { render, screen } from '@testing-library/react';
import React from 'react';
import user from '@testing-library/user-event';
import { RowView } from './RowView';

describe('RowView', () => {
  const setPerPage = jest.fn();

  it('renders without crashing', () => {
    render(<RowView perPage={10} setPerPage={setPerPage} />);

    expect(screen.getByTestId('RowView')).toBeInTheDocument();
  });

  it('to have a className', () => {
    render(<RowView className="example" perPage={10} setPerPage={setPerPage} />);

    expect(screen.getByTestId('RowView')).toHaveClass('example');
  });

  it('to Have Text Content', () => {
    render(<RowView className="example" perPage={10} setPerPage={setPerPage} />);

    expect(screen.getByTestId('RowView')).toHaveTextContent('10');
  });

  it('RowView button is clicked', async () => {
    render(<RowView className="example" perPage={10} setPerPage={setPerPage} />);

    await user.click(screen.getByTestId('RowView'));

    expect(screen.getByTestId('idoit-popper')).toBeInTheDocument();
    expect(screen.getByTestId('item-10')).toBeInTheDocument();
    expect(screen.getByTestId('item-25')).toBeInTheDocument();
    expect(screen.getByTestId('item-50')).toBeInTheDocument();
    expect(screen.getByTestId('item-100')).toBeInTheDocument();
  });

  it('RowView button custom options', async () => {
    render(<RowView className="example" perPage={10} setPerPage={setPerPage} options={[2, 5]} />);

    await user.click(screen.getByTestId('RowView'));

    expect(screen.getByTestId('idoit-popper')).toBeInTheDocument();
    expect(screen.getByText('2')).toBeInTheDocument();
    expect(screen.getByText('5')).toBeInTheDocument();
  });

  it('item-1 is clicked', async () => {
    render(<RowView className="example" perPage={10} setPerPage={setPerPage} />);

    await user.click(screen.getByTestId('RowView'));
    await user.click(screen.getByTestId('item-10'));

    expect(screen.getByTestId('RowView')).toHaveTextContent('10');
  });

  it('item-2 is clicked', async () => {
    render(<RowView className="example" perPage={25} setPerPage={setPerPage} />);

    await user.click(screen.getByTestId('RowView'));
    await user.click(screen.getByTestId('item-25'));

    expect(screen.getByTestId('RowView')).toHaveTextContent('25');
  });

  it('item-3 is clicked', async () => {
    render(<RowView className="example" perPage={50} setPerPage={setPerPage} />);

    await user.click(screen.getByTestId('RowView'));
    await user.click(screen.getByTestId('item-50'));

    expect(screen.getByTestId('RowView')).toHaveTextContent('50');
  });

  it('item-4 is clicked', async () => {
    render(<RowView className="example" perPage={100} setPerPage={setPerPage} />);

    await user.click(screen.getByTestId('RowView'));
    await user.click(screen.getByTestId('item-100'));

    expect(screen.getByTestId('RowView')).toHaveTextContent('100');
  });
});
