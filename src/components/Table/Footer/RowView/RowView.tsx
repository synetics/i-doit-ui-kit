import React from 'react';
import classNames from 'classnames';
import { ButtonPulldown } from '../../../ButtonPulldown';
import { Item } from '../../../Menu/components/items';
import style from './RowView.module.scss';

type RowViewProps = {
  /**
   * Class of the container
   */
  className?: string;
  /**
   * Rows in the Page
   */
  perPage: number;
  /**
   * Callback to set the Numbers of Rows
   */
  setPerPage: (perPage: number) => void;
  /**
   * Array of numbers for options for the dropdown
   */
  options?: number[];
};

const RowView = ({ className, perPage, setPerPage, options = [10, 25, 50, 100] }: RowViewProps) => (
  <ButtonPulldown
    data-testid="RowView"
    className={classNames(className, style.rowView)}
    label={perPage.toString()}
    variant="text"
    type="button"
  >
    {(onClose) => {
      const onPerPageSelect = (perPageSelect: number) => {
        onClose();
        setPerPage(perPageSelect);
      };
      return (
        <>
          {options.map((item) => (
            <Item
              key={`key-${item}`}
              singleSelect
              selected={perPage === item}
              className={style.item}
              id={`item-${item}`}
              label={item.toString()}
              onSelect={() => onPerPageSelect(item)}
            />
          ))}
        </>
      );
    }}
  </ButtonPulldown>
);
export { RowView, RowViewProps };
