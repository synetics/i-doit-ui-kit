import { Column } from 'react-table';

export type Data = Record<string, unknown>;

export type ColumnType<T extends Data> = Column<T> & {
  className?: string;
  isResizing?: boolean;
  resizable?: boolean;
  selectable?: boolean;
  overflow?: boolean;
  headerClassName?: string;
};
