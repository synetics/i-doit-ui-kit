import { act, fireEvent, screen } from '@testing-library/react';
import React from 'react';
import user from '@testing-library/user-event';
import { createRenderer } from '../../../test/utils/renderer';
import { arrow_up } from '../../icons';
import { Table, TableProps } from './Table';
import { FooterInfo } from './Footer/FooterInfo';
import { ActionCell, SelectionCell, TextCell } from './Cell';
import { cellTypeColumns, expandableColumns } from './stories/dataCellTypeExample';
import { expandedData, simpleColumns, simpleData } from './stories/dataExample';
import { ColumnType } from './types';

type ArrayType<T> = T extends (infer A)[] ? A : never;

type ExampleData = {
  name: string;
};

type ExpandedData = ArrayType<typeof expandedData>;

const defaultColumns: ColumnType<ExampleData>[] = [{ Header: 'Name', accessor: 'name', Cell: TextCell }];
const multipleColumns: ColumnType<ExampleData>[] = [
  {
    id: 'one',
    minWidth: 100,
    resizable: true,
    selectable: true,
    Header: 'Name1',
    accessor: 'name',
    Cell: TextCell,
  },
  {
    id: 'second',
    minWidth: 100,
    resizable: true,
    selectable: true,
    Header: 'Name2',
    accessor: 'name',
    Cell: TextCell,
  },
];
const render = createRenderer((props: TableProps<ExampleData>) => <Table {...props} />, {
  columns: defaultColumns,
  data: [],
});
const renderExpanded = createRenderer((props: TableProps<ExpandedData>) => <Table {...props} />, {
  columns: expandableColumns,
  data: [],
});

const propsJustWithMainIcon = {
  mainIcon: { icon: arrow_up, name: 'arrow_up' },
};

const exampleData: ExampleData[] = [{ name: 'Anna' }, { name: 'Karl' }, { name: 'Lisa' }];

describe('Table', () => {
  it('renders without crashing', () => {
    render();

    screen.getByTestId('table');
  });

  it('has no "hover" css class by default', () => {
    render();

    expect(screen.getByTestId('table')).not.toHaveClass('show-hover');
  });

  it('gets "hover" css class', () => {
    render({ columns: [{ Header: 'Name', accessor: 'name' }], data: [], showHover: true });

    expect(screen.getByTestId('table')).toHaveClass('show-hover');
  });

  it('Render some data', () => {
    render({ columns: [{ Header: 'Name', accessor: 'name' }], data: [{ name: 'Anna' }, { name: 'Karl' }] });

    expect(screen.getByText('Anna')).toBeInTheDocument();
    expect(screen.getByText('Karl')).toBeInTheDocument();
  });

  it('footer gets rendered with string', () => {
    render({ columns: [{ Header: 'Name', accessor: 'name' }], data: [], footer: 'Hello world', showHover: true });

    expect(screen.getByTestId('tfoot')).toHaveTextContent('Hello world');
  });

  it('footer gets rendered with Element', () => {
    render({
      columns: [{ Header: 'Name', accessor: 'name' }],
      data: [],
      footer: <FooterInfo text="Hello world" />,
      showHover: true,
    });

    expect(screen.getByTestId('tfoot')).toHaveTextContent('Hello world');
  });

  it('to make use of tabindex', () => {
    render({
      columns: [{ Header: 'Name', accessor: (data: ExampleData) => data.name, Cell: TextCell }],
      data: exampleData,
      showHover: true,
    });

    const firstGridCell = screen.getAllByRole('gridcell');
    const gridHeader = screen.getAllByRole('rowheader');

    expect(gridHeader[0]).toHaveAttribute('tabindex', '0');
    expect(gridHeader[0]).toHaveAttribute('data-cellid', '0-0');
    expect(firstGridCell[0]).toHaveAttribute('tabindex', '-1');
    expect(firstGridCell[0]).toHaveAttribute('data-cellid', '1-0');

    expect(firstGridCell[1]).toHaveAttribute('tabindex', '-1');
    expect(firstGridCell[1]).toHaveAttribute('data-cellid', '2-0');
  });

  it('to check focus', async () => {
    render({
      columns: [{ Header: 'Name', accessor: 'name', Cell: TextCell }],
      data: exampleData,
      showHover: true,
    });

    const firstGridCell = screen.getAllByRole('gridcell');
    const gridHeader = screen.getAllByRole('rowheader');

    await user.type(gridHeader[0], '{enter}');
    await act(() => fireEvent.focus(firstGridCell[0]));
    expect(firstGridCell[0]).toHaveFocus();
  });

  it('to check resize and search', async () => {
    const fn = jest.fn();
    render({
      columns: simpleColumns,
      data: simpleData,
      showHover: true,
      onSort: fn,
    });

    const gridHeader = screen.getAllByRole('rowheader');
    act(() => gridHeader[0].focus());
    await act(() => fireEvent.keyDown(gridHeader[0], { key: '+' }));
    expect(gridHeader[0]?.style?.width).toEqual('200px');
    const resizer = screen.getByTestId('resizer-container');
    expect(resizer).toBeInTheDocument();
    await user.dblClick(resizer);
    await user.click(resizer);
    expect(fn).not.toHaveBeenCalled();
    expect(screen.getAllByRole('rowheader')[0]?.style?.width).toEqual('250px');
    act(() => gridHeader[0].focus());
    await user.type(document.activeElement || document.body, '+');
    expect(gridHeader[0]?.style?.width).toEqual('300px');
    await user.type(gridHeader[0], '-');
    expect(gridHeader[0]?.style?.width).toEqual('250px');
    act(() => gridHeader[0].focus());
    await user.type(gridHeader[0], '{enter}');
    expect(fn).toHaveBeenCalled();
    await user.type(gridHeader[0], ' ');
    expect(fn).toHaveBeenCalled();
  });

  it('to check select cell and call onSelectedCell', async () => {
    const fn = jest.fn();
    render({
      columns: multipleColumns,
      data: exampleData,
      showHover: true,
      onSelectedCell: fn,
    });
    const gridCells = screen.getAllByRole('gridcell');
    await user.click(gridCells[0]);
    expect(fn).toHaveBeenCalled();
  });

  it('to check select cell with keyboard', async () => {
    const fn = jest.fn();
    const fn2 = jest.fn();
    render({
      columns: multipleColumns,
      data: exampleData,
      showHover: true,
      onSelectedCell: fn,
      onSelectAll: fn2,
    });
    const gridCells = screen.getAllByRole('gridcell');
    await user.type(gridCells[0], ' ');
    expect(fn).toHaveBeenCalled();
    await user.type(gridCells[0], '{enter}');
    expect(fn).toHaveBeenCalled();
    await user.type(gridCells[0], '{shift>}{Control>}{home}{/Control}{/shift}');
    expect(fn).toHaveBeenCalled();
    await user.type(gridCells[0], '{Control>}a{/Control}');
    expect(fn2).toHaveBeenCalled();
    await user.type(gridCells[0], '{shift>}{Control>}{end}{/Control}{/shift}');
    expect(fn).toHaveBeenCalled();
    await user.type(gridCells[0], 'o');
    expect(fn).toHaveBeenCalled();
    await user.type(gridCells[0], '{meta>}{arrowup}{/meta}');
    expect(fn).toHaveBeenCalled();
    await user.type(gridCells[0], '{meta>}{arrowdown}{/meta}');
    expect(fn).toHaveBeenCalled();
    await user.type(gridCells[0], '{escape}');
    expect(fn).toHaveBeenCalled();
  });

  it('to check render selected cell', () => {
    render({
      columns: multipleColumns,
      data: exampleData,
      showHover: true,
      selectedItems: { 0: ['0'], 1: ['0'] },
    });
    expect(screen.getAllByRole('gridcell')[0]).toHaveClass('selectedCell startSelection selectedNextRight');
  });

  it('to check focus on icon', async () => {
    render({
      columns: cellTypeColumns,
      data: exampleData,
      showHover: true,
    });

    const gridHeader = screen.getAllByRole('rowheader');
    act(() => gridHeader[0].focus());
    await act(() => user.tab());
    const buttons = screen.getAllByRole('button');
    expect(buttons[0]).toHaveFocus();
  });

  it('render children excepts body if they are', () => {
    render({
      columns: [{ Header: 'Name', accessor: 'name', Cell: TextCell }],
      data: [],
      children: 'No data',
    });

    expect(screen.getByText('No data')).toBeInTheDocument();
    expect(screen.queryByRole('gridcell')).not.toBeInTheDocument();
  });

  it('render cells with needed borders if grouped', () => {
    render({
      columns: [
        {
          Header: 'Name',
          Cell: TextCell,
          columns: [
            { id: 'FirstName', Header: 'FirstName', accessor: 'name' },
            {
              id: 'SecondName',
              Header: 'SecondName',
              accessor: 'name',
            },
          ],
        },
      ],
      data: [{ name: 'Anna' }, { name: 'Karl' }, { name: 'Lisa' }],
    });

    expect(screen.getByText('Name')?.parentElement?.parentElement?.parentElement).toHaveClass('borders');
    expect(screen.getByText('FirstName')?.parentElement?.parentElement?.parentElement).toHaveClass('borderLeft');
    expect(screen.getByText('SecondName')?.parentElement?.parentElement?.parentElement).toHaveClass('borderRight');
  });

  it('dont recieves focus if unfocusable', async () => {
    render({
      columns: [
        {
          Header: 'Name',
          Cell: <ActionCell {...propsJustWithMainIcon} />,
          columns: [
            { id: 'FirstName', Header: 'FirstName', accessor: 'name' },
            {
              id: 'SecondName',
              Header: 'SecondName',
              accessor: 'name',
            },
          ],
        },
      ],
      data: [{ name: 'Anna' }, { name: 'Karl' }, { name: 'Lisa' }],
    });

    const firstCell = screen.getAllByRole('gridcell')[0];

    await user.click(firstCell);
    await user.tab();

    expect(firstCell).not.toHaveFocus();
  });

  it('navigate with tab', async () => {
    render({
      columns: cellTypeColumns,
      data: exampleData,
    });
    await user.tab();
    await user.tab();
    expect(screen.getAllByRole('button')[0]).toHaveFocus();
  });

  it('renders with expanded data', async () => {
    renderExpanded({
      columns: expandableColumns,
      data: expandedData,
    });

    await user.click(screen.getAllByRole('button')[0]);
  });
  it('renders first header without overflow', async () => {
    render({
      columns: [
        {
          Header: 'selected',
          Cell: SelectionCell,
          minWidth: 100,
          resizable: true,
          selectable: true,
          overflow: false,
        },
        ...multipleColumns,
      ],
      data: exampleData,
    });
    const header = screen.getAllByRole('rowheader');
    expect(header[0]).not.toHaveClass('tooltip');
  });

  it('renders fixed table header', () => {
    render({
      columns: multipleColumns,
      data: exampleData,
      fixed: true,
    });
    const header = screen.getByTestId('tableHead');
    expect(header).toHaveClass('fixed');
  });
});
