import { ReactNode, useContext } from 'react';
import { Row as RowType } from 'react-table';
import classnames from 'classnames';
import { SelectionContext } from '../../../hooks/use-multi-selection';
import { Data } from '../types';
import classes from './Row.module.scss';

type RowComponentType<T extends Data> = {
  className?: string;
  row: RowType<T> & { depth?: number; canExpand?: boolean };
  children?: ReactNode;
};

const Row = <T extends Data>({ className, row, children, ...rest }: RowComponentType<T>) => {
  const selectionApi = useContext(SelectionContext);
  return (
    <tr
      {...rest}
      className={classnames(classes.row, className, {
        [classes.selected]: selectionApi.isSelected(row.id),
      })}
    >
      {children}
    </tr>
  );
};

export { Row, RowType };
