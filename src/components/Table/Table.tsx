import React, { BaseSyntheticEvent, ForwardedRef, forwardRef, ReactElement, useEffect } from 'react';
import {
  ColumnInstance,
  HeaderGroup,
  Row as RowType,
  SortingRule,
  useBlockLayout,
  useExpanded,
  useResizeColumns,
  useSortBy,
  useTable,
} from 'react-table';
import classnames from 'classnames';
import { pointer_down, pointer_up } from '../../icons';
import { useEventListener, useGlobalizedRef, useKeyboardMatrix } from '../../hooks';
import { checkIsFocusWithin } from '../../utils/check-is-focus-whithin';
import { handleKey, ObjectEventsType } from '../../utils';
import { Icon } from '../Icon';
import { OverflowWithTooltip } from '../OverflowWithTooltip';
import { Row } from './Row';
import styles from './Table.module.scss';
import { adjustBordersStyle, findParentXY } from './utils';
import { getLeafHeaders } from './utils/getLeafHeaders';
import { ColumnType, Data } from './types';

type ExpandableRow<T extends Data> = RowType<T> & { depth: number; canExpand?: boolean };

export type CellPropsType<T extends Data> = ColumnInstance<T> & {
  unfocusable?: boolean;
  classname?: string;
  selectable?: boolean;
  overflow?: boolean;
  hasBottomBorder?: boolean;
};

type TableProps<T extends Data> = {
  className?: string;
  /**
   * Required array of columns.
   */
  columns: ColumnType<T>[];

  /**
   * Array of data objects which needs to contain the column accessors as keys and actual (mixed type) values as values.
   */
  data: T[];

  /**
   * Property to display optional footer string / element.
   */
  footer?: string | ReactElement;

  /**
   * Property to display fixed header
   */
  fixed?: boolean;

  /**
   * Switch to en- and disable the visual hover states.
   */
  showHover?: boolean;

  /**
   * Component in case table is empty or has some errors with data.
   */
  children?: string | ReactElement;

  /**
   * Sorting of the table
   */
  sort?: SortingRule<T>;

  /**
   * Set sorting callback
   */
  onSort?: (sort: SortingRule<T>) => void;

  /**
   * Get row identifier
   */
  getRowId?: (originalRow: T, relativeIndex: number, parent?: RowType<T>) => string;
  /**
   * resize step by double click on resizer
   */
  resizeStep?: number;
  /**
   * function for select/deselect cell
   */
  onSelectedCell?: (e: BaseSyntheticEvent, rowId: string, cellIndex: number) => void;
  /**
   * Object key is selected column and value Array of selected cells in this column
   */
  selectedItems?: { [key: number]: string[] };
  /**
   * function for reset selection
   */
  onResetSelection?: () => void;
  /**
   * function for select all cells in column
   */
  onSelectAll?: (columnInd: number, e: BaseSyntheticEvent) => void;
};

const Table = forwardRef(
  <T extends Data>(
    {
      className,
      columns,
      data = [],
      getRowId,
      footer,
      fixed,
      showHover = false,
      children,
      sort,
      onSort,
      resizeStep = 50,
      onSelectedCell,
      selectedItems,
      onResetSelection,
      onSelectAll,
    }: TableProps<T>,
    ref: ForwardedRef<HTMLTableElement>,
  ) => {
    const { localRef: referenceElement, setRefsCallback: setReferenceElement } =
      useGlobalizedRef<HTMLTableElement>(ref);

    const { getTableProps, getTableBodyProps, headerGroups, rows, prepareRow, dispatch } = useTable(
      {
        columns,
        data,
        manualSortBy: typeof onSort === 'function',
        disableSortRemove: true,
        initialState: { sortBy: sort ? [sort] : [] },
        getRowId,
        stateReducer: (newState, action) => {
          if (onSort && action.type === 'toggleSortBy') {
            onSort(newState.sortBy[0]);
          }

          return newState;
        },
      },
      useSortBy,
      useResizeColumns,
      useBlockLayout,
      useExpanded,
    );
    const sizeChange = (event: BaseSyntheticEvent, column: HeaderGroup<T>, increase = true) => {
      const headersToResize = getLeafHeaders(column);
      const headerIdWidths = headersToResize.map((d) => [d.id, d.totalWidth]);
      const node = event.target as HTMLElement;
      const width: number = parseFloat(node.style.width);
      const { x } = node.getBoundingClientRect();
      dispatch({
        type: 'columnStartResizing',
        columnId: column.id,
        columnWidth: column.width || width,
        clientX: Math.floor(x),
        headerIdWidths,
      });

      if (!increase && column.minWidth && width - resizeStep > column.minWidth) {
        dispatch({ type: 'columnResizing', clientX: x - resizeStep });
      }
      if (increase) {
        dispatch({ type: 'columnResizing', clientX: x + resizeStep });
      }
      dispatch({ type: 'columnDoneResizing' });
    };

    const preparedRows = rows.map((row) => {
      prepareRow(row);
      return row;
    });

    const matrix: { size: number; unfocusable: boolean }[][] = [
      ...headerGroups.map((el) =>
        el.headers.map((e) => ({ size: e.totalVisibleHeaderCount, unfocusable: e.unfocusable || false })),
      ),
      ...preparedRows.map((row) =>
        row.cells.map((e) => ({ size: 1, unfocusable: e.unfocusable || false, selectable: e.selectable || false })),
      ),
    ];
    const { isSelected, x, y, setCoord } = useKeyboardMatrix(referenceElement.current || document.body, matrix);

    const setFocus = (newX: number, newY: number) => setCoord({ x: newX, y: newY });

    useEventListener(
      'keyup',
      handleKey(
        {
          Tab: (e: BaseSyntheticEvent) => {
            const newCoord = findParentXY(e.target, 'TD');
            if (newCoord) {
              setCoord(newCoord);
            }
          },
        },
        false,
      ),
      referenceElement.current || document.body,
    );

    useEffect(() => {
      const cell = referenceElement.current?.querySelector<HTMLTableDataCellElement>(`[data-cellid="${y}-${x}"]`);

      if (checkIsFocusWithin(document.activeElement, cell)) {
        return;
      }
      if (!checkIsFocusWithin(document.activeElement, referenceElement.current)) {
        return;
      }
      if (cell) {
        const tabbableChildren = cell.querySelectorAll<HTMLElement>(
          'button, [href], input, select, textarea, [tabindex]:not([tabindex="-1"])',
        );
        const validChildren: HTMLElement[] = [];
        tabbableChildren.forEach((node) => {
          if (node.getBoundingClientRect().width > 0 && node.tabIndex > -1) {
            validChildren.push(node);
          }
        });
        if (validChildren.length > 0) {
          validChildren[0].focus();
        } else {
          cell.focus();
        }
      }
    }, [referenceElement, x, y]);
    return (
      <>
        <table
          {...getTableProps()}
          className={classnames(styles.table, className, { [styles['show-hover']]: showHover })}
          data-testid="table"
          ref={setReferenceElement}
          role="grid"
        >
          <thead data-testid="tableHead" className={classnames({ [styles.fixed]: fixed })}>
            {headerGroups.map((headerGroup, rowIndex) => (
              <tr {...headerGroup.getHeaderGroupProps()}>
                {headerGroup.headers.map((column, cellIndex) => {
                  const {
                    canSort,
                    isSorted,
                    isSortedDesc,
                    toggleSortBy,
                    isResizing,
                    resizable,
                    overflow = true,
                  } = column;
                  const columnHandlers: ObjectEventsType<HTMLTableHeaderCellElement> = {};
                  if (toggleSortBy) {
                    columnHandlers.Enter = () => toggleSortBy();
                    columnHandlers[' '] = () => toggleSortBy();
                  }
                  if (resizable) {
                    columnHandlers['+'] = (event) => sizeChange(event, column);
                    columnHandlers['-'] = (event) => sizeChange(event, column, false);
                  }

                  return (
                    <th
                      {...column.getHeaderProps([
                        {
                          ...column.getSortByToggleProps(),
                          className: classnames(
                            column.headerClassName || column.className,
                            adjustBordersStyle(column.parent, column.Header),
                            {
                              [styles.canSort]: canSort,
                              [styles.unfocusable]: !!(column as CellPropsType<T>).unfocusable,
                              [styles.borders]: column.headers?.length > 1,
                              [styles.noBottomBorder]: !column.accessor && column.columns?.length,
                              [styles.isSorted]: column.isSorted,
                              [styles.resizable]: column.resizable,
                              [styles.resizing]: column.isResizing,
                            },
                          ),
                        },
                      ])}
                      onFocus={() => !(column as CellPropsType<T>).unfocusable && setFocus(cellIndex, rowIndex)}
                      data-cellid={`${rowIndex}-${cellIndex}`}
                      tabIndex={isSelected(cellIndex, rowIndex) && !(column as CellPropsType<T>).unfocusable ? 0 : -1}
                      onKeyDown={handleKey(columnHandlers)}
                      role="rowheader"
                    >
                      {column.canSort && (
                        <Icon
                          className={classnames(styles.sorting, { [styles.active]: isSorted })}
                          src={isSorted && isSortedDesc ? pointer_down : pointer_up}
                          width={12}
                          wrapper="span"
                          fill={isSorted ? 'neutral-900' : 'neutral-700'}
                        />
                      )}
                      {overflow ? (
                        <OverflowWithTooltip>{column.render('Header')}</OverflowWithTooltip>
                      ) : (
                        column.render('Header')
                      )}
                      {resizable && (
                        <div
                          role="button"
                          tabIndex={-1}
                          onKeyPress={() => {}}
                          onClick={(e) => e.stopPropagation()}
                          onDoubleClick={(e: BaseSyntheticEvent) => sizeChange(e, column)}
                          data-testid="resizer-container"
                          {...column.getResizerProps()}
                          className={styles.resizerContainer}
                        >
                          <div className={classnames(styles.resizer, { [styles.isResizing]: isResizing })} />
                        </div>
                      )}
                    </th>
                  );
                })}
                <th aria-label="fill-header" className={classnames(styles.placeholder, styles.unfocusable)} />
              </tr>
            ))}
          </thead>
          <tbody {...getTableBodyProps()}>
            {children ? (
              <tr>
                <td className={styles.fullWidthChild}>{children}</td>
              </tr>
            ) : (
              preparedRows.map((row, rowIndex) => (
                <Row row={row} key={row.id}>
                  {row.cells.map((cell, cellIndex) => {
                    const cellsClassName = adjustBordersStyle<T>(cell?.column?.parent, cell?.column?.Header);
                    const cellWithProps: CellPropsType<T> = cell.column;
                    const previousRowId = preparedRows[rowIndex - 1]?.id;
                    const firstId = preparedRows[0]?.id;
                    const lastId = preparedRows[preparedRows.length - 1]?.id;
                    const nextRowId = preparedRows[rowIndex + 1]?.id;
                    const selectedCell = selectedItems && selectedItems[cellIndex]?.includes(row.id);
                    const startSelection = selectedCell && !selectedItems[cellIndex]?.includes(previousRowId);
                    const endSelection = selectedCell && !selectedItems[cellIndex]?.includes(nextRowId);
                    const endRight = selectedCell && selectedItems[cellIndex + 1]?.includes(row.id);
                    const cellHandlers: ObjectEventsType<HTMLTableHeaderCellElement> = {
                      ' ': (e) => onSelectedCell && onSelectedCell(e, row.id, cellIndex),
                      Enter: (e) => onSelectedCell && onSelectedCell(e, row.id, cellIndex),
                      Home: (e) => {
                        if (onSelectedCell && e.ctrlKey && e.shiftKey) {
                          onSelectedCell(e, firstId, cellIndex);
                          setFocus(cellIndex, headerGroups.length);
                        }
                      },
                      End: (e) => {
                        if (onSelectedCell && e.ctrlKey && e.shiftKey) {
                          onSelectedCell(e, lastId, cellIndex);
                          setFocus(cellIndex, preparedRows.length + (headerGroups.length - 1));
                        }
                      },
                      ArrowDown: (e) => onSelectedCell && e.metaKey && onSelectedCell(e, nextRowId, cellIndex),
                      ArrowUp: (e) => onSelectedCell && e.metaKey && onSelectedCell(e, previousRowId, cellIndex),
                      Escape: () => onResetSelection && onResetSelection(),
                      a: (e) => {
                        if (e.ctrlKey && onSelectAll) {
                          onSelectAll(cellIndex, e);
                        }
                      },
                    };
                    return (
                      <td
                        {...cell.getCellProps()}
                        data-cellid={`${rowIndex + headerGroups.length}-${cellIndex}`}
                        className={classnames(cell?.column?.className, cellsClassName, {
                          [styles.selectedCell]: selectedCell,
                          [styles.startSelection]: startSelection,
                          [styles.endSelection]: endSelection,
                          [styles.selectedNextRight]: endRight,
                          [styles.noBottomBorder]:
                            !cellWithProps.hasBottomBorder &&
                            (cell.row as ExpandableRow<T>).depth > 0 &&
                            cell.row.index === 0 &&
                            (cell.value === 'novalue' ||
                              (cell.column.id === 'expander' && !(row as ExpandableRow<T>).canExpand)),
                        })}
                        role="gridcell"
                        tabIndex={
                          rowIndex + headerGroups.length === y && cellIndex === x && !cellWithProps.unfocusable ? 0 : -1
                        }
                        onFocus={() => {
                          if (!cellWithProps.unfocusable) {
                            setFocus(cellIndex, rowIndex + headerGroups.length);
                          }
                        }}
                        onClick={
                          cellWithProps.selectable && onSelectedCell
                            ? (e) => onSelectedCell(e, row.id, cellIndex)
                            : () => {}
                        }
                        onKeyDown={cellWithProps.selectable && onSelectedCell ? handleKey(cellHandlers) : () => {}}
                      >
                        {cell.render('Cell')}
                      </td>
                    );
                  })}
                  <td className={styles.placeholder} />
                </Row>
              ))
            )}
          </tbody>
        </table>
        {footer && (
          <div data-testid="tfoot" className={styles['table-footer']}>
            {footer}
          </div>
        )}
      </>
    );
  },
) as <T extends Data>(props: TableProps<T> & { ref?: ForwardedRef<HTMLTableElement> }) => ReactElement | null;

export { Table, TableProps };
