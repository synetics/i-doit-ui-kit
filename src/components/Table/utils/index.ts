import { ColumnInstance, HeaderProps, Renderer } from 'react-table';
import styles from '../Table.module.scss';
import { Data } from '../types';

const adjustBordersStyle = <T extends Data>(
  parent: ColumnInstance<T> | undefined,
  columnHeader: Renderer<HeaderProps<T>> | undefined,
): string => {
  const parentColumns = parent?.columns || [];
  const parentHeader = parent?.Header;
  const isGrouped = parentColumns && parentHeader;
  if (isGrouped) {
    if (parentColumns[0].Header === columnHeader) {
      return styles.borderLeft;
    }
    if (parentColumns[parentColumns.length - 1].Header === columnHeader) {
      return styles.borderRight;
    }
  }

  return '';
};

const findParentXY = (element: HTMLElement, tag: string): false | { x: number; y: number } => {
  const coord = element
    .closest(tag)
    ?.getAttribute('data-cellid')
    ?.split('-')
    .map((e) => Number(e));
  if (coord && typeof coord[0] === 'number' && typeof coord[1] === 'number') {
    return { y: coord[0], x: coord[1] };
  }
  return false;
};

export { adjustBordersStyle, findParentXY };
