import { ColumnInstance } from 'react-table';
import { getLeafHeaders } from './getLeafHeaders';

describe('getLeafHeaders', () => {
  it('get all headers', () => {
    const parentHeader = {
      Header: 'Parent',
      columns: [
        { Header: 'Child 1', columns: [] },
        { Header: 'Child 2', columns: [] },
      ],
    };

    const leafHeaders: ColumnInstance[] = getLeafHeaders(parentHeader as unknown as ColumnInstance);
    expect(leafHeaders).toHaveLength(3);
    expect(leafHeaders[0]).toBe(parentHeader.columns[0]);
    expect(leafHeaders[1]).toBe(parentHeader.columns[1]);
    expect(leafHeaders[2]).toBe(parentHeader);
  });
});
