import { ColumnInstance } from 'react-table';
import { Data } from '../types';

export const getLeafHeaders = <T extends Data>(header: ColumnInstance<T>) => {
  const leafHeaders: ColumnInstance<T>[] = [];
  const recurseHeader = (headerForRecurs: ColumnInstance<T>) => {
    if (headerForRecurs.columns && headerForRecurs.columns.length) {
      headerForRecurs.columns.map(recurseHeader);
    }
    leafHeaders.push(headerForRecurs);
  };
  recurseHeader(header);
  return leafHeaders;
};
