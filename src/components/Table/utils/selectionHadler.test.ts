import { singleColumnSelectionHandler } from './selectionHandler';

describe('selectionHandler', () => {
  it('selects single cell on click', () => {
    const result = singleColumnSelectionHandler([0], 0, {} as KeyboardEvent, undefined, []);
    expect(result).toEqual([0]);
  });
  it('deselects single cell if it is already selected', () => {
    const result = singleColumnSelectionHandler(
      [0, 1, 2, 3, 4, 5],
      0,
      { metaKey: true } as KeyboardEvent,
      1,
      [0, 1, 2, 3, 4, 5],
    );
    expect(result).toEqual([1, 2, 3, 4, 5]);
  });
  it('selects range of cells if click with shift', () => {
    const result = singleColumnSelectionHandler([], 4, { shiftKey: true } as KeyboardEvent, 1, []);
    expect(result).toEqual([1, 2, 3, 4]);
  });
  it('selects range of cells if click with shift', () => {
    const result = singleColumnSelectionHandler([1], 4, { metaKey: true } as KeyboardEvent, 1, [1]);
    expect(result).toEqual([1, 4]);
  });
  it('deselects range of cells excluding the clicked if click with shift with existing selection', () => {
    const result = singleColumnSelectionHandler(
      [0, 1, 2, 3, 4, 5, 6],
      3,
      { shiftKey: true } as KeyboardEvent,
      6,
      [0, 1, 2, 3, 4, 5, 6],
    );
    expect(result).toEqual([0, 1, 2, 3]);
  });
});
