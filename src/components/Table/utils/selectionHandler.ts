export const singleColumnSelectionHandler = (
  selection: number[],
  clickedOn: number,
  e: KeyboardEvent,
  lastSelected: number | undefined,
  selected: number[],
): number[] => {
  const isSelected = selection.includes(clickedOn);
  if (e.shiftKey && lastSelected !== undefined) {
    const min = Math.min(lastSelected, clickedOn);
    const max = Math.max(lastSelected, clickedOn);
    const selectionRange = new Array(max + 1 - min).fill(0).map((_, i) => min + i);
    if (isSelected) {
      return selection.filter((index) => index === clickedOn || !selectionRange.includes(index));
    }
    const result = Array.from(new Set([...selectionRange, ...selected]));
    return result;
  }
  if (e.ctrlKey || e.metaKey) {
    if (isSelected) {
      return selection.filter((a) => a !== clickedOn);
    }
    return [...selection, clickedOn];
  }
  return [clickedOn];
};
