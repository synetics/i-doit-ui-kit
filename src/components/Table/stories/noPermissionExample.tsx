import { NoPermissionCell } from '../Cell';

const noPermissionColumns = [
  {
    Header: 'Left aligned "no permission" cell',
    accessor: 'left',
    Cell: <NoPermissionCell align="left" />,
    minWidth: 100,
    resizable: true,
  },
  {
    Header: 'Center aligned "no permission" cell',
    accessor: 'center',
    Cell: <NoPermissionCell align="center" />,
    minWidth: 100,
    resizable: true,
  },
  {
    Header: 'Right aligned "no permission" cell',
    accessor: 'right',
    Cell: <NoPermissionCell align="right" />,
    minWidth: 100,
    resizable: true,
  },
];

const noPermissionData = [{ left: null, center: null, right: null }];

export { noPermissionColumns, noPermissionData };
