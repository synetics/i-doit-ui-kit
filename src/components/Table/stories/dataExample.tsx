import { name } from 'faker';
import { TextCell } from '../Cell';
import { SelectionCell } from '../Cell/SelectionCell';

type SimpleDataType = {
  name: string;
};

const simpleColumns = [
  {
    Header: 'Name',
    accessor: 'name' as const,
    Cell: TextCell,
    minWidth: 100,
    resizable: true,
  },
];

const simpleData: SimpleDataType[] = [
  { name: 'Anna' },
  { name: 'Karl' },
  { name: 'Lisa' },
  { name: 'Mark' },
  { name: 'Stephanie' },
];

const multipleColumns = [
  {
    Header: 'First name',
    accessor: 'firstname',
    Cell: TextCell,
    minWidth: 100,
    resizable: true,
    selectable: true,
  },
  {
    Header: 'Middle Name',
    accessor: 'middlename',
    Cell: TextCell,
    minWidth: 200,
    resizable: true,
    selectable: true,
  },
  {
    Header: 'Last Name',
    accessor: 'lastname',
    Cell: TextCell,
    minWidth: 200,
    resizable: true,
    selectable: true,
  },
];

const manyColumns = [
  {
    Header: 'First name',
    accessor: 'firstname',
    Cell: TextCell,
    minWidth: 100,
    resizable: true,
    selectable: true,
  },
  {
    Header: 'Middle Name',
    accessor: 'middlename',
    Cell: TextCell,
    minWidth: 200,
    resizable: true,
    selectable: true,
  },
  {
    Header: 'Last Name',
    accessor: 'lastname',
    Cell: TextCell,
    minWidth: 200,
    resizable: true,
    selectable: true,
  },
  {
    Header: 'Gender',
    accessor: 'gender',
    Cell: TextCell,
    minWidth: 200,
    resizable: true,
    selectable: true,
  },
  {
    Header: 'Job Title',
    accessor: 'job_title',
    Cell: TextCell,
    minWidth: 200,
    resizable: true,
    selectable: true,
  },
  {
    Header: 'Prefix',
    accessor: 'prefix',
    Cell: TextCell,
    minWidth: 200,
    resizable: true,
    selectable: true,
  },
  {
    Header: 'Suffix',
    accessor: 'suffix',
    Cell: TextCell,
    minWidth: 200,
    resizable: true,
    selectable: true,
  },
];

const multipleData = new Array(10)
  .fill(0)
  .map(() => ({ firstname: name.firstName(), middlename: name.firstName(), lastname: name.lastName() }));

const manyColumnsData = new Array(100).fill(0).map(() => ({
  firstname: name.firstName(),
  middlename: name.firstName(),
  lastname: name.lastName(),
  jobTitle: name.jobTitle(),
  gender: name.gender(),
  prefix: name.prefix(),
  suffix: name.suffix(),
}));

const multipleDataWithActions = [
  { firstname: 'Anna', status: false, favorite: false },
  { firstname: 'Karl', status: false, favorite: false },
  { firstname: 'Lisa', status: false, favorite: false },
  { firstname: 'Mark', status: false, favorite: false },
  { firstname: 'Stephanie', status: false, favorite: false },
];

const expandedData = [
  {
    title: 'Admin-Server-002',
    titleInside: 'Several',
    url: 'https://several.com',
    subRows: [
      { titleInside: 'Instrustions', url: 'https://guide.com', title: 'novalue', purpose: 'novalue' },
      { titleInside: 'Drivers', url: 'https://guide.com', title: 'novalue', purpose: 'novalue' },
    ],
    purpose: 'Office IT',
  },
  {
    title: 'Office-Server',
    titleInside: 'Drivers',
    url: 'https://guide.com',
    purpose: 'Office IT',
    subRows: [
      { titleInside: 'Cars', url: 'https://cars.com', title: 'novalue', purpose: 'novalue' },
      { titleInside: 'Cities', url: 'https://cities.com', title: 'novalue', purpose: 'novalue' },
    ],
  },
  {
    title: 'Server-002',
    titleInside: 'Instrustions',
    subRows: [
      { titleInside: 'Cars', url: 'https://cars.com', title: 'novalue', purpose: 'novalue' },
      { titleInside: 'Cities', url: 'https://cities.com', title: 'novalue', purpose: 'novalue' },
    ],
    url: 'https://guide.com',
    purpose: 'Office IT',
  },
];

const multipleColumnsWithSelected = [
  {
    Header: 'selected',
    accessor: 'selected',
    Cell: SelectionCell,
    minWidth: 100,
    resizable: true,
    selectable: true,
    overflow: false,
  },
  {
    Header: 'First name',
    accessor: 'firstname',
    Cell: TextCell,
    minWidth: 100,
    resizable: true,
    selectable: true,
  },
  {
    Header: 'Middle Name',
    accessor: 'middlename',
    Cell: TextCell,
    minWidth: 200,
    resizable: true,
    selectable: true,
  },
  {
    Header: 'Last Name',
    accessor: 'lastname',
    Cell: TextCell,
    minWidth: 200,
    resizable: true,
    selectable: true,
  },
];

export {
  simpleColumns,
  simpleData,
  multipleColumns,
  manyColumns,
  multipleData,
  multipleDataWithActions,
  expandedData,
  multipleColumnsWithSelected,
  manyColumnsData,
};
