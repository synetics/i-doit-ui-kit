import React, { FC, useMemo } from 'react';
import { CellProps, UseExpandedInstanceProps, UseExpandedRowProps } from 'react-table';
import classnames from 'classnames';
import { Table } from '../Table';
import { IconButton } from '../..';
import { TextCell } from '../Cell';
import { arrow_down, arrow_up } from '../../../icons';
import { Icon } from '../../Icon';
import { CellWrapper } from '../CellWrapper';
import styles from './ExpandableTable.module.scss';

type ExpandableProps = {
  [key: string]: string;
};

type TableWithActionProps = {
  data: ExpandableProps[];
};

type ExpandableRowProps = {
  expand?: boolean;
  isHeader?: boolean;
};

type ExpandableHeader = {
  getToggleAllRowsExpandedProps: () => UseExpandedInstanceProps<ExpandableProps>;
  isAllRowsExpanded: boolean;
};

type ExpandableCell = CellProps<ExpandableProps, string> & { row: UseExpandedRowProps<ExpandableProps> };

const ExpandableRowIcon = ({ expand, isHeader }: ExpandableRowProps) => (
  <CellWrapper className={classnames({ 'p-0': isHeader })}>
    {expand ? (
      <IconButton variant="text" label="Collapse" iconClassName={styles.expandIcon} icon={<Icon src={arrow_up} />} />
    ) : (
      <IconButton variant="text" label="Expand" iconClassName={styles.expandIcon} icon={<Icon src={arrow_down} />} />
    )}
  </CellWrapper>
);

const ExpandableTable: FC<TableWithActionProps> = ({ data }) => {
  const columns = useMemo(
    () => [
      {
        Header: '',
        id: 'aligner',
        className: styles.headerWidth,
        columns: [
          {
            id: 'expander',
            className: styles.expandButton,
            headerClassName: styles.expandHeader,
            unfocusable: true,
            hasBottomBorder: false,
            Header: ({ getToggleAllRowsExpandedProps, isAllRowsExpanded }: ExpandableHeader) => (
              <span {...getToggleAllRowsExpandedProps()}>
                <ExpandableRowIcon expand={isAllRowsExpanded} isHeader />
              </span>
            ),
            Cell: ({ row }: ExpandableCell) =>
              row.canExpand ? (
                <span
                  {...row.getToggleRowExpandedProps({
                    style: {
                      paddingLeft: `${row.depth * 2}rem`,
                    },
                  })}
                >
                  <ExpandableRowIcon expand={row.isExpanded} />
                </span>
              ) : null,
          },
          {
            Header: 'Title',
            accessor: 'title',
            headerClassName: styles.headerTitle,
            Cell: (cell: CellProps<ExpandableProps, string>) => {
              const { value } = cell;
              return value !== 'novalue' ? <TextCell {...cell} /> : null;
            },
          },
        ],
      },
      {
        Header: 'Links',
        columns: [
          {
            Header: 'Title',
            accessor: 'titleInside',
            headerClassName: styles.headerTitle,
            Cell: (cell: CellProps<ExpandableProps, string>) => <TextCell {...cell} />,
          },
          {
            Header: 'URL',
            accessor: 'url',
            headerClassName: styles.headerTitle,
            Cell: (cell: CellProps<ExpandableProps, string>) => <TextCell {...cell} />,
          },
        ],
        Cell: (cell: CellProps<ExpandableProps, string>) => <TextCell {...cell} />,
      },
      {
        Header: 'Purpose',
        accessor: 'purpose',
        headerClassName: styles.headerTitle,
        Cell: (cell: CellProps<ExpandableProps, string>) => {
          const { value } = cell;
          return value !== 'novalue' ? <TextCell {...cell} /> : null;
        },
      },
    ],
    [],
  );

  return <Table columns={columns} data={data} />;
};

export { ExpandableTable, ExpandableProps, ExpandableHeader, ExpandableCell, ExpandableRowIcon };
