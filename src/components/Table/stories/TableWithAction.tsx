import React, { FC, ReactElement, useState } from 'react';
import { CellProps } from 'react-table';
import { Table } from '../Table';
import { Checkbox } from '../../Form/Control/Checkbox';
import { StatusCell, TextCell, ToggleFavoriteCell } from '../Cell';
import { useControlledState } from '../../../hooks';

type DataStatus = {
  [key: string]: string | boolean;
};

type ValueStatus = {
  [key: string]: boolean;
};

type TableWithActionProps = {
  data: DataStatus[];
  onChange?: () => void;
};

const initialState = {
  favorite: false,
  status: false,
};

enum ActionColumns {
  status = 'status',
  favorite = 'favorite',
}

export const TableWithAction: FC<TableWithActionProps> = ({ data, onChange }) => {
  const [derivedData, setData] = useControlledState(data, onChange);
  const [value, setValue] = useState<ValueStatus>(initialState);

  const toggleStatus = (dataAttribute: string) => {
    const status = value[dataAttribute];
    const reverseStatus = !status;
    setValue({ ...value, [dataAttribute]: reverseStatus });
    setData(derivedData.map((item) => ({ ...item, [dataAttribute]: reverseStatus })));
  };

  const toggleCell = (index: number, param: string) => {
    setData(derivedData.map((item, i) => (i === index ? { ...item, [param]: !item[param] } : item)));
    setValue({
      ...value,
      [param]: value[param] ? !value[param] : value[param],
    });
  };

  const columns = [
    {
      Header: 'Name',
      accessor: 'firstname',
      Cell: (cell: CellProps<DataStatus, string>): ReactElement => <TextCell {...cell} />,
      minWidth: 100,
    },
    {
      Header: () => (
        <Checkbox
          aria-label="checkbox"
          checked={value[ActionColumns.status]}
          onChangeAfter={() => toggleStatus(ActionColumns.status)}
        />
      ),
      accessor: ActionColumns.status,
      unfocusable: true,
      minWidth: 100,
      resizable: true,
      Cell: ({ value: cellValue, row }: CellProps<DataStatus, boolean>): ReactElement => (
        <StatusCell status={cellValue} onChange={() => toggleCell(row.index, 'status')} />
      ),
    },
    {
      Header: () => (
        <ToggleFavoriteCell
          status={value[ActionColumns.favorite]}
          onChange={() => toggleStatus(ActionColumns.favorite)}
        />
      ),
      accessor: ActionColumns.favorite,
      unfocusable: true,
      minWidth: 100,
      resizable: true,
      Cell: ({ value: cellValue, row }: CellProps<DataStatus, boolean>): ReactElement => (
        <ToggleFavoriteCell status={cellValue} onChange={() => toggleCell(row.index, 'favorite')} />
      ),
    },
  ];

  return <Table columns={columns} data={derivedData} />;
};
