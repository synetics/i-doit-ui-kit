import { ReactElement } from 'react';
import { CellProps } from 'react-table';
import {
  ColorIndicatorCell,
  CurrencyCell,
  DateFormatCell,
  IconTextCell,
  LinkHoverCell,
  ListTextCell,
  NoPermissionCell,
  NotAvailableCell,
  RangeCell,
  StatusTextCell,
  SuffixTextCell,
  TextCell,
  TooltipCell,
} from '../Cell';
import { city, delete_icon, edit, location, open, star } from '../../../icons';
import { CellWrapper } from '../CellWrapper';
import { ActionCell } from '../Cell/ActionCell';
import { ExpandableCell, ExpandableHeader, ExpandableProps, ExpandableRowIcon } from './ExpandableTable';

type CellTypeDataType = {
  colored: {
    color: string;
    value?: string;
  };
  action: {
    mainIcon: {
      name: string;
      icon: string;
    };
    icons: {
      name: string;
      icon: string;
    }[];
  };
  name: string;
  age: number;
  country: string;
  money: string;
  link?: string;
  membership: Record<string, string | undefined>;
  website: Record<string, string>;
  email: Record<string, string>;
  birthdate: {
    value?: Date;
    format?: string;
  };
  birthtime: {
    value?: Date;
    format?: string;
  };
  app_number: number;
  hobbies: {
    value?: string;
    withTooltip?: boolean;
    separator?: string;
  };
  status: boolean;
  city: string;
};

type NotAvailable = {
  notAvailable?: boolean;
  noPermission?: boolean;
  align?: 'left' | 'center' | 'right';
};

function availability<T>(value: T | NotAvailable, original: (value: T) => ReactElement): ReactElement {
  if (value !== null && typeof value === 'object') {
    if ((value as NotAvailable).notAvailable) {
      return <NotAvailableCell />;
    }

    if ((value as NotAvailable).noPermission) {
      return <NoPermissionCell align={(value as NotAvailable).align} />;
    }
  }

  return original(value as T);
}

const cellTypeColumns = [
  {
    Header: '',
    id: 'Colored',
    minWidth: 100,
    resizable: true,
    columns: [
      {
        Header: 'Colored',
        accessor: 'colored',
        headerClassName: 'text-center',
        unfocusable: true,
        disableSortBy: true,
        Cell: (cell: CellProps<CellTypeDataType, CellTypeDataType['colored']>): ReactElement =>
          availability(cell.value, (value) => <ColorIndicatorCell color={value?.color} value={value?.value} />),
      },
    ],
  },
  {
    Header: '',
    id: 'Action',
    unfocusable: true,
    minWidth: 100,
    resizable: true,
    columns: [
      {
        Header: 'Action',
        accessor: 'action',
        unfocusable: true,
        disableSortBy: true,
        Cell: (cell: CellProps<CellTypeDataType, CellTypeDataType['action']>): ReactElement =>
          availability(cell.value, (value) => <ActionCell mainIcon={value?.mainIcon} icons={value?.icons} />),
      },
    ],
  },
  {
    Header: '',
    id: 'Name',
    unfocusable: true,
    minWidth: 100,
    resizable: true,
    columns: [
      {
        Header: 'Name',
        accessor: 'name',
        minWidth: 100,
        resizable: true,
        Cell: (cell: CellProps<CellTypeDataType, string>): ReactElement =>
          availability(cell.value, (value) => <TextCell value={value} />),
      },
    ],
  },
  {
    Header: '',
    id: 'Age',
    unfocusable: true,
    minWidth: 100,
    resizable: true,
    columns: [
      {
        Header: 'Age',
        accessor: 'age',
        minWidth: 100,
        resizable: true,
        Cell: (cell: CellProps<CellTypeDataType, string>): ReactElement =>
          availability(cell.value, (value) => <TextCell align="right" value={value} />),
      },
    ],
  },
  {
    Header: 'Residence',
    minWidth: 100,
    resizable: true,
    columns: [
      {
        Header: 'Country',
        accessor: 'country',
        minWidth: 100,
        resizable: true,
        Cell: (cell: CellProps<CellTypeDataType, string>): ReactElement =>
          availability(cell.value, (value) => <IconTextCell src={location} value={value} />),
      },
      {
        Header: 'City',
        accessor: 'city',
        minWidth: 100,
        resizable: true,
        Cell: (cell: CellProps<CellTypeDataType, string>): ReactElement =>
          availability(cell.value, (value) => <IconTextCell src={city} value={value} />),
      },
    ],
  },
  {
    Header: '',
    id: 'Net. income',
    unfocusable: true,
    minWidth: 100,
    resizable: true,
    columns: [
      {
        Header: 'Net. income',
        accessor: 'money',
        minWidth: 100,
        resizable: true,
        Cell: (cell: CellProps<CellTypeDataType, string>): ReactElement =>
          availability(cell.value, (value) => <CurrencyCell currency="€" value={value} />),
      },
    ],
  },
  {
    Header: '',
    id: 'Email',
    unfocusable: true,
    minWidth: 100,
    resizable: true,
    columns: [
      {
        Header: 'Email',
        accessor: 'email',
        minWidth: 100,
        resizable: true,
        Cell: (cell: CellProps<CellTypeDataType, Record<string, string>>): ReactElement =>
          availability(cell.value, (value) => <SuffixTextCell suffix={value?.suffix} value={value?.value} />),
      },
    ],
  },
  {
    Header: '',
    id: 'Website',
    unfocusable: true,
    minWidth: 100,
    resizable: true,
    columns: [
      {
        Header: 'Website',
        accessor: 'website',
        minWidth: 100,
        resizable: true,
        unfocusable: true,
        Cell: (cell: CellProps<CellTypeDataType, Record<string, string>>): ReactElement =>
          availability(cell.value, (value) => <LinkHoverCell value={value?.value} link={value?.link} />),
      },
    ],
  },
  {
    Header: '',
    id: 'Membership',
    unfocusable: true,
    minWidth: 100,
    resizable: true,
    columns: [
      {
        Header: 'Membership',
        accessor: 'membership',
        minWidth: 100,
        resizable: true,
        Cell: (cell: CellProps<CellTypeDataType, Record<string, string>>): ReactElement =>
          availability(cell.value, (value) => <StatusTextCell color={value?.color} value={value?.value} />),
      },
    ],
  },
  {
    Header: '',
    id: 'Birth date',
    unfocusable: true,
    minWidth: 100,
    resizable: true,
    columns: [
      {
        Header: 'Birth date',
        accessor: 'birthdate',
        minWidth: 100,
        resizable: true,
        Cell: (
          cell: CellProps<
            CellTypeDataType,
            {
              value?: Date;
              format?: string;
            }
          >,
        ): ReactElement =>
          availability(cell.value, (value) => <DateFormatCell format={value?.format} value={value?.value} />),
      },
    ],
  },
  {
    Header: '',
    id: 'Birth time',
    unfocusable: true,
    minWidth: 100,
    resizable: true,
    columns: [
      {
        Header: 'Birth time',
        accessor: 'birthtime',
        minWidth: 100,
        resizable: true,
        Cell: (
          cell: CellProps<
            CellTypeDataType,
            {
              value?: Date;
              format?: string;
            }
          >,
        ): ReactElement =>
          availability(cell.value, (value) => <DateFormatCell format={value?.format} value={value?.value} />),
      },
    ],
  },
  {
    Header: '',
    id: 'Weight',
    unfocusable: true,
    minWidth: 100,
    resizable: true,
    columns: [
      {
        Header: 'Weight',
        accessor: 'weight',
        minWidth: 100,
        resizable: true,
        Cell: (cell: CellProps<CellTypeDataType, Record<string, string>>): ReactElement =>
          availability(cell.value, (value) => (
            <SuffixTextCell align="between" suffix={value?.suffix} value={value?.value} />
          )),
      },
    ],
  },
  {
    Header: '',
    id: 'Hobbies',
    unfocusable: true,
    minWidth: 100,
    resizable: true,
    columns: [
      {
        Header: 'Hobbies',
        accessor: 'hobbies',
        minWidth: 100,
        resizable: true,
        Cell: (
          cell: CellProps<
            CellTypeDataType,
            {
              value?: string | string[];
              withTooltip?: boolean;
              separator?: string;
            }
          >,
        ): ReactElement =>
          availability(cell.value, (value) => (
            <ListTextCell value={value?.value} withTooltip={value?.withTooltip} separator={value?.separator} />
          )),
      },
    ],
  },
  {
    Header: '',
    id: 'CV',
    unfocusable: true,
    minWidth: 100,
    resizable: true,
    columns: [
      {
        Header: 'CV',
        accessor: 'cv',
        minWidth: 100,
        resizable: true,
        Cell: (cell: CellProps<CellTypeDataType, Record<string, string>>): ReactElement =>
          availability(cell.value, (value) => (
            <CellWrapper>
              <TooltipCell value={value?.value}>{value?.value}</TooltipCell>
            </CellWrapper>
          )),
      },
    ],
  },
  {
    Header: '',
    id: 'Application number',
    unfocusable: true,
    minWidth: 100,
    resizable: true,
    columns: [
      {
        Header: 'Application number',
        accessor: 'app_number',
        minWidth: 100,
        resizable: true,
        Cell: (cell: CellProps<CellTypeDataType, number>): ReactElement =>
          availability(cell.value, (value) => <RangeCell value={value} range={100} />),
      },
    ],
  },
  {
    Header: '',
    id: 'Application date',
    unfocusable: true,
    minWidth: 100,
    resizable: true,
    columns: [
      {
        Header: 'Application date',
        accessor: 'datetime',
        minWidth: 100,
        resizable: true,
        Cell: (cell: CellProps<CellTypeDataType, Record<string, string>>): ReactElement =>
          availability(cell.value, (value) => (
            <SuffixTextCell suffix={value?.suffix} value={value?.value} align="between" />
          )),
      },
    ],
  },
  {
    Header: '',
    id: 'Application date',
    unfocusable: true,
    minWidth: 100,
    resizable: true,
    columns: [
      {
        Header: 'Without sorting',
        accessor: 'without_sorting',
        disableSortBy: true,
        minWidth: 100,
        resizable: true,
        Cell: (cell: CellProps<CellTypeDataType, string>): ReactElement =>
          availability(cell.value, (value) => <TextCell value={value} />),
      },
    ],
  },
];

const cellTypeData = [
  {
    colored: { color: '#414ADC', value: 'DHCP Scope' },
    action: {
      mainIcon: { icon: open, name: 'Open' },
      icons: [],
    },
    name: 'Anna',
    age: 26,
    birthdate: { format: 'MM/dd/yyyy', value: new Date(1994, 2, 14) },
    birthtime: { format: 'hh:mm aaa', value: new Date(1994, 2, 14, 11, 33, 0) },
    weight: { suffix: 'kg', value: 58 },
    country: 'Germany',
    money: '2.200',
    membership: { color: '#378069', value: 'Active' },
    datetime: { value: '03/14/2020', suffix: '04:22 pm' },
    email: { suffix: '@example.com', value: 'anna.holt' },
    website: { link: 'https://example.com', value: 'example.com' },
    cv: { value: 'Lorem ipsum dolor sit amet, consetetur sadipscing elit' },
    app_number: 33,
    hobbies: { value: ['Horse riding', 'swimming', 'surfing'], withTooltip: true, separator: ';' },
    city: 'Hamburg',
    without_sorting: (Math.random() * 100).toFixed(2).toString(),
  },

  {
    colored: { color: '#FFD15B', value: 'Network address' },
    action: {
      mainIcon: { icon: open, name: 'Open' },
      icons: [{ name: 'Favorite', icon: star }],
    },
    name: 'Karl',
    age: 33,
    birthdate: { format: 'MM/dd/yyyy', value: new Date(1987, 8, 28) },
    birthtime: { format: 'hh:mm aaa', value: new Date(1987, 8, 28, 12, 31, 0) },
    weight: { suffix: 'kg', value: 85 },
    country: 'Germany',
    money: '1.800',
    membership: { color: '#e3000f', value: 'Cancelled' },
    datetime: { value: '09/28/2020', suffix: '05:20 pm' },
    email: { suffix: '@example.com', value: 'karl.klanze' },
    website: { link: 'https://example.com', value: 'example.com' },
    cv: { value: 'Sed diam nonumy eirmod tempor invidunt ut labore' },
    app_number: 22,
    hobbies: { value: ['Soccer', 'music'] },
    city: 'Berlin',
    without_sorting: (Math.random() * 100).toFixed(2).toString(),
  },
  {
    colored: { color: '#414ADC', value: 'DHCP Scope' },
    action: {
      mainIcon: { icon: open, name: 'Open' },
      icons: [
        { name: 'Delete', icon: delete_icon },
        { name: 'Edit', icon: edit },
      ],
    },
    name: 'Lisa',
    age: 25,
    birthdate: { format: 'MM/dd/yyyy', value: new Date(1995, 5, 14) },
    birthtime: { format: 'hh:mm aaa', value: new Date(1995, 5, 14, 5, 20, 0) },
    weight: { suffix: 'kg', value: 55 },
    country: 'France',
    money: '2.300',
    membership: { color: '#378069', value: 'Active' },
    datetime: { value: '07/21/2019', suffix: '11:33 pm' },
    email: { suffix: '@example.com', value: 'lisa.loraine' },
    website: { link: 'https://example.com', value: 'example.com' },
    cv: { value: 'Et dolore magna aliquyam erat' },
    app_number: 1,
    hobbies: { value: ['Cooking', 'Painting', 'Fishing', 'Football'], withTooltip: true },
    city: 'Paris',
    without_sorting: (Math.random() * 100).toFixed(2).toString(),
  },
  {
    colored: { color: '#414ADC', value: 'DHCP Scope' },
    action: {
      mainIcon: { icon: open, name: 'Open' },
      icons: [
        { name: 'Delete', icon: delete_icon },
        { name: 'Open', icon: open },
        { name: 'Edit', icon: edit },
        { name: 'Favorite', icon: star },
      ],
    },
    name: 'Mark',
    age: 41,
    birthdate: { format: 'MM/dd/yyyy', value: new Date(1979, 6, 21) },
    birthtime: { format: 'hh:mm aaa', value: new Date(1979, 6, 21, 16, 22, 0) },
    weight: { suffix: 'kg', value: 75 },
    country: 'Germany',
    money: '3.200',
    membership: { color: '#378069', value: 'Active' },
    datetime: { value: '06/14/2018', suffix: '12:31 pm' },
    email: { suffix: '@example.com', value: 'mark.schmitz' },
    website: { link: 'https://example.com', value: 'example.com' },
    cv: { value: 'Sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor' },
    app_number: 14,
    hobbies: { value: ['Baseball', 'Basketball'] },
    city: 'Munich',
    without_sorting: (Math.random() * 100).toFixed(2).toString(),
  },
  {
    colored: { color: '#FF3341', value: 'DHCP Reserved' },
    action: {
      mainIcon: { icon: open, name: 'Open' },
      icons: [
        { name: 'Delete', icon: delete_icon },
        { name: 'Open', icon: open },
        { name: 'Edit', icon: edit },
        { name: 'Favorite', icon: star },
      ],
    },
    name: 'Stephanie',
    age: 56,
    birthdate: { format: 'MM/dd/yyyy', value: new Date(1964, 10, 12) },
    birthtime: { format: 'hh:mm aaa', value: new Date(1964, 10, 12, 13, 13, 0) },
    weight: { suffix: 'kg', value: 65 },
    country: 'France',
    money: '2.800',
    membership: { color: '#fe7f00', value: 'Prospect' },
    datetime: { value: '06/14/1999', suffix: '00:01 pm' },
    email: { suffix: '@example.com', value: 'stephanie.maison' },
    website: { link: 'https://example.com/stairway-to-heaven', value: 'https://example.com/stairway-to-heaven' },
    cv: { value: 'Lorem ipsum dolor sit amet' },
    app_number: null,
    hobbies: { value: ['Outdoor', 'camping'] },
    city: 'Saint Lô',
    without_sorting: (Math.random() * 100).toFixed(2).toString(),
  },
  {
    action: null,
    name: null,
    age: null,
    birthday: null,
    country: null,
    money: null,
    membership: null,
    website: null,
    email: null,
    cv: null,
    app_number: null,
    datetime: null,
    hobbies: null,
    city: null,
    without_sorting: null,
  },
  {
    action: { notAvailable: true },
    name: { notAvailable: true },
    age: { notAvailable: true },
    birthdate: { notAvailable: true },
    birthtime: { notAvailable: true },
    weight: { notAvailable: true },
    country: { notAvailable: true },
    money: { notAvailable: true },
    membership: { notAvailable: true },
    website: { notAvailable: true },
    email: { notAvailable: true },
    cv: { notAvailable: true },
    app_number: { notAvailable: true },
    datetime: { notAvailable: true },
    hobbies: { notAvailable: true },
    city: { notAvailable: true },
    without_sorting: { notAvailable: true },
  },
  {
    action: { noPermission: true },
    name: { noPermission: true },
    age: { noPermission: true, align: 'right' },
    birthdate: { noPermission: true, align: 'right' },
    birthtime: { noPermission: true, align: 'right' },
    weight: { noPermission: true, align: 'right' },
    country: { noPermission: true },
    money: { noPermission: true },
    membership: { noPermission: true },
    website: { noPermission: true },
    email: { noPermission: true, align: 'right' },
    cv: { noPermission: true },
    app_number: { noPermission: true },
    datetime: { noPermission: true },
    hobbies: { noPermission: true },
    city: { noPermission: true },
    without_sorting: { noPermission: true },
  },
];

const expandableColumns = [
  {
    Header: '',
    id: 'aligner',
    columns: [
      {
        id: 'expander',
        Header: ({ getToggleAllRowsExpandedProps, isAllRowsExpanded }: ExpandableHeader) => (
          <span {...getToggleAllRowsExpandedProps()}>
            <ExpandableRowIcon expand={isAllRowsExpanded} isHeader />
          </span>
        ),
        Cell: ({ row }: ExpandableCell) =>
          row.canExpand ? (
            <span
              {...row.getToggleRowExpandedProps({
                style: {
                  paddingLeft: `${row.depth * 2}rem`,
                },
              })}
            >
              <ExpandableRowIcon expand={row.isExpanded} />
            </span>
          ) : null,
      },
      {
        Header: 'Title',
        accessor: 'title',
        Cell: (cell: CellProps<ExpandableProps, string>) => {
          const { value } = cell;
          return value !== 'novalue' ? <TextCell {...cell} /> : null;
        },
      },
    ],
  },
];

export { cellTypeColumns, cellTypeData, expandableColumns };
