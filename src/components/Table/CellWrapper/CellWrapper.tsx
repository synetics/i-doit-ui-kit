import { forwardRef, HTMLProps, ReactNode } from 'react';
import classnames from 'classnames';
import styles from './CellWrapper.module.scss';

export type AlignmentProp = {
  align?: 'left' | 'center' | 'right' | 'between';
};

type CellWrapperProps = {
  className?: string;
  muted?: boolean;
  nowrap?: boolean;
  children: ReactNode;
} & AlignmentProp &
  HTMLProps<HTMLDivElement>;

export const CellWrapper = forwardRef<HTMLDivElement, CellWrapperProps>(
  ({ align = 'left', children, className, muted = false, nowrap = false, ...rest }, ref) => (
    <div
      {...rest}
      ref={ref}
      data-testid="cell-wrapper"
      className={classnames(styles['cell-wrapper'], className, styles[align], {
        [styles.muted]: muted,
        [styles.nowrap]: nowrap,
      })}
    >
      {children}
    </div>
  ),
);
