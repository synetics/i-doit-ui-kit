import { useState } from 'react';
import { ColorPicker } from '../Form/Control/ColorPicker';
import { Label } from '../Menu/components/items/Label';
import { Checkbox, Input, Radio } from '../Form/Control';
import { Progress } from './Progress';

export default {
  title: 'Components/Atoms/Progress',
  component: Progress,
};

export const ProgressComponent = () => {
  const [color, setColor] = useState('414adc');
  const [value, setValue] = useState(10);
  const [total, setTotal] = useState(100);
  const [size, setSize] = useState<'xs' | 'sm' | 'md' | 'lg'>('sm');
  const [orientation, setOrientation] = useState(false);
  return (
    <div style={{ width: 400, height: 400 }}>
      <Checkbox aria-label="orientation" label="Vertical" onChangeAfter={setOrientation} />
      <Label label="Progress component" className="mb-4" />
      <Progress
        value={value}
        total={total}
        color={color}
        size={size}
        orientation={orientation ? 'vertical' : 'horizontal'}
        className="mb-4"
      />
      <Label label="Value" />
      <Input className="mb-4" id="value" type="number" value={value} onChange={(v) => setValue(+v)} />
      <Label label="Total" />
      <Input className="mb-4" id="total" type="number" value={total} onChange={(v) => setTotal(+v)} />
      <Label label="Choose size" className="mb-4" />
      <Radio label="Extra small" aria-label="Extra small" onChange={() => setSize('xs')} checked={size === 'xs'} />
      <Radio label="Small" aria-label="Small" onChange={() => setSize('sm')} checked={size === 'sm'} />
      <Radio label="Medium" aria-label="Medium" onChange={() => setSize('md')} checked={size === 'md'} />
      <Radio label="Large" aria-label="Large" onChange={() => setSize('lg')} checked={size === 'lg'} className="mb-4" />
      <Label label="Choose color" className="mb-4" />
      <ColorPicker value={color} onChange={setColor} className="mb-4" />
    </div>
  );
};
