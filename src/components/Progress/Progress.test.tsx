import { render, screen } from '@testing-library/react';
import React from 'react';
import { Progress, ProgressSize } from './Progress';

describe('Progress', () => {
  it('renders without crashing', () => {
    render(<Progress value={1} total={3} />);
    const progressBar = screen.getByRole('progressbar');
    expect(progressBar).toBeInTheDocument();
  });

  it('displays correct progress percentage in horizontal orientation', () => {
    render(<Progress value={2} total={4} orientation="horizontal" />);
    const bar = screen.getByTestId('bar-id');
    expect(bar).toHaveStyle('width: 50%');
  });

  it('displays correct progress percentage in vertical orientation', () => {
    render(<Progress value={2} total={4} orientation="vertical" />);
    const bar = screen.getByTestId('bar-id');
    expect(bar).toHaveStyle('height: 50%');
  });

  it('to have a className', () => {
    render(<Progress className="test" value={2} total={4} />);
    const progressBar = screen.getByRole('progressbar');
    expect(progressBar).toHaveClass('test');
  });

  it.each<[ProgressSize]>([['xs'], ['sm'], ['md'], ['lg']])('renders %s size in horizontal orientation', (size) => {
    render(<Progress value={1} total={3} size={size} orientation="horizontal" />);
    const progressBarContainer = screen.getByRole('progressbar');
    expect(progressBarContainer).toHaveClass(size);
  });

  it.each<[ProgressSize]>([['sm'], ['md'], ['lg']])('renders %s size in vertical orientation', (size) => {
    render(<Progress value={1} total={3} size={size} orientation="vertical" />);
    const progressBarContainer = screen.getByRole('progressbar');
    expect(progressBarContainer).toHaveClass(size);
  });
});
