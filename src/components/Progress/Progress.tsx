import React from 'react';
import classnames from 'classnames';
import classes from './Progress.module.scss';

export type ProgressSize = 'xs' | 'sm' | 'md' | 'lg';
export type Orientation = 'horizontal' | 'vertical';

type ProgressProps = {
  className?: string;
  value: number;
  total: number;
  size?: ProgressSize;
  color?: string;
  orientation?: Orientation;
};

export const Progress = ({
  className,
  value,
  total,
  size = 'sm',
  color,
  orientation = 'horizontal',
}: ProgressProps) => {
  const percentage = Math.min(100, Math.max(0, (value / total) * 100));

  return (
    <div
      className={classnames(classes.container, classes[size], classes[orientation], className)}
      role="progressbar"
      aria-valuenow={value}
      aria-valuemin={0}
      aria-valuemax={total}
    >
      <div
        data-testid="bar-id"
        className={classnames(classes.bar, classes[orientation])}
        style={
          orientation === 'horizontal'
            ? { width: `${percentage}%`, backgroundColor: color || '' }
            : { height: `${percentage}%`, backgroundColor: color || '' }
        }
      />
    </div>
  );
};
