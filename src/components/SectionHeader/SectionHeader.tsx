import { ReactNode } from 'react';
import classnames from 'classnames';
import { Headline, HeadlineTag } from '../Headline';
import styles from './SectionHeader.module.scss';

type SectionHeaderProps = {
  /**
   * A string to use a header HTML element
   */
  tag?: HeadlineTag;

  /**
   * Class name
   */
  className?: string;

  /**
   * Content that will be appended at the end of the component
   */
  children?: ReactNode | ReactNode[];

  /**
   * Arbitrary string that describes content placed bellow the component
   */
  title: ReactNode;
};

export const SectionHeader = ({ className, tag = 'h3', children, title }: SectionHeaderProps) => (
  <div data-testid="idoit-section-header" className={classnames(styles.sectionHeader, className)}>
    <Headline tag={tag} className={styles.sectionTitle}>
      {title}
    </Headline>
    {children && <div className={styles.actionPanel}>{children}</div>}
  </div>
);
