import { render, screen } from '@testing-library/react';
import { SectionHeader } from './SectionHeader';

describe('SectionHeader', () => {
  it('renders without crashing', () => {
    render(<SectionHeader title="title" />);

    expect(screen.getByTestId('idoit-section-header')).toBeInTheDocument();
  });

  it('renders with title', () => {
    render(<SectionHeader title="title" />);

    expect(screen.getByRole('heading', { level: 3 })).toBeInTheDocument();
  });

  it('renders with given level of heading', () => {
    render(<SectionHeader title="title" tag="h2" />);

    expect(screen.getByRole('heading', { level: 2 })).toBeInTheDocument();
  });

  it('renders children', () => {
    render(<SectionHeader title="title">hello</SectionHeader>);

    expect(screen.getByText('hello')).toBeInTheDocument();
  });
});
