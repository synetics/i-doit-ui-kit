import { ReactNode } from 'react';
import { edit, more_vertical } from '../../icons';
import { IconButton } from '../IconButton';
import { Icon } from '../Icon';
import { SectionHeader } from './SectionHeader';

export default {
  title: 'Components/Molecules/Section Header',
  component: SectionHeader,
};

export const Regular = (): ReactNode => (
  <SectionHeader title="Title of the section header">
    <IconButton icon={<Icon src={edit} />} label="Edit" variant="text" />
    <IconButton icon={<Icon src={more_vertical} />} label="More options" variant="text" />
  </SectionHeader>
);
