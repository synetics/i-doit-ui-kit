import { name } from 'faker';
import { ReactNode, useState } from 'react';
import { Item, Separator } from '../Menu/components';
import { Wrapper } from '../Modal';
import { Icon } from '../Icon';
import { NavigationItem } from '../NavigationItem';
import { Button, ButtonSpacer } from '../Button';
import { ButtonPulldown } from '../ButtonPulldown';
import { AvatarPulldown } from '../PulldownAvatar';
import { IconButton } from '../IconButton';
import { info, history } from '../../icons';
import { MainNavigation } from './MainNavigation';
import { MainNavigationItem } from './MainNavigationItem';

const items = new Array(70).fill(0).map((_, i) => ({
  label: `{Tenant${i}}`,
  id: `id${i}`,
}));

const avatarItems = new Array(70).fill(0).map((_, i) => ({
  label: name.firstName()[0] + name.lastName()[0],
  id: `id${i}`,
}));

export default {
  title: 'Components/Organisms/MainNavigation',
  component: MainNavigation,
};

export const Regular = (): ReactNode => {
  const [menuLabel, setMenuLabel] = useState('Lorem ipsum dolor sit amet, consetetur sadipscing elitr');
  const [menuLabelAvatar, setMenuLabelAvatar] = useState('AA');

  return (
    <MainNavigation>
      <NavigationItem label={menuLabel}>
        {(onClose) => (
          <>
            <Item id="settings" label="Tenant setting " onSelect={() => onClose()} />
            <Separator />
            <Wrapper
              className="p-0 border-0"
              style={{
                maxHeight: 300,
                overflow: 'auto',
                display: 'flex',
                flexDirection: 'column',
              }}
            >
              {items.map(({ label, id }) => (
                <Item
                  id={id}
                  label={label}
                  onSelect={() => {
                    onClose();
                    setMenuLabel(label);
                  }}
                />
              ))}
            </Wrapper>
          </>
        )}
      </NavigationItem>
      <MainNavigationItem>
        <a href="https://www.google.de/" target="_blank" rel="noreferrer noopener">
          <img
            style={{
              width: 112.86,
              marginLeft: 24,
            }}
            className="mr-8"
            src="/assets/images/logo-white.svg"
            alt="docupike"
          />
        </a>
      </MainNavigationItem>
      <div className="d-flex w-100 justify-content-between">
        <ButtonSpacer gridSize="m" className="d-flex">
          <ButtonPulldown label="Inventory" variant="ghost">
            {(onClose) => (
              <>
                <Item id="item-1" label="{Item}" onSelect={() => onClose()} />
                <Item id="item-2" label="{Item}" onSelect={() => onClose()} />
              </>
            )}
          </ButtonPulldown>
          <Button variant="ghost">Reports</Button>
        </ButtonSpacer>
        <ButtonSpacer gridSize="m" className="d-flex">
          <Button variant="ghost" icon={<Icon src={history} wrapper="span" fill="white" />} iconPosition="right">
            History
          </Button>
          <IconButton icon={<Icon src={info} />} label="Info" variant="ghost" />
        </ButtonSpacer>
      </div>
      <div
        style={{
          padding: 20,
          marginRight: 40,
        }}
      >
        <AvatarPulldown label={menuLabelAvatar}>
          {(onClose) => (
            <>
              <Wrapper
                className="p-0 border-0"
                style={{
                  maxHeight: 300,
                  overflow: 'auto',
                  display: 'flex',
                  flexDirection: 'column',
                }}
              >
                {avatarItems.map(({ label, id }) => (
                  <Item
                    id={id}
                    label={label}
                    onSelect={() => {
                      onClose();
                      setMenuLabelAvatar(label);
                    }}
                  />
                ))}
              </Wrapper>
            </>
          )}
        </AvatarPulldown>
      </div>
    </MainNavigation>
  );
};
