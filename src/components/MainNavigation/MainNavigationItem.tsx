import { ReactNode } from 'react';
import classnames from 'classnames';
import classes from './MainNavigation.module.scss';

type MainNavigationItemProps = {
  /** Class of the nav bar */
  className?: string;
  /**
   * The content of the MainNavigation
   */
  children?: ReactNode | ReactNode[];
};

const MainNavigationItem = ({ children, className }: MainNavigationItemProps) => (
  <li className={classnames(classes.navigationItem, className)} role="menuitem">
    {children}
  </li>
);

export { MainNavigationItem };
