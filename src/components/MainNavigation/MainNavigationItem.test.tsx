import { render, screen } from '@testing-library/react';
import React from 'react';
import { Button } from '../Button';
import { MainNavigation } from './MainNavigation';
import { MainNavigationItem } from './MainNavigationItem';

describe('mainNavigationItem', () => {
  it('renders without crashing', () => {
    render(
      <MainNavigation className="mainNavigation">
        <MainNavigationItem className="mainNavigationItem">
          <Button variant="text" className="buttonNavigation">
            Button
          </Button>
        </MainNavigationItem>
      </MainNavigation>,
    );
    const navigationItem = screen.getByRole('menuitem');
    const button = screen.getByText('Button');

    expect(navigationItem).toBeInTheDocument();
    expect(navigationItem).toHaveClass('mainNavigationItem');
    expect(button).toBeInTheDocument();
    expect(button).toHaveClass('buttonNavigation');
  });

  it('renders with some label', () => {
    render(
      <MainNavigation className="mainNavigation" label="some-label">
        <MainNavigationItem className="mainNavigationItem">
          <Button variant="text" className="buttonNavigation">
            Button
          </Button>
        </MainNavigationItem>
      </MainNavigation>,
    );
    const navigationItem = screen.getByLabelText('some-label');

    expect(navigationItem).toBeInTheDocument();
    expect(navigationItem).toHaveClass('mainNavigation');
  });
});
