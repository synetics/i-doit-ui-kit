import { render, screen } from '@testing-library/react';
import React from 'react';
import { Button } from '../Button';
import { MainNavigation } from './MainNavigation';

describe('mainNavigation', () => {
  it('renders without crashing', () => {
    render(
      <MainNavigation className="mainNavigation" label="main">
        <Button variant="text" className="buttonNavigation">
          Button
        </Button>
      </MainNavigation>,
    );
    const navigation = screen.getByLabelText('main');

    expect(navigation).toBeInTheDocument();
    expect(navigation).toHaveClass('mainNavigation');
  });
});
