import { ReactNode } from 'react';
import classnames from 'classnames';
import classes from './MainNavigation.module.scss';

type MainNavigationProps = {
  /** Class of the nav bar */
  className?: string;
  /** Navigation label */
  label?: string;
  /** The content of the MainNavigation */
  children?: ReactNode | ReactNode[];
};

const MainNavigation = ({ children, className, label = 'Main' }: MainNavigationProps) => (
  <nav aria-label={label} className={classnames(classes.navigation, className)}>
    <ul className={classnames(classes.navigationList)}>{children}</ul>
  </nav>
);

export { MainNavigation };
