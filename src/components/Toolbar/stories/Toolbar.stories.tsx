import { ReactNode, useState } from 'react';
import { Button } from '../../Button';
import { Toolbar } from '../Toolbar';
import { Section } from '../Section';
import { Headline } from '../../Headline';
import { Icon } from '../../Icon';
import { SearchField } from '../../Form/Control/SearchField';
import { location } from '../../../icons';
import { Radio } from '../../Form/Control';
import styles from './Toolbar.module.scss';

export default {
  title: 'Components/Organisms/Toolbar',
  component: Toolbar,
};

/**
 *
 * Receives children items as child and renders them in right way with right spacings
 *
 */
export const ToolbarComponent = (): ReactNode => {
  const [size, setSize] = useState<'m' | 'l'>('m');
  return (
    <>
      <Radio
        name="regular-label"
        label="Meduim"
        aria-label="Meduim"
        onChange={() => setSize('m')}
        checked={size === 'm'}
      />
      <Radio
        name="regular-label"
        label="Large"
        aria-label="Large"
        onChange={() => setSize('l')}
        checked={size === 'l'}
      />
      <div className="d-flex mb-5 mt-5">
        <div className={styles.label}>1 child</div>
        <Toolbar size={size}>
          <Section>
            <Button variant="primary">{`{Label}`}</Button>
            <Button variant="secondary">{`{Label}`}</Button>
          </Section>
        </Toolbar>
      </div>
      <div className="d-flex mb-5 mt-5">
        <div className={styles.label}>2 child</div>
        <Toolbar size={size}>
          <Section>
            <Headline className={styles.marginBottomHeadline} as="h2">{`{Headline}`}</Headline>
            <Icon src={location} />
          </Section>
          <Section>
            <Button variant="primary">{`{Label}`}</Button>
            <Button variant="secondary">{`{Label}`}</Button>
          </Section>
        </Toolbar>
      </div>
      <div className="d-flex mb-5 mt-5">
        <div className={styles.label}>3 child</div>
        <Toolbar size={size}>
          <Section>
            <div style={{ whiteSpace: 'nowrap' }}>
              <Headline className={styles.marginBottomHeadline} as="h2">{`{Headline}`}</Headline>
              <Headline className={styles.subHeadline} as="h4">{`{Sub-Headline}`}</Headline>
            </div>
            <Icon src={location} />
          </Section>
          <Section>
            <SearchField
              id="{Value}"
              value="{Value}"
              isLoading={false}
              onChange={() => false}
              placeholder="Search in objects"
              flexible={false}
              className={styles.marginForm}
            />
          </Section>
          <Section>
            <Button variant="primary">{`{Label}`}</Button>
            <Button variant="secondary">{`{Label}`}</Button>
          </Section>
        </Toolbar>
      </div>
    </>
  );
};
