import React, { PropsWithChildren, ReactNode } from 'react';
import classnames from 'classnames';
import { hasClassName } from '../../utils/helpers';
import styles from './Toolbar.module.scss';

type SectionProps = PropsWithChildren<{
  /**
   * Classname for container
   */
  className?: string;
}>;

export const Section = ({ children, className, ...props }: SectionProps) => {
  const length = React.Children.count(children);
  const clonedElements = React.Children.map<ReactNode, ReactNode>(children, (child, i) => {
    if (React.isValidElement(child)) {
      let childClassName = '';
      if (hasClassName(child.props)) {
        childClassName = child.props.className;
      }
      return React.cloneElement(child, {
        ...child.props,
        className: classnames(childClassName, {
          [styles.marginRight]: i !== length - 1,
        }),
      });
    }
    return undefined;
  });
  return (
    <div className={classnames(styles.section, className)} {...props}>
      {clonedElements}
    </div>
  );
};
