import React, { ReactElement } from 'react';
import classnames from 'classnames';
import styles from './Toolbar.module.scss';

type ToolbarProps = { children: ReactElement[] | ReactElement; className?: string; size?: 'm' | 'l' };

export const Toolbar = ({ children, className, size = 'm' }: ToolbarProps) => {
  const length = React.Children.count(children);
  const itemArr = React.Children.toArray(children);
  return (
    <div
      data-testid="toolbar"
      className={classnames(className, styles.container, {
        [styles.largeContainer]: size === 'l',
        [styles.mediumContainer]: size === 'm',
      })}
    >
      {length > 2 ? (
        <>
          <div className={styles.addSectionFirst}>
            <div className={size === 'l' ? styles.largeSection : styles.mediumSection}>{itemArr[0]}</div>
            <div>{itemArr[1]}</div>
          </div>
          <div>{itemArr[2]}</div>
        </>
      ) : (
        children
      )}
    </div>
  );
};
