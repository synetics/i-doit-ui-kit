import { render, screen } from '@testing-library/react';
import { Toolbar } from './Toolbar';
import { Section } from './Section';

describe('Toolbar', () => {
  it('renders without crashing', () => {
    render(
      <Toolbar>
        <Section>ToolbarContent</Section>
      </Toolbar>,
    );

    expect(screen.getByTestId('toolbar')).toBeInTheDocument();
  });

  it.each([['l'], ['m']])('renders several children without crashing', (size: string) => {
    render(
      <Toolbar size={size as 'l' | 'm'}>
        <Section>ToolbarContent</Section>
        <Section>ToolbarContent</Section>
        <Section>
          <div className="some_style">ToolbarContent</div>
        </Section>
      </Toolbar>,
    );

    expect(screen.getByTestId('toolbar')).toBeInTheDocument();
  });
});
