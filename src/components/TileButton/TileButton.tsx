import classNames from 'classnames';
import { cloneElement, forwardRef, ReactElement } from 'react';
import { Text } from '../Text';
import { Tooltip } from '../Tooltip';
import { Button, ButtonProps } from '../Button';
import styles from './TileButton.module.scss';

type TileButtonProps = Omit<ButtonProps, 'icon' | 'iconPosition' | 'label'> & {
  /**
   * Text on button.
   */
  text?: string;
  /**
   * Text for tooltip.
   */
  label?: string;
  /**
   * Element placed inside the component.
   */
  icon: ReactElement;
  /**
   * additional className for icon
   */
  iconClassName?: string;
  /**
   * If true label will be shown in the tooltip
   */
  withTooltip?: boolean;
  /**
   * Defines placement of the tooltip
   */
  tooltipPlacement?: 'bottom' | 'left' | 'top' | 'right';
  /**
   * Defines classname of the tooltip container
   */
  tooltipClassName?: string;
};

const TileButton = forwardRef<HTMLButtonElement, TileButtonProps>(
  (
    {
      icon,
      text,
      label,
      disabled,
      loading,
      className,
      iconClassName,
      tooltipPlacement = 'bottom',
      withTooltip = true,
      tabIndex,
      tooltipClassName,
      ...props
    },
    ref,
  ) => {
    const isDisabled = disabled || loading;
    const tileButton = (
      <Button
        className={classNames(styles.container, className, {
          [styles.disabled]: isDisabled,
          [styles.loading]: loading,
        })}
        variant="outline"
        disabled={disabled}
        loading={loading}
        icon={cloneElement(icon)}
        tabIndex={isDisabled ? -1 : tabIndex}
        {...props}
        ref={ref}
      >
        <div className={styles.textContainer}>
          <Text className={styles.text}>{text}</Text>
        </div>
      </Button>
    );

    if (disabled || loading || !withTooltip) {
      return tileButton;
    }

    return (
      <Tooltip className={tooltipClassName} placement={tooltipPlacement} content={<div>{label}</div>}>
        {tileButton}
      </Tooltip>
    );
  },
);

export { TileButton, TileButtonProps };
