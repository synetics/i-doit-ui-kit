import { ReactNode } from 'react';
import { plus, plus_circled, search, open_menu, arrow_down } from '../../icons';
import { Icon } from '../Icon';
import { TileButton } from './TileButton';

export default {
  title: 'Components/Atoms/Buttons/TileButton',
  component: TileButton,
};

/**
 *
 * Tile buttons are reduced option to show interactions on the screen. They have text inside and also a label,
 * so user can fully understand what functionality it does.
 *
 */
export const Regular = (): ReactNode => (
  <>
    <div style={{ minHeight: 150 }}>
      <TileButton icon={<Icon src={plus} />} text="{{Regular}}" label="regular" />
    </div>
    <div style={{ minHeight: 150 }}>
      <TileButton
        icon={<Icon src={plus} />}
        text="{{Regular more text in several lines Regular more text in several lines}}"
        label="regular"
      />
    </div>
  </>
);

export const Disabled = (): ReactNode => (
  <>
    <div style={{ minHeight: 150 }}>
      <TileButton icon={<Icon src={plus_circled} />} text="{{Disabled}}" disabled label="disabled" />
    </div>
    <div style={{ minHeight: 150 }}>
      <p>Disabled with tooltip</p>
      <TileButton
        icon={<Icon src={search} />}
        label="disabled with tooltip"
        text="{{Disabled with tooltip}}"
        disabled
        disabledTooltip="Disabled for some reason"
      />
    </div>
  </>
);

export const Loading = (): ReactNode => (
  <div style={{ minHeight: 150 }}>
    <TileButton icon={<Icon src={open_menu} />} label="loading" text="{{Loading}}" loading />
  </div>
);

export const Example = (): ReactNode => (
  <TileButton
    icon={<Icon src={arrow_down} />}
    tooltipClassName="mr-5"
    text="my_button"
    label="disabled"
    loading
    variant="text"
  />
);
