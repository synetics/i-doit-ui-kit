import { act, fireEvent, render, screen } from '@testing-library/react';
import user from '@testing-library/user-event';
import { palette } from '../../icons';
import { Icon } from '../Icon';
import { TileButton } from './TileButton';

describe('TileButton', () => {
  const icon = <Icon src={palette} />;

  it('renders without crashing', async () => {
    render(<TileButton icon={icon} label="aria label" />);
    const tileButton = screen.getByRole('button');

    expect(screen.getByTestId('icon')).toBeInTheDocument();

    await user.hover(tileButton);
    expect(await screen.findByRole('tooltip')).toBeInTheDocument();

    await user.unhover(tileButton);
    expect(screen.queryByRole('tooltip')).not.toBeInTheDocument();

    fireEvent.focus(tileButton);
    expect(await screen.findByRole('tooltip')).toBeInTheDocument();
  });

  it('renders in disabled state', async () => {
    render(<TileButton icon={icon} label="aria label" disabled />);
    const tileButton = screen.getByRole('button');

    await user.hover(tileButton);

    expect(screen.queryByRole('tooltip')).not.toBeInTheDocument();
    expect(tileButton).toBeDisabled();
  });

  it('renders in loading state', async () => {
    render(<TileButton icon={icon} label="aria label" loading />);

    const iconButton = screen.getByRole('button');

    await user.hover(iconButton);

    expect(iconButton).toBeDisabled();
    expect(screen.getByTestId('idoit-spinner')).toBeInTheDocument();
    expect(screen.queryByRole('tooltip')).not.toBeInTheDocument();
  });

  it('raises onClick event', async () => {
    const onClickMock = jest.fn();
    render(<TileButton icon={icon} label="aria label" onClick={onClickMock} />);

    await act(async () => {
      await user.click(screen.getByRole('button'));
    });

    expect(onClickMock).toHaveBeenCalledTimes(1);
  });
});
