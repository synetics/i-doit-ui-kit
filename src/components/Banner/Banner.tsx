import React, { ReactNode } from 'react';
import classNames from 'classnames';
import style from './Banner.module.scss';

export type Variant = 'system' | 'error' | 'success' | 'warning';

type BannerProps = {
  /**
   * Class of the container
   */
  className?: string;
  /**
   * Accent style of the component
   */
  variant?: Variant;
  /**
   * The content of the field
   */
  children?: ReactNode | ReactNode[];
  /**
   * to set a Shadow to the Banner
   */
  withShadow?: boolean;
};

const Banner = ({ className, variant, withShadow = true, children }: BannerProps) => (
  <div className={classNames(className, style.banner, variant && style[variant], withShadow && style.withShadow)}>
    {children}
  </div>
);
export { Banner, BannerProps };
