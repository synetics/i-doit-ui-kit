import { ReactNode } from 'react';
import { check_circled, close_circled, info, star, warning } from '../../icons';
import { Button, ButtonSpacer } from '../Button';
import { Checkbox } from '../Form/Control/Checkbox';
import { Banner, Variant } from './Banner';
import { BannerHeader } from './BannerHeader';
import { BannerBody } from './BannerBody';
import { BannerFooter } from './BannerFooter';
import { NotificationHeader } from './NotificationHeader';
import { NotificationFooter } from './NotificationFooter';
import { NotificationBody } from './NotificationBody';

export default {
  title: 'Components/Molecules/Banner',
  component: Banner,
};

/**
 *
 * Banner is to be used to show the information to user.
 *
 */
export const BannerComponent = (): ReactNode => (
  <Banner>
    <BannerHeader variant="system" headerIcon={info} onClose={() => true}>
      Regular example
    </BannerHeader>
    <BannerBody>
      Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind
      texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small
      river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic
      country, in which roasted parts of sentences fly into your mouth. Even the all-powerful Pointing has no control
      about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name
      of Lorem Ipsum decided to leave for the far World of Grammar. The Big Oxmox advised her not to do so, because
      there were thousands of bad Commas, wild Question Marks and devious Semikoli, but the Little Blind Text didn’t
      listen.
    </BannerBody>
    <BannerFooter>
      <div
        style={{
          flexGrow: 1,
        }}
      >
        <Checkbox id="anatomy-1" aria-label="Send me notifications" label="Send me notifications" />
      </div>
      <div>
        <Button variant="secondary">OK</Button>
      </div>
    </BannerFooter>
  </Banner>
);

/**
 *
 * The essential content of the banner is its header.
 *
 * only `BannerHeader`:
 */
export const BasicBanner = (): ReactNode => (
  <Banner>
    <BannerHeader variant="system">Header only example</BannerHeader>
  </Banner>
);

/**
 *
 * The essential content of the banner is its header.
 *
 */
export const BannerWithHeader = (): ReactNode => (
  <Banner>
    <BannerHeader variant="system">Header only example</BannerHeader>
  </Banner>
);

/**
 *
 * You can define the icon of the header by passing headerIcon property:
 *
 */
export const HeaderWithIcon = (): ReactNode => (
  <Banner>
    <BannerHeader variant="system" headerIcon={info}>
      Header with Icon
    </BannerHeader>
  </Banner>
);

/**
 *
 * You can add a close button, by passing `onClose` callback:
 *
 */

export const WithCloseButtonExample = (): ReactNode => (
  <Banner>
    <BannerHeader variant="system" headerIcon={info} onClose={() => true}>
      With close button example
    </BannerHeader>
  </Banner>
);

/**
 *
 * only `BannerBody`:
 *
 */

export const BodyOnlyExample = (): ReactNode => (
  <Banner>
    <BannerBody variant="system" onClose={() => true}>
      Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind
      texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.
    </BannerBody>
  </Banner>
);

/**
 *
 * To pass the body of the banner, pass the React Element or text to the `body` property:
 *
 */

export const BannerWithBody = (): ReactNode => (
  <Banner>
    <BannerHeader variant="system">Banner with body</BannerHeader>
    <BannerBody>
      Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind
      texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.
    </BannerBody>
  </Banner>
);

/**
 *
 * To add the footer of the banner, pass the `footer` property
 *
 */
export const BannerWithBodyAndFooter = (): ReactNode => (
  <Banner>
    <BannerHeader variant="system" headerIcon={info}>
      Banner with body and footer
    </BannerHeader>
    <BannerBody>
      Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind
      texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.
    </BannerBody>
    <BannerFooter>
      <div
        style={{
          flexGrow: 1,
        }}
      >
        <Checkbox id="anatomy-1" aria-label="Send me notifications" label="Send me notifications" />
      </div>
      <div>
        <Button variant="secondary">OK</Button>
      </div>
    </BannerFooter>
  </Banner>
);

/**
 *
 * If the banner is built in the modal, it should not be raised. To disable shadow of the banner, set the option `withShadow={ false }`
 *
 */

export const BannerWithoutShadow = (): ReactNode => (
  <Banner variant="system" withShadow={false}>
    <BannerHeader headerIcon={star}>Banner without shadow</BannerHeader>
    <BannerBody>
      Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind
      texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.
    </BannerBody>
    <BannerFooter>
      <div
        style={{
          flexGrow: 1,
        }}
      >
        <Checkbox aria-label="Send me notifications" label="Send me notifications" />
      </div>
      <div>
        <Button variant="secondary">OK</Button>
      </div>
    </BannerFooter>
  </Banner>
);

/**
 *
 * According to the use-case, you can choose the color of the banner:
 *
 * ### System
 *
 * Should be used for the notifications
 *
 */
export const BannerColors = (): ReactNode => (
  <Banner>
    <BannerHeader variant="system" headerIcon={star}>
      System Banner
    </BannerHeader>
    <BannerBody>
      Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind
      texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.
    </BannerBody>
    <BannerFooter>
      <div
        style={{
          flexGrow: 1,
        }}
      >
        <Checkbox aria-label="Send me notifications" label="Send me notifications" />
      </div>
      <div>
        <Button variant="secondary">OK</Button>
      </div>
    </BannerFooter>
  </Banner>
);

/**
 *
 *
 * Should be used for the success notifications
 *
 */

export const Success = (): ReactNode => (
  <Banner>
    <BannerHeader variant="success" headerIcon={star}>
      Success Banner
    </BannerHeader>
    <BannerBody>
      Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind
      texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.
    </BannerBody>
    <BannerFooter>
      <div
        style={{
          flexGrow: 1,
        }}
      >
        <Checkbox aria-label="Send me notifications" label="Send me notifications" />
      </div>
      <div>
        <Button variant="secondary">OK</Button>
      </div>
    </BannerFooter>
  </Banner>
);

/**
 *
 * Should be used to show the error
 *
 */

export const Error = (): ReactNode => (
  <Banner>
    <BannerHeader variant="error" headerIcon={star}>
      Error Banner
    </BannerHeader>
    <BannerBody>
      Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind
      texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.
    </BannerBody>
    <BannerFooter>
      <div
        style={{
          flexGrow: 1,
        }}
      >
        <Checkbox aria-label="Send me notifications" label="Send me notifications" />
      </div>
      <div>
        <Button variant="secondary">OK</Button>
      </div>
    </BannerFooter>
  </Banner>
);

/**
 *
 * Should be used to show the warning
 *
 */

export const Warning = (): ReactNode => (
  <div style={{ maxWidth: 500 }}>
    <Banner>
      <BannerHeader variant="warning" headerIcon={warning} onClose={() => {}}>
        Warning Banner
      </BannerHeader>
      <BannerBody>
        Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind
        texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.
      </BannerBody>
      <BannerFooter>
        <ButtonSpacer>
          <Button variant="secondary">Label</Button>
          <Button variant="secondary">OK</Button>
        </ButtonSpacer>
      </BannerFooter>
    </Banner>
  </div>
);

/**
 *
 * Notification Banners
 *
 */

export const NotificationBanners = (): ReactNode => {
  const notifications: { icon: string; variant: Variant }[] = [
    { icon: warning, variant: 'warning' },
    { icon: info, variant: 'system' },
    { icon: check_circled, variant: 'success' },
    { icon: close_circled, variant: 'error' },
  ];

  return (
    <div style={{ maxWidth: 500 }}>
      {notifications.map(({ icon, variant }) => (
        <Banner className="mb-6">
          <NotificationHeader variant={variant} headerIcon={icon} onClose={() => {}}>
            {variant} notification
          </NotificationHeader>
          <NotificationBody>
            Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the
            blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language
            ocean.
          </NotificationBody>
          <NotificationFooter>
            <ButtonSpacer>
              <Button variant="secondary">Label</Button>
              <Button variant="secondary">OK</Button>
            </ButtonSpacer>
          </NotificationFooter>
        </Banner>
      ))}
    </div>
  );
};

/**
 *
 * To use the full color banner, set the `variant` property of the Banner component.
 *
 * #### System
 *
 */

export const FullColorSystemBanner = (): ReactNode => (
  <Banner variant="system">
    <BannerHeader headerIcon={star}>System Banner</BannerHeader>
    <BannerBody>
      Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind
      texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.
    </BannerBody>
    <BannerFooter>
      <div
        style={{
          flexGrow: 1,
        }}
      >
        <Checkbox aria-label="Send me notifications" label="Send me notifications" />
      </div>
      <div>
        <Button variant="secondary">OK</Button>
      </div>
    </BannerFooter>
  </Banner>
);

/**
 *
 * #### Success
 *
 */

export const FullColorSuccessBanner = (): ReactNode => (
  <Banner variant="success">
    <BannerHeader headerIcon={star}>Success Banner</BannerHeader>
    <BannerBody>
      Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind
      texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.
    </BannerBody>
    <BannerFooter>
      <div
        style={{
          flexGrow: 1,
        }}
      >
        <Checkbox aria-label="Send me notifications" label="Send me notifications" />
      </div>
      <div>
        <Button variant="secondary">OK</Button>
      </div>
    </BannerFooter>
  </Banner>
);

/**
 *
 * #### Error
 *
 */

export const FullColorErrorBanner = (): ReactNode => (
  <Banner variant="error">
    <BannerHeader headerIcon={star}>Error Banner</BannerHeader>
    <BannerBody>
      Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind
      texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.
    </BannerBody>
    <BannerFooter>
      <div
        style={{
          flexGrow: 1,
        }}
      >
        <Checkbox aria-label="Send me notifications" label="Send me notifications" />
      </div>
      <div>
        <Button variant="secondary">OK</Button>
      </div>
    </BannerFooter>
  </Banner>
);

/**
 *
 * The banner takes the complete available width. You can define its borders by putting it into the container:
 *
 */

export const Sizing = (): ReactNode => (
  <div
    style={{
      width: 368,
    }}
  >
    <Banner>
      <BannerHeader variant="system" headerIcon={info} onClose={() => true}>
        Regular example
      </BannerHeader>
      <BannerBody>
        Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind
        texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.
      </BannerBody>
      <BannerFooter>
        <div
          style={{
            flexGrow: 1,
          }}
        >
          <Checkbox id="anatomy-1" aria-label="Send me notifications" label="Send me notifications" />
        </div>
        <div>
          <Button variant="secondary">OK</Button>
        </div>
      </BannerFooter>
    </Banner>
  </div>
);
