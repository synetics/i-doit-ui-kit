import React, { ReactNode } from 'react';
import classNames from 'classnames';
import style from '../Banner.module.scss';
import { BannerBody } from '../BannerBody';
import { Variant } from '../Banner';

type BannerBodyProps = {
  /**
   * Class of the container
   */
  className?: string;
  /**
   * Accent style of the component
   */
  variant?: Variant;
  /**
   * The content of the field
   */
  children?: ReactNode | ReactNode[];
  /**
   * set the close Icon color
   */
  closeIconFill?: string;
  /**
   * Callback for the 'close' action
   */
  onClose?: () => void;
};

export const NotificationBody = ({
  className,
  variant,
  onClose,
  closeIconFill = 'white',
  children,
}: BannerBodyProps) => (
  <BannerBody
    variant={variant}
    className={classNames(style.notificationBody, className)}
    onClose={onClose}
    closeIconFill={closeIconFill}
  >
    {children}
  </BannerBody>
);
