import React, { ReactNode } from 'react';
import classNames from 'classnames';
import { close } from '../../../icons';
import { Icon } from '../../Icon';
import style from '../Banner.module.scss';
import { Variant } from '../Banner';

type BannerBodyProps = {
  /**
   * Class of the container
   */
  className?: string;
  /**
   * Accent style of the component
   */
  variant?: Variant;
  /**
   * The content of the field
   */
  children?: ReactNode | ReactNode[];
  /**
   * set the close Icon color
   */
  closeIconFill?: string;
  /**
   * Callback for the 'close' action
   */
  onClose?: () => void;
};

const BannerBody = ({ className, variant, onClose, closeIconFill = 'white', children }: BannerBodyProps) => (
  <div className={classNames(className, style.bannerBody, variant && style[variant])}>
    {children}
    {onClose && (
      <Icon className="closeButton" src={close} onClick={onClose} wrapper="span" width={20} fill={closeIconFill} />
    )}
  </div>
);
export { BannerBody, BannerBodyProps };
