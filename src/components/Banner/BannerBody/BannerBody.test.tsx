import { render, screen } from '@testing-library/react';
import React from 'react';
import { Variant } from '../Banner';
import { BannerBody } from './BannerBody';

describe('BannerBody', () => {
  it('renders without crashing', () => {
    render(<BannerBody variant="system">Example</BannerBody>);

    expect(screen.getByText('Example')).toBeInTheDocument();
  });

  it('to have a className', () => {
    render(
      <BannerBody className="Example" variant="system">
        Example
      </BannerBody>,
    );

    expect(screen.getByText('Example')).toHaveClass('Example');
  });

  it.each<[Variant]>([['system'], ['error'], ['success'], ['warning']])('renders %s variant', (variant) => {
    render(
      <BannerBody className="Example" variant={variant}>
        Example
      </BannerBody>,
    );

    expect(screen.getByText('Example')).toHaveClass(variant);
  });

  it('renders with children as a div', () => {
    render(
      <BannerBody variant="system">
        <div>Example</div>
      </BannerBody>,
    );

    expect(screen.getByText('Example')).toBeInTheDocument();
  });

  it('renders with Close Icon', () => {
    render(
      <BannerBody variant="system" onClose={() => true}>
        Example
      </BannerBody>,
    );

    expect(screen.getByTestId('icon')).toBeInTheDocument();
  });
});
