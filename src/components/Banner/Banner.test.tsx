import { render, screen } from '@testing-library/react';
import React from 'react';
import { Banner, Variant } from './Banner';
import { NotificationBody } from './NotificationBody';
import { NotificationFooter } from './NotificationFooter';
import { NotificationHeader } from './NotificationHeader';

describe('Banner', () => {
  it('renders without crashing', () => {
    render(<Banner variant="system">Example</Banner>);

    expect(screen.getByText('Example')).toBeInTheDocument();
  });

  it('to have a className', () => {
    render(
      <Banner className="Example" variant="system">
        Example
      </Banner>,
    );

    expect(screen.getByText('Example')).toHaveClass('Example');
  });

  it('to have  withShadow as a className', () => {
    render(<Banner variant="system">Example</Banner>);

    expect(screen.getByText('Example')).toHaveClass('withShadow');
  });

  it('to dont have  withShadow as a className', () => {
    render(
      <Banner variant="system" withShadow={false}>
        Example
      </Banner>,
    );

    expect(screen.getByText('Example')).not.toHaveClass('withShadow');
  });

  it.each<[Variant]>([['system'], ['error'], ['success'], ['warning']])('renders %s variant', (variant) => {
    render(
      <Banner className="Example" variant={variant}>
        Example
      </Banner>,
    );

    expect(screen.getByText('Example')).toHaveClass(variant);
  });

  it('renders with children as a div', () => {
    render(
      <Banner className="Example" variant="system">
        <div>Example</div>
      </Banner>,
    );

    expect(screen.getByText('Example')).toBeInTheDocument();
  });

  it('renders NotificationBody without crashing', () => {
    render(
      <NotificationBody onClose={() => {}}>
        <div>Example</div>
      </NotificationBody>,
    );
    expect(screen.getByText('Example')).toBeInTheDocument();
  });

  it('renders NotificationFooter without crashing', () => {
    render(
      <NotificationFooter>
        <div>Example</div>
      </NotificationFooter>,
    );

    expect(screen.getByText('Example')).toBeInTheDocument();
  });
  it('renders NotificationHeader without crashing', () => {
    render(
      <NotificationHeader>
        <div>Example</div>
      </NotificationHeader>,
    );

    expect(screen.getByText('Example')).toBeInTheDocument();
  });
});
