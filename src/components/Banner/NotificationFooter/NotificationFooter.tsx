import React, { ReactNode } from 'react';
import classNames from 'classnames';
import style from '../Banner.module.scss';
import { BannerFooter } from '../BannerFooter';
import { Variant } from '../Banner';

type BannerFooterProps = {
  /**
   * Class of the container
   */
  className?: string;
  /**
   * Accent style of the component
   */
  variant?: Variant;
  /**
   * The content of the field
   */
  children?: ReactNode | ReactNode[];
};

export const NotificationFooter = ({ className, variant, children }: BannerFooterProps) => (
  <BannerFooter className={classNames(style.notificationFooter, className, variant && style[variant])}>
    {children}
  </BannerFooter>
);
