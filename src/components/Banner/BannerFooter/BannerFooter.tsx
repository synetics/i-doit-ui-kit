import React, { ReactNode } from 'react';
import classNames from 'classnames';
import style from '../Banner.module.scss';
import { Variant } from '../Banner';

type BannerFooterProps = {
  /**
   * Class of the container
   */
  className?: string;
  /**
   * Accent style of the component
   */
  variant?: Variant;
  /**
   * The content of the field
   */
  children?: ReactNode | ReactNode[];
};

const BannerFooter = ({ className, variant, children }: BannerFooterProps) => (
  <div className={classNames(className, style.bannerFooter, variant && style[variant])}>{children}</div>
);
export { BannerFooter, BannerFooterProps };
