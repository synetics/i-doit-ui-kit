import { render, screen } from '@testing-library/react';
import React from 'react';
import { BannerFooter } from './BannerFooter';

describe('BannerFooter', () => {
  it('renders without crashing', () => {
    render(<BannerFooter>Test</BannerFooter>);

    expect(screen.getByText('Test')).toBeInTheDocument();
  });

  it('to have a className', () => {
    render(
      <BannerFooter className="Example" variant="system">
        Example
      </BannerFooter>,
    );

    expect(screen.getByText('Example')).toHaveClass('Example');
  });

  it('renders with children as a div', () => {
    render(
      <BannerFooter className="Example" variant="system">
        <div>Example</div>
      </BannerFooter>,
    );

    expect(screen.getByText('Example')).toBeInTheDocument();
  });
});
