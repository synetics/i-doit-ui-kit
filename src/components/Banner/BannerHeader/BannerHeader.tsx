import React, { ReactNode } from 'react';
import classNames from 'classnames';
import { Icon } from '../../Icon';
import { close } from '../../../icons';
import style from '../Banner.module.scss';
import { Variant } from '../Banner';

type BannerHeaderProps = {
  /**
   * Class of the container
   */
  className?: string;
  /**
   * Accent style of the component
   */
  variant?: Variant;
  /**
   * The content of the field
   */
  children?: ReactNode | ReactNode[];
  /**
   * Path to the svg source.
   */
  headerIcon?: string;
  /**
   * set the header Icon color
   */
  headerIconFill?: string;
  /**
   * set the close Icon color
   */
  closeIconFill?: string;
  /**
   * Callback for the 'close' action
   */
  onClose?: () => void;
};

const BannerHeader = ({
  className,
  variant,
  headerIconFill = 'white',
  closeIconFill = 'white',
  headerIcon,
  onClose,
  children,
}: BannerHeaderProps) => (
  <div data-testid="BannerHeader" className={classNames(className, style.bannerHeader, variant && style[variant])}>
    {headerIcon && (
      <Icon className={classNames(style.headerIcon)} src={headerIcon} wrapper="span" width={20} fill={headerIconFill} />
    )}
    <div className={classNames(style.headerString)}> {children} </div>
    {onClose && (
      <Icon className="closeButton" src={close} onClick={onClose} wrapper="span" width={20} fill={closeIconFill} />
    )}
  </div>
);
export { BannerHeader, BannerHeaderProps };
