import { render, screen } from '@testing-library/react';
import React from 'react';
import { info } from '../../../icons';
import { Variant } from '../Banner';
import { BannerHeader } from './BannerHeader';

describe('BannerHeader', () => {
  it('renders without crashing', () => {
    render(<BannerHeader variant="system">Test</BannerHeader>);

    expect(screen.getByText('Test')).toBeInTheDocument();
  });

  it('to have a className', () => {
    render(
      <BannerHeader className="Example" variant="system">
        Example
      </BannerHeader>,
    );

    expect(screen.getByTestId('BannerHeader')).toHaveClass('Example');
  });

  it.each<[Variant]>([['system'], ['error'], ['success'], ['warning']])('renders %s variant', (variant) => {
    render(
      <BannerHeader className="Example" variant={variant}>
        Example
      </BannerHeader>,
    );

    expect(screen.getByTestId('BannerHeader')).toHaveClass(variant);
  });

  it('renders with children as a div', () => {
    render(
      <BannerHeader className="Example" variant="system">
        <div>Example</div>
      </BannerHeader>,
    );

    expect(screen.getByTestId('BannerHeader')).toBeInTheDocument();
  });
  it('renders with close Icon', () => {
    render(
      <BannerHeader variant="system" onClose={() => true}>
        Example
      </BannerHeader>,
    );

    expect(screen.getByTestId('icon')).toBeInTheDocument();
  });

  it('renders with Icons', () => {
    render(
      <BannerHeader variant="system" headerIcon={info}>
        Example
      </BannerHeader>,
    );

    expect(screen.getByTestId('icon')).toBeInTheDocument();
  });
});
