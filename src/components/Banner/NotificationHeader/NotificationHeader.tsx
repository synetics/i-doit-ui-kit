import React, { ReactNode } from 'react';
import classnames from 'classnames';
import { BannerHeader } from '../BannerHeader';
import style from '../Banner.module.scss';
import { Variant } from '../Banner';

type BannerHeaderProps = {
  /**
   * Class of the container
   */
  className?: string;
  /**
   * Accent style of the component
   */
  variant?: Variant;
  /**
   * The content of the field
   */
  children?: ReactNode | ReactNode[];
  /**
   * Path to the svg source.
   */
  headerIcon?: string;
  /**
   * set the header Icon color
   */
  headerIconFill?: string;
  /**
   * set the close Icon color
   */
  closeIconFill?: string;
  /**
   * Callback for the 'close' action
   */
  onClose?: () => void;
};

export const NotificationHeader = ({
  className,
  variant,
  headerIconFill = 'white',
  closeIconFill = 'white',
  headerIcon,
  onClose,
  children,
}: BannerHeaderProps) => (
  <BannerHeader
    className={classnames(style.notificationBanner, className)}
    variant={variant}
    headerIcon={headerIcon}
    closeIconFill={closeIconFill}
    headerIconFill={headerIconFill}
    onClose={onClose}
  >
    {children}
  </BannerHeader>
);
