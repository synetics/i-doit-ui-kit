import { ReactNode } from 'react';
import { Item, Separator } from '../Menu/components';
import { Wrapper } from '../Modal';
import { TagPulldown } from './TagPulldown';

const items = new Array(70).fill(0).map((_, i) => ({
  label: `{Item-${i}}`,
  id: `id${i}`,
}));

export default {
  title: 'Components/Atoms/Tag Pulldown',
  component: TagPulldown,
};

export const TagPulldownRegular = (): ReactNode => (
  <div
    style={{
      minHeight: 600,
    }}
    className="p-6"
  >
    <TagPulldown label="regular">
      {(onClose) => (
        <>
          <Wrapper
            className="p-0 border-0"
            style={{
              maxHeight: 300,
              overflow: 'auto',
              display: 'flex',
              flexDirection: 'column',
            }}
          >
            {items.map(({ label, id }) => (
              <Item
                id={id}
                label={label}
                onSelect={() => {
                  onClose();
                }}
              />
            ))}
          </Wrapper>
        </>
      )}
    </TagPulldown>
  </div>
);

export const TagPulldownDisabled = (): ReactNode => (
  <TagPulldown label="disabled" disabled>
    {(onClose) => (
      <>
        <Item id="settings" label="Tenant setting " onSelect={() => onClose()} />
        <Separator />
        <Wrapper
          className="p-0 border-0"
          style={{
            maxHeight: 300,
            overflow: 'auto',
            display: 'flex',
            flexDirection: 'column',
          }}
        >
          {items.map(({ label, id }) => (
            <Item
              id={id}
              label={label}
              onSelect={() => {
                onClose();
              }}
            />
          ))}
        </Wrapper>
      </>
    )}
  </TagPulldown>
);
