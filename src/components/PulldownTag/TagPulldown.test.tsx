import { render, screen } from '@testing-library/react';
import { TagPulldown } from './TagPulldown';

describe('SubMenu', () => {
  it('renders with content inside without crashing', () => {
    render(<TagPulldown label="Label">{() => <div>Test</div>}</TagPulldown>);
    expect(screen.getByText('Label')).toBeInTheDocument();
  });
});
