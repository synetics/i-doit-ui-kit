import React, { forwardRef, MouseEventHandler, ReactNode } from 'react';
import classnames from 'classnames';
import { arrow_down, arrow_up } from '../../icons';
import { useGlobalizedRef } from '../../hooks';
import { ButtonPulldown } from '../ButtonPulldown';
import { Button } from '../Button';
import { Icon } from '../Icon';
import { OverflowWithTooltip } from '../OverflowWithTooltip';
import classes from './TagPulldown.module.scss';

type TagPulldownItemType = {
  children: (onClose: () => void) => ReactNode;
  label: string;
  className?: string;
  disabled?: boolean;
};

type TriggerProps = {
  activeState?: boolean;
  hovered?: boolean;
  onClick?: MouseEventHandler;
  label?: string | ReactNode;
  className?: string;
  disabled?: boolean;
};

const Trigger = forwardRef<HTMLButtonElement, TriggerProps>(
  ({ activeState, hovered, label, className, onClick, disabled = false, ...props }: TriggerProps, ref) => {
    const { localRef, setRefsCallback } = useGlobalizedRef(ref);
    return (
      <Button
        {...props}
        ref={setRefsCallback}
        iconPosition="right"
        onClick={onClick}
        variant="secondary"
        disabled={disabled}
        icon={
          <Icon height={16} width={16} className={classnames(classes.icon)} src={activeState ? arrow_up : arrow_down} />
        }
        activeState={activeState}
        className={classnames(classes.button, className, { [classes.open]: activeState, [classes.disabled]: disabled })}
      >
        <OverflowWithTooltip
          className={classes.label}
          containerRef={localRef.current || undefined}
          showTooltip={hovered}
        >
          {label}
        </OverflowWithTooltip>
      </Button>
    );
  },
);

export const TagPulldown = ({ children, label, className, disabled, ...props }: TagPulldownItemType) => (
  <ButtonPulldown
    className={className}
    disabled={disabled}
    containerClassName={classes.container}
    component={Trigger}
    label={label}
    {...props}
  >
    {children}
  </ButtonPulldown>
);
