import { useState } from 'react';
import { SingleSelect } from '../Form/Control';
import { Icon } from '../Icon';
import { more_vertical } from '../../icons';
import { Badge } from '../Badge';
import { Headline } from '../Headline';
import { Button, ButtonSpacer } from '../Button';
import { SubLine } from '../List/Item';
import { OverflowWithTooltip } from '../OverflowWithTooltip';
import { IconButton } from '../IconButton';
import { Indicator } from '../Indicator';
import { getRandomIcon } from '../Form/Control/Pulldown/MultiSelect/stories/get-random-icon';
import { FormGroup, Label } from '../Form/Infrastructure';
import { Card, CardProps } from './Card';
import { CardSection } from './CardSection';
import { CardImage } from './CardImage';
import classes from './CardStories.module.scss';

export default {
  title: 'Components/Molecules/Card',
  component: Card,
};

const variants = [
  { value: 'no-border' as const, label: 'No border' },
  { value: 'has-border' as const, label: 'Has border' },
  { value: 'active' as const, label: 'Active' },
  { value: 'recommended' as const, label: 'Recommended' },
];
const shadows = [
  { value: '' as const, label: 'No shadow' },
  { value: 'md' as const, label: 'Medium' },
  { value: 'lg' as const, label: 'Large' },
];

type ConfigProps = {
  shadow: CardProps['shadow'];
  variant: CardProps['variant'];
  onShadowChange: (value: CardProps['shadow']) => void;
  onVariantChange: (value: CardProps['variant']) => void;
};

const Config = ({ shadow, variant, onShadowChange, onVariantChange }: ConfigProps) => (
  <div className="w-50 mb-6">
    <FormGroup className="mb-2">
      <Label id="shadow">Shadow</Label>
      <SingleSelect
        id="shadow"
        onChange={(a) => onShadowChange(!a?.value ? undefined : (a.value as CardProps['shadow']))}
        items={shadows}
        value={shadows.find((a) => a.value === shadow || (!a.value && !shadows)) ?? shadows[0]}
      />
    </FormGroup>
    <FormGroup className="mb-2">
      <Label id="variant">Variant</Label>
      <SingleSelect
        id="variant"
        onChange={(a) => onVariantChange((a?.value ?? 'no-border') as CardProps['variant'])}
        items={variants}
        value={variants.find((a) => a.value === variant) ?? variants[0]}
      />
    </FormGroup>
  </div>
);

/**
 * Card component represents the shopping item with descriptive information.
 *
 * It enables us to structure content in various ways.
 *
 * The following story shows variants of the card in default vertical layout.
 */
export const CardComponent = () => {
  const [shadow, setShadow] = useState<CardProps['shadow']>('md');
  const [variant, setVariant] = useState<CardProps['variant'] | undefined>('has-border');
  const handleClick = () => {
    window.console.log('Card clicked');
  };
  return (
    <>
      <Config shadow={shadow} variant={variant} onShadowChange={setShadow} onVariantChange={setVariant} />
      <div className="d-flex">
        <Card shadow={shadow} variant={variant} className="mb-4 mr-4" onClick={handleClick}>
          <CardImage className="position-relative p-10">
            <Badge border variant="green" className={classes.badge}>
              <Icon width={16} height={16} src={getRandomIcon()} />
              Active
            </Badge>
            <div className={classes.image}>
              <Icon src={getRandomIcon()} />
            </div>
          </CardImage>
          <CardSection>
            <Headline as="h2">Form</Headline>
          </CardSection>
          <CardSection>
            <OverflowWithTooltip className={classes.fiveLines}>
              <span className={classes.fiveLines}>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias aliquam cum dolor id, in laborum
                necessitatibus nobis recusandae similique sunt tempora totam ullam unde? Aperiam eius hic in inventore
                libero? lo
              </span>
            </OverflowWithTooltip>
          </CardSection>
          <CardSection>
            <Badge variant="violet" className="mr-2">
              <Icon width={16} height={16} src={getRandomIcon()} />
              <span className="ml-2">Recommended</span>
            </Badge>
            <Badge>
              <Icon width={16} height={16} src={getRandomIcon()} />
              <span className="ml-2">trial</span>
            </Badge>
          </CardSection>
          <CardSection>
            <ButtonSpacer className="ml-auto">
              <Button variant="secondary">Details</Button>
              <Button>Update</Button>
            </ButtonSpacer>
          </CardSection>
        </Card>
        <Indicator
          className="d-flex"
          xPosition="middle"
          offset
          label={
            <div className="d-flex">
              <Badge border variant="violet" className="mr-2" shadow>
                <Icon width={16} height={16} src={getRandomIcon()} />
                <span className="ml-2">Recommended</span>
              </Badge>
              <Badge border variant="green" shadow>
                <Icon width={16} height={16} src={getRandomIcon()} className="mr-1" />
                Active
              </Badge>
            </div>
          }
        >
          <Card shadow={shadow} variant={variant} className="mb-4" onClick={handleClick}>
            <CardImage className="p-10">
              <div className={classes.image}>
                <Icon src={getRandomIcon()} />
              </div>
            </CardImage>
            <CardSection>
              <Headline as="h2">Form</Headline>
            </CardSection>
            <CardSection>
              <OverflowWithTooltip className={classes.fiveLines}>
                <span className={classes.fiveLines}>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias aliquam cum dolor id, in laborum
                  necessitatibus nobis recusandae similique sunt tempora totam ullam unde? Aperiam eius hic in inventore
                  libero? lo
                </span>
              </OverflowWithTooltip>
            </CardSection>
            <CardSection className="d-flex mt-auto">
              <ButtonSpacer className="ml-auto">
                <Button variant="secondary">Details</Button>
                <Button>Update</Button>
              </ButtonSpacer>
            </CardSection>
          </Card>
        </Indicator>
      </div>
    </>
  );
};

/**
 * It is also possible to structure card in different ways. For example, use horizontal layout.
 *
 * The CardImage, CardSection components help creating the custom layout of the content.
 */
export const HorizontalCard = () => {
  const [shadow, setShadow] = useState<CardProps['shadow']>('md');
  const [variant, setVariant] = useState<CardProps['variant']>('has-border');
  return (
    <>
      <Config shadow={shadow} variant={variant} onShadowChange={setShadow} onVariantChange={setVariant} />
      <Card type="horizontal" shadow={shadow} variant={variant} className="w-50">
        <CardImage className="p-6" variant="full">
          <Icon src={getRandomIcon()} />
        </CardImage>
        <CardSection className="justify-content-between d-flex flex-column" variant="horizontal">
          <Headline as="h2">Add-on: Documents</Headline>
          <Badge variant="violet" className="mr-2">
            <Icon width={16} height={16} src={getRandomIcon()} />
            <span className="ml-2">Recommended</span>
          </Badge>
        </CardSection>
        <CardSection className="ml-auto d-flex flex-column align-items-end" variant="horizontal">
          <Headline as="h4" className={classes.textVertical}>
            250€
          </Headline>
          <SubLine className="p-0 mb-2">1-Year Subscription</SubLine>
          <IconButton variant="text" label="more" icon={<Icon src={more_vertical} />} />
        </CardSection>
      </Card>
    </>
  );
};
