import { render, screen } from '@testing-library/react';
import React from 'react';
import '@testing-library/jest-dom';
import { CardImage } from './CardImage';

describe('CardImage Component', () => {
  it('renders children content', () => {
    render(<CardImage>Test Image Content</CardImage>);
    expect(screen.getByText('Test Image Content')).toBeInTheDocument();
  });

  it('applies variant class when variant is "full"', () => {
    render(<CardImage variant="full">Full Variant Content</CardImage>);
    const cardImage = screen.getByText('Full Variant Content');
    expect(cardImage).toHaveClass('cardImage');
    expect(cardImage).toHaveClass('full');
  });

  it('does not apply any variant class if variant prop is not provided', () => {
    render(<CardImage>Default Variant Content</CardImage>);
    const cardImage = screen.getByText('Default Variant Content');
    expect(cardImage).toHaveClass('cardImage');
    expect(cardImage).not.toHaveClass('full');
    expect(cardImage).not.toHaveClass('horizontal');
  });

  it('applies custom className if provided', () => {
    render(<CardImage className="custom-class">CardImage with custom class</CardImage>);
    const cardImage = screen.getByText('CardImage with custom class');
    expect(cardImage).toHaveClass('cardImage');
    expect(cardImage).toHaveClass('custom-class');
  });
});
