import classnames from 'classnames';
import { Container, ContainerProps } from '../../Container';
import classes from './CardImage.module.scss';

type CardImageProps = ContainerProps & {
  variant?: 'full' | 'rounded';
  className?: string;
};
export const CardImage = ({ children, variant = 'rounded', className, ...rest }: CardImageProps) => (
  <Container {...rest} className={classnames(classes.cardImage, classes[variant], className)}>
    {children}
  </Container>
);
