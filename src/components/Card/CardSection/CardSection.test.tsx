import { render, screen } from '@testing-library/react';
import React from 'react';
import '@testing-library/jest-dom';
import { CardSection } from './CardSection';

describe('CardSection Component', () => {
  it('renders children content', () => {
    render(<CardSection>Test Section Content</CardSection>);
    expect(screen.getByText('Test Section Content')).toBeInTheDocument();
  });

  it('applies the default variant', () => {
    render(<CardSection>test</CardSection>);
    const cardSection = screen.getByText('test');
    expect(cardSection).toHaveClass('vertical');
  });

  it('applies horizontal variant', () => {
    render(<CardSection variant="horizontal">test</CardSection>);
    const cardSection = screen.getByText('test');
    expect(cardSection).toHaveClass('horizontal');
  });

  it('applies custom className if provided', () => {
    render(<CardSection className="custom-class">Section with custom class</CardSection>);
    const cardSection = screen.getByText('Section with custom class');
    expect(cardSection).toHaveClass('custom-class');
  });
});
