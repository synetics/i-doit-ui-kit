import classnames from 'classnames';
import { Container, ContainerProps } from '../../Container';
import classes from './CardSection.module.scss';

type CardSectionProps = ContainerProps & {
  /**
   * The position within the card section
   */
  variant?: 'horizontal' | 'vertical';
};

export const CardSection = ({ children, className, variant = 'vertical', ...props }: CardSectionProps) => (
  <Container {...props} className={classnames(classes.section, classes[variant], className)}>
    {children}
  </Container>
);
