import classnames from 'classnames';
import { Container, ContainerProps } from '../Container';
import { handleKey } from '../../utils/helpers';
import classes from './Card.module.scss';

type BorderTypes = 'no-border' | 'active' | 'has-border' | 'recommended';

export type CardProps = ContainerProps & {
  variant?: BorderTypes;
  className?: string;
  type?: 'horizontal' | 'vertical';
  shadow?: 'md' | 'lg';
};
export const Card = ({
  children,
  variant = 'no-border',
  role = 'button',
  className,
  shadow,
  onClick,
  onKeyDown,
  type = 'vertical',
  ...rest
}: CardProps) => (
  <Container
    {...rest}
    onClick={onClick}
    onKeyDown={onClick && handleKey({ Enter: onClick })}
    tabIndex={onClick ? 0 : undefined}
    role={role}
    className={classnames(classes.card, classes[type], classes[variant || ''], classes[shadow || ''], className)}
  >
    {children}
  </Container>
);
