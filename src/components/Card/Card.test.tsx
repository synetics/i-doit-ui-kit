import { render, screen, fireEvent } from '@testing-library/react';
import React from 'react';
import '@testing-library/jest-dom';
import { Card } from './Card';

describe('Card Component', () => {
  it('renders children content', () => {
    render(<Card>Test Content</Card>);
    expect(screen.getByText('Test Content')).toBeInTheDocument();
  });

  it('applies variant class when variant prop is provided', () => {
    render(<Card variant="active">Content with active variant</Card>);
    const card = screen.getByText('Content with active variant');
    expect(card).toHaveClass('card');
    expect(card).toHaveClass('active');
  });

  it('applies shadow class when shadow prop is provided', () => {
    render(<Card shadow="md">Content with shadow</Card>);
    const card = screen.getByText('Content with shadow');
    expect(card).toHaveClass('md');
  });

  it('defaults to vertical type if no type prop is provided', () => {
    render(<Card>Default Vertical</Card>);
    const card = screen.getByText('Default Vertical');
    expect(card).toHaveClass('vertical');
  });

  it('applies type class when type prop is horizontal', () => {
    render(<Card type="horizontal">Horizontal Card</Card>);
    const card = screen.getByText('Horizontal Card');
    expect(card).toHaveClass('horizontal');
  });

  it('handles click event when onClick prop is provided', () => {
    const handleClick = jest.fn();
    render(<Card onClick={handleClick}>Clickable Card</Card>);
    const card = screen.getByText('Clickable Card');
    fireEvent.click(card);
    expect(handleClick).toHaveBeenCalledTimes(1);
  });

  it('handles Enter keydown event when onClick prop is provided', () => {
    const handleClick = jest.fn();
    render(<Card onClick={handleClick}>Enter Key Clickable</Card>);
    const card = screen.getByText('Enter Key Clickable');
    fireEvent.keyDown(card, { key: 'Enter', code: 'Enter', charCode: 13 });
    expect(handleClick).toHaveBeenCalledTimes(1);
  });

  it('does not handle click if onClick is not provided', () => {
    render(<Card>Non-clickable Card</Card>);
    const card = screen.getByText('Non-clickable Card');
    fireEvent.click(card);
  });

  it('applies custom className if provided', () => {
    render(<Card className="custom-class">Card with custom class</Card>);
    const card = screen.getByText('Card with custom class');
    expect(card).toHaveClass('custom-class');
  });
});
