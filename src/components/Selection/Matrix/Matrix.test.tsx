import { render, screen } from '@testing-library/react';
import { Matrix } from './Matrix';

describe('Matrix', () => {
  it('renders without crashing', () => {
    render(
      <Matrix size={32} rows={8}>
        {(index) => <div key={`div${index}`}>{index}</div>}
      </Matrix>,
    );
    expect(screen.getByText('1')).toBeInTheDocument();
  });
});
