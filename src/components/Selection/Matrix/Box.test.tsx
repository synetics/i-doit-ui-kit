import { render, screen } from '@testing-library/react';
import { Box } from './Box';

describe('Box', () => {
  it('renders without crashing', () => {
    const fn = jest.fn();
    render(<Box type={{ after: false }} onSelect={fn} />);
    expect(screen.getByRole('button')).toBeInTheDocument();
  });
});
