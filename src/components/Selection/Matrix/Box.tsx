import React, { ReactNode } from 'react';
import classnames from 'classnames';
import { Button } from '../../Button';
import classes from './Box.module.scss';

export type BoxType = {
  type: { [key: string]: boolean };
  className?: string;
  onSelect: () => void;
  children?: ReactNode;
};

export const Box = ({ type, onSelect, className, children, ...rest }: BoxType) => {
  const typeClasses = Object.keys(type).reduce(
    (result, key) => ({
      ...result,
      [classes[key]]: type[key],
    }),
    {} as Record<string, boolean>,
  );
  return (
    <Button
      {...rest}
      variant="text"
      className={classnames(className, classes.btn, classes.container, typeClasses)}
      onClick={onSelect}
    >
      {children}
    </Button>
  );
};
