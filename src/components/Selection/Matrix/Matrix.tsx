import React, { Fragment, ReactNode } from 'react';

export type MatrixType = {
  size: number;
  rows: number;
  className?: string;
  children: (i: number) => ReactNode;
};

export const Matrix = ({ size, rows, className, children }: MatrixType) => (
  <div className={className}>
    {new Array(rows).fill(0).map((_, i) => (
      <Fragment key={`row-${i.toString()}`}>
        {new Array(size).fill(0).map((__, j) => {
          const index = i * size + j;
          return children(index);
        })}
      </Fragment>
    ))}
  </div>
);
