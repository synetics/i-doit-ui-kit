import React, { forwardRef, InputHTMLAttributes, MouseEvent, SyntheticEvent, useCallback, useRef } from 'react';
import { addDays, addYears, subDays, subYears } from 'date-fns';
import DatePicker, { ReactDatePickerProps } from 'react-datepicker';
import { Options, VirtualElement } from '@popperjs/core';
import {
  ChangeKeyboardEvent,
  checkDateInRange,
  formatDate,
  getFocusedBlock,
  handleKey,
  modifyDate,
  parseDate,
  uniqueId,
} from '../../utils';
import { useControlledState, useEventListener, useFocusOut, useGlobalizedRef } from '../../hooks';
import { date as dateIcon } from '../../icons';
import { KeyboardKeys } from '../../static';
import { Error, FieldExtra, FieldGroup, FieldProps, ValueProps } from '../Form/Infrastructure';
import { Input } from '../Form/Control';
import { Icon } from '../Icon';
import { IconButton } from '../IconButton';
import { Popper, ScrollParentModifier } from '../Popper';
import calendarStyles from './Calendar.module.scss';
import { CalendarHeader } from './CalendarHeader';

const popperOptions = {
  modifiers: [
    {
      name: 'offset',
      options: {
        offset: [0, 4],
      },
    },
    ScrollParentModifier,
  ],
};

type DateType = Date | undefined | null;

type CalendarProps = FieldProps &
  Omit<InputHTMLAttributes<HTMLInputElement>, 'value' | 'onChange' | 'type'> &
  ValueProps<DateType> & {
    disabled?: boolean;
    excludeDates?: Date[];
    format?: string;
    timeFormat?: string;
    maxDate?: Date;
    minDate?: Date;
    errorText?: string;
    /**
     * Reference element for pulldown
     */
    pulldownContainer?: Element | VirtualElement | null;

    /**
     * The custom options to apply for popper of pulldown
     */
    pulldownOptions?: Partial<Options>;

    errorValue?: string;

    setErrorValue?: (value: string | undefined) => void;

    /**
     * Tooltips text for open and close state on button
     */
    openLabel?: string;
    closeLabel?: string;
    /**
     * Show or not time also
     */
    showTime?: boolean;
    /**
     * Label for time
     */
    timeInputLabel?: string;
    /**
     * string user inputted value and change for that
     */
    userValue?: string;
    setUserValue?: (value: string) => void;
    onInputChange?: (value: string | number) => void;
    autoFocus?: boolean;
  };

const Calendar = forwardRef<HTMLInputElement, CalendarProps>(
  (
    {
      id = uniqueId('idoit-calendar'),
      disabled = false,
      format = 'yyyy-MM-dd',
      timeFormat = 'hh:mm aa',
      excludeDates,
      maxDate = addYears(new Date(), 30),
      minDate = subYears(new Date(), 30),
      onChange,
      value,
      errorText = 'Invalid date',
      pulldownContainer,
      className,
      pulldownOptions = popperOptions,
      errorValue,
      setErrorValue,
      openLabel = 'Open calendar',
      closeLabel = 'Close calendar',
      showTime = false,
      timeInputLabel = 'Time',
      userValue = '',
      setUserValue,
      onInputChange,
      autoFocus,
      ...props
    },
    ref,
  ) => {
    const { localRef: referenceElement, setRefsCallback: setReferenceElement } =
      useGlobalizedRef<HTMLInputElement>(ref);
    const wrapperRef = useRef<HTMLDivElement>(null);
    const fullFormat = showTime ? `${format} ${timeFormat}` : format;
    const [date, setDate] = useControlledState<DateType>(value, onChange);
    const [userInput, setUserInput] = useControlledState<string>(
      userValue || formatDate(value, fullFormat),
      setUserValue,
    );
    const [show, setShow] = useControlledState<boolean>(props.active || false);
    const [error, setError] = useControlledState(errorValue, setErrorValue);

    const hidePopper = useCallback(() => setShow(false), [setShow]);

    const hideAndFocus = useCallback(() => {
      hidePopper();
      referenceElement.current?.focus();
    }, [hidePopper, referenceElement]);

    useFocusOut(wrapperRef, hidePopper, {
      adoptiveParentsSelectors: ['[role="listbox"]', '.react-datepicker'],
    });

    useEventListener('keydown', handleKey({ Escape: hideAndFocus }, false), document.body);

    const togglePopper = useCallback(() => setShow(!show), [show, setShow]);

    const changeDate = (event: ChangeKeyboardEvent<HTMLInputElement>) => {
      const eventTarget = event as unknown as React.ChangeEvent<HTMLInputElement>;

      const { selectionStart: start, selectionEnd: end } = eventTarget.target;
      setError(undefined);
      if (end && start && date) {
        const nextDate = modifyDate(
          getFocusedBlock(fullFormat, start, end - start),
          date,
          event.key === KeyboardKeys.ArrowUp ? 1 : -1,
        );
        setDate(nextDate);
      }

      window.requestAnimationFrame(() => {
        eventTarget.target.selectionStart = start;
        eventTarget.target.selectionEnd = end;
      });
    };

    const handleDataCheck = (checkValue: string) => {
      if (!checkValue) {
        setError(undefined);
        return;
      }

      const dateValue = parseDate(checkValue, fullFormat);
      const dateCheck = checkDateInRange(dateValue, subDays(minDate, 1), addDays(maxDate, 1)) || null;
      if (dateCheck) {
        setDate(dateValue);
        setError(undefined);
        return;
      }
      setError(errorText);
    };

    const listeners = {
      [KeyboardKeys.Enter]: () => {
        handleDataCheck(userInput);
      },
      [KeyboardKeys.ArrowUp]: changeDate,
      [KeyboardKeys.ArrowDown]: changeDate,
    };

    const handleInputChange = (inputChangeValue: string | number) => {
      const inputString = inputChangeValue.toString();

      if (onInputChange) {
        onInputChange(inputChangeValue);
      }

      if (!inputString) {
        setUserInput('');
        setDate(null);
        setError(undefined);
        return;
      }

      if (!/^[0-9-/.:PMA ]+$/.exec(inputString)) {
        return;
      }

      if (inputString.length <= fullFormat.length) {
        setUserInput(inputString);
      }

      if (inputString.length === fullFormat.length && onChange) {
        onChange(new Date(inputChangeValue));
        handleDataCheck(inputString);
      }
    };

    const handleDateChange = (newDate: Date, e: SyntheticEvent) => {
      if (e) {
        e.stopPropagation();
        referenceElement.current?.focus();
        setShow(false);
      }
      setError(undefined);
      setDate(newDate);
      setUserInput(formatDate(newDate, fullFormat));
    };

    const calendarHeader: ReactDatePickerProps['renderCustomHeader'] = (headerProps) => (
      <CalendarHeader maxDate={maxDate} minDate={minDate} {...headerProps} />
    );

    return (
      <>
        <FieldGroup
          {...props}
          className={className}
          onBlur={() => handleDataCheck(userInput)}
          variant={error ? 'has-error' : undefined}
          active={show}
        >
          <Input
            id={id}
            autoFocus={autoFocus}
            type="text"
            value={userInput}
            onChange={handleInputChange}
            onKeyDown={handleKey<HTMLInputElement>(listeners, false)}
            ref={setReferenceElement}
            placeholder={fullFormat.toUpperCase()}
            disabled={disabled}
            autoComplete="off"
          />
          <FieldExtra>
            <IconButton
              className="m-0"
              disabled={disabled}
              icon={<Icon src={dateIcon} />}
              onClick={(e: MouseEvent<HTMLButtonElement>) => {
                e.stopPropagation();
                togglePopper();
              }}
              onKeyDown={(e) => {
                e.stopPropagation();
                handleKey({ Enter: togglePopper, Escape: hideAndFocus })(e);
              }}
              activeState={show}
              label={show ? closeLabel : openLabel}
              variant="text"
            />
          </FieldExtra>
        </FieldGroup>
        {error && !setErrorValue && <Error id={id}>{errorText}</Error>}
        {show && (
          <Popper
            open={show}
            dataTestId="calendar-popper"
            options={pulldownOptions}
            referenceElement={pulldownContainer || referenceElement.current}
          >
            <div ref={wrapperRef}>
              <DatePicker
                timeFormat="hh:mm"
                dateFormat={format}
                timeIntervals={15}
                disabled={disabled}
                excludeDates={excludeDates}
                fixedHeight
                inline
                maxDate={maxDate}
                minDate={minDate}
                onChange={handleDateChange}
                popperPlacement="top"
                selected={date}
                shouldCloseOnSelect={false}
                showPopperArrow={false}
                calendarClassName={calendarStyles.datepicker}
                dayClassName={() => calendarStyles.dayBlock}
                weekDayClassName={() => calendarStyles.dayBlock}
                renderCustomHeader={calendarHeader}
                showTimeInput={showTime}
                timeInputLabel={timeInputLabel}
              />
            </div>
          </Popper>
        )}
      </>
    );
  },
);

export { Calendar, CalendarProps, DateType };
