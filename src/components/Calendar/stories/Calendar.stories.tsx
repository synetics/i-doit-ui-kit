import { ReactNode } from 'react';
import { Calendar } from '../Calendar';
import ControlledCalendar from './ControlledCalendar';

export default {
  title: 'Components/Molecules/Form/Control/Calendar',
  component: Calendar,
};

export const CalendarComponent = (): ReactNode => (
  <div
    style={{
      maxWidth: 400,
      minHeight: 500,
    }}
  >
    <div
      style={{
        maxWidth: 400,
        minHeight: 60,
      }}
    >
      <ControlledCalendar />
    </div>
    <p
      style={{
        paddingLeft: 10,
      }}
    >
      With time
    </p>
    <div
      style={{
        maxWidth: 400,
      }}
    >
      <ControlledCalendar showTime />
    </div>
  </div>
);
