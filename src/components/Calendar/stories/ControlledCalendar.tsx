import { FC, useState } from 'react';
import { Calendar, DateType } from '../Calendar';

type ControlledCalendarProps = {
  showTime?: boolean | undefined;
};
const ControlledCalendar: FC<ControlledCalendarProps> = (showTime, props) => {
  const [date, setDate] = useState<DateType>(null);

  const handleDate = (value: DateType) => value && setDate(value);

  return <Calendar value={date} onChange={handleDate} showTime={showTime} {...props} />;
};

export default ControlledCalendar;
