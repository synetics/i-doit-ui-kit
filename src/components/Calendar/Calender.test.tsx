import { fireEvent, render as renderProps, screen } from '@testing-library/react';
import user from '@testing-library/user-event';
import { act } from 'react-dom/test-utils';
import { createRenderer } from '../../../test/utils/renderer';
import { Calendar } from './Calendar';

const render = createRenderer(Calendar, {
  onChange: jest.fn(),
});

describe('<Calendar />', () => {
  it('renders regular state', async () => {
    await act(async () => {
      render();
    });

    expect(screen.getByPlaceholderText('YYYY-MM-DD')).toBeInTheDocument();
    expect(screen.getByRole('button', { name: 'Open calendar' })).toBeInTheDocument();
  });

  it('renders calendar with time', async () => {
    await act(async () => {
      renderProps(<Calendar showTime timeInputLabel="myTime" />);
    });

    expect(screen.getByPlaceholderText('YYYY-MM-DD HH:MM AA')).toBeInTheDocument();
  });

  it('renders toggle popper', async () => {
    await act(async () => {
      render();
    });

    expect(screen.getByPlaceholderText('YYYY-MM-DD')).toBeInTheDocument();
    const button = screen.getByRole('button', { name: 'Open calendar' });
    expect(button).toBeInTheDocument();
    await user.click(button);
    const popper = screen.getAllByTestId('idoit-popper');
    expect(popper[0]).toBeInTheDocument();
    await act(() => user.click(button));
    expect(popper[0]).not.toBeInTheDocument();
  });

  it('closes by pressing esc', async () => {
    render();

    const button = screen.getByRole('button', { name: 'Open calendar' });
    await user.click(button);
    const popper = screen.getByTestId('calendar-popper');
    expect(popper).toBeInTheDocument();
    await act(() => user.keyboard('{escape}'));

    expect(popper).not.toBeInTheDocument();
  });

  it('renders disabled', async () => {
    render({ disabled: true });

    const input = screen.getByPlaceholderText('YYYY-MM-DD');

    act(() => input.focus());
    await user.type(input, '20202010');

    expect(input).toHaveValue('');
    expect(screen.getByRole('button', { name: 'Open calendar' })).toHaveClass('disabled');
  });

  it('selects null after pressing Enter key', async () => {
    const { onChange } = render();
    const input = screen.getByPlaceholderText('YYYY-MM-DD');

    act(() => input.focus());
    await user.type(input, '{enter}');

    expect(onChange).not.toHaveBeenCalled();
  });

  it('closes after date was selected', async () => {
    const { onChange } = render();

    const openButton = screen.getByRole('button', { name: 'Open calendar' });
    await user.click(openButton);

    const datePickerButton = screen.getAllByRole('option', { name: /Choose/ })[0];
    const arrowRight = screen.getByRole('button', { name: 'Switch to next month' });

    await user.click(datePickerButton);

    expect(onChange).toHaveBeenCalled();
    expect(arrowRight).not.toBeInTheDocument();
    expect(screen.getByPlaceholderText('YYYY-MM-DD')).toHaveFocus();
  });

  it('allows to type a date', async () => {
    const callFunc = jest.fn();
    render({ onChange: callFunc });

    const input = screen.getByPlaceholderText('YYYY-MM-DD');

    act(() => {
      input.focus();
    });
    await user.type(input, '2020-12-10');

    expect(callFunc).toHaveBeenCalled();
    expect(input).toHaveValue('2020-12-10');

    await user.type(input, 'www');
    expect(input).toHaveValue('2020-12-10');
  });

  it('allows to change the date using ArrowUp and ArrowDown keys', async () => {
    const { onChange } = render({
      value: new Date('2020-11-10'),
      maxDate: new Date(),
    });

    const input = screen.getByDisplayValue('2020-11-10');

    act(() => input.focus());

    await user.type(input, '{arrowup}');

    expect(onChange).toHaveBeenCalledWith(expect.any(Date));

    await user.type(input, '{arrowdown}');

    expect(onChange).toHaveBeenCalledWith(expect.any(Date));
  });

  it('does not allow to select excluded dates', async () => {
    render({
      value: new Date('2020-11-10'),
      maxDate: new Date('2030-01-01'),
      excludeDates: [new Date('2020-11-11')],
    });

    const input = screen.getByPlaceholderText('YYYY-MM-DD');

    await act(async () => user.click(screen.getByRole('button', { name: 'Open calendar' })));
    await act(async () =>
      user.click(screen.getByRole('option', { name: 'Not available Wednesday, November 11th, 2020' })),
    );
    expect(input).not.toHaveValue('2020-11-11');
  });

  it('renders error state when a date is not valid', async () => {
    render();

    const input = screen.getByPlaceholderText('YYYY-MM-DD');
    await act(async () => input.focus());

    await act(async () => user.type(input, '20202010'));

    await act(async () => user.click(document.body));

    expect(input).toHaveValue('20202010');
    expect(screen.getByText('Invalid date')).toBeInTheDocument();

    await act(async () => user.clear(input));
    expect(screen.queryByText('Invalid date')).not.toBeInTheDocument();
  });

  it('allows input empty value', async () => {
    render({ value: new Date('2020-11-10') });

    const input = screen.getByPlaceholderText('YYYY-MM-DD');

    await act(async () => user.clear(input));
    await act(async () => input.focus());

    expect(input).toHaveValue('');
    expect(screen.queryByText('validation.invalid-date')).not.toBeInTheDocument();
  });

  it('selects next and prev months when clicked', async () => {
    render({ value: new Date('2020-11-10') });

    const openButton = screen.getByRole('button', { name: 'Open calendar' });
    await act(async () => user.click(openButton));

    const arrowRight = screen.getByRole('button', { name: 'Switch to next month' });
    await act(async () => user.click(arrowRight));

    expect(screen.getAllByText('December')[0]).toBeInTheDocument();

    const arrowLeft = screen.getByRole('button', { name: 'Switch to previous month' });
    await act(async () => user.click(arrowLeft));

    expect(screen.getAllByText('November')[0]).toBeInTheDocument();
  });

  it('selects year with pulldown', async () => {
    render({ value: new Date('2020-11-10') });

    const openButton = screen.getByRole('button', { name: 'Open calendar' });
    await act(async () => user.click(openButton));

    const year = screen.getAllByTestId('pulldown-trigger')[0];
    await act(async () => user.click(year));

    const selectYear = screen.getByText('2019');
    await act(async () => user.click(selectYear));

    screen.getByRole('option', { name: 'Choose Sunday, October 27th, 2019' });
  });

  it('selects month with pulldown', async () => {
    render({ value: new Date('2020-11-10') });

    const openButton = screen.getByRole('button', { name: 'Open calendar' });
    await act(async () => user.click(openButton));

    const month = screen.getAllByTestId('pulldown-trigger')[1];
    await act(async () => user.click(month));

    const selectMonth = screen.getByText('March');
    await act(async () => user.click(selectMonth));

    screen.getByRole('option', { name: 'Choose Saturday, March 7th, 2020' });
  });

  it('selects date with button', async () => {
    const { onChange } = render({ value: new Date('2020-11-10') });

    const openButton = screen.getByRole('button', { name: 'Open calendar' });
    await act(async () => user.click(openButton));

    const day = screen.getByRole('option', { name: 'Choose Monday, November 30th, 2020' });
    await act(async () => user.click(day));
    expect(onChange).toHaveBeenCalledWith(expect.any(Date));
  });

  it('change month by button', async () => {
    render({ value: new Date('2020-11-10') });

    const openButton = screen.getByRole('button', { name: 'Open calendar' });
    await user.click(openButton);

    const nextMonth = screen.getByRole('button', { name: 'Switch to next month' });
    await user.click(nextMonth);
    await new Promise((resolve) => setTimeout(resolve, 10));
    expect(screen.getAllByText('December')[0]).toBeInTheDocument();
  });

  it('not close when choose date by select', async () => {
    render({ value: new Date('2020-11-10') });
    const openButton = screen.getByRole('button', { name: 'Open calendar' });

    await user.click(openButton);
    const selectMonth = screen.getAllByTestId('pulldown-trigger')[1];
    await user.click(selectMonth);

    const newMonth = screen.getByRole('option', { name: 'October' });
    await user.click(newMonth);

    expect(screen.getAllByText('October')[0]).toBeInTheDocument();
  });

  it('calls onInputChange callback', async () => {
    const callFunc = jest.fn();
    render({ onInputChange: callFunc });

    const input = screen.getByPlaceholderText('YYYY-MM-DD');

    await act(async () => {
      fireEvent.focus(input);
    });

    await user.type(input, '2');

    expect(callFunc).toHaveBeenCalled();
  });
});
