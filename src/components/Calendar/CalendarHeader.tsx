import React, { KeyboardEvent, MouseEvent, useMemo } from 'react';
import classNames from 'classnames';
import { IconButton } from '../IconButton';
import { Icon } from '../Icon';
import { getMonthNames } from '../../utils';
import { arrow_left, arrow_right } from '../../icons';
import { ItemType, SingleSelect } from '../Form/Control';
import { ActiveContext } from '../Form/Infrastructure';
import calendarHeaderStyle from './CalendarHeader.module.scss';

type CalendarHeaderType = {
  date: Date;
  changeYear: (year: number) => void;
  changeMonth: (month: number) => void;
  maxDate: Date;
  minDate: Date;
  decreaseMonth: () => void;
  increaseMonth: () => void;
  prevMonthButtonDisabled: boolean;
  nextMonthButtonDisabled: boolean;
  className?: string;
  nextMonthLabel?: string;
  prevMonthLabel?: string;
};

const CalendarHeader = ({
  date,
  changeYear,
  changeMonth,
  decreaseMonth,
  increaseMonth,
  maxDate,
  minDate,
  prevMonthButtonDisabled,
  nextMonthButtonDisabled,
  className,
  nextMonthLabel = 'Switch to next month',
  prevMonthLabel = 'Switch to previous month',
}: CalendarHeaderType) => {
  const minDateYear = minDate.getFullYear();
  const yearSpan = 1 + maxDate.getFullYear() - minDateYear;
  const yearOptions = useMemo(
    () =>
      Array.from({ length: yearSpan }, (_, i) => ({
        value: `${minDateYear + i}`,
        label: `${minDateYear + i}`,
      })),
    [minDateYear, yearSpan],
  );

  const monthOptions = useMemo(
    () =>
      getMonthNames().map((name, i) => ({
        value: `${i}`,
        label: name,
      })),
    [],
  );

  const getSelectValue = (value: number | string) => ({
    value: `${value}`,
    label: `${value}`,
  });

  const handleSelectItem = (setter: (value: number) => void) => (item: ItemType | null) => {
    if (item === null) {
      return;
    }

    setter(Number(item.value));
  };

  const leftIconClick = (e: MouseEvent) => {
    e.stopPropagation();
    decreaseMonth();
  };

  const rightIconClick = (e: MouseEvent) => {
    e.stopPropagation();
    increaseMonth();
  };

  return (
    <ActiveContext.Provider value={null}>
      <div
        className={classNames(calendarHeaderStyle.headerContainer, className)}
        onKeyDown={(e: KeyboardEvent) => {
          if (e.key !== 'Escape') e.stopPropagation();
        }}
        role="button"
        tabIndex={-1}
      >
        <IconButton
          icon={<Icon src={arrow_left} />}
          onClick={leftIconClick}
          disabled={prevMonthButtonDisabled}
          variant="text"
          label={prevMonthLabel}
        />
        <SingleSelect
          value={getSelectValue(date.getFullYear())}
          items={yearOptions}
          onChange={handleSelectItem(changeYear)}
          className={classNames(
            calendarHeaderStyle.selectField,
            calendarHeaderStyle.maxWidth100,
            calendarHeaderStyle.mrRight2,
          )}
          options={{ overscan: 100 }}
          initial
        />
        <SingleSelect
          value={monthOptions[date.getMonth()]}
          items={monthOptions}
          onChange={handleSelectItem(changeMonth)}
          className={classNames(calendarHeaderStyle.selectField, calendarHeaderStyle.mrLeft2)}
          options={{ overscan: 10 }}
          initial
        />
        <IconButton
          icon={<Icon src={arrow_right} />}
          onClick={rightIconClick}
          disabled={nextMonthButtonDisabled}
          variant="text"
          label={nextMonthLabel}
        />
      </div>
    </ActiveContext.Provider>
  );
};

export { CalendarHeader };
