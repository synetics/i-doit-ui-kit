import { ReactNode } from 'react';
import classnames from 'classnames';
import classes from './Indicator.module.scss';

export type XPosition = 'left' | 'middle' | 'right';
export type YPosition = 'top' | 'center' | 'bottom';

type IndicatorProps = {
  label: ReactNode;
  children: ReactNode;
  className?: string;
  xPosition?: XPosition;
  yPosition?: YPosition;
  offset?: boolean;
};

export const Indicator = ({
  label,
  children,
  xPosition = 'right',
  yPosition = 'top',
  offset,
  className,
}: IndicatorProps) => (
  <div className={classnames(classes.container, className)}>
    {children}
    <div
      className={classnames(classes.indicator, classes[xPosition], classes[yPosition], { [classes.offset]: offset })}
    >
      {label}
    </div>
  </div>
);
