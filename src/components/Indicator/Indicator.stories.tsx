import React, { useState } from 'react';
import { Button } from '../Button';
import { Label } from '../Menu/components/items/Label';
import { Checkbox, Radio } from '../Form/Control';
import { Indicator, XPosition, YPosition } from './Indicator';

export default {
  title: 'Components/Atoms/Indicator',
  component: Indicator,
};

export const IndicatorComponent = () => {
  const [xPosition, setXPosition] = useState<XPosition>('right');
  const [yPosition, setYPosition] = useState<YPosition>('top');
  const [offset, setOffset] = useState(true);

  return (
    <div style={{ width: 400, height: 400 }}>
      <Checkbox
        aria-label="offset"
        checked={offset}
        onChange={() => setOffset(!offset)}
        label={`Offset ${String(offset)}`}
      />
      <div>
        <Label label="Choose x position" className="my-2" />
        <Radio
          name="regular-label"
          label="left"
          aria-label="left"
          onChange={() => setXPosition('left')}
          checked={xPosition === 'left'}
        />
        <Radio
          name="regular-label"
          label="middle"
          aria-label="middle"
          onChange={() => setXPosition('middle')}
          checked={xPosition === 'middle'}
          className="mb-4"
        />
        <Radio
          name="regular-label"
          label="right"
          aria-label="right"
          onChange={() => setXPosition('right')}
          checked={xPosition === 'right'}
        />
      </div>
      <div>
        <Label label="Choose y position" className="my-2" />
        <div className="d-flex flex-column">
          <Radio
            name="y-label"
            label="top"
            aria-label="top"
            onChange={() => setYPosition('top')}
            checked={yPosition === 'top'}
          />
          <Radio
            name="y-label"
            label="center"
            aria-label="center"
            onChange={() => setYPosition('center')}
            checked={yPosition === 'center'}
          />
          <Radio
            name="y-label"
            label="bottom"
            aria-label="bottom"
            onChange={() => setYPosition('bottom')}
            checked={yPosition === 'bottom'}
            className="mb-4"
          />
        </div>
      </div>
      <Indicator
        label={
          <div
            style={{
              backgroundColor: 'red',
              color: 'white',
              width: 20,
              borderRadius: '50%',
              height: 20,
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            1
          </div>
        }
        yPosition={yPosition}
        xPosition={xPosition}
        offset={offset}
      >
        <Button>Button</Button>
      </Indicator>
    </div>
  );
};
