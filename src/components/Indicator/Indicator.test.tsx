import { render, screen } from '@testing-library/react';
import { Indicator, XPosition, YPosition } from './Indicator';
import '@testing-library/jest-dom';

describe('Indicator Component', () => {
  const labelText = 'Test Label';
  const childText = 'Child Element';

  const renderIndicator = (xPosition: XPosition = 'right', yPosition: YPosition = 'top', offset = false) => {
    render(
      <Indicator label={labelText} xPosition={xPosition} yPosition={yPosition} offset={offset}>
        <div>{childText}</div>
      </Indicator>,
    );
  };

  it('renders children correctly', () => {
    renderIndicator();
    expect(screen.getByText(childText)).toBeInTheDocument();
  });

  it('renders label correctly', () => {
    renderIndicator();
    expect(screen.getByText(labelText)).toBeInTheDocument();
  });

  it('applies correct classes for xPosition and yPosition without offset', () => {
    renderIndicator('left', 'center');
    const indicator = screen.getByText(labelText);

    expect(indicator).toHaveClass('left');
    expect(indicator).toHaveClass('center');
    expect(indicator).not.toHaveClass('offset');
  });

  it('applies correct classes for xPosition and yPosition with offset', () => {
    renderIndicator('middle', 'bottom', true);
    const indicator = screen.getByText(labelText);

    expect(indicator).toHaveClass('middle');
    expect(indicator).toHaveClass('bottom');
    expect(indicator).toHaveClass('offset');
  });

  it('defaults to xPosition="right" and yPosition="top"', () => {
    renderIndicator();
    const indicator = screen.getByText(labelText);

    expect(indicator).toHaveClass('right');
    expect(indicator).toHaveClass('top');
  });
});
