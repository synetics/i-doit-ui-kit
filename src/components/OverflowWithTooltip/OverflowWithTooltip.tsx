import React, { ReactNode, useRef, useState } from 'react';
import { Modifier } from 'react-popper';
import { Placement } from '@popperjs/core';
import classNames from 'classnames';
import { Tooltip, TooltipProps } from '../Tooltip';
import styles from './OverflowWithTooltip.module.scss';

type OverflowWithTooltipProps = {
  /**
   * Class name for the contaienr
   */
  className?: string;
  /**
   * Class name for closest children contaienr
   */
  containerClassName?: string;
  /**
   * Content for tooltip
   */
  tooltipContent?: TooltipProps['content'];
  /**
   * Class name for tooltip container
   */
  tooltipClassName?: string;
  /**
   * Preferred placement of the tooltip
   */
  tooltipPlacement?: Placement;
  /**
   * Show tooltip independent from text overflow
   */
  forceTooltip?: boolean;
  /**
   * Class name for popper
   */
  popperClassName?: string;
  /**
   * Class name for text inside
   */
  textClassName?: string;
  /**
   * Show or hide tooltip
   */
  showTooltip?: boolean;
  /**
   * Content for component
   */
  children: ReactNode | string;
  /**
   * If needs to define custom container for tooltip
   */
  containerRef?: Element;
  /**
   * Custom modifier
   */
  modifiers?: Partial<Modifier<'sizeChange' | 'widthChange' | 'sameWidth', Record<string, unknown>>>[];
};

const OverflowWithTooltip = ({
  children,
  className,
  containerClassName,
  tooltipContent,
  tooltipClassName,
  forceTooltip,
  popperClassName,
  textClassName,
  showTooltip,
  containerRef,
  modifiers = [],
  tooltipPlacement = 'bottom',
}: OverflowWithTooltipProps) => {
  const [tooltipActivityStatus, setTooltipActivityStatus] = useState(false);
  const textElementRef = useRef<HTMLSpanElement | null>(null);
  const disableHoverAction = forceTooltip ? !forceTooltip : !tooltipActivityStatus;

  const compareSize = () => {
    if (textElementRef.current) {
      setTooltipActivityStatus(
        textElementRef.current.scrollWidth > textElementRef.current.clientWidth ||
          textElementRef.current.scrollHeight > textElementRef.current.clientHeight,
      );
    }
  };
  const handleRef = (element: HTMLSpanElement | null): void => {
    textElementRef.current = element;
    setTimeout(compareSize, 100);
  };

  return (
    <Tooltip
      content={tooltipContent || (children as React.ReactElement) || ''}
      tooltipClassName={tooltipClassName}
      placement={tooltipPlacement}
      className={classNames(styles.container, className)}
      disableHoverAction={disableHoverAction}
      showOnFocus={!disableHoverAction}
      triggerClassName={classNames(containerClassName, styles.container)}
      popperClassName={popperClassName}
      showTooltip={!disableHoverAction && showTooltip}
      containerRef={containerRef}
      modifiers={modifiers}
    >
      <span onMouseEnter={compareSize} ref={handleRef} className={classNames(styles.overflowContent, textClassName)}>
        {children}
      </span>
    </Tooltip>
  );
};

export { OverflowWithTooltip };
