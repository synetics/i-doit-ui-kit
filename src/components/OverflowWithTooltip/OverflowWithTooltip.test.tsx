import { render, screen } from '@testing-library/react';
import { SameWidthModifier } from '../Popper';
import { OverflowWithTooltip } from './OverflowWithTooltip';

const longText =
  'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.';
it('renders without crashing', async () => {
  const reference = document.createElement('div');
  reference.innerText = 'reference';
  render(
    <div style={{ width: 100 }}>
      <OverflowWithTooltip tooltipPlacement="right" modifiers={[SameWidthModifier]}>
        <span>{longText}</span>
      </OverflowWithTooltip>
    </div>,
  );
  const text = screen.getByText(new RegExp('ipsum'));
  expect(text).toBeInTheDocument();
});
