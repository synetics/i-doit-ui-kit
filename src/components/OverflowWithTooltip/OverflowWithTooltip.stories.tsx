import { ReactNode } from 'react';
import { OverflowWithTooltip } from './OverflowWithTooltip';

export default {
  title: 'Components/Atoms/Overflow with Tooltip',
  component: OverflowWithTooltip,
};

/**
 *
 * The overflow with tooltip component is used to add the tooltip for your content if needed.
 *
 * It adds the overflow styles for your content and shows tooltip if needed
 *
 */
export const AddsTheTooltipIfNeeded = (): ReactNode => (
  <>
    <div>
      <OverflowWithTooltip>
        Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea
        commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel
        illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent
        luptatum zzril delenit augue duis dolore te feugait nulla facilisi.
      </OverflowWithTooltip>
    </div>
    <div style={{ maxWidth: 150 }}>
      <OverflowWithTooltip>Ut wisi eUt wisi eUt wisi e</OverflowWithTooltip>
    </div>
    <div>
      <OverflowWithTooltip>Ut wisi eUt wisi eUt wisi e</OverflowWithTooltip>
    </div>
  </>
);
