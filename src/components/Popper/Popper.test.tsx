import { render, screen } from '@testing-library/react';
import { FC } from 'react';
import { Popper, SameWidthModifier, SizeChangeModifier } from './Popper';

const ComponentMock: FC = () => <div>ComponentMock</div>;

describe('Popper', () => {
  it('renders without crashing', () => {
    render(
      <Popper>
        <ComponentMock />
      </Popper>,
    );

    expect(screen.getByTestId('idoit-popper')).toBeInTheDocument();
  });

  it('renders closed', () => {
    render(
      <Popper>
        <ComponentMock />
      </Popper>,
    );

    expect(screen.getByTestId('idoit-popper')).toBeInTheDocument();
    expect(screen.getByTestId('idoit-popper')).not.toBeVisible();
    expect(screen.getByText('ComponentMock')).toBeInTheDocument();
  });

  it('renders opened', () => {
    render(
      <Popper open>
        <ComponentMock />
      </Popper>,
    );

    expect(screen.getByTestId('idoit-popper')).toBeVisible();
    expect(screen.getByText('ComponentMock')).toBeInTheDocument();
  });

  it('renders at given position', async () => {
    const reference = document.createElement('div');
    reference.innerText = 'reference';

    render(
      <Popper
        open
        options={{
          placement: 'left',
        }}
        referenceElement={reference}
      >
        <ComponentMock />
      </Popper>,
    );

    expect(await screen.findByTestId('idoit-popper')).toHaveAttribute('data-popper-placement', 'left');
  });

  it('makes the popper the same width as the reference', async () => {
    const expectedStyles = { width: '100px' };
    const reference = document.createElement('div');
    reference.getBoundingClientRect = (): DOMRect => ({ width: 100 } as DOMRect);

    render(
      <Popper
        open
        options={{
          modifiers: [SameWidthModifier],
        }}
        referenceElement={reference}
      >
        content
      </Popper>,
    );

    expect(await screen.findByText('content')).toHaveStyle(expectedStyles);
  });

  it('updates position of the popper when the size of the reference is changed', async () => {
    const reference = document.createElement('div');
    reference.getBoundingClientRect = (): DOMRect => ({ width: 100 } as DOMRect);

    render(
      <Popper
        open
        options={{
          modifiers: [SizeChangeModifier],
        }}
        referenceElement={reference}
      >
        content
      </Popper>,
    );

    expect(await screen.findByText('content')).toBeInTheDocument();
  });

  it('renders contents into the given portal', () => {
    const contents = 'contents';
    const portalElement = document.createElement('div');
    portalElement.setAttribute('id', 'portal');
    document.body.append(portalElement);

    render(
      <Popper open portal={portalElement}>
        {contents}
      </Popper>,
    );

    const contentsInPortal = screen.getByText(contents);
    expect(contentsInPortal).toBeInTheDocument();
    expect(contentsInPortal.closest('#portal')).toBeInTheDocument();
  });
});
