import { Placement } from '@popperjs/core';
import { ReactNode, useState } from 'react';
import { Popper, SameWidthModifier, ScrollParentModifier, SizeChangeModifier } from '../Popper';
import { useCallbackRef } from '../../../hooks';

export default {
  title: 'Components/Atoms/Popper',
  component: Popper,
};
const placements: Placement[] = [
  'top-start',
  'top',
  'top-end',
  'right-start',
  'right',
  'right-end',
  'bottom-start',
  'bottom',
  'bottom-end',
  'left-start',
  'left',
  'left-end',
  'auto',
  'auto-start',
  'auto-end',
];

/**
 *
 * A Popper can be used to display some content on top of another. It's built on top of the 3rd party library
 * (Popper.js) for perfect positioning.
 *
 * The Popper.js library provides a pretty good default configuration that fit to many cases.
 *
 */
export const Regular = (): ReactNode => {
  const [activePlacement, setActivePlacement] = useState<Placement | undefined>('top-start');
  const [referenceElement, setReferenceElement] = useCallbackRef<HTMLButtonElement>();
  return (
    <div>
      <select
        onChange={(event) => {
          setActivePlacement(event.target.value as Placement);
        }}
        value={activePlacement}
      >
        {placements.map((placement: Placement) => (
          <option value={placement} key={placement}>
            {placement}
          </option>
        ))}
      </select>

      <div style={{ margin: '100px 200px' }}>
        <button ref={setReferenceElement} style={{ width: 150, height: 150, lineHeight: '50px' }}>
          reference
        </button>
        <Popper open options={{ placement: activePlacement }} referenceElement={referenceElement}>
          Your content can be placed here
        </Popper>
      </div>
    </div>
  );
};

/**
 *
 * To sync the reference width with the popper element use `SameWidthModifier`.
 *
 */
export const SameWidth = (): ReactNode => {
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const [referenceElement, setReferenceElement] = useCallbackRef<HTMLButtonElement>();

  const toggle = () => {
    setIsOpen((prevIsOpen) => !prevIsOpen);
  };

  return (
    <div>
      <div style={{ margin: '100px 200px' }}>
        <button ref={setReferenceElement} onClick={toggle} style={{ width: 200 }}>
          reference
        </button>
        <Popper
          open={isOpen}
          referenceElement={referenceElement}
          options={{
            modifiers: [SameWidthModifier],
          }}
          style={{ background: '#ccc', padding: 10 }}
        >
          sequi vitae voluptatibus!
        </Popper>
      </div>
    </div>
  );
};

/**
 *
 * The `SizeChangeModirier` is useful when the reference element changes size.
 *
 * > This modifier is not exhaustive and don't cover every single case. Please consider updating the instance via
 * > instance.update(), a ResizeObserver or requestAnimationFrame loop (if animating) can to solve related issues.
 *
 */
export const SizeChange = (): ReactNode => {
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const [ratio, setRatio] = useState(2);
  const [referenceElement, setReferenceElement] = useCallbackRef<HTMLButtonElement>();

  const toggle = () => {
    setIsOpen((prevIsOpen) => !prevIsOpen);
  };

  const increase = () => {
    setRatio((prevRatio) => prevRatio + 1);
  };

  const reduce = () => {
    setRatio((prevRatio) => prevRatio - 1);
  };
  return (
    <div>
      <button onClick={increase}>increase `reference`</button>
      <button onClick={reduce} disabled={ratio < 3}>
        reduce `reference`
      </button>

      <div style={{ margin: '100px 200px' }}>
        <button ref={setReferenceElement} onClick={toggle} style={{ width: 50 * ratio, height: 25 * ratio }}>
          reference
        </button>
        <Popper
          open={isOpen}
          referenceElement={referenceElement}
          options={{
            modifiers: [SizeChangeModifier],
          }}
        >
          sequi vitae voluptatibus!
        </Popper>
      </div>
    </div>
  );
};

/**
 *
 * By default, the Popper component ignores scroll containers. To enable this options you should use `ScrollParentModifier`.
 *
 */
export const ScrollParent = (): ReactNode => {
  const [referenceElement, setReferenceElement] = useCallbackRef<HTMLButtonElement>();
  return (
    <div style={{ height: 100, overflow: 'auto', width: 300, margin: 100 }}>
      <p style={{ width: 800 }}>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet, atque delectus dolorem eum explicabo facilis
        fuga fugit iure magnam maxime odit perferendis qui sequi velit voluptatibus. Doloribus ipsa iusto nobis? Lorem
        ipsum dolor sit amet, consectetur adipisicing elit. Amet, atque delectus dolorem eum explicabo facilis fuga
        fugit iure magnam maxime odit perferendis qui sequi velit voluptatibus. Doloribus ipsa iusto nobis? Lorem ipsum
        dolor sit amet, consectetur adipisicing elit. Amet, atque delectus dolorem eum explicabo facilis fuga fugit iure
        magnam maxime odit perferendis qui sequi velit voluptatibus. Doloribus ipsa iusto nobis? Lorem ipsum dolor sit
        amet, consectetur adipisicing elit. Amet, atque delectus dolorem eum explicabo facilis fuga fugit iure magnam
        maxime odit perferendis qui sequi velit voluptatibus. Doloribus ipsa iusto nobis?
      </p>
      <div className="text-center">
        <button ref={setReferenceElement}>reference</button>
        <Popper
          open
          referenceElement={referenceElement}
          options={{
            modifiers: [ScrollParentModifier],
          }}
          style={{ background: '#ccc', padding: 10 }}
        >
          voluptatibus!
        </Popper>
      </div>
      <p style={{ width: 500, marginTop: 20 }}>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aut cumque cupiditate, deleniti distinctio dolorum
        enim illo, iusto labore laboriosam minima molestias praesentium, quae quam repellat sapiente sunt tempore ullam.
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aut cumque cupiditate, deleniti distinctio dolorum
        enim illo, iusto labore laboriosam minima molestias praesentium, quae quam repellat sapiente sunt tempore ullam.
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aut cumque cupiditate, deleniti distinctio dolorum
        enim illo, iusto labore laboriosam minima molestias praesentium, quae quam repellat sapiente sunt tempore ullam.
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aut cumque cupiditate, deleniti distinctio dolorum
        enim illo, iusto labore laboriosam minima molestias praesentium, quae quam repellat sapiente sunt tempore ullam.
      </p>
    </div>
  );
};
