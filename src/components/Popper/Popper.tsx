import { CSSProperties, forwardRef, ReactNode, SyntheticEvent, useCallback, useEffect, useState } from 'react';
import { Modifier, usePopper } from 'react-popper';
import { Instance, Options, State, VirtualElement } from '@popperjs/core';
import ResizeObserver from 'resize-observer-polyfill';
import classnames from 'classnames';
import { useCallbackRef, useGlobalizedRef } from '../../hooks';
import { Portal } from '../Portal';
import styles from './Popper.module.scss';

export type ModifiersType = Partial<
  Modifier<
    'sizeChange' | 'widthChange' | 'sameWidth' | 'offset' | 'changeArrowRightAndArrowLeft',
    Record<string, unknown>
  >
>[];

type PopperProps = {
  /**
   * Defines visibility of the content
   */
  open?: boolean;

  /**
   * Contents of the component
   */
  children: ReactNode | ReactNode[];

  /**
   * Custom options to override the ones defined in defaults
   */
  options?: Partial<Options>;

  /**
   * The property is used to set a specific style of the host element using different CSS properties
   */
  style?: CSSProperties;

  /**
   * A DOM node that exists outside the DOM hierarchy of the parent component
   */
  portal?: Element;

  /**
   * The reference element used to position the popper
   */
  referenceElement?: Element | VirtualElement | null;

  /**
   * If we need to change positioning by classes
   */
  className?: string;

  /**
   * For tests if we have more than one popper inside
   */
  dataTestId?: string;

  /**
   * Need stop propagation on keyboard
   */
  keyStopPropagation?: boolean;

  modifiers?: ModifiersType;
};

const Popper = forwardRef<HTMLElement, PopperProps>(
  (
    {
      children,
      open = false,
      options,
      style,
      referenceElement = null,
      portal,
      className,
      dataTestId = 'idoit-popper',
      keyStopPropagation,
      modifiers = [],
    },
    ref,
  ) => {
    const [popperElement, setPopperElement] = useCallbackRef<HTMLDivElement>();
    const { setRefsCallback } = useGlobalizedRef(ref);

    const [position, setPosition] = useState<'absolute' | 'fixed'>('fixed');
    const { styles: css, attributes } = usePopper(referenceElement, popperElement, {
      modifiers: [{ name: 'offset', options: { offset: [0, 0] } }, ...modifiers],
      placement: 'bottom-start',
      ...options,
      strategy: position,
    });
    const stopPropagation = useCallback((e: SyntheticEvent) => e.stopPropagation(), []);

    useEffect(() => {
      if (open) {
        setPosition('absolute');
      } else {
        setPosition('fixed');
      }
    }, [open]);

    const popper = (
      <div
        role="presentation"
        className={classnames(styles.popper, className)}
        ref={(element) => {
          if (element) {
            setPopperElement(element);
          }
          setRefsCallback(element);
        }}
        onClick={stopPropagation}
        onKeyDown={keyStopPropagation ? stopPropagation : () => false}
        data-open={open}
        {...attributes.popper}
        style={{
          ...css.popper,
          ...style,
          position,
          display: open ? 'unset' : 'none',
        }}
        data-testid={dataTestId}
      >
        {children}
      </div>
    );

    return portal ? <Portal portal={portal}>{popper}</Portal> : popper;
  },
);

const SameWidthModifier: Modifier<'sameWidth', Record<string, unknown>> = {
  name: 'sameWidth',
  enabled: true,
  fn: ({ state }: { state: State }): void => {
    // eslint-disable-next-line no-param-reassign
    state.styles.popper.width = `${state.rects.reference.width}px`;
  },
  phase: 'beforeWrite',
  requires: ['computeStyles'],
};

const SameWidthInlineModifier: Modifier<'sameWidthParentControl', Record<string, unknown>> = {
  name: 'sameWidthParentControl',
  enabled: true,
  fn: ({ state }: { state: State }): void => {
    const getParentControl = (state.elements.reference as Element).closest('.control-wrapper');
    /* eslint-disable no-return-assign, no-param-reassign */
    if (getParentControl) {
      state.styles.popper.width = `${getParentControl?.clientWidth}px`;
      state.styles.popper.left = `-10px`;
    } else {
      // eslint-disable-next-line no-param-reassign
      state.styles.popper.width = `${state.rects.reference.width}px`;
    }
  },
  phase: 'beforeWrite',
  requires: ['computeStyles'],
};

const SizeChangeModifier: Modifier<'sizeChange', Record<string, unknown>> = {
  name: 'sizeChange',
  enabled: true,
  phase: 'main',
  fn: () => {},
  effect: ({ state, instance }: { state: State; instance: Instance }): (() => void) => {
    const { reference } = state.elements;
    const resizeObserver = new ResizeObserver(() => {
      instance.update().catch(console.error);
    });

    resizeObserver.observe(reference as Element);

    return () => {
      resizeObserver.unobserve(reference as Element);
    };
  },
};

const SizeChangeParentModifier: Modifier<'sizeChange', Record<string, unknown>> = {
  name: 'sizeChange',
  enabled: true,
  phase: 'main',
  fn: () => {},
  effect: ({ state, instance }: { state: State; instance: Instance }): (() => void) => {
    const { reference } = state.elements;
    const resizeObserver = new ResizeObserver(() => {
      instance.update().catch(console.error);
    });
    if ((reference as HTMLElement).offsetParent) {
      resizeObserver.observe((reference as HTMLElement).offsetParent as Element);
    }

    return () => {
      if ((reference as HTMLElement).offsetParent) {
        resizeObserver.unobserve((reference as HTMLElement).offsetParent as Element);
      }
    };
  },
};

const ScrollParentModifier: Modifier<'flip', { altBoundary: boolean }> = {
  name: 'flip',
  enabled: true,
  options: {
    altBoundary: true,
  },
};

const SameWidthModifierAndSizeChange: Modifier<'sameWidthAndSize', Record<string, unknown>> = {
  name: 'sameWidthAndSize',
  enabled: true,
  effect: ({ state, instance }: { state: State; instance: Instance }): (() => void) => {
    const { reference } = state.elements;

    const resizeObserver = new ResizeObserver(() => {
      instance.update().catch(console.error);
    });

    resizeObserver.observe(reference as Element);

    return () => {
      resizeObserver.unobserve(reference as Element);
    };
  },
  fn: ({ state }: { state: State }): void => {
    // eslint-disable-next-line no-param-reassign
    state.styles.popper.width = `${state.rects.reference.width}px`;
  },
  phase: 'beforeWrite',
  requires: ['computeStyles'],
};

const changeArrowRightAndArrowLeft: Modifier<'changeArrowRightAndArrowLeft', Record<string, unknown>> = {
  name: 'changeArrowRightAndArrowLeft',
  enabled: true,
  effect: ({ state, instance }: { state: State; instance: Instance }): (() => void) => {
    const { reference } = state.elements;
    const keydown = (e: KeyboardEvent) => {
      if (e.key === 'ArrowLeft' || e.key === 'ArrowRight') {
        setTimeout(() => {
          instance.update().catch(console.error);
        }, 0);
      }
    };
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    // eslint-disable-next-line @typescript-eslint/no-unsafe-call
    reference.addEventListener('keydown', keydown);

    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    // eslint-disable-next-line @typescript-eslint/no-unsafe-call, @typescript-eslint/no-unsafe-return
    return () => reference.removeEventListener('keydown', keydown);
  },
  fn: (): void => {},
  phase: 'beforeWrite',
  requires: ['computeStyles'],
};

export {
  Popper,
  PopperProps,
  SizeChangeModifier,
  SizeChangeParentModifier,
  SameWidthModifier,
  SameWidthInlineModifier,
  ScrollParentModifier,
  SameWidthModifierAndSizeChange,
  changeArrowRightAndArrowLeft,
};
