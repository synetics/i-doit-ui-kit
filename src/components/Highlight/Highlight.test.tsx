import { render } from '@testing-library/react';
import React from 'react';
import { Highlight, HighlightType } from './Highlight';

describe('Highlight', () => {
  it('renders correctly with highlight', () => {
    const props: HighlightType = {
      text: 'Some text for test',
      highlight: 'for test',
    };
    const { container } = render(<Highlight {...props} />);
    const highlightedText = container.querySelector('b');

    const whitespace = '\u00a0';
    expect(highlightedText?.textContent).toBe(`for${whitespace}test`);
  });

  it('renders correctly without highlight', () => {
    const props: HighlightType = {
      text: 'Some text for test',
      highlight: '',
    };
    const { container } = render(<Highlight {...props} />);
    expect(container.querySelector('b')).toBeFalsy();
  });
});
