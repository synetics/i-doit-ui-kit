import { ReactNode } from 'react';
import { Highlight } from './Highlight';

export default {
  title: 'Components/Misc/Highlight',
  component: Highlight,
};

/**
 *
 * Highlight allows you to highlight parts of the text.
 *
 */
export const BasicUsage = (): ReactNode => <Highlight text="Some textetaetasasteteta te tet tetet" highlight="te" />;
