import React, { ReactNode, useMemo } from 'react';

export type HighlightType = {
  text: string;
  highlight: string;
};

const whitespace = '\u00a0';

const splitText = (text: string, highlight: string): ReactNode[] => {
  if (highlight.length === 0) {
    return [text];
  }
  const parts = [] as ReactNode[];
  const template = text.replace(/\s/g, whitespace);
  const lowerText = text.toLowerCase();
  const lowerHighlight = highlight.toLowerCase();
  let startIndex = 0;
  do {
    const nextIndex = lowerText.indexOf(lowerHighlight.toLowerCase(), startIndex);
    if (nextIndex < 0) {
      parts.push(
        <span className="m-0" key="last">
          {template.slice(startIndex)}
        </span>,
      );
      break;
    }
    parts.push(
      <span className="m-0" key={`text${nextIndex}`}>
        {template.slice(startIndex, nextIndex)}
      </span>,
    );
    parts.push(
      <b className="m-0" key={`part${nextIndex}`}>
        {template.slice(nextIndex, nextIndex + highlight.length)}
      </b>,
    );
    startIndex = nextIndex + highlight.length;
  } while (startIndex <= text.length);
  return parts;
};

export const Highlight = ({ text, highlight }: HighlightType) => {
  const parts = useMemo(() => splitText(text, highlight), [text, highlight]);
  return <>{parts}</>;
};
