import React, { MouseEvent, ReactNode, useCallback, useRef } from 'react';
import classnames from 'classnames';
import { Options as FocusTrapOptions } from 'focus-trap';
import { Transition } from 'react-transition-group';
import FocusTrap from 'focus-trap-react';
import { useControlledState, useEventListener } from '../../hooks';
import { handleKey } from '../../utils/helpers';
import { Portal } from '../Portal';
import classes from './Drawer.module.scss';

type DrawerProps = {
  /**
   * Callback function for changing the open state
   */
  setOpen?: (value: boolean) => void;
  /**
   * Open state
   */
  open?: boolean;
  /**
   * Drawer size
   */
  size?: 'xs' | 's' | 'm' | 'l';
  /**
   * The portal element
   */
  portal: Element;
  /**
   * Can be Drawer autofocused
   */
  autoFocus?: boolean;
  /**
   * options which can can control focus trap behavior
   */
  focusTrapOptions?: FocusTrapOptions;
  /**
   * Class name
   */
  className?: string;
  /**
   * The content of the Drawer
   */
  children?: ReactNode | ReactNode[];
};

const sizeToClassName = {
  xs: 'col-3',
  s: 'col-4',
  m: 'col-5',
  l: 'col-8',
};

const initialFocus = '[data-testid="drawer-container"]';

const Drawer = ({
  setOpen,
  size = 's',
  children,
  open,
  portal,
  autoFocus = true,
  focusTrapOptions,
  className,
}: DrawerProps) => {
  const [shown, setShown] = useControlledState(!!open, setOpen);
  const hide = useCallback(() => setShown(false), [setShown]);

  const modalRef = useRef(null);
  useEventListener('keydown', handleKey({ Escape: hide }, false), modalRef.current);

  const closeOnBackdropClick = useCallback(
    (e: MouseEvent) => {
      if (e.target === e.currentTarget) {
        hide();
      }
    },
    [hide],
  );
  const content = (
    <Transition in={shown} timeout={150} unmountOnExit>
      {(state) => (
        <FocusTrap active={shown && autoFocus} focusTrapOptions={{ initialFocus, ...focusTrapOptions }}>
          <div
            className={classnames(classes.drawerBackdrop)}
            data-testid="drawer-backdrop"
            onClick={closeOnBackdropClick}
            role="presentation"
          >
            <div
              className={classnames(classes.drawer, classes[`state-${state}`], `${sizeToClassName[size]}`, className)}
              ref={modalRef}
              tabIndex={-1}
              role="dialog"
              data-testid="drawer-container"
            >
              {children}
            </div>
          </div>
        </FocusTrap>
      )}
    </Transition>
  );

  return <Portal portal={portal}>{content}</Portal>;
};

export { Drawer };
