import { ReactNode, useState } from 'react';
import { Footer, Header } from '../Modal';
import { Headline } from '../Headline';
import { Button, ButtonSpacer } from '../Button';
import { Text } from '../Text';
import { Drawer } from './Drawer';

export default {
  title: 'Components/Molecules/Drawer',
  component: Drawer,
};

export const Regular = (): ReactNode => {
  const [open, setOpen] = useState<boolean>(false);
  const [size, setSize] = useState<'xs' | 's' | 'm' | 'l'>('m');
  const portalElement = document.getElementById('portal-root')!;
  return (
    <>
      <Button className="m-10" variant="primary" onClick={() => setOpen(!open)}>
        drawer open
      </Button>
      <Text>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur cum debitis dolore dolorem ea, eligendi
        error excepturi fugiat ipsum, libero magnam maxime minus nobis non omnis porro possimus quisquam sit sunt,
        tempore ullam voluptas voluptatem voluptatum. A accusamus architecto aspernatur assumenda, atque blanditiis,
        consequatur, delectus distinctio dolorem earum fugiat hic itaque laborum nam non officiis optio possimus quam
        quibusdam quo ratione reiciendis rerum sapiente similique tempora tempore ullam vitae. Ad, doloribus dolorum hic
        impedit inventore maiores maxime molestiae nesciunt nihil recusandae. Ab beatae cupiditate eum laudantium
        nostrum perferendis quos? At blanditiis consequatur delectus dicta eligendi harum incidunt iusto magnam, minima
        minus natus nemo nulla officiis quae quibusdam repellat sint totam. Ad adipisci assumenda autem dicta ea,
        eveniet fugit inventore ipsam laboriosam maiores, natus, repellat totam ut? Accusamus architecto assumenda at
        consequatur cumque deserunt dignissimos distinctio dolorem dolorum earum eligendi expedita, fugit laborum magni
        nam nulla omnis perspiciatis quisquam repellat sit sunt tenetur voluptate? Adipisci aliquam autem, beatae
        eveniet excepturi exercitationem ipsum iste, necessitatibus officiis quasi repudiandae sint totam ullam. Ab eum,
        recusandae? Dolores nostrum porro ratione repellat vero. A adipisci alias aliquam aliquid aperiam autem cumque
        cupiditate debitis eaque error eum, ex expedita hic ipsam minima nam neque nihil odit optio perspiciatis quaerat
        quam qui quia quibusdam quidem quis quod, ratione recusandae repellendus reprehenderit similique sunt totam
        ullam vel velit vitae voluptatibus! Animi aspernatur consequuntur, delectus dicta distinctio, dolores explicabo
        id in ipsa ipsum iure iusto laboriosam nam nihil perferendis perspiciatis quaerat quidem, quod reprehenderit
        repudiandae sequi sint suscipit tempora totam unde. A alias consequatur consequuntur culpa, debitis dignissimos
        dolorem doloremque est facere id inventore itaque, laudantium modi neque nisi nobis numquam obcaecati officiis
        placeat quae quia quisquam reiciendis sed sequi soluta totam veniam, voluptatum? Ad alias est id placeat qui
        quod velit! Aspernatur, at beatae dolor esse fugiat id illo ipsa magnam molestiae obcaecati quasi quisquam quo
        quod recusandae sint sit tempore vel veniam veritatis, voluptatum? Accusantium ad, adipisci asperiores beatae
        commodi, eligendi et ex explicabo ipsam iste labore mollitia natus pariatur perferendis quas rerum sapiente
        sequi sint unde voluptatem! A accusamus adipisci aperiam aspernatur commodi consequuntur cum cumque delectus
        dicta distinctio doloribus earum eius error est facere incidunt, ipsam laborum laudantium magni natus nemo
        nostrum nulla omnis pariatur perspiciatis placeat provident quas quasi quos repellat repellendus reprehenderit
        repudiandae saepe sit sunt ut veritatis? Ad adipisci alias at beatae consequuntur deleniti dicta distinctio
        dolore eos eveniet illo laborum laudantium, minus natus necessitatibus nemo nihil provident qui, quis similique
        sint sunt unde voluptatum. Ab ad adipisci, alias aspernatur assumenda cumque cupiditate debitis distinctio
        doloremque dolorum ducimus error esse illum in magni, maxime natus nemo nesciunt nostrum numquam quas reiciendis
        repudiandae sunt suscipit totam veritatis voluptate. Accusantium ad dolorum ducimus laborum nemo nesciunt quae
        quis tenetur veniam. Aperiam delectus, dicta dolor perferendis quod rerum! Ab architecto assumenda aut autem
        consectetur cumque debitis eos eum exercitationem expedita labore laudantium magnam maiores minus modi nemo
        neque, non nostrum officia perferendis placeat quisquam quod ratione rem reprehenderit rerum sit tempora tempore
        veniam voluptatibus! Assumenda eius error maxime quasi sint! Accusantium aut hic iure laboriosam modi, quam
        repellendus saepe sequi totam voluptate. A aspernatur assumenda deleniti dignissimos error esse et expedita
        illum incidunt maxime minima nesciunt nisi optio, perferendis quae qui quod tempore temporibus. Eius eos fugiat
        incidunt nam nobis placeat praesentium ratione vel veniam vero? Corporis eius fuga maxime porro quia? Architecto
        aspernatur consectetur deleniti est exercitationem facere fugiat illo inventore ipsam iste laborum libero magni
        nam non provident qui quidem quis, quod, recusandae saepe tempore tenetur veritatis. Aliquid dolores doloribus,
        eius illo inventore magni, minima minus mollitia nisi obcaecati officiis quas quisquam rem.
      </Text>
      <Drawer className="w-100" open={open} setOpen={setOpen} size={size} portal={portalElement}>
        <Header onClose={() => setOpen(false)}>Small drawer</Header>
        <Headline as="h3">Choose size:</Headline>
        <ButtonSpacer className="pl-6 pr-6">
          <Button variant="secondary" activeState={size === 'xs'} onClick={() => setSize('xs')}>
            XS
          </Button>
          <Button variant="secondary" activeState={size === 's'} onClick={() => setSize('s')}>
            S
          </Button>
          <Button variant="secondary" activeState={size === 'm'} onClick={() => setSize('m')}>
            M
          </Button>
          <Button variant="secondary" activeState={size === 'l'} onClick={() => setSize('l')}>
            L
          </Button>
        </ButtonSpacer>
        <div className="p-6">
          <Headline className="pb-2" as="h2">
            Some headline:
          </Headline>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum eveniet exercitationem nesciunt odit
          perspiciatis porro, quae quae unde voluptate? Animi asperiores impedit necessitatibus nemo obcaecati possimus
          quisquam reiciendis rem ullam veniam.
        </div>
        <Footer className="mt-auto ">
          <Button variant="text" onClick={() => setOpen(false)}>
            Cancel
          </Button>
        </Footer>
      </Drawer>
    </>
  );
};
