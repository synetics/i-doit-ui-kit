import { render, screen } from '@testing-library/react';
import { TransitionProps } from 'react-transition-group/Transition';
import React, { ReactElement } from 'react';
import user from '@testing-library/user-event';
import { Drawer } from './Drawer';

type MyTransition = TransitionProps & {
  in: boolean;
  children: () => ReactElement;
};

jest.mock('react-transition-group', () => ({
  Transition: ({ in: opened, children }: MyTransition) => (opened ? children('entered') : null),
}));

const options = { fallbackFocus: () => document.body };

describe('Drawer', () => {
  const portalElement = document.createElement('div');
  portalElement.setAttribute('id', 'portal');
  document.body.append(portalElement);

  it('renders without crashing', () => {
    render(
      <Drawer open portal={portalElement} focusTrapOptions={options}>
        Hello
      </Drawer>,
    );

    expect(screen.getByTestId('drawer-backdrop')).toBeInTheDocument();
    expect(screen.getByTestId('drawer-container')).toBeInTheDocument();
    expect(screen.getByText('Hello')).toBeInTheDocument();
  });

  it('close on backdrop click', async () => {
    render(
      <Drawer open portal={portalElement} focusTrapOptions={options}>
        Hello
      </Drawer>,
    );
    const drawer = screen.getByTestId('drawer-container');
    await user.click(screen.getByTestId('drawer-backdrop'));
    expect(drawer).not.toBeInTheDocument();
  });
});
