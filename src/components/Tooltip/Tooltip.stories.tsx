import { lorem } from 'faker';
import { ReactNode, useState } from 'react';
import { open_tab } from '../../icons';
import { Icon } from '../Icon';
import { Checkbox } from '../Form/Control';
import { Button } from '../Button';
import { Tooltip } from './Tooltip';

export default {
  title: 'Components/Atoms/Tooltip',
  component: Tooltip,
};

/**
 *
 * Tooltips display informative text when users hover over, focus on, or tap an element.
 *
 * The Tooltip component provides more than 12 placement choices.
 *
 */

export const Placement = (): ReactNode => (
  <div className="container">
    <div className="row">
      <div className="col text-right">
        <Tooltip content="Hello world" placement="top-start">
          <Button>Top Start</Button>
        </Tooltip>
      </div>
      <div className="col text-center">
        <Tooltip content="Hello world" placement="top">
          <Button>Top</Button>
        </Tooltip>
      </div>
      <div className="col text-left">
        <Tooltip content="Hello world" placement="top-end">
          <Button>Top End</Button>
        </Tooltip>
      </div>
    </div>
    <br />
    <div className="row">
      <div className="col text-center">
        <Tooltip content="Hello world" placement="left-start">
          <Button>Left Start</Button>
        </Tooltip>
      </div>
      <div className="col"> </div>
      <div className="col text-center">
        <Tooltip content="Hello world" placement="right-start">
          <Button>Right Start</Button>
        </Tooltip>
      </div>
    </div>
    <br />
    <div className="row">
      <div className="col text-center">
        <Tooltip content="Hello world" placement="left">
          <Button>Left</Button>
        </Tooltip>
      </div>
      <div className="col"> </div>
      <div className="col text-center">
        <Tooltip content="Hello world" placement="right">
          <Button>Right</Button>
        </Tooltip>
      </div>
    </div>
    <br />
    <div className="row">
      <div className="col text-center">
        <Tooltip content="Hello world" placement="left-end">
          <Button>Left End</Button>
        </Tooltip>
      </div>
      <div className="col"> </div>
      <div className="col text-center">
        <Tooltip content="Hello world" placement="right-end">
          <Button>Right End</Button>
        </Tooltip>
      </div>
    </div>
    <br />
    <div className="row">
      <div className="col text-right">
        <Tooltip content="Hello world" placement="bottom-start">
          <Button>Bottom Start</Button>
        </Tooltip>
      </div>
      <div className="col text-center">
        <Tooltip content="Hello world" placement="bottom">
          <Button>Bottom</Button>
        </Tooltip>
      </div>
      <div className="col text-left">
        <Tooltip content="Hello world" placement="bottom-end">
          <Button>Bottom End</Button>
        </Tooltip>
      </div>
    </div>
  </div>
);

/**
 *
 * The tooltip component can also be placed inside a scroll container. In the case the placement depends on the dimensions of that container.
 *
 */
export const ScrollContainer = (): ReactNode => (
  <div style={{ height: 100, overflow: 'auto', width: 300, margin: 100, background: '#ccc' }}>
    <p style={{ width: 800 }}>{lorem.sentences(20)}</p>
    <div className="text-center">
      <Tooltip content={<div>hello</div>} placement="top">
        <Button>Button</Button>
      </Tooltip>
    </div>
    <p style={{ width: 500, marginTop: 20 }}>{lorem.sentences(20)}</p>
  </div>
);

/**
 *
 * The Tooltip component gives you the ability to pass in any content. Bellow we can find frequently use patterns and use cases
 * but feel free to put any content to the component.
 *
 */
export const SimpleContent = (): ReactNode => (
  <div>
    <div>
      <Tooltip content="Content of the tooltip" placement="top">
        <a href="https://google.de" rel="noopener noreferrer">
          Link with tooltip
        </a>
      </Tooltip>
    </div>
    <hr />
    <div>
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. A consequatur recusandae sapiente voluptates? Aperiam
      assumenda, consequuntur esse eveniet, harum,&nbsp;
      <Tooltip content="Content of the tooltip" placement="top">
        <a href="https://google.de" rel="noopener noreferrer">
          Link with tooltip
        </a>
      </Tooltip>
      &nbsp; iusto laboriosam odio quis quos recusandae similique ullam! Cumque, fugit, maiores.
    </div>
  </div>
);

export const Link = (): ReactNode => (
  <div>
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolor &nbsp;
    <Tooltip
      content={
        <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
          <Icon src={open_tab} fill="white" width={16} />
          <a
            href="tel:+345981233221"
            target="_blank"
            rel="noopener noreferrer"
            style={{ marginLeft: 5, color: '#fff' }}
          >
            Make a call
          </a>
        </div>
      }
    >
      <strong>+345 (98) 123 32 21</strong>
    </Tooltip>
    &nbsp;Lorem ipsum dolor sit amet, consectetur adipisicing elit. A ab ad adipisci amet consequatur debitis?
  </div>
);

export const ButtonTooltip = (): ReactNode => (
  <Tooltip
    content={
      <div>
        {/* eslint-disable-next-line no-console */}
        <Button onClick={() => console.log('Button has been clicked')}>Click me</Button>
      </div>
    }
  >
    <Button>Action in tooltip</Button>
  </Tooltip>
);

export const LongContent = () => (
  <Tooltip
    content="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad culpa deserunt error exercitationem, illo iusto
          maxime numquam quo repellat reprehenderit sapiente ullam, unde. Consectetur, delectus est ex exercitationem
          porro sit."
  >
    <Button>Button</Button>
  </Tooltip>
);

export const Portal = (): ReactNode => (
  <div style={{ width: 70, height: 40, overflow: 'hidden' }}>
    <Tooltip
      portal={document.getElementById('portal-root')!}
      content={<div>This content has been rendered in the give portal</div>}
    >
      <Button>Button</Button>
    </Tooltip>
  </div>
);

export const ProgrammaticallyShowTooltip = (): ReactNode => {
  const [show, setShow] = useState(true);

  return (
    <div>
      <Tooltip
        showTooltip={show}
        content="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad culpa deserunt error exercitationem, illo iusto
        maxime numquam quo repellat reprehenderit sapiente ullam, unde. Consectetur, delectus est ex exercitationem
        porro sit."
      >
        Tooltip trigger
      </Tooltip>
      <div>
        <Checkbox checked={show} onChangeAfter={setShow} label="Show/Hide tooltip" aria-label="Show/Hide tooltip" />
      </div>
    </div>
  );
};
