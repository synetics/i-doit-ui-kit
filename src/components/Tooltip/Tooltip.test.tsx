import { render, screen } from '@testing-library/react';
import user from '@testing-library/user-event';
import { Tooltip } from './Tooltip';

describe('Tooltip', () => {
  it('renders without crashing', () => {
    render(
      <Tooltip content="Hello" placement="bottom">
        trigger
      </Tooltip>,
    );

    expect(screen.getByTestId('idoit-tooltip')).toBeInTheDocument();
  });

  it('shows content when mouse is over the trigger element', async () => {
    render(<Tooltip content="Hello">trigger</Tooltip>);
    const trigger = screen.getByText('trigger');
    expect(screen.queryByRole('tooltip')).not.toBeInTheDocument();

    await user.hover(trigger);
    expect(screen.getByRole('tooltip')).toBeInTheDocument();

    await user.unhover(trigger);
    expect(screen.queryByRole('tooltip')).not.toBeInTheDocument();
  });

  // it('shows content when focus is moved to the trigger element and hides after it has been moved outside', () => {
  //   render(
  //     <Tooltip
  //       content={
  //         <div>
  //           <button>button 1</button>
  //           <button>button 2</button>
  //         </div>
  //       }
  //     >
  //       <button>trigger</button>
  //     </Tooltip>,
  //   );
  //   const trigger = screen.getByText('trigger');
  //   expect(screen.queryByRole('tooltip')).not.toBeInTheDocument();
  //
  //   trigger.focus();
  //   expect(screen.getByRole('tooltip')).toBeInTheDocument();
  //
  //   screen.getByText('button 1').focus();
  //   screen.getByText('button 2').focus();
  //   expect(screen.getByRole('tooltip')).toBeInTheDocument();
  //
  //   trigger.focus();
  //   trigger.blur();
  //   expect(screen.queryByRole('tooltip')).not.toBeInTheDocument();
  // });

  it('renders ReactElement as content', async () => {
    render(
      <Tooltip
        content={
          <div>
            <button>Hello</button>
          </div>
        }
      >
        <button>trigger</button>
      </Tooltip>,
    );
    const trigger = screen.getByText('trigger');

    await user.hover(trigger);

    expect(await screen.findByText('Hello')).toBeInTheDocument();
  });
});
