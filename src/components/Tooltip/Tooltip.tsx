import { FocusEvent, ReactElement, ReactNode, useRef, useState } from 'react';
import { Placement } from '@popperjs/core';
import classnames from 'classnames';
import { useCallbackRef, useEvent } from '../../hooks';
import { FadeTransition } from '../../transitions';
import { ModifiersType, Popper, ScrollParentModifier, SizeChangeParentModifier } from '../Popper';
import styles from './Tooltip.module.scss';

type UseTooltipResult = Readonly<{
  /**
   * Show the content
   */
  show: () => void;

  /**
   * Hide the content
   */
  hide: () => void;

  /**
   * Defines initial state
   */
  isOpen: boolean;

  /**
   * Defines a unique identifier (ID) of the component in the whole document
   */
  tooltipId: string;
}>;

const useTooltip = (): UseTooltipResult => {
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const [tooltipId] = useState<string>(`idoit-tooltip-${Date.now()}`);

  const show = useEvent(() => setIsOpen(true));

  const hide = useEvent(() => setIsOpen(false));

  return {
    show,
    hide,
    isOpen,
    tooltipId,
  } as const;
};

type TooltipProps = {
  /**
   * Trigger element
   */
  children: ReactNode;

  /**
   * Content of the tooltip
   */
  content: ReactElement | string;

  /**
   * Preferred placement of the content
   */
  placement?: Placement;

  /**
   * Custom className of its container
   */
  className?: string;

  /**
   * Custom className of tooltip
   */
  tooltipClassName?: string;

  /**
   *
   */
  portal?: HTMLElement;

  /**
   * Disable onMouseEnter and onMouseLeave actions
   */
  disableHoverAction?: boolean;

  triggerClassName?: string;
  /**
   * If popper needs to be placed over the trigger
   */
  popperClassName?: string;

  /**
   * Show tooltip on focus if we have focusable elem inside
   */
  showOnFocus?: boolean;

  /**
   * Show or hide tooltip
   */
  showTooltip?: boolean;
  /**
   * If needs to define custom container for tooltip
   */
  containerRef?: Element | null;
  /**
   * Custom modifier
   */
  modifiers?: ModifiersType;
};

const Tooltip = ({
  children,
  content,
  placement = 'top',
  portal,
  disableHoverAction = false,
  triggerClassName,
  className,
  tooltipClassName,
  popperClassName,
  showOnFocus = true,
  showTooltip,
  containerRef,
  modifiers = [],
}: TooltipProps) => {
  const portalRef = useRef(portal || document.body);
  const [ref, setRef] = useCallbackRef<HTMLDivElement>();
  const { isOpen, show, hide, tooltipId } = useTooltip();

  const handleBlur = (event: FocusEvent<HTMLDivElement>) => {
    const isChildFocused = event.currentTarget.contains(event.relatedTarget as Node);

    if (!isChildFocused) {
      hide();
    }
  };
  const isShown = isOpen || (typeof showTooltip !== 'undefined' && showTooltip);

  return (
    <div
      onBlur={handleBlur}
      onFocus={showOnFocus ? show : undefined}
      onMouseEnter={disableHoverAction ? undefined : show}
      onMouseLeave={hide}
      className={classnames(styles.tooltip, className)}
      data-testid="idoit-tooltip"
    >
      <div ref={setRef} className={classnames(styles.trigger, triggerClassName)} aria-describedby={tooltipId}>
        {children}
      </div>
      <FadeTransition inProp={isShown}>
        <Popper
          referenceElement={containerRef || ref}
          open={isShown}
          options={{
            placement,
            modifiers: [ScrollParentModifier, SizeChangeParentModifier, ...modifiers],
          }}
          style={{ display: 'block', padding: 4 }}
          className={classnames(styles.popper, popperClassName)}
          portal={portalRef.current}
        >
          <div role="tooltip" id={tooltipId} className={classnames(styles.content, tooltipClassName)}>
            {content}
          </div>
        </Popper>
      </FadeTransition>
    </div>
  );
};

export { Tooltip, TooltipProps, useTooltip, UseTooltipResult };
