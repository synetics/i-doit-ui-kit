import React, { forwardRef, MouseEventHandler, ReactNode } from 'react';
import classnames from 'classnames';
import { chevron_down, chevron_up } from '../../icons';
import { ButtonPulldown } from '../ButtonPulldown';
import { Button } from '../Button';
import { Icon } from '../Icon';
import { OverflowWithTooltip } from '../OverflowWithTooltip';
import { useGlobalizedRef } from '../../hooks';
import classes from './NavigationItem.module.scss';

type NavigationItemType = {
  children: (onClose: () => void) => ReactNode;
  label: string;
  className?: string;
};

type TriggerProps = {
  activeState?: boolean;
  hovered?: boolean;
  onClick?: MouseEventHandler;
  label?: string;
  className?: string;
};

const Trigger = forwardRef<HTMLButtonElement, TriggerProps>(
  ({ activeState, hovered, label, className, onClick, ...props }: TriggerProps, ref) => {
    const { localRef, setRefsCallback } = useGlobalizedRef(ref);
    return (
      <Button
        {...props}
        ref={setRefsCallback}
        iconPosition="right"
        onClick={onClick}
        icon={<Icon className={classnames(classes.icon)} src={activeState ? chevron_up : chevron_down} />}
        activeState={activeState}
        className={classnames(classes.button, className, { [classes.open]: activeState })}
      >
        <OverflowWithTooltip
          className={classes.label}
          containerRef={localRef.current || undefined}
          showTooltip={hovered}
        >
          {label}
        </OverflowWithTooltip>
      </Button>
    );
  },
);

export const NavigationItem = ({ children, label, className, ...props }: NavigationItemType) => (
  <ButtonPulldown
    className={className}
    containerClassName={classes.container}
    component={Trigger}
    label={label}
    {...props}
  >
    {children}
  </ButtonPulldown>
);
