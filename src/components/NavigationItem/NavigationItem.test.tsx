import { render, screen } from '@testing-library/react';
import { NavigationItem } from './NavigationItem';

describe('SubMenu', () => {
  it('renders with content inside without crashing', () => {
    render(<NavigationItem label="Label">{() => <div>Test</div>}</NavigationItem>);
    expect(screen.getByText('Label')).toBeInTheDocument();
  });
});
