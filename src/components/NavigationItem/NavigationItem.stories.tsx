import { useState, ReactNode } from 'react';
import { Separator } from '../List/Item';
import { Wrapper } from '../Modal';
import { Item } from '../Menu/components';
import { NavigationItem } from './NavigationItem';

const items = new Array(70).fill(0).map((_, i) => ({
  label: `{Tenant${i}}`,
  id: `id${i}`,
}));

export default {
  title: 'Components/Organisms/NavigationItem',
  comment: NavigationItem,
};

export const Regular = (): ReactNode => {
  const [menuLabel, setMenuLabel] = useState('Lorem ipsum dolor sit amet, consetetur sadipscing elitr');

  return (
    <div
      style={{
        minHeight: 600,
      }}
    >
      <div
        style={{
          height: 60,
          backgroundColor: '#212121',
        }}
      >
        <NavigationItem label={menuLabel}>
          {(onClose) => (
            <>
              <Item id="settings" label="Tenant setting " onSelect={() => onClose()} />
              <Separator />
              <Wrapper
                className="p-0 border-0"
                style={{
                  maxHeight: 300,
                  overflow: 'auto',
                  display: 'flex',
                  flexDirection: 'column',
                }}
              >
                {items.map(({ label, id }) => (
                  <Item
                    id={id}
                    label={label}
                    onSelect={() => {
                      onClose();
                      setMenuLabel(label);
                    }}
                  />
                ))}
              </Wrapper>
            </>
          )}
        </NavigationItem>
      </div>
    </div>
  );
};
