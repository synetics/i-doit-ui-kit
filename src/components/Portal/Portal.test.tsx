import { render, screen } from '@testing-library/react';
import { Portal } from './Portal';

describe('Portal', () => {
  it('renders contents in the given portal', () => {
    const contents = 'contents';
    const portalElement = document.createElement('div');
    portalElement.setAttribute('id', 'portal');
    document.body.append(portalElement);

    render(<Portal portal={portalElement}>{contents}</Portal>);

    expect(screen.getByText(contents)).toBeInTheDocument();
    expect(screen.getByText(contents)).toHaveAttribute('id', 'portal');
  });
});
