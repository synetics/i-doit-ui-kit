import { ReactNode } from 'react';
import { createPortal } from 'react-dom';

type PortalProps = {
  /**
   * Contents to be rendered into the portal
   */
  children: ReactNode;

  /**
   * A DOM node that exists outside the DOM hierarchy of the parent component
   */
  portal: Element;
};

const Portal = ({ children, portal }: PortalProps) => createPortal(children, portal);

export { Portal, PortalProps };
