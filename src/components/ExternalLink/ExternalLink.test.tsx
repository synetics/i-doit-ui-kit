import { render, screen } from '@testing-library/react';
import { ExternalLink, ExternalLinkVariant } from './ExternalLink';

describe('ExternalLink', () => {
  it('renders without crashing', () => {
    render(<ExternalLink href="https://www.example.com">Test</ExternalLink>);

    expect(screen.getByRole('link')).toBeInTheDocument();
  });

  it('renders given href as it is', () => {
    render(<ExternalLink href="https://www.example.com">Test</ExternalLink>);

    expect(screen.getByRole('link')).toHaveAttribute('href', 'https://www.example.com');
  });

  it('renders given content as it is', () => {
    const content = 'Test';

    render(<ExternalLink href="https://www.example.com">{content}</ExternalLink>);

    expect(screen.getByRole('link', { name: content })).toBeInTheDocument();
  });

  it('always opens in new tab', () => {
    render(<ExternalLink href="https://www.example.com">Test</ExternalLink>);

    expect(screen.getByRole('link')).toHaveAttribute('target', '_blank');
  });

  it('prevents passing the referrer information to the target website by removing the referral info from the HTTP header', () => {
    render(<ExternalLink href="https://www.example.com">Test</ExternalLink>);

    const rel = screen.getByRole('link').getAttribute('rel');
    expect(rel).toContain('noreferrer');
  });

  it('prevents the opening page to gain any kind of access to the original page', () => {
    render(<ExternalLink href="https://www.example.com">Test</ExternalLink>);

    const rel = screen.getByRole('link').getAttribute('rel');
    expect(rel).toContain('noopener');
  });

  it('simulates disabled state', () => {
    render(
      <ExternalLink href="https://www.example.com" disabled>
        Test
      </ExternalLink>,
    );

    const link = screen.getByRole('link');
    expect(link).toHaveAttribute('aria-disabled', 'true');
    expect(link).toHaveAttribute('tabindex', '-1');
    expect(link).toHaveClass('disabled');
  });

  it('adds an extra className', () => {
    const className = 'className';

    render(
      <ExternalLink className={className} href="https://www.example.com">
        Test
      </ExternalLink>,
    );

    expect(screen.getByRole('link')).toHaveClass(className);
  });

  it.each<[ExternalLinkVariant]>([['brand'], ['interaction-press'], ['regular'], ['black'], ['white']])(
    'renders %s variant',
    (variant) => {
      render(
        <ExternalLink variant={variant} href="https://www.example.com">
          Test
        </ExternalLink>,
      );

      expect(screen.getByRole('link')).toHaveClass(variant);
    },
  );
});
