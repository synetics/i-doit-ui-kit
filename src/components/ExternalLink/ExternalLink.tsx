import { HTMLProps, ReactNode } from 'react';
import classnames from 'classnames';
import styles from './ExternalLink.module.scss';

type ExternalLinkVariant = 'brand' | 'interaction-press' | 'regular' | 'black' | 'white';

type ExternalLinkProps = Omit<HTMLProps<HTMLAnchorElement>, 'href'> & {
  disabled?: boolean;
  variant?: ExternalLinkVariant;
  children: ReactNode;
  href: string;
};

const ExternalLink = ({
  disabled,
  variant = 'interaction-press',
  className,
  children,
  href,
  ...anchorProps
}: ExternalLinkProps) => (
  <a
    href={href}
    aria-disabled={disabled}
    rel="noopener noreferrer"
    target="_blank"
    tabIndex={disabled ? -1 : undefined}
    className={classnames(styles[variant], styles.externalLink, { [styles.disabled]: disabled }, className)}
    {...anchorProps}
  >
    {children}
  </a>
);

export { ExternalLink, ExternalLinkProps, ExternalLinkVariant };
