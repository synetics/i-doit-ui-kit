import { lorem } from 'faker';
import { ReactNode } from 'react';
import { ExternalLink } from './ExternalLink';

export default {
  title: 'Components/Atoms/ExternalLink',
  component: ExternalLink,
};

/**
 *
 *  ExternalLink
 *
 * The ExternalLink component allows you to easily customize anchor elements with your theme colors and typography styles.
 *
 */
export const Regular = (): ReactNode => (
  <>
    <ExternalLink href="#">{lorem.words(3)}</ExternalLink>
    <hr />
    <p>
      {lorem.sentences(2)} <ExternalLink href="#"> {lorem.sentences(1)} </ExternalLink>
    </p>
  </>
);

export const Disabled = (): ReactNode => (
  <>
    <ExternalLink href="#" disabled>
      {lorem.words(3)}
    </ExternalLink>
    <hr />
    <p>
      {lorem.sentences(2)}{' '}
      <ExternalLink href="#" disabled>
        {lorem.words(3)}
      </ExternalLink>
    </p>
  </>
);

/**
 *
 * brand | interaction-press | regular | black | white
 *
 */
export const LinkColor = (): ReactNode => (
  <>
    <h1>Brand</h1>
    <p>
      <ExternalLink href="#" variant="brand">
        {lorem.words(3)}
      </ExternalLink>
    </p>
    <hr />
    <h1>Interaction-Press</h1>
    <p>
      <ExternalLink href="#" variant="interaction-press">
        {lorem.words(3)}
      </ExternalLink>
    </p>
    <hr />
    <h1>Regular</h1>
    <p>
      <ExternalLink href="#" variant="regular">
        {lorem.words(3)}
      </ExternalLink>
    </p>
    <hr />
    <h1>Black</h1>
    <p>
      <ExternalLink href="#" variant="black">
        {lorem.words(3)}
      </ExternalLink>
    </p>
  </>
);
