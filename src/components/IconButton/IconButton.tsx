import { cloneElement, forwardRef, MouseEventHandler, ReactElement, ReactNode } from 'react';
import classnames from 'classnames';
import { Button, ButtonProps } from '../Button';
import { Tooltip } from '../Tooltip';
import { ModifiersType } from '../Popper';
import styles from './IconButton.module.scss';

type IconButtonProps = Omit<ButtonProps, 'icon' | 'iconPosition' | 'label'> & {
  /**
   * Defines an accessible name that labels the component.
   */
  label: string | ReactNode;

  /**
   * Element placed inside the component.
   */
  icon: ReactElement;

  /**
   * Specifies the URL or segment of url path of the page the link goes to
   */
  href?: string;

  /**
   * additional className for icon
   */
  iconClassName?: string;

  /**
   * If true label will be showhref in the tooltip
   */
  withTooltip?: boolean;

  /**
   * Defines size of the icon element
   */
  iconSize?: 'small' | 'normal';

  /**
   * Defines placement of the tooltip
   */
  tooltipPlacement?: 'bottom' | 'left' | 'top' | 'right';
  /**
   * Class name for popper
   */
  popperClassName?: string;
  /**
   * Class name for popper
   */
  tooltipClassName?: string;
  /**
   * Custom modifier
   */
  modifiers?: ModifiersType;
};

const IconButton = forwardRef<HTMLButtonElement, IconButtonProps>(
  (
    {
      icon,
      label,
      href,
      disabled,
      loading,
      withTooltip = true,
      className,
      iconSize = 'normal',
      iconClassName,
      tooltipPlacement = 'bottom',
      popperClassName,
      tooltipClassName,
      modifiers,
      ...props
    },
    ref,
  ) => {
    const iconButtonLike = (
      <Button
        className={classnames(styles.iconButton, styles[iconSize], className)}
        disabled={disabled}
        icon={cloneElement(icon, { className: classnames(styles.icon, iconClassName) })}
        loading={loading}
        {...props}
        ref={ref}
        component={
          href
            ? forwardRef(({ children, icon: _icon, className: _className, onMouseEnter, onMouseLeave }) => {
                const isDisabled = disabled || loading;

                return (
                  <div
                    onMouseEnter={onMouseEnter as MouseEventHandler}
                    onMouseLeave={onMouseLeave as MouseEventHandler}
                    className={classnames({ [styles.disabled]: isDisabled, [styles.loading]: loading })}
                  >
                    <a
                      href={href}
                      target="_blank"
                      rel="noreferrer noopener"
                      className={classnames(_className, { [styles.noPointer]: isDisabled })}
                      tabIndex={isDisabled ? -1 : undefined}
                    >
                      {_icon}
                      {children}
                    </a>
                  </div>
                );
              })
            : undefined
        }
      >
        <span className={styles.visuallyHidden}>{label}</span>
      </Button>
    );

    if (disabled || loading || !withTooltip) {
      return iconButtonLike;
    }

    return (
      <Tooltip
        popperClassName={popperClassName}
        className={tooltipClassName}
        placement={tooltipPlacement}
        modifiers={modifiers}
        content={<div>{label}</div>}
      >
        {iconButtonLike}
      </Tooltip>
    );
  },
);

export { IconButton, IconButtonProps };
