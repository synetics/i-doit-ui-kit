import { ReactNode } from 'react';
import { arrow_down } from '../../icons';
import { Icon } from '../Icon';
import { IconButton } from './IconButton';

export default {
  title: 'Components/Atoms/Buttons/IconButton',
  component: IconButton,
};

export const Button = (): ReactNode => (
  <table className="table">
    <tbody>
      <tr>
        <td />
        <td># Primary</td>
        <td># Secondary</td>
        <td># Outline</td>
        <td># Text</td>
        <td># Ghost</td>
      </tr>
      <tr>
        <td>Regular</td>
        <td>
          <IconButton icon={<Icon src={arrow_down} />} label="regular" />
        </td>
        <td>
          <IconButton icon={<Icon src={arrow_down} />} label="regular" variant="secondary" />
        </td>
        <td>
          <IconButton icon={<Icon src={arrow_down} />} label="regular" variant="outline" />
        </td>
        <td>
          <IconButton icon={<Icon src={arrow_down} />} label="regular" variant="text" />
        </td>
        <td
          style={{
            border: '1px solid black',
            backgroundColor: 'black',
            padding: '2px',
          }}
        >
          <IconButton icon={<Icon src={arrow_down} />} label="regular" variant="ghost" />
        </td>
      </tr>
      <tr>
        <td>Disabled</td>
        <td>
          <IconButton icon={<Icon src={arrow_down} />} label="disabled" disabled />
        </td>
        <td>
          <IconButton icon={<Icon src={arrow_down} />} label="disabled" disabled variant="secondary" />
        </td>
        <td>
          <IconButton icon={<Icon src={arrow_down} />} label="disabled" disabled variant="outline" />
        </td>
        <td>
          <IconButton icon={<Icon src={arrow_down} />} label="disabled" disabled variant="text" />
        </td>
        <td
          style={{
            border: '1px solid black',
            backgroundColor: 'black',
            padding: '2px',
          }}
        >
          <IconButton icon={<Icon src={arrow_down} />} label="disabled" disabled variant="ghost" />
        </td>
      </tr>
      <tr>
        <td>Disabled with tooltip</td>
        <td>
          <IconButton
            icon={<Icon src={arrow_down} />}
            label="disabled"
            disabled
            disabledTooltip="Disabled for some reason"
          />
        </td>
        <td>
          <IconButton
            icon={<Icon src={arrow_down} />}
            label="disabled"
            disabled
            disabledTooltip="Disabled for some reason"
            variant="secondary"
          />
        </td>
        <td>
          <IconButton
            icon={<Icon src={arrow_down} />}
            label="disabled"
            disabled
            disabledTooltip="Disabled for some reason"
            variant="outline"
          />
        </td>
        <td>
          <IconButton
            icon={<Icon src={arrow_down} />}
            label="disabled"
            disabled
            disabledTooltip="Disabled for some reason"
            variant="text"
          />
        </td>
        <td
          style={{
            border: '1px solid black',
            backgroundColor: 'black',
            padding: '2px',
          }}
        >
          <IconButton
            icon={<Icon src={arrow_down} />}
            label="disabled"
            disabled
            disabledTooltip="Disabled for some reason"
            variant="ghost"
          />
        </td>
      </tr>
      <tr>
        <td>Loading</td>
        <td>
          <IconButton icon={<Icon src={arrow_down} />} label="disabled" loading />
        </td>
        <td>
          <IconButton icon={<Icon src={arrow_down} />} label="disabled" loading variant="secondary" />
        </td>
        <td>
          <IconButton icon={<Icon src={arrow_down} />} label="disabled" loading variant="outline" />
        </td>
        <td>
          <IconButton icon={<Icon src={arrow_down} />} label="disabled" loading variant="text" />
        </td>
        <td
          style={{
            border: '1px solid black',
            backgroundColor: 'black',
            padding: '2px',
          }}
        >
          <IconButton icon={<Icon src={arrow_down} />} label="disabled" loading variant="ghost" />
        </td>
      </tr>
    </tbody>
  </table>
);

export const Link = (): ReactNode => (
  <table className="table">
    <tbody>
      <tr>
        <td />
        <td># Primary</td>
        <td># Secondary</td>
        <td># Outline</td>
        <td># Text</td>
      </tr>
      <tr>
        <td>Regular</td>
        <td>
          <IconButton href="https://www.google.com" icon={<Icon src={arrow_down} />} label="regular" />
        </td>
        <td>
          <IconButton
            href="https://www.google.com"
            icon={<Icon src={arrow_down} />}
            label="regular"
            variant="secondary"
          />
        </td>
        <td>
          <IconButton
            href="https://www.google.com"
            icon={<Icon src={arrow_down} />}
            label="regular"
            variant="outline"
          />
        </td>
        <td>
          <IconButton href="https://www.google.com" icon={<Icon src={arrow_down} />} label="regular" variant="text" />
        </td>
      </tr>
      <tr>
        <td>Disabled</td>
        <td>
          <IconButton href="https://www.google.com" icon={<Icon src={arrow_down} />} label="disabled" disabled />
        </td>
        <td>
          <IconButton icon={<Icon src={arrow_down} />} label="disabled" disabled variant="secondary" />
        </td>
        <td>
          <IconButton icon={<Icon src={arrow_down} />} label="disabled" disabled variant="outline" />
        </td>
        <td>
          <IconButton icon={<Icon src={arrow_down} />} label="disabled" disabled variant="text" />
        </td>
      </tr>
      <tr>
        <td>Loading</td>
        <td>
          <IconButton href="https://www.google.com" icon={<Icon src={arrow_down} />} label="disabled" loading />
        </td>
        <td>
          <IconButton icon={<Icon src={arrow_down} />} label="disabled" loading variant="secondary" />
        </td>
        <td>
          <IconButton icon={<Icon src={arrow_down} />} label="disabled" loading variant="outline" />
        </td>
        <td>
          <IconButton icon={<Icon src={arrow_down} />} label="disabled" loading variant="text" />
        </td>
      </tr>
    </tbody>
  </table>
);
