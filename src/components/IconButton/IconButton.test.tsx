import { act, fireEvent, render, screen } from '@testing-library/react';
import user from '@testing-library/user-event';
import { palette } from '../../icons';
import { Icon } from '../Icon';
import { IconButton } from './IconButton';

describe('IconButton', () => {
  const icon = <Icon src={palette} />;

  it('renders without crashing', async () => {
    render(<IconButton icon={icon} label="aria label" />);
    const iconButton = screen.getByRole('button', { name: /aria label/ });

    expect(screen.getByTestId('icon')).toBeInTheDocument();

    await user.hover(iconButton);
    expect(await screen.findByRole('tooltip')).toBeInTheDocument();

    await user.unhover(iconButton);
    expect(screen.queryByRole('tooltip')).not.toBeInTheDocument();

    fireEvent.focus(iconButton);
    expect(await screen.findByRole('tooltip')).toBeInTheDocument();
  });

  it('renders in disabled state', async () => {
    render(<IconButton icon={icon} label="aria label" disabled />);
    const iconButton = screen.getByRole('button', { name: /aria label/ });

    await user.hover(iconButton);

    expect(screen.queryByRole('tooltip')).not.toBeInTheDocument();
    expect(iconButton).toBeDisabled();
  });

  it('renders in loading state', async () => {
    render(<IconButton icon={icon} label="aria label" loading />);

    const iconButton = screen.getByRole('button', { name: /aria label/ });

    await user.hover(iconButton);

    expect(iconButton).toBeDisabled();
    expect(screen.getByTestId('idoit-spinner')).toBeInTheDocument();
    expect(screen.queryByRole('tooltip')).not.toBeInTheDocument();
  });

  it('renders as a secure link', async () => {
    render(<IconButton icon={icon} label="aria label" href="link" />);

    const linkButton = screen.getByRole('link', { name: /aria label/ });

    await user.hover(linkButton);
    expect(await screen.findByRole('tooltip')).toBeInTheDocument();

    await user.unhover(linkButton);
    expect(screen.queryByRole('tooltip')).not.toBeInTheDocument();

    fireEvent.focus(linkButton);
    expect(await screen.findByRole('tooltip')).toBeInTheDocument();

    expect(screen.getByTestId('icon')).toBeInTheDocument();
    expect(linkButton).toHaveAttribute('href', 'link');
    expect(linkButton).toHaveAttribute('rel', 'noreferrer noopener');
  });

  it('renders as link in disabled state', async () => {
    render(<IconButton icon={icon} label="aria label" href="link" disabled />);

    const linkButton = screen.getByRole('link', { name: /aria label/ });

    await user.hover(linkButton);
    expect(screen.queryByRole('tooltip')).not.toBeInTheDocument();

    expect(linkButton).toHaveAttribute('tabindex', '-1');
  });

  it('renders as link in loading state', async () => {
    render(<IconButton icon={icon} label="aria label" href="link" loading />);

    const linkButton = screen.getByRole('link', { name: /aria label/ });

    await user.hover(linkButton);
    expect(screen.queryByRole('tooltip')).not.toBeInTheDocument();

    expect(linkButton).toHaveAttribute('tabindex', '-1');
  });

  it('raises onClick event', async () => {
    const onClickMock = jest.fn();
    render(<IconButton icon={icon} label="aria label" onClick={onClickMock} />);

    await act(async () => {
      await user.click(screen.getByRole('button', { name: /aria label/ }));
    });

    expect(onClickMock).toHaveBeenCalledTimes(1);
  });
});
