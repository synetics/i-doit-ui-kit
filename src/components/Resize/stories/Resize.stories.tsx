import { ReactNode, useState } from 'react';
import { Resize } from '../Resize';
import { Checkbox } from '../../Form/Control/Checkbox';
import { Input } from '../../Form/Control/Input';
import { Label } from '../../Form/Infrastructure/Label';
import styles from './Resize.module.scss';

/**
 *
 * Resize used to provide resizing functionality for different items and cotainers
 *
 */

export default {
  title: 'Components/Misc/Resize',
  component: Resize,
};

export const Basic = (): ReactNode => {
  const [enabled, setEnabled] = useState({ right: true, bottom: true });
  const [minWidth, setMinWidth] = useState(400);
  const [maxWidth, setMaxWidth] = useState(800);
  return (
    <>
      <Checkbox
        id="bottom"
        aria-label="bottom"
        label="bottom"
        checked={enabled?.bottom}
        onChange={() => setEnabled({ ...enabled, bottom: !enabled.bottom })}
        onClick={() => setEnabled({ ...enabled, bottom: !enabled.bottom })}
      />
      <Checkbox
        id="right"
        aria-label="right"
        label="right"
        checked={enabled?.right}
        onChange={() => setEnabled({ ...enabled, right: !enabled.right })}
        onClick={() => setEnabled({ ...enabled, right: !enabled.right })}
      />
      <div className={styles.dFlex}>
        <Label id="minWidth">Minimum resize width</Label>
        <Input
          id="minWidthInput"
          className={styles.inputWidth}
          placeholder="minWidth"
          value={minWidth}
          onChange={(v) => setMinWidth(Number(v))}
        />
      </div>
      <div className={styles.dFlex}>
        <Label id="maxWidth">Maximum resize width</Label>
        <Input
          id="maxWidthInput"
          className={styles.inputWidth}
          placeholder="maxWidth"
          value={maxWidth}
          onChange={(v) => setMaxWidth(Number(v))}
        />
      </div>
      <Resize
        defaultSize={{ width: 500, height: 260 }}
        className={styles.resizer}
        enable={enabled}
        minWidth={minWidth}
        maxWidth={maxWidth}
        maxHeight={400}
        minHeight={160}
      >
        There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in
        some form, by injected humour, or randomised words which dont look even slightly believable. If you are going to
        use a passage of Lorem Ipsum, you need to be sure there isnt anything embarrassing hidden in the middle of text.
        All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the
        first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of
        model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is
        therefore always free from repetition, injected humour, or non-characteristic words etc.
      </Resize>
    </>
  );
};
