import { render, screen } from '@testing-library/react';
import user from '@testing-library/user-event';
import { Resize } from './Resize';

describe('Resize', () => {
  it('renders without crashing with given content', () => {
    render(
      <Resize minWidth={100} maxWidth={200} className="my-class">
        content
      </Resize>,
    );

    expect(screen.getByTestId('i-resizable')).toBeInTheDocument();
    expect(screen.getByText('content')).toBeInTheDocument();
    expect(screen.getByTestId('i-resizable')).toHaveClass('my-class');
  });

  it('dont change the size overlimit', async () => {
    render(
      <Resize minWidth={100} maxWidth={200} resizeStep={100} resizeLabel="MyResize">
        <div style={{ width: 100, height: 100 }}>content</div>
      </Resize>,
    );

    const resizeButton = screen.getByRole('button');
    const { offsetWidth } = screen.getByTestId('i-resizable');
    await user.type(resizeButton, '{arrowright}');
    expect(screen.getByTestId('i-resizable').offsetWidth).toBe(offsetWidth);
    await user.type(resizeButton, '{arrowleft}');
    expect(screen.getByTestId('i-resizable').offsetWidth).toBe(offsetWidth);
  });
});
