import React, { useCallback, useRef } from 'react';
import { Resizable, ResizableProps } from 're-resizable';
import classnames from 'classnames';
import { arrow_right } from '../../icons';
import { handleKey } from '../../utils';
import { IconButton } from '../IconButton';
import { changeArrowRightAndArrowLeft } from '../Popper';
import { Icon } from '../Icon';
import styles from './Resize.module.scss';

type ResizeType = ResizableProps & { minWidth: number; maxWidth: number; resizeStep?: number; resizeLabel?: string };
type ResizeHandleType = { changeSize: (increase: boolean) => void; resizeLabel: string };

const RightHandle = ({ resizeLabel, changeSize }: ResizeHandleType) => (
  <div className={styles.rightHandleContainer}>
    <IconButton
      variant="text"
      type="button"
      label={resizeLabel}
      aria-label={resizeLabel}
      tooltipPlacement="right"
      className={styles.resizeButton}
      icon={<Icon src={arrow_right} />}
      contrast
      modifiers={[changeArrowRightAndArrowLeft]}
      onKeyDown={handleKey({
        ArrowRight: () => changeSize(true),
        ArrowLeft: () => changeSize(false),
      })}
    />
  </div>
);

const Resize = ({
  className,
  children,
  minWidth,
  maxWidth,
  resizeLabel = 'Resize',
  resizeStep = 30,
  ...props
}: ResizeType) => {
  const ref = useRef<Resizable>(null);
  const changeSize = useCallback(
    (increase: boolean) => {
      const clientWidth = ref.current?.resizable?.clientWidth;
      if (clientWidth !== undefined) {
        const changedSize = increase ? clientWidth + resizeStep : clientWidth - resizeStep;
        ref.current?.updateSize({ width: changedSize, height: 'auto' });
      }
    },
    [ref, resizeStep],
  );

  return (
    <Resizable
      data-testid="i-resizable"
      className={classnames(styles.container, className)}
      minWidth={minWidth}
      maxWidth={maxWidth}
      ref={ref}
      {...props}
      handleComponent={{
        right: <RightHandle changeSize={changeSize} resizeLabel={resizeLabel} />,
      }}
    >
      {children}
    </Resizable>
  );
};

export { Resize, ResizeType };
