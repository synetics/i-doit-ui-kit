import { CSSProperties, ElementType, forwardRef, ReactNode } from 'react';
import classnames from 'classnames';
import { Container } from '../Container';
import classes from './Content.module.scss';

type ContentProps = {
  className?: string;
  style?: CSSProperties;
};

type ContentType = {
  as?: ElementType<ContentProps>;
  style?: CSSProperties;
  variant?: 'full' | 'content';
  /**
   * The content of the Content
   */
  children?: ReactNode | ReactNode[];
} & ContentProps;

const Content = forwardRef(({ children, variant = 'content', className, style, as }: ContentType, ref) => (
  <Container
    component={as}
    className={classnames(className, classes.container, classes[variant])}
    style={style}
    ref={ref}
  >
    {children}
  </Container>
));

export { Content };
