import { render, screen } from '@testing-library/react';
import React from 'react';
import { Content } from './Content';

describe('Content', () => {
  it('renders without crashing', () => {
    render(<Content>test content</Content>);
    const content = screen.getByText('test content');
    expect(content).toBeInTheDocument();
    expect(content).toHaveClass('content');
  });
  it('custom className', () => {
    render(<Content className="test">test content</Content>);
    expect(screen.getByText('test content')).toHaveClass('test');
  });
});
