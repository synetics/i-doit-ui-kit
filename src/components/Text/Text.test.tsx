import { screen } from '@testing-library/react';
import { createRenderer } from '../../../test/utils/renderer';
import { Tag, Text } from './Text';

const render = createRenderer(Text, { children: 'Test' });

describe('Text', () => {
  it('renders without crashing', () => {
    const { children } = render();

    screen.getByText(children as string);
  });

  it('adds given className', () => {
    const props = { className: 'test-class' };

    const { children } = render(props);

    expect(screen.getByText(children as string)).toHaveClass(props.className);
  });

  it.each<[Tag]>([['span'], ['strong'], ['em'], ['i'], ['cite'], ['p']])('render %s tag', (tag) => {
    const expectedTag = tag.toUpperCase();

    const { children } = render({ tag });
    const actualTag = screen.getByText(children as string).tagName;

    expect(actualTag).toBe(expectedTag);
  });

  it('sets font size and weight', () => {
    const { children } = render({ fontSize: 'small', fontWeight: 'semi-bold' });

    expect(screen.getByText(children as string)).toHaveClass('small', 'semi-bold');
  });
});
