import { createElement, ReactNode } from 'react';
import classNames from 'classnames';
import styles from './Text.module.scss';

type FontSize = 'small' | 'regular' | 'large';

type FontWeight = 'normal' | 'semi-bold';

type Tag = 'span' | 'strong' | 'em' | 'i' | 'cite' | 'p' | 'pre' | 'li' | 'a';

type TextProps = {
  /**
   *  An extra className to be added to the root element.
   */
  className?: string;

  /**
   * Represents the root element
   */
  tag?: Tag;

  /**
   * Font size of the content
   */
  fontSize?: FontSize;

  /**
   * Font weight of the content
   */
  fontWeight?: FontWeight;

  href?: string;
  children?: ReactNode;
};

const Text = ({ className, children, tag = 'p', fontSize = 'regular', fontWeight = 'normal', ...rest }: TextProps) =>
  createElement(
    tag,
    {
      ...rest,
      className: classNames(className, styles[fontSize], styles[fontWeight]),
    },
    children,
  );

export { Text, TextProps, Tag, FontWeight, FontSize };
