import { lorem } from 'faker';
import { ReactNode } from 'react';
import { Text } from './Text';
import Story from './Story.module.scss';

export default {
  title: 'Components/Atoms/Text',
  component: Text,
};

export const Regular = (): ReactNode => (
  <>
    <Text>{lorem.sentences(10)}</Text>
    <Text>{lorem.sentences(10)}</Text>
    <Text>{lorem.sentences(10)}</Text>
  </>
);

export const FontWeight = (): ReactNode => (
  <>
    <Text fontWeight="semi-bold">{lorem.sentences(10)}</Text>
    <hr />
    <Text fontWeight="normal">{lorem.sentences(10)}</Text>
  </>
);

export const FontSize = (): ReactNode => (
  <>
    <Text fontSize="small">{lorem.sentences(10)}</Text>
    <hr />
    <Text fontSize="regular">{lorem.sentences(10)}</Text>
    <hr />
    <Text fontSize="large">{lorem.sentences(10)}</Text>
  </>
);

export const FontSizeAndFontWeight = (): ReactNode => (
  <>
    <Text fontSize="small" fontWeight="semi-bold">
      {lorem.sentences(10)}
    </Text>
    <hr />
    <Text fontSize="regular" fontWeight="semi-bold">
      {lorem.sentences(10)}
    </Text>
    <hr />
    <Text fontSize="large" fontWeight="semi-bold">
      {lorem.sentences(10)}
    </Text>
  </>
);

export const Caption = (): ReactNode => (
  <Text className={Story.caption} tag="span">
    {lorem.sentences(1)}
  </Text>
);

export const Overline = (): ReactNode => (
  <Text className={Story.overline} tag="span">
    {lorem.sentences(1)}
  </Text>
);

/**
 *
 * some Examples
 *
 */
export const Example = (): ReactNode => (
  <>
    {/* Regular */}
    <h3 className="my-4">Regular</h3>
    <Text>Hello world</Text>

    {/* Overline */}
    <h3 className="my-4">Overline</h3>
    <Text className={Story.overline}>Overline</Text>

    {/* Caption */}
    <h3 className="my-4">Caption</h3>
    <Text className={Story.caption}>Caption</Text>
  </>
);
