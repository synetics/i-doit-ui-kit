import React, { forwardRef, HTMLAttributes, ReactNode, RefObject, useCallback, useImperativeHandle } from 'react';
import classnames from 'classnames';
import { Options, useVirtual, VirtualItem } from 'react-virtual';
import { useGlobalizedRef } from '../../hooks';
import { Wrapper } from '../Modal';
import classes from './ScrollList.module.scss';

type ScrollAlignment = 'start' | 'center' | 'end' | 'auto';

interface ScrollToOptions {
  align: ScrollAlignment;
}

export type ApiType = {
  virtualItems: VirtualItem[];
  totalSize: number;
  scrollToOffset: (index: number, options?: ScrollToOptions) => void;
  scrollToIndex: (index: number, options?: ScrollToOptions) => void;
  measure: () => void;
};

export type ScrollListOptions = Partial<Options<HTMLDivElement>>;

export type ScrollListType = {
  size: number;
  itemSize: number;
  className?: string;
  children: (index: number) => ReactNode;
  apiRef?: RefObject<ApiType>;
  options?: ScrollListOptions;
} & Omit<HTMLAttributes<HTMLDivElement>, 'children'>;

export const ScrollList = forwardRef<HTMLDivElement, ScrollListType>(
  ({ size, itemSize, className, children, apiRef, options, ...rest }, ref) => {
    const { localRef: parentRef, setRefsCallback } = useGlobalizedRef(ref);
    const rowVirtualizer = useVirtual({
      size,
      parentRef,
      estimateSize: useCallback(() => itemSize, [itemSize]),
      ...options,
    });
    useImperativeHandle(apiRef, () => rowVirtualizer, [rowVirtualizer]);

    return (
      <Wrapper {...rest} className={classnames(className, classes.container)} ref={setRefsCallback}>
        <div
          className={classes.wrapper}
          style={{
            height: `${rowVirtualizer.totalSize}px`,
          }}
        >
          {rowVirtualizer.virtualItems.map((row) => (
            <div
              key={`row-${row.key}`}
              className={classes.item}
              style={{
                maxHeight: itemSize,
                transform: `translateY(${row.start}px)`,
              }}
            >
              {children(row.index)}
            </div>
          ))}
        </div>
      </Wrapper>
    );
  },
);
