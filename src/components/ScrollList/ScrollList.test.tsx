import { render, screen } from '@testing-library/react';
import { ScrollList } from './ScrollList';

const renderItem = (i: number) => (
  <span key={`item-${i}`} data-testid={`item-${i}`}>
    {i}
  </span>
);

describe('ScrollList', () => {
  it('renders list', () => {
    const renderItemFn = jest.fn(renderItem);
    render(
      <ScrollList size={100} itemSize={10}>
        {renderItemFn}
      </ScrollList>,
    );

    expect(renderItemFn).toHaveBeenCalled();
    expect(screen.queryByTestId('item-0')).toBeInTheDocument();
    expect(screen.queryByTestId('item-10')).not.toBeInTheDocument();
  });
});
