import { ReactNode, useRef } from 'react';
import { Button, ButtonSpacer } from '../Button';
import { ApiType, ScrollList } from './ScrollList';

export default {
  title: 'Components/Misc/Scroll List',
  component: ScrollList,
};

/**
 *
 * Scroll list renders only the visible part of the list.
 *
 * This behaviour is useful when you need to work with huge amount of data.
 *
 * ### Example
 * In the following example, we can see how the list with 100.000 elements performs
 *
 */
export const Regular = (): ReactNode => (
  <ScrollList
    size={100000}
    itemSize={40}
    style={{
      maxHeight: 300,
    }}
  >
    {(i) => <div className="p-2 border rounded d-flex align-items-center">{i}</div>}
  </ScrollList>
);

/**
 *
 * It's possible to control the list - tell it position to scroll to
 *
 */
export const Imperative = (): ReactNode => {
  const ref = useRef<ApiType | null>(null);

  return (
    <div>
      <ButtonSpacer gridSize="m">
        <Button onClick={() => ref.current?.scrollToIndex(0)}>Scroll to top</Button>
        <Button onClick={() => ref.current?.scrollToIndex(50000)}>Scroll to middle</Button>
        <Button onClick={() => ref.current?.scrollToIndex(100000)}>Scroll to bottom</Button>
      </ButtonSpacer>
      <ScrollList
        className="py-4"
        size={100000}
        itemSize={40}
        style={{
          maxHeight: 300,
        }}
        apiRef={ref}
      >
        {(i) => <div className="p-2 border rounded d-flex align-items-center">{i}</div>}
      </ScrollList>
    </div>
  );
};
