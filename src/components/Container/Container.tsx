import { ElementType, forwardRef, HTMLAttributes, ReactNode } from 'react';
import { ObjectKeysType, omit } from '../../utils';

type ContainerProps = HTMLAttributes<unknown> & {
  /**
   * The content of the field
   */
  children?: ReactNode | ReactNode[];

  /**
   * Class of the container
   */
  className?: string;

  /**
   * Determines how to render the host element
   */
  component?: ElementType;

  dataTestId?: string;
};

const Container = forwardRef(
  ({ children, className, dataTestId, component: Component = 'div', ...props }: ContainerProps, ref) => (
    <Component
      className={className}
      data-testid={dataTestId}
      {...omit(props as ObjectKeysType, 'active', 'readonly', 'setActive')}
      ref={ref}
    >
      {children}
    </Component>
  ),
);

export { Container, ContainerProps };
