import { render, screen } from '@testing-library/react';
import { Container } from './Container';

describe('Container', () => {
  it('renders without crashing', () => {
    render(<Container>Child</Container>);

    const element = screen.getByText('Child');
    expect(element).toBeInTheDocument();
    expect(element.tagName).toBe('DIV');
  });
  it('renders with custom tag', () => {
    render(<Container component="span">Child</Container>);

    const element = screen.getByText('Child');
    expect(element).toBeInTheDocument();
    expect(element.tagName).toBe('SPAN');
  });
  it('renders with passed class', () => {
    render(<Container className="test-class">Child</Container>);

    const element = screen.getByText('Child');
    expect(element).toBeInTheDocument();
    expect(element).toHaveClass('test-class');
  });
});
