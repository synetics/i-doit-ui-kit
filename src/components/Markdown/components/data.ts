const Lorem = `Lorem \`markdownum\`, palmis tellus, ubi nec saepe auctor. Esse **sidera**, volumina
hasta haec manus mane ~~orbem matrona perpetiar~~ ponderis habenas. Ministro pocula
quoque explicat gravis, longum talibus Euphorbus certa remeabat ante, noviens
vos, sustinuisse. Trahit altis rerum! Contingere haec, dea loqui tutus *deus*
advena, spectator classem panda nec.`;

export const data = `
## Headlines
\n
# Headline 1\n
## Headline 2\n
### Headline 3\n
#### Headline 4\n
##### Headline 5\n
###### Headline 6\n\n
---

## Link

[docupike](https://docupike.com)

[empty]()

---
## Unordered list
* item 1
* item 2
* item 3
\n
## Ordered list
1. item 1
2. item 2
3. item 3
\n
## Checked list
* [ ] item 1
* [x] item 2
* [ ] item 3
\n
## Code block
\n
\`\`\`tsx
const a = 123;
return () => a;
\`\`\`
\n
## Quote
\n
> ${Lorem}
\n
## Text
\n
${Lorem}
\n
## Table
\n\n
| Header 1 | Header 2 | Header 3 |
|----------|----------|----------|
| A | B | C |
| A | B | C |
| A | B | C |
\n\n
`;
