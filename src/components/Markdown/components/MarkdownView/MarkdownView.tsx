import React, { forwardRef } from 'react';
import { MarkdownPreviewProps, MarkdownPreviewRef } from '@uiw/react-markdown-preview';
import MDEditor from '@uiw/react-md-editor';
import { isExact, isObjectWithShape } from '@i-doit/enten-types';
import { Element } from 'hast';
import { defaultComponents } from '../defaultComponents';

type MarkdownInnerType = {
  theme?: 'dark' | 'light' | 'auto';
  components?: Exclude<MarkdownPreviewProps['components'], null>;
};

type MarkdownViewType = MarkdownInnerType & Omit<MarkdownPreviewProps, keyof MarkdownInnerType>;

const isElement = (node: unknown): node is Element =>
  isObjectWithShape({
    type: isExact('element'),
  })(node);

export const MarkdownView = forwardRef<MarkdownPreviewRef, MarkdownViewType>(
  ({ components = defaultComponents, wrapperElement, theme = 'light', ...props }, ref) => (
    <MDEditor.Markdown
      skipHtml={false}
      {...props}
      ref={ref}
      wrapperElement={{
        ...(wrapperElement || {}),
        'data-color-mode': theme === 'auto' ? undefined : theme,
      }}
      components={components}
      rehypeRewrite={(node, index, parent) => {
        if (isElement(node) && node.tagName === 'a' && isElement(parent) && /^h(1|2|3|4|5|6)/.test(parent.tagName)) {
          parent.children.splice(0, 1);
        }
      }}
    />
  ),
);
