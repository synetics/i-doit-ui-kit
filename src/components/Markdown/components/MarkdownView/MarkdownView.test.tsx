import { render, screen } from '@testing-library/react';
import { data } from '../data';
import { MarkdownView } from './MarkdownView';

describe('MarkdownView', () => {
  it.each(['dark' as const, 'light' as const, 'auto' as const, undefined])(
    'renders without crashing',
    async (theme) => {
      render(
        <MarkdownView
          wrapperElement={{
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            'data-testId': 'markdown',
          }}
          source={data}
          theme={theme}
        />,
      );
      expect(await screen.findByTestId('markdown')).toBeInTheDocument();
    },
  );

  it('renders with custom headline', async () => {
    render(
      <MarkdownView
        source="# Headline 1"
        components={{ h1: ({ children }) => <div data-testId="testHeadline">{children}</div> }}
      />,
    );
    expect(await screen.findByTestId('testHeadline')).toBeInTheDocument();
  });
});
