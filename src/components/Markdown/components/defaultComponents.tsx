import { MarkdownPreviewProps } from '@uiw/react-markdown-preview';
import { MDComponents } from './MDComponents';

export const defaultComponents: MarkdownPreviewProps['components'] = {
  h1: MDComponents.Headline,
  h2: MDComponents.Headline,
  h3: MDComponents.Headline,
  h4: MDComponents.Headline,
  h5: MDComponents.Headline,
  h6: MDComponents.Headline,
  p: MDComponents.Textual<'p'>('p'),
  li: MDComponents.Textual<'li'>('li'),
  hr: () => <div className="my-2 w-100 border-bottom" />,
  a: MDComponents.Anchor,
  table: MDComponents.TableElement,
  thead: MDComponents.TableHead,
  tr: MDComponents.TableRow,
  td: MDComponents.TableData,
  th: MDComponents.TableHeader,
};
