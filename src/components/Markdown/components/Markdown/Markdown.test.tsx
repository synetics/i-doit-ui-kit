import { act, render, screen } from '@testing-library/react';
import user from '@testing-library/user-event';
import { commands } from '../commands';
import { asGroup } from '../Toolbar';
import { Markdown } from './Markdown';

describe('Markdown', () => {
  it('renders without crashing', () => {
    render(<Markdown value="# Headline" />);
  });

  it('renders without crashing in disabled state', () => {
    render(<Markdown disabled value="# Headline" />);
  });

  it('renders with special commands', () => {
    render(
      <Markdown
        value="# Headline"
        commands={[asGroup('group', [{ name: 'divider' }])]}
        extraCommands={[commands.view.group]}
      />,
    );
  });

  it('renders with empty group', () => {
    render(<Markdown value="# Headline" commands={[asGroup('group', [])]} />);
  });

  it('renders without commands', () => {
    render(<Markdown value="# Headline" commands={[]} extraCommands={[]} />);
  });

  it('renders with custom headline', () => {
    render(
      <Markdown
        value="# Headline"
        components={{
          h1: ({ children }) => <div data-testId="test">{children}</div>,
        }}
      />,
    );
    expect(screen.queryByTestId('test')).toBeInTheDocument();
  });

  it.each(['dark' as const, 'light' as const, 'auto' as const, undefined])(
    'renders without crashing in themes',
    (theme) => {
      render(<Markdown theme={theme} value="# Headline" />);
    },
  );
  it('renders in edit mode', () => {
    render(
      <Markdown
        preview="edit"
        value="# Headline"
        previewOptions={{
          wrapperElement: {
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            'data-testId': 'preview',
          },
        }}
      />,
    );
    expect(screen.queryByTestId('preview')).not.toBeInTheDocument();
  });
  it('renders in view mode', () => {
    render(
      <Markdown
        preview="preview"
        value="# Headline"
        previewOptions={{
          wrapperElement: {
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            'data-testId': 'preview',
          },
        }}
      />,
    );
    expect(screen.queryByTestId('preview')).toBeInTheDocument();
  });
  it('renders in live mode', () => {
    render(
      <Markdown
        preview="live"
        value="# Headline"
        previewOptions={{
          wrapperElement: {
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            'data-testId': 'preview',
          },
        }}
      />,
    );
    expect(screen.queryByTestId('preview')).toBeInTheDocument();
  });
  it('changes value', async () => {
    const onChange = jest.fn();
    render(<Markdown id="test" value="# Headline" onChange={onChange} />);
    const textbox = await screen.findByRole('textbox');
    await user.click(textbox);
    await user.paste('Test');
    expect(onChange).toHaveBeenCalledWith('# HeadlineTest', expect.anything(), expect.anything());
  });

  it('reacts on click', async () => {
    const onChange = jest.fn();
    render(<Markdown id="test" value="# Headline" onChange={onChange} />);
    const buttons = await screen.findAllByTitle('Insert title1 (ctrl + 1)');
    act(() => buttons[0].click());
    expect(onChange).toHaveBeenCalledWith('Headline', expect.anything(), expect.anything());
  });
});
