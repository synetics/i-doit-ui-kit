import React, { forwardRef } from 'react';
import { MarkdownPreviewProps } from '@uiw/react-markdown-preview';
import MDEditor, { RefMDEditor } from '@uiw/react-md-editor';
import classnames from 'classnames';
import { GetComponentProps } from '../../../../types/get-component-type';
import { defaultCommands } from '../defaultCommands';
import { defaultComponents } from '../defaultComponents';
import { defaultExtraCommands } from '../defaultExtraCommands';
import classes from './Markdown.module.scss';

type MarkdownInnerType = {
  disabled?: boolean;
  theme?: 'dark' | 'light' | 'auto';
  commands?: typeof defaultCommands;
  components?: Exclude<MarkdownPreviewProps['components'], null>;
};

export type MarkdownType = MarkdownInnerType & Omit<GetComponentProps<typeof MDEditor>, keyof MarkdownInnerType>;

export const Markdown = forwardRef<RefMDEditor, MarkdownType>(
  (
    {
      disabled,
      className,
      theme = 'light',
      preview = 'live',
      onChange,
      commands = defaultCommands,
      extraCommands = defaultExtraCommands,
      components = defaultComponents,
      previewOptions,
      ...props
    },
    ref,
  ) => (
    <MDEditor
      {...props}
      ref={ref}
      textareaProps={{
        ...props.textareaProps,
        disabled,
      }}
      onChange={disabled ? undefined : onChange}
      commands={disabled ? [] : commands}
      previewOptions={{
        skipHtml: false,
        ...(previewOptions || {}),
        components,
      }}
      extraCommands={extraCommands}
      preview={preview}
      className={classnames(className, classes.markdown)}
      data-color-mode={theme === 'auto' ? undefined : theme}
    />
  ),
);
