import classnames from 'classnames';
import { MdComponentTypes } from '../types';
import classes from './Table.module.scss';

export const TableRow: MdComponentTypes['tr'] = ({ node, className, children, ...rest }) => (
  <div {...rest} className={classnames(classes.tr, className)}>
    {children}
  </div>
);
