import { CellWrapper } from '../../../../Table';
import { Text } from '../../../../Text';
import { MdComponentTypes } from '../types';
import classes from './Table.module.scss';

export const TableHeader: MdComponentTypes['th'] = ({ node, className, children, ...rest }) => (
  <CellWrapper className={classes.td}>
    <Text tag="span" {...rest} fontWeight="semi-bold" className={className}>
      {children}
    </Text>
  </CellWrapper>
);
