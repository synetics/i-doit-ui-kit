import classnames from 'classnames';
import { MdComponentTypes } from '../types';
import classes from './Table.module.scss';

export const TableElement: MdComponentTypes['table'] = ({ node, className, children, ...rest }) => (
  <div {...rest} className={classnames(classes.table, className)}>
    {children}
  </div>
);
