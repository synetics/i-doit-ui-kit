import { Text } from '../../../../Text';
import { MdComponentTypes } from '../types';
import { CellWrapper } from '../../../../Table';
import classes from './Table.module.scss';

export const TableData: MdComponentTypes['td'] = ({ node, className, children, ...rest }) => (
  <CellWrapper className={classes.td}>
    <Text tag="span" {...rest} className={className}>
      {children}
    </Text>
  </CellWrapper>
);
