import classnames from 'classnames';
import { MdComponentTypes } from '../types';
import classes from './Table.module.scss';

export const TableHead: MdComponentTypes['thead'] = ({ node, className, children, ...rest }) => (
  <div {...rest} className={classnames(classes.thead, className)}>
    {children}
  </div>
);
