import React from 'react';
import { Text } from '../../../../Text';
import { ExternalLink } from '../../../../ExternalLink';
import { MdComponentTypes } from '../types';

export const Anchor: MdComponentTypes['a'] = ({ node, className, children, href, ...rest }) => {
  if (!href || href.startsWith('#')) {
    return (
      <Text {...rest} tag="a" className={className} href={href}>
        {children}
      </Text>
    );
  }
  return (
    <ExternalLink {...rest} className={className} href={href}>
      {children}
    </ExternalLink>
  );
};
