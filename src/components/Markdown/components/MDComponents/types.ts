import { MarkdownPreviewProps } from '@uiw/react-markdown-preview';

export type MdComponentTypes = Required<Exclude<MarkdownPreviewProps['components'], null | undefined>>;
