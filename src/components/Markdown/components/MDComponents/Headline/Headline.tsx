import classnames from 'classnames';
import { isString } from '@i-doit/enten-types';
import { MdComponentTypes } from '../types';
import { Headline as BaseHeadline } from '../../../../Headline';
import classes from './Headline.module.scss';

const supportedLevels = ['h1' as const, 'h2' as const, 'h3' as const, 'h4' as const, 'h5' as const, 'h6' as const];
type ArrayType<T> = T extends (infer A)[] ? A : never;
type Levels = ArrayType<typeof supportedLevels>;
const isSupportedLevel = (v: unknown): v is Levels => isString(v) && (supportedLevels as string[]).includes(v);

type HeadlineType = MdComponentTypes[Levels];

export const Headline: HeadlineType = ({ node, className, ref, ...rest }) => {
  const as = node?.tagName;
  const correctedRef = isString(ref) ? undefined : ref;
  return (
    <BaseHeadline
      {...rest}
      className={classnames(classes.headline, className)}
      as={isSupportedLevel(as) ? as : 'h6'}
      ref={correctedRef}
    />
  );
};
