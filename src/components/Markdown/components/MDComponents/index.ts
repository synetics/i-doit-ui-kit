import { Headline } from './Headline';
import { Textual } from './Textual';
import { Anchor } from './Anchor';
import * as TableComponents from './Table';

export const MDComponents = {
  ...TableComponents,
  Anchor,
  Headline,
  Textual,
} as const;

export * from './types';
