import React from 'react';
import classnames from 'classnames';
import { GetComponentProps } from '../../../../../types/get-component-type';
import { Text, TextProps } from '../../../../Text/Text';
import { MdComponentTypes } from '../types';

export const Textual = <T extends keyof MdComponentTypes>(tag: TextProps['tag'], className?: string) =>
  (({ node, ...props }: Omit<GetComponentProps<MdComponentTypes[T]>, 'fontSize'>) => (
    <Text {...props} tag={tag} className={classnames(className, props.className)} />
  )) as MdComponentTypes[T];
