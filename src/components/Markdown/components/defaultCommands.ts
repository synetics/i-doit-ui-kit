import { ICommand } from '@uiw/react-md-editor';
import { asGroup } from './Toolbar';
import { commands } from './commands';

export const defaultCommands: ICommand[] = [
  asGroup('format', [commands.style.bold, commands.style.italic, commands.style.strikethrough, commands.style.code]),
  asGroup('headline', [commands.style.title1, commands.style.title2, commands.style.title3]),
  asGroup('list', [commands.list.orderedList, commands.list.unorderedList, commands.list.checkedList]),
  asGroup('block', [commands.block.codeBlock, commands.block.quote]),
];
