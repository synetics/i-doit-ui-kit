import { ICommand } from '@uiw/react-md-editor';
import { commands } from './commands';

export const defaultExtraCommands: ICommand[] = [commands.helpLink];
