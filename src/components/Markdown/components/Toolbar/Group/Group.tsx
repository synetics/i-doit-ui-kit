import { ICommand } from '@uiw/react-md-editor';
import React from 'react';
import { ButtonGroup } from '../../../../Button';

export const Group: ICommand['render'] = (command, disabled, executeCommand) => {
  const { children, keyCommand } = command;
  return (
    <ButtonGroup key={keyCommand} groupType="outline" className="mr-2">
      {children && Array.isArray(children)
        ? children.map((a, i) => {
            if (a.render) {
              const rendered = a.render(
                {
                  ...a,
                  buttonProps: {
                    ...a.buttonProps,
                    className: '',
                  },
                },
                disabled,
                executeCommand,
                i,
              );
              return rendered && React.isValidElement(rendered) ? rendered : null;
            }
            return null;
          })
        : null}
    </ButtonGroup>
  );
};

export const asGroup = (name: string, children: ICommand[], canDisable = true): ICommand => ({
  keyCommand: name,
  children,
  render: (cmd, disabled, executeCommand, index) => Group(cmd, canDisable ? disabled : false, executeCommand, index),
});
