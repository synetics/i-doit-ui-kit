import React, { ReactElement } from 'react';
import { ICommand } from '@uiw/react-md-editor';
import { IconButton } from '../../../../IconButton';
import classes from './ToolbarButton.module.scss';

export type ToolbarButtonProps = ICommand['render'];

export const ToolbarButton: ToolbarButtonProps = (command, disabled, executeCommand) => {
  const { icon, shortcuts, name, buttonProps } = command;
  if (!icon || !name) {
    return null;
  }
  const shortcut = shortcuts
    ? ` (${shortcuts
        .replace(/ctrl(\W)/gi, '⌃$1')
        .replace('ctrlcmd', '⌘')
        .replace('shift', '⇧')})`
    : '';
  const label = `${name}${shortcut}`;
  return (
    <IconButton
      key={label}
      label={label}
      className={classes.button}
      variant="outline"
      {...buttonProps}
      disabled={disabled}
      disabledTooltip={`${name}`}
      icon={icon}
      onClick={() => executeCommand(command, name)}
    />
  );
};

export const asToolbarButton = <T extends ICommand>(command: T, title?: string, icon?: ReactElement): T => {
  const produced = {
    ...command,
    render: ToolbarButton,
  } as T;
  if (title) {
    produced.name = title;
  }
  if (icon) {
    produced.icon = icon;
  }
  return produced;
};
