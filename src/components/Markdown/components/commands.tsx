import React from 'react';
import {
  bold,
  checkedListCommand,
  code,
  codeBlock,
  codeEdit,
  codeLive,
  codePreview,
  comment,
  fullscreen,
  help,
  hr,
  ICommand,
  image,
  italic,
  link,
  orderedListCommand,
  quote,
  strikethrough,
  table,
  title1,
  title2,
  title3,
  title4,
  title5,
  title6,
  unorderedListCommand,
} from '@uiw/react-md-editor';
import { ExternalLink } from '../../ExternalLink';
import { Icon } from '../../Icon';
import { open_tab } from '../../../icons';
import { asGroup, asToolbarButton } from './Toolbar';

const viewCommands = {
  edit: asToolbarButton(codeEdit, 'Edit'),
  live: asToolbarButton(codeLive, 'Live'),
  preview: asToolbarButton(codePreview, 'Preview'),
  fullscreen: asToolbarButton(fullscreen, 'Fullscreen'),
};

export const commands = {
  style: {
    bold: asToolbarButton(bold, 'Bold'),
    italic: asToolbarButton(italic, 'Italic'),
    strikethrough: asToolbarButton(strikethrough, 'Strikethrough'),
    code: asToolbarButton(code, 'Code'),
    title1: asToolbarButton(title1, 'Headline 1', React.createElement('span', {}, 'H1')),
    title2: asToolbarButton(title2, 'Headline 2', React.createElement('span', {}, 'H2')),
    title3: asToolbarButton(title3, 'Headline 3', React.createElement('span', {}, 'H3')),
    title4: asToolbarButton(title4, 'Headline 4', React.createElement('span', {}, 'H4')),
    title5: asToolbarButton(title5, 'Headline 5', React.createElement('span', {}, 'H5')),
    title6: asToolbarButton(title6, 'Headline 6', React.createElement('span', {}, 'H6')),
  },
  list: {
    orderedList: asToolbarButton(orderedListCommand, 'Ordered list'),
    unorderedList: asToolbarButton(unorderedListCommand, 'Unordered list'),
    checkedList: asToolbarButton(checkedListCommand, 'Check list'),
  },
  block: {
    quote: asToolbarButton(quote, 'Quote'),
    codeBlock: asToolbarButton(codeBlock, 'Code block'),
    comment: asToolbarButton(comment, 'Comment'),
  },
  elements: {
    table: asToolbarButton(table, 'Table'),
    image: asToolbarButton(image, 'External image'),
    link: asToolbarButton(link, 'Link'),
    hr: asToolbarButton(hr, 'Divider'),
  },
  view: {
    ...viewCommands,
    group: asGroup('view', [viewCommands.edit, viewCommands.live, viewCommands.preview], false),
  },
  help: asToolbarButton(help, 'Learn about syntax'),
  helpLink: {
    ...help,
    render: () => (
      <ExternalLink className="d-flex align-items-center" href="https://www.markdownguide.org/basic-syntax/">
        <Icon fill="active" src={open_tab} className="mr-1" />
        <span>What markdown is supported?</span>
      </ExternalLink>
    ),
  } as ICommand,
} as const;
