import { ReactNode, useState } from 'react';
import { TextArea } from '../Form/Control';
import { editText } from '../../icons';
import { IconButton } from '../IconButton';
import { Icon } from '../Icon';
import { useToggle } from '../../hooks';
import { Footer, Header, Modal, Wrapper } from '../Modal';
import { Button, ButtonSpacer } from '../Button';
import { FormGroup, Label } from '../Form/Infrastructure';
import { data } from './components/data';
import { Markdown, MarkdownView } from './components';

export default {
  title: 'Components/Molecules/Form/Markdown',
  component: Markdown,
};

/**
 *
 * Markdown allows you to render the markdown content. In the following example, you can see the supported elements.
 *
 */

export const ViewMode = (): ReactNode => {
  const [text] = useState(data);
  return (
    <div style={{ minHeight: 500 }} className="d-flex flex-column">
      <MarkdownView theme="light" className="flex-fill" source={text} />
    </div>
  );
};

/**
 *
 * Here is the example markdown in a disabled view
 *
 */

export const DisabledMode = (): ReactNode => {
  const [text, setText] = useState(data);
  return (
    <div style={{ minHeight: 500 }} className="d-flex flex-column">
      <Markdown
        disabled
        theme="light"
        className="flex-fill"
        value={text}
        onChange={(a) => {
          if (a) {
            setText(a);
          }
        }}
      />
    </div>
  );
};

/**
 *
 * Markdown editor allows you to edit the markdown content
 *
 */

export const EditMode = (): ReactNode => {
  const [text, setText] = useState(data);
  return (
    <div style={{ minHeight: 500 }} className="d-flex flex-column">
      <Markdown
        theme="light"
        className="flex-fill"
        value={text}
        onChange={(a) => {
          if (a) {
            setText(a);
          }
        }}
      />
    </div>
  );
};

/**
 *
 * The markdown editor can be integrated to the text area control.
 *
 */

export const TextMode = (): ReactNode => {
  const [text, setText] = useState<string>(`# Example`);
  const [tempText, setTempText] = useState(`# Example`);
  const [show, showApi] = useToggle(false);

  const confirm = () => {
    setText(tempText);
    showApi.disable();
  };
  const close = () => {
    showApi.disable();
  };
  const open = () => {
    setTempText(text);
    showApi.enable();
  };

  return (
    <div style={{ minHeight: 500 }} className="d-flex flex-column">
      <FormGroup>
        <Label id="textArea">Text</Label>
        <div className="d-flex align-items-start">
          <TextArea
            id="textarea"
            value={text}
            style={{
              maxWidth: 300,
              maxHeight: 300,
            }}
          />
          <IconButton
            className="ml-1 mt-3"
            variant="outline"
            icon={<Icon src={editText} />}
            onClick={open}
            label="Edit text"
          />
        </div>
      </FormGroup>
      <Modal
        contentClassName="h-100"
        size="xl"
        portal={document.getElementById('portal-root') || document.body}
        open={show}
      >
        <Header onClose={close}>Edit text</Header>
        <Wrapper>
          <Markdown
            autoFocus
            theme="light"
            className="flex-fill h-100"
            value={tempText}
            onChange={(a) => {
              if (a) {
                setTempText(a);
              }
            }}
          />
        </Wrapper>
        <Footer>
          <ButtonSpacer gridSize="m">
            <Button variant="primary" onClick={confirm}>
              Save
            </Button>
            <Button variant="text" onClick={close}>
              Close
            </Button>
          </ButtonSpacer>
        </Footer>
      </Modal>
    </div>
  );
};
