import React, { BaseSyntheticEvent, CSSProperties, LegacyRef, ReactNode, RefObject, useContext, useMemo } from 'react';
import classnames from 'classnames';
import { useFocus } from '../../hooks/use-focus';
import { handleKey } from '../../utils';
import styles from './Menu.module.scss';
import { PathContext } from './context';

export type MenuProps = {
  autoFocus?: boolean;
  className?: string;
  id?: string;
  role?: string;
  trigger?: RefObject<HTMLElement>;
  style?: CSSProperties;
  onClose?: (e: BaseSyntheticEvent) => void;
  size?: 'm' | 'l';
  children?: ReactNode;
};

const SubMenu = ({ autoFocus = false, children, className, id, onClose, size = 'm', style }: MenuProps) => {
  const menuRef = useFocus(autoFocus);
  const parentPath = useContext(PathContext);
  const path = useMemo(() => (id ? [...parentPath, id] : parentPath), [id, parentPath]);
  return (
    <PathContext.Provider value={path}>
      <div
        id={id}
        style={style}
        className={classnames(styles.menu, styles[`size-${size}`], className)}
        ref={menuRef as LegacyRef<HTMLDivElement>}
        onKeyDown={onClose ? handleKey({ ArrowLeft: (e) => onClose(e) }) : undefined}
        role="button"
        tabIndex={-1}
      >
        {children}
      </div>
    </PathContext.Provider>
  );
};

export { SubMenu };
