import { fireEvent, render, screen } from '@testing-library/react';
import user from '@testing-library/user-event';
import { Item, SubItem } from './components/items';
import { Menu } from './Menu';

describe('Menu', () => {
  const icon = 'image/src';
  const fn = jest.fn();

  it('renders with content inside without crashing', () => {
    render(
      <Menu id="id" autoFocus>
        <Item id="test-item" icon={icon} label="{Label}" onSelect={fn} selected />
      </Menu>,
    );

    expect(screen.getByRole('button')).toBeInTheDocument();
  });

  it('handles onCloseRequest on Escape keydown', () => {
    render(
      <Menu id="id" autoFocus onCloseRequest={fn}>
        <Item id="test-item" icon={icon} label="{Label}" onSelect={fn} selected />
      </Menu>,
    );

    const element = screen.getByRole('button');
    element.focus();

    fireEvent.keyDown(element, {
      key: 'Escape',
      code: 'Escape',
      keyCode: 27,
      charCode: 27,
    });

    expect(screen.getByRole('button')).toBeInTheDocument();
    expect(fn).toHaveBeenCalled();
  });

  it('Does not crash if onCloseRequest is missing Escape keydown', () => {
    render(
      <Menu id="id" autoFocus>
        <Item id="test-item" icon={icon} label="{Label}" onSelect={fn} selected />
      </Menu>,
    );

    const element = screen.getByRole('button');
    element.focus();

    fireEvent.keyDown(element, {
      key: 'Escape',
      code: 'Escape',
      keyCode: 27,
      charCode: 27,
    });

    expect(screen.getByRole('button')).toBeInTheDocument();
  });

  it('handles onSelect on Enter keydown', () => {
    render(
      <Menu id="id" autoFocus>
        <Item id="test-item" icon={icon} label="{Label}" onSelect={fn} selected />
      </Menu>,
    );

    const element = screen.getByRole('menuitem');
    element.focus();

    fireEvent.keyUp(element, {
      key: 'Enter',
      code: 'Enter',
      keyCode: 13,
      charCode: 13,
    });

    expect(screen.getByRole('button')).toBeInTheDocument();
    expect(fn).toHaveBeenCalled();
  });

  it('opens children on arrow click', async () => {
    render(
      <Menu id="id">
        <SubItem id="test" onSelect={fn} label="MyLabel" open={false}>
          <Item id="submenu.0" label="{Label}" onSelect={() => {}} />
        </SubItem>
      </Menu>,
    );

    const item = screen.getByText('MyLabel');
    item.focus();
    expect(screen.queryByText('{Label}')).not.toBeInTheDocument();
    await user.type(item, '{arrowright}');
    screen.getByText('{Label}');
    await user.type(item, '{arrowleft}');
  });

  it('opens children on mouse hover', async () => {
    render(
      <Menu id="id">
        <SubItem id="test" onSelect={fn} label="MyLabel" open={false}>
          <Item id="submenu.0" label="{Label}" onSelect={() => {}} />
        </SubItem>
      </Menu>,
    );

    const item = screen.getByRole('menuitem');
    await user.hover(item);
    expect(screen.queryByText('{Label}')).toBeInTheDocument();
  });
  it('check size class', () => {
    render(
      <Menu id="id" autoFocus size="l">
        <Item id="test-item" icon={icon} label="{Label}" onSelect={fn} selected />
      </Menu>,
    );

    expect(screen.getByRole('button')).toHaveClass('size-l');
  });
});
