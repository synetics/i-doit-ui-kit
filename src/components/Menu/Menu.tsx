import { CSSProperties, useCallback, useEffect, useMemo, useState } from 'react';
import classnames from 'classnames';
import { useFocusWithin } from '@mantine/hooks';
import { useEventListener, useKeyboardUsed } from '../../hooks';
import { handleKey } from '../../utils/helpers';
import { MenuContext, MenuContextType } from './context';
import { MenuState, registerItems, unregisterItems, useHandleItems } from './utils';
import { MenuProps, SubMenu } from './SubMenu';
import classes from './Menu.module.scss';

type TopMenuProps = MenuProps & {
  onCloseRequest?: (e: CloseEvent) => void;
  size?: 'm' | 'l';
  style?: CSSProperties;
};

const Menu = ({
  children,
  onCloseRequest,
  trigger,
  autoFocus,
  size = 'm',
  style,
  className,
  ...props
}: TopMenuProps) => {
  const [items, setItems] = useState<MenuState>({ topLevel: [], map: {} });
  const [focused, setFocused] = useState<string[]>([]);
  const keyboardUsed = useKeyboardUsed();
  const register = useCallback((path: string[]) => setItems(registerItems(path)), []);
  const unRegister = useCallback((path: string[]) => setItems(unregisterItems(path)), []);
  const { ref, focused: focusedState } = useFocusWithin<HTMLDivElement>({
    onFocus: () => setFocused([items.topLevel[0]]),
    onBlur: () => setFocused([]),
  });
  useEffect(() => {
    if (autoFocus) {
      ref.current?.focus();
    }
  }, [autoFocus, ref]);

  const isFocused = useCallback((id: string) => focused.includes(id), [focused]);
  const context: MenuContextType = useMemo(
    () => ({
      register,
      unRegister,
      isFocused,
      focus: setFocused,
    }),
    [register, isFocused, unRegister],
  );
  useHandleItems(items, focused, setFocused, trigger);
  useEventListener(
    'keydown',
    handleKey({
      Escape: (e) => {
        setFocused([]);
        if (onCloseRequest) {
          onCloseRequest(e);
        }
      },
    }),
    trigger?.current || document.body,
  );

  return (
    <MenuContext.Provider value={context}>
      <div
        className={classes.container}
        data-keyboard={keyboardUsed}
        tabIndex={focusedState ? -1 : 0}
        role="presentation"
        ref={ref}
        style={style}
      >
        <SubMenu {...props} className={classnames(classes.menu, classes[`size-${size}`], className)}>
          {children}
        </SubMenu>
      </div>
    </MenuContext.Provider>
  );
};

export { Menu };
