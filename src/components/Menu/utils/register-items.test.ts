import { MenuState } from './types';
import { registerItems } from './register-items';

describe('register items', () => {
  const initialState: MenuState = {
    topLevel: ['root', 'A'],
    map: {
      root: ['B'],
      A: ['C'],
      B: [],
      C: [],
    },
  };
  it.each([
    [[], { topLevel: [], map: {} }],
    [['root'], { topLevel: ['root'], map: { root: [] } }],
    [['root', 'A'], { topLevel: ['root'], map: { root: ['A'], A: [] } }],
    [['root', 'A', 'B', 'C'], { topLevel: ['root'], map: { root: ['A'], A: ['B'], B: ['C'], C: [] } }],
  ])('registers correct items', (path: string[], expected: MenuState): void => {
    const result = registerItems(path)({ topLevel: [], map: {} });
    expect(result).toStrictEqual(expected);
  });
  it.each([
    [['root'], initialState],
    [['root', 'B'], initialState],
    [['root', 'B', 'D'], { topLevel: ['root', 'A'], map: { root: ['B'], A: ['C'], B: ['D'], C: [], D: [] } }],
  ])('registers correct items with existing state', (path: string[], expected: MenuState): void => {
    const result = registerItems(path)(initialState);
    expect(result).toStrictEqual(expected);
  });
});
