import { handleAction } from './handle-action';

describe('handle-action', () => {
  it('calls the callback and prevents event', () => {
    const callback = jest.fn();
    const event: Event = new Event('custom event');
    const called = jest.spyOn(event, 'stopPropagation');
    handleAction(callback)(event);
    expect(callback).toHaveBeenCalledTimes(1);
    expect(called).toHaveBeenCalledTimes(1);
  });

  it('calls the callback without event', () => {
    const callback = jest.fn();
    handleAction(callback)();
    expect(callback).toHaveBeenCalledTimes(1);
  });
});
