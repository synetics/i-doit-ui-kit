import { MenuState } from './types';

const getPathByIndices = ({ topLevel, map }: MenuState, indices: number[]): string[] => {
  const result: string[] = [];
  indices.reduce((level, index) => {
    const id = level[index];
    result.push(id);
    return map[id];
  }, topLevel);

  return result;
};

export { getPathByIndices };
