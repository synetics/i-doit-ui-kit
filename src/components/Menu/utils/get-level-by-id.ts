import { MenuItems } from './types';

const getLevelById = (map: MenuItems, id: string): string[] => map[id] || [];

export { getLevelById };
