import { MenuState } from './types';

const getIndicesByPath = ({ topLevel, map }: MenuState, path: string[]): number[] => {
  const result: number[] = [];
  path.reduce((level, id) => {
    result.push(level.indexOf(id));
    return map[id];
  }, topLevel);

  return result;
};

export { getIndicesByPath };
