import { getIndicesByPath } from './get-indices-by-path';
import { MenuState } from './types';

describe('Get indices by path', () => {
  const data: MenuState = {
    topLevel: ['root'],
    map: {
      root: ['A', 'B'],
      A: ['C'],
      B: ['D'],
      C: [],
    },
  };
  it.each([
    [data, ['non-existing'], [-1]],
    [data, ['root'], [0]],
    [data, ['root', 'A'], [0, 0]],
    [data, ['root', 'B'], [0, 1]],
    [data, ['root', 'A', 'C'], [0, 0, 0]],
  ])('returns the right result', (state: MenuState, path: string[], expected: number[]) => {
    const result = getIndicesByPath(state, path);
    expect(result).toStrictEqual(expected);
  });
});
