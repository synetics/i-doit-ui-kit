import { MenuState } from './types';

type UnregisterItemsType = (path: string[]) => (state: MenuState) => MenuState;

const unregisterItems: UnregisterItemsType =
  (path: string[]) =>
  (state: MenuState): MenuState => {
    if (path.length === 0) {
      return state;
    }
    const { topLevel, map } = state;
    const newTopLevel = topLevel.filter((item) => !path.includes(item));
    const newMap = { ...map };

    for (let i = 0; i < path.length; i += 1) {
      const key = path[i];
      const nextKey = path[i + 1] || null;

      if (newMap[key]) {
        if (nextKey !== null) {
          newMap[key] = newMap[key].filter((item) => item !== nextKey);
        } else {
          delete newMap[key];
        }
      }
    }
    return {
      topLevel: newTopLevel,
      map: newMap,
    };
  };

export { unregisterItems };
