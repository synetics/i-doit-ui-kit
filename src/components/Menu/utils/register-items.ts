import { MenuState } from './types';

type RegisterItemsType = (path: string[]) => (state: MenuState) => MenuState;

const registerItems: RegisterItemsType =
  (path: string[]) =>
  (state: MenuState): MenuState => {
    if (path.length === 0) {
      return state;
    }
    const [root] = path;
    const { topLevel, map } = state;
    const newTopLevel = topLevel.includes(root) ? topLevel : [...topLevel, root];
    const newMap = { ...map };
    for (let i = 0; i < path.length; i += 1) {
      const key = path[i];
      const nextKey = path[i + 1] || null;

      if (!newMap[key]) {
        newMap[key] = [];
      }

      if (nextKey !== null && !newMap[key].includes(nextKey)) {
        newMap[key].push(nextKey);
      }
    }

    return {
      topLevel: newTopLevel,
      map: newMap,
    };
  };

export { registerItems };
