import { RefObject, useMemo } from 'react';
import { useKeyboardIndices } from '../../../hooks';
import { getLevelById } from './get-level-by-id';
import { MenuState } from './types';
import { getIndicesByPath } from './get-indices-by-path';
import { getPathByIndices } from './get-path-by-indices';

const useHandleItems = (
  state: MenuState,
  focused: string[],
  setFocused: (focused: string[]) => void,
  trigger?: RefObject<HTMLElement>,
): void => {
  const { topLevel, map } = state;
  const currentLevel = useMemo(
    () => (focused[focused.length - 2] ? getLevelById(map, focused[focused.length - 2]) : topLevel),
    [focused, map, topLevel],
  );
  const nextLevel = useMemo(() => getLevelById(map, focused[focused.length - 1]), [focused, map]);
  const indices = useMemo(() => getIndicesByPath(state, focused), [focused, state]);

  useKeyboardIndices(
    indices,
    (newIndices) => {
      setFocused(getPathByIndices(state, newIndices));
    },
    currentLevel.length,
    nextLevel.length > 0,
    trigger?.current || document.body,
  );
};

export { useHandleItems };
