type MenuItems = {
  [key: string]: string[];
};

type MenuState = {
  topLevel: string[];
  map: MenuItems;
};

export { MenuState, MenuItems };
