export const handleAction =
  <A extends () => void>(fn: A) =>
  (event?: Event): void => {
    event?.stopPropagation();
    fn();
  };
