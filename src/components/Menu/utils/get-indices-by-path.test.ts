import { getPathByIndices } from './get-path-by-indices';
import { MenuState } from './types';

describe('Get indices by path', () => {
  const data: MenuState = {
    topLevel: ['root'],
    map: {
      root: ['A', 'B'],
      A: ['C'],
      B: ['D'],
      C: [],
    },
  };
  it.each([
    [data, [0], ['root']],
    [data, [0, 0], ['root', 'A']],
    [data, [0, 1], ['root', 'B']],
    [data, [0, 0, 0], ['root', 'A', 'C']],
  ])('returns the right result', (state: MenuState, indices: number[], path: string[]) => {
    const result = getPathByIndices(state, indices);
    expect(result).toStrictEqual(path);
  });
});
