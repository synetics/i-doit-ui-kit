import { renderHook } from '@testing-library/react-hooks';
import user from '@testing-library/user-event';
import { useHandleItems } from './use-handle-items';
import { MenuState } from './types';

describe('useHandleItems', () => {
  const state: MenuState = {
    topLevel: ['a', 'b'],
    map: {
      a: ['c', 'd'],
      b: ['e', 'f'],
      c: [],
      d: [],
      e: [],
      f: [],
    },
  };

  it.each([
    ['{arrowdown}', ['a'], ['b']],
    ['{arrowup}', ['a'], ['a']],
    ['{end}', ['a'], ['b']],
    ['{home}', ['b'], ['a']],
    ['{arrowright}', ['a'], ['a', 'c']],
    ['{arrowleft}', ['a', 'c'], ['a']],
  ])('on %s changes focus from %s to %s', async (text: string, focused: string[], expected: string[]) => {
    const setFocused = jest.fn();
    renderHook(() => useHandleItems(state, focused, setFocused));

    await user.type(document.body, text);

    expect(setFocused).toHaveBeenCalledTimes(1);
    expect(setFocused).toHaveBeenNthCalledWith(1, expected);
  });
});
