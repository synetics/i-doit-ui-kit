import { getLevelById } from './get-level-by-id';
import { MenuItems } from './types';

describe('Get level by id', () => {
  const data: MenuItems = {
    root: ['A', 'B'],
    A: ['C'],
    B: ['D'],
    C: [],
  };
  it.each([
    [data, 'non-existing', []],
    [data, 'root', ['A', 'B']],
    [data, 'A', ['C']],
  ])('returns the right result', (state: MenuItems, path: string, expected: string[]) => {
    const result = getLevelById(state, path);
    expect(result).toStrictEqual(expected);
  });
});
