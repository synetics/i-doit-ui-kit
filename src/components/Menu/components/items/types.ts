import { ElementType, KeyboardEvent, MouseEvent, MouseEventHandler, ReactNode } from 'react';

type Position = 'left' | 'right';

export type Item = {
  icon?: string;
  label: ReactNode;
  meta?: string | ReactNode;
  shortCut?: string;
  bulletColor?: string;
  iconPosition?: Position;
  disabled?: boolean;
  indicator?: ReactNode;
  indicatorPosition?: Position;
  subline?: string | ReactNode;
};

export type ItemProps = {
  className?: string;
  id: string;
  onMouseEnter?: MouseEventHandler;
  onMouseLeave?: MouseEventHandler;
  onSelect: <T extends Element>(event?: KeyboardEvent<T> | MouseEvent<T>) => void;
  role?: string;
  selected?: boolean;
  component?: ElementType;
  containerProps?: Record<string, unknown>;
  tabIndex?: number;
  focused?: boolean;
  multiSelect?: boolean;
  singleSelect?: boolean;
  children?: ReactNode;
} & Item;
