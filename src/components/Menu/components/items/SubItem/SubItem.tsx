import {
  BaseSyntheticEvent,
  forwardRef,
  KeyboardEventHandler,
  MouseEvent,
  MouseEventHandler,
  useCallback,
  useEffect,
  useState,
} from 'react';
import classnames from 'classnames';
import { useDebouncedValue } from '@mantine/hooks';
import { Icon } from '../../../../Icon';
import { chevron_right as chevronRight } from '../../../../../icons';
import { Popper, ScrollParentModifier } from '../../../../Popper';
import { OverflowWithTooltip } from '../../../../OverflowWithTooltip';
import { SubLine } from '../../../../List/Item';
import { handleKey, stopPropagation, withStopPropagation } from '../../../../../utils/helpers';
import { useMenuItemState } from '../../../context';
import { MenuProps, SubMenu } from '../../../SubMenu';
import itemStyles from '../Item/Item.module.scss';
import { ItemProps } from '../types';
import { Label } from '../Label';

export type SubItemProps = {
  open: boolean;
  onClick?: () => void;
  onKeyDown?: (e: BaseSyntheticEvent) => void;
  onKeyUp?: (e: BaseSyntheticEvent) => void;
  subMenuProps?: MenuProps;
} & ItemProps;

const submenuPopperOptions = {
  placement: 'right-end' as const,
  modifiers: [
    ScrollParentModifier,
    {
      name: 'offset',
      options: {
        offset: [5, 0],
      },
    },
  ],
};

const SubItem = forwardRef<HTMLLIElement, SubItemProps>(
  (
    {
      children,
      className,
      id,
      onMouseEnter,
      onMouseLeave,
      open,
      role = 'menuitem',
      label,
      icon,
      onClick,
      onKeyDown,
      onKeyUp,
      iconPosition = 'left',
      indicator,
      indicatorPosition = 'left',
      disabled,
      meta,
      subline,
      subMenuProps,
    },
    ref,
  ) => {
    const { isFocused, ref: localRef, setRef, focus } = useMenuItemState(id, ref);
    const metaOutput = meta && <div className={classnames(itemStyles.shortcut, 'text-right')}>{meta}</div>;
    const [pressed, setPressed] = useState(false);
    const [showSubMenu, setIsShowSubMenu] = useState(false);
    const [isMouseHover, setIsMouseHover] = useState(false);
    const opened = (isFocused && showSubMenu) || isMouseHover || open;
    const [closing] = useDebouncedValue(opened, 200);
    const isOpen = opened || closing;
    const handleMouseEnter: MouseEventHandler<HTMLDivElement> = (e) => {
      setIsShowSubMenu(true);
      setIsMouseHover(true);
      if (onMouseEnter) {
        onMouseEnter(e);
      }
    };
    const handleMouseLeave: MouseEventHandler<HTMLDivElement> = (e) => {
      setIsShowSubMenu(false);
      setIsMouseHover(false);
      if (onMouseLeave) {
        onMouseLeave(e);
      }
    };

    const handleOnClick: MouseEventHandler<HTMLLIElement> = () => {
      if (onClick) {
        onClick();
      }
    };

    const handlePress = useCallback(
      (value: boolean) => {
        if (disabled) {
          return;
        }
        setPressed(value);
      },
      [setPressed, disabled],
    );

    const handleClose = useCallback(
      (e: BaseSyntheticEvent) => {
        e.stopPropagation();
        setIsShowSubMenu(false);
        focus();
        localRef?.current?.focus();
      },
      [focus, localRef],
    );

    useEffect(() => setIsShowSubMenu(false), [isFocused]);

    const handleKeyDown: KeyboardEventHandler<HTMLLIElement> = (e) => {
      if (e.key === 'ArrowRight') {
        setIsShowSubMenu(true);
      }

      if (e.key === 'Enter') {
        setPressed(true);
        setIsShowSubMenu(!isOpen);
      }
      if (onKeyDown) {
        onKeyDown(e);
      }
    };

    /* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
    return (
      <div
        onMouseEnter={!disabled ? handleMouseEnter : undefined}
        onMouseLeave={!disabled ? handleMouseLeave : undefined}
      >
        <li
          id={id}
          role={role}
          tabIndex={-1}
          onKeyDown={!disabled ? handleKeyDown : undefined}
          onKeyUp={
            disabled
              ? stopPropagation
              : (e) => {
                  handleKey({
                    Enter: () => {
                      setPressed(false);
                    },
                  })(e);
                  if (onKeyUp) {
                    onKeyUp(e);
                  }
                }
          }
          className={classnames(itemStyles.item, className, {
            [itemStyles.hover]: isFocused,
            [itemStyles.pressed]: pressed,
            [itemStyles.disabled]: disabled,
          })}
          ref={setRef}
          onClick={!disabled ? handleOnClick : undefined}
          onMouseDown={withStopPropagation((e: MouseEvent) => {
            e.preventDefault();
            if (e.button !== 2) {
              handlePress(true);
            }
          })}
          onMouseUp={withStopPropagation(() => handlePress(false))}
        >
          {indicator && indicatorPosition === 'left' && indicator}
          {icon && iconPosition === 'left' && <Icon src={icon} />}
          <div className="flex-grow-1">
            <OverflowWithTooltip tooltipContent={label as string} showTooltip={isFocused && !isOpen}>
              <Label className={itemStyles.twoLines} label={label} />
            </OverflowWithTooltip>
            {subline && <SubLine>{subline}</SubLine>}
          </div>
          {metaOutput}
          {icon && iconPosition === 'right' && <Icon src={icon} />}
          {indicator && indicatorPosition === 'right' && indicator}
          <Icon src={chevronRight} />
        </li>
        {isOpen && (
          <Popper open options={submenuPopperOptions} referenceElement={localRef.current} keyStopPropagation={false}>
            <SubMenu {...subMenuProps} id={id} onClose={disabled ? undefined : handleClose}>
              {children}
            </SubMenu>
          </Popper>
        )}
      </div>
    );
  },
);

SubItem.displayName = 'SubItem';

export { SubItem };
