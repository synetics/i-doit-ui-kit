import { fireEvent, render, screen } from '@testing-library/react';
import { Status } from '../../../../List/Item';
import { info } from '../../../../../icons';
import { Menu } from '../../../Menu';
import { SubItem } from './SubItem';

describe('SubItem', () => {
  const fn = jest.fn();
  const label = 'Test';

  it('renders with content inside without crashing', () => {
    render(
      <SubItem id="test" onSelect={fn} label={label} open={false}>
        <div>Child</div>
      </SubItem>,
    );

    fireEvent.mouseEnter(screen.getByText(label));
    fireEvent.mouseLeave(screen.getByText(label));
    expect(screen.getByText(label)).toBeInTheDocument();
  });

  it('does not render child if subitem is not opened', () => {
    render(
      <SubItem id="test" onSelect={fn} label={label} open={false}>
        <div>Child</div>
      </SubItem>,
    );

    expect(screen.queryByText('Child')).not.toBeInTheDocument();
  });

  it('renders with children when open', () => {
    render(
      <SubItem id="test" onSelect={fn} label={label} open>
        <div>Child</div>
      </SubItem>,
    );

    expect(screen.queryByText('Child')).toBeInTheDocument();
  });

  it('renders with custom role', () => {
    render(<SubItem id="test" onSelect={fn} label={label} open={false} role="listitem" />);

    expect(screen.queryByRole('listitem')).toBeInTheDocument();
  });

  it('renders with an icon', () => {
    render(<SubItem id="test" onSelect={fn} label={label} open={false} icon="bla" />);

    const icons = screen.queryAllByTestId('icon').filter((a) => a.dataset.src === 'bla');
    expect(icons).toHaveLength(1);
  });

  it('adds hover classname and calls function onMouseEnter', () => {
    const onEnter = jest.fn();
    const onLeave = jest.fn();

    render(
      <SubItem id="test" onMouseEnter={onEnter} onMouseLeave={onLeave} onSelect={fn} label={label} open={false} />,
    );
    expect(screen.getByText(label)).toBeInTheDocument();

    fireEvent.mouseEnter(screen.getByText(label));
    fireEvent.mouseLeave(screen.getByText(label));

    expect(onEnter).toHaveBeenCalledTimes(1);
    expect(onLeave).toHaveBeenCalledTimes(1);
  });

  it('calls function onKeyDown, onClick and onKeyUp', () => {
    const onKeyDown = jest.fn();
    const onKeyUp = jest.fn();
    const onClick = jest.fn();
    render(
      <SubItem
        id="test"
        onSelect={fn}
        onKeyDown={onKeyDown}
        onKeyUp={onKeyUp}
        onClick={onClick}
        label={label}
        open={false}
      />,
    );
    expect(screen.getByText(label)).toBeInTheDocument();

    fireEvent.keyDown(screen.getByText(label), { key: 'Enter' });
    fireEvent.keyUp(screen.getByText(label), { key: 'Enter' });
    fireEvent.click(screen.getByText(label));

    expect(onKeyDown).toHaveBeenCalledTimes(1);
    expect(onKeyUp).toHaveBeenCalledTimes(1);
    expect(onClick).toHaveBeenCalledTimes(1);
  });

  it('handle ArrowRight and ArrowLeft', () => {
    const onKeyDownMock = jest.fn();
    render(
      <Menu>
        <SubItem label="Test" onKeyDown={onKeyDownMock} id="test-id" onSelect={() => {}} open={false}>
          <div>Hello</div>
        </SubItem>
      </Menu>,
    );
    expect(screen.queryByText('Hello')).not.toBeInTheDocument();

    fireEvent.focus(screen.getByRole('menuitem'));
    fireEvent.keyDown(screen.getByRole('menuitem'), { key: 'ArrowRight' });
    expect(onKeyDownMock).toHaveBeenCalled();
    expect(screen.getByText('Hello')).toBeInTheDocument();
    fireEvent.keyDown(screen.getByText('Test'), { key: 'ArrowLeft' });
    expect(onKeyDownMock).toHaveBeenCalled();
  });

  it('not calls function onKeyUp', () => {
    const onKeyUp = jest.fn();
    render(<SubItem disabled id="test" onSelect={fn} onKeyUp={onKeyUp} label={label} open={false} />);
    expect(screen.getByText(label)).toBeInTheDocument();
    fireEvent.keyUp(screen.getByText(label));
    expect(onKeyUp).not.toHaveBeenCalled();
  });
  it('render meta, indicator and subline', () => {
    render(
      <SubItem
        disabled
        id="test"
        meta="meta"
        onSelect={fn}
        label={label}
        open={false}
        indicator={<Status bulletColor="#378069" data-testid="bullet" />}
        subline="subline"
      />,
    );
    expect(screen.getByText('meta')).toBeInTheDocument();
    expect(screen.getByTestId('bullet')).toBeInTheDocument();
    expect(screen.getByText('subline')).toBeInTheDocument();
  });
  it('render right position icon und indicator', () => {
    render(
      <SubItem
        disabled
        id="test"
        indicatorPosition="right"
        iconPosition="right"
        onSelect={fn}
        label={label}
        open={false}
        icon={info}
        indicator={<Status bulletColor="#378069" data-testid="bullet" />}
        subline="subline"
      />,
    );
    const icons = screen.getAllByTestId('icon');
    expect(screen.getByTestId('bullet')).toBeInTheDocument();
    expect(icons[0]).toHaveAttribute('data-src', info);
    expect(screen.getByText('subline')).toBeInTheDocument();
  });
  it('should not update the pressed state when disabled', () => {
    const { getByRole } = render(<SubItem id="subitem" open={false} label="subitem" disabled onSelect={jest.fn()} />);

    const liElement = getByRole('menuitem');

    fireEvent.mouseDown(liElement);
    expect(liElement).not.toHaveClass('pressed');
  });
});
