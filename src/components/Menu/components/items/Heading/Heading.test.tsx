import { render, screen } from '@testing-library/react';
import { Heading } from './Heading';

describe('Heading', () => {
  it('renders with content inside without crashing', () => {
    const label = 'Test';

    render(<Heading label={label} />);

    expect(screen.getByText(label)).toBeInTheDocument();
  });
});
