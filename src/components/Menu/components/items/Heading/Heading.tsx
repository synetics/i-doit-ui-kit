import classnames from 'classnames';
import { forwardRef } from 'react';
import styles from './Heading.module.scss';

type HeadingProps = {
  label: string;
  className?: string;
};

const Heading = forwardRef<HTMLLIElement, HeadingProps>(({ className, label }, ref) => (
  <li className={classnames(styles.heading, className)} role="heading" aria-level={3} ref={ref}>
    {label}
  </li>
));

Heading.displayName = 'Heading';

export { Heading };
