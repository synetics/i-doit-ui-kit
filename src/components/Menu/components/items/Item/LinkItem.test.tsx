import { act, fireEvent, render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { LinkItem } from './LinkItem';

describe('LinkItem', () => {
  const fn = jest.fn();

  it('renders with content inside without crashing', () => {
    const label = 'Test';

    render(<LinkItem href="/test" id="test" onSelect={fn} label={label} />);

    expect(screen.getByText(label)).toBeInTheDocument();
  });

  it('calls onSelect when event has no meta or ctrl key', async () => {
    const label = 'Test';

    render(<LinkItem href="/test" id="test" onSelect={fn} label={label} />);

    const element = screen.getByRole('menuitem');
    act(() => element.focus());

    await act(() =>
      fireEvent.keyUp(element, {
        key: 'Enter',
        code: 'Enter',
        keyCode: 13,
        charCode: 13,
      }),
    );

    expect(screen.getByText(label)).toBeInTheDocument();
    expect(fn).toHaveBeenCalledTimes(1);
  });

  it('should not call onSelect when metaKey or ctrlKey is pressed', async () => {
    const onSelectMock = jest.fn();
    render(<LinkItem href="/test" id="test" onSelect={fn} label="Link Text" />);
    const link = screen.getByText('Link Text');

    await act(() => fireEvent.click(link, { metaKey: true }));

    expect(onSelectMock).not.toHaveBeenCalled();

    await act(() => fireEvent.click(link, { ctrlKey: true }));

    expect(onSelectMock).not.toHaveBeenCalled();
  });

  it('ignores onSelect when event has meta or ctrl key', async () => {
    const label = 'Test';

    render(<LinkItem href="/test" id="test" onSelect={fn} label={label} />);

    const element = screen.getByRole('menuitem');
    act(() => element.focus());

    await userEvent.type(element, '{meta>}{enter}{/meta}', { skipClick: true });

    expect(screen.getByText(label)).toBeInTheDocument();
    expect(fn).not.toHaveBeenCalled();
  });
});
