import { forwardRef, useEffect, useRef } from 'react';
import classnames, { Value } from 'classnames';
import { useMenuItemState } from '../../../context';
import { SearchField } from '../../../../Form/Control/SearchField';
import { ItemProps } from '../types';
import styles from './SearchItem.module.scss';

type SearchItemProps = { placeholder?: string; onChange?: (v: Value) => void } & Omit<ItemProps, 'label' | 'onSelect'>;

const SearchItem = forwardRef<HTMLLIElement, SearchItemProps>(
  ({ className, id, role = 'menuitem', selected = false, component, ...rest }, ref) => {
    const { isFocused, setRef, focus } = useMenuItemState(id, ref);
    const searchRef = useRef<HTMLInputElement>(null);
    const Content = component || 'li';

    useEffect(() => {
      if (isFocused) {
        searchRef.current?.querySelector('input')?.focus();
      }
    }, [isFocused]);

    return (
      <Content
        id={id}
        data-testid={id}
        role={role}
        ref={setRef}
        tabIndex={-1}
        aria-selected={selected}
        onClick={() => focus()}
        onFocus={() => focus()}
        className={classnames(styles.item, className)}
      >
        <SearchField id={id} ref={searchRef} {...rest} />
      </Content>
    );
  },
);

export { SearchItem };
