import { forwardRef, KeyboardEvent, useCallback, useState } from 'react';
import classnames from 'classnames';
import { handleKey, withStopPropagation } from '../../../../../utils/helpers';
import { OverflowWithTooltip } from '../../../../OverflowWithTooltip';
import { SubLine } from '../../../../List/Item';
import { Shortcut } from '../../../../Shortcut';
import { Icon } from '../../../../Icon';
import { useMenuItemState } from '../../../context';
import { Checkbox } from '../../../../Form/Control';
import { check } from '../../../../../icons';
import { Label } from '../Label';
import { ItemProps } from '../types';
import styles from './Item.module.scss';

const Item = forwardRef<HTMLLIElement, ItemProps>(
  (
    {
      className,
      id,
      onSelect,
      role = 'menuitem',
      selected = false,
      icon,
      label,
      meta,
      shortCut,
      component,
      iconPosition = 'left',
      indicator,
      indicatorPosition = 'left',
      disabled,
      subline,
      multiSelect,
      singleSelect,
      ...rest
    },
    ref,
  ) => {
    const { isFocused, setRef } = useMenuItemState(id, ref);
    const [tooltipShow, setTooltip] = useState(false);
    const metaOutput = meta && <div className={classnames(styles.shortcut, 'text-right')}>{meta}</div>;
    const shortCutOutput = shortCut && <Shortcut className="text-right" onSelect={onSelect} shortCut={shortCut} />;
    const Content = component || 'li';
    const [pressed, setPressed] = useState(false);
    const handlePress = useCallback(
      (value: boolean) => {
        if (disabled) {
          return;
        }
        setPressed(value);
      },
      [setPressed, disabled],
    );

    return (
      <Content
        id={id}
        data-testid={id}
        onMouseLeave={() => setTooltip(false)}
        onMouseOver={() => setTooltip(true)}
        role={role}
        ref={setRef}
        tabIndex={-1}
        aria-selected={selected}
        onClick={() => {
          if (onSelect) {
            onSelect();
          }
        }}
        onMouseDown={withStopPropagation((e: MouseEvent) => {
          e.preventDefault();
          if (e.button !== 2) {
            handlePress(true);
          }
        })}
        onMouseUp={withStopPropagation(() => handlePress(false))}
        onKeyDown={handleKey({
          Enter: withStopPropagation(() => handlePress(true)),
        })}
        onKeyUp={handleKey({
          Enter: withStopPropagation((e: KeyboardEvent) => {
            handlePress(false);
            if (onSelect) {
              onSelect(e);
            }
          }),
        })}
        className={classnames(styles.item, className, {
          [styles.selected]: selected,
          [styles.disabled]: disabled,
          [styles.pressed]: pressed,
        })}
        {...rest}
      >
        {multiSelect && (
          <Checkbox
            aria-label="checkbox"
            tabIndex={-1}
            className={styles.checkbox}
            checked={selected}
            onChange={() => {}}
            disabled={disabled}
          />
        )}
        {singleSelect && (
          <Icon
            className={classnames(styles.selectItem, {
              [styles.disabledIcon]: selected && disabled,
              [styles.selectedSingle]: selected,
            })}
            src={check}
          />
        )}
        {indicator && indicatorPosition === 'left' && indicator}
        {icon && iconPosition === 'left' && <Icon className={styles.icon} src={icon} />}
        <div className={styles.text}>
          <OverflowWithTooltip tooltipContent={label as string} showTooltip={tooltipShow || isFocused}>
            <Label className={styles.twoLines} label={label} />
          </OverflowWithTooltip>
          {subline && <SubLine>{subline}</SubLine>}
        </div>
        {metaOutput || shortCutOutput}
        {icon && iconPosition === 'right' && <Icon className={styles.icon} src={icon} />}
        {indicator && indicatorPosition === 'right' && indicator}
      </Content>
    );
  },
);

export { Item };
