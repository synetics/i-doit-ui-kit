import { fireEvent, render, screen } from '@testing-library/react';
import { Menu } from '../../../Menu';
import { SearchItem } from './SearchItem';

describe('SearchItem', () => {
  it('renders with content inside without crashing', () => {
    render(<SearchItem id="test" />);

    expect(screen.getByRole('textbox')).toBeInTheDocument();
  });

  it('check focus on click on Item', () => {
    render(
      <Menu>
        <SearchItem id="test" />
      </Menu>,
    );
    const item = screen.getByRole('menuitem');

    fireEvent.click(item);
    expect(screen.getByRole('textbox')).toHaveFocus();
  });
});
