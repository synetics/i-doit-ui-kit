import { fireEvent, render, screen } from '@testing-library/react';
import { Status } from '../../../../List/Item';
import { info } from '../../../../../icons';
import { Item } from './Item';

describe('Item', () => {
  const fn = jest.fn();
  const label = 'Test';

  it('renders with content inside without crashing', () => {
    render(<Item id="test" onSelect={fn} label={label} />);

    fireEvent.mouseEnter(screen.getByText(label));
    fireEvent.mouseLeave(screen.getByText(label));

    expect(screen.getByText(label)).toBeInTheDocument();
  });

  it('renders with meta', () => {
    render(<Item id="test" onSelect={fn} label={label} meta="meta-text" />);

    expect(screen.getByText('meta-text')).toBeInTheDocument();
  });

  it('renders with shortcut', () => {
    render(<Item id="test" onSelect={fn} label={label} shortCut="A" />);

    expect(screen.getByTestId('i-shortcut')).toBeInTheDocument();
  });

  it('renders with custom role', () => {
    render(<Item id="test" onSelect={fn} label={label} role="listitem" />);

    expect(screen.getByRole('listitem')).toBeInTheDocument();
  });

  it('adds hover classname and calls function onMouseEnter', () => {
    const onEnter = jest.fn();
    const onLeave = jest.fn();
    render(<Item id="test" onMouseEnter={onEnter} onMouseLeave={onLeave} onSelect={fn} label={label} />);
    expect(screen.getByText(label)).toBeInTheDocument();

    fireEvent.mouseEnter(screen.getByText(label));
    fireEvent.mouseLeave(screen.getByText(label));

    expect(onLeave).toHaveBeenCalledTimes(1);
  });

  it('check pressed class on mouse down', () => {
    render(<Item disabled id="test" onSelect={fn} label={label} />);
    expect(screen.getByText(label)).toBeInTheDocument();
    const item = screen.getByTestId('test');
    fireEvent.mouseDown(item);

    expect(screen.getByTestId('test')).not.toHaveClass('pressed');
  });

  it('render meta, indicator and subline, multiselect', () => {
    render(
      <Item
        disabled
        multiSelect
        id="test"
        meta="meta"
        onSelect={fn}
        label={label}
        indicator={<Status bulletColor="#378069" data-testid="bullet" />}
        subline="subline"
      />,
    );
    expect(screen.getByText('meta')).toBeInTheDocument();
    expect(screen.getByTestId('bullet')).toBeInTheDocument();
    expect(screen.getByText('subline')).toBeInTheDocument();
    expect(screen.getByRole('checkbox')).toBeInTheDocument();
  });

  it('render right position icon und indicator', () => {
    render(
      <Item
        disabled
        id="test"
        indicatorPosition="right"
        iconPosition="right"
        onSelect={fn}
        label={label}
        icon={info}
        indicator={<Status bulletColor="#378069" data-testid="bullet" />}
        subline="subline"
      />,
    );
    const icons = screen.getAllByTestId('icon');
    expect(screen.getByTestId('bullet')).toBeInTheDocument();
    expect(icons[0]).toHaveAttribute('data-src', info);
    expect(screen.getByText('subline')).toBeInTheDocument();
  });
});
