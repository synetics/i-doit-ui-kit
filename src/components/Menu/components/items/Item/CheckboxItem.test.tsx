import { fireEvent, render, screen } from '@testing-library/react';
import { CheckboxItem } from './CheckBoxItem';

describe('CheckboxItem', () => {
  const fn = jest.fn();

  it('renders with content inside without crashing', () => {
    const label = 'Test';

    render(<CheckboxItem id="test" onSelect={fn} label={label} />);

    expect(screen.getByRole('checkbox')).toBeInTheDocument();
  });

  it('call onSelect', () => {
    const label = 'Test';

    render(<CheckboxItem id="test" onSelect={fn} label={label} />);
    const checkbox = screen.getByRole('checkbox');
    expect(checkbox).toBeInTheDocument();
    fireEvent.click(checkbox);
    expect(fn).toHaveBeenCalledTimes(1);
  });
});
