import { forwardRef } from 'react';
import { Checkbox } from '../../../../Form/Control';
import { ItemProps } from '../types';
import { Item } from './Item';

const CheckboxItem = forwardRef<HTMLLIElement, ItemProps>(({ selected, onSelect, label, ...props }, ref) => {
  const newLabel = (
    <div className="d-flex align-items-center">
      <div className="mr-2">
        <Checkbox aria-label={label as string} checked={selected} onChange={() => onSelect()} />
      </div>
      {label}
    </div>
  );

  return <Item selected={selected} ref={ref} label={newLabel} onSelect={onSelect} {...props} />;
});

CheckboxItem.displayName = 'CheckboxItem';

export { CheckboxItem };
