import { forwardRef } from 'react';
import { ItemProps } from '../types';
import { Item } from './Item';

export type LinkItemProps = {
  href: string;
} & ItemProps;

const LinkItem = forwardRef<HTMLLIElement, LinkItemProps>(({ onSelect, href, ...props }, ref) => {
  const handleSelect: ItemProps['onSelect'] = (e) => {
    if (!e || e.metaKey || e.ctrlKey) {
      return;
    }

    e.preventDefault();
    onSelect(e);
  };

  return <Item {...props} ref={ref} onSelect={handleSelect} component="a" containerProps={{ href }} />;
});

LinkItem.displayName = 'LinkItem';

export { LinkItem };
