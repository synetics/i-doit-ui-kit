/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import { forwardRef } from 'react';
import classnames from 'classnames';
import { ItemProps } from '../types';
import { handleKey } from '../../../../../utils/helpers';
import { Label } from '../Label';
import { Icon } from '../../../../Icon';
import { handleAction } from '../../../utils';
import styles from '../Item/Item.module.scss';
import { useMenuItemState } from '../../../context';

export type ActionItemProps = {
  onlyShowIconOnHover?: boolean;
  iconAction?: () => void;
  secondIcon?: ItemProps['icon'];
  secondIconAction?: (...args: unknown[]) => void;
} & ItemProps;

const ActionItem = forwardRef<HTMLLIElement, ActionItemProps>(
  (
    {
      className,
      id,
      onSelect,
      role = 'menuitem',
      selected,
      onlyShowIconOnHover,
      iconAction,
      secondIconAction,
      label,
      icon,
      secondIcon,
    },
    ref,
  ) => {
    const { setRef, focus, leave, isFocused } = useMenuItemState(id, ref);
    return (
      <li
        role={role}
        className={classnames(styles.item, className, {
          [styles['only-show-icon-on-hover']]: onlyShowIconOnHover,
          [styles.selected]: selected,
          [styles.hover]: isFocused,
        })}
        id={id}
        onClick={onSelect}
        tabIndex={-1}
        onKeyDown={handleKey({ Enter: onSelect })}
        onMouseEnter={focus}
        onMouseLeave={leave}
        ref={setRef}
      >
        <Label label={label} className="text-truncate" />
        {icon && (
          <Icon
            className={classnames('ml-1', styles['i-icon'])}
            src={icon}
            onClick={iconAction && handleAction(iconAction)}
          />
        )}
        {secondIcon && (
          <Icon
            className={classnames('ml-1', styles['i-icon'])}
            src={secondIcon}
            onClick={secondIconAction && handleAction(secondIconAction)}
          />
        )}
      </li>
    );
  },
);

ActionItem.displayName = 'ActionItem';

export { ActionItem };
