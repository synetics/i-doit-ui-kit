import { render, screen } from '@testing-library/react';
import { ActionItem } from './ActionItem';

describe('ActionItem', () => {
  const fn = jest.fn();
  const icon = 'image/src';

  it('renders with content inside without crashing', () => {
    const label = 'Test';

    render(<ActionItem id="test" onSelect={fn} label={label} role="menuitem" />);

    expect(screen.getByText(label)).toBeInTheDocument();
  });

  it('renders item with icon', () => {
    const label = 'Test';

    render(<ActionItem id="test" onSelect={fn} label={label} icon={icon} />);

    expect(screen.getByText(label)).toBeInTheDocument();
    expect(screen.getByTestId('icon')).toBeInTheDocument();
  });

  it('renders item with second icon', () => {
    const label = 'Test';

    render(<ActionItem id="test" onSelect={fn} label={label} secondIcon={icon} />);

    expect(screen.getByText(label)).toBeInTheDocument();
    expect(screen.getByTestId('icon')).toBeInTheDocument();
  });
});
