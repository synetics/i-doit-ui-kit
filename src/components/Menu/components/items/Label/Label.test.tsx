import { render, screen } from '@testing-library/react';
import { Label } from './Label';

describe('Label', () => {
  it('renders with content inside without crashing', () => {
    const label = 'Test';

    render(<Label label={label} />);

    expect(screen.getByText(label)).toBeInTheDocument();
  });
});
