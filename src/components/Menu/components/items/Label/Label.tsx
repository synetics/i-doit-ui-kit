import classnames from 'classnames';
import { ReactNode } from 'react';
import styles from './Label.module.scss';

export type LabelProps = { label?: ReactNode; className?: string; selected?: boolean };

const Label = ({ label, className, selected }: LabelProps) => (
  <div className={classnames(styles.label, { [styles['selected-label']]: selected }, className)}>{label}</div>
);

export { Label };
