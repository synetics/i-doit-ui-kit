/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import { forwardRef } from 'react';
import classnames from 'classnames';
import { Label } from '../Label';
import { ItemProps } from '../types';
import { Icon } from '../../../../Icon';
import { Button } from '../../../../Button';
import { handleAction } from '../../../utils';
import { handleKey } from '../../../../../utils/helpers';
import styles from '../Item/Item.module.scss';

export type ActionHighlightItemProps = {
  onlyShowIconOnHover?: boolean;
  iconAction?: () => void;
  secondIcon?: ItemProps['icon'];
  secondIconAction?: (...args: unknown[]) => void;
  highlightIcon?: ItemProps['icon'];
} & ItemProps;

const ActionHighlightItem = forwardRef<HTMLLIElement, ActionHighlightItemProps>(
  (
    {
      className,
      id,
      onMouseEnter,
      onSelect,
      role = 'menuitem',
      selected = false,
      highlightIcon,
      label,
      onlyShowIconOnHover,
      secondIcon,
      secondIconAction,
    },
    ref,
  ) => (
    <li
      className={classnames(styles.item, className, {
        'only-show-icon-on-hover': onlyShowIconOnHover,
        selected,
      })}
      id={id}
      onClick={onSelect}
      onKeyDown={handleKey({ Enter: onSelect })}
      onMouseEnter={onMouseEnter}
      ref={ref}
      role={role}
    >
      <Label className="text-truncate" label={label} />
      {secondIcon && (
        <Icon
          className="ml-1 action-icon"
          src={secondIcon}
          onClick={secondIconAction && handleAction(secondIconAction)}
        />
      )}
      <Button
        icon={highlightIcon ? <Icon fill="white" height="16" src={highlightIcon} width="16" /> : undefined}
        onClick={onSelect}
      />
    </li>
  ),
);

ActionHighlightItem.displayName = 'ActionHighlightItem';
export { ActionHighlightItem };
