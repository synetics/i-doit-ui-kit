import { render, screen } from '@testing-library/react';
import { ActionHighlightItem } from './ActionHighlightItem';

describe('ActionHighlightItem', () => {
  const fn = jest.fn();
  const icon = 'image/src';

  it('renders with content inside without crashing', () => {
    const label = 'Test';

    render(<ActionHighlightItem id="test" onSelect={fn} label={label} role="menuitem" selected />);

    expect(screen.getByText(label)).toBeInTheDocument();
  });

  it('renders item with highlight icon', () => {
    const label = 'Test';

    render(<ActionHighlightItem id="test" onSelect={fn} label={label} highlightIcon={icon} />);

    expect(screen.getByText(label)).toBeInTheDocument();
    expect(screen.getByTestId('icon')).toBeInTheDocument();
  });

  it('renders item with second icon', () => {
    const label = 'Test';

    render(<ActionHighlightItem id="test" onSelect={fn} label={label} secondIcon={icon} />);

    expect(screen.getByText(label)).toBeInTheDocument();
    expect(screen.getByTestId('icon')).toBeInTheDocument();
  });
});
