import { render, screen } from '@testing-library/react';
import { Separator } from './Separator';

describe('Separator', () => {
  it('renders with content inside without crashing', () => {
    render(<Separator />);

    expect(screen.getByRole('separator')).toBeInTheDocument();
  });
});
