import { forwardRef } from 'react';
import classnames from 'classnames';
import styles from './Separator.module.scss';

const Separator = forwardRef<HTMLLIElement>((_, ref) => (
  <li className={classnames(styles.item, styles['i-menu-list-separator'])} role="separator" ref={ref} />
));

Separator.displayName = 'Separator';

export { Separator };
