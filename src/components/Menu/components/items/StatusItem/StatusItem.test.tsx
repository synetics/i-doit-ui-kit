import { render, screen } from '@testing-library/react';
import itemStyles from '../Item/Item.module.scss';
import { StatusItem } from './StatusItem';

describe('StatusItem', () => {
  const fn = jest.fn();
  const label = 'Test';

  it('renders with content inside without crashing', () => {
    render(<StatusItem id="test" onSelect={fn} label={label} />);

    expect(screen.getByText(label)).toBeInTheDocument();
  });

  it('renders with custom role', () => {
    render(<StatusItem id="test" onSelect={fn} label={label} role="listitem" />);

    expect(screen.getByRole('listitem')).toBeInTheDocument();
  });

  it('renders with selected class', () => {
    render(<StatusItem id="test" onSelect={fn} label={label} selected />);

    expect(screen.getByRole('menuitem')).toHaveClass(itemStyles.selected);
  });
});
