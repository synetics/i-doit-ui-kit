/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import classnames from 'classnames';
import { forwardRef } from 'react';
import { handleKey } from '../../../../../utils/helpers';
import { Label } from '../Label';
import { ItemProps } from '../types';
import itemStyles from '../Item/Item.module.scss';
import styles from './StatusItem.module.scss';

const StatusItem = forwardRef<HTMLLIElement, ItemProps>(
  ({ className, id, onMouseEnter, label, bulletColor, onSelect, role = 'menuitem', selected = false }, ref) => (
    <li
      className={classnames(itemStyles.item, className, {
        [itemStyles.selected]: selected,
      })}
      id={id}
      onClick={onSelect}
      onKeyDown={handleKey({ Enter: onSelect })}
      onMouseEnter={onMouseEnter}
      ref={ref}
      role={role}
    >
      {/* todo extract a component to share the styles (check at least the ColorPicker component for details) */}
      <div
        className={styles['status-item-status']}
        style={{ backgroundColor: bulletColor }}
        data-testid="status-color"
      />
      <Label label={label} />
    </li>
  ),
);

StatusItem.displayName = 'StatusItem';

export { StatusItem };
