import { render, screen } from '@testing-library/react';
import user from '@testing-library/user-event';
import { useFocus } from '../../hooks';
import { SubMenu } from './SubMenu';
import { Item } from './components';

jest.mock('../../hooks/use-focus', () => ({ useFocus: jest.fn() }));

describe('SubMenu', () => {
  it('renders with content inside without crashing', () => {
    render(
      <SubMenu id="id" autoFocus>
        <Item id="test-item" label="{Label}" onSelect={jest.fn()} />
      </SubMenu>,
    );

    expect(useFocus).toHaveBeenCalledWith(true);
    expect(screen.getByRole('button')).toBeInTheDocument();
  });
  it('renders with custom role', () => {
    render(<SubMenu id="id" autoFocus={false} role="button" />);

    expect(useFocus).toHaveBeenCalledWith(false);
    expect(screen.getByRole('button')).toBeInTheDocument();
  });
  it('check onClose', async () => {
    const onClose = jest.fn();
    render(
      <SubMenu id="id" autoFocus onClose={onClose}>
        <Item id="test-item" label="{Label}" onSelect={() => {}} />
      </SubMenu>,
    );
    await user.type(screen.getByRole('button'), '{arrowleft}');
    expect(onClose).toHaveBeenCalled();
  });
});
