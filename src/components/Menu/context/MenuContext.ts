import {
  createContext,
  ForwardedRef,
  MutableRefObject,
  RefCallback,
  useCallback,
  useContext,
  useEffect,
  useMemo,
} from 'react';
import { useGlobalizedRef } from '../../../hooks';
import { PathContext } from './PathContext';

type MenuContextType = {
  register: (path: string[]) => void;
  unRegister: (path: string[]) => void;
  focus: (path: string[]) => void;
  isFocused: (id: string) => boolean;
};

const MenuContext = createContext<MenuContextType | null>(null);

const defaultMenu: MenuContextType = {
  register: () => {},
  unRegister: () => {},
  focus: () => {},
  isFocused: () => false,
};

const useMenuItemState = <T extends HTMLElement>(
  id: string,
  ref: ForwardedRef<T>,
): {
  focus: () => void;
  leave: () => void;
  isFocused: boolean;
  ref: MutableRefObject<T | null>;
  setRef: RefCallback<T>;
} => {
  const { localRef, setRefsCallback: setRef } = useGlobalizedRef(ref);
  const { register, focus, isFocused, unRegister } = useContext(MenuContext) || defaultMenu;
  const path = useContext(PathContext);
  const currentPath = useMemo(() => [...path, id], [path, id]);
  const setFocus = useCallback(() => focus(currentPath), [currentPath, focus]);
  const leave = useCallback(() => focus(path), [path, focus]);
  const isItemFocused = useMemo(() => isFocused(id), [isFocused, id]);

  useEffect(() => {
    register(currentPath);

    return () => unRegister(currentPath);
  }, [currentPath, register, unRegister]);

  useEffect(() => {
    if (isItemFocused && localRef.current) {
      localRef.current.focus();
    }
  }, [localRef, isItemFocused]);

  return {
    focus: setFocus,
    leave,
    isFocused: isItemFocused,
    ref: localRef,
    setRef,
  } as const;
};

export { MenuContextType, MenuContext, useMenuItemState };
