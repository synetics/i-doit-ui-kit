import { createContext } from 'react';

const PathContext = createContext<string[]>([]);
export { PathContext };
