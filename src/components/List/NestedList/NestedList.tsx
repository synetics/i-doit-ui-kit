import React, { ReactNode } from 'react';
import classnames from 'classnames';

type NestedListType = {
  level?: number;
  'data-testid'?: string;
  className?: string;
  children?: ReactNode;
};

const NestedList = ({ level = 1, 'data-testid': testId, children, className }: NestedListType) => (
  <div data-testid={testId} className={classnames(className)} style={{ marginLeft: 32 * level }}>
    {children}
  </div>
);

export { NestedList };
