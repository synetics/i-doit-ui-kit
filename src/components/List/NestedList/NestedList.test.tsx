import { render, screen } from '@testing-library/react';
import { NestedList } from './NestedList';

describe('NestedList', () => {
  it('renders without crashing', () => {
    render(
      <NestedList data-testid="NestedList">
        <div>Test</div>
      </NestedList>,
    );
    const list = screen.getByTestId('NestedList');
    expect(list).toHaveAttribute('data-testid', 'NestedList');
    expect(list).toBeInTheDocument();
  });

  it('renders with level', () => {
    render(
      <NestedList data-testid="NestedList" level={4}>
        <div>Test</div>
      </NestedList>,
    );
    const list = screen.getByTestId('NestedList');
    expect(list).toBeInTheDocument();
    expect(list).toHaveStyle('margin-left: 128px');
  });

  it('renders without custom class', () => {
    render(<NestedList className="test" data-testid="NestedList" />);
    const list = screen.getByTestId('NestedList');
    expect(list).toHaveClass('test');
  });
});
