import React, { ReactNode } from 'react';
import classnames from 'classnames';
import { SelectorItem } from '../../Item';
import classes from './RangeItem.module.scss';

type SelectType = 'from' | 'to';

export type RangeItemType = {
  'aria-label': string;
  disabled?: boolean;
  className?: string;
  children?: ReactNode;
  selected: boolean;
  selectType: SelectType;
  start?: boolean;
  end?: boolean;
  hovered?: boolean;
  onSelect: () => void;
};

export const RangeItem = ({
  'aria-label': ariaLabel,
  selected,
  className,
  children,
  disabled,
  onSelect,
  hovered,
  start,
  end,
  selectType,
  ...rest
}: RangeItemType) => (
  <SelectorItem
    {...rest}
    aria-label={ariaLabel}
    className={classnames(
      classes.item,
      className,
      {
        [classes.disabled]: disabled,
        [classes.selected]: selected,
        [classes.edge]: selected && (start || end),
        [classes.start]: selected && start,
        [classes.end]: selected && end,
        [classes.hovered]: hovered,
      },
      classes[selectType],
    )}
    disabled={disabled}
    selected={selected}
    onSelect={onSelect}
  >
    {children}
  </SelectorItem>
);
