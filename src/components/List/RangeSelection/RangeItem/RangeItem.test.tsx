import { render, screen } from '@testing-library/react';
import user from '@testing-library/user-event';
import { RangeItem, RangeItemType } from './RangeItem';
import classes from './RangeItem.module.scss';

type SelectionType = 'from' | 'to';

describe('RangeItem', () => {
  const renderItem = (props: Partial<RangeItemType>) => {
    const onSelect = jest.fn();
    render(
      <RangeItem
        aria-label="Label"
        selected={false}
        selectType="from"
        onSelect={onSelect}
        {...props}
        data-testid="test-id"
      >
        Label
      </RangeItem>,
    );
    const element = screen.getByTestId('test-id');

    return {
      onSelect,
      element,
    };
  };
  it('renders without crashing', () => {
    const { element } = renderItem({});
    expect(element).toBeInTheDocument();
    expect(element).toHaveClass(classes.item, classes.from);
  });

  it('calls onSelect on click', async () => {
    const { element, onSelect } = renderItem({});
    await user.click(element);
    expect(onSelect).toHaveBeenCalled();
  });

  it.each([
    ['from' as SelectionType, classes.from],
    ['to' as SelectionType, classes.to],
  ])('pass the correct classes with type', (type: SelectionType, className: string) => {
    const { element } = renderItem({ selectType: type });
    expect(element).toHaveClass(className);
  });

  it('pass the correct classes on selected', () => {
    const { element } = renderItem({ selected: true });
    expect(element).toHaveClass(classes.selected);
    expect(element).not.toHaveClass(classes.edge, classes.start, classes.end, classes.hovered, classes.disabled);
  });
  it('pass the correct classes on selected start', () => {
    const { element } = renderItem({ selected: true, start: true });
    expect(element).toHaveClass(classes.selected, classes.start, classes.edge);
    expect(element).not.toHaveClass(classes.end, classes.hovered, classes.disabled);
  });
  it('pass the correct classes on selected end', () => {
    const { element } = renderItem({ selected: true, end: true });
    expect(element).toHaveClass(classes.selected, classes.end, classes.edge);
    expect(element).not.toHaveClass(classes.start, classes.hovered, classes.disabled);
  });
  it('pass the correct classes on selected start and end', () => {
    const { element } = renderItem({ selected: true, start: true, end: true });
    expect(element).toHaveClass(classes.selected, classes.start, classes.end, classes.edge);
    expect(element).not.toHaveClass(classes.hovered, classes.disabled);
  });
  it('pass the correct classes on not selected start and end', () => {
    const { element } = renderItem({ selected: false, start: true, end: true });
    expect(element).not.toHaveClass(classes.selected, classes.start, classes.end, classes.edge);
  });
});
