import { createLocalContext } from '../../../../utils';
import { TreeApi, TreeState } from '../types';
import { TreeAction } from '../state/actions';

export const [TreeStateContextProvider] = createLocalContext<TreeState>('Tree state');
const [TreeStateProvider, useTreeApi] = createLocalContext<TreeApi>('Tree api');

export const TreeStateApiProvider = TreeStateProvider;

export const useTreeStateApi = <TA extends TreeAction>(): TreeApi<TA> => useTreeApi() as TreeApi<TA>;
