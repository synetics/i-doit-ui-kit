import { fireEvent, render, screen } from '@testing-library/react';
import React, { useRef } from 'react';
import user from '@testing-library/user-event';
import { ToggleButton } from '../../../ToggleButton/ToggleButton';
import { TreeItem } from '../TreeItem';
import { Tree } from './Tree';

const testSmallTree: Record<string, { id: string; title: string; children: string[] }> = {
  a: {
    id: 'root-1',
    title: 'First',
    children: [],
  },
  b: {
    id: 'root-2',
    title: 'Second',
    children: [],
  },
  c: {
    id: 'root-3',
    title: 'Third',
    children: [],
  },
};

export const TreeExample = () => {
  const refContainer = useRef(null);
  const apiRef = useRef({
    focus: () => {},
    keydown: () => {},
    setOpen: () => {},
    open: () => {},
    toggle: () => {},
    close: () => {},
    select: () => {},
    blur: () => {},
    dispatch: () => {},
  });

  return (
    <div className="d-flex flex-column flex-fill">
      <div ref={refContainer} className="overflow-auto flex-fill">
        <Tree
          rootId="root"
          apiRef={apiRef}
          defaultState={{ selected: 'root', focused: 'root', open: ['root'], visible: [] }}
          getNodes={(id) => (id === 'root' ? Object.keys(testSmallTree) : [])}
        >
          {({ node, api, nodeState, ref }) => (
            <TreeItem
              key={node.id}
              ref={(element) => {
                if (element) {
                  ref(element);
                }
              }}
              state={nodeState}
              node={node}
            >
              {node.id === 'root' && (
                <>
                  <ToggleButton
                    tabIndex={-1}
                    variant="text"
                    className="mr-2"
                    isOpen={nodeState.open}
                    onToggle={() => api.toggle(node.id)}
                  />
                  All Cities
                </>
              )}
              {testSmallTree[node.id]?.title || ''}
            </TreeItem>
          )}
        </Tree>
      </div>
    </div>
  );
};

describe('TreeItem', () => {
  it('renders correctly', () => {
    render(<TreeExample />);
    expect(screen.queryByText('All Cities')).toBeInTheDocument();
  });
  it('rerender after navigate', async () => {
    const component = render(<TreeExample />);
    const root = screen.queryByText('All Cities');
    fireEvent.focus(root || document.body);
    expect(root).toHaveClass('selected');
    await user.type(root || document.body, '{arrowright}');
    expect(component.rerender).toBeTruthy();
  });
});
