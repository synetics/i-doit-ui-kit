import React, { MutableRefObject, ReactElement, ReactNode, useImperativeHandle, useMemo, useRef } from 'react';
import classnames from 'classnames';
import { useEvent, useFocusWithin } from '../../../../../hooks';
import { ApiType, ScrollList } from '../../../../ScrollList';
import { useTree } from '../../hooks';
import { TreeStateApiProvider, TreeStateContextProvider } from '../../context';
import { ExtraReducerType, GetNodesType, NodeType, TreeApi, TreeState } from '../../types';
import { Callbacks, keyboardReducer, openCloseReducer, stateChangeReducer, visibleReducer } from '../../state';
import { TreeAction } from '../../state/actions';
import { collectVisible } from '../../utils';
import classes from './Tree.module.scss';

export type NodeState = {
  open: boolean;
  selected: boolean;
  focused: boolean;
};

export type TreeNodeProps<A extends TreeAction> = {
  node: NodeType;
  api: TreeApi<A>;
  nodeState: NodeState;
  ref: (element: HTMLElement) => void;
};

export type TreeType<T extends TreeState, A extends TreeAction> = {
  apiRef?: MutableRefObject<TreeApi<A> | undefined>;
  className?: string;
  children: (child: TreeNodeProps<A>) => ReactNode;
  getNodes: GetNodesType;
  rootId: string;
  onChange?: Callbacks<T, A>;
  extraReducers?: ExtraReducerType<T, A>[];
  defaultState: T;
};

export const Tree = <T extends TreeState, A extends TreeAction>({
  apiRef: treeApiRef,
  rootId,
  className,
  onChange,
  getNodes,
  children,
  extraReducers,
  defaultState,
}: TreeType<T, A>): ReactElement => {
  const ref = useRef<HTMLDivElement>(null);
  const refs = useRef<Record<string, HTMLElement>>({});
  const handleChildRef = (id: string) => (element: HTMLElement) => {
    refs.current[id] = element;
  };
  const focusWithin = useFocusWithin(ref.current);
  const apiRef = useRef<ApiType>(null);
  const handleFocus = useEvent(({ focused, visible }: T) => {
    if (!focused) {
      return;
    }
    if (apiRef.current) {
      apiRef.current.scrollToIndex(visible.findIndex((a) => a.id === focused));
    }
    if (refs.current[focused]) {
      refs.current[focused].focus();
    } else {
      setTimeout(() => {
        refs.current[focused].focus();
      }, 100);
    }
  });
  const retrieveNodes = useEvent(getNodes);
  const reducers = useMemo(
    () => [
      openCloseReducer,
      visibleReducer,
      keyboardReducer,
      ...(extraReducers || []),
      stateChangeReducer<T, A>({
        focused: handleFocus,
      }),
      stateChangeReducer(onChange || {}),
    ],
    [extraReducers, handleFocus, onChange],
  );
  const [state, api] = useTree<T, A>({
    defaultState,
    reducers,
    config: {
      rootId,
      getNodes: retrieveNodes,
    },
  });
  useImperativeHandle(treeApiRef, () => api, [api]);
  const { open, focused, selected } = state;
  const visible = useMemo(() => collectVisible(open, getNodes, rootId), [getNodes, open, rootId]);
  const shown = apiRef.current?.virtualItems.map((a) => visible[a.index]?.id || null) || [];
  const tabbable = [focused, selected, rootId, shown[0]].reduce(
    (r, id) => r || (id && shown.includes(id) ? id : null),
    null,
  );
  return (
    <TreeStateApiProvider value={api as TreeApi}>
      <TreeStateContextProvider value={state}>
        <ScrollList
          ref={ref}
          apiRef={apiRef}
          itemSize={40}
          size={visible.length}
          className={classnames(classes.container, className)}
          onKeyDown={(e) => {
            e.stopPropagation();
            api.keydown(e);
          }}
        >
          {(i) => {
            const node = visible[i];
            const { id } = node;
            return children({
              ref: handleChildRef(id),
              node,
              nodeState: {
                open: open?.includes(id),
                focused: focusWithin ? focused === id : tabbable === id,
                selected: id === selected,
              },
              api,
            });
          }}
        </ScrollList>
      </TreeStateContextProvider>
    </TreeStateApiProvider>
  );
};
