import React, { forwardRef, ReactNode } from 'react';
import { Item } from '../../../Item';
import { useTreeStateApi } from '../../context';
import { NodeType } from '../../types';
import { NodeState } from '../Tree';

export type TreeItemType = {
  className?: string;
  node: NodeType;
  state: NodeState;
  children: ReactNode;
};

export const TreeItem = forwardRef<HTMLDivElement, TreeItemType>(
  ({ node: { id, level }, state: { selected, focused }, className, children }, ref) => {
    const api = useTreeStateApi();
    return (
      <Item
        style={{ marginLeft: level * 40 }}
        tabIndex={focused || selected ? 0 : -1}
        ref={ref}
        className={className}
        selected={selected}
        onFocus={() => api.focus(id)}
        onSelect={() => {
          api.select(id);
          api.focus(id);
        }}
      >
        {children}
      </Item>
    );
  },
);
