import { render, screen } from '@testing-library/react';
import React from 'react';
import { Tree } from '../Tree';
import { TreeItem } from './TreeItem';

describe('TreeItem', () => {
  it('renders correctly', () => {
    render(
      <Tree
        rootId="root"
        defaultState={{ selected: 'root', focused: 'root', open: ['root'], visible: [] }}
        getNodes={() => []}
      >
        {({ node, nodeState }) => (
          <TreeItem node={node} state={nodeState}>
            Test
          </TreeItem>
        )}
      </Tree>,
    );
    expect(screen.queryByText('Test')).toBeInTheDocument();
  });

  it('check tabIndex', () => {
    render(
      <Tree
        rootId="root"
        defaultState={{ selected: 'root-1', focused: 'root-1', open: ['root'], visible: [] }}
        getNodes={() => []}
      >
        {({ node, nodeState }) => (
          <TreeItem node={node} state={nodeState}>
            Test
          </TreeItem>
        )}
      </Tree>,
    );
    const item = screen.getByText('Test');
    expect(item.tabIndex).toEqual(-1);
  });
});
