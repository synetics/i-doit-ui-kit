import { GetNodesType } from '../types';
import { collectVisible } from './collect-visible';

describe('collectVisible', () => {
  it('returns an array of visible nodes', () => {
    const open = ['root'];
    const getNodes: GetNodesType = (id: string) => (id === 'root' ? ['1.1', '1.2'] : []);
    const rootId = 'root';

    const expected = [
      {
        id: 'root',
        level: 0,
      },
      {
        id: '1.1',
        level: 1,
      },
      {
        id: '1.2',
        level: 1,
      },
    ];
    const result = collectVisible(open, getNodes, rootId);
    expect(result).toEqual(expected);
  });
});
