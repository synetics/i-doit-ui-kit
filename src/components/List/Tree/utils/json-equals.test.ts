import { jsonEquals } from './json-equals';

describe('jsonEquals', () => {
  it('identical objects', () => {
    const obj1 = { foo: 'bar', baz: { qux: 42 } };
    const obj2 = { foo: 'bar', baz: { qux: 42 } };

    expect(jsonEquals(obj1, obj2)).toBe(true);
  });

  it('non-identical objects', () => {
    const obj1 = { foo: 'bar', baz: { qux: 42 } };
    const obj2 = { foo: 'baz', baz: { qux: 42 } };

    expect(jsonEquals(obj1, obj2)).toBe(false);
  });
});
