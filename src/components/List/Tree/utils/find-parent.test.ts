import { findParent } from './find-parent';

describe('findParent', () => {
  it('find correct parent node ID', () => {
    const nodes: Record<string, string[]> = {
      '1': ['2', '3'],
      '2': ['4'],
      '3': ['5'],
      '4': ['6'],
      '5': [],
      '6': [],
    };

    const getNodes = (id: string) => nodes[id];

    expect(findParent(getNodes, '1', '6')).toEqual('4');
    expect(findParent(getNodes, '1', '5')).toEqual('3');
    expect(findParent(getNodes, '5', '2')).toEqual(null);
  });
});
