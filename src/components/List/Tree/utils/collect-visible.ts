import { traverse } from '../../../../utils/traverse';
import { GetNodesType, NodeType } from '../types';

export const collectVisible = (open: string[], getNodes: GetNodesType, rootId: string) =>
  traverse<string, NodeType[], number>((node, next, level = 0): NodeType[] => {
    const childNodes = getNodes(node);
    const result = childNodes.reduce((r, child) => [...r, ...next(child, level + 1)], [] as NodeType[]);
    return [{ id: node, level }, ...(open.includes(node) ? result : [])];
  })(rootId, 0);
