import { traverse } from '../../../../utils/traverse';
import { GetNodesType } from '../types';

export const findParent = (getNodes: GetNodesType, rootId: string, search: string) =>
  traverse<string, string | null, unknown>((node: string, next) => {
    const children = getNodes(node);
    if (children.includes(search)) {
      return node;
    }

    return children.reduce((r, child) => r || next(child), null as string | null);
  })(rootId, '');
