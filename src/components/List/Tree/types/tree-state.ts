import { KeyboardEvent } from 'react';
import { TreeAction } from '../state/actions';

export type NodeType = { id: string; level: number };

export type TreeState = {
  selected: string | null;
  focused: string | null;
  open: string[];
  visible: NodeType[];
};

export type TreeApi<TA extends TreeAction = TreeAction> = {
  focus: (id: string) => void;
  keydown: (e: KeyboardEvent) => void;
  open: (id: string) => void;
  setOpen: (ids: string[]) => void;
  toggle: (id: string) => void;
  close: (id: string) => void;
  select: (id: string) => void;
  blur: () => void;
  dispatch: (action: TA) => void;
};

export type GetNodesType = (id: string) => string[];

export type TreeConfig = {
  getNodes: GetNodesType;
  rootId: string;
};

export type ExtraReducerType<T extends TreeState = TreeState, ActionType extends TreeAction = TreeAction> = (
  previous: T,
  action: ActionType,
  computed: T,
  config: TreeConfig,
) => T;
