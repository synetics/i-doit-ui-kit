import { KeyboardEvent } from 'react';
import { TreeState } from '../types';
import { keyboardReducer } from './keyboard-reducer';
import { TreeAction } from './actions';

describe('keyboardReducer', () => {
  const initialState: TreeState = {
    selected: null,
    focused: '1',
    open: ['root'],
    visible: [
      { id: '1', level: 1 },
      { id: '2', level: 1 },
      { id: '3', level: 1 },
    ],
  };
  const config = {
    getNodes: (id: string) => (id === 'root' ? new Array(30).fill(0).map((el, ind) => ind.toString()) : []),
    rootId: 'root',
  };

  it('return the initial state', () => {
    expect(keyboardReducer(initialState, {} as TreeAction, initialState, config)).toStrictEqual(initialState);
  });

  it("should update the state when a 'keydown' action is dispatched", () => {
    let state: undefined | TreeState;
    const cases = [
      { key: 'PageDown', result: '11' },
      { key: 'PageUp', result: '1' },
      { key: 'End', result: '29' },
      { key: 'Home', result: 'root' },
      { key: 'ArrowDown', result: '0' },
      { key: 'ArrowDown', result: '1' },
      { key: 'ArrowUp', result: '0' },
    ];

    cases.forEach(({ key, result }) => {
      const preventDefault = jest.fn();
      const stopPropagation = jest.fn();
      const payload = {
        key,
        preventDefault,
        stopPropagation,
      } as unknown as KeyboardEvent;
      state = keyboardReducer(state || initialState, { type: 'keydown', payload }, state || initialState, config);
      expect(state.focused).toEqual(result);
      expect(preventDefault).toHaveBeenCalledTimes(1);
      expect(preventDefault).toHaveBeenCalledTimes(1);
    });
  });
});
