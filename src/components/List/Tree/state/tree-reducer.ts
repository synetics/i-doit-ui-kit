import { TreeState } from '../types';
import { TreeAction } from './actions';

const focusReducer = (state: string | null, action: TreeAction): string | null => {
  switch (action.type) {
    case 'focus':
      return action.payload;
    case 'blur':
      return null;
    default:
      return state;
  }
};

const selectedReducer = (state: string | null, action: TreeAction): string | null => {
  switch (action.type) {
    case 'select':
      return action.payload;
    case 'blur':
      return null;
    default:
      return state;
  }
};

const openReducer = (state: string[], action: TreeAction): string[] => {
  switch (action.type) {
    case 'toggle':
      if (state.includes(action.payload)) {
        return state.filter((a) => a !== action.payload);
      }
      return [...state, action.payload];
    case 'set-open':
      return action.payload;
    case 'open':
      return state.includes(action.payload) ? state : [...state, action.payload];
    case 'close':
      return state.filter((a) => a !== action.payload);
    default:
      return state;
  }
};

export const TreeStateReducer = <T extends TreeState, A extends TreeAction>(state: T, action: A): T => {
  const { focused, open, selected } = state;
  return {
    ...state,
    focused: focusReducer(focused, action),
    open: openReducer(open, action),
    selected: selectedReducer(selected, action),
  };
};
