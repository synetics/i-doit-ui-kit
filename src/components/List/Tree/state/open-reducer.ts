import { KeyboardKeys } from '../../../../static';
import { TreeConfig, TreeState } from '../types';
import { findParent } from '../utils';
import { TreeAction } from './actions';

export const openCloseReducer = <T extends TreeState, TA extends TreeAction>(
  newState: T,
  action: TA,
  old: T,
  { rootId, getNodes }: TreeConfig,
): T => {
  const next = { ...newState };
  const { open, focused } = next;
  if (!focused) {
    return newState;
  }
  if (action.type === 'keydown') {
    if (action.payload.key === KeyboardKeys.ArrowRight) {
      if (!open.includes(focused) && getNodes(focused).length > 0) {
        next.open.push(focused);
      } else {
        const index = newState.visible.findIndex((a) => a.id === focused);
        next.focused = newState.visible[Math.min(index + 1, newState.visible.length)]?.id || null;
      }
    }
    if (action.payload.key === KeyboardKeys.ArrowLeft) {
      if (open.includes(focused)) {
        next.open = open.filter((a) => a !== focused);
      } else {
        const parent = findParent(getNodes, rootId, focused);
        next.focused = parent || rootId;
      }
    }
  }
  return next;
};
