import { openCloseReducer } from './open-reducer';
import { TreeAction } from './actions';

describe('open-Reducer', () => {
  const state = {
    selected: null,
    focused: '1',
    open: ['2', '3'],
    visible: [
      { id: '1', level: 0 },
      { id: '2', level: 1 },
      { id: '3', level: 1 },
    ],
  };
  const config = {
    rootId: '1',
    getNodes: (id: string) => (id === '1' ? ['2', '3'] : []),
  };

  it('check unknown type', () => {
    const action = { type: 'unknown' };

    const newState = openCloseReducer(state, action as TreeAction, state, config);
    expect(newState).toStrictEqual(state);
  });

  it('!focused', () => {
    const stateLocal = {
      selected: null,
      focused: null,
      open: ['2', '3'],
      visible: [
        { id: '1', level: 0 },
        { id: '2', level: 1 },
        { id: '3', level: 1 },
      ],
    };
    const configLocal = {
      rootId: '1',
      getNodes: (id: string) => (id === '1' ? ['2', '3'] : []),
    };
    const action = { type: 'unknown' };

    const newState = openCloseReducer(stateLocal, action as TreeAction, stateLocal, configLocal);
    expect(newState).toStrictEqual(stateLocal);
  });

  it('open include focus', () => {
    const stateLocal = {
      selected: null,
      focused: '2',
      open: ['2', '3'],
      visible: [
        { id: '1', level: 0 },
        { id: '2', level: 1 },
        { id: '3', level: 1 },
      ],
    };
    const configLocal = {
      rootId: '1',
      getNodes: (id: string) => (id === '1' ? ['2', '3'] : []),
    };
    const action = { type: 'keydown', payload: { key: 'ArrowLeft' } };

    const newState = openCloseReducer(stateLocal, action as TreeAction, stateLocal, configLocal);
    expect(newState.open).toEqual(['3']);
  });

  it('open the focused node when ArrowRight is pressed and it has children', () => {
    const action = { type: 'keydown', payload: { key: 'ArrowRight' } };

    const newState = openCloseReducer(state, action as TreeAction, state, config);
    expect(newState.open).toEqual(['2', '3', '1']);
  });

  it('move focus to the next visible node when ArrowRight is pressed and the focused node is already open', () => {
    const newState = { ...state, open: ['2', '3', '1'], focused: '2' };
    const action = { type: 'keydown', payload: { key: 'ArrowRight' } };

    const result = openCloseReducer(newState, action as TreeAction, state, config);
    expect(result.focused).toBe('3');
  });

  it('move focus to the previous visible node when ArrowLeft is pressed and the focused node is closed', () => {
    const newState = { ...state, focused: '2', open: ['3'] };
    const action = { type: 'keydown', payload: { key: 'ArrowLeft' } };

    const result = openCloseReducer(newState, action as TreeAction, state, config);
    expect(result.focused).toBe('1');
  });
});
