import { KeyboardKeys } from '../../../../static';
import { TreeConfig, TreeState } from '../types';
import { collectVisible } from '../utils';
import { TreeAction } from './actions';

const offsetMap: Record<string, number> = {
  [KeyboardKeys.Home]: -Infinity,
  [KeyboardKeys.End]: Infinity,
  [KeyboardKeys.PageDown]: 10,
  [KeyboardKeys.PageUp]: -10,
  [KeyboardKeys.ArrowUp]: -1,
  [KeyboardKeys.ArrowDown]: 1,
};

export const keyboardReducer = <T extends TreeState, TA extends TreeAction>(
  newState: T,
  action: TA,
  oldState: T,
  { rootId, getNodes }: TreeConfig,
): T => {
  const next = { ...newState };
  if (action.type === 'keydown') {
    const visible = collectVisible(next.open, getNodes, rootId);
    const { key } = action.payload;
    const offset = offsetMap[key];
    if (offset) {
      const index = visible.findIndex((a) => a.id === next.focused) + offset;
      next.focused = visible[Math.max(0, Math.min(visible.length - 1, index))].id;
      if (typeof action.payload.stopPropagation === 'function' && typeof action.payload.preventDefault === 'function') {
        action.payload.stopPropagation();
        action.payload.preventDefault();
      }
    }
  }
  return next;
};
