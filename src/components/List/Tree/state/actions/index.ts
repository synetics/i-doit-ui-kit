import { KeyboardEvent } from 'react';
import { ReturnType } from '../../../../../types/function';
import { createTreeAction } from './tree-action';

export const TreeActions = {
  focus: createTreeAction<'focus', string>('focus' as const),
  select: createTreeAction<'select', string | null>('select' as const),
  keydown: createTreeAction<'keydown', KeyboardEvent>('keydown' as const),
  open: createTreeAction<'open', string>('open' as const),
  setOpen: createTreeAction<'set-open', string[]>('set-open' as const),
  close: createTreeAction<'close', string>('close' as const),
  toggle: createTreeAction<'toggle', string>('toggle' as const),
  blur: () => createTreeAction<'blur', undefined>('blur' as const)(undefined),
};

export type TreeAction =
  | ReturnType<typeof TreeActions.focus>
  | ReturnType<typeof TreeActions.select>
  | ReturnType<typeof TreeActions.blur>
  | ReturnType<typeof TreeActions.toggle>
  | ReturnType<typeof TreeActions.open>
  | ReturnType<typeof TreeActions.setOpen>
  | ReturnType<typeof TreeActions.close>
  | ReturnType<typeof TreeActions.keydown>;
