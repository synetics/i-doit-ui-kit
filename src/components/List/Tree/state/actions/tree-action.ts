export type BaseTreeAction<TYPE extends string, PAYLOAD> = {
  type: TYPE;
  payload: PAYLOAD;
};

export const createTreeAction =
  <TYPE extends string, PAYLOAD>(type: TYPE) =>
  (payload: PAYLOAD): BaseTreeAction<TYPE, PAYLOAD> => ({
    type,
    payload,
  });
