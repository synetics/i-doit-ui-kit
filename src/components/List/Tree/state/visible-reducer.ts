import { NodeType, TreeConfig, TreeState } from '../types';
import { collectVisible } from '../utils';
import { TreeAction } from './actions';

const contains =
  (items: NodeType[]) =>
  (id: string): boolean =>
    !!items.find((a) => a.id === id);

const fallbackChain = (ids: (string | null)[], check: (id: string) => boolean): string | null =>
  ids.reduce((result: string | null, item) => {
    if (result === null && item !== null && check(item)) {
      return item;
    }
    return result;
  }, null);

export const visibleReducer = <T extends TreeState, TA extends TreeAction>(
  newState: T,
  action: TA,
  old: T,
  { rootId, getNodes }: TreeConfig,
): T => {
  const { focused, selected, open } = newState;
  const visible = collectVisible(open, getNodes, rootId);
  return {
    ...newState,
    visible,
    focused: fallbackChain([focused, selected, rootId], contains(visible)),
    open: open.filter(contains(visible)),
  };
};
