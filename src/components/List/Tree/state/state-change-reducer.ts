import { TreeState } from '../types';
import { jsonEquals } from '../utils';
import { TreeAction } from './actions';

export type Callbacks<T extends TreeState, TA extends TreeAction> = {
  [K in keyof T]?: (state: T, action: TA) => void;
};

export const stateChangeReducer =
  <T extends TreeState, TA extends TreeAction>(callbacks: Callbacks<T, TA>) =>
  (newState: T, action: TA, oldState: T): T => {
    const keys = Object.keys(newState) as (keyof T)[];
    keys.forEach((k) => {
      const callback = callbacks[k];
      if (callback && !jsonEquals(newState[k], oldState[k])) {
        callback(newState, action);
      }
    });
    return newState;
  };
