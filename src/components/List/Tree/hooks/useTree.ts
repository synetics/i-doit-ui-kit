import { KeyboardEvent, useMemo, useReducer } from 'react';
import { useEvent } from '../../../../hooks';
import { ExtraReducerType, TreeApi, TreeConfig, TreeState } from '../types';
import { TreeAction, TreeActions, TreeStateReducer } from '../state';

export type TreeOptions<T extends TreeState, ActionType extends TreeAction> = {
  reducers?: ExtraReducerType<T, ActionType>[];
  defaultState: T;
  config: TreeConfig;
};

export const useTree = <T extends TreeState, TA extends TreeAction>({
  reducers = [],
  defaultState,
  config,
}: TreeOptions<T, TA>): [T, TreeApi<TA>] => {
  const Reducer = useEvent(
    (state: T, action: TA): T =>
      reducers.reduce((next, reducer) => reducer(next, action, state, config), TreeStateReducer(state, action)),
  );
  const [state, dispatch] = useReducer(Reducer, defaultState);
  const api = useMemo(
    () => ({
      focus: (id: string) => dispatch(TreeActions.focus(id) as TA),
      open: (id: string) => dispatch(TreeActions.open(id) as TA),
      setOpen: (id: string[]) => dispatch(TreeActions.setOpen(id) as TA),
      close: (id: string) => dispatch(TreeActions.close(id) as TA),
      toggle: (id: string) => dispatch(TreeActions.toggle(id) as TA),
      select: (id: string | null) => dispatch(TreeActions.select(id) as TA),
      blur: () => dispatch(TreeActions.blur() as TA),
      keydown: (e: KeyboardEvent) => dispatch(TreeActions.keydown(e) as TA),
      dispatch,
    }),
    [],
  );
  return [state, api];
};
