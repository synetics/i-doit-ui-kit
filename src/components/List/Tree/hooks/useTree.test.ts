import { act, renderHook } from '@testing-library/react-hooks';
import { TreeConfig, TreeState } from '../types';
import { useTree } from './useTree';

describe('useTree', () => {
  const config: TreeConfig = {
    getNodes: () => [],
    rootId: 'root',
  };

  const defaultState: TreeState = {
    selected: null,
    focused: null,
    open: [],
    visible: [],
  };

  const apiState = ['focus', 'keydown', 'open', 'toggle', 'close', 'select', 'blur', 'dispatch', 'setOpen'];

  it('should initialize state correctly', () => {
    const { result } = renderHook(() => useTree({ defaultState, config }));
    const [state, api] = result.current;

    expect(state).toEqual(defaultState);
    apiState.map((el) => expect(api).toHaveProperty(el));
  });

  it('should update state correctly', () => {
    const { result } = renderHook(() => useTree({ defaultState, config }));
    const [, api] = result.current;

    act(() => {
      api.focus('node');
    });

    expect(result.current[0].focused).toEqual('node');

    act(() => {
      api.select('node');
    });

    expect(result.current[0].selected).toEqual('node');

    act(() => {
      api.open('node');
    });

    expect(result.current[0].open).toEqual(['node']);

    // open already opened keep open
    act(() => {
      api.open('node');
    });

    expect(result.current[0].open).toEqual(['node']);

    act(() => {
      api.toggle('node');
    });

    expect(result.current[0].open).toEqual([]);

    act(() => {
      api.toggle('node');
    });

    expect(result.current[0].open).toEqual(['node']);

    act(() => {
      api.close('node');
    });

    expect(result.current[0].open).toEqual([]);

    act(() => {
      api.blur();
    });

    expect(result.current[0].focused).toEqual(null);

    act(() => {
      api.dispatch({ type: 'test', payload: '' } as ReturnType<any>);
    });

    expect(result.current[0]).toEqual(defaultState);

    act(() => {
      api.setOpen(['node', '1']);
    });

    expect(result.current[0].open).toEqual(['node', '1']);

    act(() => {
      api.close('node');
    });

    expect(result.current[0].open).toEqual(['1']);
  });
});
