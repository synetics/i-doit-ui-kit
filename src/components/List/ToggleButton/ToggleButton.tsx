import React, { forwardRef } from 'react';
import { Icon } from '../../Icon';
import { IconButton, IconButtonProps } from '../../IconButton';
import { arrow_down, arrow_right } from '../../../icons';

type ToggleButtonType = Omit<IconButtonProps, 'label' | 'onClick' | 'icon' | 'iconSize'> & {
  isOpen: boolean;
  onToggle: () => void;
  openLabel?: string;
  closeLabel?: string;
  className?: string;
};

const ToggleButton = forwardRef<HTMLButtonElement, ToggleButtonType>(
  ({ isOpen, onToggle, openLabel = 'Open', closeLabel = 'Close', ...rest }, ref) => (
    <IconButton
      {...rest}
      ref={ref}
      iconSize="small"
      onMouseDown={onToggle}
      label={isOpen ? closeLabel : openLabel}
      icon={<Icon src={isOpen ? arrow_down : arrow_right} />}
    />
  ),
);

export { ToggleButton };
