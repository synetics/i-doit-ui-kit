import { render, screen } from '@testing-library/react';
import user from '@testing-library/user-event';
import { ToggleButton } from './ToggleButton';

describe('ToggleButton', () => {
  it('renders without crashing', () => {
    const callback = jest.fn();
    render(<ToggleButton isOpen onToggle={callback} />);

    const button = screen.getByRole('button');
    expect(button).toBeInTheDocument();
  });

  it('click on ToggleButton', async () => {
    const callback = jest.fn();
    render(<ToggleButton isOpen onToggle={callback} />);
    const button = screen.getByRole('button');
    await user.click(button);
    expect(callback).toHaveBeenCalled();
  });

  it('renders with custom closeLabel', async () => {
    const callback = jest.fn();
    render(<ToggleButton isOpen onToggle={callback} closeLabel="test-close" />);
    const toggleButton = screen.getByRole('button');

    expect(screen.getByTestId('icon')).toBeInTheDocument();

    await user.hover(toggleButton);
    const tooltip = await screen.findByRole('tooltip');
    expect(tooltip).toBeInTheDocument();
    expect(tooltip).toHaveTextContent('test-close');
  });

  it('renders with custom openLabel', async () => {
    const callback = jest.fn();
    render(<ToggleButton isOpen={false} onToggle={callback} openLabel="test-open" />);
    const toggleButton = screen.getByRole('button');

    expect(screen.getByTestId('icon')).toBeInTheDocument();

    await user.hover(toggleButton);
    const tooltip = await screen.findByRole('tooltip');
    expect(tooltip).toBeInTheDocument();
    expect(tooltip).toHaveTextContent('test-open');
  });

  it('renders with custom className', async () => {
    const callback = jest.fn();
    render(<ToggleButton isOpen={false} onToggle={callback} className="test" />);
    const toggleButton = screen.getByRole('button');

    expect(toggleButton).toHaveClass('test');
  });
});
