import { screen } from '@testing-library/react';
import { createRenderer } from '../../../../../test/utils/renderer';
import { Separator } from './Separator';

const renderComponent = createRenderer(Separator, {});

describe('<Separator/>', () => {
  it('renders without crashing', () => {
    renderComponent({ 'data-testid': 'test' });
    const item = screen.getByTestId('test');
    expect(item).toBeInTheDocument();
  });

  it('passes the className', () => {
    renderComponent({ 'data-testid': 'test', className: 'custom' });
    const item = screen.queryByTestId('test');
    expect(item).toHaveClass('custom');
  });
});
