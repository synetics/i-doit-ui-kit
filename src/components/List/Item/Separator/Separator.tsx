import classnames from 'classnames';
import classes from './Separator.module.scss';

type SeparatorType = {
  className?: string;
  'data-testid'?: string;
};

const Separator = ({ 'data-testid': testId, className }: SeparatorType) => (
  <hr data-testid={testId} className={classnames(classes.delimiter, className)} />
);

export { Separator };
