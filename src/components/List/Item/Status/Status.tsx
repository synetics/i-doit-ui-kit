import classnames from 'classnames';
import classes from './Status.module.scss';

type StatusType = {
  className?: string;
  'data-testid'?: string;
  bulletColor: string;
};

const Status = ({ 'data-testid': testId, bulletColor, className }: StatusType) => (
  <div data-testid={testId} className={classnames(classes.status, className)}>
    <div className={classes.bullet} style={{ background: bulletColor }} />
  </div>
);

export { Status };
