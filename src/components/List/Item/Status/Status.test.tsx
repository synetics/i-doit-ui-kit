import { screen } from '@testing-library/react';
import { createRenderer } from '../../../../../test/utils/renderer';
import { Status } from './Status';

const renderComponent = createRenderer(Status, { bulletColor: 'red' });

describe('<Status/>', () => {
  it('renders without crashing', () => {
    renderComponent({ 'data-testid': 'test', bulletColor: 'red' });
    const item = screen.getByTestId('test');
    expect(item).toBeInTheDocument();
  });

  it('passes the className', () => {
    renderComponent({ 'data-testid': 'test', className: 'custom', bulletColor: 'red' });
    const item = screen.queryByTestId('test');
    expect(item).toHaveClass('custom');
  });
});
