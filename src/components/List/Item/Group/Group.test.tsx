import { screen } from '@testing-library/react';
import { createRenderer } from '../../../../../test/utils/renderer';
import { Group } from './Group';
import classes from './Group.module.scss';

const renderComponent = createRenderer(Group, {});

describe('<Group/>', () => {
  it('renders without crashing', () => {
    renderComponent({ dataTestId: 'test' });
    const item = screen.getByTestId('test');
    expect(item).toBeInTheDocument();
  });

  it('passes the className', () => {
    renderComponent({ dataTestId: 'test', className: 'custom' });
    const item = screen.queryByTestId('test');
    expect(item).toHaveClass('custom');
  });

  it('passes the size', () => {
    renderComponent({ dataTestId: 'test', size: 'small' });
    const item = screen.queryByTestId('test');
    expect(item).toHaveClass(classes['size-small']);
  });

  it('renders children', () => {
    renderComponent({ children: <div>test</div> });
    const child = screen.queryByText('test');
    expect(child).toBeInTheDocument();
  });
});
