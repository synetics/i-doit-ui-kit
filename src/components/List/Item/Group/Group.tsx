import classnames from 'classnames';
import { Container, ContainerProps } from '../../../Container';
import classes from './Group.module.scss';

type GroupType = ContainerProps & {
  size?: 'normal' | 'thick' | 'small';
};

const Group = ({ className, children, size = 'normal', ...props }: GroupType) => (
  <Container {...props} className={classnames(className, classes.content, classes[`size-${size}`])}>
    {children}
  </Container>
);

export { Group };
