import classnames from 'classnames';
import { DragHandle } from '../../../DragAndDrop';
import { Item, ItemType } from '../Item';
import classes from './DragItemPreview.module.scss';

type DragItemPreviewType = ItemType & {
  count: number;
};

const DragItemPreview = ({ count, className, children, ...props }: DragItemPreviewType) => (
  <Item {...props} selected className={classnames(className, classes.item)}>
    <DragHandle className={classes.handle} active />
    {children}
    {count > 1 && <div className={classes.counter}>{count}</div>}
  </Item>
);

export { DragItemPreview, DragItemPreviewType };
