import { screen } from '@testing-library/react';
import { createRenderer } from '../../../../../test/utils/renderer';
import { DragItemPreview } from './DragItemPreview';

const renderComponent = createRenderer(DragItemPreview, { count: 0 });

describe('<DragItemPreview/>', () => {
  it('renders without crashing', () => {
    renderComponent({ count: 1, children: 'test' });
    const item = screen.getByText('test');
    expect(item).toBeInTheDocument();
    const counter = screen.queryByText('1');
    expect(counter).not.toBeInTheDocument();
  });

  it('renders counter', () => {
    renderComponent({ count: 10, children: 'test' });
    const item = screen.getByText('test');
    expect(item).toBeInTheDocument();
    const counter = screen.queryByText('10');
    expect(counter).toBeInTheDocument();
  });
});
