import { screen } from '@testing-library/react';
import user from '@testing-library/user-event';
import { createRenderer } from '../../../../../test/utils/renderer';
import { Item } from './Item';
import classes from './Item.module.scss';

const renderComponent = createRenderer(Item, {});

describe('<Item/>', () => {
  it('renders without crashing', () => {
    renderComponent({ 'data-testid': 'test' });
    const item = screen.getByTestId('test');
    expect(item).toBeInTheDocument();
    expect(item).toHaveAttribute('tabIndex', '0');
  });

  it('passes the className', () => {
    renderComponent({ 'data-testid': 'test', className: 'custom' });
    const item = screen.queryByTestId('test');
    expect(item).toHaveClass('custom');
  });

  it('selects on click', async () => {
    const onSelect = jest.fn();
    renderComponent({ 'data-testid': 'test', onSelect });
    const item = screen.getByTestId('test');
    await user.click(item);
    expect(onSelect).toHaveBeenCalledTimes(1);
  });

  it('forwards the click handler', async () => {
    const onClick = jest.fn();
    renderComponent({ 'data-testid': 'test', onClick });
    const item = screen.getByTestId('test');
    await user.click(item);
    expect(onClick).toHaveBeenCalledTimes(1);
  });

  it('forwards the click handler even if disabled', async () => {
    const onClick = jest.fn();
    renderComponent({ 'data-testid': 'test', onClick, disabled: true });
    const item = screen.getByTestId('test');
    await user.click(item);
    expect(onClick).toHaveBeenCalled();
  });

  it('does not selects on click with disabled', async () => {
    const onSelect = jest.fn();
    renderComponent({ 'data-testid': 'test', onSelect, disabled: true });
    const item = screen.getByTestId('test');
    await user.click(item);
    expect(onSelect).not.toHaveBeenCalled();
  });

  it.each(['{enter}', ' '])('selects on keydown', async (text: string) => {
    const onSelect = jest.fn();
    renderComponent({ 'data-testid': 'test', onSelect });
    const item = screen.getByTestId('test');
    await user.type(item, text);
    expect(onSelect).toHaveBeenCalled();
  });
  it.each(['a', 'b', 'abc'])('ignores other keys on keydown', async (text: string) => {
    const onSelect = jest.fn();
    const onKeyDown = jest.fn();
    renderComponent({ 'data-testid': 'test', onSelect, onKeyDown });
    const item = screen.getByTestId('test');
    await user.tab();
    await user.type(item, text, { skipClick: true });
    expect(onSelect).not.toHaveBeenCalled();
    expect(onKeyDown).toHaveBeenCalled();
  });

  it.each(['{enter}', ' '])('forwards on keydown', async (text: string) => {
    const onKeyDown = jest.fn();
    renderComponent({ 'data-testid': 'test', onKeyDown });
    const item = screen.getByTestId('test');
    await user.type(item, text);
    expect(onKeyDown).toHaveBeenCalled();
  });

  it.each(['{enter}', ' '])('does not forward on keydown with disabled', async (text: string) => {
    const onKeyDown = jest.fn();
    renderComponent({ 'data-testid': 'test', onKeyDown, disabled: true });
    const item = screen.getByTestId('test');
    await user.type(item, text);
    expect(onKeyDown).not.toHaveBeenCalled();
  });

  it.each(['{enter}', ' '])('does not selects on keydown if disabled', async (text: string) => {
    const onSelect = jest.fn();
    renderComponent({ 'data-testid': 'test', onSelect, disabled: true });
    const item = screen.getByTestId('test');
    await user.type(item, text);
    expect(onSelect).toHaveBeenCalledTimes(0);
  });

  it('selected has needed attributes', () => {
    renderComponent({ 'data-testid': 'test', selected: true });
    const item = screen.queryByTestId('test');
    expect(item).toHaveClass(classes.selected);
    expect(item).toHaveAttribute('aria-selected', 'true');
    expect(item).toHaveAttribute('data-selected', 'true');
  });

  it('disabled has needed attributes', () => {
    renderComponent({ 'data-testid': 'test', disabled: true });
    const item = screen.queryByTestId('test');
    expect(item).toHaveClass(classes.disabled);
    expect(item).toHaveAttribute('data-disabled', 'true');
    expect(item).not.toHaveAttribute('tabIndex');
  });

  it('readsOnly have readOnly class', () => {
    renderComponent({ 'data-testid': 'test', readOnly: true });
    const item = screen.queryByTestId('test');
    expect(item).toHaveClass(classes.readOnly);
  });

  it('renders children', () => {
    renderComponent({ children: <div>test</div> });
    const child = screen.queryByText('test');
    expect(child).toBeInTheDocument();
  });
});
