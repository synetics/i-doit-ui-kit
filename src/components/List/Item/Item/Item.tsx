import React, { ComponentType, forwardRef, useCallback } from 'react';
import classnames from 'classnames';
import { useGlobalizedRef } from '../../../../hooks';
import { useRenderItem } from '../../../Form/Control';
import classes from './Item.module.scss';

type ItemType<P = React.HTMLAttributes<HTMLDivElement>> = P & {
  /**
   * Is the item selected
   */
  selected?: boolean;
  /**
   * Is the item disabled
   */
  disabled?: boolean;
  /**
   * Is the item in read-only mode
   */
  readOnly?: boolean;
  /**
   * Callback on select
   */
  onSelect?: () => void;
  /**
   * Custom component to render item
   */
  component?: ComponentType<P>;
  /**
   * value item id
   */
  value?: string;
  /**
   * test id
   */
  'data-testid'?: string;
};

const Item = forwardRef<HTMLDivElement, ItemType>(
  (
    {
      component,
      className,
      children,
      disabled = false,
      readOnly = false,
      selected = false,
      onSelect,
      onKeyDown,
      onClick,
      value,
      ...rest
    },
    ref,
  ) => {
    const { highlighted, props } = useRenderItem(value || '');
    const { localRef, setRefsCallback } = useGlobalizedRef(props.ref);
    const { setRefsCallback: setForwardRef } = useGlobalizedRef(ref);
    const setRef = (element: HTMLDivElement) => {
      setRefsCallback(element);
      setForwardRef(element);
    };
    const handleSelect = useCallback(() => {
      if (!disabled && !readOnly && onSelect && document.activeElement === localRef.current) {
        onSelect();
        return true;
      }
      return false;
    }, [localRef, onSelect, disabled, readOnly]);
    const handleClick = useCallback(
      (e: React.MouseEvent<HTMLDivElement>) => {
        if (onClick) {
          onClick(e);
        }
        handleSelect();
      },
      [onClick, handleSelect],
    );
    const handleKeyDown = useCallback(
      (e: React.KeyboardEvent<HTMLDivElement>) => {
        if (onKeyDown) {
          onKeyDown(e);
        }
        if (['Enter', ' '].includes(e.key)) {
          if (handleSelect()) {
            e.preventDefault();
          }
        }
      },
      [handleSelect, onKeyDown],
    );
    const Component = component || 'div';
    return (
      <Component
        role="option"
        tabIndex={disabled ? undefined : 0}
        {...rest}
        ref={setRef}
        aria-selected={selected}
        data-selected={selected}
        data-disabled={disabled}
        className={classnames(className, classes.item, {
          [classes.selected]: selected,
          [classes.disabled]: disabled,
          [classes.hovered]: highlighted,
          [classes.readOnly]: readOnly && !disabled,
        })}
        onClick={handleClick}
        onKeyDown={handleKeyDown}
      >
        {children}
      </Component>
    );
  },
);

export { Item, ItemType };
