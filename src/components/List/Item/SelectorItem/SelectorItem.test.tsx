import { screen } from '@testing-library/react';
import { createRenderer } from '../../../../../test/utils/renderer';
import { SelectorItem } from './SelectorItem';

const renderComponent = createRenderer(SelectorItem, { 'aria-label': 'label' });

describe('<SelectorItem/>', () => {
  it('renders without crashing', () => {
    renderComponent({ 'data-testid': 'test', 'aria-label': 'label' });
    const item = screen.getByTestId('test');
    expect(item).toBeInTheDocument();
  });

  it('passes the className', () => {
    renderComponent({ 'data-testid': 'test', className: 'custom', 'aria-label': 'label' });
    const item = screen.queryByTestId('test');
    expect(item).toHaveClass('custom');
  });

  it('renders children', () => {
    renderComponent({ children: <div>test</div>, 'aria-label': 'label' });
    const child = screen.queryByText('test');
    expect(child).toBeInTheDocument();
  });
});
