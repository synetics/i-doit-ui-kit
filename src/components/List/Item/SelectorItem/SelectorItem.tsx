import { Checkbox } from '../../../Form/Control';
import { Item, ItemType } from '../Item';

type SelectorItemType = ItemType & {
  'aria-label': string;
  value?: string;
  onCheckBoxFocus?: (v: boolean) => void;
};

const SelectorItem = ({
  'aria-label': ariaLabel,
  disabled,
  selected,
  onSelect,
  children,
  onCheckBoxFocus = () => {},
  ...props
}: SelectorItemType) => (
  <Item {...props} tabIndex={undefined} onClick={onSelect} disabled={disabled}>
    <Checkbox
      tabIndex={undefined}
      aria-label={ariaLabel}
      disabled={disabled}
      checked={selected}
      onChange={onSelect}
      onBlur={() => onCheckBoxFocus(false)}
      onFocus={() => onCheckBoxFocus(true)}
    />
    {children}
  </Item>
);

export { SelectorItem, SelectorItemType };
