import { screen } from '@testing-library/react';
import { createRenderer } from '../../../../../test/utils/renderer';
import { Content } from './Content';
import classes from './Content.module.scss';

const renderComponent = createRenderer(Content, {});

describe('<Content/>', () => {
  it('renders without crashing', () => {
    renderComponent({ dataTestId: 'test' });
    const item = screen.getByTestId('test');
    expect(item).toBeInTheDocument();
  });

  it('passes the className', () => {
    renderComponent({ dataTestId: 'test', className: 'custom' });
    const item = screen.queryByTestId('test');
    expect(item).toHaveClass('custom');
  });

  it('passes the overflow', () => {
    renderComponent({ dataTestId: 'test', overflow: true });
    const item = screen.queryByTestId('test');
    expect(item).toHaveClass(classes.overflow);
  });

  it('renders children', () => {
    renderComponent({ children: <div>test</div> });
    const child = screen.queryByText('test');
    expect(child).toBeInTheDocument();
  });
});
