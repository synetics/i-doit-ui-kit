import classnames from 'classnames';
import { Container, ContainerProps } from '../../../Container';
import classes from './Content.module.scss';

type ContentType = {
  overflow?: boolean;
} & ContainerProps;

const Content = ({ className, children, overflow, ...props }: ContentType) => (
  <Container
    {...props}
    className={classnames(className, classes.content, {
      [classes.overflow]: overflow,
    })}
  >
    {children}
  </Container>
);

export { Content };
