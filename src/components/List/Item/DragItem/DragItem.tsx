import { ComponentType, forwardRef } from 'react';
import classnames from 'classnames';
import { Drag, DropArea } from '../../../DragAndDrop';
import { Item, ItemType } from '../Item';
import classes from './DragItem.module.scss';

type DragItemType = Omit<ItemType, 'onDrag' | 'onDrop'> & {
  component?: ComponentType<ItemType>;
  index: number;
  dragging?: boolean;
  type: string;
  onDrag: () => void;
  onDrop: (index: number) => void;
  onCancel: () => void;
};

const DragItem = forwardRef<HTMLDivElement, DragItemType>(
  (
    { className, component: Component = Item, children, dragging, index, onDrag, onDrop, onCancel, type, ...props },
    ref,
  ) => (
    <>
      <DropArea className={classes.dropArea} accept={[type]} onDrop={() => onDrop(index)} />
      <Drag disablePreview type={type} onDrag={onDrag} onCancel={onCancel} className={classes.item} hidden={dragging}>
        <Component {...props} className={classnames(className, classes.item)} ref={ref}>
          {children}
        </Component>
      </Drag>
      <DropArea className={classes.dropArea} accept={[type]} onDrop={() => onDrop(index + 1)} />
    </>
  ),
);

export { DragItem, DragItemType };
