import { render, screen } from '@testing-library/react';
import { DndProvider } from 'react-dnd';
import { KeyboardBackend } from '../../../DragAndDrop';
import { DragItem, DragItemType } from './DragItem';

const renderComponent = (props: Partial<DragItemType>) => {
  const actions = {
    onDrag: jest.fn(),
    onDrop: jest.fn(),
    onCancel: jest.fn(),
  };
  const result = render(
    <DndProvider backend={KeyboardBackend}>
      <DragItem
        index={0}
        type="Item"
        onCancel={actions.onCancel}
        onDrag={actions.onDrag}
        onDrop={actions.onDrop}
        {...props}
      />
    </DndProvider>,
    {},
  );

  return {
    actions,
    result,
  };
};

describe('<DragItem/>', () => {
  it('renders without crashing', () => {
    renderComponent({ children: 'test' });
    const item = screen.getByText('test');
    expect(item).toBeInTheDocument();
  });
  it('renders with custom renderer', () => {
    const TestComponent = () => <div data-testid="test-id">test</div>;
    renderComponent({ component: TestComponent });
    const testItem = screen.getByTestId('test-id');
    expect(testItem).toBeInTheDocument();
  });
});
