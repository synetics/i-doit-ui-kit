import classNames from 'classnames';
import { Icon } from '../../../Icon';
import { IconProps } from '../../../Icon/Icon';
import classes from './ItemIcon.module.scss';

const ItemIcon = ({ className, ...props }: IconProps) => (
  <Icon {...props} className={classNames(className, classes.icon)} />
);

export { ItemIcon };
