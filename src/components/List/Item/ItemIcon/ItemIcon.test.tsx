import { screen } from '@testing-library/react';
import { createRenderer } from '../../../../../test/utils/renderer';
import { ItemIcon } from './ItemIcon';

const renderComponent = createRenderer(ItemIcon, { src: 'icon' });

describe('<ItemIcon/>', () => {
  it('renders without crashing', () => {
    renderComponent();
    const item = screen.getByTestId('icon');
    expect(item).toBeInTheDocument();
  });

  it('passes the className', () => {
    renderComponent({ className: 'custom', src: 'icon' });
    const item = screen.queryByTestId('icon');
    expect(item).toHaveClass('custom');
  });
});
