import classnames from 'classnames';
import { Container, ContainerProps } from '../../../Container';
import classes from './Buttons.module.scss';

type ButtonsProps = ContainerProps & {
  visible?: boolean;
};

const Buttons = ({ className, visible, children, ...props }: ButtonsProps) => (
  <Container
    {...props}
    className={classnames(className, classes.content, {
      [classes.visible]: visible,
    })}
  >
    {children}
  </Container>
);

export { Buttons };
