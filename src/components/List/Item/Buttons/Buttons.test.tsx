import { screen } from '@testing-library/react';
import { createRenderer } from '../../../../../test/utils/renderer';
import { Buttons } from './Buttons';
import classes from './Buttons.module.scss';

const renderComponent = createRenderer(Buttons, {});

describe('<Buttons/>', () => {
  it('renders without crashing', () => {
    renderComponent({ dataTestId: 'test' });
    const item = screen.queryByTestId('test');
    expect(item).toBeInTheDocument();
    expect(item).not.toHaveClass(classes.visible);
  });

  it('passes the className', () => {
    renderComponent({ dataTestId: 'test', className: 'custom' });
    const item = screen.queryByTestId('test');
    expect(item).toHaveClass('custom');
  });

  it('reacts on visible', () => {
    renderComponent({ dataTestId: 'test', visible: true });
    const item = screen.queryByTestId('test');
    expect(item).toHaveClass(classes.visible);
  });

  it('renders children', () => {
    renderComponent({ children: <div>test</div> });
    const child = screen.queryByText('test');
    expect(child).toBeInTheDocument();
  });
});
