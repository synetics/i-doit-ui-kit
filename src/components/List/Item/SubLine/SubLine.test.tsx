import { screen } from '@testing-library/react';
import { createRenderer } from '../../../../../test/utils/renderer';
import { SubLine } from './SubLine';

const renderComponent = createRenderer(SubLine, {});

describe('<SubLine/>', () => {
  it('renders without crashing', () => {
    renderComponent({ dataTestId: 'test' });
    const item = screen.getByTestId('test');
    expect(item).toBeInTheDocument();
  });

  it('passes the className', () => {
    renderComponent({ dataTestId: 'test', className: 'custom' });
    const item = screen.queryByTestId('test');
    expect(item).toHaveClass('custom');
  });

  it('renders children', () => {
    renderComponent({ children: <div>test</div> });
    const child = screen.queryByText('test');
    expect(child).toBeInTheDocument();
  });
});
