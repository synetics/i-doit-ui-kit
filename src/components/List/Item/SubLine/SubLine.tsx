import classnames from 'classnames';
import { Container, ContainerProps } from '../../../Container';
import classes from './SubLine.module.scss';

const SubLine = ({ className, children, ...props }: ContainerProps) => (
  <Container {...props} className={classnames(className, classes.content)}>
    {children}
  </Container>
);

export { SubLine };
