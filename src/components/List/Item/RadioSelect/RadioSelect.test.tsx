import { screen } from '@testing-library/react';
import { createRenderer } from '../../../../../test/utils/renderer';
import { RadioSelect } from './RadioSelect';

const renderComponent = createRenderer(RadioSelect, { 'aria-label': 'label' });

describe('<RadioSelect/>', () => {
  it('renders without crashing', () => {
    renderComponent({ 'data-testid': 'test', 'aria-label': 'label' });
    const item = screen.getByTestId('test');
    expect(item).toBeInTheDocument();
  });

  it('passes the className', () => {
    renderComponent({ 'data-testid': 'test', className: 'custom', 'aria-label': 'label' });
    const item = screen.queryByTestId('test');
    expect(item).toHaveClass('custom');
  });

  it('renders children', () => {
    renderComponent({ children: <div>test</div>, 'aria-label': 'label' });
    const child = screen.queryByText('test');
    expect(child).toBeInTheDocument();
  });
});
