import { Radio } from '../../../Form/Control';
import { Item, ItemType } from '../Item';

type SelectorItemType = ItemType & {
  'aria-label': string;
  value?: string;
};

const RadioSelect = ({
  'aria-label': ariaLabel,
  disabled,
  selected,
  onSelect,
  children,
  ...props
}: SelectorItemType) => (
  <Item {...props} tabIndex={undefined} onClick={onSelect} disabled={disabled}>
    <Radio tabIndex={undefined} aria-label={ariaLabel} disabled={disabled} checked={selected} onChange={onSelect} />
    {children}
  </Item>
);

export { RadioSelect, SelectorItemType };
