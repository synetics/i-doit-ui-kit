import React, { ReactElement, ReactNode, RefObject, useImperativeHandle, useMemo, useRef, useState } from 'react';
import { isObjectWithShape, isString } from '@i-doit/enten-types';
import { useCallbackRef, useEvent, useFocusWithin, useKeyboardIndex } from '../../../hooks';
import { ValueProps } from '../../Form/Infrastructure';
import { ApiType, ScrollList } from '../../ScrollList';
import { ReturnType } from '../../../types/function';

type IdItemType = {
  id: string;
};

type RenderItemChildrenType<T extends IdItemType> = {
  item: T;
  nodeState: {
    focused: boolean;
    selected: boolean;
  };
  api: {
    select: () => void;
    focus: () => void;
  };
  ref: (element: HTMLElement) => void;
};

type CustomElement = {
  component: ReactElement;
};

const isItem = <T extends IdItemType>(item: unknown): item is T => isObjectWithShape({ id: isString })(item);

type RenderItemChildren<T extends IdItemType> = (props: RenderItemChildrenType<T>) => ReactNode;

type SingleSelectListType<T extends IdItemType> = Required<ValueProps<T | null>> & {
  className?: string;
  children: RenderItemChildren<T>;
  items: (T | CustomElement)[];
  apiRef?: RefObject<{
    select: (id: string) => void;
    focus: (id: string) => void;
    scrollToId: (index: string) => void;
    scrollToOffset: (index: string) => void;
  }>;
};

export const SingleSelectList = <T extends IdItemType>({
  children,
  items,
  value,
  onChange,
  className,
  apiRef: providedApiRef,
}: SingleSelectListType<T>): ReactElement => {
  const valuedItems: T[] = useMemo(() => items.filter((a: unknown): a is T => isItem<T>(a)), [items]);
  const selected = value?.id || null;
  const [ref, setRef] = useCallbackRef<HTMLDivElement>();
  const focusWithin = useFocusWithin(ref || null);
  const apiRef = useRef<ApiType>(null);
  const itemsRef = useRef<Record<string, HTMLElement>>({});
  const [focused, setFocused] = useState(valuedItems.findIndex((a) => a.id === selected));
  const timeoutRef = useRef<ReturnType<typeof setTimeout>>();
  const handleScrollToOffset = (id: string) => {
    const index = Math.max(
      0,
      items.findIndex((a) => isItem(a) && a.id === id),
    );

    apiRef.current?.scrollToOffset(index * 40);
  };
  const handleScrollTo = (id: string) => {
    const index = Math.max(
      0,
      items.findIndex((a) => isItem(a) && a.id === id),
    );
    apiRef.current?.scrollToIndex(index);
  };

  const handleUnFocus = () => setFocused(-1);
  const handleFocus = (id: string) => {
    const index = Math.max(
      0,
      valuedItems.findIndex((a) => a.id === id),
    );

    const indexAllItems = Math.max(
      0,
      items.findIndex((a) => isItem(a) && a.id === id),
    );

    setFocused(index);

    apiRef.current?.scrollToIndex(indexAllItems);
    const targetScrollItem = items[indexAllItems];
    if (timeoutRef.current) {
      clearTimeout(timeoutRef.current);
    }
    if (!isItem(targetScrollItem)) {
      return;
    }
    if (apiRef.current?.virtualItems?.some((a) => a.index === indexAllItems)) {
      itemsRef.current[targetScrollItem.id]?.focus();
    } else {
      timeoutRef.current = setTimeout(() => {
        itemsRef.current[valuedItems[indexAllItems].id]?.focus();
      }, 10);
    }
  };
  const handleSelect = (id: string) => {
    onChange(valuedItems.find((a) => a.id === id) || null);
  };
  const focus = useEvent(handleFocus);
  const unFocus = useEvent(handleUnFocus);
  const scrollToId = useEvent(handleScrollTo);
  const select = useEvent(handleSelect);
  const scrollToOffset = useEvent(handleScrollToOffset);
  const api = useMemo(
    () => ({
      focus,
      select,
      scrollToId,
      scrollToOffset,
      unFocus,
      ref,
    }),
    [focus, select, scrollToId, scrollToOffset, unFocus, ref],
  );
  useImperativeHandle(providedApiRef, () => api, [api]);

  useKeyboardIndex(
    focused,
    (i, e) => {
      handleFocus(valuedItems[i].id);
      e.preventDefault();
      e.stopPropagation();
    },
    valuedItems.length,
    ref || document.body,
    {},
  );

  return (
    <ScrollList
      className={className}
      ref={setRef}
      apiRef={apiRef}
      itemSize={40}
      size={items.length}
      tabIndex={focusWithin ? -1 : 0}
      onFocus={(e) => {
        if (e.target !== ref) {
          return;
        }
        handleFocus(selected || valuedItems[0]?.id || '');
      }}
    >
      {(i) => {
        const current = items[i];
        if (isItem(current)) {
          const { id } = current;

          return children({
            nodeState: {
              selected: id === selected,
              focused: focused === i,
            },
            ref: (itemRef) => {
              itemsRef.current[id] = itemRef;
            },
            item: current,
            api: {
              select: () => api.select(id),
              focus: () => api.focus(id),
            },
          });
        }
        return current.component;
      }}
    </ScrollList>
  );
};
