import { render, screen } from '@testing-library/react';
import React, { useEffect, useRef, useState } from 'react';
import user from '@testing-library/user-event';
import { Item } from '../Item';
import { generateItem } from '../../../utils/filter-tree/stories/generate';
import { SingleSelectList } from './SingleSelectList';

type ItemType = { id: string; title: string; children: string[] };

const generatedItems: ItemType[] = new Array(100).fill(0).map(() => generateItem());

const testItems: ItemType[] = [
  {
    id: 'root-1',
    title: 'First',
    children: [],
  },
  {
    id: 'root-2',
    title: 'Second',
    children: [],
  },
  {
    id: 'root-3',
    title: 'Third',
    children: [],
  },
];

type ApiType = {
  select: (id: string) => void;
  focus: (id: string) => void;
  scrollToId: (index: string) => void;
  scrollToOffset: (index: string) => void;
};

export const SingleSelectListExample = ({
  selectedItem,
  items,
  scrollTo,
}: {
  selectedItem?: ItemType;
  items?: ItemType[];
  scrollTo?: boolean;
}) => {
  const [value, setSelected] = useState<ItemType | null>(selectedItem || testItems[0]);
  const apiRef = useRef<null | ApiType>(null);
  useEffect(() => {
    if (!value?.id) {
      return;
    }
    if (apiRef.current && scrollTo) {
      apiRef.current.scrollToId(value?.id);
    }
    if (apiRef.current && !scrollTo) {
      apiRef.current.scrollToOffset(value?.id);
    }
  }, [value?.id, scrollTo]);
  return (
    <div className="d-flex flex-column flex-fill">
      <div className="overflow-auto flex-fill">
        <SingleSelectList apiRef={apiRef} items={items || testItems} value={value} onChange={setSelected}>
          {({ item, ref, api, nodeState: { selected } }) => (
            <Item
              id={item.id}
              key={item.id}
              data-testid={item.id}
              ref={(element) => {
                if (element) {
                  ref(element);
                }
              }}
              tabIndex={selected ? 0 : -1}
              selected={selected}
              onSelect={api.select}
              onFocus={api.focus}
            >
              {item?.title}
            </Item>
          )}
        </SingleSelectList>
      </div>
    </div>
  );
};

describe('SingleSelectList', () => {
  it('renders correctly', () => {
    render(<SingleSelectListExample />);
    expect(screen.queryByText('First')).toBeInTheDocument();
    expect(screen.queryByText('First')).toHaveAttribute('data-selected', 'true');
  });
  it('rerender after navigate and focus correct item', async () => {
    render(<SingleSelectListExample />);
    const root = screen.queryByText('First');
    await user.tab();
    expect(root).toHaveClass('selected');
    expect(root).toHaveFocus();
    await user.type(root || document.body, '{arrowdown}');
    expect(screen.queryByText('Second')).toHaveFocus();
  });
  it('rerender without selected id', async () => {
    render(<SingleSelectListExample selectedItem={{ id: '', title: 'Test', children: [] }} />);
    const root = screen.queryByText('First');
    await user.tab();
    await new Promise((resolve) => setTimeout(resolve, 100));
    expect(root).toHaveFocus();
  });

  it('scroll scrollToOffset', () => {
    render(<SingleSelectListExample selectedItem={generatedItems[79]} items={generatedItems} />);
    expect(screen.queryByText(generatedItems[79].title)).toBeInTheDocument();
    expect(screen.queryByText(generatedItems[79].title)).toHaveAttribute('data-selected', 'true');
  });

  it('scroll scrollToId', () => {
    render(<SingleSelectListExample scrollTo selectedItem={generatedItems[79]} items={generatedItems} />);
    expect(screen.queryByText(generatedItems[79].title)).toBeInTheDocument();
    expect(screen.queryByText(generatedItems[79].title)).toHaveAttribute('data-selected', 'true');
  });

  it('render custom item', () => {
    render(
      <SingleSelectListExample
        selectedItem={undefined}
        items={[{ component: <>test</> }, ...testItems] as ItemType[]}
      />,
    );
    expect(screen.queryByText('test')).toBeInTheDocument();
  });

  it('call onChange', async () => {
    const value = null;
    const setSelected = jest.fn();
    render(
      <div className="d-flex flex-column flex-fill">
        <div className="overflow-auto flex-fill">
          <SingleSelectList items={testItems} value={value} onChange={setSelected}>
            {({ item, ref, api, nodeState: { selected } }) => (
              <Item
                id={item.id}
                key={item.id}
                data-testid={item.id}
                ref={(element) => {
                  if (element) {
                    ref(element);
                  }
                }}
                tabIndex={selected ? 0 : -1}
                selected={selected}
                onSelect={api.select}
                onFocus={api.focus}
              >
                {item?.title}
              </Item>
            )}
          </SingleSelectList>
        </div>
      </div>,
    );

    const root = screen.getByText('First');
    await user.type(document.body, '{tab}');
    await user.type(root || document.body, '{arrowdown}');
    expect(setSelected).toHaveBeenCalledWith({ id: testItems[0].id, title: testItems[0].title, children: [] });
  });
});
