import React, { ReactNode } from 'react';
import { OverflowWithTooltip } from '../../OverflowWithTooltip';
import { Item } from '../Item';
import { SingleSelectList } from './SingleSelectList';

type ItemType = { id: string; title: string; view?: ReactNode };

type SelectInListProps<T extends ItemType> = {
  value: T | null;
  items: T[];
  onChange: (item: T | null) => void;
  className?: string;
};

export const SelectInList = <T extends ItemType>({ className, value, items, onChange }: SelectInListProps<T>) => (
  <SingleSelectList className={className} items={items} value={value} onChange={onChange}>
    {({ item, nodeState, api }) => (
      <Item
        tabIndex={nodeState.selected ? 0 : -1}
        selected={nodeState.selected}
        onSelect={api.select}
        onFocus={api.focus}
        id={item.id}
        key={item.id}
      >
        {item.view || <OverflowWithTooltip>{item.title}</OverflowWithTooltip>}
      </Item>
    )}
  </SingleSelectList>
);
