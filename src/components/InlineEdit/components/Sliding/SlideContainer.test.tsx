import { render, screen } from '@testing-library/react';
import { SlideContainer } from './SlideContainer';
import classes from './SlideContainer.module.scss';

describe('Slide container', () => {
  it('shows content on hover', async () => {
    render(
      <SlideContainer data-testid="container" content={<div>Content</div>}>
        <div>Children</div>
      </SlideContainer>,
    );

    const container = screen.getByTestId('container');
    expect(container).toBeInTheDocument();
    expect(screen.queryByText('Content')).toBeInTheDocument();
    expect(screen.queryByText('Children')).toBeInTheDocument();
    expect(screen.getByText('Content').parentElement).toHaveClass(classes.sliding);
  });

  it('shows content if it is active', () => {
    render(
      <SlideContainer active data-testid="container" content={<div>Content</div>}>
        <div>Children</div>
      </SlideContainer>,
    );

    expect(screen.queryByText('Content')).toBeVisible();
    expect(screen.queryByText('Children')).toBeInTheDocument();
    expect(screen.getByText('Content').parentElement).toHaveClass(classes.sliding);
  });
});
