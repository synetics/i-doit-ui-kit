import React, { FC, HTMLProps, ReactNode } from 'react';
import classnames from 'classnames';
import { Placement } from '@popperjs/core';
import { useCallbackRef } from '../../../../hooks';
import { Popper, SizeChangeModifier } from '../../../Popper';
import classes from './SlideContainer.module.scss';

export type SlideContainerProps = {
  active?: boolean;
  content: ReactNode;
  placement?: Placement;
} & Omit<HTMLProps<HTMLDivElement>, 'content'>;

const modifiers = [
  SizeChangeModifier,
  {
    name: 'flip',
    options: {
      fallbackPlacements: ['bottom-end', 'top-end', 'left-start'],
      flipVariations: false,
      altBoundary: true,
    },
  },
];

type SliderContainerShape = FC<SlideContainerProps> & { showOnHover: string; className: string };

export const SlideContainer: SliderContainerShape = ({
  active = false,
  placement = 'right-start',
  children,
  content,
  ...rest
}: SlideContainerProps) => {
  const [ref, setRef] = useCallbackRef<HTMLDivElement>();
  return (
    <div {...rest} ref={setRef} className={classes.trigger}>
      {children}
      <Popper referenceElement={ref} open={!!ref} options={{ placement, modifiers }}>
        <div
          className={classnames(classes.sliding, classes.showOnHover, {
            [classes.active]: active,
          })}
        >
          {content}
        </div>
      </Popper>
    </div>
  );
};

SlideContainer.className = classes.trigger;
SlideContainer.showOnHover = classes.showOnHover;
