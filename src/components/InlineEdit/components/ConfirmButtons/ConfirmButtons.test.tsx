import { act, render, screen } from '@testing-library/react';
import user from '@testing-library/user-event';
import { ConfirmButtons, ConfirmButtonsProps } from './ConfirmButtons';

describe('Confirm Buttons', () => {
  const renderButtons = (props: Omit<ConfirmButtonsProps, 'onCancel' | 'onConfirm'>) => {
    const cancel = jest.fn();
    const confirm = jest.fn();
    render(<ConfirmButtons {...props} onCancel={cancel} onConfirm={confirm} />);

    return {
      action: { cancel, confirm },
      buttons: { cancel: screen.getByTestId('cancel'), confirm: screen.getByTestId('confirm') },
    };
  };
  it('renders buttons', () => {
    render(<ConfirmButtons onCancel={jest.fn()} onConfirm={jest.fn()} />);

    expect(screen.queryByTestId('confirm')).toBeInTheDocument();
    expect(screen.queryByTestId('cancel')).toBeInTheDocument();
  });

  it('calls confirm on click', () => {
    const { action, buttons } = renderButtons({});

    buttons.confirm.click();
    expect(action.confirm).toHaveBeenCalled();
  });

  it('calls confirm on enter', async () => {
    const { action, buttons } = renderButtons({});

    await act(() => Promise.resolve(user.type(buttons.confirm, '{enter}')));

    expect(action.confirm).toHaveBeenCalled();
  });

  it('calls cancel on click', () => {
    const { action, buttons } = renderButtons({});

    buttons.cancel.click();
    expect(action.cancel).toHaveBeenCalled();
  });

  it('calls cancel on enter', async () => {
    const { action, buttons } = renderButtons({});

    await act(() => Promise.resolve(user.type(buttons.cancel, '{enter}')));

    expect(action.cancel).toHaveBeenCalled();
  });

  it('does not call actions on enter/click if disabled', async () => {
    const { action, buttons } = renderButtons({ disabled: true });

    buttons.confirm.click();
    await act(() => Promise.resolve(user.type(buttons.confirm, '{enter}')));

    buttons.cancel.click();
    await act(() => Promise.resolve(user.type(buttons.cancel, '{enter}')));

    expect(action.confirm).not.toHaveBeenCalled();
    expect(action.cancel).not.toHaveBeenCalled();
  });

  it('can use own button names', async () => {
    renderButtons({
      confirmLabel: 'Confirm label',
      cancelLabel: 'Cancel label',
    });

    expect(screen.getByText('Confirm label')).toBeInTheDocument();
    expect(screen.getByText('Cancel label')).toBeInTheDocument();
  });
});
