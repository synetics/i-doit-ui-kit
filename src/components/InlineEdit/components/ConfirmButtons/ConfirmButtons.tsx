import React, { HTMLProps, SyntheticEvent, useMemo } from 'react';
import classnames from 'classnames';
import { check, close } from '../../../../icons';
import { handleKey } from '../../../../utils';
import { IconButton } from '../../../IconButton';
import { Icon } from '../../../Icon';
import classes from './ConfirmButtons.module.scss';

export type ConfirmButtonsProps = {
  disabled?: boolean;
  className?: string;
  loading?: boolean;
  onCancel: (e: SyntheticEvent) => void;
  onConfirm: (e: SyntheticEvent) => void;
  confirmLabel?: string;
  cancelLabel?: string;
} & HTMLProps<HTMLDivElement>;

export const ConfirmButtons = ({
  className,
  cancelLabel = 'Cancel',
  confirmLabel = 'Confirm',
  disabled,
  loading,
  onCancel,
  onConfirm,
  ...rest
}: ConfirmButtonsProps) => {
  const isDisabled = disabled || loading;
  const handleConfirm = useMemo(() => handleKey({ Enter: onConfirm }, true, true), [onConfirm]);
  const handleCancel = useMemo(() => handleKey({ Enter: onCancel }), [onCancel]);

  return (
    <div {...rest} className={classnames(className, classes.container)}>
      <IconButton
        data-testid="confirm"
        onKeyDown={isDisabled ? undefined : handleConfirm}
        className={classnames(classes.confirmButton, classes.btn)}
        disabled={isDisabled}
        icon={<Icon src={check} />}
        label={confirmLabel}
        loading={loading}
        variant="primary"
        onClick={onConfirm}
      />
      <IconButton
        contrast
        data-testid="cancel"
        className={classes.btn}
        onKeyDown={isDisabled ? undefined : handleCancel}
        disabled={isDisabled}
        icon={<Icon src={close} />}
        label={cancelLabel}
        variant="outline"
        onClick={onCancel}
      />
    </div>
  );
};
