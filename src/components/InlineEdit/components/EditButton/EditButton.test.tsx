import { render, screen } from '@testing-library/react';
import { EditButton, EditButtonProps } from './EditButton';

describe('Edit button', () => {
  const renderButton = (props: Omit<EditButtonProps, 'ref' | 'onClick'>) => {
    const click = jest.fn();
    render(<EditButton data-testid="edit-button" {...props} onClick={click} />);
    return {
      click,
      button: screen.getByTestId('edit-button'),
    };
  };
  it('renders without crash', () => {
    const api = renderButton({});

    expect(api.button).toBeInTheDocument();
    expect(api.click).not.toHaveBeenCalled();
  });

  it('calls click callback', () => {
    const api = renderButton({});

    api.button.click();
    expect(api.click).toHaveBeenCalled();
  });
});
