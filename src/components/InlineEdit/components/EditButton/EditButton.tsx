import React, { forwardRef, ReactElement } from 'react';
import { edit as editIcon } from '../../../../icons';
import { Icon } from '../../../Icon';
import { IconButton, IconButtonProps } from '../../../IconButton';

export type EditButtonProps = Omit<IconButtonProps, 'label' | 'icon'> & {
  label?: string;
  icon?: ReactElement;
};

const defaultIcon = <Icon src={editIcon} />;

export const EditButton = forwardRef<HTMLButtonElement, EditButtonProps>(
  ({ label = 'Edit', icon = defaultIcon, variant = 'outline', ...props }, forwardedRef) => (
    <IconButton {...props} icon={icon} ref={forwardedRef} variant={variant} label={label} aria-label={label} />
  ),
);
