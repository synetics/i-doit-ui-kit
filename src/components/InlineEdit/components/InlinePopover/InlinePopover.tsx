import React, { ReactNode, SyntheticEvent } from 'react';
import FocusTrap from 'focus-trap-react';
import { Placement } from '@popperjs/core';
import { useCallbackRef, useEvent } from '../../../../hooks';
import { handleKey } from '../../../../utils';
import { Popper, SameWidthModifierAndSizeChange, ScrollParentModifier } from '../../../Popper';
import { ConfirmButtons } from '../ConfirmButtons';
import { SlideContainer } from '../Sliding';
import classes from './InlinePopover.module.scss';

type InlinePopoverProps = {
  onConfirm: () => void;
  onClose: () => void;
  loading: boolean;
  placement?: Placement;
  children: ReactNode | ReactNode[];
};

export const InlinePopover = ({ onConfirm, onClose, loading, children, placement }: InlinePopoverProps) => {
  const [ref, setRef] = useCallbackRef<HTMLDivElement>();
  const handleConfirm = useEvent((e: SyntheticEvent) => {
    onConfirm();
    e.stopPropagation();
  });
  const handleClose = useEvent((e: SyntheticEvent): void => {
    onClose();
    e.stopPropagation();
  });

  return (
    <>
      <div ref={setRef} />
      <Popper
        open
        referenceElement={ref}
        className={classes.popper}
        options={{
          placement: 'bottom-start',
          modifiers: [SameWidthModifierAndSizeChange, ScrollParentModifier],
        }}
      >
        <FocusTrap
          active
          paused={loading}
          focusTrapOptions={{
            clickOutsideDeactivates: true,
            escapeDeactivates: true,
            onDeactivate: onClose,
          }}
        >
          <div
            role="presentation"
            className={classes.editing}
            onKeyDown={handleKey({
              Enter: handleConfirm,
            })}
            onFocus={(e) => e.stopPropagation()}
          >
            <SlideContainer
              active
              placement={placement}
              content={
                <ConfirmButtons
                  className={classes.buttons}
                  loading={loading}
                  onCancel={handleClose}
                  onConfirm={handleConfirm}
                />
              }
            >
              {children}
            </SlideContainer>
          </div>
        </FocusTrap>
      </Popper>
    </>
  );
};
