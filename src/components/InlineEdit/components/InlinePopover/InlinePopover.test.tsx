import { act, render, screen } from '@testing-library/react';
import user from '@testing-library/user-event';
import { InlinePopover } from './InlinePopover';

describe('InlinePopover', () => {
  const renderButtons = (loading: boolean) => {
    const cancel = jest.fn();
    const confirm = jest.fn();
    const { rerender } = render(
      <InlinePopover onConfirm={confirm} onClose={cancel} loading={loading}>
        <button>Content</button>
      </InlinePopover>,
    );

    return {
      action: {
        confirm,
        cancel,
      },
      button: {
        confirm: screen.getByTestId('confirm'),
        cancel: screen.getByTestId('cancel'),
      },
      rerender: (newLoading: boolean) =>
        rerender(
          <InlinePopover onConfirm={confirm} onClose={cancel} loading={newLoading}>
            <div>Content</div>
          </InlinePopover>,
        ),
    };
  };

  it('renders the content', () => {
    const {
      button: { confirm, cancel },
    } = renderButtons(false);

    expect(screen.queryByText('Content')).toBeInTheDocument();
    expect(confirm).toBeInTheDocument();
    expect(cancel).toBeInTheDocument();
  });

  it('click on confirm calls callback', () => {
    const { action, button } = renderButtons(false);
    button.confirm.click();

    expect(action.cancel).not.toHaveBeenCalled();
    expect(action.confirm).toHaveBeenCalled();
  });
  it('enter on confirm calls callback', async () => {
    const { action, button } = renderButtons(false);

    await act(() => Promise.resolve(user.type(button.confirm, '{enter}')));

    expect(action.cancel).not.toHaveBeenCalled();
    expect(action.confirm).toHaveBeenCalled();
  });

  it('click on cancel calls callback', () => {
    const { action, button } = renderButtons(false);

    button.cancel.click();

    expect(action.cancel).toHaveBeenCalled();
    expect(action.confirm).not.toHaveBeenCalled();
  });
  it('enter on cancel calls callback', async () => {
    const { action, button } = renderButtons(false);

    await act(() => Promise.resolve(user.type(button.cancel, '{enter}')));

    expect(action.cancel).toHaveBeenCalled();
    expect(action.confirm).not.toHaveBeenCalled();
  });
  it('does not react while loading', async () => {
    const { action, button, rerender } = renderButtons(false);

    rerender(true);

    button.cancel.click();
    button.confirm.click();

    await act(() => Promise.resolve(user.type(button.cancel, '{enter}')));
    await act(() => Promise.resolve(user.type(button.confirm, '{enter}')));

    expect(action.cancel).not.toHaveBeenCalled();
    expect(action.confirm).not.toHaveBeenCalled();
  });
});
