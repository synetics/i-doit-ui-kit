import { useMemo, useRef, useState } from 'react';
import { name } from 'faker';
import { Canvas, Meta, Story } from '@storybook/blocks';
import { useEventListener, useToggle } from '../../../hooks';
import { Checkbox, Input } from '../../Form/Control';
import { Error, FieldError, FieldGroup, FormGroup, Label } from '../../Form/Infrastructure';
import { CellWrapper, Table } from '../../Table';
import { ConfirmButtons, EditButton, InlinePopover, SlideContainer } from '../components';
import { useInlineEdit } from '../hooks';
import { Footer, Header, LoadingScreen, Modal, ModalError, Wrapper } from '../../Modal';
import { Button, ButtonSpacer } from '../../Button';
import classnames from 'classnames';
import { handleKey } from '../../../utils';

<Meta title="Components/Molecules/Inline Edit" />

export const delayedSave = (onSave) =>
  new Promise((resolve, reject) => {
    setTimeout(() => {
      try {
        onSave();
        resolve();
      } catch (e) {
        reject(e);
      }
    }, 2000);
  });

export const Editing = ({ loading, error, api, value }) => (
  <InlinePopover loading={loading} onConfirm={api.confirm} onClose={api.cancel}>
    <FormGroup>
      <FieldGroup className="my-0" variant={error ? 'has-error' : 'has-border'}>
        <Input disabled={loading} id="input" onChange={api.setValue} value={value} />
        {error && <FieldError />}
      </FieldGroup>
      {error && (
        <Error id="input" className="mt-1">
          {error}
        </Error>
      )}
    </FormGroup>
  </InlinePopover>
);

# Inline Edit

Inline Edit allows user to directly edit values.

It consists of the following parts:

## useInlineEdit hook

The hook allows you to integrate inline editing.

### Usage:

```
const [enabled, { loading, value, error }, api] = useInlineEdit(initialValue, handleSave);
```

### Arguments

- value - initial value
- handleSave - async callback that receives the modified value and either resolves or throws an error

### Result

As a result, you receive an array with the following entries:

1. boolean - is the inline edit is currently enabled
2. the state of the inline edit - current modified value, is it in loading state, what is the error
3. api - methods to interact with inline edit: enable, cancel, confirm and setValue

## Examples

### Usage with form controls

<Canvas>
  <Story name="Usage with form controls">
    {() => {
      const ref = useRef();
      const [value, setValue] = useState('123');
      const handleSave = (newValue) =>
        delayedSave(() => {
          if (Math.random() > 0.5) {
            setValue(newValue);
            return;
          }
          throw 'Incorrect value';
        });
      const [enabled, { loading, value: current, error }, api] = useInlineEdit(value, handleSave);
      return (
        <div style={{ width: 300 }}>
          <FormGroup>
            <Label id="firstName">First name</Label>
            {enabled && <Editing value={current} api={api} error={error} loading={loading} />}
            <SlideContainer content={<EditButton className="m-1" showOnHover onClick={api.enable} />}>
              <FieldGroup
                variant="regular"
                ref={ref}
                onDoubleClick={api.enable}
                onKeyDown={handleKey({
                  Enter: enabled ? undefined : api.enable,
                })}
              >
                <Input tabIndex={0} value={value} readonly />
              </FieldGroup>
            </SlideContainer>
          </FormGroup>
        </div>
      );
    }}
  </Story>
</Canvas>

### Usage with tables

<Canvas>
  <Story name="Table">
    {() => {
      const [showButtons, buttonsApi] = useToggle(false);
      const [data, setData] = useState(() =>
        new Array(10).fill(0).map((_, i) => ({
          id: `id-${i}`,
          name: name.firstName(),
          lastName: name.lastName(),
        })),
      );
      const Cell = useMemo(
        () =>
          ({ value, row }) => {
            const ref = useRef(null);
            const handleSave = (newValue) =>
              delayedSave(() =>
                setData((data) => {
                  const current = data.findIndex((a) => a.id === row.id);
                  const newData = [...data];
                  newData[current].name = newValue;
                  return newData;
                }),
              );
            const [enabled, { loading, error, value: current }, api] = useInlineEdit(value, handleSave);
            const button = (
              <EditButton
                contrast
                className={classnames({
                  'm-1': showButtons,
                  [SlideContainer.showOnHover]: !showButtons,
                })}
                variant="text"
                onClick={api.enable}
                onFocus={(a) => a.stopPropagation()}
              />
            );
            useEventListener(
              'keydown',
              handleKey({
                Enter: enabled ? undefined : api.enable,
              }),
              ref.current?.parentElement || null,
              false,
            );
            return (
              <>
                {enabled && <Editing value={current} api={api} error={error} loading={loading} />}
                {!showButtons && (
                  <CellWrapper ref={ref} onDoubleClick={api.enable}>
                    <span className="flex-fill">{value}</span>
                    {button}
                  </CellWrapper>
                )}
                {showButtons && (
                  <SlideContainer content={button}>
                    <CellWrapper ref={ref} onDoubleClick={api.enable}>
                      <span className="flex-fill">{value}</span>
                    </CellWrapper>
                  </SlideContainer>
                )}
              </>
            );
          },
        [showButtons],
      );
      const columns = useMemo(
        () =>
          new Array(10).fill(1).map((_, i) => ({
            id: `col${i}`,
            Header: 'Name',
            accessor: 'name',
            Cell,
            width: 400,
            className: SlideContainer.className,
          })),
        [Cell],
      );
      return (
        <div style={{ minHeight: 300 }}>
          <div className="mb-2">
            <Checkbox
              checked={showButtons}
              onChangeAfter={buttonsApi.toggle}
              label="Use sliding for edit buttons"
              aria-label="Use sliding for edit buttons"
            />
          </div>
          <Table columns={columns} data={data} getRowId={(a) => a.id} />
        </div>
      );
    }}
  </Story>
</Canvas>

### Custom complex form - form with dependencies

<Canvas>
  <Story name="Form with dependencies">
    {() => {
      const [firstName, setFirstName] = useState('Max');
      const [lastName, setLastName] = useState('Mustermann');
      const initialValue = useMemo(() => ({ firstName, lastName }), [firstName, lastName]);
      const [enabled, { loading, value, error }, api] = useInlineEdit(initialValue, ({ firstName, lastName }) =>
        delayedSave(() => {
          const saveError = {};
          if ((firstName?.length || 0) === 0) {
            saveError.firstName = 'Field cannot be empty';
          }
          if ((lastName?.length || 0) === 0) {
            saveError.lastName = 'Field cannot be empty';
          }
          if (Object.keys(saveError).length > 0) {
            saveError.common = 'Please, check the marked fields';
            throw saveError;
          }
          setFirstName(firstName);
          setLastName(lastName);
        }),
      );
      return (
        <div style={{ width: 300 }}>
          <FormGroup className="my-2">
            <Label id="firstName">Name</Label>
            <SlideContainer content={<EditButton className="m-1" variant="outline" onClick={api.enable} />}>
              <FieldGroup
                variant="regular"
                onDoubleClick={api.enable}
                tabIndex={0}
                onKeyDown={handleKey({
                  Enter: enabled ? undefined : api.enable,
                })}
              >
                <span className="flex-fill">
                  {firstName} {lastName}
                </span>
              </FieldGroup>
            </SlideContainer>
          </FormGroup>
          {enabled && (
            <Modal
              open
              loading={loading}
              size="m"
              focusTrapOptions={{ escapeDeactivates: true, onDeactivate: api.cancel }}
            >
              {loading && (
                <LoadingScreen>
                  <h2>Saving...</h2>
                </LoadingScreen>
              )}
              <Header onClose={api.cancel}>Edit personal information</Header>
              {error?.common && <ModalError>{error?.common}</ModalError>}
              <Wrapper>
                <FormGroup className="my-2">
                  <Label id="firstName">First name</Label>
                  <Input
                    autoFocus
                    id="firstName"
                    variant={error?.firstName ? 'has-error' : 'has-border'}
                    value={value.firstName}
                    onChange={(a) =>
                      api.setValue({
                        ...value,
                        firstName: a,
                      })
                    }
                  />
                  {error?.firstName && <Error id={firstName}>{error?.firstName}</Error>}
                </FormGroup>
                <FormGroup className="my-2">
                  <Label id="lastName">Last name</Label>
                  <Input
                    id="lastName"
                    variant={error?.lastName ? 'has-error' : 'has-border'}
                    value={value.lastName}
                    onChange={(a) =>
                      api.setValue({
                        ...value,
                        lastName: a,
                      })
                    }
                  />
                  {error?.lastName && <Error id={firstName}>{error?.lastName}</Error>}
                </FormGroup>
              </Wrapper>
              <Footer>
                <ButtonSpacer gridSize="sm">
                  <Button variant="primary" onClick={api.confirm} disabled={loading}>
                    Save
                  </Button>
                  <Button variant="text" onClick={api.cancel} disabled={loading}>
                    Cancel
                  </Button>
                </ButtonSpacer>
              </Footer>
            </Modal>
          )}
        </div>
      );
    }}
  </Story>
</Canvas>

## Quick edit of attributes with dependencies

<Canvas>
  <Story name="Quick edit of attributes with dependencies">
    {() => {
      const [value, setValue] = useState({ firstName: 'Max', lastName: 'Mustermann' });
      const handleSave = (newValue) => delayedSave(() => setValue(newValue));
      const [enabled, { value: current, loading }, api] = useInlineEdit(value, handleSave);
      const setField = (field) => (v) =>
        api.setValue({
          ...current,
          [field]: v,
        });
      return (
        <div style={{ width: 300 }}>
          <div className="p-2">
            <FormGroup className="my-2">
              <Label id="static">Read-only</Label>
              <FieldGroup variant="regular" readonly>
                <span className="flex-fill">Static value</span>
              </FieldGroup>
            </FormGroup>
            {enabled && (
              <>
                <div style={{ position: 'absolute', left: 0, right: 0, top: 0, bottom: 0 }} />
                <InlinePopover loading={loading} onClose={api.cancel} onConfirm={api.confirm} placement="bottom-end">
                  <div
                    className="rounded withShadow border p-2"
                    style={{ background: '#EEEEEE', marginTop: -17, marginLeft: -9 }}
                  >
                    <FormGroup className="my-2">
                      <Label id="firstName">First name</Label>
                      <Input value={current.firstName} onChange={setField('firstName')} />
                    </FormGroup>
                    <FormGroup className="my-2">
                      <Label id="lastName">Last name</Label>
                      <Input value={current.lastName} onChange={setField('lastName')} />
                    </FormGroup>
                  </div>
                </InlinePopover>
              </>
            )}
            <FormGroup className="my-2">
              <Label id="firstName">Name</Label>
              <SlideContainer content={<EditButton className="m-1" variant="outline" onClick={api.enable} />}>
                <FieldGroup
                  variant="regular"
                  onDoubleClick={api.enable}
                  tabIndex={0}
                  onKeyDown={handleKey({
                    Enter: enabled ? undefined : api.enable,
                  })}
                >
                  <span className="flex-fill">
                    {value.firstName} {value.lastName}
                  </span>
                </FieldGroup>
              </SlideContainer>
            </FormGroup>
            <FormGroup className="my-2">
              <Label id="static">Read-only</Label>
              <FieldGroup variant="regular" readonly>
                <span className="flex-fill">Static value</span>
              </FieldGroup>
            </FormGroup>
            <FormGroup className="my-2">
              <Label id="static">Read-only</Label>
              <FieldGroup variant="regular" readonly>
                <span className="flex-fill">Static value</span>
              </FieldGroup>
            </FormGroup>
          </div>
        </div>
      );
    }}
  </Story>
</Canvas>

## Helper components

### Confirm Buttons

Confirm buttons are shown when inline edit is active.

<Canvas>
  <Story name="Confirm Buttons">
    {() => {
      const [disabled, disabledApi] = useToggle(false);
      const [loading, loadingApi] = useToggle(false);
      return (
        <div style={{ maxWidth: 200 }}>
          <div className="mb-2">
            <Checkbox checked={disabled} onChangeAfter={disabledApi.toggle} label="Disabled" aria-label="Disabled" />
          </div>
          <div className="mb-2">
            <Checkbox checked={loading} onChangeAfter={loadingApi.toggle} label="Loading" aria-label="Loading" />
          </div>
          <div className="m-4">
            <ConfirmButtons disabled={disabled} loading={loading} onConfirm={() => null} onCancel={() => null} />
          </div>
        </div>
      );
    }}
  </Story>
</Canvas>

### Edit Button

Edit button is a primitive to start the editing

<Canvas>
  <Story name="Edit Button">
    {() => {
      const [label, setLabel] = useState('Edit');
      const [disabled, disabledApi] = useToggle(false);
      const [loading, loadingApi] = useToggle(false);
      return (
        <div style={{ maxWidth: 200 }}>
          <div className="mb-2">
            <Label id="label">Custom label</Label>
            <Input id="label" value={label} onChange={setLabel} />
          </div>
          <div className="mb-2">
            <Checkbox checked={disabled} onChangeAfter={disabledApi.toggle} label="Disabled" aria-label="Disabled" />
          </div>
          <div className="mb-2">
            <Checkbox checked={loading} onChangeAfter={loadingApi.toggle} label="Loading" aria-label="Loading" />
          </div>
          <div className="m-4">
            <EditButton label={label} disabled={disabled} loading={loading} onClick={() => null} />
          </div>
        </div>
      );
    }}
  </Story>
</Canvas>

## Sliding animation

<Canvas>
  <Story name="SlideContainer">
    <div style={{ maxWidth: 200 }}>
      <SlideContainer content={<EditButton className="m-1" />}>
        <FieldGroup>
          <Input />
        </FieldGroup>
      </SlideContainer>
    </div>
  </Story>
</Canvas>
