import { act, renderHook } from '@testing-library/react-hooks';
import { useInlineEdit } from './use-inline-edit';

describe('use inline edit', () => {
  it('has correct initial state', () => {
    const processSave = jest.fn();
    const { result } = renderHook(() => useInlineEdit('initial', processSave));
    const [enabled, state] = result.current;
    expect(enabled).toBe(false);
    expect(state.error).toBe(null);
    expect(state.value).toBe('initial');
    expect(state.loading).toBe(false);
  });

  it('enables editing', async () => {
    const processSave = jest.fn();
    const { result } = renderHook(() => useInlineEdit('initial', processSave));
    act(() => result.current[2].enable());
    expect(result.current[0]).toBe(true);
  });

  it('can set value', async () => {
    const processSave = jest.fn();
    const { result } = renderHook(() => useInlineEdit<string>('initial', processSave));
    act(() => {
      result.current[2].enable();
      result.current[2].setValue('test');
    });
    expect(result.current[1].value).toBe('test');
  });

  it('calls save with correct parameters', async () => {
    const processSave = jest.fn();
    const { result } = renderHook(() => useInlineEdit<string>('initial', processSave));
    act(() => {
      result.current[2].enable();
      result.current[2].setValue('test');
    });
    await act(() => result.current[2].confirm());
    expect(processSave).toHaveBeenCalledWith('test');
    expect(result.current[0]).toBe(false);
  });

  it('handles error', async () => {
    const processSave = () => {
      // eslint-disable-next-line @typescript-eslint/no-throw-literal
      throw 'Error';
    };
    const { result } = renderHook(() => useInlineEdit<string>('initial', processSave));
    act(() => {
      result.current[2].enable();
      result.current[2].setValue('test');
    });
    await act(() => result.current[2].confirm());
    expect(result.current[1].error).toBe('Error');
    expect(result.current[0]).toBe(true);
  });

  it('handles cancel', async () => {
    const processSave = jest.fn();
    const { result } = renderHook(() => useInlineEdit<string>('initial', processSave));
    act(() => {
      result.current[2].enable();
      result.current[2].setValue('test');
      result.current[2].cancel();
    });
    expect(result.current[0]).toBe(false);
  });
  it('should cancel and reset localValue if loading is true', async () => {
    const onSaveMock = jest.fn();
    const value = 'initialValue';
    const { result } = renderHook(() => useInlineEdit(value, onSaveMock));

    act(() => result.current[2].enable());
    // eslint-disable-next-line @typescript-eslint/no-floating-promises
    act(() => result.current[2].confirm());

    expect(result.current[1].loading).toBe(true);

    act(() => result.current[2].cancel());

    expect(result.current[1].loading).toBe(true);
    expect(result.current[1].value).toBe(value);

    await act(() => result.current[2].confirm());

    expect(result.current[1].loading).toBe(false);
  });
});
