import { useMemo, useState } from 'react';
import { useEvent, useToggle } from '../../../hooks';

type InlineEditState<T, E> = {
  loading: boolean;
  error: E | null;
  value: T;
};

export type InlineEditApi<T> = {
  enable: () => void;
  cancel: () => void;
  confirm: () => Promise<void>;
  setValue: (value: T) => void;
};

type UseInlineEditResult<T, E> = [boolean, InlineEditState<T, E>, InlineEditApi<T>];

export const useInlineEdit = <T, E = string>(
  value: T,
  onSave: (value: T) => Promise<void>,
): UseInlineEditResult<T, E> => {
  const [enabled, enabledApi] = useToggle(false);
  const [loading, loadingApi] = useToggle(false);
  const [localValue, setLocalValue] = useState(value);
  const [error, setError] = useState<E | null>(null);
  const handleEnable = useEvent(() => {
    setLocalValue(value);
    enabledApi.enable();
  });
  const handleConfirm = useEvent(async () => {
    loadingApi.enable();
    try {
      await onSave(localValue);
      enabledApi.disable();
    } catch (e) {
      setError(e as E);
    }
    loadingApi.disable();
  });
  const handleCancel = useEvent(() => {
    if (loading) {
      return;
    }
    setLocalValue(value);
    setError(null);
    enabledApi.disable();
  });
  const api = useMemo(
    () => ({
      enable: handleEnable,
      cancel: handleCancel,
      confirm: handleConfirm,
      setValue: (newValue: T) => {
        setLocalValue(newValue);
        setError(null);
      },
    }),
    [setLocalValue, handleConfirm, handleEnable, handleCancel],
  );
  const state = useMemo(
    () => ({
      error,
      loading,
      value: localValue,
    }),
    [error, loading, localValue],
  );
  return [enabled, state, api];
};
