import { ReactNode } from 'react';
import { bug } from '../../icons';
import { Icon } from './Icon';
import './Story.scss';

export default {
  title: 'Components/Atoms/Icon',
  component: Icon,
};

/**
 *
 * The icon color can be defined by `fill` props.
 *
 */
export const Color = (): ReactNode => (
  <>
    <Icon src={bug} wrapper="span" width={40} fill="white" />
    <Icon src={bug} wrapper="span" width={40} fill="black" />
    <Icon src={bug} wrapper="span" width={40} fill="warning" />
    <Icon src={bug} wrapper="span" width={40} fill="error" />
    <Icon src={bug} wrapper="span" width={40} fill="success" />
    <Icon src={bug} wrapper="span" width={40} fill="neutral-100" />
    <Icon src={bug} wrapper="span" width={40} fill="neutral-200" />
    <Icon src={bug} wrapper="span" width={40} fill="neutral-300" />
    <Icon src={bug} wrapper="span" width={40} fill="neutral-500" />
    <Icon src={bug} wrapper="span" width={40} fill="neutral-700" />
    <Icon src={bug} wrapper="span" width={40} fill="neutral-900" />
    <Icon src={bug} wrapper="span" width={40} fill="active" />
  </>
);

/**
 *
 * The icon size can be defined by `width` and `height` props.
 * The default icon size is 24 by 24 pixels.
 *
 */
export const Size = (): ReactNode => (
  <>
    <span>Default size (24x24):</span>
    <br />
    <Icon src={bug} wrapper="span" className="showcase" />
    <hr />
    <span>Width 40:</span>
    <br />
    <Icon src={bug} wrapper="span" className="showcase" width={40} />
    <hr />
    <span>Height 60:</span>
    <br />
    <Icon src={bug} wrapper="span" className="showcase" height={60} />
    <hr />
    <span>Width 100, height 60:</span>
    <br />
    <Icon src={bug} wrapper="span" className="showcase" width={100} height={60} />
    <hr />
    <span>Width 80, height 100:</span>
    <br />
    <Icon src={bug} wrapper="span" className="showcase" width={80} height={100} />
  </>
);

/**
 *
 * The wrapper property allows specifying by what html tag the icon should be surrounded.
 * By default, we use `div` html element.
 *
 */
export const Wrapper = (): ReactNode => (
  <>
    <h1>block (default)</h1>
    <h2>Div</h2>
    <Icon src={bug} wrapper="div" className="showcase" />
    <Icon src={bug} wrapper="div" className="showcase" width={40} />
    <Icon src={bug} wrapper="div" className="showcase" height={40} />
    <Icon src={bug} wrapper="div" className="showcase" width={60} height={60} />
  </>
);

/**
 *
 * Highlight allows you to highlight parts of the text.
 *
 */
export const Span = (): ReactNode => (
  <>
    <Icon src={bug} wrapper="span" className="showcase" />
    <Icon src={bug} wrapper="span" className="showcase" width={40} />
    <Icon src={bug} wrapper="span" className="showcase" height={40} />
    <Icon src={bug} wrapper="span" className="showcase" width={60} height={60} />
  </>
);
