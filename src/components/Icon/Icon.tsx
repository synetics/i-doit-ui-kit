import React from 'react';
import classnames from 'classnames';
import { ReactSVG } from 'react-svg';
import styles from './Icon.module.scss';

export type IconProps = {
  /**
   * An extra className to be added to the root element.
   */
  className?: string;

  /**
   * Fills the svg icon with a color by the given name from the color set.
   */
  fill?: string;

  /**
   * Specifies the height of the root element.
   */
  height?: string | number;

  /**
   * A callback function that will be invoked after the user clicked the icon.
   */
  onClick?: () => void;

  /**
   * A callback function that will be invoked after the user clicked the icon.
   */
  onMouseDown?: () => void;

  /**
   * Path to the svg source.
   */
  src: string;

  /**
   * A className to be added to the svg element.
   */
  svgClassName?: string;

  /**
   * Specifies the width of the root element.
   */
  width?: string | number;

  /**
   * Specifies a html tag of the root element.
   */
  wrapper?: 'div' | 'span';

  /**
   * Specifies data-testid property for testing lib.
   */
  dataTestId?: string;
};

export const beforeInjection =
  ({ svgClassName, fill }: Partial<IconProps>) =>
  (svg: SVGElement): void => {
    if (svgClassName) {
      svg.classList.add(svgClassName);
    }

    if (fill) {
      svg.classList.add(styles[`path-fill-${fill}`]);
    }
  };

export const Icon = ({
  className,
  fill = 'neutral-700',
  height,
  onClick,
  onMouseDown,
  src,
  svgClassName,
  width,
  wrapper,
  dataTestId = 'icon',
}: IconProps) => (
  <ReactSVG
    className={classnames(styles.icon, className, { [styles.pointer]: !!onClick })}
    onClick={onClick}
    onMouseDown={onMouseDown}
    style={{
      width: width || height || 24,
      height: height || width || 24,
    }}
    aria-hidden
    wrapper={wrapper}
    src={src}
    beforeInjection={beforeInjection({ svgClassName, fill })}
    data-testid={dataTestId}
  />
);
