import { render, screen } from '@testing-library/react';
import React from 'react';
import user from '@testing-library/user-event';
import { beforeInjection, Icon } from './Icon';

const { spyOn } = jest;

describe('Icon', () => {
  it('renders without crashing', () => {
    render(<Icon src="image/src" />);

    expect(screen.getByTestId('icon')).toBeInTheDocument();
  });

  it('sets default width and height', () => {
    render(<Icon src="image/src" />);

    expect(screen.getByTestId('icon')).toHaveStyle({ width: '24px', height: '24px' });
  });

  it('sets width and height based on given width', () => {
    render(<Icon src="image/src" width={30} />);

    expect(screen.getByTestId('icon')).toHaveStyle({ width: '30px', height: '30px' });
  });

  it('sets width and height based on given height', () => {
    render(<Icon src="image/src" height={40} />);

    expect(screen.getByTestId('icon')).toHaveStyle({ width: '40px', height: '40px' });
  });

  it('sets given width and height', () => {
    render(<Icon src="image/src" width={15} height={15} />);
  });

  it('renders wrapped by given element', () => {
    render(<Icon src="image/src" wrapper="span" />);

    expect(screen.getByTestId('icon').tagName.toLowerCase()).toBe('span');
  });

  it('adds given className to a wrapper element', () => {
    render(<Icon src="image/src" className="className" />);

    expect(screen.getByTestId('icon')).toHaveClass('className');
  });

  it('renders interactive', async () => {
    const handleClickMock = jest.fn();
    render(<Icon src="image/src" onClick={handleClickMock} />);

    await user.click(screen.getByTestId('icon'));

    expect(handleClickMock).toHaveBeenCalledTimes(1);
  });

  it('renders hidden in the accessibility tree', () => {
    render(<Icon src="image/src" />);

    expect(screen.getByTestId('icon')).toHaveAttribute('aria-hidden', 'true');
  });
});

describe('beforeInjection', () => {
  it('does not add the svgClassName and the fill props', () => {
    const svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    const addSpy = spyOn(svg.classList, 'add');

    beforeInjection({ svgClassName: undefined, fill: undefined })(svg);

    expect(addSpy).not.toHaveBeenCalled();
  });

  it('adds given className to the svg', () => {
    const svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    const addSpy = spyOn(svg.classList, 'add');

    beforeInjection({ svgClassName: 'svgClassName' })(svg);

    expect(addSpy).toHaveBeenCalledTimes(1);
    expect(addSpy).toHaveBeenCalledWith('svgClassName');
  });

  it('generates a className by given color name and adds it to the svg', () => {
    const svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    const addSpy = spyOn(svg.classList, 'add');

    beforeInjection({ fill: 'color-name' })(svg);

    expect(addSpy).toHaveBeenCalledTimes(1);
    expect(addSpy).toHaveBeenCalledWith('path-fill-color-name');
  });

  it('adds given className to the svg along with generated color name', () => {
    const svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    const addSpy = spyOn(svg.classList, 'add');

    beforeInjection({ svgClassName: 'svgClassName', fill: 'color-name' })(svg);

    expect(addSpy).toHaveBeenCalledTimes(2);
    expect(addSpy).nthCalledWith(1, 'svgClassName');
    expect(addSpy).nthCalledWith(2, 'path-fill-color-name');
  });
});
