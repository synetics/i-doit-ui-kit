import { render, screen } from '@testing-library/react';
import { TransitionProps } from 'react-transition-group/Transition';
import { ReactElement } from 'react';
import { Button } from '../Button';
import { Input } from '../Form/Control';
import { Footer, Wrapper } from '.';
import { Modal, Size } from './Modal';

type MyTransition = TransitionProps & {
  in: boolean;
  children: () => ReactElement;
};

jest.mock('react-transition-group', () => ({
  Transition: ({ in: opened, children }: MyTransition) => (opened ? children('entered') : null),
}));

const options = { fallbackFocus: () => document.body };

describe('Modal', () => {
  // Prepare a portal target.
  const portalElement = document.createElement('div');
  portalElement.setAttribute('id', 'portal');
  document.body.append(portalElement);

  it('renders without crashing', () => {
    render(
      <Modal open portal={portalElement} focusTrapOptions={options}>
        Hello
      </Modal>,
    );

    expect(screen.getByTestId('portal-backdrop')).toBeInTheDocument();
    expect(screen.getByTestId('portal-container')).toBeInTheDocument();
    expect(screen.getByTestId('portal-dialog')).toBeInTheDocument();
  });

  it('renders with default size "m"', () => {
    render(
      <Modal open portal={portalElement} focusTrapOptions={options}>
        Hello
      </Modal>,
    );

    expect(screen.getByTestId('portal-dialog')).toHaveClass('size-m');
  });

  it.each<[Size]>([['s'], ['m'], ['l'], ['xl'], ['xxl']])('renders with sizes "%s"', (size) => {
    render(
      <Modal open portal={portalElement} size={size} focusTrapOptions={options}>
        Hello
      </Modal>,
    );

    expect(screen.getByTestId('portal-dialog')).toHaveClass(`size-${size}`);
  });

  it('does not render when closed', () => {
    render(<Modal portal={portalElement}>Hello</Modal>);

    expect(screen.queryByTestId('portal-backdrop')).not.toBeInTheDocument();
    expect(screen.queryByTestId('portal-container')).not.toBeInTheDocument();
  });

  it('focused if autoFocus passed', async () => {
    const buttonContent = 'Test';
    render(
      <Modal portal={portalElement} open autoFocus>
        <Footer>
          <Button>{buttonContent}</Button>
        </Footer>
      </Modal>,
    );
    await new Promise((resolve) => setTimeout(resolve, 100));

    expect(screen.getByText(buttonContent)).toHaveFocus();
  });

  it('any other element can be focused', async () => {
    const buttonContent = 'Test';
    const testId = 'focusInput';
    render(
      <Modal portal={portalElement} open autoFocus>
        <Wrapper>
          <Input autoFocus id={testId} />
        </Wrapper>
        <Footer>
          <Button>{buttonContent}</Button>
        </Footer>
      </Modal>,
    );

    expect(screen.getByTestId(testId)).toHaveFocus();
  });
});
