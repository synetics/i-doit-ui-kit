import { ReactElement, ReactNode } from 'react';
import classnames from 'classnames';
import { BannerHeader } from '../../Banner/BannerHeader';
import styles from './Error.module.scss';

type ErrorProps = {
  /**
   * Path to the svg source.
   */
  icon?: string;
  /**
   * Class of the container
   */
  className?: string;
  /**
   * Children
   */
  children?: ReactNode | ReactNode[];
};

const ModalError = ({ icon, children, className }: ErrorProps): ReactElement => (
  <BannerHeader className={classnames(styles.error, className)} headerIcon={icon} variant="error">
    {children}
  </BannerHeader>
);

export { ModalError };
