import { render, screen } from '@testing-library/react';
import { info } from '../../../icons';
import { ModalError } from './ModalError';

describe('Modal Error', () => {
  it('renders without crashing', () => {
    render(
      <ModalError>
        <p>Paragraph</p>
      </ModalError>,
    );

    expect(screen.getByText('Paragraph')).toBeInTheDocument();
  });

  it('renders with  Icon', () => {
    render(<ModalError icon={info}>Example</ModalError>);

    expect(screen.getByTestId('icon')).toBeInTheDocument();
  });

  it('renders with className', () => {
    render(<ModalError className="test">Example</ModalError>);

    expect(screen.getByTestId('BannerHeader')).toHaveClass('test');
  });
});
