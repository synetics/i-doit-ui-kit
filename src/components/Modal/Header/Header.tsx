import { PropsWithChildren, ReactElement } from 'react';
import classnames from 'classnames';
import { Icon } from '../../Icon';
import { close } from '../../../icons';
import { handleKey } from '../../../utils/helpers';
import { IconButton } from '../../IconButton';
import styles from './Header.module.scss';

type HeaderProps = PropsWithChildren<{
  /**
   * Callback for the 'close' action
   */
  onClose?: () => void;

  /**
   * Classname for container
   */
  className?: string;

  /**
   * The label of the button
   */
  closeLabel?: string;
}>;

const Header = ({ children, onClose, className, closeLabel = 'Close' }: HeaderProps): ReactElement => (
  <div data-testid="modal-content-header" className={classnames(styles.modalHeader, className)}>
    {children}
    {onClose && (
      <IconButton
        variant="text"
        label={closeLabel}
        onClick={onClose}
        onKeyDown={handleKey({ Enter: onClose })}
        icon={<Icon className={styles.closeIcon} src={close} />}
      />
    )}
  </div>
);

export { Header };
