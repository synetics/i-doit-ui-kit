import { render, screen } from '@testing-library/react';
import user from '@testing-library/user-event';
import { Header } from './Header';

describe('Modal Footer', () => {
  it('renders without crashing', () => {
    render(
      <Header>
        <p>Paragraph</p>
      </Header>,
    );

    expect(screen.getByText('Paragraph')).toBeInTheDocument();
    expect(screen.getByTestId('modal-content-header')).toBeInTheDocument();
  });

  it('renders without close icon button', () => {
    render(
      <Header>
        <p>Paragraph</p>
      </Header>,
    );

    expect(screen.queryByTestId('icon')).not.toBeInTheDocument();
    expect(screen.queryByTestId('idoit-tooltip')).not.toBeInTheDocument();
  });

  it('renders a close icon button', () => {
    render(
      <Header onClose={() => undefined}>
        <p>Paragraph</p>
      </Header>,
    );

    expect(screen.getByTestId('icon')).toBeInTheDocument();
    expect(screen.getByTestId('idoit-tooltip')).toBeInTheDocument();
  });

  it('renders a custom close text', () => {
    render(
      <Header onClose={() => undefined} closeLabel="Schließen">
        <p>Paragraph</p>
      </Header>,
    );

    const label = screen.getByTestId('idoit-tooltip');
    expect(label).toHaveTextContent('Schließen');
  });

  it('click a close triggers closing', async () => {
    const onClose = jest.fn();
    render(
      <Header onClose={onClose}>
        <p>Paragraph</p>
      </Header>,
    );

    const button = screen.getByTestId('icon');
    await user.click(button);
    expect(onClose).toHaveBeenCalledTimes(1);
  });
});
