import { useState, useEffect, ReactNode } from 'react';
import { Button } from '../Button';
import { Input } from '../Form/Control/Input';
import { info } from '../../icons';
import { SingleSelect } from '../Form/Control';
import { Modal } from './Modal';
import { Header } from './Header';
import { Footer } from './Footer';
import { Wrapper } from './Content';
import { ModalError } from './ModalError';
import { LoadingScreen } from './LoadingScreen';

const items = new Array(100).fill(1).map((_, i) => ({ value: `${i}`, label: `Item ${i}` }));

export default {
  title: 'Components/Molecules/Modal',
  component: Modal,
};

export const Small = (): ReactNode => {
  const [open, setOpen] = useState(false);
  const [value, setValue] = useState(items[0]);

  return (
    <>
      <p>Content behind the modal</p>
      <Button variant="primary" onClick={() => setOpen(!open)}>
        Small modal open
      </Button>
      <Modal open={open} portal={document.getElementById('portal-root')!} size="s">
        <Header>Small modal</Header>
        <Wrapper>
          <p>Small modal body</p>
          <p>Small modal body</p>
          <p className="mb-0">Small modal body</p>
          <SingleSelect
            items={items}
            value={value}
            onChange={(a) => {
              if (a) {
                setValue(a);
              }
            }}
          />
        </Wrapper>
        <Footer>
          <Button variant="primary" className="mr-4">
            Save
          </Button>
          <Button variant="text" onClick={() => setOpen(false)}>
            Cancel
          </Button>
        </Footer>
      </Modal>
    </>
  );
};

/**
 *
 * Medium modal (default)
 *
 */
export const Medium = (): ReactNode => {
  const [open, setOpen] = useState(false);

  return (
    <>
      <p>Content behind the modal</p>
      <Button variant="primary" onClick={() => setOpen(!open)}>
        Medium modal open
      </Button>
      <Modal open={open} portal={document.getElementById('portal-root')!}>
        <Header>Medium modal</Header>
        <Wrapper>
          <p>Medium modal body</p>
          <p>Medium modal body</p>
          <p className="mb-0">Medium modal body</p>
        </Wrapper>
        <Footer>
          <Button variant="primary" className="mr-4">
            Save
          </Button>
          <Button variant="text" onClick={() => setOpen(false)}>
            Cancel
          </Button>
        </Footer>
      </Modal>
    </>
  );
};

/**
 *
 * Large modal
 *
 */

export const Large = (): ReactNode => {
  const [open, setOpen] = useState(false);

  return (
    <>
      <p>Content behind the modal</p>
      <Button variant="primary" onClick={() => setOpen(!open)}>
        Large modal open
      </Button>
      <Modal open={open} portal={document.getElementById('portal-root')!} size="l">
        <Header>Large modal</Header>
        <Wrapper>
          <p>Large modal body</p>
          <p>Large modal body</p>
          <p className="mb-0">Large modal body</p>
        </Wrapper>
        <Footer>
          <Button variant="primary" className="mr-4">
            Save
          </Button>
          <Button variant="text" onClick={() => setOpen(false)}>
            Cancel
          </Button>
        </Footer>
      </Modal>
    </>
  );
};

/**
 *
 * Extra Large modal
 *
 */

export const ExtraLarge = (): ReactNode => {
  const [open, setOpen] = useState(false);

  return (
    <>
      <p>Content behind the modal</p>
      <Button variant="primary" onClick={() => setOpen(!open)}>
        Extra large modal open
      </Button>
      <Modal open={open} portal={document.getElementById('portal-root')!} size="xl">
        <Header>Extra large</Header>
        <Wrapper>
          <p>Extra large body</p>
          <p>Extra large body</p>
          <p className="mb-0">Extra large body</p>
        </Wrapper>
        <Footer>
          <Button variant="primary" className="mr-4">
            Save
          </Button>
          <Button variant="text" onClick={() => setOpen(false)}>
            Cancel
          </Button>
        </Footer>
      </Modal>
    </>
  );
};

/**
 *
 * Extra extra Large modal
 *
 */

export const ExtraExtraLarge = (): ReactNode => {
  const [open, setOpen] = useState(false);

  return (
    <>
      <p>Content behind the modal</p>
      <Button variant="primary" onClick={() => setOpen(!open)}>
        Extra extra large modal open
      </Button>
      <Modal open={open} portal={document.getElementById('portal-root')!} size="xxl">
        <Header>Extra extra large</Header>
        <Wrapper>
          <p>Extra extra large body</p>
          <p>Extra extra large body</p>
          <p className="mb-0">Extra extra large body</p>
        </Wrapper>
        <Footer>
          <Button variant="primary" className="mr-4">
            Save
          </Button>
          <Button variant="text" onClick={() => setOpen(false)}>
            Cancel
          </Button>
        </Footer>
      </Modal>
    </>
  );
};

/**
 *
 * The maximum height of a modal should be 85% of the screen.
 *
 */

export const MaximumHeight = (): ReactNode => {
  const [open, setOpen] = useState(false);

  return (
    <>
      <p>Content behind the modal</p>
      <Button variant="primary" onClick={() => setOpen(!open)}>
        Maximum height modal open
      </Button>
      <Modal open={open} portal={document.getElementById('portal-root')!}>
        <Header>Medium modal</Header>
        <Wrapper>
          <p>
            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et
            dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet
            clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet,
            consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat,
            sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no
            sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing
            elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At
            vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est
            Lorem ipsum dolor sit amet.
          </p>
          <p>
            Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu
            feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril
            delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet, consectetuer adipiscing
            elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
          </p>
          <p>
            Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea
            commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat,
            vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit
            praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.
          </p>
        </Wrapper>
        <Footer>
          <Button variant="primary" className="mr-4">
            Save
          </Button>
          <Button variant="text" onClick={() => setOpen(false)}>
            Cancel
          </Button>
        </Footer>
      </Modal>
    </>
  );
};

/**
 *
 * The maximum height of a modal should be 85% of the screen.
 *
 */
export const WithError = (): ReactNode => {
  const [open, setOpen] = useState(false);
  const [error, setError] = useState('');
  const [inputError1, setInputError1] = useState(false);
  const [inputError2, setInputError2] = useState(false);
  const [input, setInput] = useState('');
  const [input2, setInput2] = useState('');

  useEffect(() => {
    if (!input.length) {
      setInputError1(true);
    }

    if (!input2.length) {
      setInputError2(true);
    }

    if (input2.length) {
      setInputError2(false);
    }

    if (input.length) {
      setInputError1(false);
    }
  }, [input, input2]);

  useEffect(() => {
    if (inputError1 || inputError2) {
      return setError('Entry could not be saved. Please check the marked fields.');
    }

    return setError('');
  }, [inputError1, inputError2]);

  return (
    <>
      <p>Content behind the modal</p>
      <Button variant="primary" onClick={() => setOpen(!open)}>
        Modal with error state open
      </Button>
      <Modal size="l" open={open} portal={document.getElementById('portal-root')!}>
        <Header>Large modal</Header>
        {error && <ModalError icon={info}>{error}</ModalError>}
        <Wrapper>
          <Input
            id="ErrorModal"
            placeholder="Name"
            value={input}
            onChange={(e) => {
              if (e) {
                setInput(`${e}`);
              }
            }}
            variant={inputError1 ? 'has-error' : 'has-border'}
          />
          <Input
            id="ErrorModal2"
            type="password"
            placeholder="Password"
            value={input2}
            onChange={(e) => {
              if (e) {
                setInput2(`${e}`);
              }
            }}
            variant={inputError2 ? 'has-error' : 'has-border'}
          />
          <p>
            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et
            dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet
            clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet,
            consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat,
            sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no
            sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing
            elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At
            vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est
            Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod
            tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo
            duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.
            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et
            dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet
            clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet,
            consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat,
            sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no
            sea takimata sanctus est Lorem ipsum dolor sit amet.
          </p>
          <p>
            Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu
            feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril
            delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet, consectetuer adipiscing
            elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
          </p>
          <p>
            Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea
            commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat,
            vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit
            praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.
          </p>
        </Wrapper>
        <Footer>
          <Button variant="primary" className="mr-4">
            Save
          </Button>
          <Button variant="text" onClick={() => setOpen(false)}>
            Cancel
          </Button>
        </Footer>
      </Modal>
    </>
  );
};

/**
 *
 * Modal with Loading
 *
 */

export const ModalWithLoading = (): ReactNode => {
  const [open, setOpen] = useState(false);
  const [loading, setLoading] = useState(false);
  const [text, setText] = useState('');

  const onOpenHandler = () => {
    setOpen(!open);
    setLoading(true);
    setTimeout(() => setLoading(false), 3000);
  };

  return (
    <>
      <p>Content behind the modal</p>
      <Input
        className="w-50"
        id="inputModal"
        value={text}
        onChange={(t) => {
          if (t) {
            setText(`${t}`);
          }
        }}
        placeholder="For custom text in LoadingScreen"
      />
      <Button variant="primary" onClick={onOpenHandler}>
        Medium modal open
      </Button>
      <Modal loading={loading} size="xxl" open={open} portal={document.getElementById('portal-root')!}>
        {loading && <LoadingScreen text={text || 'Loading data...'} />}
        <>
          <Header>Modal with 3 seconds Loading</Header>
          <Wrapper>
            <p>Medium modal body</p>
            <p>Medium modal body</p>
            <p className="mb-0">Medium modal body</p>
          </Wrapper>
          <Footer>
            <Button variant="primary" className="mr-4">
              Save
            </Button>
            <Button variant="text" onClick={() => setOpen(false)}>
              Cancel
            </Button>
          </Footer>
        </>
      </Modal>
    </>
  );
};

/**
 *
 * LoadingScreen without Modal
 *
 */
export const LoadingScreenWithoutModal = (): ReactNode => {
  const [loading, setLoading] = useState(false);

  const onOpenHandler = () => {
    setLoading(true);
    setTimeout(() => setLoading(false), 30);
  };

  return (
    <>
      <p>Content behind the LoadingScreen</p>
      <Button variant="primary" onClick={onOpenHandler}>
        LoadingScreen open for 3 seconds
      </Button>
      <div
        style={{
          width: '100%',
          height: 300,
          backgroundColor: 'red',
          position: 'relative',
        }}
      >
        {loading && <LoadingScreen text="Loading..." />}
      </div>
    </>
  );
};

/**
 *
 *  Modal with initial focus button element inside
 *
 *  Focus default
 *
 */

export const FocusDefault = (): ReactNode => {
  const [open, setOpen] = useState(false);

  return (
    <>
      <p>Content behind the modal</p>
      <Button variant="primary" onClick={() => setOpen(!open)}>
        Modal with default focus
      </Button>
      <Modal open={open} portal={document.getElementById('portal-root')!} size="s">
        <Header onClose={() => null}>Small modal</Header>
        <Wrapper>
          <p>Focus modal body</p>
          <p className="mb-0">Focus modal body</p>
        </Wrapper>
        <Footer>
          <Button variant="primary" className="mr-4">
            Save
          </Button>
          <Button variant="text" onClick={() => setOpen(false)}>
            Cancel
          </Button>
        </Footer>
      </Modal>
    </>
  );
};

export const FocusCustom = (): ReactNode => {
  const [open, setOpen] = useState(false);

  return (
    <>
      <p>Pass focus to input</p>
      <Button variant="primary" onClick={() => setOpen(!open)}>
        Open modal
      </Button>
      <Modal open={open} autoFocus portal={document.getElementById('portal-root')!} size="s">
        <Header onClose={() => null}>Small modal</Header>
        <Wrapper>
          <Input autoFocus id="focusInput" placeholder="Focus input" />
        </Wrapper>
        <Footer>
          <Button variant="primary" className="mr-4">
            Save
          </Button>
          <Button variant="text" onClick={() => setOpen(false)}>
            Cancel
          </Button>
        </Footer>
      </Modal>
    </>
  );
};

export const FocusNoElems = (): ReactNode => {
  const [open, setOpen] = useState(false);

  return (
    <>
      <p>Content behind the modal</p>
      <Button variant="primary" onClick={() => setOpen(!open)}>
        No elems for focus
      </Button>
      <Modal
        focusTrapOptions={{
          fallbackFocus: () => document.body,
        }}
        open={open}
        portal={document.getElementById('portal-root')!}
        size="s"
      >
        <Wrapper>
          <p>Focus modal body</p>
          <p className="mb-0">Focus modal body</p>
        </Wrapper>
      </Modal>
    </>
  );
};
