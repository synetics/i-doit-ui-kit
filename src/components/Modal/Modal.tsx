import { PropsWithChildren, ReactElement } from 'react';
import classnames from 'classnames';
import { Transition } from 'react-transition-group';
import { Options as FocusTrapOptions } from 'focus-trap';
import FocusTrap from 'focus-trap-react';
import { Portal } from '../Portal';
import styles from './Modal.module.scss';

// S = 3 columns (formerly Modal 400)
// M = 4 columns (formerly Modal 600)
// L = 5 columns (formerly Modal 800)
// XL = 8 columns (new Modal)
// XXL = 10 columns (new Modal)

type Size = 's' | 'm' | 'l' | 'xl' | 'xxl';

type ModalProps = PropsWithChildren<{
  /**
   * Class name
   */
  className?: string;
  /**
   * Class name of the content wrapper
   */
  contentClassName?: string;
  /**
   * Actually display the modal
   */
  open?: boolean;

  /**
   * The portal element
   */
  portal: Element;

  /**
   * Represents the size of Modal
   */
  size?: Size;

  /**
   * Can be Modal autofocused
   */

  autoFocus?: boolean;

  /**
   * Is the modal in the loading state
   */
  loading?: boolean;

  /**
   * options which can can control focus trap behavior
   */

  focusTrapOptions?: FocusTrapOptions;
}>;

const initialFocus = '[data-testid="modal-content-footer"] > button';

const Modal = ({
  className,
  contentClassName,
  children,
  loading,
  open = false,
  portal,
  size = 'm',
  autoFocus = true,
  focusTrapOptions,
}: ModalProps): ReactElement => {
  const content = (
    <Transition appear in={open} timeout={{ appear: 0, enter: 0, exit: 150 }} mountOnEnter unmountOnExit>
      {(state) => (
        <>
          <div data-testid="portal-backdrop" className={classnames(styles.modalBackdrop, styles[`state-${state}`])} />
          <FocusTrap active={open && autoFocus} focusTrapOptions={{ initialFocus, ...focusTrapOptions }}>
            <div data-testid="portal-container" className={classnames(styles.modal, styles[`state-${state}`])}>
              <div
                data-testid="portal-dialog"
                className={classnames(className, styles.modalDialog, styles[`size-${size}`], {
                  [styles.loading]: loading,
                })}
              >
                <div data-testid="portal-content" className={classnames(contentClassName, styles.modalContent)}>
                  {children}
                </div>
              </div>
            </div>
          </FocusTrap>
        </>
      )}
    </Transition>
  );
  return portal ? <Portal portal={portal}>{content}</Portal> : content;
};

export { Modal, Size };
