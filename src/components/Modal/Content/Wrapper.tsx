import { forwardRef, HTMLAttributes, ReactElement } from 'react';
import classnames from 'classnames';
import { useGlobalizedRef, useToggle } from '../../../hooks';
import styles from './Wrapper.module.scss';

type WrapperProps = {
  /**
   * Classname for container
   */
  className?: string;
} & HTMLAttributes<HTMLDivElement>;

const Wrapper = forwardRef<HTMLDivElement, WrapperProps>(({ className, children, ...rest }, ref): ReactElement => {
  const [, { toggle }] = useToggle(false);
  const { localRef: container, setRefsCallback } = useGlobalizedRef(ref);
  const scrollTop = container.current?.scrollTop || 0;
  const currentScrollHeight = container.current?.scrollHeight || 0;
  const currentClientHeight = container.current?.clientHeight || 0;
  const scrollBottom = Math.floor(currentScrollHeight - scrollTop - currentClientHeight);

  return (
    <div
      {...rest}
      ref={setRefsCallback}
      data-testid="modal-content-wrapper"
      onScroll={toggle}
      className={classnames(styles.modalContent, className, {
        [styles.borderTop]: scrollTop !== 0,
        [styles.borderBottom]: scrollBottom !== 0,
      })}
    >
      {children}
    </div>
  );
});

export { Wrapper };
