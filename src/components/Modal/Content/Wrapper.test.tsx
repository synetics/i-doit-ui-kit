import { render, screen } from '@testing-library/react';
import { Wrapper } from './Wrapper';

describe('Modal Wrapper', () => {
  it('renders without crashing', () => {
    render(
      <Wrapper>
        <p>Paragraph</p>
      </Wrapper>,
    );

    expect(screen.getByText('Paragraph')).toBeInTheDocument();
    expect(screen.getByTestId('modal-content-wrapper')).toBeInTheDocument();
  });

  it.skip('sets scroll data', () => {
    render(
      <div style={{ position: 'relative', width: 50, height: 50, maxWidth: 50, maxHeight: 50, overflow: 'scroll' }}>
        <Wrapper>
          <p>Paragraph</p>
          <p>Paragraph</p>
          <p>Paragraph</p>
          <p>Paragraph</p>
          <p>Paragraph</p>
        </Wrapper>
      </div>,
    );

    const contentWrapper = screen.getByTestId('modal-content-wrapper');

    expect(contentWrapper).toHaveAttribute('data-scrolltop', '0');

    // @see https://github.com/testing-library/react-testing-library/issues/671
    // fireEvent.scroll(contentWrapper, {target: {scrollY: 10}});
    // expect(contentWrapper).toHaveAttribute('data-scrolltop', '10');
  });
});
