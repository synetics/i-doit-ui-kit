import { ReactElement, ReactNode } from 'react';
import { Spinner } from '../../Spinner';
import { Headline } from '../../Headline';
import styles from './LoadingScreen.module.scss';

type LoadingProps = {
  /**
   * Loading Text
   */
  text?: string;
  /**
   * Content for component
   */
  children?: ReactNode | string;
};

const LoadingScreen = ({ text, children }: LoadingProps): ReactElement => (
  <div className={styles.loadingContainer}>
    <Spinner className={styles.spinner} />
    <Headline tag="h2">{text}</Headline>
    {children}
  </div>
);

export { LoadingScreen };
