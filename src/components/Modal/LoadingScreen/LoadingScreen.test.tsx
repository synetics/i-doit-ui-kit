import { render, screen } from '@testing-library/react';
import { LoadingScreen } from './LoadingScreen';

describe('<LoadingScreen/>', () => {
  it('renders without crashing', () => {
    render(
      <LoadingScreen>
        <p>Paragraph</p>
      </LoadingScreen>,
    );

    expect(screen.getByText('Paragraph')).toBeInTheDocument();
  });

  it('renders with HeaderText', () => {
    render(<LoadingScreen text="Test" />);

    expect(screen.getByText('Test')).toBeInTheDocument();
  });
});
