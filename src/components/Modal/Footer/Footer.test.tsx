import { render, screen } from '@testing-library/react';
import { Footer } from './Footer';

describe('Modal Footer', () => {
  it('renders without crashing', () => {
    render(
      <Footer>
        <p>Paragraph</p>
      </Footer>,
    );

    expect(screen.getByText('Paragraph')).toBeInTheDocument();
    expect(screen.getByTestId('modal-content-footer')).toBeInTheDocument();
  });
});
