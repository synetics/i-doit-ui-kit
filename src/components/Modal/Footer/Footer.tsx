import { PropsWithChildren, ReactElement } from 'react';
import classnames from 'classnames';
import styles from './Footer.module.scss';

type FooterProps = PropsWithChildren<{
  /**
   * Classname for container
   */
  className?: string;
}>;

const Footer = ({ children, className }: FooterProps): ReactElement => (
  <div data-testid="modal-content-footer" className={classnames(styles.modalFooter, className)}>
    {children}
  </div>
);

export { Footer };
