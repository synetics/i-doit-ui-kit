import React, { ReactElement, useMemo } from 'react';
import { CurveFactory } from 'd3-shape';
import { curveStep } from '@visx/curve';
import { AreaStack } from '@visx/shape';
import { scaleLinear } from '@visx/scale';

export type AreaStackChartType<Data> = {
  data: Data[];
  columns: string[];
  colors: Record<string, string>;
  curve?: CurveFactory;
  width: number;
  height: number;
  horizontal?: boolean;
};

export const AreaStackChart = <Data,>({
  columns,
  data,
  width,
  height,
  colors,
  curve = curveStep,
  horizontal = false,
  ...rest
}: AreaStackChartType<Data>): ReactElement => {
  const xMax = horizontal ? width : height;
  const yMax = horizontal ? height : width;
  const xScale = useMemo(
    () =>
      scaleLinear({
        nice: true,
        domain: [0, data.length],
        range: [0, xMax],
      }),
    [data.length, xMax],
  );
  const yScale = useMemo(
    () =>
      scaleLinear({
        domain: [0, 1],
        range: [0, yMax],
      }),
    [yMax],
  );
  return (
    <AreaStack<Data>
      {...rest}
      curve={curve}
      offset="expand"
      data={data}
      keys={columns}
      color={(key) => colors[key]}
      y0={(a, i) => (horizontal ? yScale(a[0]) : xScale(i))}
      y1={(a, i) => (horizontal ? yScale(a[1]) : xScale(i))}
      x0={(a, i) => (horizontal ? xScale(i) : yScale(a[0]))}
      x1={(a, i) => (horizontal ? xScale(i) : yScale(a[1]))}
    />
  );
};
