import { render } from '@testing-library/react';
import { AreaStackChart, AreaStackChartType } from './AreaStackChart';

type Data = {
  a: number;
  b: number;
};

const data = [
  { a: 1, b: 2 },
  { a: 12, b: 5 },
  { a: 10, b: 20 },
];

const colorMap = {
  a: 'white',
  b: 'red',
};

describe('AreaStackChart', () => {
  const create = (props: Partial<AreaStackChartType<Data>>) => {
    const result = render(
      <svg width={400} height={400}>
        <AreaStackChart
          data={data}
          columns={['a', 'b']}
          colors={colorMap}
          width={100}
          height={200}
          {...props}
          data-testid="chart"
        />
      </svg>,
    );

    return {
      lines: result.getAllByTestId('chart'),
    };
  };
  it('renders without error', () => {
    const { lines } = create({});

    expect(lines.length).toBe(2);
  });
  it('renders horizontally without error', () => {
    const { lines } = create({ horizontal: true });

    expect(lines.length).toBe(2);
  });
});
