import { ReactNode, useEffect, useMemo, useRef, useState } from 'react';
import * as Curve from '@visx/curve';
import { FormGroup, Label } from '../../Form/Infrastructure';
import { ApiType, ScrollList } from '../../ScrollList';
import { SingleSelect } from '../../Form/Control';
import { Scroll } from '../Scroll';
import { Handle } from '../Scroll/Handle';
import { AreaStackChart } from './AreaStackChart';

const generateData = (size: number) =>
  new Array(size).fill(0).map(() => ({
    dhcp: Math.ceil(Math.random() * 100),
    reserved: Math.ceil(Math.random() * 100),
    free: Math.ceil(Math.random() * 100),
  }));

const data = generateData(1000);

const colorMap = {
  dhcp: '#176AEC',
  reserved: '#BAD1F3',
  free: 'white',
};

export default {
  title: 'Components/Organisms/Charts/Area Stack',
  component: AreaStackChart,
};

/**
 *
 * Area stack shows aggregated date on the chart.
 *
 * Area stack is SVG component. You should wrap it in a svg element.
 *
 * In order to use the area stack chart, you should:
 *
 * - define its size via `width` and `height`
 * - define the data
 * - `data` is an array of objects
 * - `columns` is an array of keys of the data. It defines in which fields of the data to use
 * - `colors` is a matching of the columns to the color
 *
 * ```
 * <svg width={300} height={500}>
 *   <AreaStackChart
 *     width={300}
 *     height={500}
 *     data={data}
 *     colors={colorMap}
 *     columns={['dhcp', 'reserved', 'free']}
 *     horizontal={horizontal}
 *   />
 * </svg>
 * ```
 *
 * You can define, if you want to show the chart horizontally or vertically using `horizontal` property. By default, it's vertical.
 */

export const AreaStack = (): ReactNode => {
  const items = useMemo(
    () => [
      {
        value: 'horizontal',
        type: true,
        label: 'Horizontal',
      },
      {
        value: 'vertical',
        type: false,
        label: 'Vertical',
      },
    ],
    [],
  );

  const [horizontal, setHorizontal] = useState(items[1]);

  return (
    <div>
      <FormGroup
        className="my-4"
        style={{
          width: 300,
        }}
      >
        <Label id="direction" required>
          Direction
        </Label>
        <SingleSelect
          value={horizontal}
          items={items}
          onChange={(a) => {
            if (a) {
              // eslint-disable-next-line @typescript-eslint/ban-ts-comment
              // @ts-ignore
              setHorizontal(a);
            }
          }}
        />
      </FormGroup>
      <div>
        <svg width={500} height={500}>
          <AreaStackChart
            width={300}
            height={500}
            colors={colorMap}
            columns={['dhcp', 'reserved', 'free']}
            data={data}
            horizontal={horizontal.type}
          />
        </svg>
      </div>
    </div>
  );
};

/**
 *
 * In some cases, it's better to use smooth lines, in some - strict.
 *
 * The changes are more noticeable on the smaller data sets.
 *
 * In the following example, you can see the effect of different types of lines on the data
 *
 */
export const LineTypes = (): ReactNode => {
  const entries = useMemo(() => generateData(200), []);

  const types = useMemo(
    () => [
      {
        value: 'Step',
        type: Curve.curveStep,
        label: 'Step',
      },
      {
        value: 'Basis',
        type: Curve.curveBasis,
        label: 'Basis',
      },
      {
        value: 'MonotoneX',
        type: Curve.curveMonotoneX,
        label: 'MonotoneX',
      },
      {
        value: 'MonotoneY',
        type: Curve.curveMonotoneY,
        label: 'MonotoneY',
      },
      {
        value: 'Natural',
        type: Curve.curveNatural,
        label: 'Natural',
      },
      {
        value: 'Linear',
        type: Curve.curveLinear,
        label: 'Linear',
      },
      {
        value: 'Cardinal',
        type: Curve.curveCardinal,
        label: 'Cardinal',
      },
      {
        value: 'CatmullRom',
        type: Curve.curveCatmullRom,
        label: 'CatmullRom',
      },
    ],
    [],
  );

  const [curveType, setCurveType] = useState(types[0]);

  return (
    <div>
      <FormGroup
        className="my-4"
        style={{
          width: 300,
        }}
      >
        <Label id="curve" required>
          Line type
        </Label>
        <SingleSelect
          value={curveType}
          items={types}
          onChange={(a) => {
            if (a) {
              // eslint-disable-next-line @typescript-eslint/ban-ts-comment
              // @ts-ignore
              setCurveType(a);
            }
          }}
        />
      </FormGroup>
      <div>
        <svg width={500} height={500}>
          <AreaStackChart
            curve={curveType.type}
            width={300}
            height={500}
            colors={colorMap}
            columns={['dhcp', 'reserved', 'free']}
            data={entries}
          />
        </svg>
      </div>
    </div>
  );
};

/**
 *
 * As it's the SVG element, you can combine it with other SVG components to build a complex application.
 *
 * In the following example, you can see an example of usage the chart with the scroll and synced with the list view.
 *
 */
export const AreaStackConnectedWithScrollAndTheScrollList = (): ReactNode => {
  const apiRef = useRef<ApiType | null>(null);
  const offset = 0;
  const total = 1000;
  const maxHeight = 600;
  const width = 160;
  const size = total;
  const [index, setIndex] = useState(50);
  const perPage = 30;

  useEffect(() => {
    if (apiRef.current?.virtualItems.some((a) => a.index === index)) {
      return;
    }

    apiRef.current?.scrollToIndex(index, {
      align: 'center',
    });
  }, [index]);

  return (
    <div className="d-flex">
      <div
        className="overflow-auto"
        style={{
          maxHeight,
          width: 300,
        }}
      >
        <ScrollList
          size={total}
          itemSize={20}
          apiRef={apiRef}
          onScrollCapture={() => {
            const count = apiRef.current?.virtualItems?.length || 0;
            const minIndex = apiRef.current?.virtualItems?.at(0)?.index || 0;
            const itemIndex = Math.floor((minIndex * count) / total);
            setIndex(apiRef.current?.virtualItems?.at(itemIndex)?.index || 0);
          }}
        >
          {(i) => <div>{i}</div>}
        </ScrollList>
      </div>
      <svg
        width={width + 2}
        height={maxHeight}
        className="rounded ml-4"
        style={{
          borderTop: '4px solid #8B8B8B',
          borderBottom: '4px solid #8B8B8B',
          borderLeft: '1px solid #8B8B8B',
          borderRight: '1px solid #8B8B8B',
        }}
      >
        <AreaStackChart
          columns={['dhcp', 'reserved', 'free']}
          colors={colorMap}
          data={data}
          height={maxHeight}
          width={width}
        />
        <Scroll
          offset={offset}
          onScroll={setIndex}
          position={index}
          size={size}
          height={maxHeight - size / perPage}
          width={width}
        >
          {({ isDragging, changePosition }) => (
            <Handle
              active={isDragging}
              changePosition={changePosition}
              height={size / perPage}
              strokeWidth={3}
              width={width}
            />
          )}
        </Scroll>
      </svg>
    </div>
  );
};
