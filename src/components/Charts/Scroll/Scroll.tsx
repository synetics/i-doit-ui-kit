import React, { ReactElement, useMemo, useState } from 'react';
import { useDrag } from '@visx/drag';
import { HandlerArgs } from '@visx/drag/lib/Drag';
import { Group } from '@visx/group';
import { scaleLinear } from '@visx/scale';
import { useDelay, useEvent } from '../../../hooks';

type ScrollHandler = (position: number) => void;

type ChildrenPropTypes = {
  isDragging: boolean;
  changePosition: (difference: number) => void;
};

export type ScrollType = {
  // height of the Scroll
  height: number;
  // width of the Scroll
  width: number;
  // top and bottom padding of the scroll area. It's recommended to have additional padding to be able to scroll till the end of the area
  padding?: number;
  // start position of the scrolling area
  offset?: number;
  // current scrolling position
  position: number;
  // size of the scrolling area
  size: number;
  // handler on scroll
  onScroll: ScrollHandler;
  // handler on start scrolling
  onScrollStart?: ScrollHandler;
  // // handler on finish scrolling
  onScrollEnd?: ScrollHandler;
  // render scroll handler
  children: (childrenProps: ChildrenPropTypes) => ReactElement;
};

export const Scroll = ({
  offset = 0,
  width,
  height,
  padding = 0,
  size,
  position,
  onScroll,
  onScrollStart,
  onScrollEnd,
  children,
  ...rest
}: ScrollType): ReactElement => {
  const scale = useMemo(
    () =>
      scaleLinear<number>({
        range: [padding, height - padding],
        domain: [offset, offset + size],
        nice: true,
      }),
    [padding, height, size, offset],
  );
  const [localPosition, setPosition] = useState(scale(position));
  const handleDragging =
    (callback: ScrollHandler | undefined = onScroll) =>
    (e: HandlerArgs): void => {
      const newIndex = Math.floor(scale.invert((e.y || 0) + e.dy));
      e.event.preventDefault();
      callback(newIndex);
    };
  const {
    y = 0,
    dy,
    isDragging,
    dragStart,
    dragEnd,
    dragMove,
  } = useDrag({
    x: 0,
    y: localPosition,
    resetOnStart: true,
    onDragStart: handleDragging(onScrollStart),
    onDragEnd: handleDragging(onScrollEnd),
    onDragMove: handleDragging(onScroll),
    restrict: {
      xMin: 0,
      xMax: 0,
      yMin: scale(offset),
      yMax: scale(offset + size),
    },
  });
  useDelay(
    () => {
      const newPosition = scale(position);
      if (!isDragging) {
        setPosition(newPosition);
      }
    },
    0,
    [isDragging, position],
  );
  const changePosition = useEvent((difference: number) => {
    onScroll(Math.min(offset + size, Math.max(0, difference + position)));
  });
  return (
    <>
      <Group {...rest} top={y + dy}>
        {children({
          changePosition,
          isDragging,
        })}
      </Group>
      <rect
        x={-padding}
        y={-padding}
        fill="transparent"
        width={width + 2 * padding}
        height={height + 2 * padding}
        cursor="ns-resize"
        onWheel={(e) => {
          changePosition(Math.floor(scale.invert(e.deltaY)));
        }}
        onMouseMove={dragMove}
        onMouseLeave={dragEnd}
        onMouseUp={dragEnd}
        onMouseDown={dragStart}
        onTouchStart={dragStart}
        onTouchMove={dragMove}
        onTouchEnd={dragEnd}
      />
    </>
  );
};
