import { act, fireEvent, render } from '@testing-library/react';
import user from '@testing-library/user-event';
import { Scroll, ScrollType } from './Scroll';

describe('Scroll', () => {
  const create = (props: Partial<ScrollType>, diff: number) => {
    const actions = {
      onScroll: jest.fn(),
      onScrollStart: jest.fn(),
      onScrollEnd: jest.fn(),
    };
    const result = render(
      <svg data-testid="svg" width={100} height={300}>
        <Scroll
          height={200}
          width={100}
          position={0}
          size={1000}
          {...props}
          onScroll={actions.onScroll}
          onScrollStart={actions.onScrollStart}
          onScrollEnd={actions.onScrollEnd}
          data-testid="scroll"
        >
          {({ changePosition, isDragging }) => (
            <rect
              data-testid="child"
              onClick={() => changePosition(diff)}
              data-state={isDragging ? 'dragging' : 'stay'}
            />
          )}
        </Scroll>
      </svg>,
    );
    return {
      ...result,
      actions,
      svg: result.getByTestId('svg'),
      element: result.getByTestId('scroll'),
      child: result.getByTestId('child'),
    };
  };
  it('renders without error', () => {
    const { element, child } = create({ padding: 30 }, 0);
    expect(element).toBeInTheDocument();
    expect(child).toBeInTheDocument();
  });

  it('reacts on click', async () => {
    const { actions, child } = create({}, 10);
    await user.click(child);
    expect(actions.onScroll).toHaveBeenCalledWith(10);
  });

  it('reacts on click with offset', async () => {
    const { actions, child } = create({ offset: 40, position: 50 }, 10);
    await user.click(child);
    expect(actions.onScroll).toHaveBeenCalledWith(60);
  });

  it('reacts on click with position', async () => {
    const { actions, child } = create({ position: 40 }, 10);
    await user.click(child);
    expect(actions.onScroll).toHaveBeenCalledWith(50);
  });

  it('reacts on wheel', async () => {
    const { actions, svg } = create({ position: 40 }, 10);
    await act(async () => {
      const child = svg.lastChild;
      if (!child) {
        return;
      }

      fireEvent.wheel(child, { deltaY: 30 });
    });
    expect(actions.onScroll).toHaveBeenCalled();
  });

  it('reacts drag and drop', async () => {
    const { actions, svg } = create({ position: 40 }, 10);
    const current = {
      clientX: 0,
      clientY: 10,
    };
    const child = svg.lastChild;
    if (!child) {
      return;
    }

    const steps = [
      [0, 10],
      [0, 100],
    ];
    await act(() => fireEvent.mouseEnter(child, current));
    await act(() => fireEvent.mouseOver(child, current));
    await act(() => fireEvent.mouseMove(child, current));
    await act(() => fireEvent.mouseDown(child, current));
    for (let i = 0; i < steps.length; i += 1) {
      const step = steps[i];
      current.clientX += step[0];
      current.clientY += step[1];
      // eslint-disable-next-line no-await-in-loop
      await act(() => fireEvent.mouseMove(child, current));
    }
    await act(() => fireEvent.mouseUp(child, current));
    expect(actions.onScrollStart).toHaveBeenCalled();
    expect(actions.onScroll).toHaveBeenCalled();
    expect(actions.onScrollEnd).toHaveBeenCalled();
  });
});
