import { fireEvent, render, screen } from '@testing-library/react';
import { PropsWithoutRef } from 'react';
import { Handle, HandleType } from './Handle';

describe('Handle', () => {
  const create = (props: Partial<PropsWithoutRef<HandleType>>) => {
    const changePosition = jest.fn();
    render(
      <svg>
        <Handle {...props} data-testid="handler" changePosition={changePosition} height={10} width={100} />
      </svg>,
    );
    const element = screen.getByTestId('handler');

    return {
      changePosition,
      element,
    };
  };
  it('should render', () => {
    const { changePosition, element } = create({});
    expect(changePosition).not.toHaveBeenCalled();

    expect(element).toHaveAttribute('tabindex', '0');
  });

  it('should render custom stroke', () => {
    const { element } = create({ strokeWidth: 20 });
    expect(element).toHaveAttribute('stroke-width', '20');
  });

  it.each([
    ['PageUp', -20],
    ['PageDown', 20],
    ['ArrowUp', -10],
    ['ArrowDown', 10],
    ['Home', -Infinity],
    ['End', Infinity],
  ])('should react on keys', (key: string, argument: number) => {
    const { changePosition, element } = create({
      steps: [10, 20],
    });
    fireEvent.keyDown(element, { key });
    expect(changePosition).toHaveBeenCalledWith(argument);
  });
});
