import React, { ForwardedRef, forwardRef, ReactElement } from 'react';
import { Polygon } from '@visx/shape';
import { AddSVGProps } from '@visx/shape/lib/types';
import classnames from 'classnames';
import { handleKey } from '../../../../utils';
import classes from './Handle.module.scss';

export type HandleType = AddSVGProps<
  {
    active?: boolean;
    className?: string;
    changePosition: (difference: number) => void;
    children?: never;
    height: number;
    rotate?: number;
    strokeWidth?: number;
    steps?: [number, number];
    width: number;
  },
  SVGPolygonElement
>;

export const Handle = forwardRef(
  (
    { active, className, changePosition, height, strokeWidth = 3, steps = [5, 20], width, ...rest }: HandleType,
    ref: ForwardedRef<SVGPolygonElement>,
  ): ReactElement => {
    const handleKeys = handleKey({
      Home: () => changePosition(-Infinity),
      End: () => changePosition(Infinity),
      ArrowUp: () => changePosition(-steps[0]),
      PageUp: () => changePosition(-steps[1]),
      ArrowDown: () => changePosition(steps[0]),
      PageDown: () => changePosition(steps[1]),
    });
    const left = strokeWidth / 2;
    const right = width - strokeWidth / 2;
    const top = -strokeWidth / 2;
    const bottom = height - strokeWidth / 2;
    return (
      <Polygon
        {...rest}
        className={classnames(className, classes.drag, {
          [classes.active]: active,
        })}
        fill="none"
        points={[
          [left, top],
          [right, top],
          [right, bottom],
          [left, bottom],
        ]}
        ref={ref}
        strokeWidth={strokeWidth}
        onKeyDown={handleKeys}
        tabIndex={0}
      />
    );
  },
);
