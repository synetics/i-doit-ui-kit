import { act, render, screen } from '@testing-library/react';
import user from '@testing-library/user-event';
import { Donut } from './Donut';

describe('Donut', () => {
  const data = [
    { label: 'unassigned in DHCP', value: 100, color: '#FFD15B' },
    { label: 'assigned statically', value: 100, color: '#414ADC', unfocusable: true },
    { label: 'assigned via DHCP', value: 100, color: '#FF5656' },
    { label: 'unassigned', value: 100, color: '#C1F8D0' },
  ];
  it('renders with content inside without crashing', () => {
    render(<Donut chartData={data}>{() => 'displayedText'}</Donut>);

    expect(screen.getByText('displayedText')).toBeInTheDocument();
  });

  it('show tooltip', async () => {
    render(<Donut chartData={data}>{(active) => active?.label}</Donut>);
    const piecePie = screen.getByTestId(data[0].label);
    await user.hover(piecePie);
    const [text, tooltip] = screen.getAllByText('unassigned in DHCP');
    expect(text).toBeInTheDocument();
    expect(tooltip).toBeInTheDocument();
    await user.unhover(piecePie);
    expect(text).toHaveTextContent('');
    expect(tooltip).not.toBeInTheDocument();
  });

  it('set/unset active on focus', async () => {
    render(<Donut chartData={data}>{(active) => active?.label}</Donut>);
    const piecePie = screen.getByTestId(data[0].label);
    act(() => {
      piecePie.focus();
    });
    const [text, tooltip] = screen.getAllByText('unassigned in DHCP');
    expect(text).toBeInTheDocument();
    expect(tooltip).toBeInTheDocument();
    act(() => {
      piecePie.blur();
    });
    expect(tooltip).not.toBeInTheDocument();
    expect(text).toHaveTextContent('');
  });

  it('check focus out', async () => {
    render(
      <>
        <Donut chartData={data}>{(active) => active?.label}</Donut>
        <button>button</button>
      </>,
    );
    const piecePie = screen.getByTestId(data[0].label);
    act(() => {
      piecePie.focus();
    });
    const [text] = screen.getAllByText('unassigned in DHCP');
    expect(text).toBeInTheDocument();
    act(() => {
      screen.getByText('button').focus();
    });
    expect(text).toHaveTextContent('');
  });

  it('dont focus on unfocusable piece', () => {
    render(<Donut chartData={data} />);
    const piecePie = screen.getByTestId(data[1].label);
    piecePie.focus();
    expect(piecePie).not.toHaveFocus();
  });

  it('show custom piece class', () => {
    render(<Donut chartData={data} pieceClassName="pieceClassName" />);
    const piecePie = screen.getByTestId(data[0].label);
    expect(piecePie).toHaveClass('pieceClassName');
  });

  it('renders with custom text', () => {
    const text = 'textTest textTest';
    render(<Donut chartData={data}>{() => text}</Donut>);
    expect(screen.getByText(text)).toBeInTheDocument();
  });
});
