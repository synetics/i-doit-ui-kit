import React, { ReactElement, ReactNode, useRef, useState } from 'react';
import { Pie } from '@visx/shape';
import { Group } from '@visx/group';
import { localPoint } from '@visx/event';
import classnames from 'classnames';
import { useTooltip } from '@visx/tooltip';
import { PieArcDatum } from 'd3-shape';
import { ProvidedProps } from '@visx/shape/lib/shapes/Pie';
import { useFocusOut } from '../../../hooks';
import tooltipStyles from '../../Tooltip/Tooltip.module.scss';
import styles from './Donut.module.scss';

export type ChartPiece = {
  label: string;
  color: string;
  value: number;
  unfocusable?: boolean;
};

export type DonutProps<T extends ChartPiece> = {
  /**
   * Custom className of its container
   */
  className?: string;

  /**
   * Custom className for SVG
   */
  svgClassName?: string;
  /**
   * data for donut chart
   */
  chartData: T[];

  width?: number;

  /**
   * Function for custom sorting
   */
  sortFn?: (a: T, b: T) => number;

  /**
   * The content inside the Donut
   */
  children?: (active: T | null) => ReactNode;

  /**
   * The content of the tooltip
   */
  tooltipContent?: (active: T) => ReactNode | null;

  /**
   * The content of the labels
   */
  labelText?: (active: T | null, data: PieArcDatum<T>, pie: ProvidedProps<T>) => ReactNode | null;

  /**
   * Props for set Donut radius and active radius
   */
  radius?: number;
  activeRadius?: number;
  /**
   * Custom className for piece of chart
   */
  pieceClassName?: string;
  /**
   * Distance between parts of donut chart
   */
  pieceSpace?: number;
};

export const Donut = <T extends ChartPiece = ChartPiece>({
  chartData,
  children,
  className,
  svgClassName,
  labelText,
  sortFn,
  width = 200,
  tooltipContent = (active: T) => active.label,
  radius = width / 6,
  activeRadius = width / 5,
  pieceClassName,
  pieceSpace = 0.02,
}: DonutProps<T>): ReactElement | null => {
  const [active, setActive] = useState<null | T>(null);
  const ref = useRef<HTMLDivElement | null>(null);
  const half = width / 2;
  const { tooltipLeft, tooltipTop, tooltipOpen, showTooltip, hideTooltip } = useTooltip();

  const handleMouseOver = (event: React.MouseEvent | React.FocusEvent, activeElement: T) => {
    setActive(activeElement);
    const point = localPoint(event);
    if (!point) {
      return;
    }

    showTooltip({
      tooltipLeft: point.x,
      tooltipTop: point.y,
    });
  };
  const unsetActive = () => {
    setActive(null);
    hideTooltip();
  };

  const svgSize = pieceClassName ? width + width / 10 : width;
  const svgPosition = pieceClassName ? half + width / 20 : half;

  useFocusOut(ref || document.body, unsetActive);
  return (
    <div ref={ref} className={className}>
      <svg width={svgSize} height={svgSize} className={svgClassName}>
        <Group top={svgPosition} left={svgPosition}>
          <Pie
            pieSort={sortFn || null}
            data={chartData}
            pieValue={(d) => d.value}
            outerRadius={half}
            innerRadius={({ data }) => {
              const size = active && active === data ? activeRadius : radius;
              return half - size;
            }}
            padAngle={pieceSpace}
          >
            {(pie) =>
              pie.arcs.map((arc) => (
                <g
                  className={classnames(styles.piece, pieceClassName)}
                  key={arc.data.label}
                  onFocus={(e) => handleMouseOver(e, arc.data)}
                  onBlur={unsetActive}
                  tabIndex={arc.data.unfocusable ? undefined : 0}
                  data-testid={arc.data.label}
                  onMouseOver={
                    arc.data.unfocusable
                      ? () => {}
                      : (e) => {
                          handleMouseOver(e, arc.data);
                        }
                  }
                  onMouseOut={arc.data.unfocusable ? () => {} : unsetActive}
                >
                  <path d={pie.path(arc) || undefined} fill={arc.data.color} />
                  {labelText && labelText(active, arc, pie)}
                </g>
              ))
            }
          </Pie>
          {children && children(active)}
        </Group>
      </svg>
      {active && tooltipContent && tooltipOpen && (
        <div
          role="tooltip"
          style={{ top: tooltipTop, left: tooltipLeft }}
          className={classnames(tooltipStyles.content, styles.tooltip)}
        >
          {tooltipContent(active)}
        </div>
      )}
    </div>
  );
};
