export const move = <T>(items: T[], from: number, to: number): T[] => {
  const result = [...items];
  result.splice(to, 0, result[from]);
  result.splice(to >= from ? from : from + 1, 1);
  return result;
};
