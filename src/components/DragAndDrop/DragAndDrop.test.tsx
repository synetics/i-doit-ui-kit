import { render, screen } from '@testing-library/react';
import user from '@testing-library/user-event';
import { DndProvider } from 'react-dnd';
import { DragItem, DragItemPreview } from '../List/Item';
import { KeyboardBackend } from './KeyboardBackend';
import { Preview } from './Preview';
import { Drag } from './Drag';

describe('KeyboardBackend', () => {
  const label = 'Test';

  const renderDnd = () => {
    const callbacks = {
      onCancel: jest.fn(),
      onDrag: jest.fn(),
      onDrop: jest.fn(),
    };

    render(
      <DndProvider backend={KeyboardBackend}>
        {[0, 1, 2, 3].map((id, index) => (
          <DragItem
            index={index}
            onCancel={callbacks.onCancel}
            onDrag={() => {
              callbacks.onDrag(id);
            }}
            onDrop={callbacks.onDrop}
            key={`item-${id}`}
            type="Item"
          >
            <span>Item {id}</span>
          </DragItem>
        ))}
        <Preview>
          <DragItemPreview count={1}>{label}</DragItemPreview>
        </Preview>
      </DndProvider>,
    );

    return callbacks;
  };

  it('renders custom drag', async () => {
    const onCancel = jest.fn();
    const onDrag = jest.fn();
    render(
      <DndProvider backend={KeyboardBackend}>
        {[0, 1, 2, 3].map((id) => (
          <Drag
            onCancel={onCancel}
            onDrag={() => {
              onDrag(id);
            }}
            key={`item-${id}`}
            type="Item"
          >
            <button>Item {id}</button>
          </Drag>
        ))}
      </DndProvider>,
    );

    const item = screen.getByText('Item 1');
    await user.type(item, '{Control>} {/Control}');
    expect(onDrag).toHaveBeenCalled();
  });

  it('does not render preview if not dragging', () => {
    renderDnd();

    expect(screen.queryByText(label)).not.toBeInTheDocument();
  });

  it('ctrl + space starts dragging and renders preview', async () => {
    const { onDrag } = renderDnd();

    const item = screen.getByText('Item 1');
    await user.type(item, '{Control>} {/Control}');
    expect(onDrag).toHaveBeenCalledWith(1);
    expect(screen.queryByText(label)).toBeInTheDocument();
  });

  it('escape cancels dragging', async () => {
    const { onCancel, onDrop } = renderDnd();

    const item = screen.getByText('Item 1');
    await user.type(item, '{Control>} {/Control}');
    await user.type(item, '{Escape}');
    expect(screen.queryByText(label)).not.toBeInTheDocument();
    expect(onCancel).toHaveBeenCalled();
    expect(onDrop).not.toHaveBeenCalled();
  });

  it.each([
    ['home', 0],
    ['end', 4],
    ['arrowup', 0],
    ['arrowdown', 3],
  ])('%s change hover', async (key: string, index: number) => {
    const { onDrop } = renderDnd();

    const item = screen.getByText('Item 1');
    await user.type(item, `{Control>} {/Control}{${key}} `);
    expect(onDrop).toHaveBeenCalledWith(index);
  });
});
