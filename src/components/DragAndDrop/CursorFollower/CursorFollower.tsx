import { ReactNode, useEffect, useState } from 'react';
import classnames from 'classnames';
import classes from './CursorFollower.module.scss';

export const positionMap = {
  topLeft: 'translate(-100%, -100%)',
  topRight: 'translate(0%, -100%)',
  bottomRight: 'translate(0%, 50%)',
  bottomLeft: 'translate(-100%, 50%)',
  center: 'translate(-50%, -100%)',
};

type CursorFollowerType = {
  componentPosition?: 'topLeft' | 'topRight' | 'bottomLeft' | 'bottomRight' | 'center';
  children: ReactNode;
  className?: string;
};

const CursorFollower = ({ children, componentPosition = 'topLeft', className }: CursorFollowerType) => {
  const [position, setPosition] = useState({ x: 0, y: 0 });
  const updateCursorPosition = (e: MouseEvent) => {
    const { clientX, clientY } = e;

    setPosition({ x: clientX, y: clientY });
  };
  useEffect(() => {
    window.addEventListener('drag', updateCursorPosition);

    return () => {
      window.removeEventListener('drag', updateCursorPosition);
    };
  }, []);
  return (
    <div
      style={{
        left: `${position.x}px`,
        top: `${position.y}px`,
        transform: positionMap[componentPosition],
      }}
      className={classnames(classes.container, className)}
      data-testid="cursor-follower"
    >
      {children}
    </div>
  );
};

export { CursorFollower };
