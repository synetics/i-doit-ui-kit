import { act, render, screen } from '@testing-library/react';
import { CursorFollower } from './CursorFollower';

describe('CursorFollower', () => {
  it('renders without crashing', () => {
    render(<CursorFollower>Test Children</CursorFollower>);
    expect(screen.getByText('Test Children')).toBeInTheDocument();
    const cursorFollower = screen.getByTestId('cursor-follower');
    expect(cursorFollower).toHaveStyle('transform: translate(-100%, -100%)');
  });

  it('applies the correct component position style', () => {
    render(<CursorFollower componentPosition="topRight">Test Children</CursorFollower>);
    const cursorFollower = screen.getByTestId('cursor-follower');
    expect(cursorFollower).toHaveStyle('transform: translate(0%, -100%)');
  });
  it('updates cursor position on drag event', () => {
    render(<CursorFollower>Test Children</CursorFollower>);
    const element = screen.getByTestId('cursor-follower');
    const event = new MouseEvent('drag', { clientX: 100, clientY: 200 });
    act(() => {
      window.dispatchEvent(event);
    });
    expect(element).toHaveStyle({ left: '100px', top: '200px' });
  });
});
