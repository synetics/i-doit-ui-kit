import { useMemo, useState } from 'react';
import { useEvent } from '../../hooks';

type UseDragNDropType<T> = {
  onDrag?: (id: T) => void;
  onDrop?: (id: T, index: number) => void;
  onCancel?: () => void;
};

type UseDragNDropApi<T> = {
  drag: (item: T) => void;
  drop: (index: number) => void;
  cancel: () => void;
};

export const useDragNDropApi = <T>(
  items: T[],
  { onDrag, onDrop, onCancel }: UseDragNDropType<T>,
): [T | null, UseDragNDropApi<T>] => {
  const [dragging, setDragging] = useState<T | null>(null);
  const drag = useEvent((id: T) => {
    setDragging(id);
    if (onDrag) {
      onDrag(id);
    }
  });
  const cancel = useEvent(() => {
    setDragging(null);
    if (onCancel) {
      onCancel();
    }
  });
  const drop = useEvent((index: number) => {
    if (!dragging) {
      return;
    }
    const currentIndex = items.indexOf(dragging) ?? 0;
    const nextIndex = index > currentIndex ? index - 1 : index;
    if (onDrop) {
      onDrop(dragging, nextIndex);
    }
    setDragging(null);
  });
  const api = useMemo(
    () => ({
      drag,
      drop,
      cancel,
    }),
    [drag, drop, cancel],
  );

  return [dragging, api];
};
