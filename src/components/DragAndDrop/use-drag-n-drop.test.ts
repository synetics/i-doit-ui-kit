import { act, renderHook } from '@testing-library/react-hooks';
import { useDragNDropApi } from './use-drag-n-drop';

describe('useDragNDropApi()', () => {
  const items = [1, 2, 3, 4, 5];
  const props = {
    onDrag: jest.fn(),
    onDrop: jest.fn(),
    onCancel: jest.fn(),
  };

  it('Initial state', () => {
    const { result } = renderHook(() => useDragNDropApi(items, props));
    const [dragging] = result.current;
    expect(dragging).toBeNull();
  });
  it('drags item and calls events', () => {
    const { result } = renderHook(() => useDragNDropApi(items, props));
    const [dragging, api] = result.current;
    expect(dragging).toBeNull();
    act(() => {
      api.drag(2);
    });
    expect(result.current[0]).toBe(2);
    expect(props.onDrag).toHaveBeenCalledWith(2);

    act(() => {
      api.drop(4);
    });
    expect(result.current[0]).toBeNull();
    expect(props.onDrop).toHaveBeenCalledWith(2, 3);
  });

  it('drags item and calls cancel', () => {
    const { result } = renderHook(() => useDragNDropApi(items, props));
    const [dragging, api] = result.current;
    expect(dragging).toBeNull();
    act(() => {
      api.drag(2);
    });

    act(() => {
      api.cancel();
    });
    expect(result.current[0]).toBeNull();
    expect(props.onCancel).toHaveBeenCalled();
    expect(props.onDrop).not.toHaveBeenCalled();
  });
});
