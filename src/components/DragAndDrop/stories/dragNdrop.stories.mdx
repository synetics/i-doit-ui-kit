import { useEffect, useRef, useState } from 'react';
import { DndProvider } from 'react-dnd';
import { Canvas, Meta, Story } from '@storybook/blocks';
import { useToggle } from '../../../hooks';
import { DragItem, DragItemPreview, Item } from '../../List/Item';
import { move } from '../utils/move';
import { KeyboardBackend } from '../KeyboardBackend';
import { Drag } from '../Drag';
import { DropArea } from '../DropArea';
import { DragHandle } from '../DragHandle';
import { Preview } from '../Preview';
import { Radio } from '../../Form/Control';
import { Text } from '../../Text';
import { ScrollableDnDContainer, CursorFollower } from '../';

<Meta title="Components/Molecules/Lists/Drag and Drop" />

# Drag and Drop

The Drag and Drop function allows users to easily manipulate order of their items.

## Drag handle

Drag handle is an icon to show the handle for dragging

<Canvas>
  <Story name="Drag handle">
    <DragHandle />
    <DragHandle active />
  </Story>
</Canvas>

## Drag Item

Drag item is a wrapper to add functionality of dragging to the element. It calls the callbacks on dragging and when drag is canceled.

<Canvas>
  <Story name="Drag item">
    <DndProvider backend={KeyboardBackend}>
      <Drag type="Item" onDrag={() => console.log('Drag')} onCancel={() => console.log('Cancel')}>
        <div>Item</div>
      </Drag>
    </DndProvider>
  </Story>
</Canvas>

## Drop Area

Drop area is responsible to define the places where to drop an item.

<Canvas>
  <Story name="Drop area">
    <DndProvider backend={KeyboardBackend}>
      <Drag type="Item" onDrag={() => console.log('Drag')} onCancel={() => console.log('Cancel')}>
        <div>Item</div>
      </Drag>
      <DropArea accept={['Item']} onDrop={() => console.log('Moved to area')}>
        <div>Move an item here</div>
      </DropArea>
    </DndProvider>
  </Story>
</Canvas>

## Custom preview

You can define a custom preview for you item using `<Preview>` component and with passing `disablePreview` to your `Drag`.

## Simple list

In the following example, you can see how to implement drag and drop with simple list items

export const items = new Array(5).fill(0).map((_, i) => i);

<Canvas>
  <Story name="Drag with single select">
    {() => {
      const refs = useRef({});
      const [elements, setElements] = useState(items);
      const [selected, setSelected] = useState(null);
      const [dragging, setDragging] = useState(null);
      const handleDrop = (index) => {
        if (dragging !== null) {
          setElements((old) => move(old, dragging, index));
          setDragging(null);
          setTimeout(() => {
            const element = refs.current[dragging];
            if (element && element.focus) {
              element.focus();
            }
          }, 0);
        }
      };
      const cancel = () => setDragging(null);
      const handleDrag = (index) => () => {
        setSelected(elements[index]);
        setDragging(index);
      };
      return (
        <div style={{ minHeight: 300 }}>
          <DndProvider backend={KeyboardBackend}>
            {elements.map((item, index) => [
              <DragItem
                ref={(ref) => (refs.current[item] = ref)}
                key={`item-${item}`}
                type="ItemSingle"
                index={index}
                selected={item === selected}
                onSelect={() => setSelected(item)}
                dragging={index === dragging}
                onCancel={cancel}
                onDrop={handleDrop}
                onDrag={handleDrag(index)}
              >
                <span>Item {item}</span>
              </DragItem>,
            ])}
            <Preview>
              {dragging !== null && (
                <DragItemPreview count={1}>
                  <span>Item {elements[dragging]}</span>
                </DragItemPreview>
              )}
            </Preview>
          </DndProvider>
        </div>
      );
    }}
  </Story>
</Canvas>

## Multiselect List

You can customize the logic of drag and drop.

In the following example, there are a special logic to handle drag and drop.

If you are dragging already selected item, it (and all other selected items) will be dragged.

If you drag non selected item, it will be selected and it will be dragged.

<Canvas>
  <Story name="Drag with multiselect">
    {() => {
      const refs = useRef({});
      const [drag, dragApi] = useToggle(false);
      const [selected, setSelected] = useState([]);
      const [elements, setElements] = useState(items);
      const onSelect = (id) => () =>
        setSelected((current) => {
          if (current.includes(id)) {
            return current.filter((a) => a !== id);
          }
          return [...current, id];
        });
      const handleDrag = (id) => () => {
        setSelected((current) => {
          if (!current.includes(id)) {
            return [id];
          }
          return [id, ...current.filter((a) => a !== id)];
        });
        dragApi.enable();
      };
      const handleDrop = (to) => {
        setElements((current) => selected.reduceRight((result, id) => move(result, result.indexOf(id), to), current));
        dragApi.disable();
        setTimeout(() => {
          const element = refs.current[selected[0]];
          if (element && element.focus) {
            element.focus();
          }
        }, 0);
      };
      return (
        <div style={{ minHeight: 300 }}>
          <DndProvider backend={KeyboardBackend}>
            {elements.map((id, index) => (
              <DragItem
                ref={(ref) => (refs.current[id] = ref)}
                index={index}
                selected={selected.includes(id)}
                dragging={drag ? selected.includes(id) : false}
                onSelect={onSelect(id)}
                onCancel={dragApi.disable}
                onDrag={handleDrag(id)}
                onDrop={handleDrop}
                key={`item-${id}`}
                type="ItemMulti"
              >
                <span>Item {id}</span>
              </DragItem>
            ))}
            <Preview>
              {drag && (
                <DragItemPreview count={selected.length}>
                  <span>Item {selected[0]}</span>
                </DragItemPreview>
              )}
            </Preview>
          </DndProvider>
        </div>
      );
    }}
  </Story>
</Canvas>

export const itemsForScroll = new Array(25).fill(0).map((_, i) => i);

<Canvas>
  <Story name="Drag with scroll">
    {() => {
      document.body.style.overflow = 'hidden';
      const positionOptions = ['topLeft', 'topRight', 'bottomLeft', 'bottomRight', 'center'];
      const refs = useRef({});
      const [elements, setElements] = useState(itemsForScroll);
      const [selected, setSelected] = useState(null);
      const [dragging, setDragging] = useState(null);
      const [position, setPosition] = useState(positionOptions[0]);
      const handleDrop = (index) => {
        if (dragging !== null) {
          setElements((old) => move(old, dragging, index));
          setDragging(null);
        }
      };
      const cancel = () => setDragging(null);
      const handleDrag = (index) => () => {
        setSelected(elements[index]);
        setDragging(index);
        setTimeout(() => {
          const element = refs.current[dragging];
          if (element && element.focus) {
            element.focus();
          }
        }, 0);
      };
      return (
        <div style={{ display: 'flex', justifyContent: 'center' }}>
          <div className={'d-flex flex-column mr-10'}>
            <Text className="mb-2">Choose drag position:</Text>
            {positionOptions.map((el) => (
              <Radio checked={position === el} label={el} onChange={() => setPosition(el)} aria-label={el} />
            ))}
          </div>
          <div style={{ minHeight: 300, padding: 'px 0', width: 200, overflow: 'hidden' }}>
            <div style={{ height: 100, textAlign: 'center', padding: '40px 0', backgroundColor: '#f0f0f0' }}>
              drag here to scroll up
            </div>
            <DndProvider backend={KeyboardBackend}>
              <div style={{ overflow: 'hidden', height: '400px' }}>
                <ScrollableDnDContainer>
                  {elements.map((item, index) => [
                    <div style={{ position: 'relative' }}>
                      <DragItem
                        ref={(ref) => (refs.current[item] = ref)}
                        key={`item-${item}`}
                        type="ItemSingle"
                        index={index}
                        selected={item === selected}
                        onSelect={() => setSelected(item)}
                        dragging={index === dragging}
                        onCancel={cancel}
                        onDrop={handleDrop}
                        onDrag={handleDrag(index)}
                      >
                        <span>Item {item}</span>
                      </DragItem>
                    </div>,
                  ])}
                  {dragging !== null && (
                    <CursorFollower componentPosition={position}>
                      <Item selected={true} style={{ width: 200 }}>
                        Item {elements[dragging]}
                      </Item>
                    </CursorFollower>
                  )}
                </ScrollableDnDContainer>
              </div>
            </DndProvider>
            <div style={{ height: 100, textAlign: 'center', padding: '40px 0', backgroundColor: '#f0f0f0' }}>
              drag here to scroll bottom
            </div>
          </div>
        </div>
      );
    }}
  </Story>
</Canvas>
