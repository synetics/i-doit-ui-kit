import { ForwardedRef, forwardRef } from 'react';
import classnames from 'classnames';
import { Icon } from '../../Icon';
import { dnd } from '../../../icons';
import classes from './DragHandle.module.scss';

type DragHandleType = {
  className?: string;
  active?: boolean;
};

const DragHandle = forwardRef(({ className, active, ...rest }: DragHandleType, ref: ForwardedRef<HTMLDivElement>) => (
  <div
    {...rest}
    ref={ref}
    className={classnames(className, classes.drag, {
      [classes.active]: active,
    })}
    data-dnd-type="handle"
  >
    <Icon src={dnd} fill={active ? 'active' : undefined} width={20} height={20} />
  </div>
));

export { DragHandle, DragHandleType };
