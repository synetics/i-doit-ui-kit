export const getScrollDirection = (
  position: number | null,
  top: number | null,
  bottom: number | null,
): 'top' | 'bottom' | null => {
  if (position === null || top === null || bottom === null || position <= 0) {
    return null;
  }
  if (top > position) {
    return 'top';
  }
  if (position > bottom) {
    return 'bottom';
  }
  return null;
};
