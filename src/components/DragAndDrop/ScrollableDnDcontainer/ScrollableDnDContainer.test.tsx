import { render, screen } from '@testing-library/react';
import React, { RefObject } from 'react';
import { ScrollableDnDContainer } from './ScrollableDnDContainer';

describe('ScrollableDnDContainer', () => {
  it('renders without crashing', () => {
    const onScrollMock = jest.fn();
    const ref = { current: null } as RefObject<HTMLDivElement>;
    render(
      <ScrollableDnDContainer onScroll={onScrollMock} ref={ref}>
        <div>Test content</div>
      </ScrollableDnDContainer>,
    );
    expect(ref.current).toBeInTheDocument();
    expect(screen.getByText('Test content')).toBeInTheDocument();
  });
  it('can pass custom speed', () => {
    const onScrollMock = jest.fn();
    const ref = { current: null } as RefObject<HTMLDivElement>;
    render(
      <ScrollableDnDContainer speed={40} timeInterval={10} onScroll={onScrollMock} ref={ref}>
        <div>Test content</div>
      </ScrollableDnDContainer>,
    );
    expect(ref.current).toBeInTheDocument();
  });
});
