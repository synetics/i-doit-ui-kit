import React, { CSSProperties, forwardRef } from 'react';
import classnames from 'classnames';
import { useMergedRef } from '@mantine/hooks';
import { useCallbackRef } from '../../../hooks';
import { useScroller } from './use-scroller';
import classes from './ScrollableDnDContainer.module.scss';

type ScrollableDnDContainerType = {
  className?: string;
  children: React.ReactNode;
  onScroll?: (direction: 'top' | 'bottom') => void;
  speed?: number;
  timeInterval?: number;
  style?: CSSProperties;
};

const ScrollableDnDContainer = forwardRef<HTMLDivElement, ScrollableDnDContainerType>(
  ({ className, children, onScroll, style, speed = 200, timeInterval = 100 }, passedRef) => {
    const [ref, setRef] = useCallbackRef<HTMLDivElement | null>();
    const mergedRef = useMergedRef<HTMLDivElement | null>(setRef, passedRef);

    useScroller(ref, speed, timeInterval, onScroll);

    return (
      <div style={style} ref={mergedRef} className={classnames(classes.container, className)}>
        {children}
      </div>
    );
  },
);

export { ScrollableDnDContainer };
