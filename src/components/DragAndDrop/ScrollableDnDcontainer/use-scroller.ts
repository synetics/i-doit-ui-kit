import { useRef } from 'react';
import { useInterval } from '@mantine/hooks';
import { useEvent, useEventListener } from '../../../hooks';
import { getScrollDirection } from './get-scroll-direction';

export const useScroller = (
  container: HTMLElement | null,
  speed: number,
  timeInterval: number,
  onScroll?: (direction: 'top' | 'bottom') => void,
): void => {
  const position = useRef<number | null>(null);
  const move = useEvent(() => {
    if (!container) {
      return;
    }
    const { top, bottom } = container.getBoundingClientRect();
    const direction = getScrollDirection(position.current, top, bottom);
    if (!direction) {
      return;
    }
    container.scrollTo({
      top: container.scrollTop + (direction === 'top' ? -speed : speed),
      behavior: 'smooth',
    });
    if (onScroll) {
      onScroll(direction);
    }
  });
  const interval = useInterval(move, timeInterval);
  useEventListener(
    'drag',
    (e: MouseEvent) => {
      position.current = e.clientY;
    },
    container,
  );
  useEventListener('dragstart', interval.start, container);
  useEventListener('drop', interval.stop, container);
  useEventListener('dragend', interval.stop, container);
};
