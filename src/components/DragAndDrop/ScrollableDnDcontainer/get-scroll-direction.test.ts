import { getScrollDirection } from './get-scroll-direction';

describe('ScrollableDnDContainer', () => {
  it.each([
    [0, 10, 20, null],
    [null, 10, 20, null],
    [12, null, 20, null],
    [12, 10, null, null],
    [5, 10, 20, 'top' as const],
    [15, 10, 20, null],
    [105, 10, 20, 'bottom' as const],
  ])(
    'returns expected',
    (position: number | null, top: number | null, bottom: number | null, expected: 'top' | 'bottom' | null) => {
      const result = getScrollDirection(position, top, bottom);
      expect(result).toBe(expected);
    },
  );
});
