import { getItemStyles } from './getItemStyles';

describe('getItemStyles', () => {
  it('does not display if no offsets', () => {
    const result = getItemStyles(null);
    expect(result).toStrictEqual({
      display: 'none',
    });
  });

  it('computes offset', () => {
    const result = getItemStyles({ x: 100, y: 200 });
    expect(result).toStrictEqual({
      transform: 'translate(100px, 200px)',
    });
  });
});
