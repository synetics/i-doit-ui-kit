import { ReactNode } from 'react';
import { useDragLayer } from 'react-dnd';
import classnames from 'classnames';
import classes from './Preview.module.scss';
import { getItemStyles } from './getItemStyles';

type PreviewProps = {
  className?: string;
  children?: ReactNode;
};

const Preview = ({ className, children }: PreviewProps) => {
  const { isDragging, currentOffset } = useDragLayer((monitor) => ({
    itemType: monitor.getItemType(),
    currentOffset: monitor.getSourceClientOffset(),
    isDragging: monitor.isDragging(),
  }));

  if (!isDragging) {
    return null;
  }

  return (
    <div className={classnames(classes.container, className)}>
      <div style={getItemStyles(currentOffset)}>{children}</div>
    </div>
  );
};

export { Preview };
