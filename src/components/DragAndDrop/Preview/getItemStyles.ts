import { XYCoord } from 'react-dnd';

const getItemStyles = (currentOffset: XYCoord | null): Record<string, string> => {
  if (!currentOffset) {
    return {
      display: 'none',
    };
  }

  const { x, y } = currentOffset;

  return {
    transform: `translate(${x}px, ${y}px)`,
  };
};

export { getItemStyles };
