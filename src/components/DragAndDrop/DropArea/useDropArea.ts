import { ConnectDropTarget, useDrop } from 'react-dnd';
import { DragItem } from '../types';

type UseDropAreaType = {
  isOver: boolean;
  isDragging: boolean;
  ref: ConnectDropTarget;
};

type CollectedType = {
  isOver: boolean;
  isDragging: boolean;
};

const useDropArea = <T = DragItem>(accept: string[], onDrop: (item: T) => void): UseDropAreaType => {
  const [{ isOver, isDragging }, ref] = useDrop<T, T, CollectedType>({
    accept,
    collect: (monitor) => {
      const type = monitor.getItemType();
      return {
        isDragging: type !== null && accept.includes(type.toString()),
        isOver: monitor.isOver(),
      };
    },
    drop(item) {
      onDrop(item);
      return item;
    },
  });

  return {
    isOver,
    isDragging,
    ref,
  } as const;
};

export { useDropArea };
