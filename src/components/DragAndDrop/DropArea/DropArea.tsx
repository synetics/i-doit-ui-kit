import { ReactNode } from 'react';
import classnames from 'classnames';
import { DragItem } from '../types';
import classes from './DropArea.module.scss';
import { useDropArea } from './useDropArea';

type DropType = {
  className?: string;
  accept: string[];
  onDrop: (item: DragItem) => void;
  children?: ReactNode;
};

const DropArea = ({ accept, className, children, onDrop }: DropType) => {
  const { ref, isDragging, isOver } = useDropArea(accept, onDrop);

  return (
    <div className={classnames(className, classes.dropArea)} data-dnd-active={isDragging} data-dnd-over={isOver}>
      {children}
      <div className={classes.drop} ref={ref} />
    </div>
  );
};

export { DropArea, DropType };
