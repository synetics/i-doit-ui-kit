import {
  Backend,
  DragDropActions,
  DragDropManager,
  DragDropMonitor,
  HandlerRegistry,
  Unsubscribe,
  XYCoord,
} from 'dnd-core';
import { HTML5Backend } from 'react-dnd-html5-backend';
import { makeDndScroll } from '../../../utils/make-dnd-scroll';

type OffsetParameters = {
  clientOffset: XYCoord | null;
  getSourceClientOffset: () => XYCoord | null;
};

const keyMap = {
  Home: -Infinity,
  End: Infinity,
  ArrowUp: -1,
  ArrowDown: 1,
  PageUp: -5,
  PageDown: 4,
};

export class KeyboardBackendImpl implements Backend {
  monitor: DragDropMonitor;

  subBackend: Backend;

  actions: DragDropActions;

  registry: HandlerRegistry;

  constructor(manager: DragDropManager) {
    this.monitor = manager.getMonitor();
    this.subBackend = HTML5Backend(manager);
    this.actions = manager.getActions();
    this.registry = manager.getRegistry();
  }

  /* istanbul ignore next */
  profile = (): Record<string, number> => ({});

  setup = (): void => {
    this.subBackend.setup();
    document.addEventListener('keydown', this.handleDragActions);
    document.addEventListener('mouseup', this.handleClick);
  };

  teardown = (): void => {
    document.removeEventListener('mouseup', this.handleClick);
    document.removeEventListener('keydown', this.handleDragActions);
    this.subBackend.teardown();
  };

  getType = (id: string): string =>
    this.registry.isTargetId(id)
      ? this.registry.getTargetType(id).toString()
      : this.registry.getSourceType(id).toString();

  connectNode = (node: HTMLElement, id: string): void => {
    const { dataset } = node;
    dataset.dndId = id;
    dataset.dndType = this.getType(id);
  };

  getOffsetParameters = (id: string): OffsetParameters => {
    const node = document.querySelector<HTMLElement>(`[data-dnd-id=${id}]`);
    const yPos = node?.getBoundingClientRect().y || 0;
    const container = node?.parentElement?.parentElement;
    const scrollChanger = 300;
    makeDndScroll(yPos, scrollChanger, container);

    /* istanbul ignore next */
    if (!node) {
      return {
        clientOffset: null,
        getSourceClientOffset: () => null,
      };
    }

    return {
      clientOffset: node.getBoundingClientRect(),
      getSourceClientOffset: () => node.getBoundingClientRect(),
    };
  };

  handleClick = (): void => {
    document.body.classList.remove('using-keyboard');
    this.cancel();
  };

  hover = (diff: number): void => {
    const [targetId] = this.monitor.getTargetIds();
    const type = this.monitor.getItemType();
    const targets = this.getTargets(type ? type.toString() : '');
    const currentIndex = targets.findIndex((a) => a.length === 0 || a.includes(targetId.toString()));
    const newIndex = Math.max(0, Math.min(targets.length - 1, currentIndex + diff));
    const newTarget = targets[newIndex][0];

    if (newTarget && targetId !== newTarget) {
      this.actions.hover([newTarget], this.getOffsetParameters(newTarget));
    }
  };

  cancel = (): void => {
    if (!this.monitor.isDragging()) {
      return;
    }

    this.actions.endDrag();
  };

  drop = (e: Event): void => {
    this.actions.drop();
    this.actions.endDrag();
    e.stopPropagation();
  };

  getTargets = (itemType: string): string[][] => {
    const all = this.getSourcesAndTargetsByType(itemType);
    return all.reduce(
      (result, item) => {
        if (item.substr(0, 1) === 'S') {
          result.push([]);
          return result;
        }
        result[result.length - 1].push(item);
        return result;
      },
      [[]] as string[][],
    );
  };

  getSourcesAndTargetsByType = (itemType: string): string[] => {
    const selector = document.querySelectorAll<HTMLElement>(
      `[data-dnd-type="${itemType}"]:not([data-dnd-dragging="true"])`,
    );
    const ids = [] as string[];
    selector.forEach(({ dataset }) => {
      if (dataset.dndId) {
        ids.push(dataset.dndId);
      }
    });

    return ids;
  };

  drag = (sourceId: string): boolean => {
    if (this.monitor.isDragging() || !this.monitor.canDragSource(sourceId)) {
      return false;
    }

    const all = this.getSourcesAndTargetsByType(this.registry.getSourceType(sourceId).toString());
    let target = null;
    for (let i = 0; i < all.length; i += 1) {
      if (sourceId === all[i]) {
        for (; i < all.length; i += 1) {
          if (this.registry.isTargetId(all[i])) {
            target = all[i];
            break;
          }
        }
      }
    }

    this.actions.beginDrag([sourceId], this.getOffsetParameters(sourceId));

    if (target) {
      this.actions.hover([target], this.getOffsetParameters(target));
    }

    return true;
  };

  handleDragActions = (e: KeyboardEvent): boolean => {
    document.body.classList.add('using-keyboard');

    if (!this.monitor.isDragging()) {
      return false;
    }

    switch (e.key) {
      case ' ':
        this.drop(e);
        e.preventDefault();
        break;
      case 'Escape':
        this.cancel();
        break;
      case 'Home':
      case 'End':
      case 'ArrowUp':
      case 'ArrowDown':
      case 'PageUp':
      case 'PageDown':
        if (keyMap[e.key]) {
          this.hover(keyMap[e.key]);
          e.preventDefault();
        }
        break;
      default:
    }

    e.stopPropagation();
    return true;
  };

  connectDragSource(sourceId: string, node: HTMLElement): Unsubscribe {
    this.connectNode(node, sourceId);
    const handleKeys = (e: KeyboardEvent) => {
      if (this.handleDragActions(e)) {
        return;
      }
      if (e.key === ' ' && e.ctrlKey && this.drag(sourceId)) {
        e.preventDefault();
        e.stopPropagation();
      }
    };
    node.addEventListener('keydown', handleKeys);

    const unsubscribe = this.subBackend.connectDragSource(sourceId, node);
    return () => {
      node.removeEventListener('keydown', handleKeys);
      unsubscribe();
    };
  }

  connectDragPreview = (sourceId: string, node: HTMLElement, options?: any): Unsubscribe =>
    this.subBackend.connectDragPreview(sourceId, node, options);

  connectDropTarget = (targetId: string, node: HTMLElement): Unsubscribe => {
    const subscribe = this.subBackend.connectDropTarget(targetId, node);
    this.connectNode(node, targetId);

    return subscribe;
  };
}

const KeyboardBackend = (manager: DragDropManager): Backend => new KeyboardBackendImpl(manager);

export { KeyboardBackend };
