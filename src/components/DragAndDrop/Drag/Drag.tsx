import { ReactNode } from 'react';
import classnames from 'classnames';
import { DragHandle } from '../DragHandle';
import classes from './Drag.module.scss';
import { useDragHandle } from './useDragHandle';

type DragType = {
  disablePreview?: boolean;
  type: string;
  className?: string;
  hidden?: boolean;
  onDrag: () => void;
  onCancel: () => void;
  children?: ReactNode;
};

const Drag = ({ disablePreview, className, onDrag, hidden, onCancel, type, children }: DragType) => {
  const { ref, visible, dragging } = useDragHandle(type, onDrag, onCancel, hidden || false, disablePreview || false);
  return (
    <div
      ref={ref}
      className={classnames(className, classes.drag, {
        [classes.hidden]: !visible,
      })}
      data-dnd-dragging={dragging || hidden}
    >
      <DragHandle className={classes.handle} active={dragging} />
      {children}
    </div>
  );
};

export { Drag, DragType };
