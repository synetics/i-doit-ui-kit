import { renderHook } from '@testing-library/react-hooks';
import { useDelayHidden } from './useDelayHidden';

describe('useDelayHidden', () => {
  it('changes result', async () => {
    const { result, rerender } = renderHook(({ hidden }) => useDelayHidden(hidden), {
      initialProps: {
        hidden: true,
      },
    });
    expect(result.current).toBe(true);
    rerender({ hidden: false });
    expect(result.current).toBe(false);
  });
});
