import { Ref, useEffect, useRef } from 'react';
import { useDrag } from 'react-dnd';
import { getEmptyImage } from 'react-dnd-html5-backend';
import { DragItem } from '../types';
import { useDelayHidden } from './useDelayHidden';

type UseDragHandleType = {
  visible: boolean;
  dragging: boolean;
  ref: Ref<HTMLDivElement>;
};

const useDragHandle = (
  type: string,
  onDrag: () => void,
  onCancel: () => void,
  hidden: boolean,
  disablePreview: boolean,
): UseDragHandleType => {
  const ref = useRef<HTMLDivElement>(null);
  const [{ isDragging }, drag, preview] = useDrag<DragItem, DragItem, { isDragging: boolean }>(() => ({
    type,
    item: () => {
      onDrag();
      return { type };
    },
    end: (draggedItem, monitor) => {
      if (monitor.getDropResult() === null) {
        onCancel();
      }
    },
    collect: (monitor) => ({
      isDragging: monitor.isDragging(),
    }),
  }));
  const isHidden = useDelayHidden(isDragging || hidden);

  drag(ref);

  useEffect(() => {
    preview(disablePreview ? getEmptyImage() : ref.current, { captureDraggingState: true });
  }, [preview, disablePreview]);

  return {
    visible: !isHidden,
    dragging: isDragging,
    ref,
  } as const;
};

export { useDragHandle };
