import { useEffect, useRef } from 'react';
import { useToggle } from '../../../hooks';

const useDelayHidden = (isHidden: boolean): boolean => {
  const [hidden, hiddenApi] = useToggle(isHidden);
  const timeout = useRef<number | undefined>(undefined);

  useEffect(() => {
    clearTimeout(timeout.current);
    if (isHidden) {
      timeout.current = window.setTimeout(hiddenApi.enable, 0);
    } else {
      hiddenApi.disable();
    }
  }, [isHidden, hiddenApi]);

  return hidden;
};

export { useDelayHidden };
