import { render, screen } from '@testing-library/react';
import React from 'react';
import { Sidebar } from './Sidebar';

describe('Sidebar', () => {
  it('renders without crashing', () => {
    render(<Sidebar>test content</Sidebar>);
    expect(screen.getByText('test content')).toBeInTheDocument();
  });
  it('default className', () => {
    render(<Sidebar>test content</Sidebar>);
    expect(screen.getByText('test content')).toHaveClass('md', 'col-2');
  });

  it('other size', () => {
    render(
      <Sidebar size="lg" shadow="none">
        test content
      </Sidebar>,
    );
    expect(screen.getByText('test content')).toHaveClass('none', 'col-3');
  });

  it('without col classes', () => {
    render(
      <Sidebar inContainer shadow="none">
        test content
      </Sidebar>,
    );
    expect(screen.getByText('test content')).not.toHaveClass('col-2');
  });

  it('other component', () => {
    render(<Sidebar as="span">test content</Sidebar>);
    expect(screen.getByText('test content').tagName).toBe('SPAN');
  });
});
