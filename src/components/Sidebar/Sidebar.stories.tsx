import { lorem } from 'faker';
import { ReactNode, useState } from 'react';
import { Button, ButtonSpacer } from '../Button';
import { Headline } from '../Headline';
import { Text } from '../Text';
import classes from './SidebarStories.module.scss';
import { Sidebar } from './Sidebar';

const text = lorem.sentence(1000);

export default {
  title: 'Components/Organisms/Sidebar',
  component: Sidebar,
};

export const Regular = (): ReactNode => {
  const sizes = ['sm', 'lg'];
  const shadows = ['sm', 'md', 'lg', 'none'];
  const [size, setSize] = useState<'sm' | 'lg'>('sm');
  const [shadow, setShadow] = useState<'none' | 'sm' | 'md' | 'lg'>('sm');

  return (
    <div className={classes.container}>
      <Sidebar size={size} shadow={shadow} className="overflow-auto">
        <Headline as="h2" className="mb-4">
          Sizes:
        </Headline>
        <ButtonSpacer className="mb-4">
          {sizes.map((s) => (
            <Button
              variant="text"
              key={s}
              activeState={s === size}
              onClick={() => {
                if (s === 'sm') {
                  setSize(s);
                } else if (s === 'lg') {
                  setSize(s);
                }
              }}
            >
              {s}
            </Button>
          ))}
        </ButtonSpacer>
        <Headline as="h2" className="mb-4">
          Shadows:
        </Headline>
        <ButtonSpacer className="mb-4">
          {shadows.map((s) => (
            <Button
              variant="text"
              className="mt-2"
              key={s}
              activeState={s === shadow}
              onClick={() => {
                if (s === 'sm') {
                  setShadow(s);
                } else if (s === 'md') {
                  setShadow(s);
                } else if (s === 'lg') {
                  setShadow(s);
                } else if (s === 'none') {
                  setShadow(s);
                }
              }}
            >
              {s}
            </Button>
          ))}
        </ButtonSpacer>
        <Text>{text}</Text>
      </Sidebar>
    </div>
  );
};
