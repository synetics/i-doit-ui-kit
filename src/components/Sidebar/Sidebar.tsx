import { CSSProperties, ElementType, forwardRef, ReactNode } from 'react';
import classnames from 'classnames';
import { Container } from '../Container';
import classes from './Sidebar.module.scss';

type SidebarProps = {
  className?: string;
  style?: CSSProperties;
};

type SidebarType = {
  as?: ElementType<SidebarProps>;
  /**
   * Size of the sidebar. Can be sm: 'col-2' or lg: 'col-3'
   */
  size?: 'sm' | 'lg';
  shadow?: 'none' | 'sm' | 'md' | 'lg';
  /**
   * Property to not use 'col-2' or 'col-3' classes
   */
  inContainer?: boolean;
  /**
   * The content of the Sidebar
   */
  children?: ReactNode | ReactNode[];
} & SidebarProps;

const sizeToClassName = {
  sm: 'col-2',
  lg: 'col-3',
};

const Sidebar = forwardRef<HTMLDivElement, SidebarType>(
  ({ children, size = 'sm', shadow = 'md', className, style, as = 'div', inContainer = false, ...rest }, ref) => (
    <Container
      {...rest}
      component={as}
      className={classnames(className, classes.container, !inContainer && sizeToClassName[size], classes[shadow])}
      style={style}
      ref={ref}
    >
      {children}
    </Container>
  ),
);

export { Sidebar };
