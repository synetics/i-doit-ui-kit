import { render, screen } from '@testing-library/react';
import React from 'react';
import { Badge, BadgeVariant } from './Badge';

describe('Badge', () => {
  it('renders without crashing', () => {
    render(<Badge>Badge</Badge>);

    expect(screen.getByText('Badge')).toBeInTheDocument();
  });

  it('to have a className', () => {
    render(<Badge className="test">Badge</Badge>);

    expect(screen.getByText('Badge')).toHaveClass('test');
  });

  it('to have a shadow', () => {
    render(<Badge shadow>Badge</Badge>);

    expect(screen.getByText('Badge')).toHaveClass('shadow');
  });

  it('to have a border', () => {
    render(<Badge border>Badge</Badge>);

    expect(screen.getByText('Badge')).toHaveClass('border');
  });

  it('to dont have  withShadow as a className', () => {
    render(<Badge shadow={false}>Badge</Badge>);

    expect(screen.getByText('Badge')).not.toHaveClass('shadow');
  });

  it.each<[BadgeVariant]>([['light-green'], ['green'], ['yellow'], ['violet'], ['bright-blue']])(
    'renders %s variant',
    (variant) => {
      render(<Badge variant={variant}>Badge</Badge>);

      expect(screen.getByText('Badge')).toHaveClass(variant);
    },
  );
});
