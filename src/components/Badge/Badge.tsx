import classnames from 'classnames';
import { Container, ContainerProps } from '../Container';
import classes from './Badge.module.scss';

export type BadgeVariant = 'light-green' | 'green' | 'yellow' | 'violet' | 'bright-blue';

type BadgeProps = ContainerProps & {
  /**
   * Class of the container
   */
  className?: string;
  /**
   * Accent style of the component
   */
  variant?: BadgeVariant;
  /**
   * If the badge has border
   */
  border?: boolean;
  /**
   * to set a shadow to the Badge
   */
  shadow?: boolean;
};
export const Badge = ({ variant = 'light-green', border, className, children, shadow, ...rest }: BadgeProps) => (
  <Container
    {...rest}
    className={classnames(classes.badge, classes[variant], className, {
      [classes.shadow]: shadow,
      [classes.border]: border,
    })}
  >
    {children}
  </Container>
);
