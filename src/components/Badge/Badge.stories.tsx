import React, { useState } from 'react';
import { Icon } from '../Icon';
import { getRandomIcon } from '../Form/Control/Pulldown/MultiSelect/stories/get-random-icon';
import { FormGroup, Label } from '../Form/Infrastructure';
import { SingleSelect } from '../Form/Control';
import { Badge, BadgeVariant } from './Badge';

export default {
  title: 'Components/Atoms/Badge',
  component: Badge,
};

const yesNoItems = [
  {
    inner: false,
    value: '0',
    label: 'No',
  },
  {
    inner: true,
    value: '1',
    label: 'Yes',
  },
];

const variants: BadgeVariant[] = ['light-green', 'green', 'yellow', 'violet', 'bright-blue'];

/**
 * Badge component renders a non-interactive information.
 *
 * Different variants help user to easily notice specific badges and create visual groups of them.
 */
export const BadgeComponent = () => {
  const [shadow, setShadow] = useState<boolean>(false);
  const [border, setBorder] = useState<boolean>(false);
  return (
    <>
      <div style={{ maxWidth: 300 }}>
        <FormGroup className="mb-2">
          <Label id="shadow">Shadow</Label>
          <SingleSelect
            id="shadow"
            items={yesNoItems}
            value={yesNoItems.find((a) => shadow === a.inner) ?? yesNoItems[0]}
            onChange={(a) => setShadow(a?.value === '1')}
          />
        </FormGroup>
        <FormGroup className="mb-2">
          <Label id="border">Border</Label>
          <SingleSelect
            id="border"
            items={yesNoItems}
            value={yesNoItems.find((a) => border === a.inner) ?? yesNoItems[0]}
            onChange={(a) => setBorder(a?.value === '1')}
          />
        </FormGroup>
      </div>
      {variants.map((variant) => (
        <Badge border={border} shadow={shadow} key={variant} variant={variant} className="mb-4">
          <Icon src={getRandomIcon()} width={16} className="mr-1" /> {variant}
        </Badge>
      ))}
    </>
  );
};
