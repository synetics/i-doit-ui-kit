import { createElement, forwardRef, ForwardRefExoticComponent, HTMLProps, ReactElement, RefAttributes } from 'react';
import classnames from 'classnames';
import { Spinner } from '../Spinner';
import { Tooltip } from '../Tooltip';
import styles from './BaseButton.module.scss';

type ButtonIconPosition = 'left' | 'right';

type ButtonVariant = 'primary' | 'secondary' | 'text' | 'outline' | 'ghost' | 'violet';

type ButtonBaseProps = {
  /**
   * Indicator to render spinner component
   */
  loading?: boolean;

  /**
   * Accent style of the component
   */
  variant?: ButtonVariant;

  /**
   * Element to be rendered inside the component
   */
  icon?: ReactElement;

  /**
   * Defines placement of the icon element
   */
  iconPosition?: ButtonIconPosition;

  /**
   * Is it a contrast button
   */
  contrast?: boolean;

  /**
   * Determines how to render the host element
   */
  component?: ForwardRefExoticComponent<
    Omit<ButtonBaseProps, 'component' | 'label'> & RefAttributes<HTMLButtonElement>
  >;

  /**
   * Value set the active state of the button
   */
  activeState?: boolean;

  /**
   * Adds an optional tooltip in 'disabled' state
   */
  disabledTooltip?: string;
  /**
   * Class name for popper
   */
  popperClassName?: string;
} & HTMLProps<HTMLButtonElement>;

const BaseButton = forwardRef<HTMLButtonElement, ButtonBaseProps>(
  (
    {
      contrast = false,
      children,
      className,
      disabled = false,
      disabledTooltip,
      loading = false,
      icon: _icon = null,
      iconPosition = 'left',
      variant = 'primary',
      component = 'button',
      activeState,
      tabIndex = 0,
      type = 'button',
      popperClassName,
      ...props
    },
    ref,
  ) => {
    const icon = (_icon || loading) && (
      <div className={classnames(styles.icon, styles[iconPosition], { [styles.spinner]: loading })}>
        {loading ? <Spinner className={classnames(styles.spinner)} /> : _icon}
      </div>
    );

    const content = {
      left: [icon, children],
      right: [children, icon],
    };

    const button = createElement(
      component,
      {
        ...props,
        onClick: disabled && disabledTooltip ? undefined : props.onClick,
        disabled: (disabled && !disabledTooltip) || loading,
        tabIndex,
        type,
        className: classnames(className, styles.buttonBase, {
          [styles.contrast]: contrast,
          [styles.loading]: loading,
          [styles[variant]]: variant,
          [styles.disabled]: disabled,
          [styles.active]: activeState,
        }),
        ref,
      },
      ...content[iconPosition],
    );

    if (disabled && disabledTooltip) {
      return (
        <Tooltip popperClassName={popperClassName} placement="bottom" content={<div>{disabledTooltip}</div>}>
          {button}
        </Tooltip>
      );
    }

    return button;
  },
);

export { BaseButton, ButtonBaseProps, ButtonVariant };
