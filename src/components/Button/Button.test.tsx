import { render, screen } from '@testing-library/react';
import user from '@testing-library/user-event';
import { Icon } from '../Icon';
import { Button } from './Button';

describe('Button', () => {
  const icon = <Icon src="image/src" />;

  it('renders with content inside without crashing', () => {
    const buttonContent = 'Test';

    render(<Button>{buttonContent}</Button>);

    expect(screen.getByText(buttonContent)).toBeInTheDocument();
  });

  it('renders default button with icon', () => {
    const buttonContent = 'Test';

    render(<Button icon={icon}>{buttonContent}</Button>);

    expect(screen.getByText(buttonContent)).toBeInTheDocument();
    expect(screen.getByTestId('icon')).toBeInTheDocument();
  });

  it('renders with content and icon at given position', () => {
    const buttonContent = 'Test';

    render(
      <Button icon={icon} iconPosition="right">
        {buttonContent}
      </Button>,
    );

    expect(screen.getByText(buttonContent)).toBeInTheDocument();
    expect(screen.getByTestId('icon')).toBeInTheDocument();
  });

  it('renders with content with given variant', () => {
    const buttonContent = 'Test';

    render(<Button variant="secondary">{buttonContent}</Button>);

    expect(screen.getByText(buttonContent)).toBeInTheDocument();
  });

  it('renders with content and loader', () => {
    const buttonContent = 'Test';

    render(<Button loading>{buttonContent}</Button>);

    expect(screen.getByText(buttonContent)).toBeDisabled();
    expect(screen.getByRole('status')).toBeInTheDocument();
  });

  it('renders optional tooltip in disabled state', async () => {
    render(
      <Button disabled disabledTooltip="Hello">
        Button Text
      </Button>,
    );
    const button = screen.getByText('Button Text');

    await user.hover(button);

    expect(screen.queryByRole('tooltip')).toBeInTheDocument();
    expect(button).toBeEnabled();
    expect(button).toHaveClass('disabled');
  });

  it('does not trigger onClick event when disabled with optional tooltip', async () => {
    const onClickMock = jest.fn();
    render(
      <Button onClick={onClickMock} disabled disabledTooltip="Hello">
        Button Text
      </Button>,
    );

    await user.click(screen.getByText('Button Text'));

    expect(onClickMock).toHaveBeenCalledTimes(0);
  });

  it('onClick works if only disabled passed', async () => {
    const onClickMock = jest.fn();
    render(
      <Button onClick={onClickMock} disabledTooltip="Disabled">
        Button Text
      </Button>,
    );

    await user.click(screen.getByText('Button Text'));

    expect(onClickMock).toHaveBeenCalledTimes(1);
  });
});
