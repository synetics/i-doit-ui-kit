import React, { PropsWithChildren, ReactNode } from 'react';
import classnames from 'classnames';
import { hasClassName } from '../../../utils/helpers';
import classes from './ButtonSpacer.module.scss';

export type GridSize = 'xs' | 'sm' | 'm' | 'l' | 'xl' | 'xxl';

type ButtonGroupProps = PropsWithChildren<{
  /**
   * Distance between buttons in container
   */
  gridSize?: GridSize;
  /**
   * classname of container
   */
  className?: string;
}>;

export const ButtonSpacer = ({ children, className, gridSize = 'l', ...props }: ButtonGroupProps) => {
  const length = React.Children.count(children);
  const clonedElements = React.Children.map<ReactNode, ReactNode>(children, (child, i) => {
    if (React.isValidElement(child)) {
      let childClassName = '';
      if (hasClassName(child.props)) {
        childClassName = child.props.className;
      }
      return React.cloneElement(child, {
        ...child.props,
        className: classnames(childClassName, {
          [classes[gridSize]]: i !== length - 1,
        }),
      });
    }
    return undefined;
  });

  return (
    <div {...props} data-testid="button-spacer-container" className={classnames([classes.container, className])}>
      {clonedElements}
    </div>
  );
};
