import { render, screen } from '@testing-library/react';
import { Button } from '../Button';
import { ButtonSpacer, GridSize } from './ButtonSpacer';

describe('ButtonSpacer', () => {
  it('renders without crashing', () => {
    render(
      <ButtonSpacer>
        <Button>Text</Button>
      </ButtonSpacer>,
    );

    const button = screen.getByText('Text');
    expect(button).toBeInTheDocument();
  });

  it('renders with extra className', () => {
    render(
      <ButtonSpacer>
        <Button className="newClass">Text</Button>
      </ButtonSpacer>,
    );

    const button = screen.getByText('Text');
    expect(button).toHaveClass('newClass');
  });

  it('with invalid react element', () => {
    render(<ButtonSpacer>1</ButtonSpacer>);
    const container = screen.getByTestId('button-spacer-container');
    expect(container).toBeEmptyDOMElement();
  });

  it.each<[GridSize]>([['xs'], ['sm'], ['m'], ['l'], ['xl'], ['xxl']])('renders with sizes "%s"', (size) => {
    render(
      <ButtonSpacer gridSize={size}>
        <Button data-testid="first">First button</Button>
        <Button data-testid="second">Second button</Button>
        <Button data-testid="third">Third button</Button>
      </ButtonSpacer>,
    );

    expect(screen.getByTestId('first')).toHaveClass(size);
    expect(screen.getByTestId('second')).toHaveClass(size);
    expect(screen.getByTestId('third')).not.toHaveClass(size);
  });
});
