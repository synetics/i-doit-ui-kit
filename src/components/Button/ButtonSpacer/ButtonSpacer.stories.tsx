import { ReactNode } from 'react';
import { Button } from '../Button';
import { ButtonSpacer } from './ButtonSpacer';
import classes from './ButtonSpacer.module.scss';

const sizes = ['xs' as const, 'sm' as const, 'm' as const, 'l' as const, 'xl' as const, 'xxl' as const];

export default {
  title: 'Components/Atoms/Buttons/ButtonSpacer',
  component: ButtonSpacer,
};

/**
 *
 * Receives buttons and renders them with different spaces as a group of buttons.
 *
 */

export const ButtonSpacerComponent = (): ReactNode =>
  sizes.map((size) => (
    <ButtonSpacer gridSize={size} className={classes.buttonSpacer}>
      <Button>One</Button>
      <Button variant="outline">Two</Button>
      <Button variant="secondary">Three</Button>
    </ButtonSpacer>
  ));
