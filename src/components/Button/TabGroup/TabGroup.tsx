import React, { ReactNode } from 'react';
import classnames from 'classnames';
import { isBoolean, isObjectWithShape, isString, optional } from '@i-doit/enten-types';
import classes from './TabGroup.module.scss';

export type TabGroupType = {
  className?: string;
  children?: ReactNode;
};

const isButtonChild = isObjectWithShape({
  activeState: optional(isBoolean),
  disabled: optional(isBoolean),
  className: optional(isString),
});

export const TabGroup = ({ className, children }: TabGroupType) => {
  const count = React.Children.count(children);
  const clonedElements = React.Children.map<ReactNode, ReactNode>(children, (child, i) => {
    if (React.isValidElement(child) && isButtonChild(child.props)) {
      return React.cloneElement(child, {
        ...child.props,
        variant: 'text',
        className: classnames(child.props.className, classes.item, {
          [classes.first]: i === 0,
          [classes.last]: i === count - 1,
          [classes.disabled]: child.props.disabled,
          [classes.activeState]: child.props.activeState,
        }),
      });
    }
    return undefined;
  });

  return (
    <div data-testid="tab-group" className={classnames([classes.container, className])}>
      {clonedElements}
    </div>
  );
};
