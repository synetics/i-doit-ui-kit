import { ReactNode, useState } from 'react';
import { Button } from '../Button';
import { IconButton } from '../../IconButton';
import { info } from '../../../icons';
import { Icon } from '../../Icon';
import { TabGroup } from './TabGroup';

export default {
  title: 'Components/Atoms/Buttons/TabGroup',
  component: TabGroup,
};

export const Regular = (): ReactNode => {
  const [active, setActive] = useState(0);

  return (
    <TabGroup>
      {new Array(10).fill(0).map((_, i) => (
        <Button key="tab" onClick={() => setActive(i)} activeState={i === active}>
          Item {i}
        </Button>
      ))}
    </TabGroup>
  );
};

export const IconButtons = (): ReactNode => {
  const [active, setActive] = useState(0);

  return (
    <TabGroup>
      {new Array(10).fill(0).map((_, i) => (
        <IconButton
          key="IconButton"
          onClick={() => setActive(i)}
          activeState={i === active}
          label={`Item ${i}`}
          icon={<Icon src={info} />}
        />
      ))}
    </TabGroup>
  );
};

export const WithIcons = (): ReactNode => {
  const [active, setActive] = useState(0);

  return (
    <TabGroup>
      {new Array(10).fill(0).map((_, i) => (
        <Button
          key="IconButton"
          onClick={() => setActive(i)}
          activeState={i === active}
          label={`Item ${i}`}
          icon={<Icon src={info} wrapper="span" />}
        >
          Item{i}
        </Button>
      ))}
    </TabGroup>
  );
};

export const Disabled = (): ReactNode => (
  <TabGroup>
    <Button disabled>Disabled tab</Button>
    <Button disabled disabledTooltip="No permission to open tab">
      No permission
    </Button>
    <Button activeState disabled>
      Disabled active tab
    </Button>
  </TabGroup>
);
