import { render, screen } from '@testing-library/react';
import { Button } from '../Button';
import { TabGroup } from './TabGroup';

describe('TabGroup', () => {
  it('renders without crashing', () => {
    render(
      <TabGroup>
        <Button>Text</Button>
      </TabGroup>,
    );

    const button = screen.getByText('Text');
    expect(button).toBeInTheDocument();
  });

  it('renders with extra className', () => {
    render(
      <TabGroup>
        <Button className="newClass">Text</Button>
      </TabGroup>,
    );

    const button = screen.getByText('Text');
    expect(button).toHaveClass('newClass');
  });

  it('renders without ReactNodeElement', () => {
    render(<TabGroup>text</TabGroup>);
    expect(screen.getByTestId('tab-group')).toBeEmptyDOMElement();
  });
});
