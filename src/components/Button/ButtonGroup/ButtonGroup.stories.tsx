import { ReactNode } from 'react';
import { ButtonPulldown } from '../../ButtonPulldown';
import { Item } from '../../Menu/components/items/Item';
import { Button } from '../Button';
import { IconButton } from '../../IconButton';
import { arrow_up } from '../../../icons';
import { Icon } from '../../Icon';
import { ButtonGroup } from './ButtonGroup';

export default {
  title: 'Components/Atoms/Buttons/ButtonGroup',
  component: ButtonGroup,
};

/**
 *
 * Receives buttons and renders them as a group of buttons.
 *
 */

export const Primary = (): ReactNode => (
  <>
    <ButtonGroup groupType="primary">
      <IconButton label="label" icon={<Icon src={arrow_up} />} />
    </ButtonGroup>
    <ButtonGroup className="mt-10" groupType="primary">
      <Button>primary</Button>
    </ButtonGroup>
    <ButtonGroup className="mt-10" groupType="primary">
      <IconButton label="label" icon={<Icon src={arrow_up} />} />
      <ButtonPulldown label="More" asIcon>
        {(onClose) => (
          <>
            <Item id="item-1" label="{Item}" onSelect={() => onClose()} />
            <Item id="item-2" label="{Item}" onSelect={() => onClose()} />
          </>
        )}
      </ButtonPulldown>
    </ButtonGroup>
    <ButtonGroup className="mt-10" groupType="primary">
      <IconButton label="label" icon={<Icon src={arrow_up} />} />
      <ButtonPulldown label="More">
        {(onClose) => (
          <>
            <Item id="item-1" label="{Item}" onSelect={() => onClose()} />
            <Item id="item-2" label="{Item}" onSelect={() => onClose()} />
          </>
        )}
      </ButtonPulldown>
      <IconButton label="label" icon={<Icon src={arrow_up} />} />
      <ButtonPulldown label="More">
        {(onClose) => (
          <>
            <Item id="item-1" label="{Item}" onSelect={() => onClose()} />
            <Item id="item-2" label="{Item}" onSelect={() => onClose()} />
          </>
        )}
      </ButtonPulldown>
    </ButtonGroup>
    <ButtonGroup className="mt-10" groupType="primary">
      <Button>primary</Button>
      <IconButton label="label" icon={<Icon src={arrow_up} />} />
      <IconButton label="label" icon={<Icon src={arrow_up} />} />
    </ButtonGroup>
    <ButtonGroup className="mt-10" groupType="primary">
      <IconButton label="label" icon={<Icon src={arrow_up} />} />
      <Button>primary</Button>
      <Button>primary</Button>
      <IconButton label="label" icon={<Icon src={arrow_up} />} />
    </ButtonGroup>
  </>
);

export const Secondary = (): ReactNode => (
  <>
    <ButtonGroup groupType="secondary">
      <IconButton label="label" icon={<Icon src={arrow_up} />} />
    </ButtonGroup>
    <ButtonGroup className="mt-10" groupType="secondary">
      <Button>secondary</Button>
    </ButtonGroup>
    <ButtonGroup className="mt-10" groupType="secondary">
      <IconButton label="label" icon={<Icon src={arrow_up} />} />
      <ButtonPulldown label="More" asIcon>
        {(onClose) => (
          <>
            <Item id="item-1" label="{Item}" onSelect={() => onClose()} />
            <Item id="item-2" label="{Item}" onSelect={() => onClose()} />
          </>
        )}
      </ButtonPulldown>
      <ButtonPulldown label="More">
        {(onClose) => (
          <>
            <Item id="item-1" label="{Item}" onSelect={() => onClose()} />
            <Item id="item-2" label="{Item}" onSelect={() => onClose()} />
          </>
        )}
      </ButtonPulldown>
    </ButtonGroup>
    <ButtonGroup className="mt-10" groupType="secondary">
      <IconButton label="label" icon={<Icon src={arrow_up} />} />
      <Button>secondary</Button>
      <IconButton label="label" icon={<Icon src={arrow_up} />} />
      <Button>secondary</Button>
      <IconButton label="label" icon={<Icon src={arrow_up} />} />
    </ButtonGroup>
  </>
);

export const Text = (): ReactNode => (
  <>
    <ButtonGroup groupType="text">
      <IconButton label="label" icon={<Icon src={arrow_up} />} />
    </ButtonGroup>
    <ButtonGroup className="mt-10" groupType="text">
      <Button>text</Button>
    </ButtonGroup>
    <ButtonGroup className="mt-10" groupType="text">
      <IconButton label="label" icon={<Icon src={arrow_up} />} />
      <ButtonPulldown label="More">
        {(onClose) => (
          <>
            <Item id="item-1" label="{Item}" onSelect={() => onClose()} />
            <Item id="item-2" label="{Item}" onSelect={() => onClose()} />
          </>
        )}
      </ButtonPulldown>
      <ButtonPulldown label="More" asIcon>
        {(onClose) => (
          <>
            <Item id="item-1" label="{Item}" onSelect={() => onClose()} />
            <Item id="item-2" label="{Item}" onSelect={() => onClose()} />
          </>
        )}
      </ButtonPulldown>
    </ButtonGroup>
    <ButtonGroup className="mt-10" groupType="text">
      <IconButton label="label" icon={<Icon src={arrow_up} />} />
      <Button>text</Button>
      <IconButton label="label" icon={<Icon src={arrow_up} />} />
      <Button>text</Button>
      <IconButton label="label" icon={<Icon src={arrow_up} />} />
    </ButtonGroup>
  </>
);
