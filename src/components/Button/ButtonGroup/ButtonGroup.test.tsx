import { render, screen } from '@testing-library/react';
import { Button } from '../Button';
import { ButtonGroup } from './ButtonGroup';

describe('ButtonGroup', () => {
  it('renders without crashing', () => {
    render(
      <ButtonGroup groupType="text">
        <Button>Text</Button>
      </ButtonGroup>,
    );

    const button = screen.getByText('Text');
    expect(button).toBeInTheDocument();
  });

  it('renders with extra className', () => {
    render(
      <ButtonGroup groupType="text">
        <Button className="newClass">Text</Button>
      </ButtonGroup>,
    );

    const button = screen.getByText('Text');
    expect(button).toHaveClass('newClass');
  });

  it('renders without ReactNodeElement', () => {
    render(<ButtonGroup groupType="text">text</ButtonGroup>);
    expect(screen.getByTestId('group-button-container')).toBeEmptyDOMElement();
  });
});
