import React, { ReactNode } from 'react';
import classnames from 'classnames';
import { ButtonVariant } from '../BaseButton';
import classes from './ButtonGroup.module.scss';

type ButtonGroupProps = {
  /**
   * Accent style of all buttons
   */
  groupType: ButtonVariant;

  className?: string;
  children?: ReactNode;
};
const hasClassName = (props: unknown): props is { className: string } =>
  typeof props === 'object' && Object.hasOwnProperty.call(props, 'className');

export const ButtonGroup = ({ children, groupType, className }: ButtonGroupProps) => {
  const clonedElements = React.Children.map<ReactNode, ReactNode>(children, (child) => {
    if (React.isValidElement(child)) {
      let childClassName = '';
      if (hasClassName(child.props)) {
        childClassName = child.props.className;
      }
      return React.cloneElement(child, {
        ...child.props,
        variant: groupType,
        className: classnames(childClassName, classes.item, classes[groupType]),
      });
    }
    return undefined;
  });

  return (
    <div data-testid="group-button-container" className={classnames([classes.container, className])}>
      {clonedElements}
    </div>
  );
};
