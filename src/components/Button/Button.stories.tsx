import { ReactNode, useState } from 'react';
import { Icon } from '../Icon';
import { edit } from '../../icons';
import { useToggle } from '../../hooks';
import { Checkbox, SingleSelect } from '../Form/Control';
import { Button } from './Button';

const variants = [
  { value: 'primary' as const, label: 'Variant: Primary' },
  { value: 'secondary' as const, label: 'Variant: Secondary' },
  { value: 'outline' as const, label: 'Variant: Outline' },
  { value: 'text' as const, label: 'Variant: Text' },
  { value: 'ghost' as const, label: 'Variant: Ghost' },
  { value: 'violet' as const, label: 'Variant: Violet' },
];

const backgrounds = [
  { value: '#FFFFFF', label: 'Background: White' },
  { value: '#EEEEEE', label: 'Background: Grey' },
  { value: '#212121', label: 'Background: Dark' },
];

export default {
  title: 'Components/Atoms/Buttons/Button',
  component: Button,
};

export const Primary = (): ReactNode => {
  const [contrast, contrastApi] = useToggle(false);
  const [selectedVariant, setVariant] = useState(variants[0]);
  const [background, setBackground] = useState(backgrounds[0]);
  const variant = selectedVariant.value;
  const style = {
    background: background.value,
  };

  return (
    <div>
      <div>
        <Checkbox aria-label="Contrast" label="Contrast" onChange={contrastApi.toggle} />
        <SingleSelect
          className="col-4"
          value={selectedVariant}
          items={variants}
          onChange={(a) => {
            const selectedValue = variants.find((v) => v.value === a?.value);
            if (selectedValue) {
              setVariant(selectedValue);
            }
          }}
        />
        <SingleSelect
          className="col-4"
          value={background}
          items={backgrounds}
          onChange={(v) => {
            if (v) {
              setBackground(v);
            }
          }}
        />
      </div>
      <table className="table">
        <thead>
          <tr>
            <th scope="col">&nbsp;</th>
            <th scope="col">Regular</th>
            <th scope="col">Disabled</th>
            <th scope="col">Disabled with tooltip</th>
            <th scope="col">Active</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Default</td>
            <td style={style}>
              <Button variant={variant} contrast={contrast}>
                Button
              </Button>
            </td>
            <td style={style}>
              <Button variant={variant} contrast={contrast} disabled>
                Button
              </Button>
            </td>
            <td style={style}>
              <Button variant={variant} contrast={contrast} disabled disabledTooltip="Disabled for some reason">
                Button
              </Button>
            </td>
            <td style={style}>
              <Button activeState variant={variant} contrast={contrast}>
                Button
              </Button>
            </td>
          </tr>
          <tr>
            <td>Spinner left (default)</td>
            <td style={style}>
              <Button variant={variant} contrast={contrast} loading>
                Button
              </Button>
            </td>
            <td style={style}>
              <Button variant={variant} contrast={contrast} loading disabled>
                Button
              </Button>
            </td>
            <td style={style}>
              <Button variant={variant} contrast={contrast} loading disabled disabledTooltip="Disabled for some reason">
                Button
              </Button>
            </td>
            <td style={style}>
              <Button activeState variant={variant} contrast={contrast} loading>
                Button
              </Button>
            </td>
          </tr>
          <tr>
            <td>Spinner right</td>
            <td style={style}>
              <Button variant={variant} contrast={contrast} iconPosition="right" loading>
                Button
              </Button>
            </td>
            <td style={style}>
              <Button variant={variant} contrast={contrast} iconPosition="right" loading disabled>
                Button
              </Button>
            </td>
            <td style={style}>
              <Button
                variant={variant}
                contrast={contrast}
                iconPosition="right"
                loading
                disabled
                disabledTooltip="Disabled for some reason"
              >
                Button
              </Button>
            </td>
            <td style={style}>
              <Button activeState variant={variant} contrast={contrast} iconPosition="right" loading>
                Button
              </Button>
            </td>
          </tr>
          <tr>
            <td>Spinner only</td>
            <td style={style}>
              <Button variant={variant} contrast={contrast} loading />
            </td>
            <td style={style}>
              <Button variant={variant} contrast={contrast} loading disabled />
            </td>
            <td style={style}>
              <Button
                variant={variant}
                contrast={contrast}
                loading
                disabled
                disabledTooltip="Disabled for some reason"
              />
            </td>
            <td style={style}>
              <Button activeState variant={variant} contrast={contrast} loading />
            </td>
          </tr>
          <tr>
            <td>Icon left (default)</td>
            <td style={style}>
              <Button variant={variant} contrast={contrast} icon={<Icon src={edit} wrapper="span" fill="white" />}>
                Button
              </Button>
            </td>
            <td style={style}>
              <Button
                variant={variant}
                contrast={contrast}
                icon={<Icon src={edit} wrapper="span" fill="white" />}
                disabled
              >
                Button
              </Button>
            </td>
            <td style={style}>
              <Button
                variant={variant}
                contrast={contrast}
                icon={<Icon src={edit} wrapper="span" fill="white" />}
                disabled
                disabledTooltip="Disabled for some reason"
              >
                Button
              </Button>
            </td>
            <td style={style}>
              <Button
                activeState
                variant={variant}
                contrast={contrast}
                icon={<Icon src={edit} wrapper="span" fill="white" />}
              >
                Button
              </Button>
            </td>
          </tr>
          <tr>
            <td>Icon right</td>
            <td style={style}>
              <Button
                variant={variant}
                contrast={contrast}
                icon={<Icon src={edit} wrapper="span" fill="white" />}
                iconPosition="right"
              >
                Button
              </Button>
            </td>
            <td style={style}>
              <Button
                variant={variant}
                contrast={contrast}
                icon={<Icon src={edit} wrapper="span" fill="white" />}
                iconPosition="right"
                disabled
              >
                Button
              </Button>
            </td>
            <td style={style}>
              <Button
                variant={variant}
                contrast={contrast}
                icon={<Icon src={edit} wrapper="span" fill="white" />}
                iconPosition="right"
                disabled
                disabledTooltip="Disabled for some reason"
              >
                Button
              </Button>
            </td>
            <td style={style}>
              <Button
                activeState
                variant={variant}
                contrast={contrast}
                icon={<Icon src={edit} wrapper="span" fill="white" />}
                iconPosition="right"
              >
                Button
              </Button>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  );
};
