import React, { forwardRef } from 'react';
import { BaseButton as ButtonBase, ButtonBaseProps } from './BaseButton';

export type ButtonProps = ButtonBaseProps;

export const Button = forwardRef<HTMLButtonElement, ButtonProps>(({ children, ...buttonProps }, ref) =>
  React.createElement(ButtonBase, { ...buttonProps, ref }, children),
);
