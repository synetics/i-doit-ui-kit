import { render, screen } from '@testing-library/react';
import { act } from 'react-dom/test-utils';
import React from 'react';
import user from '@testing-library/user-event';
import { createRenderer } from '../../../test/utils/renderer';
import { chevron_down, chevron_up, more_vertical } from '../../icons';
import { Item } from '../Menu/components';
import { ButtonPulldown } from './ButtonPulldown';

const initialProps = {
  label: 'test',
  children: (onClose: () => void) => (
    <>
      <Item id="item-1" label="Item1" onSelect={() => onClose()} />
    </>
  ),
  disabled: false,
};

const pulldownRender = createRenderer(ButtonPulldown, initialProps);

describe('<ButtonPulldown />', () => {
  it('renders with content inside without crashing', async () => {
    await act(async () => {
      pulldownRender();
    });
    expect(screen.queryByText('Item1')).not.toBeInTheDocument();
  });

  it('renders with content and open menu', async () => {
    await act(async () => {
      pulldownRender();
    });

    await user.click(screen.getByRole('button'));

    expect(screen.getByTestId('idoit-popper')).toBeVisible();
  });

  it('renders with content, open menu and close with escape', async () => {
    await act(async () => {
      pulldownRender();
    });

    await user.click(screen.getByRole('button'));
    const popper = screen.getByTestId('idoit-popper');
    expect(popper).toBeVisible();

    await user.keyboard('{Escape}');

    expect(popper).not.toBeInTheDocument();
  });

  it('renders with content and open/close menu by clicking on menu item', async () => {
    await act(async () => {
      pulldownRender();
    });

    expect(screen.getByTestId('icon')).toHaveAttribute('data-src', chevron_down);
    await user.click(screen.getByRole('button'));
    expect(screen.getByTestId('icon')).toHaveAttribute('data-src', chevron_up);

    const item = screen.getByRole('menuitem');

    act(() => item.focus());
    await user.type(item, '{enter}');

    expect(screen.queryByTestId('idoit-popper')).not.toBeInTheDocument();
    expect(screen.getByTestId('icon')).toHaveAttribute('data-src', chevron_down);
  });

  it('renders with content and open/close menu by pressing arrow down', async () => {
    await act(async () => {
      pulldownRender();
    });

    const item = screen.getByText('test');

    item.focus();
    await user.type(item, '{arrowdown}');

    expect(screen.getByText('Item1')).toBeInTheDocument();
  });

  it('renders with content and open menu and closing by clicking outside', async () => {
    await act(async () => {
      pulldownRender();
      render(React.createElement('div', { id: 'outside', role: 'outside' }));
    });

    expect(screen.getByTestId('icon')).toHaveAttribute('data-src', chevron_down);
    await user.click(screen.getByRole('button'));
    expect(screen.getByTestId('idoit-popper')).toBeVisible();
    expect(screen.getByTestId('icon')).toHaveAttribute('data-src', chevron_up);

    await user.click(screen.getByRole('outside'));
    expect(screen.queryByTestId('idoit-popper')).not.toBeInTheDocument();
    expect(screen.getByTestId('icon')).toHaveAttribute('data-src', chevron_down);
  });

  it('renders disabled', async () => {
    await act(async () => {
      pulldownRender({
        label: 'test',
        children: (onClose: () => void) => (
          <>
            <Item id="item-1" label="Item1" onSelect={() => onClose()} />
          </>
        ),
        disabled: true,
      });
    });

    expect(screen.getByRole('button')).toBeDisabled();
  });

  it('render as icon button', async () => {
    await act(async () => {
      pulldownRender({
        label: 'test',
        children: (onClose: () => void) => (
          <>
            <Item id="item-1" label="Item1" onSelect={() => onClose()} />
          </>
        ),
        asIcon: true,
      });
    });

    expect(screen.getByTestId('icon')).toHaveAttribute('data-src', more_vertical);
  });
});
