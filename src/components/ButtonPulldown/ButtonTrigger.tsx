import React, { forwardRef, SyntheticEvent } from 'react';
import { IconButton } from '../IconButton';
import { Icon } from '../Icon';
import { chevron_down, chevron_up, more_vertical } from '../../icons';
import { Button } from '../Button';
import { OverflowWithTooltip } from '../OverflowWithTooltip';
import { useGlobalizedRef } from '../../hooks';
import { useHover } from '../../hooks/use-hover';

type ButtonTriggerType = {
  activeState: boolean;
  className?: string;
  onClick: (e: SyntheticEvent) => void;
  label: string;
  asIcon?: boolean;
};

export const ButtonTrigger = forwardRef<HTMLButtonElement, ButtonTriggerType>(
  ({ activeState, className, onClick, label, asIcon, ...props }: ButtonTriggerType, ref) => {
    const { localRef, setRefsCallback } = useGlobalizedRef(ref);
    const hovered = useHover(localRef.current);
    if (asIcon) {
      return (
        <IconButton
          {...props}
          onClick={onClick}
          label={label}
          ref={setRefsCallback}
          icon={<Icon src={more_vertical} />}
          activeState={activeState}
          className={className}
        />
      );
    }
    return (
      <Button
        {...props}
        ref={setRefsCallback}
        iconPosition="right"
        onClick={onClick}
        icon={<Icon src={activeState ? chevron_up : chevron_down} />}
        activeState={activeState}
        className={className}
      >
        <OverflowWithTooltip containerRef={localRef.current || undefined} showTooltip={hovered}>
          {label}
        </OverflowWithTooltip>
      </Button>
    );
  },
);
