import React, { forwardRef, SyntheticEvent, useCallback } from 'react';
import { Modifier } from 'react-popper';
import { useClickOutside } from '@mantine/hooks';
import { useControlledState, useEventListener, useGlobalizedRef } from '../../hooks';
import { handleKey } from '../../utils';
import { KeyboardKeys } from '../../static';
import { ButtonProps } from '../Button';
import { Menu } from '../Menu';
import { Popper } from '../Popper';
import { ButtonTrigger } from './ButtonTrigger';

type ButtonPulldownProps = Omit<ButtonProps, 'icon' | 'iconPosition' | 'label' | 'children'> & {
  children: (onClose: () => void) => React.ReactNode;
  label: string;
  asIcon?: boolean;
  setOpen?: (v: boolean) => void;
  containerClassName?: string;
  modifiers?: Partial<Modifier<'sizeChange' | 'widthChange' | 'sameWidth' | 'offset', Record<string, unknown>>>[];
};

const ButtonPulldown = forwardRef<HTMLButtonElement, ButtonPulldownProps>(
  (
    {
      children,
      label,
      className,
      setOpen,
      open,
      component: Component = ButtonTrigger,
      containerClassName,
      modifiers,
      popperClassName,
      ...props
    },
    ref,
  ) => {
    const { localRef: referenceElement, setRefsCallback: setReferenceElement } =
      useGlobalizedRef<HTMLButtonElement>(ref);
    const [show, setShow] = useControlledState(!!open, setOpen);
    const togglePopper = useCallback(() => setShow(!show), [show, setShow]);
    const hideMenu = useCallback(() => setShow(false), [setShow]);
    const containerRef = useClickOutside(hideMenu, ['focusin', 'mousedown', 'touchstart']);
    const popper = (
      <Popper
        open={show}
        referenceElement={referenceElement.current}
        options={{ placement: 'bottom-start' }}
        modifiers={modifiers}
        className={popperClassName}
      >
        <Menu autoFocus>{children(hideMenu)}</Menu>
      </Popper>
    );

    const clickEvent = (e: SyntheticEvent) => {
      togglePopper();
      e.stopPropagation();
    };

    useEventListener('keydown', handleKey({ [KeyboardKeys.Escape]: () => setShow(false) }, false), document.body);

    return (
      <div ref={containerRef} className={containerClassName}>
        <Component
          {...props}
          className={className}
          activeState={show}
          onClick={clickEvent}
          ref={setReferenceElement}
          label={label}
          onKeyDown={handleKey({ [KeyboardKeys.ArrowDown]: () => setShow(true) }, false)}
        />
        {show && popper}
      </div>
    );
  },
);

export { ButtonPulldown, ButtonPulldownProps };
