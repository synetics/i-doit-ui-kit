import faker from 'faker';
import { FC, forwardRef, ReactNode, useState } from 'react';
import { Item, CheckboxItem } from '../Menu/components/items/Item';
import { Headline } from '../Headline';
import { Button } from '../Button';
import { ButtonPulldown } from './ButtonPulldown';

export default {
  title: 'Components/Atoms/Buttons/ButtonPulldown',
  component: ButtonPulldown,
};

// Define the type for items
type ItemProps = {
  label: string;
  value: string;
};

// Generate the items array
const items: ItemProps[] = new Array(5)
  .fill(0)
  .map((_, i) => ({
    label: faker.address.city(),
    value: `value${i}`,
  }))
  .sort(({ label: a }, { label: b }) => a.localeCompare(b));

// Define the props type for the component
type StatefulSelectorItemsProps = {
  value?: ItemProps[];
};

const StatefulSelectorItems: FC<StatefulSelectorItemsProps> = ({ value: defaultValue = [], ...props }) => {
  const [value, setValue] = useState<ItemProps[]>(defaultValue);
  const temp = value || [];

  return (
    <>
      {items.map((item) => (
        <CheckboxItem
          key={item.value}
          id={item.value}
          label={item.label}
          selected={temp.includes(item)}
          onSelect={() =>
            value.includes(item) ? setValue(value.filter((fItem) => fItem !== item)) : setValue([...value, item])
          }
          {...props}
        />
      ))}
    </>
  );
};

export const Menu = (): ReactNode => (
  <>
    <div
      style={{
        minHeight: 200,
      }}
    >
      <ButtonPulldown label="{Label}">
        {(onClose) => (
          <>
            <Item id="item-1" label="{Item}" onSelect={() => onClose()} />
            <Item id="item-2" label="{Item}" onSelect={() => onClose()} />
          </>
        )}
      </ButtonPulldown>
    </div>
    <div style={{ minHeight: 200 }}>
      <ButtonPulldown label="With selector">{() => <StatefulSelectorItems />}</ButtonPulldown>
    </div>
    <div style={{ minHeight: 200 }}>
      <ButtonPulldown label="{Label}" asIcon>
        {(onClose) => (
          <>
            <Item id="item-1" label="{Item}" onSelect={() => onClose()} />
            <Item id="item-2" label="{Item}" onSelect={() => onClose()} />
          </>
        )}
      </ButtonPulldown>
    </div>
  </>
);

export const ControlledState = (): ReactNode => {
  const [open, setOpen] = useState<boolean>(true);

  return (
    <div
      style={{
        width: '70vw',
      }}
    >
      <Headline className="mb-4" as="h2">
        With controlled open state
      </Headline>
      <div
        style={{
          minHeight: 200,
        }}
      >
        <ButtonPulldown open={open} setOpen={setOpen} label="{Label}">
          {(onClose) => (
            <>
              <Item id="item-1" label="{Item}" onSelect={() => onClose()} />
              <Item id="item-2" label="{Item}" onSelect={() => onClose()} />
            </>
          )}
        </ButtonPulldown>
      </div>
    </div>
  );
};

export const CustomTrigger = (): ReactNode => (
  <div
    style={{
      minHeight: 200,
    }}
  >
    <ButtonPulldown
      label="{Label}"
      component={forwardRef(({ activeState, ...props }, ref) => (
        <Button {...props} ref={ref}>
          Button:{activeState ? 'open' : 'closed'}
        </Button>
      ))}
    >
      {(onClose) => (
        <>
          <Item id="item-1" label="{Item}" onSelect={() => onClose()} />
          <Item id="item-2" label="{Item}" onSelect={() => onClose()} />
        </>
      )}
    </ButtonPulldown>
  </div>
);
