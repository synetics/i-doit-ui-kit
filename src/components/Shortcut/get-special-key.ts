const getSpecialKey = (): string => {
  const platform = navigator.platform.substr(0, 3).toLowerCase();

  switch (platform) {
    case 'win':
      return 'ctrl';
    case 'mac':
      return '⌘';
    default:
      return '';
  }
};

export { getSpecialKey };
