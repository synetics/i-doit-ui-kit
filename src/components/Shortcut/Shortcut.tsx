import classnames from 'classnames';
import { KeyboardEvent, MouseEvent } from 'react';
import { useEventListener } from '../../hooks/use-event-listener';
import { handleKey } from '../../utils/helpers';
import { getSpecialKey } from './get-special-key';

export type ShortcutProps = {
  hidden?: boolean;
  shortCut: string;
  onSelect: <T extends Element>(e?: MouseEvent<T>) => void;
  className?: string;
};

const Shortcut = ({ className, hidden, onSelect, shortCut }: ShortcutProps) => {
  const visualShortCut = `${getSpecialKey()}+${shortCut.toUpperCase()}`;

  useEventListener(
    'keydown',
    handleKey({
      [shortCut]: (e: KeyboardEvent) => {
        if (e.metaKey || e.ctrlKey) {
          onSelect();
          return true;
        }

        return false;
      },
    }),
    document.body,
  );

  if (hidden) {
    return null;
  }

  return (
    <div data-testid="i-shortcut" className={classnames('shortcut', className)}>
      {visualShortCut}
    </div>
  );
};

export { Shortcut };
