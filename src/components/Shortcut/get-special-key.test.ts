import { getSpecialKey } from './get-special-key';

describe('get-special-key', () => {
  it.each([
    ['windows', 'ctrl'],
    ['mac', '⌘'],
    ['unknown', ''],
  ])('special key for %s to be %s', (platform: string, expected: string): void => {
    const getter = jest.spyOn(window.navigator, 'platform', 'get');
    getter.mockReturnValue(platform);

    expect(getSpecialKey()).toBe(expected);
  });
});
