import { render, screen } from '@testing-library/react';
import user from '@testing-library/user-event';
import { Shortcut } from './Shortcut';

describe('Shortcut', () => {
  const shortCut = 'A';

  it('renders with content inside without crashing', () => {
    const fn = jest.fn();
    render(<Shortcut shortCut={shortCut} onSelect={fn} />);

    expect(screen.getByTestId('i-shortcut')).toBeInTheDocument();
  });

  it('calls the callback on ctrl + A with hidden element', async () => {
    const fn = jest.fn();
    render(<Shortcut hidden shortCut={shortCut} onSelect={fn} />);

    const element = screen.queryByTestId('i-shortcut');
    await user.type(document.body, '{Control>}A{/Control}');

    expect(element).not.toBeInTheDocument();
    expect(fn).toHaveBeenCalled();
  });
  it('calls the callback on ctrl + A', async () => {
    const fn = jest.fn();
    render(<Shortcut shortCut={shortCut} onSelect={fn} />);

    await user.type(document.body, '{Control>}A{/Control}');
    expect(fn).toHaveBeenCalled();
  });

  it('does not call the callback on A', async () => {
    const fn = jest.fn();
    render(<Shortcut shortCut={shortCut} onSelect={fn} />);

    await user.type(document.body, 'A');
    expect(fn).not.toHaveBeenCalled();
  });
});
