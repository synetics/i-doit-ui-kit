import { RowId } from '../../types';
import { setSelected } from './set-selected';

describe('setSelected', () => {
  it.each([
    [[], 'a', ['a']],
    [['a'], 'a', ['a']],
    [['a'], 'b', ['a', 'b']],
    [['a', 'b'], 'c', ['a', 'b', 'c']],
  ])('add selected returns right result', (selected: RowId[], id: RowId, expected: RowId[]) => {
    expect(setSelected(selected, id, true)).toStrictEqual(expected);
  });

  it.each([
    [[], 'a', []],
    [['a'], 'a', []],
    [['a', 'b'], 'b', ['a']],
    [['a', 'b', 'c'], 'c', ['a', 'b']],
  ])('remove selected returns right result', (selected: RowId[], id: RowId, expected: RowId[]) => {
    expect(setSelected(selected, id, false)).toStrictEqual(expected);
  });
});
