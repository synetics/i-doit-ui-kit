import { RowId } from '../../types';
import { areSelected } from '../are-selected/are-selected';

const setSelected = (selected: RowId[], rowId: RowId, state: boolean): RowId[] => {
  if (areSelected(selected)(rowId) === state) {
    return selected;
  }

  if (state) {
    return [...selected, rowId];
  }

  return selected.filter((e) => e !== rowId);
};

export { setSelected };
