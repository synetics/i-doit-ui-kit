import { RowId } from '../../types';

const getSlice = (availableIds: RowId[], startRowId: RowId, endRowId: RowId): RowId[] => {
  const startIndex = availableIds.findIndex((e) => e === startRowId);
  const endIndex = availableIds.findIndex((e) => e === endRowId);

  return availableIds.slice(Math.min(startIndex, endIndex), Math.max(startIndex, endIndex) + 1);
};

export { getSlice };
