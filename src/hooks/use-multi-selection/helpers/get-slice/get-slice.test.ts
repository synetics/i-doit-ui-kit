import { RowId } from '../../types';
import { getSlice } from './get-slice';

describe('getSlice', () => {
  it.each([
    [[], 0, 2, []],
    [['a', 'b', 'c'], 'a', 'b', ['a', 'b']],
    [['a', 'b', 'c'], 'a', 'c', ['a', 'b', 'c']],
    [['a', 'b', 'c'], 'c', 'a', ['a', 'b', 'c']],
    [['a', 'b', 'c'], 'b', 'b', ['b']],
    [['a', 'b', 'c'], 'asd', 'c', ['c']],
    [['a', 'b', 'c'], 'c', 'asd', ['c']],
    [['a', 'b', 'c'], 'dsa', 'asd', []],
  ])('get correct slice', (ids: RowId[], from: RowId, to: RowId, expected: RowId[]) => {
    expect(getSlice(ids, from, to)).toStrictEqual(expected);
  });
});
