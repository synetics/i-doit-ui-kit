import { RowId } from '../../types';

const areSelected =
  (selected: RowId[]) =>
  (...ids: RowId[]): boolean =>
    selected.length === 0 ? false : ids.some((a) => selected.includes(a));

const areAllSelected =
  (selected: RowId[]) =>
  (...ids: RowId[]): boolean =>
    selected.length === 0 ? false : ids.every((a) => selected.includes(a));

export { areSelected, areAllSelected };
