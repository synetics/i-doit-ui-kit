import { RowId } from '../../types';
import { areAllSelected, areSelected } from './are-selected';

describe('areSelected', () => {
  it.each([
    [[], [], false],
    [['a'], ['a'], true],
    [['a', 'b'], ['a', 'b'], true],
    [['a', 'b', 'c', 'd'], ['a', 'b'], true],
  ])('are selected', (selected: RowId[], checkIds: RowId[], expected: boolean) => {
    expect(areSelected(selected)(...checkIds)).toBe(expected);
  });

  it.each([
    [[], ['a'], false],
    [['a'], ['a', 'b'], true],
    [['b'], ['a'], false],
  ])('are not selected', (selected: RowId[], checkIds: RowId[], expected: boolean) => {
    expect(areSelected(selected)(...checkIds)).toBe(expected);
  });

  it.each([
    [['a', 'b', 'c'], ['a', 'b', 'c'], true],
    [['b', 'c'], ['a', 'b', 'c'], false],
    [[], [], false],
  ])('are all selected', (selected: RowId[], checkIds: RowId[], expected: boolean) => {
    expect(areAllSelected(selected)(...checkIds)).toBe(expected);
  });
});
