import { useMemo, useRef, useState } from 'react';
import { RowId, SelectionApi, UseMultiSelectionType } from './types';
import { areAllSelected, areSelected, getSlice, setSelected } from './helpers';

type MultiSelectionReturnType = {
  selection: RowId[];
  api: SelectionApi;
};

const useMultiSelection = ({ selectedIds, availableIds }: UseMultiSelectionType): MultiSelectionReturnType => {
  const lastSelectedRowId = useRef<RowId | null>(null);
  const [selection, setSelection] = useState<RowId[]>(selectedIds);
  const api = useMemo(() => {
    const isSelected = areSelected(selection);
    const isAllSelected = () => areAllSelected(selection)(...availableIds);
    const set = (rowId: RowId, state: boolean) => setSelection((prev) => setSelected(prev, rowId, state));
    const select = (rowId: RowId) => set(rowId, true);
    const setAll = (state: boolean) => availableIds.forEach((e) => set(e, state));
    const deSelect = (rowId: RowId) => set(rowId, false);
    const toggle = (rowId: RowId) => set(rowId, !isSelected(rowId));
    const selectSlice = (startRowId: RowId, endRowId: RowId): void => {
      getSlice(availableIds, startRowId, endRowId).map(select);
    };
    const click = (rowId: RowId, state: boolean, shiftKey: boolean) => {
      if (shiftKey && lastSelectedRowId.current) {
        selectSlice(lastSelectedRowId.current, rowId);
      } else {
        set(rowId, state);
      }

      lastSelectedRowId.current = state ? rowId : null;
    };

    return {
      isSelected,
      isAllSelected,
      set,
      select,
      setAll,
      deSelect,
      toggle,
      selectSlice,
      click,
    };
  }, [selection, availableIds]);

  return {
    selection,
    api,
  } as const;
};

export { useMultiSelection };
