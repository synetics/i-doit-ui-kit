import { createContext, useContext } from 'react';
import { SelectionApi } from './types';

/* istanbul ignore next */
const SelectionContext = createContext<SelectionApi>({
  isSelected: () => false,
  isAllSelected: () => false,
  set: () => {},
  setAll: () => {},
  select: () => {},
  deSelect: () => {},
  toggle: () => {},
  selectSlice: () => {},
  click: () => {},
});

const useSelection = (): SelectionApi => useContext(SelectionContext);

export { SelectionContext, useSelection };
