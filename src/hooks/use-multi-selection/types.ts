type RowId = number | string;

type UseMultiSelectionType = {
  selectedIds: RowId[];
  availableIds: RowId[];
};

type SelectionApi = {
  isSelected: (rowId: RowId) => boolean;
  isAllSelected: () => boolean;
  set: (rowId: RowId, state: boolean) => void;
  setAll: (state: boolean) => void;
  select: (rowId: RowId) => void;
  deSelect: (rowId: RowId) => void;
  toggle: (rowId: RowId) => void;
  selectSlice: (startRowId: RowId, endRowId: RowId) => void;
  click: (rowId: RowId, state: boolean, shiftKey: boolean) => void;
};

export { UseMultiSelectionType, SelectionApi, RowId };
