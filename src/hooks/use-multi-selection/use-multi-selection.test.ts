import { act, renderHook } from '@testing-library/react-hooks';
import { useMultiSelection } from './use-multi-selection';

describe('useMultiselection', () => {
  const selectedIds = ['a', 'b'];
  const availableIds = ['a', 'b', 'c', 'd', 'e'];

  it('initial call sets initial state', async () => {
    const { result } = renderHook(() => useMultiSelection({ selectedIds, availableIds }));

    expect(result.current.selection).toStrictEqual(selectedIds);
  });

  it('reacts on select', async () => {
    const { result } = renderHook(() => useMultiSelection({ selectedIds, availableIds }));

    act(() => result.current.api.select('c'));
    expect(result.current.selection).toStrictEqual(['a', 'b', 'c']);
  });

  it('reacts on deselect', async () => {
    const { result } = renderHook(() => useMultiSelection({ selectedIds, availableIds }));

    act(() => result.current.api.set('a', false));
    expect(result.current.selection).toStrictEqual(['b']);
  });

  it('reacts on toggle', async () => {
    const { result } = renderHook(() => useMultiSelection({ selectedIds, availableIds }));

    act(() => result.current.api.toggle('a'));
    expect(result.current.selection).toStrictEqual(['b']);
  });

  it('reacts on clicks', async () => {
    const { result } = renderHook(() => useMultiSelection({ selectedIds: [], availableIds }));

    act(() => result.current.api.click('a', true, false));
    act(() => result.current.api.click('c', true, true));
    expect(result.current.selection).toStrictEqual(['a', 'b', 'c']);
  });

  it('reacts on clicks without shift', () => {
    const { result } = renderHook(() => useMultiSelection({ selectedIds: [], availableIds }));

    act(() => result.current.api.click('a', true, false));
    act(() => result.current.api.click('c', true, false));
    expect(result.current.selection).toStrictEqual(['a', 'c']);
  });

  it('resets the last selected on deselect', async () => {
    const { result } = renderHook(() => useMultiSelection({ selectedIds: [], availableIds }));

    act(() => result.current.api.click('a', true, false));
    act(() => result.current.api.click('c', false, false));
    act(() => result.current.api.click('c', true, true));
    expect(result.current.selection).toStrictEqual(['a', 'c']);
  });

  it('reacts on setAll', async () => {
    const { result } = renderHook(() => useMultiSelection({ selectedIds, availableIds }));

    act(() => result.current.api.setAll(true));
    expect(result.current.selection).toStrictEqual(availableIds);
  });

  it('reacts on deselect', async () => {
    const { result } = renderHook(() => useMultiSelection({ selectedIds, availableIds }));

    act(() => result.current.api.deSelect('a'));
    expect(result.current.selection).toStrictEqual(['b']);
  });

  it('reacts on deselect setAll', async () => {
    const { result } = renderHook(() => useMultiSelection({ selectedIds, availableIds }));

    act(() => result.current.api.setAll(false));
    expect(result.current.selection).toStrictEqual([]);
  });

  it('is selected', async () => {
    const { result } = renderHook(() => useMultiSelection({ selectedIds, availableIds }));
    expect(result.current.api.isSelected('a')).toBe(true);
    expect(result.current.api.isSelected('d')).toBe(false);
  });

  it('is all selected', async () => {
    const { result } = renderHook(() => useMultiSelection({ selectedIds, availableIds }));
    expect(result.current.api.isAllSelected()).toBe(false);
    act(() => result.current.api.setAll(true));
    expect(result.current.api.isAllSelected()).toBe(true);
  });

  it('selects slice', async () => {
    const { result } = renderHook(() => useMultiSelection({ selectedIds: [], availableIds }));

    act(() => result.current.api.selectSlice('b', 'd'));
    expect(result.current.selection).toStrictEqual(['b', 'c', 'd']);
  });

  it('selects slice reversed', async () => {
    const { result } = renderHook(() => useMultiSelection({ selectedIds: [], availableIds }));

    act(() => result.current.api.selectSlice('d', 'b'));
    expect(result.current.selection).toStrictEqual(['b', 'c', 'd']);
  });
});
