import { createContext } from 'react';
import { renderHook } from '@testing-library/react-hooks';
import { ContextValue, useHierarchicalValue } from './use-hierarchical-value';

describe('use-hierarchical-value', () => {
  it('will render without crashing', () => {
    const context = createContext<ContextValue<string> | null>(null);
    renderHook(() => useHierarchicalValue<string>(context, 'local-value', () => {}));
  });

  it('will return local value', () => {
    const valueSetter = (v: string) => v;
    const context = createContext<ContextValue<string> | null>(null);
    const { result } = renderHook(() => useHierarchicalValue<string>(context, 'local-value', valueSetter));

    // eslint-disable-next-line jest-dom/prefer-to-have-value
    expect(result.current).toHaveProperty('value', 'local-value');
    expect(result.current).toHaveProperty('setValue', valueSetter);
  });

  it('will return context value', () => {
    const valueSetter = (v: string) => v;
    const context = createContext<ContextValue<string> | null>({
      value: 'context-value',
      setValue: valueSetter,
    });

    const { result } = renderHook(() => useHierarchicalValue<string>(context, 'local-value', () => {}));

    // eslint-disable-next-line jest-dom/prefer-to-have-value
    expect(result.current).toHaveProperty('value', 'context-value');
    expect(result.current).toHaveProperty('setValue', valueSetter);
  });
});
