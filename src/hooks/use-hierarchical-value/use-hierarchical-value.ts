import { Context, useContext } from 'react';
import { useControlledState } from '../use-controlled-state';

type ContextValue<T> = {
  value: T;
  setValue: (value: T) => void;
};

/**
 * Uses the state with setter from:
 *
 * * context (if available)
 * * using controlled value (if available)
 * * from the local context as a fallback
 */
const useHierarchicalValue = <T>(
  context: Context<ContextValue<T> | null>,
  value: T,
  setValue?: (value: T) => void,
): ContextValue<T> => {
  const [localValue, setLocalValue] = useControlledState<T>(value, setValue);
  const contextValue = useContext<ContextValue<T> | null>(context);

  if (contextValue) {
    return contextValue;
  }

  return {
    value: localValue,
    setValue: setLocalValue,
  } as const;
};

export { ContextValue, useHierarchicalValue };
