import { act, renderHook } from '@testing-library/react-hooks';
import { useRangeSelection } from './use-range-selection';
import { ActiveState, RangeType } from './types';

const compare = (a: number, b: number): number => a - b;
const state: Record<ActiveState, ActiveState> = {
  from: 'from',
  to: 'to',
};
const getValue = (from: number, to: number): RangeType<number> => ({
  from,
  to,
});

describe('use-range-selection', () => {
  const initialValue = getValue(4, 10);
  it.each([
    [state.from, 4, state.to, getValue(4, 10)],
    [state.from, 10, state.to, getValue(10, 10)],
    [state.from, 7, state.to, getValue(7, 10)],
    [state.from, 1, state.to, getValue(1, 10)],
    [state.from, 20, state.from, getValue(4, 20)],
    [state.to, 4, state.from, getValue(4, 4)],
    [state.to, 10, state.from, getValue(4, 10)],
    [state.to, 7, state.from, getValue(4, 7)],
    [state.to, 1, state.to, getValue(1, 10)],
    [state.to, 20, state.from, getValue(4, 20)],
  ])(
    'sets correct value and changes active state',
    async (active: ActiveState, selected: number, expectedState: ActiveState, expectedValue: RangeType<number>) => {
      const onChange = jest.fn();
      const setActive = jest.fn();
      const { result } = renderHook(() =>
        useRangeSelection<number>({
          value: initialValue,
          onChange,
          active,
          setActive,
          compare,
        }),
      );

      expect(result.current.active).toBe(active);
      await act(() => Promise.resolve(result.current.select(selected)));

      expect(setActive).toHaveBeenCalledWith(expectedState);
      expect(onChange).toHaveBeenCalledWith(expectedValue);
    },
  );
  it.each([
    [state.from, 4, state.to, getValue(4, 10)],
    [state.from, 10, state.to, getValue(10, 10)],
    [state.from, 7, state.to, getValue(7, 10)],
    [state.from, 1, state.to, getValue(1, 10)],
    [state.from, 20, state.from, getValue(4, 20)],
    [state.to, 4, state.from, getValue(4, 4)],
    [state.to, 10, state.from, getValue(4, 10)],
    [state.to, 7, state.from, getValue(4, 7)],
    [state.to, 1, state.to, getValue(1, 10)],
    [state.to, 20, state.from, getValue(4, 20)],
  ])(
    'works with uncontrolled active',
    async (active: ActiveState, selected: number, expectedState: ActiveState, expectedValue: RangeType<number>) => {
      const onChange = jest.fn();
      const { result } = renderHook(() =>
        useRangeSelection<number>({
          value: initialValue,
          active,
          onChange,
          compare,
        }),
      );

      expect(result.current.active).toBe(active);
      await act(() => Promise.resolve(result.current.select(selected)));

      expect(result.current.active).toBe(expectedState);
      expect(onChange).toHaveBeenCalledWith(expectedValue);
    },
  );
  it.each([
    [4, getValue(4, 10)],
    [10, getValue(10, 10)],
    [7, getValue(7, 10)],
    [1, getValue(1, 10)],
    [20, getValue(20, 20)],
  ])('set start directly', async (selected: number, expectedValue: RangeType<number>) => {
    const onChange = jest.fn();
    const { result } = renderHook(() =>
      useRangeSelection<number>({
        value: initialValue,
        onChange,
        compare,
      }),
    );

    await act(() => Promise.resolve(result.current.setStart(selected)));

    expect(onChange).toHaveBeenCalledWith(expectedValue);
  });
  it.each([
    [4, getValue(4, 4)],
    [10, getValue(4, 10)],
    [7, getValue(4, 7)],
    [1, getValue(1, 1)],
    [20, getValue(4, 20)],
  ])('set end directly', async (selected: number, expectedValue: RangeType<number>) => {
    const onChange = jest.fn();
    const { result } = renderHook(() =>
      useRangeSelection<number>({
        value: initialValue,
        onChange,
        compare,
      }),
    );

    await act(() => Promise.resolve(result.current.setEnd(selected)));

    expect(onChange).toHaveBeenCalledWith(expectedValue);
  });
});
