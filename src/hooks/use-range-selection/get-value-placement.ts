import { PlacementType, RangeType } from './types';

export const getValuePlacement = <T>(value: T, range: RangeType<T>, compare: (a: T, b: T) => number): PlacementType => {
  const hasFrom = range.from !== null;
  const hasTo = range.to !== null;
  const compareToFrom = range.from !== null ? compare(range.from, value) : 0;
  const compareToTo = range.to !== null ? compare(range.to, value) : 0;
  return {
    before: hasFrom && compareToFrom > 0,
    start: hasFrom && compareToFrom === 0,
    after: hasTo && compareToTo < 0,
    end: hasTo && compareToTo === 0,
    between: compareToFrom < 0 && compareToTo > 0,
  };
};
