export type ActiveState = 'from' | 'to';

export type PlacementType = {
  before: boolean;
  start: boolean;
  end: boolean;
  after: boolean;
  between: boolean;
};

export type RangeType<T> = {
  from: T | null;
  to: T | null;
};

export type RangeSelectionApi<T> = {
  value: RangeType<T>;
  active: ActiveState;
  select: (value: T) => void;
  setStart: (value: T) => void;
  setEnd: (value: T) => void;
  getType: (value: T) => PlacementType;
};
