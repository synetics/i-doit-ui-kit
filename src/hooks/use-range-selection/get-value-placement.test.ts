import { getValuePlacement } from './get-value-placement';
import { PlacementType } from './types';

const getPlacement = (placement: Partial<PlacementType>): PlacementType => ({
  before: false,
  start: false,
  end: false,
  after: false,
  between: false,
  ...placement,
});

describe('get-value-placement', () => {
  it.each([
    [3, null, null, getPlacement({})],
    [3, 3, null, getPlacement({ start: true })],
    [3, null, 10, getPlacement({})],
    [1, 2, 10, getPlacement({ before: true })],
    [2, 2, 10, getPlacement({ start: true })],
    [10, 2, 10, getPlacement({ end: true })],
    [15, 2, 10, getPlacement({ after: true })],
    [5, 2, 10, getPlacement({ between: true })],
  ])(
    'correct placement value for number',
    (value: number, from: number | null, to: number | null, expectedPlacement: PlacementType) => {
      const placement = getValuePlacement<number>(value, { from, to }, (a, b) => a - b);
      expect(placement).toStrictEqual(expectedPlacement);
    },
  );

  it.each([
    [1, 10, 2, getPlacement({ after: true })],
    [2, 10, 2, getPlacement({ end: true })],
    [10, 10, 2, getPlacement({ start: true })],
    [15, 10, 2, getPlacement({ before: true })],
    [5, 10, 2, getPlacement({ between: true })],
  ])(
    'correct placement value for number reversal',
    (value: number, from: number | null, to: number | null, expectedPlacement: PlacementType) => {
      const placement = getValuePlacement<number>(value, { from, to }, (a, b) => b - a);
      expect(placement).toStrictEqual(expectedPlacement);
    },
  );

  it.each([
    ['a', 'e', 'x', getPlacement({ before: true })],
    ['e', 'e', 'x', getPlacement({ start: true })],
    ['x', 'e', 'x', getPlacement({ end: true })],
    ['z', 'e', 'x', getPlacement({ after: true })],
    ['g', 'e', 'x', getPlacement({ between: true })],
  ])(
    'correct placement value for strings',
    (value: string, from: string | null, to: string | null, expectedPlacement: PlacementType) => {
      const placement = getValuePlacement<string>(value, { from, to }, (a, b) => a.localeCompare(b));
      expect(placement).toStrictEqual(expectedPlacement);
    },
  );
});
