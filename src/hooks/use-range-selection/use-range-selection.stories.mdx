import { useState } from 'react';
import { Canvas, Meta, Story } from '@storybook/blocks';
import { Button } from '../../components';
import { FormGroup, Label } from '../../components/Form/Infrastructure';
import { Input } from '../../components/Form/Control';
import { useRangeSelection } from './use-range-selection';

<Meta title="Utils/use-range-selection" />

# use-range-selection

## Basics

Use range selection is a hook that helps developers to implement range selection in the common way.

Selection logic:

- if there are no range selected - select sets from then to
- if the selected value is less than from - it sets from
- if the selected value is higher than to - it sets to
- if the selected value between current from and to - active field will be set

## Usage

In order to use the hook, you have to:

- define, what type of data user will select and pass the value and callback on change
- define the comparison function to let hook understand what is the order of the values

As result, it gives you the method to trigger selection and to evaluate what state does your value have.

```
const [value, onChange] = useState<RangeType<number>>({ from: 10, to: 20 });
const { active, select, getType } = useRangeSelection<number>({
  value,
  onChange,
  compare: (a, b) => a - b,
});
```

## PlacementType

getType method gives you an information, in which part of the selection is your value is placed.

```
type PlacementType = {
  before: boolean;
  start: boolean;
  between: boolean;
  end: boolean;
  after: boolean;
};
```

For example, if you have selection 10 to 20, then all values less than 10 will have `before` set to true, 10 will have a `start` set to true, values between 10 and 20 will have `between`, 20 - `end` and higher than 20 - `after`.

This helps you style your selector items accordingly.

## Active state

Active state tells you, which value is active - what will be set. You can control this value to enable user to select what to select.

In the following example, you can focus from or to input to select the from or to field.

The active state of the form field gives user a hint, what will be selected.

<Canvas>
  <Story name="Controlled active state">
    {() => {
      const [active, setActive] = useState('from');
      const [value, setValue] = useState({ from: 3, to: 5 });
      const selectionApi = useRangeSelection({
        active,
        setActive,
        value,
        onChange: setValue,
        compare: (a, b) => a - b,
      });
      return (
        <div>
          <div className="d-flex">
            <FormGroup className="mr-4">
              <Label id="from">From</Label>
              <Input
                type="number"
                id="from"
                value={value.from}
                onChange={selectionApi.setStart}
                active={active === 'from'}
                onFocus={() => setActive('from')}
              />
            </FormGroup>
            <FormGroup>
              <Label id="to">To</Label>
              <Input
                type="number"
                id="to"
                value={value.to}
                onChange={selectionApi.setEnd}
                active={active === 'to'}
                onFocus={() => setActive('to')}
              />
            </FormGroup>
          </div>
          <div className="d-flex flex-wrap">
            {new Array(10).fill(1).map((_, i) => (
              <Box onClick={() => selectionApi.select(i)} type={selectionApi.getType(i)}>
                {i}
              </Box>
            ))}
          </div>
        </div>
      );
    }}
  </Story>
</Canvas>

export const getColor = ({ start, end, between }) => {
  if (start || end || between) {
    return 'white';
  }
  return undefined;
};

export const getBackground = ({ start, end, between }) => {
  if (start || end) {
    return 'blue';
  }
  if (between) {
    return 'lightblue';
  }
  return undefined;
};

export const Box = ({ background, children, type, onClick }) => (
  <Button
    variant="outline"
    className="m-2"
    style={{
      background: getBackground(type) || background,
      color: (getBackground(type) && background) || getColor(type),
    }}
    onClick={onClick}
  >
    {children}
  </Button>
);

## Headless usage

The range selection logic is done using headless approach. Developer has full control on the view of the selectors.

In the following example, we can see additional logic to mark some items.

<Canvas>
  <Story name="Range selection with numbers">
    {() => {
      const [value, setValue] = useState({
        from: 2,
        to: 6,
      });
      const { active, select, getType } = useRangeSelection({
        value,
        onChange: setValue,
        compare: (a, b) => a - b,
      });
      const activeValue = value[active];
      const ranges = [
        { from: 1, to: 4, color: 'red' },
        { from: 10, to: 14, color: 'coral' },
      ];
      return (
        <div className="d-flex flex-wrap">
          {new Array(20).fill(1).map((_, i) => (
            <Box
              background={ranges.find((a) => a.from <= i && a.to >= i)?.color}
              key={`box-${i}`}
              type={getType(i)}
              onClick={() => select(i)}
            >
              {i === activeValue ? `[${i}]` : i}
            </Box>
          ))}
        </div>
      );
    }}
  </Story>
</Canvas>

## Usage with other types

You can use other types for selection. In the following example, we use dates

export const months = ['Jan', 'Feb', 'Mar', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];

<Canvas>
  <Story name="Range selection with dates">
    {() => {
      const year = new Date().getFullYear();
      const compare = (a, b) => a.toISOString().localeCompare(b.toISOString());
      const [value, setValue] = useState({
        from: new Date(year, 4, 10),
        to: new Date(year, 6, 7),
      });
      const { active, select, getType } = useRangeSelection({
        value,
        onChange: setValue,
        compare,
      });
      const [activeMonth, setActiveMonth] = useState((value[active] || new Date()).getMonth());
      return (
        <div>
          <div>
            {new Array(12).fill(1).map((_, month) => {
              const date = new Date(year, month, 1);
              if (date.getFullYear() !== year) {
                return null;
              }
              return (
                <Box
                  key={`month-${month}`}
                  type={{
                    between: value?.from && value?.to && value.from.getMonth() <= month && value.to.getMonth() >= month,
                    start: activeMonth === month,
                  }}
                  onClick={() => setActiveMonth(month)}
                >
                  {months[date.getMonth()]}
                </Box>
              );
            })}
          </div>
          <div className="d-flex flex-wrap">
            {new Array(32).fill(1).map((_, i) => {
              const date = new Date(year, activeMonth, i);
              if (date.getMonth() !== activeMonth) {
                return null;
              }
              return (
                <Box key={`box-${i}`} type={getType(date)} onClick={() => select(date)}>
                  {date.getDate()}
                </Box>
              );
            })}
          </div>
        </div>
      );
    }}
  </Story>
</Canvas>
