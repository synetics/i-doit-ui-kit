import { useCallback } from 'react';
import { ValueProps } from '../../components/Form/Infrastructure';
import { useEvent } from '../use-event';
import { useControlledState } from '../use-controlled-state';
import { ActiveState, PlacementType, RangeSelectionApi, RangeType } from './types';
import { getValuePlacement } from './get-value-placement';

type UseRangeSelectionOptions<T> = Required<ValueProps<RangeType<T>>> & {
  compare: (a: T, b: T) => number;
  active?: ActiveState;
  setActive?: (state: ActiveState) => void;
};

export const useRangeSelection = <T>({
  active: passedActive = 'from',
  setActive: passedSetActive = undefined,
  value,
  onChange,
  compare,
}: UseRangeSelectionOptions<T>): RangeSelectionApi<T> => {
  const [active, setActive] = useControlledState<ActiveState>(passedActive, passedSetActive);
  const getType = useCallback((v: T): PlacementType => getValuePlacement(v, value, compare), [compare, value]);
  const setStart = useEvent((change: T) => {
    onChange({
      from: change,
      to: value.to && compare(value.to, change) < 0 ? change : value.to,
    });
    setActive('to');
  });
  const setEnd = useEvent((change: T) => {
    onChange({
      from: value.from && compare(value.from, change) > 0 ? change : value.from,
      to: change,
    });
    setActive('from');
  });
  const select = useEvent((change: T) => {
    const type = getType(change);
    if (type.before) {
      setStart(change);
      return;
    }
    if (type.after || active === 'to') {
      setEnd(change);
      return;
    }
    setStart(change);
  });

  return {
    active,
    value,
    setStart,
    setEnd,
    select,
    getType,
  };
};
