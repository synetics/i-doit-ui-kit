import { DependencyList, MutableRefObject, useEffect, useRef } from 'react';

const useDelay = (
  callback: () => void,
  delay: number,
  parameters: DependencyList,
): MutableRefObject<NodeJS.Timeout | undefined> => {
  const timeout = useRef<NodeJS.Timeout>();
  const callbackRef = useRef(callback);
  callbackRef.current = callback;

  useEffect(() => {
    let isMounted = true;
    if (timeout.current) {
      clearTimeout(timeout.current);
    }

    timeout.current = setTimeout(() => {
      if (isMounted) {
        callbackRef.current();
      }
    }, delay);

    return () => {
      if (timeout.current) {
        clearTimeout(timeout.current);
      }
      isMounted = false;
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, parameters);

  return timeout;
};

export { useDelay };

export default useDelay;
