import { renderHook } from '@testing-library/react-hooks';
import { useRaf } from './use-raf';

describe('useRaf()', () => {
  let spy: jest.SpyInstance<number, [callback: FrameRequestCallback]>;

  beforeEach(() => {
    spy = jest.spyOn(window, 'requestAnimationFrame').mockImplementation((cb) => {
      cb(0);
      return 0;
    });
  });

  afterEach(() => {
    spy.mockRestore();
  });

  it('does not crash', () => {
    const cb = jest.fn();

    const { unmount } = renderHook(() => useRaf(cb));

    expect(cb).toHaveBeenCalled();
    unmount();
  });
});
