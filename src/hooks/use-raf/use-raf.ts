import { useLayoutEffect, useRef } from 'react';

type Callback = () => void;

/**
 * Perform the function on request animation frame
 *
 * @param {Callback} callback
 */
export const useRaf = (callback: Callback): void => {
  const callbackRef = useRef<Callback>(callback);
  callbackRef.current = callback;

  useLayoutEffect(() => {
    let timerId: number;
    const f = () => {
      callbackRef.current();

      timerId = requestAnimationFrame(f);
    };

    timerId = requestAnimationFrame(f);

    return () => cancelAnimationFrame(timerId);
  }, []);
};
