import { getIndexInVector } from './get-index-in-vector';

const vectorA = [
  { size: 2, unfocusable: false },
  { size: 2, unfocusable: false },
  { size: 2, unfocusable: false },
  { size: 2, unfocusable: false },
  { size: 2, unfocusable: false },
];
const vectorB = [
  { size: 1, unfocusable: false },
  { size: 1, unfocusable: false },
  { size: 1, unfocusable: false },
  { size: 1, unfocusable: false },
  { size: 1, unfocusable: false },
  { size: 1, unfocusable: false },
  { size: 1, unfocusable: false },
  { size: 1, unfocusable: false },
  { size: 1, unfocusable: false },
  { size: 1, unfocusable: false },
];

describe('getIndexInVector', () => {
  it.each([
    [0, 0],
    [1, 2],
    [2, 4],
    [3, 6],
    [4, 8],
  ])('computes the right index', (x: number, expected: number) => {
    expect(getIndexInVector(x, vectorA, vectorB)).toBe(expected);
  });

  it.each([
    [0, 0],
    [1, 0],
    [2, 1],
    [3, 1],
    [4, 2],
    [5, 2],
    [6, 3],
    [7, 3],
    [8, 4],
    [9, 4],
  ])('computes the right index reversed', (x: number, expected: number) => {
    expect(getIndexInVector(x, vectorB, vectorA)).toBe(expected);
  });
});
