/**
 * Get the index of the element in the nextRow with the same position like in X'th element in the currentRow
 *
 * @param {number} x
 * @param {number[]} currentRow
 * @param {number[]} nextRow
 *
 * @return {number}
 */
export const getIndexInVector = (
  x: number,
  currentRow: { size: number; unfocusable: boolean }[],
  nextRow: { size: number; unfocusable: boolean }[],
): number => {
  const currentSum = currentRow.slice(0, x).reduce((result, item) => result + item.size, 0);

  for (let sum = 0, i = 0; i < nextRow.length - 1; i += 1) {
    const nextSum = sum + nextRow[i].size;

    if (sum <= currentSum && nextSum > currentSum) {
      return i;
    }

    sum = nextSum;
  }

  return nextRow.length - 1;
};
