import { KeyboardEvent, useState } from 'react';
import { handleKey } from '../../utils';
import { useEventListener } from '../use-event-listener';
import { getIndexInVector } from './get-index-in-vector';

type Callback = ({ x, y }: { x: number; y: number }) => void;

type UseKeyboardMatrixResponseType = {
  x: number;
  y: number;
  setCoord: Callback;
  isSelected: (x: number, y: number) => boolean;
};

/**
 * Hook  to navigate in a table with different cell sizes
 * @param {Element|null} trigger
 * @param {number[][]} matrix
 */

export const firstFocusableElement = (matrix: { size: number; unfocusable: boolean }[][]) => {
  let x = 0;
  const y = matrix.findIndex((el) =>
    el.find((e, i) => {
      if (!e?.unfocusable) {
        x = i;
        return true;
      }
      return false;
    }),
  );

  return {
    x,
    y,
  };
};
const findLastFocusableInRow = (row: { size: number; unfocusable: boolean }[]) => {
  const rowArray = row.map((cell) => cell?.unfocusable);
  return rowArray.lastIndexOf(false);
};

const findNextAvailableCell = (
  oldX: number,
  oldY: number,
  dX: number,
  dY: number,
  x: number,
  y: number,
  matrix: { size: number; unfocusable: boolean }[][],
  findNext = 0,
): { x: number; y: number } => {
  const newY = Math.max(Math.min(oldY + dY, matrix.length - 1), findNext);
  const newX = Math.max(Math.min(oldX + dX, matrix[y].length - 1), 0);
  const nextX = getIndexInVector(newX, matrix[y], matrix[newY]);

  if (dX === -Infinity && dY === -Infinity) {
    return firstFocusableElement(matrix);
  }
  if (dX === Infinity && dY === Infinity) {
    let lastY = matrix.length - 1;
    let resultCell = findLastFocusableInRow(matrix[lastY]);
    if (resultCell === -1) {
      lastY -= 1;
      resultCell = findLastFocusableInRow(matrix[lastY]);
    }
    return { y: lastY, x: resultCell };
  }
  if (dX === Infinity && dY === 0) {
    return { x: findLastFocusableInRow(matrix[y]), y: oldY };
  }

  if (dX === -Infinity && dY === 0) {
    const rowArray = matrix[y].map((cell) => cell?.unfocusable);
    return { x: rowArray.indexOf(false), y: oldY };
  }

  if (dY === -Infinity && dX === 0) {
    if (!matrix[newY][newX] || matrix[newY][newX].unfocusable) {
      return findNextAvailableCell(oldX, oldY, dX, dY, oldX, y, matrix, findNext + 1);
    }
  }

  if (matrix[newY][newX]?.unfocusable) {
    if (dY >= 1) {
      return findNextAvailableCell(oldX, oldY, dX, dY + 1, x, y, matrix);
    }
    if (dY <= -1 && newY !== 0) {
      return findNextAvailableCell(oldX, oldY, dX, dY - 1, x, y, matrix);
    }
    if (dX >= 1 && x + dX < matrix[y].length) {
      return findNextAvailableCell(oldX, oldY, dX + 1, dY, x, y, matrix);
    }
    if (dX <= -1 && newX !== 0) {
      return findNextAvailableCell(oldX, oldY, dX - 1, dY, x, y, matrix);
    }
  }

  if (matrix[newY][nextX].unfocusable) {
    return {
      x: oldX,
      y: oldY,
    };
  }
  return {
    x: nextX,
    y: newY,
  };
};

const useKeyboardMatrix = (
  trigger: Element | null,
  matrix: { size: number; unfocusable: boolean }[][],
): UseKeyboardMatrixResponseType => {
  const [{ x, y }, setCoord] = useState(firstFocusableElement(matrix));

  const isSelected = (a: number, b: number): boolean => a === x && b === y;
  const move = (dX: number, dY: number) => (): void => {
    setCoord(({ x: oldX, y: oldY }) => findNextAvailableCell(oldX, oldY, dX, dY, x, y, matrix));
  };

  const handlers = {
    ArrowLeft: move(-1, 0),
    ArrowRight: move(1, 0),
    ArrowUp: move(0, -1),
    ArrowDown: move(0, 1),
    PageUp: move(0, -Infinity),
    PageDown: move(0, Infinity),
    Home: (e: KeyboardEvent) => {
      if (!e.shiftKey) {
        move(-Infinity, e.ctrlKey ? -Infinity : 0)();
      }
    },
    End: (e: KeyboardEvent) => {
      if (!e.shiftKey) {
        move(Infinity, e.ctrlKey ? Infinity : 0)();
      }
    },
  };

  useEventListener('keydown', handleKey(handlers), trigger);
  return { x, y, setCoord, isSelected };
};

export { useKeyboardMatrix };
