import { fireEvent } from '@testing-library/react';
import { renderHook } from '@testing-library/react-hooks';
import { act } from 'react-test-renderer';
import user from '@testing-library/user-event';
import { firstFocusableElement, useKeyboardMatrix } from './use-keyboard-matrix';

const matrix = [
  [
    { size: 1, unfocusable: false },
    { size: 1, unfocusable: false },
    { size: 1, unfocusable: false },
  ],
  [
    { size: 1, unfocusable: false },
    { size: 1, unfocusable: false },
    { size: 1, unfocusable: false },
  ],
  [
    { size: 1, unfocusable: false },
    { size: 1, unfocusable: false },
    { size: 1, unfocusable: false },
  ],
  [
    { size: 1, unfocusable: false },
    { size: 1, unfocusable: false },
    { size: 1, unfocusable: false },
  ],
];

describe('use-keyboard-matrix', () => {
  it.each([
    ['{arrowdown}', { x: 0, y: 1 }],
    ['{arrowright}', { x: 1, y: 0 }],
    ['{end}', { x: 2, y: 0 }],
    ['{Control>}{end}{/Control}', { x: 2, y: 3 }],
  ])('calls setCoord on %s, change coordinates from %o to %o', async (text: string, end: { x: number; y: number }) => {
    const { result } = renderHook(() => useKeyboardMatrix(document.body, matrix));
    await user.type(document.body, text);
    expect(result.current.x).toEqual(end.x);
    expect(result.current.y).toEqual(end.y);
  });

  it.each([
    ['{arrowleft}', { x: 1, y: 3 }],
    ['{arrowup}', { x: 2, y: 2 }],
    ['{home}', { x: 0, y: 3 }],
    ['{Control>}{home}{/Control}', { x: 0, y: 0 }],
  ])('calls setCoord on %s, change coordinates from %o to %o', async (text: string, end: { x: number; y: number }) => {
    const { result } = renderHook(() => useKeyboardMatrix(document.body, matrix));

    await user.type(document.body, '{Control>}{end}{/Control}');
    await user.type(document.body, text);
    expect(result.current.x).toEqual(end.x);
    expect(result.current.y).toEqual(end.y);
  });

  it('check PageUp, PageDown', async () => {
    const { result } = renderHook(() => useKeyboardMatrix(document.body, matrix));
    await act(() => {
      fireEvent.keyDown(document.body, { key: 'PageDown' });
    });
    expect(result.current.x).toEqual(0);
    expect(result.current.y).toEqual(3);
    await act(() => {
      fireEvent.keyDown(document.body, { key: 'PageUp' });
    });

    expect(result.current.x).toEqual(0);
    expect(result.current.y).toEqual(0);
    await act(() => {
      fireEvent.keyDown(document.body, { key: 'PageUp' });
    });
    expect(result.current.x).toEqual(0);
    expect(result.current.y).toEqual(0);
  });

  it('alternative cell size', async () => {
    const alternativeMatrix = [
      [
        { size: 1, unfocusable: false },
        { size: 2, unfocusable: false },
        { size: 1, unfocusable: false },
      ],
      [
        { size: 1, unfocusable: false },
        { size: 1, unfocusable: false },
        { size: 1, unfocusable: false },
        { size: 1, unfocusable: false },
      ],
    ];
    const { result } = renderHook(() => useKeyboardMatrix(document.body, alternativeMatrix));
    await user.type(document.body, '{arrowright}');
    await user.type(document.body, '{arrowright}');
    await user.type(document.body, '{arrowdown}');

    expect(result.current.x).toEqual(3);
    expect(result.current.y).toEqual(1);
    await user.type(document.body, '{arrowleft}');

    await user.type(document.body, '{arrowup}');
    expect(result.current.x).toEqual(1);
    expect(result.current.y).toEqual(0);

    await act(() => {
      fireEvent.keyDown(document.body, { key: 'PageDown' });
    });
    await act(() => {
      fireEvent.keyDown(document.body, { key: 'PageDown' });
    });
    await user.type(document.body, '{arrowdown}');
    expect(result.current.x).toEqual(1);
    expect(result.current.y).toEqual(1);
    await act(() => {
      fireEvent.keyDown(document.body, { key: 'PageUp' });
    });
    await user.type(document.body, '{arrowup}');

    expect(result.current.x).toEqual(1);
    expect(result.current.y).toEqual(0);

    await user.type(document.body, '{arrowleft}');
    await user.type(document.body, '{arrowdown}');
    await user.type(document.body, '{arrowup}');
    expect(result.current.x).toEqual(0);
    expect(result.current.y).toEqual(0);
  });

  it('alternative matrix with unfocusable cell for PageUp', async () => {
    const alternativeMatrix = [
      [
        { size: 1, unfocusable: false },
        { size: 1, unfocusable: true },
        { size: 1, unfocusable: false },
        { size: 1, unfocusable: true },
      ],
      [
        { size: 1, unfocusable: false },
        { size: 1, unfocusable: false },
        { size: 1, unfocusable: false },
        { size: 1, unfocusable: false },
      ],
      [
        { size: 1, unfocusable: false },
        { size: 1, unfocusable: false },
        { size: 1, unfocusable: false },
        { size: 1, unfocusable: false },
      ],
    ];
    const { result } = renderHook(() => useKeyboardMatrix(document.body, alternativeMatrix));
    await user.type(document.body, '{Control>}{end}{/Control}');

    await act(() => {
      fireEvent.keyDown(document.body, { key: 'PageUp' });
    });

    expect(result.current.x).toEqual(3);
    expect(result.current.y).toEqual(1);
  });

  it('alternative matrix with unfocusable cell', async () => {
    const alternativeMatrix = [
      [
        { size: 1, unfocusable: false },
        { size: 1, unfocusable: true },
        { size: 1, unfocusable: false },
        { size: 1, unfocusable: true },
      ],
      [
        { size: 1, unfocusable: true },
        { size: 1, unfocusable: true },
        { size: 1, unfocusable: true },
        { size: 1, unfocusable: false },
      ],
      [
        { size: 1, unfocusable: false },
        { size: 1, unfocusable: false },
        { size: 1, unfocusable: false },
        { size: 1, unfocusable: true },
      ],
      [
        { size: 1, unfocusable: true },
        { size: 1, unfocusable: true },
        { size: 1, unfocusable: true },
        { size: 1, unfocusable: true },
      ],
    ];
    const { result } = renderHook(() => useKeyboardMatrix(document.body, alternativeMatrix));
    await user.type(document.body, '{Control>}{end}{/Control}');

    expect(result.current.x).toEqual(2);
    expect(result.current.y).toEqual(2);

    await user.type(document.body, '{arrowright}');
    expect(result.current.x).toEqual(2);
    expect(result.current.y).toEqual(2);

    await user.type(document.body, '{arrowup}');
    expect(result.current.x).toEqual(2);
    expect(result.current.y).toEqual(0);

    await user.type(document.body, '{arrowleft}');
    expect(result.current.x).toEqual(0);
    expect(result.current.y).toEqual(0);

    await user.type(document.body, '{arrowdown}');

    expect(result.current.x).toEqual(0);
    expect(result.current.y).toEqual(2);
  });
  it('all unfocusable', () => {
    const matrixNew = [
      [
        { size: 1, unfocusable: true },
        { size: 2, unfocusable: true },
        { size: 3, unfocusable: true },
      ],
      [
        { size: 4, unfocusable: true },
        { size: 5, unfocusable: true },
        { size: 6, unfocusable: true },
      ],
      [
        { size: 7, unfocusable: true },
        { size: 8, unfocusable: true },
        { size: 9, unfocusable: true },
      ],
    ];

    const result = firstFocusableElement(matrixNew);

    expect(result).toEqual({ x: 0, y: -1 });
  });
});
