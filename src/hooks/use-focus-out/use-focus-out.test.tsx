import { fireEvent, render, screen } from '@testing-library/react';
import { renderHook } from '@testing-library/react-hooks';
import React from 'react';
import { useFocusOut } from './use-focus-out';

const buttonContent = 'testButton';

describe('useFocusOut()', () => {
  it('add event listener to element and calls it', () => {
    const callback = jest.fn();
    const fakeRef = React.createRef<HTMLButtonElement>();
    render(
      <button data-testid="test-focus-button" ref={fakeRef}>
        {buttonContent}
      </button>,
    );

    renderHook(() => useFocusOut(fakeRef, callback));

    const focusElem = screen.getByTestId('test-focus-button');

    fireEvent.focusIn(focusElem);
    fireEvent.focusOut(focusElem);

    expect(callback).toHaveBeenCalledTimes(1);
  });

  it('do not call if no element', () => {
    const callback = jest.fn();
    const fakeRef = React.createRef<HTMLButtonElement>();
    render(<button data-testid="testButton">{buttonContent}</button>);

    renderHook(() => useFocusOut(fakeRef, callback));

    const focusElem = screen.getByTestId('testButton');

    fireEvent.focusIn(focusElem);
    fireEvent.focusOut(focusElem);

    expect(callback).toHaveBeenCalledTimes(0);
  });

  it('do not call if event on child', () => {
    const callback = jest.fn();
    const fakeRef = React.createRef<HTMLDivElement>();
    render(
      <div ref={fakeRef}>
        <button data-testid="testButton">{buttonContent}</button>
        <button id="test_selector">{buttonContent}</button>
      </div>,
    );

    renderHook(() => useFocusOut(fakeRef, callback, { adoptiveParentsSelectors: ['#test_selector'] }));

    const focusElem = screen.getByTestId('testButton');

    fireEvent.focusIn(focusElem);
    fireEvent.focusOut(focusElem);

    expect(callback).toHaveBeenCalledTimes(0);
  });
});
