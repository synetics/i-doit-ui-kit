import { FocusEvent, MutableRefObject } from 'react';
import { AbstractFunction } from '../../types/function';
import { useEventListener } from '../use-event-listener';

type AdoptiveParentsSelectors = keyof HTMLElementTagNameMap | string;
type Options = { adoptiveParentsSelectors?: AdoptiveParentsSelectors[] };

/**
 * Invokes the callback function when focus moves outside of the ref
 *
 * @param ref {Object}
 * @param callback {function}
 * @param adoptiveParentsSelectors {string[]} - css selectors
 */
export const useFocusOut = (
  ref: MutableRefObject<HTMLElement | null>,
  callback?: AbstractFunction,
  options?: Options,
): void => {
  const handler = (event: FocusEvent) => {
    if (!ref || !ref.current || !callback) {
      return;
    }

    const isChild = ref.current !== event.target && ref.current.contains(event.target as Element);
    const hasAdoptiveParents = options?.adoptiveParentsSelectors?.some(
      (selector) => event.target && (event.target as Element).closest(selector),
    );

    if (!isChild && !hasAdoptiveParents) {
      callback(event);
    }
  };

  useEventListener('focusin', handler, document.body);
  useEventListener('click', handler, document.body);
};
