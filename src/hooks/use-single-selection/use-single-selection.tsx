import { useMemo, useState } from 'react';
import { RowId, SelectionApi, UseSingleSelectionType } from './types';

type SingleSelectionReturnType = {
  selection?: RowId;
  api: SelectionApi;
};

const useSingleSelection = ({ selectedId }: UseSingleSelectionType): SingleSelectionReturnType => {
  const [selection, set] = useState<RowId>(selectedId);
  const api = useMemo(() => {
    const selected =
      (rowId: RowId) =>
      (...ids: RowId[]): boolean =>
        ids.includes(rowId);
    const isSelected = selected(selection);

    return {
      isSelected,
      set,
    };
  }, [selection]);

  return {
    selection,
    api,
  } as const;
};

export { useSingleSelection };
