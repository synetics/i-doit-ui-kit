type RowId = number | string;

type UseSingleSelectionType = {
  selectedId: RowId;
};

type SelectionApi = {
  isSelected: (rowId: RowId) => boolean;
  set: (rowId: RowId) => void;
};

export { UseSingleSelectionType, SelectionApi, RowId };
