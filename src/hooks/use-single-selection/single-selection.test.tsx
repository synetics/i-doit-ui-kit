import { renderHook } from '@testing-library/react-hooks';
import { useSingleSelectionContext } from './selection-context';

describe('useSingleSelectionContext', () => {
  it('check context', () => {
    const { result } = renderHook(() => useSingleSelectionContext());
    expect(result.current.set).toBeTruthy();
    expect(result.current.isSelected).toBeTruthy();
  });
});
