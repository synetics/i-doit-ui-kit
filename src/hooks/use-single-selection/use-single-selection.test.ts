import { act, renderHook } from '@testing-library/react-hooks';
import { useSingleSelection } from './use-single-selection';

describe('useMultiselection', () => {
  const selectedId = 'a';

  it('initial call sets initial state', async () => {
    const { result } = renderHook(() => useSingleSelection({ selectedId }));

    expect(result.current.selection).toStrictEqual(selectedId);
  });

  it('reacts on select', async () => {
    const { result } = renderHook(() => useSingleSelection({ selectedId }));

    act(() => result.current.api.set('c'));
    expect(result.current.selection).toStrictEqual('c');
  });

  it('is selected', async () => {
    const { result } = renderHook(() => useSingleSelection({ selectedId }));
    expect(result.current.api.isSelected('a')).toBe(true);
    expect(result.current.api.isSelected('d')).toBe(false);
  });
});
