import { createContext, useContext } from 'react';
import { SelectionApi } from './types';

/* istanbul ignore next */
const SelectionContext = createContext<SelectionApi>({
  isSelected: () => false,
  set: () => {},
});

const useSingleSelectionContext = (): SelectionApi => useContext(SelectionContext);

export { SelectionContext, useSingleSelectionContext };
