import { MutableRefObject, useEffect, useRef } from 'react';

type UseKeyStatusResponse = {
  [key: string]: boolean | undefined;
};

const keyStatus: UseKeyStatusResponse = {};

let registered = 0;

const createHandler =
  (status: boolean) =>
  (e: KeyboardEvent): void => {
    keyStatus[e.key] = status;
    keyStatus.Shift = e.shiftKey;
    keyStatus.Meta = e.metaKey;
    keyStatus.Control = e.ctrlKey;
    keyStatus.Alt = e.altKey;
  };

const keydownHandler = createHandler(true);
const keyupHandler = createHandler(false);

const removeHandler = () => {
  registered -= 1;

  if (registered <= 0) {
    document.body.removeEventListener('keydown', keydownHandler);
    document.body.removeEventListener('keyup', keyupHandler);
  }
};

const addHandler = (): (() => void) => {
  if (registered === 0) {
    document.body.addEventListener('keydown', keydownHandler);
    document.body.addEventListener('keyup', keyupHandler);
  }

  registered += 1;

  return removeHandler;
};

const useKeyStatus = (): MutableRefObject<UseKeyStatusResponse> => {
  useEffect(addHandler, []);

  return useRef<UseKeyStatusResponse>(keyStatus);
};

export { useKeyStatus, UseKeyStatusResponse };
