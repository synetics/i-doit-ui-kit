import { fireEvent } from '@testing-library/react';
import { renderHook } from '@testing-library/react-hooks';
import { useKeyStatus } from './use-key-status';

describe('useKeyboardIndex()', () => {
  it('reacts on key', () => {
    const { result } = renderHook(() => useKeyStatus());

    fireEvent.keyDown(document.body, { key: 'a', shiftKey: true });

    expect(result.current.current.a).toBe(true);
    expect(result.current.current.Shift).toBe(true);

    fireEvent.keyUp(document.body, { key: 'a', shiftKey: false });
    expect(result.current.current.a).toBe(false);
    expect(result.current.current.Shift).toBe(false);
  });
});
