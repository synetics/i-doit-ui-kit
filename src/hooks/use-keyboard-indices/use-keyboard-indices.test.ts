import { renderHook } from '@testing-library/react-hooks';
import user from '@testing-library/user-event';
import { useKeyboardIndices } from './use-keyboard-indices';

describe('useKeyboardIndices()', () => {
  it.each([
    ['{arrowdown}', [0], [1]],
    ['{arrowdown}', [2], [3]],
    ['{home}', [5], [0]],
    ['{end}', [5], [9]],
    ['{arrowdown}', [4, 0], [4, 1]],
    ['{arrowdown}', [4, 2], [4, 3]],
    ['{home}', [1, 5], [1, 0]],
    ['{end}', [1, 5], [1, 9]],
    ['{arrowup}', [1], [0]],
    ['{arrowup}', [3], [2]],
    ['{arrowright}', [3], [3, 0]],
    ['{arrowleft}', [3, 2], [3]],
  ])('calls set index on keydown %s', async (text: string, start: number[], expected: number[]) => {
    const setIndex = jest.fn();
    renderHook(() => useKeyboardIndices(start, setIndex, 10, true, document.body));

    await user.type(document.body, text);

    expect(setIndex).toHaveBeenCalledTimes(1);
    expect(setIndex).toHaveBeenNthCalledWith(1, expected);
  });
});
