import { useMemo } from 'react';
import { useEventListener } from '../use-event-listener';
import { useKeyboardIndex } from '../use-keyboard-index';
import { handleKey, ObjectEventsType } from '../../utils/handle-key';

const useKeyboardIndices = (
  indices: number[],
  setIndices: (indices: number[]) => void,
  count: number,
  canIncrease: boolean,
  trigger: HTMLElement,
): void => {
  const current = useMemo(() => indices[indices.length - 1] || 0, [indices]);
  const prefix = useMemo(() => indices.slice(0, -1), [indices]);
  const handlers: ObjectEventsType<HTMLElement> = {};

  if (indices.length > 1) {
    handlers.ArrowLeft = () => setIndices(prefix);
  }

  if (canIncrease) {
    handlers.ArrowRight = () => setIndices([...indices, 0]);
  }

  useEventListener('keydown', handleKey(handlers), trigger);
  useKeyboardIndex(current, (i) => setIndices([...prefix, i]), count, trigger, { cyclic: false, horizontal: false });
};

export { useKeyboardIndices };
