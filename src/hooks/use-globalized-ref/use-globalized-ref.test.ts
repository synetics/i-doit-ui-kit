import { createRef } from 'react';
import { act, renderHook } from '@testing-library/react-hooks';
import { useGlobalizedRef } from './use-globalized-ref';

describe('useGlobalizedRef()', () => {
  it('works without crashing', () => {
    const ref = createRef<boolean>();
    const { result } = renderHook(() => useGlobalizedRef<boolean>(ref));
    const { localRef, setRefsCallback } = result.current;

    expect(localRef.current).toBeNull();
    expect(setRefsCallback).toEqual(expect.any(Function));
  });

  it('sets and exposes an element reference correctly', () => {
    const inputMock = document.createElement('input');
    const ref = createRef<HTMLInputElement>();
    const { result } = renderHook(() => useGlobalizedRef<HTMLInputElement>(ref));
    const { localRef, setRefsCallback } = result.current;

    act(() => {
      setRefsCallback(inputMock);
    });

    expect(localRef.current).toEqual(inputMock);
    expect(localRef.current).toEqual(ref.current);
  });
});
