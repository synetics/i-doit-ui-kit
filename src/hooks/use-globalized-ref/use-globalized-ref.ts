import { ForwardedRef, MutableRefObject, RefCallback, useCallback, useRef } from 'react';

type UseGlobalizedRefReturn<T> = {
  localRef: MutableRefObject<T | null>;
  setRefsCallback: RefCallback<T>;
};

export const useGlobalizedRef = <T>(ref: ForwardedRef<T>): UseGlobalizedRefReturn<T> => {
  const localRef: MutableRefObject<T | null> = useRef<T>(null);

  const setRefsCallback = useCallback(
    (v: T) => {
      localRef.current = v;

      if (ref) {
        if (typeof ref === 'function') {
          ref(v);
        } else {
          // eslint-disable-next-line no-param-reassign
          ref.current = v;
        }
      }
    },
    [localRef, ref],
  );

  return {
    localRef,
    setRefsCallback,
  };
};
