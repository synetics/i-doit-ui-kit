import React, { useEffect, useRef, useState } from 'react';

const copyToClipboard = (text: string) => {
  if (navigator.clipboard) {
    return navigator.clipboard.writeText(text);
  }

  const span = document.createElement('span');
  span.textContent = text;
  span.style.whiteSpace = 'pre';
  document.body.appendChild(span);

  const selection = window.getSelection();
  if (!selection) {
    return Promise.reject();
  }
  const range = document.createRange();
  selection.removeAllRanges();
  range.selectNode(span);
  selection.addRange(range);

  try {
    document.execCommand('copy');
  } catch (err) {
    return Promise.resolve();
  }

  selection.removeAllRanges();
  document.body.removeChild(span);

  return Promise.resolve();
};

type Clipboard = Readonly<{
  copied: boolean;
  reset: () => void;
  copy: () => Promise<void>;
}>;

export const useClipboard = (text: string): Readonly<Clipboard> => {
  const [copied, setCopied] = useState<boolean>(false);
  const reset = useRef(() => setCopied(false));

  useEffect(() => reset.current, [text]);

  return {
    copied,
    copy: React.useCallback(
      () =>
        copyToClipboard(text)
          .then(() => {
            setCopied(true);
          })
          .catch(() => setCopied((val) => val)),
      [text],
    ),
    reset: reset.current,
  } as const;
};
