import { act, renderHook } from '@testing-library/react-hooks';
import { useClipboard } from './use-clipboard';

describe('useClipboard()', () => {
  it('uses navigator.clipboard', async () => {
    Object.assign(navigator, {
      clipboard: {
        writeText: () => Promise.resolve(),
      },
    });

    const { result } = renderHook(() => useClipboard('copy me'));

    expect(result.current.copied).toBe(false);

    await act(() => result.current.copy());
    expect(result.current.copied).toBe(true);

    await act(async () => result.current.reset());
    expect(result.current.copied).toBe(false);
  });

  it('uses document.execCommand', async () => {
    Object.assign(document, {
      execCommand: () => {},
    });

    Object.assign(navigator, {
      clipboard: undefined,
    });

    const { result } = renderHook(() => useClipboard('copy me'));

    expect(result.current.copied).toBe(false);

    await act(() => result.current.copy());
    expect(result.current.copied).toBe(true);

    await act(async () => result.current.reset());
    expect(result.current.copied).toBe(false);
  });

  it('does not copy if the range of text is not selected by the user', async () => {
    Object.assign(window, {
      getSelection: () => undefined,
    });

    const { result } = renderHook(() => useClipboard('copy me'));

    expect(result.current.copied).toBe(false);
    await act(() => result.current.copy());
    expect(result.current.copied).toBe(false);
  });

  it('does not copy current selection or the given range if is not valid', async () => {
    Object.assign(document, {
      execCommand: () => {
        throw new Error('current selection or the given range is not valid');
      },
    });

    Object.assign(navigator, {
      clipboard: undefined,
    });

    const { result } = renderHook(() => useClipboard('copy me'));

    expect(result.current.copied).toBe(false);
    await act(() => result.current.copy());
    expect(result.current.copied).toBe(false);
  });
});
