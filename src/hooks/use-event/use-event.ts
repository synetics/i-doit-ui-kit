import { useLayoutEffect, useMemo, useRef } from 'react';
import { GenericFunc, Parameters, ReturnType } from '../../types/function';

/**
 * Creates a stable function that always has the latest version
 */
export const useEvent = <Fn extends CallableFunction>(handler: Fn): GenericFunc<Fn> => {
  const handlerRef = useRef<Fn>(handler);

  useLayoutEffect(() => {
    handlerRef.current = handler;
  });

  return useMemo(
    () =>
      (...args: Parameters<Fn>): ReturnType<Fn> =>
        handlerRef.current(...args) as ReturnType<Fn>,
    [],
  );
};
