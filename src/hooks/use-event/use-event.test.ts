import { renderHook } from '@testing-library/react-hooks';
import { useEvent } from './use-event';

describe('use event', () => {
  it('has the stable identity', () => {
    const hook = renderHook((fn: () => string) => useEvent(fn), {
      initialProps: () => 'test',
    });

    const initial = hook.result.current;
    hook.rerender(() => 'updated');

    expect(hook.result.current).toBe(initial);
  });

  it('updates internal function', () => {
    const hook = renderHook((fn: () => string) => useEvent(fn), {
      initialProps: () => 'test',
    });

    expect(hook.result.current()).toBe('test');

    hook.rerender(() => 'updated');

    expect(hook.result.current()).toBe('updated');
  });
});
