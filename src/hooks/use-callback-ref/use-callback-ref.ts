import { useCallback, useState } from 'react';

const useCallbackRef = <T>(): Readonly<[T | null, (element: T) => void]> => {
  const [referenceElement, setReferenceElement] = useState<T | null>(null);

  const setRefCallback = useCallback((element: T) => {
    setReferenceElement(element);
  }, []);

  return [referenceElement, setRefCallback] as const;
};

export { useCallbackRef };
