import { act, renderHook } from '@testing-library/react-hooks';
import { useCallbackRef } from './use-callback-ref';

describe('useCallbackRef()', () => {
  it('works without crashing', () => {
    const { result } = renderHook(() => useCallbackRef());
    const [elementRef, setElementRef] = result.current;

    expect(elementRef).toBeNull();
    expect(setElementRef).toEqual(expect.any(Function));
  });

  it('sets and exposes an element reference', () => {
    const divMock = document.createElement('div');
    const { result } = renderHook(() => useCallbackRef<HTMLDivElement>());
    const [, setElementRef] = result.current;

    act(() => {
      setElementRef(divMock);
    });

    expect(result.current[0]).toEqual(divMock);
  });
});
