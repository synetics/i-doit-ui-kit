import { renderHook } from '@testing-library/react-hooks';
import React, { MutableRefObject } from 'react';
import { useFocus } from './useFocus';

describe('useFocus()', () => {
  it('use focus with initial autofocus', () => {
    const focus = jest.fn();
    const element = { focus } as unknown as HTMLButtonElement;
    const ref = React.createRef<HTMLButtonElement>() as MutableRefObject<HTMLButtonElement>;
    ref.current = element;

    renderHook(() => useFocus(true, ref));

    expect(focus).toHaveBeenCalled();
  });
});
