import { MutableRefObject, useEffect, useRef } from 'react';

/**
 * Sets focus on the HTML Element and returns its ref
 * @param autoFocus
 * @param lazyRef
 * @returns {MutableRefObject<HTMLElement | null>}
 */
export const useFocus = <A extends HTMLElement>(
  autoFocus: boolean,
  lazyRef?: MutableRefObject<A>,
): MutableRefObject<A | undefined> => {
  const ref = useRef<A | undefined>(undefined);

  useEffect(() => {
    if (lazyRef) {
      ref.current = lazyRef?.current;
    }

    if (autoFocus && ref.current) {
      ref.current?.focus();
    }
  }, [autoFocus, lazyRef]);

  return ref;
};
