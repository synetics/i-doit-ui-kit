import { useEventListener } from '../use-event-listener';
import { useToggle } from '../use-toggle';

export const getParent = (ref: Element, parents: Element[]): Element[] => {
  if (ref.parentElement === null) {
    return parents;
  }

  return getParent(ref.parentElement, [ref, ...parents]);
};

export const useHasFocus = (ref: Element | null): boolean => {
  if (!document.activeElement || ref === null) {
    return false;
  }
  const parents = getParent(document.activeElement, []);
  return parents.includes(ref);
};

export const useFocusWithin = (ref: Element | null): boolean => {
  const [, updateApi] = useToggle(false);
  useEventListener('focusin', updateApi.toggle, ref);
  useEventListener('focusout', updateApi.toggle, ref);

  return useHasFocus(ref);
};
