import { act, renderHook } from '@testing-library/react-hooks';
import { useControlledState } from './use-controlled-state';

describe('useControlledState()', () => {
  it('controls state inside and gives the api to change it', () => {
    const { result } = renderHook(() => useControlledState<string>('value'));
    const [value, setValue] = result.current;
    expect(value).toBe('value');

    act(() => {
      setValue('nextValue');
    });

    const [nextValue] = result.current;
    expect(nextValue).toBe('nextValue');
  });

  it('delegates the ability to control state via setter function', () => {
    const setValueMock = jest.fn();
    const { result } = renderHook(() => useControlledState<string>('value', setValueMock));
    const [value, setValue] = result.current;
    expect(value).toBe('value');

    act(() => {
      setValue('nextValue');
    });

    expect(setValueMock).toHaveBeenCalledWith('nextValue');
    const [nextValue] = result.current;
    expect(nextValue).toBe('value');
  });
});
