import { useEffect, useState } from 'react';

/**
 * Allows to manage the component state internally or delegate control over it
 * @see {@link https://en.reactjs.org/docs/forms.html#controlled-components}
 *
 * @param value
 * @param setValue
 * @returns {[*, function]}
 */
export const useControlledState = <T>(value: T, setValue?: (value: T) => void): Readonly<[T, (value: T) => void]> => {
  const [uncontrolledValue, setUncontrolledValue] = useState<T>(value);

  useEffect(() => {
    setUncontrolledValue(value);
  }, [value]);

  if (typeof setValue === 'function') {
    return [value, setValue];
  }

  return [uncontrolledValue, setUncontrolledValue];
};
