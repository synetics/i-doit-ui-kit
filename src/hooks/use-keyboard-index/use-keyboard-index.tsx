import { FocusEvent } from 'react';
import { handleKey } from '../../utils/helpers';
import { useEventListener } from '../use-event-listener';

type Index = number;

/**
 * Hook to handle the keyboard index
 *
 * @param {number} currentIndex
 * @param {function} setIndex
 * @param {number} count
 * @param {Element} trigger
 * @param {{cyclic: boolean, horizontal: boolean}} - cyclic - allow to cycle (lower 0 moves to the first element), horizontal - direction
 * @returns {void}
 */
export const useKeyboardIndex = (
  currentIndex: Index,
  setIndex: (index: Index, event: FocusEvent) => void,
  count: number,
  trigger: HTMLElement,
  { cyclic = false, horizontal = false }: { cyclic?: boolean; horizontal?: boolean },
): void => {
  const focus = (index: Index) => (e: FocusEvent) => {
    const isLessThanZero = index < 0;
    const isBiggerThanCount = index > count - 1;

    const calculateIndex = (previousIndex: number) => {
      if (isLessThanZero) {
        return cyclic ? count - 1 : 0;
      }

      if (isBiggerThanCount) {
        return cyclic ? 0 : count - 1;
      }

      return previousIndex;
    };

    setIndex(calculateIndex(index), e);
  };

  const handlers = {
    [horizontal ? 'ArrowLeft' : 'ArrowUp']: focus(currentIndex - 1),
    [horizontal ? 'ArrowRight' : 'ArrowDown']: focus(currentIndex + 1),
    Home: focus(0),
    End: focus(count - 1),
    PageUp: focus(currentIndex - 5),
    PageDown: focus(currentIndex + 5),
  };

  useEventListener('keydown', handleKey(handlers), trigger);
};
