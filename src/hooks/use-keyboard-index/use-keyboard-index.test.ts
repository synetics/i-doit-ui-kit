import { renderHook } from '@testing-library/react-hooks';
import user from '@testing-library/user-event';
import { useKeyboardIndex } from './use-keyboard-index';

describe('useKeyboardIndex()', () => {
  it.each([
    ['{arrowdown}', 0, 1, false],
    ['{arrowdown}', 2, 3, false],
    ['{arrowdown}', 9, 9, false],
    ['{arrowdown}', 10, 9, false],
    ['{arrowup}', 0, 0, false],
    ['{arrowup}', 1, 0, false],
    ['{arrowup}', 3, 2, false],
    ['{arrowdown}', 9, 0, true],
    ['{arrowdown}', 5, 6, true],
    ['{arrowup}', 0, 9, true],
    ['{arrowup}', 5, 4, true],
    ['{home}', 5, 0, false],
    ['{end}', 5, 9, false],
  ])('calls set index on keydown %s', async (text: string, start: number, expected: number, cyclic: boolean) => {
    const setIndex = jest.fn();
    renderHook(() => useKeyboardIndex(start, setIndex, 10, document.body, { cyclic }));

    await user.type(document.body, text);

    expect(setIndex).toHaveBeenCalledTimes(1);
    expect(setIndex).toHaveBeenNthCalledWith(1, expected, expect.any(KeyboardEvent));
  });

  it.each([
    ['{arrowright}', 0, 1, false],
    ['{arrowright}', 2, 3, false],
    ['{arrowright}', 9, 9, false],
    ['{arrowright}', 10, 9, false],
    ['{arrowleft}', 0, 0, false],
    ['{arrowleft}', 1, 0, false],
    ['{arrowleft}', 3, 2, false],
    ['{arrowright}', 9, 0, true],
    ['{arrowright}', 5, 6, true],
    ['{arrowleft}', 0, 9, true],
    ['{arrowleft}', 5, 4, true],
    ['{home}', 5, 0, false],
    ['{end}', 5, 9, false],
  ])(
    'calls set index on keydown %s horizontal',
    async (text: string, start: number, expected: number, cyclic: boolean) => {
      const setIndex = jest.fn();
      renderHook(() => useKeyboardIndex(start, setIndex, 10, document.body, { cyclic, horizontal: true }));

      await user.type(document.body, text);

      expect(setIndex).toHaveBeenCalledTimes(1);
      expect(setIndex).toHaveBeenNthCalledWith(1, expected, expect.any(KeyboardEvent));
    },
  );
});
