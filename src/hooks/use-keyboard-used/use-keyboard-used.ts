import { useWindowEvent } from '@mantine/hooks';
import { useToggle } from '../use-toggle';

export const useKeyboardUsed = (): boolean => {
  const [keyboard, keyboardApi] = useToggle(false);
  useWindowEvent('keydown', keyboardApi.enable);
  useWindowEvent('mousemove', keyboardApi.disable);

  return keyboard;
};
