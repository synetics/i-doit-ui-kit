import { useMemo } from 'react';
import { handleKey } from '../../utils';
import { useControlledState } from '../use-controlled-state';
import { useEventListener } from '../use-event-listener';
import { useKeyboardIndex } from '../use-keyboard-index';

export const useTagItemsFocus = (
  ref: HTMLElement | null,
  count: number,
  onRemove: (index: number) => void,
  controlledIndex?: number,
  onIndexChanged?: (index: number) => void,
): number => {
  const [index, setIndex] = useControlledState(controlledIndex || 0, onIndexChanged);
  const focusIndex = useMemo(() => Math.max(0, Math.min(index, count - 1)), [index, count]);
  const handleIndexChange = (newIndex: number) => setIndex(newIndex);
  useKeyboardIndex(focusIndex, handleIndexChange, count, ref || document.body, {
    horizontal: true,
  });

  useEventListener(
    'keydown',
    handleKey({
      Backspace: (e) => {
        e.stopPropagation();
        onRemove(index);
        setIndex(Math.max(index - 1, 0));
      },
      Delete: () => onRemove(index),
      Tab: () => false,
    }),
    ref,
  );

  return focusIndex;
};
