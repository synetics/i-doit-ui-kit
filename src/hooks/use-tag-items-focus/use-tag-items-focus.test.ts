import { act, renderHook } from '@testing-library/react-hooks';
import user from '@testing-library/user-event';
import { useTagItemsFocus } from './use-tag-items-focus';

const renderFocus = (count: number, index: number) => {
  const actions = {
    onRemove: jest.fn(),
    onIndexChanged: jest.fn(),
  };
  const element = document.body;

  const result = renderHook(
    ({ controlledIndex, count: passedCount }) =>
      useTagItemsFocus(element, passedCount, actions.onRemove, controlledIndex, actions.onIndexChanged),
    { initialProps: { count, controlledIndex: index } },
  );
  return {
    ...result,
    actions,
    element,
  };
};

describe('useTagItemsFocus', () => {
  it('has correct initial focus index', () => {
    const { result } = renderFocus(10, 2);
    expect(result.current).toBe(2);
  });

  it('reacts on backspace', async () => {
    const {
      actions: { onRemove, onIndexChanged },
      element,
    } = renderFocus(10, 2);
    await user.type(element, '{backspace}');
    expect(onRemove).toHaveBeenCalledWith(2);
    expect(onIndexChanged).toHaveBeenCalledWith(1);
  });

  it('does not react on tab', async () => {
    const { element } = renderFocus(10, 2);
    await user.type(element, '{tab}');
  });

  it('reacts on delete', async () => {
    const {
      actions: { onRemove },
      element,
    } = renderFocus(10, 2);
    await user.type(element, '{delete}');
    expect(onRemove).toHaveBeenCalledWith(2);
  });

  it('reacts on arrows', async () => {
    const {
      actions: { onIndexChanged },
      element,
    } = renderFocus(10, 2);
    await user.type(element, '{arrowleft}');
    expect(onIndexChanged).toHaveBeenCalledWith(1);
    await user.type(element, '{arrowright}{arrowright}');
    expect(onIndexChanged).toHaveBeenCalledWith(3);
  });
  it('should return false when Tab key is pressed', () => {
    const ref = document.createElement('div');
    const count = 1;
    const onRemove = jest.fn();

    const { result } = renderHook(() => useTagItemsFocus(ref, count, onRemove));

    act(() => {
      const event = new KeyboardEvent('keydown', { key: 'Tab' });
      ref.dispatchEvent(event);
    });

    expect(result.current).toBe(0);
  });
});
