import { useEffect, useState } from 'react';

export const useHover = (ref: Element | null) => {
  const [isHovered, setIsHovered] = useState(false);

  const handleMouseEnter = () => {
    setIsHovered(true);
  };

  const handleMouseLeave = () => {
    setIsHovered(false);
  };
  useEffect(() => {
    if (ref) {
      ref.addEventListener('mouseenter', handleMouseEnter);
      ref.addEventListener('mouseleave', handleMouseLeave);
    }

    return () => {
      if (ref) {
        ref.removeEventListener('mouseenter', handleMouseEnter);
        ref.removeEventListener('mouseleave', handleMouseLeave);
      }
    };
  }, [ref]);

  return isHovered;
};
