import { fireEvent, render, screen } from '@testing-library/react';
import { act, renderHook } from '@testing-library/react-hooks';
import React from 'react';
import { useHover } from './use-hover';

const buttonContent = 'testButton';

describe('useHover()', () => {
  it('add event listener to element and check hover state', () => {
    const fakeRef = React.createRef<HTMLButtonElement>();
    render(
      <button data-testid="test-hover" ref={fakeRef}>
        {buttonContent}
      </button>,
    );

    const { result } = renderHook(() => useHover(fakeRef.current));

    const hoverElem = screen.getByTestId('test-hover');

    act(() => {
      fireEvent.mouseEnter(hoverElem);
    });
    expect(result.current).toBe(true);

    act(() => {
      fireEvent.mouseLeave(hoverElem);
    });
    expect(result.current).toBe(false);
  });
});
