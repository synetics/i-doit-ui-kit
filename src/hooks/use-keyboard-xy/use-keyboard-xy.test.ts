import { renderHook } from '@testing-library/react-hooks';
import user from '@testing-library/user-event';
import { useKeyboardXY } from './use-keyboard-xy';

describe('use vertical navigation', () => {
  it.each([
    ['{arrowdown}', 0, 1],
    ['{arrowdown}', 2, 3],
    ['{arrowdown}', 9, 9],
    ['{arrowdown}', 10, 9],
    ['{arrowup}', 0, 0],
    ['{arrowup}', 1, 0],
    ['{arrowup}', 3, 2],
    ['{arrowup}', -1, 0],
  ])('calls setY on %s, change Y from %d to %d', async (text: string, start: number, expected: number) => {
    const setY = jest.fn();

    renderHook(() => useKeyboardXY(0, start, () => undefined, setY, 0, 10, document.body));

    await user.type(document.body, text);

    expect(setY).toHaveBeenCalledTimes(1);
    expect(setY).toHaveBeenNthCalledWith(1, expected);
  });

  it.each([
    ['{Control>}{home}{/Control}', 5, 0],
    ['{Control>}{end}{/Control}', 5, 9],
  ])('calls setY on %s, change Y from %d to %d', async (text: string, start: number, expected: number) => {
    const setY = jest.fn();

    renderHook(() => useKeyboardXY(0, start, () => undefined, setY, 0, 10, document.body));

    await user.type(document.body, text);

    expect(setY).toHaveBeenCalledTimes(1);
    expect(setY).toHaveBeenNthCalledWith(1, expected);
  });
});

describe('use horizontal navigation', () => {
  it.each([
    ['{arrowright}', 0, 1],
    ['{arrowright}', 2, 3],
    ['{arrowright}', 9, 9],
    ['{arrowright}', 10, 9],
    ['{arrowleft}', 0, 0],
    ['{arrowleft}', 1, 0],
    ['{arrowleft}', 3, 2],
    ['{arrowleft}', -1, 0],
  ])('calls setX on %s, change X from %d to %d', async (text: string, start: number, expected: number) => {
    const setX = jest.fn();

    renderHook(() => useKeyboardXY(start, 0, setX, () => undefined, 10, 0, document.body));

    await user.type(document.body, text);

    expect(setX).toHaveBeenCalledTimes(1);
    expect(setX).toHaveBeenNthCalledWith(1, expected);
  });

  it.each([
    ['{home}', 5, 0],
    ['{end}', 5, 9],
    ['{Control>}{home}{/Control}', 5, 0],
    ['{Control>}{end}{/Control}', 5, 9],
  ])('calls setX on %s, change X from %d to %d', async (text: string, start: number, expected: number) => {
    const setX = jest.fn();

    renderHook(() => useKeyboardXY(start, 0, setX, () => undefined, 10, 0, document.body));

    await user.type(document.body, text);

    expect(setX).toHaveBeenCalledTimes(1);
    expect(setX).toHaveBeenNthCalledWith(1, expected);
  });
});
