import { KeyboardEvent } from 'react';
import { handleKey } from '../../utils';
import { useEventListener } from '../use-event-listener';

type Callback = (x: number) => void;

/**
 * Hook to handle the keyboard indices for 2 dimensions
 *
 * @param {number} x
 * @param {number} y
 * @param {function} setX
 * @param {function} setY
 * @param {number} countX
 * @param {number} countY
 * @param {Element|null} trigger
 */
const useKeyboardXY = (
  x: number,
  y: number,
  setX: Callback,
  setY: Callback,
  countX: number,
  countY: number,
  trigger: Element | null,
): void => {
  const focus = (index: number, set: Callback, count: number) => () => {
    set(Math.min(count - 1, Math.max(0, index)));
  };
  const handlers = {
    ArrowLeft: focus(x - 1, setX, countX),
    ArrowRight: focus(x + 1, setX, countX),
    ArrowUp: focus(y - 1, setY, countY),
    ArrowDown: focus(y + 1, setY, countY),
    Home: (e: KeyboardEvent) => {
      if (e.ctrlKey) {
        setX(0);
        setY(0);
        return;
      }

      focus(0, setX, countX)();
    },
    End: (e: KeyboardEvent) => {
      if (e.ctrlKey) {
        setX(countX - 1);
        setY(countY - 1);
        return;
      }

      focus(countX - 1, setX, countX)();
    },
    PageUp: focus(0, setY, countY),
    PageDown: focus(countY - 1, setY, countY),
  };

  useEventListener('keydown', handleKey(handlers), trigger);
};

export { useKeyboardXY };
