import { act, renderHook } from '@testing-library/react-hooks';
import { useToggle } from './use-toggle';

describe('useToggle', () => {
  it.each([true, false])('has the correct initial state', (initialState: boolean) => {
    const { result } = renderHook(() => useToggle(initialState));
    expect(result.current[0]).toBe(initialState);
  });

  it('toggles the state', () => {
    const { result } = renderHook(() => useToggle());
    const { toggle } = result.current[1];
    expect(result.current[0]).toBe(false);
    act(() => toggle());
    expect(result.current[0]).toBe(true);
    act(() => toggle());
    expect(result.current[0]).toBe(false);
  });

  it('enables the state', () => {
    const { result } = renderHook(() => useToggle());
    expect(result.current[0]).toBe(false);
    const { enable } = result.current[1];
    act(() => enable());
    expect(result.current[0]).toBe(true);
    act(() => enable());
    expect(result.current[0]).toBe(true);
  });

  it('disables the state', () => {
    const { result } = renderHook(() => useToggle(true));
    expect(result.current[0]).toBe(true);
    const { disable } = result.current[1];
    act(() => disable());
    expect(result.current[0]).toBe(false);
    act(() => disable());
    expect(result.current[0]).toBe(false);
  });
});
