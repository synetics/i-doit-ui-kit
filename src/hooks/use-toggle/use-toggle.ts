import { useMemo, useState } from 'react';

export type ToggleActionsType = {
  enable: () => void;
  disable: () => void;
  toggle: () => void;
};

export type UseToggleResultType = [boolean, ToggleActionsType];

const useToggle = (initialState = false): UseToggleResultType => {
  const [state, setState] = useState(initialState);
  const actions = useMemo(
    () => ({
      enable: () => setState(true),
      disable: () => setState(false),
      toggle: () => setState((current) => !current),
    }),
    [],
  );

  return [state, actions];
};

export { useToggle };
