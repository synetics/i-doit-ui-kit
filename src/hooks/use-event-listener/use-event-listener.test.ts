import { renderHook } from '@testing-library/react-hooks';
import user from '@testing-library/user-event';
import { useEventListener } from './use-event-listener';

describe('useEventListener()', () => {
  it('add event listener to element and calls it', async () => {
    const listenFunc = jest.fn();
    const customEvent = 'click';
    renderHook(() => useEventListener<void>(customEvent, listenFunc, document.body));

    await user.click(document.body);

    expect(listenFunc).toHaveBeenCalledTimes(1);
  });

  it('add nothing if no event', async () => {
    const listenFunc = jest.fn();
    const wrongEvent = 'onclick';
    renderHook(() => useEventListener<void>(wrongEvent, listenFunc, document.body));

    await user.click(document.body);

    expect(listenFunc).toHaveBeenCalledTimes(0);
  });
});
