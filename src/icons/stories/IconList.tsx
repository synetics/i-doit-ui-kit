import React from 'react';
import { Icon } from '../../components';
import styles from './IconList.module.scss';

export default ({ icons }: { icons: Record<string, string> }): JSX.Element => (
  <ul className={styles.iconList}>
    {Object.entries(icons).map(([key, src]) => (
      <li key={key} className={styles.icon}>
        <Icon src={src} width={40} height={40} />
        <div className={styles.iconName}>{key.replace(/_/g, ' ')}</div>
      </li>
    ))}
  </ul>
);
