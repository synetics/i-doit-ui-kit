export { default as location } from '../../assets/icons/view/location.svg';
export { default as locations } from '../../assets/icons/view/locations.svg';
export { default as object } from '../../assets/icons/view/object.svg';
export { default as objects } from '../../assets/icons/view/objects.svg';
export { default as sidepanel } from '../../assets/icons/view/sidepanel.svg';
export { default as table } from '../../assets/icons/view/table.svg';
export { default as add_columns } from '../../assets/icons/view/add-columns.svg';
export { default as widget } from '../../assets/icons/view/widget.svg';
export { default as city } from '../../assets/icons/view/city.svg';
export { default as scrollable_header } from '../../assets/icons/view/scrollable_header.svg';
export { default as straighten } from '../../assets/icons/view/straighten.svg';
export { default as title } from '../../assets/icons/view/title.svg';
