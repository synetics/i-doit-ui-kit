import { useMemo, useState } from 'react';
import classnames from 'classnames';
import { ItemType, SingleSelect } from '../../components/Form/Control';
import { FormGroup, Label } from '../../components/Form/Infrastructure';
import { uniqueId } from '../../utils';

type InnerItemType<T> = ItemType & { inner: T };

type UseSelectInValuesConfig<T> = {
  label: string;
  defaultValue?: T | null;
  values: Record<string, T>;
  className?: string;
};
export const useSelectInValues = <T,>({ label, values, defaultValue, className }: UseSelectInValuesConfig<T>) => {
  const items = useMemo(
    () =>
      Object.keys(values).reduce(
        (r, key) => [
          ...r,
          {
            value: key,
            label: key,
            inner: values[key],
          },
        ],
        [] as InnerItemType<T>[],
      ),
    [values],
  );
  const [value, setValue] = useState<T | null>(() => defaultValue ?? null);
  const [id] = useState(() => uniqueId('select'));
  const selected = items.find((a) => a.inner === value) ?? null;
  const component = (
    <FormGroup className={classnames(className, 'mb-2 w-25')}>
      <Label id={id}>{label}</Label>
      <SingleSelect
        items={items}
        value={selected}
        onChange={(v) => {
          const next = v as InnerItemType<T> | null;
          setValue(next?.inner ?? null);
        }}
      />
    </FormGroup>
  );

  return [value, component] as const;
};
