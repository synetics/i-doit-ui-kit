import colors from './_colors.module.scss';

export const Brand = {
  Regular: colors.uiKitBrandRegular,
  Hover: colors.uiKitBrandHover,
  Press: colors.uiKitBrandPress,
};

export const Interaction = {
  Regular: colors.uiKitInteractionRegular,
  Hover: colors.uiKitInteractionHover,
  Press: colors.uiKitInteractionPress,
  Selected: colors.uiKitInteractionSelected,
};

export const Neutral = {
  Neutral_100: colors.uiKitNeutral_100,
  Neutral_200: colors.uiKitNeutral_200,
  Neutral_300: colors.uiKitNeutral_300,
  Neutral_500: colors.uiKitNeutral_500,
  Neutral_700: colors.uiKitNeutral_700,
  Neutral_900: colors.uiKitNeutral_900,
};

export const WHITE = colors.uiKitWhite;
export const BLACK = colors.uiKitBlack;

export const Error = {
  Regular: colors.uiKitErrorRegular,
  Hover: colors.uiKitErrorHover,
  Press: colors.uiKitErrorPress,
};

export const Focus = {
  Regular: colors.uiKitFocusRegular,
  Highlight: colors.uiKitFocusHighlight,
};

export const Accent = {
  Error: colors.uiKitErrorRegular,
  Success: colors.uiKitSuccessRegular,
  Warning: colors.uiKitWarningRegular,
  System: colors.uiKitNeutral_900,
};

export const StrongBlue = {
  uiKitColorStrongBlue50: colors.uiKitColorStrongBlue50,
  uiKitColorStrongBlue75: colors.uiKitColorStrongBlue75,
  uiKitColorStrongBlue100: colors.uiKitColorStrongBlue100,
  uiKitColorStrongBlue200: colors.uiKitColorStrongBlue200,
  uiKitColorStrongBlue300: colors.uiKitColorStrongBlue300,
  uiKitColorStrongBlue400: colors.uiKitColorStrongBlue400,
  uiKitColorStrongBlue500: colors.uiKitColorStrongBlue500,
};

export const Red = {
  uiKitColorRed50: colors.uiKitColorRed50,
  uiKitColorRed75: colors.uiKitColorRed75,
  uiKitColorRed100: colors.uiKitColorRed100,
  uiKitColorRed200: colors.uiKitColorRed200,
  uiKitColorRed300: colors.uiKitColorRed300,
  uiKitColorRed400: colors.uiKitColorRed400,
  uiKitColorRed500: colors.uiKitColorRed500,
};

export const Yellow = {
  uiKitColorYellow50: colors.uiKitColorYellow50,
  uiKitColorYellow75: colors.uiKitColorYellow75,
  uiKitColorYellow100: colors.uiKitColorYellow100,
  uiKitColorYellow200: colors.uiKitColorYellow200,
  uiKitColorYellow300: colors.uiKitColorYellow300,
  uiKitColorYellow400: colors.uiKitColorYellow400,
  uiKitColorYellow500: colors.uiKitColorYellow500,
};

export const Green = {
  uiKitColorGreen50: colors.uiKitColorGreen50,
  uiKitColorGreen75: colors.uiKitColorGreen75,
  uiKitColorGreen100: colors.uiKitColorGreen100,
  uiKitColorGreen200: colors.uiKitColorGreen200,
  uiKitColorGreen300: colors.uiKitColorGreen300,
  uiKitColorGreen400: colors.uiKitColorGreen400,
  uiKitColorGreen500: colors.uiKitColorGreen500,
};

export const BrightBlue = {
  uiKitColorBrightBlue50: colors.uiKitColorBrightBlue50,
  uiKitColorBrightBlue75: colors.uiKitColorBrightBlue75,
  uiKitColorBrightBlue100: colors.uiKitColorBrightBlue100,
  uiKitColorBrightBlue200: colors.uiKitColorBrightBlue200,
  uiKitColorBrightBlue300: colors.uiKitColorBrightBlue300,
  uiKitColorBrightBlue400: colors.uiKitColorBrightBlue400,
  uiKitColorBrightBlue500: colors.uiKitColorBrightBlue500,
};

export const Violet = {
  uiKitColorViolet50: colors.uiKitColorViolet50,
  uiKitColorViolet75: colors.uiKitColorViolet75,
  uiKitColorViolet100: colors.uiKitColorViolet100,
  uiKitColorViolet200: colors.uiKitColorViolet200,
  uiKitColorViolet300: colors.uiKitColorViolet300,
  uiKitColorViolet400: colors.uiKitColorViolet400,
  uiKitColorViolet500: colors.uiKitColorViolet500,
};

export const Gold = {
  uiKitColorGold50: colors.uiKitColorGold50,
  uiKitColorGold75: colors.uiKitColorGold75,
  uiKitColorGold100: colors.uiKitColorGold100,
  uiKitColorGold200: colors.uiKitColorGold200,
  uiKitColorGold300: colors.uiKitColorGold300,
  uiKitColorGold400: colors.uiKitColorGold400,
  uiKitColorGold500: colors.uiKitColorGold500,
};

export const LightBlue = {
  uiKitColorLightBlue50: colors.uiKitColorLightBlue50,
  uiKitColorLightBlue75: colors.uiKitColorLightBlue75,
  uiKitColorLightBlue100: colors.uiKitColorLightBlue100,
  uiKitColorLightBlue200: colors.uiKitColorLightBlue200,
  uiKitColorLightBlue300: colors.uiKitColorLightBlue300,
  uiKitColorLightBlue400: colors.uiKitColorLightBlue400,
  uiKitColorLightBlue500: colors.uiKitColorLightBlue500,
};

export const DarkBlue = {
  uiKitColorDarkBlue50: colors.uiKitColorDarkBlue50,
  uiKitColorDarkBlue75: colors.uiKitColorDarkBlue75,
  uiKitColorDarkBlue100: colors.uiKitColorDarkBlue100,
  uiKitColorDarkBlue200: colors.uiKitColorDarkBlue200,
  uiKitColorDarkBlue300: colors.uiKitColorDarkBlue300,
  uiKitColorDarkBlue400: colors.uiKitColorDarkBlue400,
  uiKitColorDarkBlue500: colors.uiKitColorDarkBlue500,
};
