import { IdType, UseResizeColumnsColumnProps, UseSortByColumnProps, UseSortByHooks, UseSortByState } from 'react-table';

declare module 'react-table' {
  // eslint-disable-next-line no-unused-vars
  export interface TableOptions<D extends Record<string, unknown>> extends Record<string, any> {}

  export type Hooks<D extends Record<string, unknown> = Record<string, unknown>> = UseSortByHooks<D>;

  export interface TableState<D extends Record<string, unknown> = Record<string, unknown>> extends UseSortByState<D> {}

  export interface ColumnInstance<D extends Record<string, unknown> = Record<string, unknown>>
    extends UseResizeColumnsColumnProps<D>,
      UseSortByColumnProps<D> {}

  export interface UseTableColumnOptions<D extends Record<string, unknown> = Record<string, unknown>>
    extends UseTableColumnOptions<D> {
    disableSortBy?: boolean;
    className?: string;
  }

  // eslint-disable-next-line no-unused-vars
  export interface HeaderGroup<D> {
    totalVisibleHeaderCount: number;
    unfocusable?: boolean;
    resizable?: boolean;
    headerClassName?: string;
    accessor?: IdType<D>;
    overflow?: boolean;
  }

  // eslint-disable-next-line no-unused-vars
  export interface Cell<D, V> {
    unfocusable: boolean;
    selectable: boolean;
  }
}
