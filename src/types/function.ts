/**
 * Abstract function
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type AbstractFunction = (...args: any[]) => any;
export type Parameters<T> = T extends (...args: infer Param) => unknown ? Param : never;
export type ReturnType<T> = T extends (...args: Parameters<T>) => infer Return ? Return : void;
export type GenericFunc<T extends CallableFunction> = (...args: Parameters<T>) => ReturnType<T>;
