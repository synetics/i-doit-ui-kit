import { ElementType } from 'react';

export type GetComponentProps<T> = T extends ElementType<infer A> ? A : never;
