export const KeyboardKeys = Object.freeze({
  ArrowDown: 'ArrowDown',
  ArrowUp: 'ArrowUp',
  Enter: 'Enter',
  Escape: 'Escape',
  Tab: 'Tab',
});
