export const checkIsFocusWithin = (element: Element | null, parent: Element | null | undefined): boolean => {
  if (!parent || !element) {
    return false;
  }
  if (element === parent) {
    return true;
  }
  return checkIsFocusWithin(element.parentElement, parent);
};
