export const toHexCode = (value: string): string => {
  const hexCode = /[a-fA-F0-9]/g;
  const match = value.match(hexCode);

  return match ? match.join('').toUpperCase() : '';
};
