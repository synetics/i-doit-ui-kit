export const makeDndScroll = (scrollPos: number, scrollChange: number, container?: HTMLElement | null): void => {
  const windowHeight = window.innerHeight;
  const left = 0;
  const behavior = 'smooth';
  const scrollTop = container?.scrollTop || 0;
  const scrollCounter = scrollPos < 0 ? -scrollChange : scrollChange;
  if (scrollPos + 150 > windowHeight || scrollPos < 0 || (scrollPos < 50 && scrollPos !== 0)) {
    container?.scrollTo({ left, top: scrollTop + scrollCounter, behavior });
  }
};
