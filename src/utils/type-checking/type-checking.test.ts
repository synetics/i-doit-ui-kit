import * as utils from './type-checking';

describe('Type checking', () => {
  it('will invoke callback', () => {
    const criteriaFn = jest.spyOn(utils, 'isString');

    utils.getByCriteria<string>(utils.isString, 'test', undefined, true, 123);
    expect(criteriaFn).toHaveBeenCalledTimes(3);
    expect(criteriaFn).toHaveBeenNthCalledWith(1, undefined, expect.anything(), expect.anything());
    expect(criteriaFn).toHaveBeenNthCalledWith(2, true, expect.anything(), expect.anything());
    expect(criteriaFn).toHaveBeenNthCalledWith(3, 123, expect.anything(), expect.anything());
  });

  it('will return correct candidate', () => {
    expect(utils.getByCriteria<string>(utils.isString, 'DEFAULT', undefined, true, 123)).toEqual('DEFAULT');

    expect(utils.getByCriteria<string>(utils.isString, 'DEFAULT', '1st Param', '2nd Param', '3rd Param')).toEqual(
      '1st Param',
    );

    expect(utils.getByCriteria<string>(utils.isString, 'DEFAULT', true, '2nd Param', '3rd Param')).toEqual('2nd Param');

    expect(utils.getByCriteria<string>(utils.isString, 'DEFAULT', true, null, '3rd Param')).toEqual('3rd Param');
  });

  it('will return even falsy values', () => {
    expect(utils.getByCriteria<boolean>(utils.isBoolean, true, undefined, null, false)).toEqual(false);
  });

  it('will return zero', () => {
    expect(utils.getByCriteria<number>(utils.isNumber, 999, undefined, null, 0)).toEqual(0);
  });

  it('will detect numbers only', () => {
    const invalidValues = [undefined, null, {}, '', function test() {}];
    const validValues = [1, -1, 0, Number.MAX_VALUE, Number.MIN_VALUE];
    invalidValues.forEach((e) => expect(utils.isNumber(e)).toEqual(false));
    validValues.forEach((e) => expect(utils.isNumber(e)).toEqual(true));
  });

  it('will detect strings only', () => {
    const invalidValues = [undefined, null, {}, 0, function test() {}, {}];
    const validValues = ['', 'test', '!'];
    invalidValues.forEach((e) => expect(utils.isString(e)).toEqual(false));
    validValues.forEach((e) => expect(utils.isString(e)).toEqual(true));
  });

  it('will detect booleans only', () => {
    const invalidValues = [0, 1, undefined, null, {}, '', function test() {}];
    const validValues = [true, false];
    invalidValues.forEach((e) => expect(utils.isBoolean(e)).toEqual(false));
    validValues.forEach((e) => expect(utils.isBoolean(e)).toEqual(true));
  });

  it('will detect empty string only', () => {
    const invalidValues = [0, 1, undefined, null, {}, '123', function test() {}];
    const validValues = [''];
    invalidValues.forEach((e) => expect(utils.isEmptyString(e)).toEqual(false));
    validValues.forEach((e) => expect(utils.isEmptyString(e)).toEqual(true));
  });
});
