export const getByCriteria = <ReturnType>(
  criteriaFn: (v: unknown) => v is ReturnType,
  defaultValue: ReturnType,
  ...values: unknown[]
): ReturnType => {
  const result: ReturnType | undefined = values.find<ReturnType>(criteriaFn);

  return result !== undefined ? result : defaultValue;
};

export const isTypeOf = (v: unknown, type: string): boolean => typeof v === type;
export const isBoolean = (v: unknown): v is boolean => isTypeOf(v, 'boolean');
export const isString = (v: unknown): v is string => isTypeOf(v, 'string');
export const isNumber = (v: unknown): v is number => isTypeOf(v, 'number');
export const isEmptyString = (v: unknown): boolean => v === '';
