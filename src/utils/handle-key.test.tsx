import { fireEvent, render, screen } from '@testing-library/react';
import { handleKey } from './handle-key';

describe('handleKey()', () => {
  it('returns function', () => {
    const handlerFunc = jest.fn();
    const listeners = {
      Enter: handlerFunc,
    };
    const result = handleKey(listeners, false);

    expect(result).toBeInstanceOf(Function);
  });

  it('calls handler function on event', () => {
    const handlerFunc = jest.fn();
    const listeners = {
      Enter: handlerFunc,
    };
    const result = handleKey(listeners, true);

    render(<input onKeyDown={result} data-testid="testDiv" />);
    const testDiv = screen.getByTestId('testDiv');

    fireEvent.keyDown(testDiv, { key: 'Enter' });

    expect(handlerFunc).toHaveBeenCalledTimes(1);
  });

  it('calls handler function not only on target', () => {
    const handlerFunc = jest.fn();
    const listeners = {
      Enter: handlerFunc,
    };
    const result = handleKey(listeners, false, true);

    render(
      <div>
        <input onKeyDown={result} data-testid="testDiv" />
      </div>,
    );
    const testDiv = screen.getByTestId('testDiv');

    fireEvent.keyDown(testDiv, { key: 'Enter' });

    expect(handlerFunc).toHaveBeenCalledTimes(1);
  });

  it('return if eventHandler returns false', () => {
    const handlerFunc = jest.fn();
    handlerFunc.mockReturnValue(false);
    const listeners = {
      Enter: handlerFunc,
    };

    const result = handleKey(listeners, false);

    render(<input onKeyDown={result} data-testid="testDiv" />);
    const testDiv = screen.getByTestId('testDiv');

    fireEvent.keyDown(testDiv, { key: 'Enter' });

    expect(handlerFunc).toHaveBeenCalledTimes(1);
  });
});
