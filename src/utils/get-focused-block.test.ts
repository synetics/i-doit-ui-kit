import { getFocusedBlock } from './get-focused-block';

describe('getFocusedBlock()', () => {
  it('returns day focused', () => {
    const result = getFocusedBlock('YYYY-MM-DD', 8, 10);
    expect(result).toBe('d');
  });

  it('returns time focused', () => {
    const result = getFocusedBlock('YYYY-MM-DD HH:mm', 14, 16);
    expect(result).toBe('i');
  });

  it('returns empty string if no selected', () => {
    const result = getFocusedBlock('', 10, 0);
    expect(result).toBe('');
  });
});
