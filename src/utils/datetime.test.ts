import { formatDate, getMonthNames, modifyDate } from './datetime';

describe('formatDate()', () => {
  it('returns - if wrong date', () => {
    const delimiter = '/';
    const getDateFormat = formatDate(new Date('13/0/2015'), undefined, delimiter);
    expect(getDateFormat).toBe(delimiter);
  });
});

describe('dateModifier()', () => {
  it('returns the same date if wrong modifier', () => {
    const testDate = new Date();
    const modifyedDate = modifyDate('t', testDate, 2);
    expect(modifyedDate).toBe(testDate);
  });
});

describe('getMonthNames()', () => {
  it('returns default monthnames', () => {
    const monthNames = getMonthNames();
    expect(monthNames).toHaveLength(12);
    expect(monthNames[0]).toBe('January');
  });
});
