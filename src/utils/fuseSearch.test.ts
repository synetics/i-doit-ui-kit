import { fuseSearch } from './fuseSearch'; // Замініть на відповідний шлях до вашого модуля

describe('fuseSearch', () => {
  const testData = [
    { label: 'apple', category: 'fruit' },
    { label: 'banana', category: 'fruit' },
    { label: 'cherry', category: 'fruit' },
    { label: 'date', category: 'fruit' },
    { label: 'carrot', category: 'vegetable' },
    { label: 'some', category: 'apple' },
  ];

  const keys = ['label', 'category'];

  it('should return the same options if searchTerm is empty', () => {
    const searchOptions = { options: testData, filters: { searchTerm: '' }, threshold: 3, keys };
    const result = fuseSearch(searchOptions);
    expect(result).toEqual(testData);
  });

  it('should return the same options without searchTerm and threshold', () => {
    const searchOptions = { options: testData, keys };
    const result = fuseSearch(searchOptions);
    expect(result).toEqual(testData);
  });

  it('should return the same options if searchTerm is less than the threshold', () => {
    const searchOptions = { options: testData, filters: { searchTerm: 'ap' }, threshold: 3, keys };
    const result = fuseSearch(searchOptions);
    expect(result).toEqual(testData);
  });

  it('should return filtered options with a threshold of 2', () => {
    const searchOptions = { options: testData, filters: { searchTerm: 'an' }, threshold: 2, keys };
    const result = fuseSearch(searchOptions);
    expect(result).toEqual([{ label: 'banana', category: 'fruit' }]);
  });

  it('should return an empty array if no matches found', () => {
    const searchOptions = { options: testData, filters: { searchTerm: 'yyy' }, threshold: 3, keys };
    const result = fuseSearch(searchOptions);
    expect(result).toEqual([]);
  });

  it('should return just matched values from keys property', () => {
    const searchOptions = { options: testData, filters: { searchTerm: 'app' }, threshold: 3, keys: ['category'] };
    const result = fuseSearch(searchOptions);
    expect(result).toEqual([{ label: 'some', category: 'apple' }]);
  });
});
