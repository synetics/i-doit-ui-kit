import Fuse from 'fuse.js';

type FuseSearchType<T> = {
  options: T[];
  filters?: {
    searchTerm: string;
  };
  threshold?: number;
  keys: string[];
};

export const fuseSearch = <T>({
  options,
  filters = { searchTerm: '' },
  threshold = 3,
  keys,
}: FuseSearchType<T>): T[] => {
  const fuseOptions = {
    minMatchCharLength: threshold,
    keys,
  };
  if (!filters.searchTerm || filters.searchTerm.length < threshold) {
    return options;
  }
  const fuse = new Fuse(options, fuseOptions);
  return fuse.search(filters.searchTerm).map((el) => el.item);
};
