type PathEvent = Event & {
  path: Node[];
};

type ComposedPathEvent = Event & {
  composedPath: () => EventTarget[];
};

const isComposedPathEvent = (event: Event): event is ComposedPathEvent =>
  Object.prototype.hasOwnProperty.call(event, 'composedPath');

const isPathEvent = (event: Event): event is PathEvent => Object.prototype.hasOwnProperty.call(event, 'path');

const getPathFromEvent = (event: Event): Node[] | EventTarget[] => {
  if (isPathEvent(event) && event.path.length) {
    return event.path;
  }

  if (isComposedPathEvent(event)) {
    return event.composedPath();
  }

  return [];
};

const checkIfClickedChild = <T extends Event>(
  wrapper: (Node & ParentNode) | null,
  event: T,
  checkParents = 0,
): boolean => {
  const path = getPathFromEvent(event);
  if (wrapper && path && path.includes(wrapper)) {
    return true;
  }

  return wrapper && checkParents > 0 ? checkIfClickedChild(wrapper.parentNode, event, checkParents - 1) : false;
};

export { isComposedPathEvent, getPathFromEvent, isPathEvent, checkIfClickedChild };
