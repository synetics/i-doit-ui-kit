import { fireEvent, render, screen } from '@testing-library/react';
import { checkIfClickedChild, getPathFromEvent, isComposedPathEvent, isPathEvent } from './check-if-clicked-child';

describe('checkIfClickedChild helpers test', () => {
  it('helper', () => {
    render(<div>Text</div>);
    document.addEventListener('keyup', (event: KeyboardEvent) => {
      const isComposed = isComposedPathEvent(event);
      expect(isComposed).toBeFalsy();
      const isPath = isPathEvent(event);
      expect(isPath).toBeFalsy();
      const getPath = getPathFromEvent(event);
      expect(getPath).toEqual([]);
    });

    fireEvent.keyUp(document.body);
  });

  it('positive test', () => {
    render(<div>Tested div</div>);
    render(<div>Recreate</div>);

    const elem = screen.getByText('Tested div');
    const elemTest = screen.getByText('Recreate');

    elem.addEventListener('keyup', (event: KeyboardEvent) => {
      const modulateEvent = { ...event, path: [elemTest] };
      modulateEvent.composedPath = () => [event.currentTarget as EventTarget];
      const getPath = getPathFromEvent(modulateEvent);
      expect(getPath).toEqual(modulateEvent.path);
      modulateEvent.path = [];
      getPathFromEvent(modulateEvent);
    });

    fireEvent.keyUp(elem);
  });

  it('checkIfClickedChild()', () => {
    render(
      <div data-testid="wrapper">
        <div>Recreate</div>
      </div>,
    );

    const elem = screen.getByTestId('wrapper');
    const elemTest = screen.getByText('Recreate');

    elemTest.addEventListener('keyup', (event: KeyboardEvent) => {
      const simulateEvent = { ...event, path: [elem] };
      checkIfClickedChild(elem, simulateEvent);
    });

    fireEvent.keyUp(elemTest);
  });
  it('returns false if the wrapper is not in the path of the event', () => {
    const target = document.createElement('div');
    const wrapper = document.createElement('div');
    const path = [target];
    const event = new Event('click', { bubbles: true });
    Object.defineProperty(event, 'path', { value: path });
    expect(checkIfClickedChild(wrapper, event)).toBe(false);
  });
});
