type GetNextType<T, R, Params> = (id: T, callback: (id: T, passed?: Params) => R, rest?: Params) => R;

export const traverse = <T, R, Params>(getNext: GetNextType<T, R, Params>): ((id: T, passed: Params) => R) => {
  const traverseNode = (id: T, passed?: Params): R => getNext(id, traverseNode, passed);
  return traverseNode;
};
