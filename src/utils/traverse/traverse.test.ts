import { traverse } from './traverse';

type Node = {
  id: string;
  children: Node[];
};

describe('traverse()', () => {
  const tree = {
    id: 'id1',
    children: [
      {
        id: 'id2',
        children: [
          { id: 'id3', children: [] },
          { id: 'id4', children: [] },
        ],
      },
    ],
  };
  it('computes depth', () => {
    const depth = traverse<Node, number, number>((id, next, level = 0) =>
      Math.max(level, ...id.children.map((a) => next(a, level + 1))),
    );
    expect(depth(tree, 1)).toBe(3);
    expect(depth(tree.children[0], 1)).toBe(2);
  });
});
