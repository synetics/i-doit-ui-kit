import { BaseSyntheticEvent } from 'react';
import { stopPropagation, uniqueId } from './helpers';

describe('uniqueId', () => {
  it('check uniqueId', () => {
    const result = uniqueId('prefix');
    const result2 = uniqueId('prefix2');
    expect(result).not.toEqual(result2);
  });
  it('stopPropagation function works properly', () => {
    const mockEvent = {
      stopPropagation: jest.fn(),
    };
    stopPropagation(mockEvent as unknown as BaseSyntheticEvent);
    expect(mockEvent.stopPropagation).toHaveBeenCalled();
  });
});
