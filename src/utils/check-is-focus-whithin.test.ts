import { checkIsFocusWithin } from './check-is-focus-whithin';

describe('checkIsFocusWithin', () => {
  it('check if FocusWithin', () => {
    const div = document.createElement('div');

    const result = checkIsFocusWithin(div, div);
    expect(result).toBeTruthy();
  });
});
