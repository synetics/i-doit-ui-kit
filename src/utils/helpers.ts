import { KeyboardEventHandler } from 'react';
import { isObjectWithShape, isString } from '@i-doit/enten-types';
import { AbstractFunction } from '../types/function';

type Handler = AbstractFunction;

/**
 * Helper to handle the key events
 * @param handlers {Record<string, Handler>}
 * @param prevent {boolean} - if event should be caught
 * @returns {void}
 */
export const handleKey: <T = Element>(
  handlers: Record<string, Handler>,
  prevent?: boolean,
) => KeyboardEventHandler<T> =
  (handlers, prevent = true) =>
  (e) => {
    if (typeof handlers[e.key] === 'function') {
      if (handlers[e.key](e) === false) {
        return;
      }
      if (prevent) {
        e.preventDefault();
      }
    }
  };

/**
 * @param {string} prefix
 * @returns {string}
 */
export const uniqueId = (prefix = ''): string => `${prefix}${new Date().getTime()}`;

/**
 * @param {string} className
 * @returns {boolean}
 */
export const hasClassName = isObjectWithShape({
  className: isString,
});

/**
 * @param {string} fill
 * @returns {boolean}
 */
export const hasFill = isObjectWithShape({
  fill: isString,
});

type EventLike = { stopPropagation: () => void };

export const withStopPropagation =
  <T extends EventLike>(handler: (e: T) => void) =>
  (event: T): void => {
    event.stopPropagation();
    handler(event);
  };

export const stopPropagation = <T extends EventLike>(event: T): void => event.stopPropagation();
