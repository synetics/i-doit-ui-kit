type ObjectKeysType = {
  [key: string]: string | number;
};

const omit = (obj: ObjectKeysType, ...keys: Array<PropertyKey>): ObjectKeysType => {
  const result = {} as ObjectKeysType;
  Object.keys(obj).forEach((key) => {
    if (!Object.prototype.hasOwnProperty.call(obj, key) || keys.includes(key)) {
      return;
    }
    result[key] = obj[key];
  });
  return result;
};

export { omit, ObjectKeysType };
