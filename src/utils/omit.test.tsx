import { render, screen } from '@testing-library/react';
import { Button, ButtonProps } from '../components/Button';
import { ObjectKeysType, omit } from './omit';

const props = { variant: 'secondary', active: 'test' } as ButtonProps;

describe('omit()', () => {
  it('returns an object', () => {
    const omitted = omit(props as ObjectKeysType, 'label');

    expect(omitted).toBeInstanceOf(Object);
  });

  it('returns component without selected prop', () => {
    render(<Button {...omit(props as ObjectKeysType, 'active')} />);

    expect(screen.getByRole('button')).not.toHaveAttribute('active');
  });
});
