/**
 * Traverses through the tree structure and returns the nodes with matching
 *
 * @param node
 * @param traverse
 * @param match
 */
export const filterTree = <T>(node: T, traverse: (node: T) => T[], match: (node: T) => boolean): T[] => {
  const items = traverse(node)
    .map((a) => filterTree(a, traverse, match))
    .reduce((r, a) => [...r, ...a], []);

  if (items.length === 0 && !match(node)) {
    return [];
  }

  return [node, ...items];
};
