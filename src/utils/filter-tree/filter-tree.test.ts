import { filterTree } from './filter-tree';

describe('filterTree()', () => {
  const tree = {
    id: 'id1',
    children: [
      {
        id: 'id2',
        children: [
          { id: 'id3', children: [] },
          { id: 'id4', children: [] },
        ],
      },
    ],
  };

  it('traverses through the complete tree', () => {
    const foundNodes = filterTree(
      tree,
      (a) => a.children,
      () => true,
    );
    expect(foundNodes).toHaveLength(4);
    expect(foundNodes[0]).toBe(tree);
    expect(foundNodes[1]).toBe(tree.children[0]);
  });

  it('traverses collects parents on matching child', () => {
    const foundNodes = filterTree(
      tree,
      (a) => a.children,
      (node) => node.id === 'id3',
    );
    expect(foundNodes).toHaveLength(3);
    expect(foundNodes[0]).toBe(tree);
    expect(foundNodes[1]).toBe(tree.children[0]);
    expect(foundNodes[2]).toBe(tree.children[0].children[0]);
  });

  it('traverses does not collect not matching child', () => {
    const foundNodes = filterTree(
      tree,
      (a) => a.children,
      (node) => node.id === 'id2',
    );
    expect(foundNodes).toHaveLength(2);
    expect(foundNodes[0]).toBe(tree);
    expect(foundNodes[1]).toBe(tree.children[0]);
  });
});
