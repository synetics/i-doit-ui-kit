import { useMemo } from 'react';

type MatchProps = {
  text: string;
  search: string;
};

export const Match = ({ text, search }: MatchProps) => {
  const result = useMemo(() => {
    const textWithReplacedSpaces = text.replace(/ /g, '\u00a0');
    const data = [];
    const parts = text.toLocaleLowerCase().split(search.toLocaleLowerCase());
    let length = 0;
    for (let i = 0; i < parts.length - 1; i += 1) {
      const part = parts[i];
      data.push(
        <span className="mx-0" key={`${text}-${i}`}>
          {textWithReplacedSpaces.substring(length, length + part.length)}
        </span>,
      );
      length += part.length;
      data.push(
        <b className="mx-0" key={`${text}-bold-${i}`}>
          {textWithReplacedSpaces.substring(length, length + search.length)}
        </b>,
      );
      length += search.length;
    }
    data.push(<span className="mx-0">{textWithReplacedSpaces.substring(length)}</span>);
    return data;
  }, [text, search]);

  if (search.length < 2) {
    return <>{text}</>;
  }

  return <>{result}</>;
};
