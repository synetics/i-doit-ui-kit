import faker from 'faker';
import { uniqueId } from '../../unique-id';

export type NodeItem<T> = {
  id: string;
  title: string;
  children: T[];
};

type Node = NodeItem<unknown>;

type FlatNodeItem = NodeItem<string>;

export const generateItem = <T>(): NodeItem<T> => ({
  id: uniqueId('node-'),
  title: faker.address.city(),
  children: [] as T[],
});

export const generateItemsFlat = (item: FlatNodeItem, level: number, maxLevel: number): FlatNodeItem[] => {
  const items =
    level + 1 > maxLevel ? [] : new Array(Math.ceil(Math.random() * 3)).fill(0).map(() => generateItem<string>());

  return [
    {
      ...item,
      children: items.map((a) => a.id),
    },
    ...items.reduce((result, a) => [...result, ...generateItemsFlat(a, level + 1, maxLevel)], [] as FlatNodeItem[]),
  ];
};

export const generateNestedItem = (level: number, maxLevel: number): Node => {
  const item = generateItem<Node>();
  item.children =
    level + 1 > maxLevel
      ? ([] as Node[])
      : new Array(Math.ceil(Math.random() * 3)).fill(0).map(() => generateNestedItem(level + 1, maxLevel));
  return item;
};

export const tree = generateItemsFlat(
  {
    id: 'root',
    title: 'All cities',
    children: [],
  },
  0,
  5,
);
