import { ToggleButton } from '../../../components/List/ToggleButton/ToggleButton';
import { NestedList } from '../../../components/List/NestedList';
import { Item } from '../../../components/List/Item';
import { Match } from './Match';
import { NodeItem } from './generate';

type NodeProps<T = unknown, NodeType extends NodeItem<T> = NodeItem<T>> = {
  node: NodeType;
  getChildren: (item: NodeType) => NodeType[];
  search: string;
  closed: string[];
  onToggle: (node: NodeType) => void;
};

export const Node = ({ node, getChildren, search, closed, onToggle }: NodeProps) => {
  const isOpen = !closed.includes(node.id);
  const children = getChildren(node);
  return (
    <div>
      <Item>
        <ToggleButton
          disabled={children.length === 0}
          isOpen={isOpen}
          onToggle={() => onToggle(node)}
          variant="text"
          className="mr-2"
        />
        <Match search={search} text={node.title} />
      </Item>
      {isOpen && children.length > 0 && (
        <NestedList>
          {children.map((a) => (
            <Node key={a.id} search={search} node={a} getChildren={getChildren} closed={closed} onToggle={onToggle} />
          ))}
        </NestedList>
      )}
    </div>
  );
};
