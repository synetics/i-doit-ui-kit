import {
  addDays,
  addHours,
  addMinutes,
  addMonths,
  addSeconds,
  addYears,
  format as fnsFormat,
  startOfYear,
} from 'date-fns';
import en from 'date-fns/locale/en-US';
import FnsFormat from 'date-fns/format';
import FnsParse from 'date-fns/parse';

type ObjectKeysType = {
  [key: string]: (date: number | Date, amount: number) => Date;
};

export const formatDate = (date: Date | string | null | undefined, format = 'yyyy-MM-dd', delimiter = ''): string => {
  if (!(date instanceof Date) || Number.isNaN(date.getDate())) {
    return delimiter;
  }
  return FnsFormat(date, format);
};

export const parseDate = (dateString: string, format = 'yyyy-MM-dd'): Date => FnsParse(dateString, format, new Date());

export const modifyDate = (type: string, date: Date, amount: number): Date => {
  const dateModifier = {
    y: addYears,
    m: addMonths,
    d: addDays,
    h: addHours,
    i: addMinutes,
    s: addSeconds,
  } as ObjectKeysType;

  if (!dateModifier[type]) {
    return date;
  }

  return dateModifier[type](date, amount);
};

export const checkDateInRange = (date: Date, minDate: Date, maxDate: Date): boolean => {
  const isDate = date.getTime && !Number.isNaN(date.getTime());
  const isInRange = date >= minDate && date <= maxDate;
  return isDate && isInRange;
};

export const getMonthNames = (currentLocale: Locale = en): string[] => {
  const monthNames = [];
  let dateIterator = startOfYear(new Date());

  for (let i = 0; i < 12; i += 1) {
    monthNames.push(fnsFormat(dateIterator, 'MMMM', { locale: currentLocale }));

    dateIterator = addMonths(dateIterator, 1);
  }

  return monthNames;
};
