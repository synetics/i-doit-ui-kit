import { render, screen } from '@testing-library/react';
import { makeDndScroll } from './make-dnd-scroll';

describe('makeDndScroll()', () => {
  beforeEach(() => {
    window.HTMLElement.prototype.scrollTo = () => {};
  });

  it('scrolls to area', () => {
    window.scrollTo = () => {};
    const onScroll = jest.fn();
    render(
      <div onScroll={onScroll} data-testid="myTest">
        Test
      </div>,
    );
    makeDndScroll(800, 50, screen.getByTestId('myTest'));
  });
});
