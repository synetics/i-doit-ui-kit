export const getFocusedBlock = (dateFormat: string, start: number, length: number): string => {
  const addRange = start > 0 ? 1 : 0;
  const focusedBlock = dateFormat.substr(start - addRange, Math.max(1, length) + addRange);
  const resultFocused = focusedBlock.replace(/m/g, 'i').toLowerCase();
  if (/[a-z]/.test(resultFocused)) {
    const foundMatches = /[a-z]/.exec(resultFocused);
    return foundMatches ? foundMatches[0] : '';
  }

  return '';
};
