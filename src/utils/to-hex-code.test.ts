import { toHexCode } from './to-hex-code';

describe('toHexCode()', () => {
  it('returns string if match', () => {
    const result = toHexCode('AA6B6B');

    expect(result).toBe('AA6B6B');
  });

  it('returns empty if no matches', () => {
    const result = toHexCode('$$$');

    expect(result).toBe('');
  });
});
