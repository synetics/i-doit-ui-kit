import { createContext, Provider, useContext } from 'react';
import { OutOfContextError } from './out-of-context.error';

/**
 * Creates a context provider and a hook with build-in error handling
 *
 * @param name - name of the context
 * @param defaultValue - a value used when a consumer does not have a matching Provider above it in the tree
 */
const createLocalContext = <T = null>(name: string, defaultValue?: T): [Provider<T | null>, () => Readonly<T>] => {
  const Context = createContext<T | null>(defaultValue || null);

  const useLocalContext = (): Readonly<T> => {
    const context = useContext(Context);

    if (!context) {
      throw new OutOfContextError(name);
    }

    return context;
  };

  return [Context.Provider, useLocalContext];
};

export { createLocalContext };
