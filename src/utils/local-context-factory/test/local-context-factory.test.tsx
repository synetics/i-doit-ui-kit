/* eslint-disable react/prop-types */
import { render, screen } from '@testing-library/react';
import { FC, PropsWithChildren } from 'react';
import { createLocalContext } from '../local-context-factory';
import { OutOfContextError } from '../out-of-context.error';

describe('createLocalContext', () => {
  let consoleErrorSpy: jest.SpyInstance;

  beforeAll(() => {
    consoleErrorSpy = jest.spyOn(console, 'error').mockImplementation(() => {});
  });

  afterAll(() => {
    consoleErrorSpy.mockRestore();
  });

  it('creates a Provider and a hook by the give name', () => {
    const [Provider, useLocalContext] = createLocalContext<{ foo: string }>('contextName');
    const Parent = ({ children }: PropsWithChildren) => <Provider value={{ foo: 'foo' }}>{children}</Provider>;
    const Kid: FC = () => {
      const { foo } = useLocalContext();
      return <div>{foo}</div>;
    };

    render(
      <Parent>
        <Kid />
      </Parent>,
    );

    expect(screen.getByText('foo')).toBeInTheDocument();
  });

  it('throws OutOfContextError when Provider is missing above the consumer in the tree', () => {
    const [, useLocalContext] = createLocalContext<{ foo: string }>('contextName');
    const Kid: FC = () => {
      const { foo } = useLocalContext();
      return <div>{foo}</div>;
    };

    try {
      render(<Kid />);
    } catch (e) {
      expect(e).toBeInstanceOf(OutOfContextError);
    }
  });
});
