import { OutOfContextError } from '../out-of-context.error';

it('throws OutOfContextError', () => {
  function errorProneFunc() {
    throw new OutOfContextError('contextName');
  }

  expect(errorProneFunc).toThrow(OutOfContextError);
});
