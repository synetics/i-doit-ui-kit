class OutOfContextError extends Error {
  constructor(name: string) {
    super(name);
    Object.setPrototypeOf(this, OutOfContextError.prototype);
    this.message = `Attempt to access ${name} context from outside. Please check components tree or provide a default value via Provider.`;
    this.name = 'OutOfContextError';
  }
}

export { OutOfContextError };
