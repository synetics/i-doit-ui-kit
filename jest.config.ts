import { Config } from '@jest/types';

export default async (): Promise<Config.InitialOptions> => ({
  clearMocks: true,
  collectCoverageFrom: [
    './src/**',
    '!./src/styles/**',
    '!./src/**/index.ts',
    '!./src/**/stories/**',
    '!./src/**/*.stories.*',
    '!./src/**/*.const.ts',
    '!./src/**/*.mock.ts',
    '!./src/icons/**',
    // jest dom doesn't support DnD
    '!./src/components/DragAndDrop/ScrollableDnDcontainer/use-scroller.ts',
  ],
  coverageReporters: ['lcov'],
  coverageThreshold: {
    global: {
      branches: 90,
      functions: 90,
      lines: 90,
      statements: 90,
    },
  },
  maxWorkers: '50%',
  moduleNameMapper: {
    '\\.scss$': 'identity-obj-proxy',
    '@uiw/react-markdown-preview': '<rootDir>/node_modules/@uiw/react-markdown-preview/dist/markdown.min.js',
    '@uiw/react-md-editor': '<rootDir>/node_modules/@uiw/react-md-editor/dist/mdeditor.min.js',
  },
  roots: ['<rootDir>/src', '<rootDir>'],
  setupFilesAfterEnv: ['<rootDir>/test/jest-setup.config.ts'],
  transform: {
    '^.+\\.tsx?$': 'ts-jest',
    '^.+\\.svg$': '<rootDir>/test/utils/svg-transform.js',
  },
  testEnvironment: 'jsdom',
  testMatch: ['**/__tests__/**/*.+(ts|tsx)', '**/?(*.)+(spec|test).+(ts|tsx)'],
  testPathIgnorePatterns: ['<rootDir>/node_modules/'],
  modulePathIgnorePatterns: ['<rootDir>/lib', '<rootDir>/src/types'],
});
