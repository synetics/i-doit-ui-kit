const projectRoot = process.cwd();

module.exports = {
  process(src, filepath) {
    const filename = filepath.substr(projectRoot.length + 1);

    const imageMock = JSON.stringify(filename);

    return {
      code: `module.exports = ${imageMock};`,
    };
  },
};
