import { render, RenderResult } from '@testing-library/react';
import React from 'react';

type RendererResult<A> = RenderResult & { update: (nextProps: A) => void };

export type CreateRenderer = <A>(
  Component: React.ComponentType<A>,
  requiredProps: A,
) => (newProps?: A) => RendererResult<A> & A;

export const createRenderer: CreateRenderer = (Component, requiredProps) => (newProps) => {
  const { rerender, ...utils } = render(React.createElement(Component, { ...requiredProps, ...(newProps || {}) }));

  return {
    ...requiredProps,
    ...utils,
    rerender,
    update: (nextProps: React.ReactElement['props']) => rerender(React.createElement(Component, { ...nextProps })),
  };
};
