import { FC } from 'react';
import { Props } from 'react-svg';

/* eslint-disable react/prop-types */
const ReactSVG: FC<Props> = ({
  src,
  style,
  beforeInjection,
  wrapper: Wrapper = 'div',
  onClick,
  'data-testid': testId,
  'aria-hidden': ariaHidden,
  className,
}) => {
  const svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');

  if (beforeInjection) {
    beforeInjection(svg);
  }

  return (
    // eslint-disable-next-line react/jsx-props-no-spreading
    <Wrapper
      data-src={src}
      onClick={onClick}
      data-testid={testId}
      style={style}
      aria-hidden={ariaHidden}
      className={className}
    >
      SVGElementMock
    </Wrapper>
  );
};

export { ReactSVG };
