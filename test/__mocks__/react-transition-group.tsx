/* eslint-disable */
import React from 'react';
import classnames from 'classnames';

const Transition = jest.fn(({ children }) => children);

const CSSTransition = jest.fn(({ in: inProp, children, onEnter, onEntering, onExiting, classNames }) => {
  const div = document.createElement('div');

  if (typeof onEnter === 'function') {
    onEnter(div);
  }

  if (typeof onEntering === 'function') {
    onEntering(div);
  }

  if (typeof onExiting === 'function') {
    onExiting(div);
  }

  return inProp ? (
    <Transition>
      <div className={classnames(classNames)}>{children}</div>
    </Transition>
  ) : null;
});

export { Transition, CSSTransition };
