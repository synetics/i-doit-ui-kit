import '../src/styles/i-doit-ui-kit.scss';
import './reset.scss';

export const parameters = {
  options: {
    storySort: {
      method: 'alphabetical',
      order: ['Components', ['*', 'Misc'], '*'],
      includeNames: false,
      locales: 'en-US',
    },
  },
  parameters: {
    actions: { argTypesRegex: '^on[A-Z].*' },
  },
};
