const { mergeConfig } = require('vite');

module.exports = {
  core: {
    options: {
      framework: 'react',
    },
  },

  stories: ['../src/**/*.stories.mdx', '../src/**/*.stories.@(js|jsx|ts|tsx)'],

  addons: ['@storybook/addon-links', '@storybook/addon-essentials', '@storybook/addon-mdx-gfm'],

  viteFinal: (config) =>
    mergeConfig(config, {
      base: '',
      resolve: {
        alias: [
          {
            find: /^~(.*)$/,
            replacement: '$1',
          },
        ],
      },
    }),

  framework: {
    name: '@storybook/react-vite',
    options: {},
  },

  docs: {
    autodocs: true,
  },
};
