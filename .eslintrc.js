module.exports = {
  parserOptions: {
    ecmaVersion: 2020,
    sourceType: 'module',
    ecmaFeatures: {
      jsx: true,
    },
  },
  env: {
    browser: true,
    node: true,
    jest: true,
  },
  extends: ['eslint:recommended'],
  rules: {},
  overrides: [
    {
      files: '**/*.+(ts|tsx)',
      parser: '@typescript-eslint/parser',
      parserOptions: {
        project: './tsconfig.json',
      },
      plugins: ['@typescript-eslint/eslint-plugin', 'testing-library', 'jest-dom'],
      extends: [
        'airbnb-typescript',
        'airbnb/hooks',
        'plugin:@typescript-eslint/recommended',
        'plugin:@typescript-eslint/recommended-requiring-type-checking',
        'plugin:import/typescript',
        'plugin:import/warnings',
        'plugin:testing-library/react',
        'plugin:jest-dom/recommended',
        'prettier',
      ],
      rules: {
        'require-await': 0,
        'no-console': ['error', { allow: ['warn', 'error'] }],
        'no-unused-vars': 'error',
        'react/button-has-type': 0,
        'react/jsx-uses-react': 0,
        'react/jsx-props-no-spreading': 0,
        'react/react-in-jsx-scope': 0,
        'react/require-default-props': 0,
        'react/prop-types': 0,

        '@typescript-eslint/require-await': 0,
        '@typescript-eslint/no-empty-interface': 0,

        'import/prefer-default-export': 0,
        'import/no-extraneous-dependencies': [
          'error',
          {
            devDependencies: ['./test-utils/*', './src/**/*.test.tsx', './src/**/*.test.ts'],
          },
        ],
        'import/no-cycle': ['error'],
        'import/namespace': [2, { allowComputed: true }],
        'import/order': [
          'error',
          {
            groups: ['builtin', 'external', 'internal', 'parent', 'index', 'sibling'],
            'newlines-between': 'never',
          },
        ],
        'jsx-a11y/label-has-associated-control': [
          'error',
          {
            required: {
              some: ['nesting', 'id'],
            },
          },
        ],
      },
      settings: {
        'import/core-modules': [
          '@testing-library/jest-dom/extend-expect',
          '@jest/types',
          '@testing-library/react',
          'faker',
        ],
      },
    },
  ],
};
