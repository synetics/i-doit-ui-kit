$npmTag = ((git branch --show-current)).replace("/", "-").replace("_", "-");
release-it --preRelease=$npmTag --npm.tag=$npmTag -c .pre-release-it.json --ci
