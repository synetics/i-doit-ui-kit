# I-DOIT UI Kit

[![Conventional Commits](https://img.shields.io/badge/Conventional%20Commits-1.0.0-yellow.svg)](https://conventionalcommits.org)
![Code style](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)

## Contents

The UI Kit is the design system built to create a consistent customer experience across our digital products.
This package contains design tokens, styling (SCSS) resources, as well as React Components.

## System Requirements

- git v2.13 or greater
- NodeJS v18 or greater
- npm v8 or greater

## Installation

UI-Kit is not public and will be hosted over an internal private npm registry. To be able to reach the registry you have to login to vpn.
After that you can simply run:

With yarn
```bash
yarn add i-doit-ui-kit
```

With npm

```bash
npm install i-doit-ui-kit
```

The commands above install the latest version of the ui-kit from the private npm registry into your node_modules folder and
automatically adds the dependency to your package.json

In general you can treat it as a regular npm package placed in the official npm registry:
```bash
# Latest version
$ npm install i-doit-ui-kit
# or
$ npm install i-doit-ui-kit@latest

# Specific version
$ npm install i-doit-ui-kit@5.5.5
```

## Development workflow

Before you start work (and if you haven't already), it would be a good idea to
discuss the change you're about to make with the dev team and UI/UX designer. You can reach us here
[#docupike](https://i-doit.slack.com/archives/CF7AC6125).

1. Create a feature/bug branch off develop branch.

1. Develop the change in your feature branch.

    > Use [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/).
    > The Conventional Commits is a specification for adding human and machine-readable meaning to commit messages.
    > It allows us to autogenerate CHANGELOG.md

1. Ensure you add or update the appropriate tests for your change, and all tests
    are passing.
    > use `npm run test` or `npm test` or even `npm t` to run tests;

    > use `npm run test:coverage` to run test with coverage.

1. When you're finished, create a [merge request](https://bitbucket.org/synetics/i-doit2.0/pull-requests/new)
    with a description of what you're changing and why.

    > If you don't have push access to the repo, please drop a note in [#docupike](https://i-doit.slack.com/archives/CF7AC6125)
    and someone will be able to give you access.

## Release workflow

After development of your feature / bugfix in a sub-branch and finishing the task you can simply run `npm run release` to publish a new version.
This will ensure that your release will be based on the last available version present in master to prevent duplicates:

```
* Merge origin/master into feature/bug branch
* Update the CHANGELOG.md
* Bump the version in package.json
* Publish release to private npm registry
* Commit changed files
* Generate git tag
* Push everything to feature/bug branch
```

Right after creating the release you should be able to merge your feature/bug branch into master to prevent overlapping versions.
Therefore it is very important to create a release after QA have tested the changes and guarantees its correct functionality. This implies that the PR already have the
necessary amount of approves.

Please consider that a release can only be applied if your local instance is clean and not include any changed files.

### Pre-Releases

Pre-Releases allow you to generate versions which are not final but can be used in external projects and validated by QA. These are predecessors of regular releases.
To generate these kind of releases just run `npm run pre-release`.

## Symlinks

Package linking is a three-step process:

- Create a global symlink for react from docupike to prevent bundling it in ui-kit.

```bash
cd ~/docupike/app/node_modules/react
npm link
cd ~/docupike/app/node_modules/react-dnd
npm link
cd ~/i-doit-ui-kit
npm link react
```

- Create a global symlink for a dependency with `npm link`. A symlink, short for symbolic link, is a shortcut that
   points to another directory or file on your system.

```bash
cd ~/i-doit-ui-kit
npm link
```

- Tell the application to use the global symlink with `npm link i-doit-ui-kit`.

```bash
cd ~/projects/docupike
npm link i-doit-ui-kit
```

As an alternative you could use the following npm scripts that are being provided by the I-DOIT-UI-KIT

`npm run symlink:list` - displays a list with global symlinks.

`npm run symlink:link` - creates a new global symlink of the i-doit-ui-kit

`npm run symlink:unlink` - removes the global symlink of the i-doit-ui-kit

### Symlink and Docker

This functionality is not supported out-of-the-box by docker due to its nature and any container has no assess
to symlinks created via `npm link`. Actually, there are several ways to make docker aware of symlinks.
We use [volumes](https://docs.docker.com/storage/volumes) to provide access from the docker image to the root directory of the `docupike` app.
See `docker-compose.yaml` for details. Currently, it is a workaround that gives us the ability to do everything on
the host machine and let the container use the build artifacts produced by `npm run build` or `npm run start`.

In other words, for local development it is enough to run `npm run start` without executing `docker-compose exec app sh`.

## LICENSE

[MIT](LICENSE)
