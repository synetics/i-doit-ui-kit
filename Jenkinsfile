pipeline {
    agent any
    stages {
        stage('Dependencies') {
            steps {
                sh './ci/prepare.sh'
                sh './ci/npm.sh ci'
            }
        }
        stage('Lint') {
          steps {
              sh './ci/npm.sh run eslint'
          }
        }
        stage('Validate') {
          parallel {
            stage('Check types') {
              steps {
                  sh './ci/npm.sh run check:types'
              }
            }
            stage('Test') {
              steps {
                  sh './ci/npm.sh run test:coverage -- --ci --silent'
              }
              post {
                  always {
                    publishHTML (target: [
                      allowMissing: false,
                      alwaysLinkToLastBuild: false,
                      keepAll: true,
                      reportDir: 'coverage/lcov-report',
                      reportFiles: 'index.html',
                      reportName: "Coverage"
                    ])
                  }
                }
            }
            stage('Build') {
              steps {
                  sh './ci/npm.sh run build'
              }
            }
            stage('Build storybook') {
              steps {
                  sh './ci/npm.sh run storybook:build'
                  publishHTML (target: [
                      allowMissing: false,
                      alwaysLinkToLastBuild: false,
                      keepAll: true,
                      reportDir: 'storybook-static',
                      reportFiles: 'index.html',
                      reportName: "Storybook"
                  ])
              }
            }
          }
        }
    }
    post {
        always {
            deleteDir()
        }
        failure {
            bitbucketStatusNotify(buildState: 'FAILED', commitId: "${env.GIT_COMMIT}")
            slackSend channel: "#jenkins", color: "danger", message: "FAILED - ${env.BUILD_URL}"
        }
        unstable {
            bitbucketStatusNotify(buildState: 'SUCCESSFUL', commitId: "${env.GIT_COMMIT}")
            slackSend channel: "#jenkins", color: "warning", message: "UNSTABLE - ${env.BUILD_URL}"
        }
        success {
            bitbucketStatusNotify(buildState: 'SUCCESSFUL', commitId: "${env.GIT_COMMIT}")
        }
        aborted {
            bitbucketStatusNotify(buildState: 'FAILED', commitId: "${env.GIT_COMMIT}")
            slackSend channel: "#jenkins", color: "danger", message: "ABORTED - ${env.BUILD_URL}"
        }
        fixed {
            bitbucketStatusNotify(buildState: 'SUCCESSFUL', commitId: "${env.GIT_COMMIT}")
            slackSend channel: "#jenkins", color: "good", message: "BACK TO NORMAL - ${env.BUILD_URL}"
        }
    }
}
