

## [6.48.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.48.0-task-ID-11347-stepper.2...v6.48.1) (2024-11-28)

# [6.48.0-task-ID-11347-stepper.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.48.0-task-ID-11347-stepper.1...v6.48.0-task-ID-11347-stepper.2) (2024-11-26)

# [6.48.0-task-ID-11347-stepper.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.48.0-task-ID-11347-stepper.0...v6.48.0-task-ID-11347-stepper.1) (2024-11-25)

# [6.48.0-task-ID-11347-stepper.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.47.0-ID-11347-ui-kit-create-stepper-component.1...v6.48.0-task-ID-11347-stepper.0) (2024-11-22)


### Features

* **stepper:** rebuild stepper component to be more compound ([9370e06](https://bitbucket.org/synetics/i-doit-ui-kit/commits/9370e06fb48a533272763609802698610138b402))



# [6.47.0-ID-11347-ui-kit-create-stepper-component.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.47.0-ID-11347-ui-kit-create-stepper-component.1...v6.48.0-task-ID-11347-stepper.0) (2024-11-20)



# [6.47.0-ID-11347-ui-kit-create-stepper-component.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.47.0-ID-11347-ui-kit-create-stepper-component.1...v6.48.0-task-ID-11347-stepper.0) (2024-11-15)


### Features

* **Stepper:** add Stepper component ([6fadc13](https://bitbucket.org/synetics/i-doit-ui-kit/commits/6fadc13a1fad697b369be9106154a0ee751a2025))



# [6.47.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.47.0-ID-11347-ui-kit-create-stepper-component.1...v6.48.0-task-ID-11347-stepper.0) (2024-11-21)


### Features

* **Button:** add new button variant ([dc363b5](https://bitbucket.org/synetics/i-doit-ui-kit/commits/dc363b5c1f608627666d10f7c9bad12728660057))

# [6.47.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.46.1...v6.47.0) (2024-11-21)


### Features

* **Button:** add new button variant ([dc363b5](https://bitbucket.org/synetics/i-doit-ui-kit/commits/dc363b5c1f608627666d10f7c9bad12728660057))

## [6.46.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.46.1-task-ID-11350-improve-card.2...v6.46.1) (2024-11-18)

## [6.46.1-task-ID-11350-improve-card.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.46.1-task-ID-11350-improve-card.1...v6.46.1-task-ID-11350-improve-card.2) (2024-11-18)

## [6.46.1-task-ID-11350-improve-card.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.46.1-task-ID-11350-improve-card.0...v6.46.1-task-ID-11350-improve-card.1) (2024-11-18)

## [6.46.1-task-ID-11350-improve-card.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.46.0...v6.46.1-task-ID-11350-improve-card.0) (2024-11-13)


### Bug Fixes

* **card:** adjust handling of focus ([b0c014f](https://bitbucket.org/synetics/i-doit-ui-kit/commits/b0c014ff1daccc76893bbd71218c12050f1c3679))

# [6.46.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.45.0...v6.46.0) (2024-11-08)


### Features

* **Card:** add Card component ([bae041e](https://bitbucket.org/synetics/i-doit-ui-kit/commits/bae041e9283ede2f2c9c3b706919cb020f169424))

# [6.45.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.44.0...v6.45.0) (2024-11-06)


### Features

* **Indicator:** add Indicator component ([64d2526](https://bitbucket.org/synetics/i-doit-ui-kit/commits/64d2526f088c5206cf9d0e0ce0570f8ba31aebaf))

# [6.44.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.43.9...v6.44.0) (2024-11-05)


### Features

* **Badge:** add Badge component ([cc4b621](https://bitbucket.org/synetics/i-doit-ui-kit/commits/cc4b6218e844ccc601de79c3a458e90aed1883f2))
* **Progress:** add Progress component ([8797992](https://bitbucket.org/synetics/i-doit-ui-kit/commits/87979928e8c6c16154be627e4396d8b27aef9682))

## [6.43.9](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.43.9-ID2-0000-adjust-password-text.0...v6.43.9) (2024-10-10)

## [6.43.9-ID2-0000-adjust-password-text.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.43.8...v6.43.9-ID2-0000-adjust-password-text.0) (2024-10-10)


### Bug Fixes

* **Password:** change label text ([4e122a0](https://bitbucket.org/synetics/i-doit-ui-kit/commits/4e122a051edb81638080b0b571fd1036729319d8))

## [6.43.8](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.43.8-ID2-0000-adjust-status-text-cell.2...v6.43.8) (2024-09-17)

## [6.43.8-ID2-0000-adjust-status-text-cell.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.43.8-ID2-0000-adjust-status-text-cell.1...v6.43.8-ID2-0000-adjust-status-text-cell.2) (2024-09-16)

## [6.43.8-ID2-0000-adjust-status-text-cell.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.43.8-ID2-0000-adjust-status-text-cell.0...v6.43.8-ID2-0000-adjust-status-text-cell.1) (2024-09-16)

## [6.43.8-ID2-0000-adjust-status-text-cell.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.43.7...v6.43.8-ID2-0000-adjust-status-text-cell.0) (2024-09-16)


### Bug Fixes

* **StatusTextCell:** add children ([94a2fdd](https://bitbucket.org/synetics/i-doit-ui-kit/commits/94a2fddc18de0b6f6af55537db286de5c7280bf6))

## [6.43.7](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.43.7-ID2-0000-adjust-types.0...v6.43.7) (2024-09-13)

## [6.43.7-ID2-0000-adjust-types.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.43.6...v6.43.7-ID2-0000-adjust-types.0) (2024-09-13)


### Bug Fixes

* **IconButton:** adjust label type ([5fd0fbf](https://bitbucket.org/synetics/i-doit-ui-kit/commits/5fd0fbf861edd6a1b2b5929167cc8a688d521167))

## [6.43.6](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.43.5...v6.43.6) (2024-09-05)


### Bug Fixes

* **icons:** add icon ([ce7323b](https://bitbucket.org/synetics/i-doit-ui-kit/commits/ce7323b31e2185aea69920c1bb1287d25c1b7ca4))

## [6.43.5](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.43.5-ID2-0000-tags-style.3...v6.43.5) (2024-08-27)

## [6.43.5-ID2-0000-tags-style.3](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.43.5-ID2-0000-tags-style.2...v6.43.5-ID2-0000-tags-style.3) (2024-08-27)

## [6.43.5-ID2-0000-tags-style.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.43.5-ID2-0000-tags-style.1...v6.43.5-ID2-0000-tags-style.2) (2024-08-27)

## [6.43.5-ID2-0000-tags-style.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.43.5-ID2-0000-tags-style.0...v6.43.5-ID2-0000-tags-style.1) (2024-08-27)

## [6.43.5-ID2-0000-tags-style.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.43.4...v6.43.5-ID2-0000-tags-style.0) (2024-08-27)


### Bug Fixes

* **ComboBox:** fixed styles ([65fe37a](https://bitbucket.org/synetics/i-doit-ui-kit/commits/65fe37a1af90786dd7667449b91dd9e8c84a8ba5))

## [6.43.4](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.43.4-ID2-0000-fix-banners.0...v6.43.4) (2024-08-15)

## [6.43.4-ID2-0000-fix-banners.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.43.3...v6.43.4-ID2-0000-fix-banners.0) (2024-08-15)


### Bug Fixes

* **Banner:** adjust imports ([dacdfb7](https://bitbucket.org/synetics/i-doit-ui-kit/commits/dacdfb71f73d87615c04ad8b32c20750d9d3074a))

## [6.43.3](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.43.2...v6.43.3) (2024-08-15)


### Bug Fixes

* **Banner:** adjust styles and notification ([b3d62d4](https://bitbucket.org/synetics/i-doit-ui-kit/commits/b3d62d48d96a63abd26ed7aafcd2630ef5028000))

## [6.43.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.43.2-ID2-0000-fix-alignment-of-checkbox-column.0...v6.43.2) (2024-08-13)

## [6.43.2-ID2-0000-fix-alignment-of-checkbox-column.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.43.1...v6.43.2-ID2-0000-fix-alignment-of-checkbox-column.0) (2024-08-12)


### Bug Fixes

* **Table:** fix alignment of checkbox column ([982ec86](https://bitbucket.org/synetics/i-doit-ui-kit/commits/982ec86491173bea913cca221ebaf04825c71c9d))

## [6.43.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.43.1-ID2-0000-adjust-types.0...v6.43.1) (2024-08-09)

## [6.43.1-ID2-0000-adjust-types.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.43.0...v6.43.1-ID2-0000-adjust-types.0) (2024-08-09)


### Bug Fixes

* **Pulldowns:** adjust types for icon ([1185d75](https://bitbucket.org/synetics/i-doit-ui-kit/commits/1185d759b0c923bfe7449347a62a8e32935b2cfd))

# [6.43.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.43.0-ID2-0000-add-warning-banner.0...v6.43.0) (2024-08-06)

# [6.43.0-ID2-0000-add-warning-banner.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.42.15...v6.43.0-ID2-0000-add-warning-banner.0) (2024-08-06)


### Features

* **Banner:** add warning banner ([d29b9f2](https://bitbucket.org/synetics/i-doit-ui-kit/commits/d29b9f2678062d9d1b2693d803d15d78f47c63aa))

## [6.42.15](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.42.14...v6.42.15) (2024-08-02)


### Bug Fixes

* **icons:** add icon ([08a3b96](https://bitbucket.org/synetics/i-doit-ui-kit/commits/08a3b965e55521460ccfd1bf137abc087c4380b0))

## [6.42.14](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.42.14-ID2-0000-scroll-list-for-group.1...v6.42.14) (2024-08-02)

## [6.42.14-ID2-0000-scroll-list-for-group.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.42.14-ID2-0000-scroll-list-for-group.0...v6.42.14-ID2-0000-scroll-list-for-group.1) (2024-08-01)

## [6.42.14-ID2-0000-scroll-list-for-group.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.42.13...v6.42.14-ID2-0000-scroll-list-for-group.0) (2024-08-01)


### Bug Fixes

* **Pulldowns:** add scroll list to groups ([3fae5f9](https://bitbucket.org/synetics/i-doit-ui-kit/commits/3fae5f980d0e84ad1b5a1f32f589de4ef9dd6698))

## [6.42.13](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.42.12...v6.42.13) (2024-07-15)


### Bug Fixes

* **story:** migrate buttongroup story from mdx to tsx ([c32ccf4](https://bitbucket.org/synetics/i-doit-ui-kit/commits/c32ccf467aa6de0e082a0a3fd9a9831bb4f3a1e9))

## [6.42.12](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.42.11...v6.42.12) (2024-07-04)


### Bug Fixes

* **Pulldowns:** add id to pulldowns ([e0f3bf1](https://bitbucket.org/synetics/i-doit-ui-kit/commits/e0f3bf1d987891f3700fad49e0e9a9fe885cb980))

## [6.42.11](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.42.9-ID2-0000-change-to-controlled-state.1...v6.42.11) (2024-07-04)

## [6.42.9-ID2-0000-change-to-controlled-state.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.42.9-ID2-0000-change-to-controlled-state.0...v6.42.9-ID2-0000-change-to-controlled-state.1) (2024-07-03)

## [6.42.9-ID2-0000-change-to-controlled-state.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.42.8...v6.42.9-ID2-0000-change-to-controlled-state.0) (2024-07-03)


### Bug Fixes

* **MultiSelect:** change to controlledState ([b528654](https://bitbucket.org/synetics/i-doit-ui-kit/commits/b52865469c4043f4792527e1a78382a118d5807e))

## [6.42.8](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.42.7...v6.42.8) (2024-06-06)

## [6.42.7](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.42.6...v6.42.7) (2024-06-04)


### Bug Fixes

* **icons:** add icons ([669dc41](https://bitbucket.org/synetics/i-doit-ui-kit/commits/669dc41e658be7ae839b70f8de508fc4f8601e8c))

## [6.42.6](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.42.5...v6.42.6) (2024-05-24)

## [6.42.5](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.42.5-task-ID2-0000-use-cloud-registry.0...v6.42.5) (2024-05-17)

## [6.42.5-task-ID2-0000-use-cloud-registry.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.42.4...v6.42.5-task-ID2-0000-use-cloud-registry.0) (2024-04-25)

## [6.42.4](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.42.3...v6.42.4) (2024-04-25)



## [6.41.13-task-ID2-0000-use-cloud-registry.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.42.3...v6.42.4) (2024-04-15)



## [6.41.13-task-ID2-0000-use-cloud-registry.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.42.3...v6.42.4) (2024-04-11)

## [6.42.3](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.42.3-ID2-0000-update-dependencies.0...v6.42.3) (2024-04-22)

## [6.42.3-ID2-0000-update-dependencies.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.42.2...v6.42.3-ID2-0000-update-dependencies.0) (2024-04-19)



## [6.41.10-ID2-0000-update-dependencies.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.42.2...v6.42.3-ID2-0000-update-dependencies.0) (2024-03-20)


### Bug Fixes

* **dependencies:** update packages ([33fc3a6](https://bitbucket.org/synetics/i-doit-ui-kit/commits/33fc3a6139b6fa5af7db39f8004127a786c52886))

## [6.42.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.42.1...v6.42.2) (2024-04-18)


### Bug Fixes

* **story:** add example tsx story ([3abe589](https://bitbucket.org/synetics/i-doit-ui-kit/commits/3abe589adf9a192ba86893772422a1c93da2f29a))

## [6.42.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.41.13...v6.42.1) (2024-04-16)

## [6.41.13](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.41.12-ID2-4893-activate-search-highlighting-by-default-for-comboboxes.1...v6.41.13) (2024-04-16)



## [6.41.12](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.41.12-ID2-4893-activate-search-highlighting-by-default-for-comboboxes.1...v6.41.13) (2024-04-11)


### Bug Fixes

* **tag:** adjust tag style and stories ([c6ef224](https://bitbucket.org/synetics/i-doit-ui-kit/commits/c6ef224ff40982d960ab1845f4242e32e57ed24d))

# [6.42.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.41.12...v6.42.0) (2024-04-15)


### Features

* **icons:** add add-on icon ([2a165e7](https://bitbucket.org/synetics/i-doit-ui-kit/commits/2a165e7ff0496d88bd1d5a69e6afd07daa352b6b))

## [6.41.12](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.41.11...v6.41.12) (2024-04-11)


### Bug Fixes

* **tag:** adjust tag style and stories ([c6ef224](https://bitbucket.org/synetics/i-doit-ui-kit/commits/c6ef224ff40982d960ab1845f4242e32e57ed24d))

## [6.41.12-ID2-4893-activate-search-highlighting-by-default-for-comboboxes.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.41.12-ID2-4893-activate-search-highlighting-by-default-for-comboboxes.0...v6.41.12-ID2-4893-activate-search-highlighting-by-default-for-comboboxes.1) (2024-04-11)

## [6.41.12-ID2-4893-activate-search-highlighting-by-default-for-comboboxes.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.41.11...v6.41.12-ID2-4893-activate-search-highlighting-by-default-for-comboboxes.0) (2024-04-11)


### Bug Fixes

* **ComboBoxes:** activated higlight, bug fixes ([e433b27](https://bitbucket.org/synetics/i-doit-ui-kit/commits/e433b2767b11911b2fb5c2fa23ed6ddd1c1bb742))

## [6.41.11](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.41.11-ID2-0000-adjust-select-in-list.1...v6.41.11) (2024-03-22)

## [6.41.11-ID2-0000-adjust-select-in-list.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.41.11-ID2-0000-adjust-select-in-list.0...v6.41.11-ID2-0000-adjust-select-in-list.1) (2024-03-21)

## [6.41.11-ID2-0000-adjust-select-in-list.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.41.10...v6.41.11-ID2-0000-adjust-select-in-list.0) (2024-03-21)


### Bug Fixes

* **SelectInList:** add view ([43d0b29](https://bitbucket.org/synetics/i-doit-ui-kit/commits/43d0b2921f8216195d36a1f93025ac7fd6433398))

## [6.41.10](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.41.9...v6.41.10) (2024-03-20)


### Bug Fixes

* **icons:** add icons ([0f1f94f](https://bitbucket.org/synetics/i-doit-ui-kit/commits/0f1f94fc0ecc1f57a8df9eb7ee9eb9a88f6f8ae6))

## [6.41.10-ID2-0000-update-dependencies.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.41.9...v6.41.10-ID2-0000-update-dependencies.0) (2024-03-20)


### Bug Fixes

* **dependencies:** update packages ([33fc3a6](https://bitbucket.org/synetics/i-doit-ui-kit/commits/33fc3a6139b6fa5af7db39f8004127a786c52886))

## [6.41.9](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.41.9-ID2-0000-adjust-imports.0...v6.41.9) (2024-03-18)

## [6.41.9-ID2-0000-adjust-imports.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.41.8...v6.41.9-ID2-0000-adjust-imports.0) (2024-03-18)


### Bug Fixes

* **Banner:** adjust imports ([7ee6e8b](https://bitbucket.org/synetics/i-doit-ui-kit/commits/7ee6e8bf98a9b7b2884b42642dec3d1e738b6479))

## [6.41.8](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.41.8-task-ID2-5030-remark-directives-and-other-reqs.1...v6.41.8) (2024-03-15)

## [6.41.8-task-ID2-5030-remark-directives-and-other-reqs.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.41.8-task-ID2-5030-remark-directives-and-other-reqs.0...v6.41.8-task-ID2-5030-remark-directives-and-other-reqs.1) (2024-03-13)

## [6.41.8-task-ID2-5030-remark-directives-and-other-reqs.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.41.7...v6.41.8-task-ID2-5030-remark-directives-and-other-reqs.0) (2024-03-13)



## [6.41.7](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.41.7...v6.41.8-task-ID2-5030-remark-directives-and-other-reqs.0) (2024-03-12)



## [6.41.7-ID2-0000-add-ref-to-rendercontrol.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.41.7...v6.41.8-task-ID2-5030-remark-directives-and-other-reqs.0) (2024-03-12)


### Bug Fixes

* **Field:** add ref to FieldGroup ([d1784dc](https://bitbucket.org/synetics/i-doit-ui-kit/commits/d1784dc4168228c5e220cb4d144805b95c1ed40e))



## [6.41.7-task-ID2-5030-remark-directives-and-other-reqs.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.41.7...v6.41.8-task-ID2-5030-remark-directives-and-other-reqs.0) (2024-03-12)

## [6.41.7](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.41.7-ID2-0000-add-ref-to-rendercontrol.0...v6.41.7) (2024-03-12)

## [6.41.7-ID2-0000-add-ref-to-rendercontrol.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.41.6...v6.41.7-ID2-0000-add-ref-to-rendercontrol.0) (2024-03-12)


### Bug Fixes

* **Field:** add ref to FieldGroup ([d1784dc](https://bitbucket.org/synetics/i-doit-ui-kit/commits/d1784dc4168228c5e220cb4d144805b95c1ed40e))

## [6.41.6](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.41.5...v6.41.6) (2024-03-12)


### Bug Fixes

* **Pulldown:** set previous color in pulldown ([400b5b4](https://bitbucket.org/synetics/i-doit-ui-kit/commits/400b5b48a04ecf16d1d83913bbd88cb1b34e372c))

## [6.41.5](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.41.4...v6.41.5) (2024-03-12)



## [6.41.2-ID2-0000-focustrap-initial-focus.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.41.4...v6.41.5) (2024-03-07)


### Bug Fixes

* **FocusTrap:** add possability to change initialFocus ([2efedc8](https://bitbucket.org/synetics/i-doit-ui-kit/commits/2efedc835c81670b311251dbb2705fb8e8e0762c))

## [6.41.4](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.41.3...v6.41.4) (2024-03-11)

## [6.41.3](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.41.3-task-ID2-0000-alignment-icons.0...v6.41.3) (2024-03-11)

## [6.41.3-task-ID2-0000-alignment-icons.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.41.2...v6.41.3-task-ID2-0000-alignment-icons.0) (2024-03-11)

## [6.41.2-ID2-0000-focustrap-initial-focus.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.41.1...v6.41.2-ID2-0000-focustrap-initial-focus.0) (2024-03-07)


### Bug Fixes

* **FocusTrap:** add possibility to change initialFocus ([2efedc8](https://bitbucket.org/synetics/i-doit-ui-kit/commits/2efedc835c81670b311251dbb2705fb8e8e0762c))

## [6.41.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.41.2-task-ID2-0000-use-popper-class-in-button-pulldown.0...v6.41.2) (2024-03-08)

## [6.41.2-task-ID2-0000-use-popper-class-in-button-pulldown.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.41.1...v6.41.2-task-ID2-0000-use-popper-class-in-button-pulldown.0) (2024-03-07)

## [6.41.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.41.0...v6.41.1) (2024-03-06)


### Bug Fixes

* **SingleSelectCombobox:** do not highlight entries after selection ([14b8236](https://bitbucket.org/synetics/i-doit-ui-kit/commits/14b82364edc3f380b478c5feaad7debdc4edb8e2))

# [6.41.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.41.0-task-react-18.3...v6.41.0) (2024-03-04)

# [6.41.0-task-react-18.3](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.41.0-task-react-18.2...v6.41.0-task-react-18.3) (2024-02-23)

# [6.41.0-task-react-18.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.41.0-task-react-18.1...v6.41.0-task-react-18.2) (2024-02-18)

# [6.41.0-task-react-18.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.41.0-task-react-18.0...v6.41.0-task-react-18.1) (2024-02-17)

# [6.41.0-task-react-18.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.40.2...v6.41.0-task-react-18.0) (2024-02-17)


### Features

* **storybook:** update version of storybook ([b679e03](https://bitbucket.org/synetics/i-doit-ui-kit/commits/b679e03a1962241d0fa119eb18e415b26ca35196))

## [6.40.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.40.2-task-ID2-0000-reformat.0...v6.40.2) (2024-02-16)

## [6.40.2-task-ID2-0000-reformat.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.40.1...v6.40.2-task-ID2-0000-reformat.0) (2024-02-16)



## [6.39.1-task-ID2-0000-reformat.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.40.1...v6.40.2-task-ID2-0000-reformat.0) (2024-02-12)

## [6.40.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.40.1-ID2-0000-LinkHoverCell.1...v6.40.1) (2024-02-16)

## [6.40.1-ID2-0000-LinkHoverCell.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.40.1-ID2-0000-LinkHoverCell.0...v6.40.1-ID2-0000-LinkHoverCell.1) (2024-02-14)

## [6.40.1-ID2-0000-LinkHoverCell.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.40.0...v6.40.1-ID2-0000-LinkHoverCell.0) (2024-02-13)


### Bug Fixes

* **LinkHoverCell:** add show on hover cell ([005fd25](https://bitbucket.org/synetics/i-doit-ui-kit/commits/005fd259cd0d718ede9ba632141dcb869ef059df))

# [6.40.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.39.0...v6.40.0) (2024-02-13)


### Features

* **MultiSelect:** add collapsed tags ([70c7c6d](https://bitbucket.org/synetics/i-doit-ui-kit/commits/70c7c6d83c83af0393762e039f13046eaf7b6967))

# [6.39.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.39.0-ID2-0000-DnD-Components.5...v6.39.0) (2024-02-08)

# [6.39.0-ID2-0000-DnD-Components.5](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.39.0-ID2-0000-use-scroller.1...v6.39.0-ID2-0000-DnD-Components.5) (2024-02-08)

# [6.39.0-ID2-0000-use-scroller.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.39.0-ID2-0000-use-scroller.0...v6.39.0-ID2-0000-use-scroller.1) (2024-02-06)

# [6.39.0-ID2-0000-use-scroller.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.39.0-ID2-0000-DnD-Components.4...v6.39.0-ID2-0000-use-scroller.0) (2024-02-02)

# [6.39.0-ID2-0000-DnD-Components.4](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.39.0-ID2-0000-DnD-Components.3...v6.39.0-ID2-0000-DnD-Components.4) (2024-02-01)



## [6.38.9](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.39.0-ID2-0000-DnD-Components.3...v6.39.0-ID2-0000-DnD-Components.4) (2024-01-26)



## [6.38.9-task-ID2-4966-add-star-outline-icon.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.39.0-ID2-0000-DnD-Components.3...v6.39.0-ID2-0000-DnD-Components.4) (2024-01-26)



## [6.38.9-task-ID2-4966-add-star-outline-icon.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.39.0-ID2-0000-DnD-Components.3...v6.39.0-ID2-0000-DnD-Components.4) (2024-01-26)

# [6.39.0-ID2-0000-DnD-Components.3](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.39.0-ID2-0000-DnD-Components.2...v6.39.0-ID2-0000-DnD-Components.3) (2024-02-01)

# [6.39.0-ID2-0000-DnD-Components.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.39.0-ID2-0000-DnD-Components.1...v6.39.0-ID2-0000-DnD-Components.2) (2024-02-01)

# [6.39.0-ID2-0000-DnD-Components.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.39.0-ID2-0000-DnD-Components.0...v6.39.0-ID2-0000-DnD-Components.1) (2024-02-01)

# [6.39.0-ID2-0000-DnD-Components.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.38.8...v6.39.0-ID2-0000-DnD-Components.0) (2024-01-26)


### Features

* **DnD:** add new DnD components ([4bfddfb](https://bitbucket.org/synetics/i-doit-ui-kit/commits/4bfddfb3562cda8225793b247469f7bc6bb3a245))

## [6.38.9](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.38.9-task-ID2-4966-add-star-outline-icon.1...v6.38.9) (2024-01-26)

## [6.38.9-task-ID2-4966-add-star-outline-icon.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.38.9-task-ID2-4966-add-star-outline-icon.0...v6.38.9-task-ID2-4966-add-star-outline-icon.1) (2024-01-26)

## [6.38.9-task-ID2-4966-add-star-outline-icon.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.38.8...v6.38.9-task-ID2-4966-add-star-outline-icon.0) (2024-01-26)

## [6.38.8](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.38.8-task-ID2-4947-disallow-usage-of-html-in-md.0...v6.38.8) (2024-01-22)

## [6.38.8-task-ID2-4947-disallow-usage-of-html-in-md.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.38.7...v6.38.8-task-ID2-4947-disallow-usage-of-html-in-md.0) (2024-01-19)


### Bug Fixes

* remove html from md interpretation ([b3a11f8](https://bitbucket.org/synetics/i-doit-ui-kit/commits/b3a11f8c98100bc0d25970e769a8cdddcfeb0577))

## [6.38.7](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.38.7-ID2-0000-banner-body.0...v6.38.7) (2024-01-18)

## [6.38.7-ID2-0000-banner-body.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.38.6...v6.38.7-ID2-0000-banner-body.0) (2024-01-17)


### Bug Fixes

* **Banner:** fix min height ([d4d6c23](https://bitbucket.org/synetics/i-doit-ui-kit/commits/d4d6c23736a10e7a3d192a8da3adbdbb05780e70))

## [6.38.6](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.38.6-ID2-0000-drawer-close-backdrop.1...v6.38.6) (2024-01-16)

## [6.38.6-ID2-0000-drawer-close-backdrop.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.38.6-ID2-0000-drawer-close-backdrop.0...v6.38.6-ID2-0000-drawer-close-backdrop.1) (2024-01-16)

## [6.38.6-ID2-0000-drawer-close-backdrop.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.38.5...v6.38.6-ID2-0000-drawer-close-backdrop.0) (2024-01-16)


### Bug Fixes

* **Drawer:** close on click on backdrop ([aba6ac4](https://bitbucket.org/synetics/i-doit-ui-kit/commits/aba6ac48f088be833bcd3b202b90ee224eba99e8))

## [6.38.5](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.38.4...v6.38.5) (2024-01-16)


### Bug Fixes

* **InlinePopover:** fix children props ([57fb9d4](https://bitbucket.org/synetics/i-doit-ui-kit/commits/57fb9d4cbe497d9014eaae14d3a3a9b1d1e2a70f))

## [6.38.4](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.38.3...v6.38.4) (2024-01-15)


### Bug Fixes

* **Markdown:** change naming ([bd06e98](https://bitbucket.org/synetics/i-doit-ui-kit/commits/bd06e98256504ffe232b42ceb411fb9708ec6061))

## [6.38.3](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.38.3-ID2-0000-tooltip-placement-overflow.0...v6.38.3) (2024-01-11)

## [6.38.3-ID2-0000-tooltip-placement-overflow.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.38.2...v6.38.3-ID2-0000-tooltip-placement-overflow.0) (2024-01-11)


### Bug Fixes

* **OverflowWithTooltip:** add tooltip placement prop ([dce8898](https://bitbucket.org/synetics/i-doit-ui-kit/commits/dce889803f118835dfec7a7dc38946547547cc14))

## [6.38.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.38.2-ID2-00000-use-fill-prop-for-icon-text-cell.0...v6.38.2) (2024-01-09)

## [6.38.2-ID2-00000-use-fill-prop-for-icon-text-cell.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.38.1...v6.38.2-ID2-00000-use-fill-prop-for-icon-text-cell.0) (2024-01-09)

## [6.38.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.38.0...v6.38.1) (2024-01-02)


### Bug Fixes

* **table:** fix cell wrapper focus state ([90ffaa1](https://bitbucket.org/synetics/i-doit-ui-kit/commits/90ffaa17e8e9e2b17af84dc4a83738a3a74e9777))



## [6.37.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.38.0...v6.38.1) (2023-12-22)



## [6.37.2-fix-modal-error-children.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.38.0...v6.38.1) (2023-12-21)


### Bug Fixes

* **ModalError:** fix props type ([72e378b](https://bitbucket.org/synetics/i-doit-ui-kit/commits/72e378b57ea8c303f4f19ca83f4a371a5e53ccb2))

## [6.37.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.37.2-fix-modal-error-children.0...v6.37.2) (2023-12-22)

## [6.37.2-fix-modal-error-children.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.37.1...v6.37.2-fix-modal-error-children.0) (2023-12-21)


### Bug Fixes

* **ModalError:** fix props type ([72e378b](https://bitbucket.org/synetics/i-doit-ui-kit/commits/72e378b57ea8c303f4f19ca83f4a371a5e53ccb2))

## [6.37.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.37.0...v6.37.1) (2023-12-19)


# [6.38.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.38.0-task-ID2-4812-markdown-editor.2...v6.38.0) (2023-12-22)

# [6.38.0-task-ID2-4812-markdown-editor.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.38.0-task-ID2-4812-markdown-editor.1...v6.38.0-task-ID2-4812-markdown-editor.2) (2023-12-21)

# [6.38.0-task-ID2-4812-markdown-editor.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.38.0-task-ID2-4812-markdown-editor.0...v6.38.0-task-ID2-4812-markdown-editor.1) (2023-12-20)

# [6.38.0-task-ID2-4812-markdown-editor.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.37.1...v6.38.0-task-ID2-4812-markdown-editor.0) (2023-12-20)



# [6.37.0-task-ID2-4812-markdown-editor.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.37.1...v6.38.0-task-ID2-4812-markdown-editor.0) (2023-12-18)



# [6.37.0-task-ID2-4812-markdown-editor.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.37.1...v6.38.0-task-ID2-4812-markdown-editor.0) (2023-12-18)



# [6.37.0-task-ID2-4812-markdown-editor.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.37.1...v6.38.0-task-ID2-4812-markdown-editor.0) (2023-12-13)


### Features

* **markdown:** add markdown editor and viewer ([826c1d4](https://bitbucket.org/synetics/i-doit-ui-kit/commits/826c1d4195aea5dd62ffdb207204f2581a270730))

## [6.37.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.37.0...v6.37.1) (2023-12-19)



## [6.36.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.37.0...v6.37.1) (2023-12-19)

# [6.37.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.36.0...v6.37.0) (2023-12-19)


### Features

* **MultiSelectComboBox:** creat an Emails Field story ([d45e24c](https://bitbucket.org/synetics/i-doit-ui-kit/commits/d45e24c4ea6d6dac0e65b0b940a12b9b9309a27e))

# [6.36.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.35.2...v6.36.0) (2023-12-01)


### Features

* **icons:** add arrow icons ([7379534](https://bitbucket.org/synetics/i-doit-ui-kit/commits/737953412938500742d270f6910205559b1ecc00))

## [6.35.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.35.1...v6.35.2) (2023-11-30)


### Bug Fixes

* **SingleSelectCombobox:** fix highlighted index ([918d943](https://bitbucket.org/synetics/i-doit-ui-kit/commits/918d94395c4fac4319154f6f8cc042a62064ecd3))

## [6.35.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.35.0...v6.35.1) (2023-11-29)

# [6.35.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.34.0...v6.35.0) (2023-11-29)


### Features

* **SingleSelectCombobox:** add autosuggestion ([5d7a215](https://bitbucket.org/synetics/i-doit-ui-kit/commits/5d7a21533a7024ec0e2043f5b4ed679cdda78dbb))

# [6.34.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.33.1...v6.34.0) (2023-11-29)


### Features

* **FuzzyHighlight:** add FuzzyHighlight ([115eaa6](https://bitbucket.org/synetics/i-doit-ui-kit/commits/115eaa68af83b91475de849285d2df4eb304f38c))

## [6.33.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.32.5-task-ID2-3804-update-colors-in-storybook.0...v6.33.1) (2023-11-23)

## [6.32.5-task-ID2-3804-update-colors-in-storybook.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.32.4...v6.32.5-task-ID2-3804-update-colors-in-storybook.0) (2023-11-23)



## [6.32.4](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.32.4...v6.32.5-task-ID2-3804-update-colors-in-storybook.0) (2023-11-17)



## [6.32.4-task-ID2-0000-fix-types.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.32.4...v6.32.5-task-ID2-3804-update-colors-in-storybook.0) (2023-11-10)



## [6.32.4-task-ID2-0000-fix-types.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.32.4...v6.32.5-task-ID2-3804-update-colors-in-storybook.0) (2023-11-10)


### Bug Fixes

* **types:** fix types ([4d97a57](https://bitbucket.org/synetics/i-doit-ui-kit/commits/4d97a57168ef2fd0d8c3999b48a94aa251d88d40))



# [6.33.0-task-ID2-3804-update-colors-in-storybook.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.32.4...v6.32.5-task-ID2-3804-update-colors-in-storybook.0) (2023-11-20)



# [6.33.0-task-ID2-3804-update-colors-in-storybook.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.32.4...v6.32.5-task-ID2-3804-update-colors-in-storybook.0) (2023-11-20)


### Features

* **colors:** update colors ([631a087](https://bitbucket.org/synetics/i-doit-ui-kit/commits/631a087bcaba96eb8d5671b6d7b815f17b6ac513))

# [6.33.0-task-ID2-3804-update-colors-in-storybook.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.33.0-task-ID2-3804-update-colors-in-storybook.0...v6.33.0-task-ID2-3804-update-colors-in-storybook.1) (2023-11-20)

# [6.33.0-task-ID2-3804-update-colors-in-storybook.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.32.3...v6.33.0-task-ID2-3804-update-colors-in-storybook.0) (2023-11-20)


### Features

* **colors:** update colors ([631a087](https://bitbucket.org/synetics/i-doit-ui-kit/commits/631a087bcaba96eb8d5671b6d7b815f17b6ac513))

## [6.32.4](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.32.4-task-ID2-0000-fix-types.1...v6.32.4) (2023-11-17)

## [6.32.4-task-ID2-0000-fix-types.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.32.4-task-ID2-0000-fix-types.0...v6.32.4-task-ID2-0000-fix-types.1) (2023-11-10)

## [6.32.4-task-ID2-0000-fix-types.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.32.3...v6.32.4-task-ID2-0000-fix-types.0) (2023-11-10)


### Bug Fixes

* **types:** fix types ([4d97a57](https://bitbucket.org/synetics/i-doit-ui-kit/commits/4d97a57168ef2fd0d8c3999b48a94aa251d88d40))

## [6.32.3](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.32.3-task-ID2-4754-multi-combobox-reloads-on-open.0...v6.32.3) (2023-11-10)

## [6.32.3-task-ID2-4754-multi-combobox-reloads-on-open.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.32.2-task-ID2-4754-multi-combobox-reloads-on-open.2...v6.32.3-task-ID2-4754-multi-combobox-reloads-on-open.0) (2023-11-10)



## [6.32.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.32.2-task-ID2-4754-multi-combobox-reloads-on-open.2...v6.32.3-task-ID2-4754-multi-combobox-reloads-on-open.0) (2023-11-10)



## [6.32.2-task-ID2-4172-use-node-20.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.32.2-task-ID2-4754-multi-combobox-reloads-on-open.2...v6.32.3-task-ID2-4754-multi-combobox-reloads-on-open.0) (2023-11-03)


### Bug Fixes

* **node:** support node 20 and npm 10 ([0ab2ca2](https://bitbucket.org/synetics/i-doit-ui-kit/commits/0ab2ca26a37d539729c57abdb6c411afcbd03f2f))

## [6.32.2-task-ID2-4754-multi-combobox-reloads-on-open.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.32.2-task-ID2-4754-multi-combobox-reloads-on-open.1...v6.32.2-task-ID2-4754-multi-combobox-reloads-on-open.2) (2023-11-10)

## [6.32.2-task-ID2-4754-multi-combobox-reloads-on-open.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.32.2-task-ID2-4754-multi-combobox-reloads-on-open.0...v6.32.2-task-ID2-4754-multi-combobox-reloads-on-open.1) (2023-11-08)

## [6.32.2-task-ID2-4754-multi-combobox-reloads-on-open.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.32.1...v6.32.2-task-ID2-4754-multi-combobox-reloads-on-open.0) (2023-11-08)


### Bug Fixes

* **multi-select-combobox:** prevent change handler to be triggered on menu open ([cf6d63d](https://bitbucket.org/synetics/i-doit-ui-kit/commits/cf6d63d1129da591627d0e4e2abe631a71fa5019))

## [6.32.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.32.2-task-ID2-4172-use-node-20.0...v6.32.2) (2023-11-10)

## [6.32.2-task-ID2-4172-use-node-20.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.32.1...v6.32.2-task-ID2-4172-use-node-20.0) (2023-11-03)


### Bug Fixes

* **node:** support node 20 and npm 10 ([0ab2ca2](https://bitbucket.org/synetics/i-doit-ui-kit/commits/0ab2ca26a37d539729c57abdb6c411afcbd03f2f))

## [6.32.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.32.1-task-ID2-0000-fix-type.0...v6.32.1) (2023-10-26)

## [6.32.1-task-ID2-0000-fix-type.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.32.0...v6.32.1-task-ID2-0000-fix-type.0) (2023-10-26)


### Bug Fixes

* **ButtonPulldown:** fix children type ([822fdc4](https://bitbucket.org/synetics/i-doit-ui-kit/commits/822fdc4ddc98a53049860ed004c61724e8a1eef5))

# [6.32.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.31.0...v6.32.0) (2023-10-24)


### Features

* **icons:** add sorting icons ([5e11c1f](https://bitbucket.org/synetics/i-doit-ui-kit/commits/5e11c1fed366bfff3eab13638072d400a92dc81f))

# [6.31.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.29.1-task-ID2-0000-adjust-item-style.14...v6.31.0) (2023-10-24)



# [6.30.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.29.1-task-ID2-0000-adjust-item-style.14...v6.31.0) (2023-10-13)


### Features

* **mainNavigation:** create a mainNavigation component ([f2f0b18](https://bitbucket.org/synetics/i-doit-ui-kit/commits/f2f0b18ef8b8bf2e5f7862be2654d0047f2e2868))

# [6.30.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.29.0...v6.30.0) (2023-10-13)


### Features

* **mainNavigation:** create a mainNavigation component ([f2f0b18](https://bitbucket.org/synetics/i-doit-ui-kit/commits/f2f0b18ef8b8bf2e5f7862be2654d0047f2e2868))

## [6.29.1-task-ID2-0000-adjust-item-style.14](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.29.1-task-ID2-0000-adjust-item-style.13...v6.29.1-task-ID2-0000-adjust-item-style.14) (2023-10-20)

## [6.29.1-task-ID2-0000-adjust-item-style.13](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.29.1-task-ID2-0000-adjust-item-style.12...v6.29.1-task-ID2-0000-adjust-item-style.13) (2023-10-20)

## [6.29.1-task-ID2-0000-adjust-item-style.12](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.29.1-task-ID2-0000-adjust-item-style.11...v6.29.1-task-ID2-0000-adjust-item-style.12) (2023-10-18)

## [6.29.1-task-ID2-0000-adjust-item-style.11](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.29.1-task-ID2-0000-adjust-item-style.10...v6.29.1-task-ID2-0000-adjust-item-style.11) (2023-10-18)

## [6.29.1-task-ID2-0000-adjust-item-style.10](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.29.1-task-ID2-0000-adjust-item-style.9...v6.29.1-task-ID2-0000-adjust-item-style.10) (2023-10-18)

## [6.29.1-task-ID2-0000-adjust-item-style.9](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.29.1-task-ID2-0000-adjust-item-style.8...v6.29.1-task-ID2-0000-adjust-item-style.9) (2023-10-13)

## [6.29.1-task-ID2-0000-adjust-item-style.8](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.29.1-task-ID2-0000-adjust-item-style.7...v6.29.1-task-ID2-0000-adjust-item-style.8) (2023-10-13)

## [6.29.1-task-ID2-0000-adjust-item-style.7](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.29.1-task-ID2-0000-adjust-item-style.6...v6.29.1-task-ID2-0000-adjust-item-style.7) (2023-10-11)

## [6.29.1-task-ID2-0000-adjust-item-style.6](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.29.1-task-ID2-0000-adjust-item-style.5...v6.29.1-task-ID2-0000-adjust-item-style.6) (2023-10-10)

## [6.29.1-task-ID2-0000-adjust-item-style.5](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.29.1-task-ID2-0000-adjust-item-style.4...v6.29.1-task-ID2-0000-adjust-item-style.5) (2023-10-10)

## [6.29.1-task-ID2-0000-adjust-item-style.4](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.29.1-task-ID2-0000-adjust-item-style.3...v6.29.1-task-ID2-0000-adjust-item-style.4) (2023-10-10)

## [6.29.1-task-ID2-0000-adjust-item-style.3](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.29.1-task-ID2-0000-adjust-item-style.2...v6.29.1-task-ID2-0000-adjust-item-style.3) (2023-10-06)

## [6.29.1-task-ID2-0000-adjust-item-style.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.29.1-task-ID2-0000-adjust-item-style.1...v6.29.1-task-ID2-0000-adjust-item-style.2) (2023-10-06)

## [6.29.1-task-ID2-0000-adjust-item-style.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.29.1-task-ID2-0000-adjust-item-style.0...v6.29.1-task-ID2-0000-adjust-item-style.1) (2023-10-06)

## [6.29.1-task-ID2-0000-adjust-item-style.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.29.0...v6.29.1-task-ID2-0000-adjust-item-style.0) (2023-10-06)


### Bug Fixes

* **Item:** adjust styles ([aea86bb](https://bitbucket.org/synetics/i-doit-ui-kit/commits/aea86bbeebaea19c857985463a5b7b3184f2048e))

# [6.29.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.29.0-task-ID2-4714-implement-single-select-list-in-ui-kit.1...v6.29.0) (2023-10-04)

# [6.29.0-task-ID2-4714-implement-single-select-list-in-ui-kit.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.29.0-task-ID2-4714-implement-single-select-list-in-ui-kit.0...v6.29.0-task-ID2-4714-implement-single-select-list-in-ui-kit.1) (2023-09-28)

# [6.29.0-task-ID2-4714-implement-single-select-list-in-ui-kit.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.28.0...v6.29.0-task-ID2-4714-implement-single-select-list-in-ui-kit.0) (2023-09-28)


### Features

* **SingleSelectList:** add SingleSelectList ([9199ebd](https://bitbucket.org/synetics/i-doit-ui-kit/commits/9199ebd39c40883bde77129e7c6e8debfb41702f))

# [6.28.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.27.1...v6.28.0) (2023-09-15)


### Features

* **PulldownTag:** add PulldownTag ([a51d2be](https://bitbucket.org/synetics/i-doit-ui-kit/commits/a51d2be911f5e1bb0909d5266005ae5e488df5c2))

## [6.27.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.27.1-bug-ID2-0000-add-children.2...v6.27.1) (2023-09-11)

## [6.27.1-bug-ID2-0000-add-children.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.27.1-bug-ID2-0000-add-children.1...v6.27.1-bug-ID2-0000-add-children.2) (2023-09-11)

## [6.27.1-bug-ID2-0000-add-children.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.27.1-bug-ID2-0000-add-children.0...v6.27.1-bug-ID2-0000-add-children.1) (2023-09-11)

## [6.27.1-bug-ID2-0000-add-children.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.27.0...v6.27.1-bug-ID2-0000-add-children.0) (2023-09-11)


### Bug Fixes

* **types:** add children ([7f44446](https://bitbucket.org/synetics/i-doit-ui-kit/commits/7f44446e6fbd670787a0a672449df456a956c2b4))

# [6.27.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.26.0...v6.27.0) (2023-09-08)


### Features

* **AvatarPulldown:** add AvatarPulldown ([716680f](https://bitbucket.org/synetics/i-doit-ui-kit/commits/716680f63e56fe7d8864d2aa8252c8853155fb9c))

# [6.26.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.25.0...v6.26.0) (2023-09-01)


### Features

* **ButtonPulldown:** add Sidebar and Content components ([fc9fe55](https://bitbucket.org/synetics/i-doit-ui-kit/commits/fc9fe5500f748486ee404e53b952e02fdc994d86))

# [6.25.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.25.0-task-ID2-4644-Select-whole-text-when-user-focuses-combobox.0...v6.25.0) (2023-09-01)

# [6.25.0-task-ID2-4644-Select-whole-text-when-user-focuses-combobox.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.24.0-task-ID2-4644-Select-whole-text-when-user-focuses-combobox.1...v6.25.0-task-ID2-4644-Select-whole-text-when-user-focuses-combobox.0) (2023-08-30)



# [6.24.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.24.0-task-ID2-4644-Select-whole-text-when-user-focuses-combobox.1...v6.25.0-task-ID2-4644-Select-whole-text-when-user-focuses-combobox.0) (2023-08-30)


### Features

* **Sidebar:** add Sidebar and Content components ([c028323](https://bitbucket.org/synetics/i-doit-ui-kit/commits/c028323c1c56dae210cf341a91b466e9eee6faf2))

# [6.24.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.23.0...v6.24.0) (2023-08-30)


### Features

* **Sidebar:** add Sidebar and Content components ([c028323](https://bitbucket.org/synetics/i-doit-ui-kit/commits/c028323c1c56dae210cf341a91b466e9eee6faf2))

# [6.23.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.22.1...v6.23.0) (2023-08-28)


### Features

* **Drawer:** add Drawer ([6df9f7a](https://bitbucket.org/synetics/i-doit-ui-kit/commits/6df9f7a1da67d07aee774d254ed44b6b30843e7d))

## [6.22.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.22.0...v6.22.1) (2023-08-24)


### Bug Fixes

* **eslint:** add new import rules ([21005d2](https://bitbucket.org/synetics/i-doit-ui-kit/commits/21005d2da79f343db1794092a43272a8dafe260e))

# [6.22.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.22.0-task-ID2-4556-allow-searching-for-tenant-by-title-in-tenant-switcher.3...v6.22.0) (2023-08-21)

# [6.22.0-task-ID2-4556-allow-searching-for-tenant-by-title-in-tenant-switcher.3](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.22.0-task-ID2-4556-allow-searching-for-tenant-by-title-in-tenant-switcher.2...v6.22.0-task-ID2-4556-allow-searching-for-tenant-by-title-in-tenant-switcher.3) (2023-08-17)

# [6.22.0-task-ID2-4556-allow-searching-for-tenant-by-title-in-tenant-switcher.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.22.0-task-ID2-4556-allow-searching-for-tenant-by-title-in-tenant-switcher.1...v6.22.0-task-ID2-4556-allow-searching-for-tenant-by-title-in-tenant-switcher.2) (2023-08-17)

# [6.22.0-task-ID2-4556-allow-searching-for-tenant-by-title-in-tenant-switcher.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.22.0-task-ID2-4556-allow-searching-for-tenant-by-title-in-tenant-switcher.0...v6.22.0-task-ID2-4556-allow-searching-for-tenant-by-title-in-tenant-switcher.1) (2023-08-17)

# [6.22.0-task-ID2-4556-allow-searching-for-tenant-by-title-in-tenant-switcher.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.21.3...v6.22.0-task-ID2-4556-allow-searching-for-tenant-by-title-in-tenant-switcher.0) (2023-08-16)


### Features

* **SearchItem:** add SearchItem ([39f2ba1](https://bitbucket.org/synetics/i-doit-ui-kit/commits/39f2ba1175e1dadb84bfb4201fd8f24dee7d8ee1))

## [6.21.3](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.21.2...v6.21.3) (2023-08-08)


### Bug Fixes

* **table:** add zIndex to fixed header ([c9dcc5a](https://bitbucket.org/synetics/i-doit-ui-kit/commits/c9dcc5a3d858fc95cdc0465583891b5a38349d71))

## [6.21.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.21.2-bug-ID2-0000-add-selected-state.1...v6.21.2) (2023-08-07)

## [6.21.2-bug-ID2-0000-add-selected-state.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.21.2-bug-ID2-0000-add-selected-state.0...v6.21.2-bug-ID2-0000-add-selected-state.1) (2023-08-07)

## [6.21.2-bug-ID2-0000-add-selected-state.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.21.1...v6.21.2-bug-ID2-0000-add-selected-state.0) (2023-08-04)


### Bug Fixes

* **row-view:** add selected state ([53b867c](https://bitbucket.org/synetics/i-doit-ui-kit/commits/53b867cbe77ff63490df2acbde746e02856670b2))

## [6.21.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.21.0...v6.21.1) (2023-07-26)


### Bug Fixes

* **item:** adjust types ([46088d1](https://bitbucket.org/synetics/i-doit-ui-kit/commits/46088d1dc5220f5a477351f805c860146cbe0f35))

# [6.21.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.21.0-task-ID2-4552-create-main-navbar-selector-in-ui-kit.1...v6.21.0) (2023-07-21)

# [6.21.0-task-ID2-4552-create-main-navbar-selector-in-ui-kit.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.21.0-task-ID2-4552-create-main-navbar-selector-in-ui-kit.0...v6.21.0-task-ID2-4552-create-main-navbar-selector-in-ui-kit.1) (2023-07-21)

# [6.21.0-task-ID2-4552-create-main-navbar-selector-in-ui-kit.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.20.0...v6.21.0-task-ID2-4552-create-main-navbar-selector-in-ui-kit.0) (2023-07-20)


### Features

* **navigation-item:** add NavigationItem ([c3095a4](https://bitbucket.org/synetics/i-doit-ui-kit/commits/c3095a4913e2e4aab911cffead69afcce7928ec0))

# [6.20.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.20.0-fix-ID2-4099-Allow-user-to-resize-sidebar-with-button-in-network-view.2...v6.20.0) (2023-07-20)

# [6.20.0-fix-ID2-4099-Allow-user-to-resize-sidebar-with-button-in-network-view.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.20.0-fix-ID2-4099-Allow-user-to-resize-sidebar-with-button-in-network-view.1...v6.20.0-fix-ID2-4099-Allow-user-to-resize-sidebar-with-button-in-network-view.2) (2023-07-20)

# [6.20.0-fix-ID2-4099-Allow-user-to-resize-sidebar-with-button-in-network-view.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.20.0-fix-ID2-4099-Allow-user-to-resize-sidebar-with-button-in-network-view.0...v6.20.0-fix-ID2-4099-Allow-user-to-resize-sidebar-with-button-in-network-view.1) (2023-07-19)

# [6.20.0-fix-ID2-4099-Allow-user-to-resize-sidebar-with-button-in-network-view.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.19.0...v6.20.0-fix-ID2-4099-Allow-user-to-resize-sidebar-with-button-in-network-view.0) (2023-07-19)


### Features

* **resize:** add handleComponent ([fa2c53f](https://bitbucket.org/synetics/i-doit-ui-kit/commits/fa2c53f3e12f1cd1055a75ff9f67be3c277e81f0))

# [6.19.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.19.0-task-ID2-0000-add-input-value-and-clear-option.4...v6.19.0) (2023-07-18)

# [6.19.0-task-ID2-0000-add-input-value-and-clear-option.4](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.19.0-task-ID2-0000-add-input-value-and-clear-option.3...v6.19.0-task-ID2-0000-add-input-value-and-clear-option.4) (2023-07-17)

# [6.19.0-task-ID2-0000-add-input-value-and-clear-option.3](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.19.0-task-ID2-0000-add-input-value-and-clear-option.2...v6.19.0-task-ID2-0000-add-input-value-and-clear-option.3) (2023-07-14)

# [6.19.0-task-ID2-0000-add-input-value-and-clear-option.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.19.0-task-ID2-0000-add-input-value-and-clear-option.1...v6.19.0-task-ID2-0000-add-input-value-and-clear-option.2) (2023-07-14)

# [6.19.0-task-ID2-0000-add-input-value-and-clear-option.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.19.0-task-ID2-0000-add-input-value-and-clear-option.0...v6.19.0-task-ID2-0000-add-input-value-and-clear-option.1) (2023-07-14)

# [6.19.0-task-ID2-0000-add-input-value-and-clear-option.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.18.1...v6.19.0-task-ID2-0000-add-input-value-and-clear-option.0) (2023-07-12)


### Features

* **combobox-singleselect:** add value for input and prop for clear option after item selection ([8f50a5e](https://bitbucket.org/synetics/i-doit-ui-kit/commits/8f50a5ebd8fbf49342d476698ce9c8c866bb0736))

## [6.18.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.18.1-fix-ID2-4493-Correct-report-table-header-line.1...v6.18.1) (2023-07-10)

## [6.18.1-fix-ID2-4493-Correct-report-table-header-line.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.18.1-fix-ID2-4493-Correct-report-table-header-line.0...v6.18.1-fix-ID2-4493-Correct-report-table-header-line.1) (2023-07-07)

## [6.18.1-fix-ID2-4493-Correct-report-table-header-line.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.18.0...v6.18.1-fix-ID2-4493-Correct-report-table-header-line.0) (2023-07-07)



## [6.16.1-fix-ID2-4493-Correct-report-table-header-line.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.18.0...v6.18.1-fix-ID2-4493-Correct-report-table-header-line.0) (2023-06-28)


### Bug Fixes

* **table:** remove display blook ([5999ea1](https://bitbucket.org/synetics/i-doit-ui-kit/commits/5999ea1bbe7fb262d6651ffb10cae876419d0fd7))

# [6.18.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.17.0...v6.18.0) (2023-06-29)


### Features

* **menu:** update menu container size ([50bda0d](https://bitbucket.org/synetics/i-doit-ui-kit/commits/50bda0dfee9f44a188d4eb2a24055f9dcb8832b1))

# [6.17.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.16.0...v6.17.0) (2023-06-28)


### Features

* **menu:** add scroll stories ([b7f53fa](https://bitbucket.org/synetics/i-doit-ui-kit/commits/b7f53faea4d68498b66f4027aefb80ed7927de93))

## [6.16.1-fix-ID2-4493-Correct-report-table-header-line.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.16.0...v6.16.1-fix-ID2-4493-Correct-report-table-header-line.0) (2023-06-28)


### Bug Fixes

* **table:** remove display blook ([5999ea1](https://bitbucket.org/synetics/i-doit-ui-kit/commits/5999ea1bbe7fb262d6651ffb10cae876419d0fd7))

# [6.16.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.15.0...v6.16.0) (2023-06-22)


### Features

* **menu:** adjust heading and add story with groups ([4af297c](https://bitbucket.org/synetics/i-doit-ui-kit/commits/4af297cc05d5b0cdc5fef46680ded448ef0a24c7))

# [6.15.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.14.2...v6.15.0) (2023-06-14)


### Features

* **menu:** updaate menu subitem and add selection ([e7b1a67](https://bitbucket.org/synetics/i-doit-ui-kit/commits/e7b1a678dc81bfd1b25159144681350ae24eac3d))

## [6.14.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.14.2-bug-ID2-0000-add-focus-state-clear.0...v6.14.2) (2023-06-07)

## [6.14.2-bug-ID2-0000-add-focus-state-clear.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.14.1...v6.14.2-bug-ID2-0000-add-focus-state-clear.0) (2023-06-07)


### Bug Fixes

* **SearchField:** fix focus state after reset ([8c8e43d](https://bitbucket.org/synetics/i-doit-ui-kit/commits/8c8e43d9704654d5f7dc23afb28ff97a26d19b82))

## [6.14.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.14.1-bug-ID2-0000-add-classname-collapse-summary.0...v6.14.1) (2023-06-02)

## [6.14.1-bug-ID2-0000-add-classname-collapse-summary.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.14.0...v6.14.1-bug-ID2-0000-add-classname-collapse-summary.0) (2023-06-02)


### Bug Fixes

* **CollapseSummart:** add className ([76d3634](https://bitbucket.org/synetics/i-doit-ui-kit/commits/76d363420377343e35ad7e1f0e813e352f26458b))

# [6.14.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.14.0-feature-ID2-3806-add-icons.0...v6.14.0) (2023-05-31)

# [6.14.0-feature-ID2-3806-add-icons.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.13.2...v6.14.0-feature-ID2-3806-add-icons.0) (2023-05-31)


### Features

* **icons:** add new icons ([bee7ec0](https://bitbucket.org/synetics/i-doit-ui-kit/commits/bee7ec0d1583633614e817aab11da54995af6b58))

## [6.13.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.13.2-bug-ID2-0000-add-props-children.0...v6.13.2) (2023-05-23)

## [6.13.2-bug-ID2-0000-add-props-children.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.13.1...v6.13.2-bug-ID2-0000-add-props-children.0) (2023-05-22)


### Bug Fixes

* **LoadingScreen:** children props ([42cfbbd](https://bitbucket.org/synetics/i-doit-ui-kit/commits/42cfbbdd4fff54e30a51e5794be00d6220bb61f5))

## [6.13.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.13.1-fix-ID2-4375-Add-calendar-input-change-callback.1...v6.13.1) (2023-05-16)

## [6.13.1-fix-ID2-4375-Add-calendar-input-change-callback.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.13.0...v6.13.1-fix-ID2-4375-Add-calendar-input-change-callback.1) (2023-05-16)



## [6.12.2-fix-ID2-4375-Add-calendar-input-change-callback.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.13.0...v6.13.1-fix-ID2-4375-Add-calendar-input-change-callback.1) (2023-05-16)


### Bug Fixes

* **calendar:** add on input change callback ([2316a9c](https://bitbucket.org/synetics/i-doit-ui-kit/commits/2316a9c45d7492fd1a2edcefb4588562cc8ce1ed))

# [6.13.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.12.1...v6.13.0) (2023-05-16)


### Features

* **tree:** implement the tree structure ([7126cdc](https://bitbucket.org/synetics/i-doit-ui-kit/commits/7126cdcc7a73bee3e48a2ae29d582d423147967b))

## [6.12.2-fix-ID2-4375-Add-calendar-input-change-callback.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.12.1...v6.12.2-fix-ID2-4375-Add-calendar-input-change-callback.0) (2023-05-16)


### Bug Fixes

* **calendar:** add on input change callback ([2316a9c](https://bitbucket.org/synetics/i-doit-ui-kit/commits/2316a9c45d7492fd1a2edcefb4588562cc8ce1ed))

## [6.12.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.12.1-task-ID2-0000-add-props-children.0...v6.12.1) (2023-05-08)

## [6.12.1-task-ID2-0000-add-props-children.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.12.0...v6.12.1-task-ID2-0000-add-props-children.0) (2023-05-08)


### Bug Fixes

* **OverflowWithTooltip:** children props ([7caee90](https://bitbucket.org/synetics/i-doit-ui-kit/commits/7caee90c836d6a97c8f933b3619ed3d195fb86be))

# [6.12.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.11.2...v6.12.0) (2023-05-02)


### Features

* **menu:** updaate menu item ([6ac56fb](https://bitbucket.org/synetics/i-doit-ui-kit/commits/6ac56fb120c63fb77b84cfab26913029f6fe8892))

## [6.11.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.11.2-bug-ID2-4329-radio-zoom.0...v6.11.2) (2023-05-01)

## [6.11.2-bug-ID2-4329-radio-zoom.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.11.1...v6.11.2-bug-ID2-4329-radio-zoom.0) (2023-05-01)


### Bug Fixes

* **radio_zoom:** fix merge conflicts in kit ([1f3f88f](https://bitbucket.org/synetics/i-doit-ui-kit/commits/1f3f88f9505b7a0d784b32097bccce52370dd067))

## [6.11.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.11.1-bug-ID2-3876-select-date.3...v6.11.1) (2023-04-28)


### Bug Fixes

* **select_date:** check seleted date in range ([dc176b6](https://bitbucket.org/synetics/i-doit-ui-kit/commits/dc176b6d4a258dcf645bf0cbe27f0bf118504003))

## [6.11.1-bug-ID2-3876-select-date.3](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.11.1-bug-ID2-3876-select-date.2...v6.11.1-bug-ID2-3876-select-date.3) (2023-04-24)


### Bug Fixes

* **select_date:** check seleted date in range ([b63fb99](https://bitbucket.org/synetics/i-doit-ui-kit/commits/b63fb9945e7e80feba281fd90497d50d57f83a76))

## [6.11.1-bug-ID2-3876-select-date.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.11.1-bug-ID2-3876-select-date.1...v6.11.1-bug-ID2-3876-select-date.2) (2023-04-24)


### Bug Fixes

* **select_date:** check seleted date in range ([53e1a8d](https://bitbucket.org/synetics/i-doit-ui-kit/commits/53e1a8dafcbac483de7dfbc945b6710451ecb969))

## [6.11.1-bug-ID2-3876-select-date.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.11.1-bug-ID2-3876-select-date.0...v6.11.1-bug-ID2-3876-select-date.1) (2023-04-24)


### Bug Fixes

* **select_date:** check seleted date in range ([d1fb969](https://bitbucket.org/synetics/i-doit-ui-kit/commits/d1fb969d704339cf216ce7752c3004333f59e80e))

## [6.11.1-bug-ID2-3876-select-date.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.11.0...v6.11.1-bug-ID2-3876-select-date.0) (2023-04-24)


### Bug Fixes

* **select_date:** check seleted date in range ([83578b3](https://bitbucket.org/synetics/i-doit-ui-kit/commits/83578b382da0788af2b2d20354ccf15ce7291ff9))
* **select_date:** check seleted date in range ([5559c63](https://bitbucket.org/synetics/i-doit-ui-kit/commits/5559c63c46ecbc25aaec0f61ba6979259fa20589))

# [6.11.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.10.4...v6.11.0) (2023-04-21)


### Features

* **fixed_header:** add overflow to general styles ([dcfac50](https://bitbucket.org/synetics/i-doit-ui-kit/commits/dcfac506e6b7c5a74fb436a86fdbef4141661e64))
* **fixed_header:** adjust table view add scrollers, view examples ([5b58017](https://bitbucket.org/synetics/i-doit-ui-kit/commits/5b5801780beea00ef6b4e436885a96743ae8110a))
* **fixed_header:** fixed header example in storybook ([6c1ed15](https://bitbucket.org/synetics/i-doit-ui-kit/commits/6c1ed1523e6d4062cc6a0ce1b75d09cc20a82a5d))

## [6.10.10-fix-ID2-4354-autofocus-text.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.10.9...v6.10.10-fix-ID2-4354-autofocus-text.0) (2023-04-14)


### Bug Fixes

* **autofocus_text:** add autofocus effect on component ([0715edb](https://bitbucket.org/synetics/i-doit-ui-kit/commits/0715edb627e86c614cd522da9143b672715340ca))

## [6.10.9](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.10.9-bug-ID2-4263-dont-reopen.0...v6.10.9) (2023-04-14)

## [6.10.9-bug-ID2-4263-dont-reopen.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.10.8...v6.10.9-bug-ID2-4263-dont-reopen.0) (2023-04-13)


### Bug Fixes

* **dont_reopen:** resolve merge conflicts ([bce217b](https://bitbucket.org/synetics/i-doit-ui-kit/commits/bce217b8a8f8dd7188a371679fbe79e97d1f6359))

## [6.10.8](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.10.8-ID2-4398-fix-icons-on-search.0...v6.10.8) (2023-04-05)

## [6.10.8-ID2-4398-fix-icons-on-search.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.10.7...v6.10.8-ID2-4398-fix-icons-on-search.0) (2023-04-05)


### Bug Fixes

* **combobox:** fix icons on search ([133285d](https://bitbucket.org/synetics/i-doit-ui-kit/commits/133285d9cd9ca830b95ff6fc5a7fb70d43e4c2f4))

## [6.10.7](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.10.7-bug-ID2-4283-check-date.0...v6.10.7) (2023-04-04)

## [6.10.7-bug-ID2-4283-check-date.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.10.6...v6.10.7-bug-ID2-4283-check-date.0) (2023-04-03)


### Bug Fixes

* **check_date:** allow to wirte in date fields resolve conflicts ([0e9366b](https://bitbucket.org/synetics/i-doit-ui-kit/commits/0e9366b3cffdad2e24db0e684aee33eb8fe674b6))
* **check_date:** allow to wirte in date fields resolve conflicts ([ff8d625](https://bitbucket.org/synetics/i-doit-ui-kit/commits/ff8d6256d63197a5425d7b82447f330ad2776949))

## [6.10.6](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.10.5-task-ID2-0000-unset-default-autocomplete.1...v6.10.6) (2023-03-30)



## [6.10.5-task-ID2-0000-unset-default-autocomplete.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.10.5-task-ID2-0000-unset-default-autocomplete.1...v6.10.6) (2023-03-29)



## [6.10.5-task-ID2-0000-unset-default-autocomplete.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.10.5-task-ID2-0000-unset-default-autocomplete.1...v6.10.6) (2023-03-29)


### Bug Fixes

* **input:** unset autocomplete for input ([1fc8d16](https://bitbucket.org/synetics/i-doit-ui-kit/commits/1fc8d16cb799038741501a4762a5ac5337d45ad8))



## [6.10.5](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.10.5-task-ID2-0000-unset-default-autocomplete.1...v6.10.6) (2023-03-29)



## [6.10.5-bug-ID2-4342-fix-time-safari.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.10.5-task-ID2-0000-unset-default-autocomplete.1...v6.10.6) (2023-03-28)


### Bug Fixes

* **fix_time_safari:** resolve merge conflicts ([32eac3c](https://bitbucket.org/synetics/i-doit-ui-kit/commits/32eac3cd77196c10421eb177cae8de43236ccf61))

## [6.10.5](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.10.5-bug-ID2-4342-fix-time-safari.0...v6.10.5) (2023-03-29)

## [6.10.5-bug-ID2-4342-fix-time-safari.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.10.4...v6.10.5-bug-ID2-4342-fix-time-safari.0) (2023-03-28)


### Bug Fixes

* **fix_time_safari:** resolve merge conflicts ([32eac3c](https://bitbucket.org/synetics/i-doit-ui-kit/commits/32eac3cd77196c10421eb177cae8de43236ccf61))

## [6.10.5-task-ID2-0000-unset-default-autocomplete.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.10.5-task-ID2-0000-unset-default-autocomplete.0...v6.10.5-task-ID2-0000-unset-default-autocomplete.1) (2023-03-29)

## [6.10.5-task-ID2-0000-unset-default-autocomplete.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.10.4...v6.10.5-task-ID2-0000-unset-default-autocomplete.0) (2023-03-29)


### Bug Fixes

* **input:** unset autocomplete for input ([1fc8d16](https://bitbucket.org/synetics/i-doit-ui-kit/commits/1fc8d16cb799038741501a4762a5ac5337d45ad8))

## [6.10.4](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.10.3...v6.10.4) (2023-03-27)

## [6.10.4-bug-ID2-3943-dont-close-pulldown.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.10.3...v6.10.4-bug-ID2-3943-dont-close-pulldown.0) (2023-03-27)


### Bug Fixes

* **dont_close_pulldown:** fix event handler in downshift ([3b8fb1a](https://bitbucket.org/synetics/i-doit-ui-kit/commits/3b8fb1a7c10725fc022def7b57c0217acb745789))

## [6.10.3](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.10.3-task-ID2-0000-close-button.1...v6.10.3) (2023-03-27)

## [6.10.3-task-ID2-0000-close-button.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.10.3-task-ID2-0000-close-button.0...v6.10.3-task-ID2-0000-close-button.1) (2023-03-23)

## [6.10.3-task-ID2-0000-close-button.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.10.2...v6.10.3-task-ID2-0000-close-button.0) (2023-03-23)


### Bug Fixes

* **modal:** close onKeydown ([e4c83a4](https://bitbucket.org/synetics/i-doit-ui-kit/commits/e4c83a4541ceb713066bdae22673037ef4f6c977))

## [6.10.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.10.1...v6.10.2) (2023-03-21)

## [6.10.2-bug-ID2-4323-calendar-styles.3](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.10.2-bug-ID2-4323-calendar-styles.2...v6.10.2-bug-ID2-4323-calendar-styles.3) (2023-03-21)


### Bug Fixes

* **calendar_styles:** resolve merge conflicts ([5adb3a6](https://bitbucket.org/synetics/i-doit-ui-kit/commits/5adb3a69b361cb16d9386df7fd53536440d0a157))

## [6.10.2-bug-ID2-4323-calendar-styles.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.10.2-bug-ID2-4323-calendar-styles.1...v6.10.2-bug-ID2-4323-calendar-styles.2) (2023-03-21)


### Bug Fixes

* **calendar_styles:** resolve merge conflicts ([140d42c](https://bitbucket.org/synetics/i-doit-ui-kit/commits/140d42cfdf14f31a8c20809fcda64fd7e661014b))

## [6.10.2-bug-ID2-4323-calendar-styles.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.10.2-bug-ID2-4323-calendar-styles.0...v6.10.2-bug-ID2-4323-calendar-styles.1) (2023-03-21)


### Bug Fixes

* **calendar_styles:** resolve merge conflicts ([7ebdf68](https://bitbucket.org/synetics/i-doit-ui-kit/commits/7ebdf68a551221c3458a42dd0f623f57f4a1c158))

## [6.10.2-bug-ID2-4323-calendar-styles.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.10.1...v6.10.2-bug-ID2-4323-calendar-styles.0) (2023-03-21)


### Bug Fixes

* **calendar_styles:** resolve merge conflicts ([4f047da](https://bitbucket.org/synetics/i-doit-ui-kit/commits/4f047da7b29e23137102437717bd3bac6b33d640))

## [6.10.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.10.1-task-ID2-0000-export-type.0...v6.10.1) (2023-03-21)

## [6.10.1-task-ID2-0000-export-type.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.10.0...v6.10.1-task-ID2-0000-export-type.0) (2023-03-20)


### Bug Fixes

* **types:** export types ([67bd7da](https://bitbucket.org/synetics/i-doit-ui-kit/commits/67bd7daec0a04004ded3e4eafc52321fd1497e92))

# [6.10.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.9.4...v6.10.0) (2023-03-20)



# [6.10.0-task-ID2-4336-abbreviation-pulldowns.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.9.4...v6.10.0) (2023-03-15)


### Features

* **pulldowns:** add abbreviations for pulldowns ([b5988b6](https://bitbucket.org/synetics/i-doit-ui-kit/commits/b5988b611a0f47e69c111e19b06a7ed7b7c39980))

# [6.10.0-task-ID2-4336-abbreviation-pulldowns.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.9.2...v6.10.0-task-ID2-4336-abbreviation-pulldowns.0) (2023-03-15)


### Features

* **pulldowns:** add abbreviations for pulldowns ([b5988b6](https://bitbucket.org/synetics/i-doit-ui-kit/commits/b5988b611a0f47e69c111e19b06a7ed7b7c39980))

## [6.9.4](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.9.4-bug-ID2-1260-fix-multiselect-keys.0...v6.9.4) (2023-03-17)

## [6.9.4-bug-ID2-1260-fix-multiselect-keys.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.9.3...v6.9.4-bug-ID2-1260-fix-multiselect-keys.0) (2023-03-17)


### Bug Fixes

* **calendar_styles:** check calendar styles ([c16c3d4](https://bitbucket.org/synetics/i-doit-ui-kit/commits/c16c3d453e56540be589610e1756cac439f9a0bc))
* **fix_multiselect_keys:** resolve merge conflicts in pr ([414cf0e](https://bitbucket.org/synetics/i-doit-ui-kit/commits/414cf0e392d727e0e9b33aad1206b832cdbea11b))
* **fix_multiselect_keys:** resolve merge conflicts in pr ([ba288a8](https://bitbucket.org/synetics/i-doit-ui-kit/commits/ba288a86ab703290ff3de26eb4792ce32ad2bab1))

## [6.9.3](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.9.3-bug-ID2-2867-sass-division.0...v6.9.3) (2023-03-16)

## [6.9.3-bug-ID2-2867-sass-division.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.9.2...v6.9.3-bug-ID2-2867-sass-division.0) (2023-03-16)


### Bug Fixes

* use math.div instead of division by slash ([a1ace34](https://bitbucket.org/synetics/i-doit-ui-kit/commits/a1ace34ecfaee1391189159459ec87e83f1f7c46))

## [6.9.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.9.1...v6.9.2) (2023-03-06)


### Bug Fixes

* **combobox_keys:** make changes to resolve conflict over released ([1fe8fe4](https://bitbucket.org/synetics/i-doit-ui-kit/commits/1fe8fe44340676e504afcfa1d64cf205da75709d))

## [6.9.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.9.0...v6.9.1) (2023-03-06)


### Bug Fixes

* **form-fields:** fix uncovered cases ([84b3e85](https://bitbucket.org/synetics/i-doit-ui-kit/commits/84b3e85d58a243b933e72b402c878d6f4924761e))

# [6.9.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.8.11...v6.9.0) (2023-03-06)



# [6.9.0-feature-form-fields.5](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.8.11...v6.9.0) (2023-03-03)



# [6.9.0-feature-form-fields.4](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.8.11...v6.9.0) (2023-03-03)



# [6.9.0-feature-form-fields.3](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.8.11...v6.9.0) (2023-03-03)



# [6.9.0-feature-form-fields.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.8.11...v6.9.0) (2023-03-02)



# [6.9.0-feature-form-fields.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.8.11...v6.9.0) (2023-03-02)



# [6.9.0-feature-form-fields.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.8.11...v6.9.0) (2023-03-02)


### Features

* **form-fields:** added the form fields component with a helper hook ([bf66030](https://bitbucket.org/synetics/i-doit-ui-kit/commits/bf660306cb12906b97c7a8766cab14909ff176d2))

## [6.8.11](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.8.10-bug-ID2-4079-allow-user-to-save-with-single-click.2...v6.8.11) (2023-03-06)



## [6.8.10](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.8.10-bug-ID2-4079-allow-user-to-save-with-single-click.2...v6.8.11) (2023-03-03)



## [6.8.10-bug-ID-1259-pulldown-keys.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.8.10-bug-ID2-4079-allow-user-to-save-with-single-click.2...v6.8.11) (2023-03-02)


### Bug Fixes

* **pulldown_keys:** resolve merge conflicts ([655979b](https://bitbucket.org/synetics/i-doit-ui-kit/commits/655979b78b9c0a26d7e2b5ed122527ce2bdf0d8a))

## [6.8.10](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.8.10-bug-ID-1259-pulldown-keys.0...v6.8.10) (2023-03-03)

## [6.8.10-bug-ID-1259-pulldown-keys.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.8.9...v6.8.10-bug-ID-1259-pulldown-keys.0) (2023-03-02)


### Bug Fixes

* **pulldown_keys:** resolve merge conflicts ([655979b](https://bitbucket.org/synetics/i-doit-ui-kit/commits/655979b78b9c0a26d7e2b5ed122527ce2bdf0d8a))

## [6.8.10-bug-ID2-4079-allow-user-to-save-with-single-click.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.8.10-bug-ID2-4079-allow-user-to-save-with-single-click.1...v6.8.10-bug-ID2-4079-allow-user-to-save-with-single-click.2) (2023-03-06)

## [6.8.10-bug-ID2-4079-allow-user-to-save-with-single-click.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.8.10-bug-ID2-4079-allow-user-to-save-with-single-click.0...v6.8.10-bug-ID2-4079-allow-user-to-save-with-single-click.1) (2023-03-02)

## [6.8.10-bug-ID2-4079-allow-user-to-save-with-single-click.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.8.9...v6.8.10-bug-ID2-4079-allow-user-to-save-with-single-click.0) (2023-03-02)


### Bug Fixes

* **calendar:** change onBlur to useFocusOut ([a85b7de](https://bitbucket.org/synetics/i-doit-ui-kit/commits/a85b7de6578c5439a2819bbdcadc580a519e020a))

## [6.8.9](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.8.9-fix-ID2-3598-fix-date.0...v6.8.9) (2023-03-02)

## [6.8.9-fix-ID2-3598-fix-date.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.8.8...v6.8.9-fix-ID2-3598-fix-date.0) (2023-03-02)


### Bug Fixes

* **fix_date:** remove default set ([f5c82e1](https://bitbucket.org/synetics/i-doit-ui-kit/commits/f5c82e10a892ea347a83387d0e093b3ba79eedb3))

## [6.8.8](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.8.8-bug-ID2-3931-fix-placeholder-position.1...v6.8.8) (2023-03-01)

## [6.8.8-bug-ID2-3931-fix-placeholder-position.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.8.8-bug-ID2-3931-fix-placeholder-position.0...v6.8.8-bug-ID2-3931-fix-placeholder-position.1) (2023-03-01)

## [6.8.8-bug-ID2-3931-fix-placeholder-position.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.8.7...v6.8.8-bug-ID2-3931-fix-placeholder-position.0) (2023-03-01)


### Bug Fixes

* **textArea:** fix placeholder position ([facdc4c](https://bitbucket.org/synetics/i-doit-ui-kit/commits/facdc4c6e3d62d35c3d8550f5fd6de218404f793))

## [6.8.7](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.8.7-fix-ID2-3664-fix-width.0...v6.8.7) (2023-02-15)

## [6.8.7-fix-ID2-3664-fix-width.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.8.6...v6.8.7-fix-ID2-3664-fix-width.0) (2023-02-13)


### Bug Fixes

* **fix_width:** add hasBottomBorder prop ([0532952](https://bitbucket.org/synetics/i-doit-ui-kit/commits/053295299cbe0e7c9850c221df37d7ec47609424))

## [6.8.6](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.8.5...v6.8.6) (2023-02-13)

## [6.8.1-fix-ID2-4225-input-number.8](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.8.1-fix-ID2-4225-input-number.7...v6.8.1-fix-ID2-4225-input-number.8) (2023-02-10)

### Bug Fixes

- **input_number:** return to previous version ([72150d8](https://bitbucket.org/synetics/i-doit-ui-kit/commits/72150d8e3ed430069d2595166eb5ad4a48efd278))

## [6.8.1-fix-ID2-4225-input-number.7](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.8.1-fix-ID2-4225-input-number.6...v6.8.1-fix-ID2-4225-input-number.7) (2023-02-10)

### Bug Fixes

- **input_number:** return to previous version ([df01e64](https://bitbucket.org/synetics/i-doit-ui-kit/commits/df01e643d462b3d885676756f50a2314d1e48710))

## [6.8.1-fix-ID2-4225-input-number.6](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.8.1-fix-ID2-4225-input-number.5...v6.8.1-fix-ID2-4225-input-number.6) (2023-02-09)

### Bug Fixes

- **input_number:** test regex input ([661be71](https://bitbucket.org/synetics/i-doit-ui-kit/commits/661be71e43d436aea4be9c152cca5b2d8446ee32))

## [6.8.1-fix-ID2-4225-input-number.5](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.8.1-fix-ID2-4225-input-number.4...v6.8.1-fix-ID2-4225-input-number.5) (2023-02-09)

### Bug Fixes

- **input_number:** test regex input ([1421b1e](https://bitbucket.org/synetics/i-doit-ui-kit/commits/1421b1ea6abd460210d91cb4a5780ccdc51d700c))

## [6.8.1-fix-ID2-4225-input-number.4](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.8.1-fix-ID2-4225-input-number.3...v6.8.1-fix-ID2-4225-input-number.4) (2023-02-09)

### Bug Fixes

- **input_number:** test regex input ([d107dc3](https://bitbucket.org/synetics/i-doit-ui-kit/commits/d107dc33d9bb6fdde480ee4485277d4487f08169))
- **input_number:** test regex input ([7afc62c](https://bitbucket.org/synetics/i-doit-ui-kit/commits/7afc62c7b39fe97f636d6012608a8eb61ec9d862))

## [6.8.1-fix-ID2-4225-input-number.3](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.8.2...v6.8.1-fix-ID2-4225-input-number.3) (2023-02-09)

### Bug Fixes

- **input_number:** test regex input ([6d9b3f4](https://bitbucket.org/synetics/i-doit-ui-kit/commits/6d9b3f4e607bd628c6c7498b22ccb5afd2913fc9))

## [6.8.1-fix-ID2-4225-input-number.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.8.2...v6.8.1-fix-ID2-4225-input-number.3) (2023-02-03)

### Bug Fixes

- **input_number:** add test for input negative ([41191c6](https://bitbucket.org/synetics/i-doit-ui-kit/commits/41191c69fec239b9999621f5dfd00369ba1a5418))

## [6.8.1-fix-ID2-4225-input-number.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.8.2...v6.8.1-fix-ID2-4225-input-number.3) (2023-02-02)

### Bug Fixes

- **input_number:** check fill negative number ([301f267](https://bitbucket.org/synetics/i-doit-ui-kit/commits/301f267a5ed32461bce39dc7da211cdd1da0d1f1))

## [6.8.1-fix-ID2-4225-input-number.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.8.2...v6.8.1-fix-ID2-4225-input-number.3) (2023-02-02)

### Bug Fixes

- **input_number:** check fill negative number ([b4837e4](https://bitbucket.org/synetics/i-doit-ui-kit/commits/b4837e4a17855a9744f5d35e3ddee8e56cb3d5ac))

## [6.8.1-fix-ID2-4225-input-number.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.8.1-fix-ID2-4225-input-number.1...v6.8.1-fix-ID2-4225-input-number.2) (2023-02-03)

### Bug Fixes

- **input_number:** add test for input negative ([41191c6](https://bitbucket.org/synetics/i-doit-ui-kit/commits/41191c69fec239b9999621f5dfd00369ba1a5418))

## [6.8.1-fix-ID2-4225-input-number.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.8.1-fix-ID2-4225-input-number.0...v6.8.1-fix-ID2-4225-input-number.1) (2023-02-02)

### Bug Fixes

- **input_number:** check fill negative number ([301f267](https://bitbucket.org/synetics/i-doit-ui-kit/commits/301f267a5ed32461bce39dc7da211cdd1da0d1f1))

## [6.8.1-fix-ID2-4225-input-number.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.8.0...v6.8.1-fix-ID2-4225-input-number.0) (2023-02-02)

### Bug Fixes

- **input_number:** check fill negative number ([b4837e4](https://bitbucket.org/synetics/i-doit-ui-kit/commits/b4837e4a17855a9744f5d35e3ddee8e56cb3d5ac))

## [6.8.1-bug-ID2-0000-fix-duplicate-selected-items.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.8.0...v6.8.1-bug-ID2-0000-fix-duplicate-selected-items.0) (2023-02-03)

### Bug Fixes

- **combobox:** fix duplication of selected items ([26d96ee](https://bitbucket.org/synetics/i-doit-ui-kit/commits/26d96ee5edac42d31c207ef62ffa75d30b49373b))
- **combobox:** fix duplication of selected items ([27d4381](https://bitbucket.org/synetics/i-doit-ui-kit/commits/27d4381ababd4ca2e553c299389427b4fa3217ce))

# [6.8.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.7.0...v6.8.0) (2023-01-16)

# [6.6.0-task-ID2-0000-input-onChange-calendar.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.7.0...v6.8.0) (2022-12-29)

# [6.6.0-task-ID2-0000-input-onChange-calendar.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.7.0...v6.8.0) (2022-12-28)

### Features

- **calendar:** add on change in input ([e9905a6](https://bitbucket.org/synetics/i-doit-ui-kit/commits/e9905a63f4a5482fba49d273899411ef6dda2510))

# [6.7.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.7.0-task-ID2-4152-loading-multi.1...v6.7.0) (2023-01-12)

### Features

- **loading_multi:** set isOpen state ([84fb973](https://bitbucket.org/synetics/i-doit-ui-kit/commits/84fb973e9a985c35682abd963f7e0fe988130fd7))

## [6.6.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.7.0-task-ID2-4152-loading-multi.1...v6.7.0) (2023-01-10)

## [6.6.1-bug-ID2-4144-tooltip-wrap.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.7.0-task-ID2-4152-loading-multi.1...v6.7.0) (2023-01-06)

# [6.6.0-task-ID2-0000-input-onChange-calendar.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.6.0-task-ID2-0000-input-onChange-calendar.0...v6.6.0-task-ID2-0000-input-onChange-calendar.1) (2022-12-29)

# [6.6.0-task-ID2-0000-input-onChange-calendar.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.5.4...v6.6.0-task-ID2-0000-input-onChange-calendar.0) (2022-12-28)

### Features

- **calendar:** add on change in input ([e9905a6](https://bitbucket.org/synetics/i-doit-ui-kit/commits/e9905a63f4a5482fba49d273899411ef6dda2510))

## [6.5.4](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.5.3...v6.5.4) (2022-12-28)

### Bug Fixes

- **tooltip_wrap:** pass text classname to fix text wrapping ([201be58](https://bitbucket.org/synetics/i-doit-ui-kit/commits/201be5887c1ad03a495aa111e13d31b672abc6c4))

# [6.7.0-task-ID2-4152-loading-multi.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.7.0-task-ID2-4152-loading-multi.0...v6.7.0-task-ID2-4152-loading-multi.1) (2023-01-10)

### Features

- **loading_multi:** check wrapper ([e2d4f17](https://bitbucket.org/synetics/i-doit-ui-kit/commits/e2d4f173c3701a52943d0a0226d8bebc6cdf4eec))

# [6.7.0-task-ID2-4152-loading-multi.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.6.0...v6.7.0-task-ID2-4152-loading-multi.0) (2023-01-06)

### Features

- **loading_multi:** change type for children ([9390908](https://bitbucket.org/synetics/i-doit-ui-kit/commits/9390908117b0a387935e8e50ac0f9c9bcbfdeb0f))

# [6.6.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.5.4...v6.6.0) (2023-01-04)

# [6.6.0-task-ID2-0000-fix-classname.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.5.4...v6.6.0) (2022-12-23)

### Features

- **fix_classname:** add tooltip classname for styling ([723e655](https://bitbucket.org/synetics/i-doit-ui-kit/commits/723e655cfba0fe9ec2457a97e077e90d12d7c2a4))

# [6.6.0-task-ID2-0000-fix-classname.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.5.3...v6.6.0-task-ID2-0000-fix-classname.0) (2022-12-23)

### Features

- **fix_classname:** add tooltip classname for styling ([723e655](https://bitbucket.org/synetics/i-doit-ui-kit/commits/723e655cfba0fe9ec2457a97e077e90d12d7c2a4))

## [6.5.3](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.5.2-bug-ID2-3895-fix-state.0...v6.5.3) (2022-12-21)

### Bug Fixes

- **fix_state:** increment package version ([f5e69e2](https://bitbucket.org/synetics/i-doit-ui-kit/commits/f5e69e23206a46e93ff91573cd3fab565c6922c8))

## [6.5.2-bug-ID2-3895-fix-state.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.5.2-bug-ID2-3895-fix-state.0...v6.5.3) (2022-12-20)

### Bug Fixes

- **multiobject_view:** fix state propagation ([8d7c0bc](https://bitbucket.org/synetics/i-doit-ui-kit/commits/8d7c0bc8aa4f163e5ce04a022538f1c12f7426cc))
- **multiobject_view:** fix state propagation ([a447e77](https://bitbucket.org/synetics/i-doit-ui-kit/commits/a447e77024e090c5cc04b83f53bf1cedc60603e6))

## [6.5.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.5.2-bug-ID2-3895-fix-state.0...v6.5.3) (2022-12-20)

## [6.5.2-bug-ID2-3895-fix-state.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.5.1...v6.5.2-bug-ID2-3895-fix-state.0) (2022-12-20)

### Bug Fixes

- **multiobject_view:** fix state propagation ([8d7c0bc](https://bitbucket.org/synetics/i-doit-ui-kit/commits/8d7c0bc8aa4f163e5ce04a022538f1c12f7426cc))
- **multiobject_view:** fix state propagation ([a447e77](https://bitbucket.org/synetics/i-doit-ui-kit/commits/a447e77024e090c5cc04b83f53bf1cedc60603e6))

## [6.5.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.5.1-bug-missing-timeformat.0...v6.5.1) (2022-12-19)

## [6.5.1-bug-missing-timeformat.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.5.0...v6.5.1-bug-missing-timeformat.0) (2022-12-19)

### Bug Fixes

- **time:** add missing time format ([0c1c7a9](https://bitbucket.org/synetics/i-doit-ui-kit/commits/0c1c7a9f9988e43b1ac5ca9670a1ae19bf702af1))

# [6.5.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.4.1...v6.5.0) (2022-12-16)

# [6.4.0-task-ID2-0000-overflow-classname-popper.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.4.1...v6.5.0) (2022-12-15)

# [6.4.0-task-ID2-0000-overflow-classname-popper.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.4.1...v6.5.0) (2022-12-15)

### Features

- **overflow:** add class prop for OverflowWithTooltip ([87fcff7](https://bitbucket.org/synetics/i-doit-ui-kit/commits/87fcff796cc2586c3eff762a3087f0bac65766a9))

## [6.4.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.4.0...v6.4.1) (2022-12-16)

## [6.3.1-bug-ID2-3802-multipulldown-one-click-open.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.4.0...v6.4.1) (2022-12-15)

### Bug Fixes

- **combobox:** fix double click open ([d6fead2](https://bitbucket.org/synetics/i-doit-ui-kit/commits/d6fead2fdd3d36894431756ce77da485392c7079))

# [6.4.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.4.0-feat-ID2-3904-tag-icon.17...v6.4.0) (2022-12-15)

# [6.4.0-feat-ID2-3904-tag-icon.17](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.4.0-feat-ID2-3904-tag-icon.16...v6.4.0-feat-ID2-3904-tag-icon.17) (2022-12-15)

### Bug Fixes

- **tag_items:** fix icon with text abbreviation ([fb09cc6](https://bitbucket.org/synetics/i-doit-ui-kit/commits/fb09cc63e9e45b466667b0c37345cef691c5166d))

# [6.4.0-feat-ID2-3904-tag-icon.16](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.4.0-feat-ID2-3904-tag-icon.15...v6.4.0-feat-ID2-3904-tag-icon.16) (2022-12-14)

### Bug Fixes

- **tag_items:** fix icon with text abbreviation ([8705b38](https://bitbucket.org/synetics/i-doit-ui-kit/commits/8705b38492e238b3cd2c672ee080f715f5ff7f6e))

# [6.4.0-feat-ID2-3904-tag-icon.15](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.4.0-feat-ID2-3904-tag-icon.14...v6.4.0-feat-ID2-3904-tag-icon.15) (2022-12-14)

### Bug Fixes

- **tag_items:** fix icon with text abbreviation ([b959f6f](https://bitbucket.org/synetics/i-doit-ui-kit/commits/b959f6f745153a0b4f856407a0935cb35e06a8d7))

# [6.4.0-feat-ID2-3904-tag-icon.14](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.4.0-feat-ID2-3904-tag-icon.13...v6.4.0-feat-ID2-3904-tag-icon.14) (2022-12-12)

### Bug Fixes

- **tag_items:** fix icon view downloading ([2624847](https://bitbucket.org/synetics/i-doit-ui-kit/commits/262484714dc8cb7ca9d1bd5be815eefe9dc792a2))

# [6.4.0-feat-ID2-3904-tag-icon.13](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.4.0-feat-ID2-3904-tag-icon.12...v6.4.0-feat-ID2-3904-tag-icon.13) (2022-12-12)

### Bug Fixes

- **tag_items:** fix icon view downloading ([b832cf2](https://bitbucket.org/synetics/i-doit-ui-kit/commits/b832cf25c818a2a3833625ba3042316166ca4a83))

# [6.4.0-feat-ID2-3904-tag-icon.12](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.4.0-feat-ID2-3904-tag-icon.11...v6.4.0-feat-ID2-3904-tag-icon.12) (2022-12-12)

### Bug Fixes

- **tag_items:** fix icon view downloading ([61455bc](https://bitbucket.org/synetics/i-doit-ui-kit/commits/61455bc4e2c63b8e53de8307c626389963c7e116))

# [6.4.0-feat-ID2-3904-tag-icon.11](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.4.0-feat-ID2-3904-tag-icon.10...v6.4.0-feat-ID2-3904-tag-icon.11) (2022-12-12)

### Bug Fixes

- **tag_items:** fix icon view downloading ([91f07a8](https://bitbucket.org/synetics/i-doit-ui-kit/commits/91f07a81b15aaebd3717c00add3e6f983d2f7b0a))

# [6.4.0-feat-ID2-3904-tag-icon.10](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.4.0-feat-ID2-3904-tag-icon.9...v6.4.0-feat-ID2-3904-tag-icon.10) (2022-12-12)

### Bug Fixes

- **tag_items:** fix icon view downloading ([59c6a3b](https://bitbucket.org/synetics/i-doit-ui-kit/commits/59c6a3b85c17caa4ff6c5ff500d7bc6f2ef00c60))

# [6.4.0-feat-ID2-3904-tag-icon.9](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.4.0-feat-ID2-3904-tag-icon.8...v6.4.0-feat-ID2-3904-tag-icon.9) (2022-12-12)

### Bug Fixes

- **tag_items:** fix icon view downloading ([ecd0f9a](https://bitbucket.org/synetics/i-doit-ui-kit/commits/ecd0f9a8800918527facdf708d026e8bb0ccd97d))

# [6.4.0-feat-ID2-3904-tag-icon.8](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.4.0-feat-ID2-3904-tag-icon.7...v6.4.0-feat-ID2-3904-tag-icon.8) (2022-12-12)

### Bug Fixes

- **tag_items:** fix icon view downloading ([a577249](https://bitbucket.org/synetics/i-doit-ui-kit/commits/a57724980d747cc1eef37d0c0e39ade18bddf32d))

# [6.4.0-feat-ID2-3904-tag-icon.7](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.4.0-feat-ID2-3904-tag-icon.6...v6.4.0-feat-ID2-3904-tag-icon.7) (2022-12-12)

### Bug Fixes

- **tag_items:** fix icon view downloading ([9bffd7d](https://bitbucket.org/synetics/i-doit-ui-kit/commits/9bffd7d9ec78d22af475dc6fd6233994f04a34f1))

# [6.4.0-feat-ID2-3904-tag-icon.6](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.4.0-feat-ID2-3904-tag-icon.5...v6.4.0-feat-ID2-3904-tag-icon.6) (2022-12-12)

### Bug Fixes

- **tag_items:** fix icon view downloading ([487ac21](https://bitbucket.org/synetics/i-doit-ui-kit/commits/487ac218fbbf320232dd08977439666255aa9fb4))

# [6.4.0-feat-ID2-3904-tag-icon.5](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.4.0-feat-ID2-3904-tag-icon.4...v6.4.0-feat-ID2-3904-tag-icon.5) (2022-12-12)

### Bug Fixes

- **tag_items:** fix icon view downloading ([c05b1fb](https://bitbucket.org/synetics/i-doit-ui-kit/commits/c05b1fbc0afaa3e25bdf35c8b02f019d4caa0051))

# [6.4.0-feat-ID2-3904-tag-icon.4](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.4.0-feat-ID2-3904-tag-icon.3...v6.4.0-feat-ID2-3904-tag-icon.4) (2022-12-12)

### Bug Fixes

- **tag_items:** fix icon view downloading ([0b75a53](https://bitbucket.org/synetics/i-doit-ui-kit/commits/0b75a534a393c544934f33a3a43ce14824823ab9))

# [6.4.0-feat-ID2-3904-tag-icon.3](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.4.0-feat-ID2-3904-tag-icon.2...v6.4.0-feat-ID2-3904-tag-icon.3) (2022-12-12)

### Bug Fixes

- **tag_items:** fix icon view downloading ([0ad53b8](https://bitbucket.org/synetics/i-doit-ui-kit/commits/0ad53b8a38948081009e61ff361130465b3deae0))

# [6.4.0-feat-ID2-3904-tag-icon.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.4.0-feat-ID2-3904-tag-icon.1...v6.4.0-feat-ID2-3904-tag-icon.2) (2022-12-12)

### Bug Fixes

- **tag_items:** fix icon view downloading ([025f455](https://bitbucket.org/synetics/i-doit-ui-kit/commits/025f4554ac6200166460d87749c2491502e9097a))
- **tag_items:** fix icon view downloading ([d4096e5](https://bitbucket.org/synetics/i-doit-ui-kit/commits/d4096e5ea83f66bd91bb5db207182461d75ca127))

# [6.4.0-feat-ID2-3904-tag-icon.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.4.0-feat-ID2-3904-tag-icon.0...v6.4.0-feat-ID2-3904-tag-icon.1) (2022-12-12)

### Bug Fixes

- **tag_items:** fix icon view downloading ([97dd755](https://bitbucket.org/synetics/i-doit-ui-kit/commits/97dd7556881e8b251cf41ca94dfe906265bd6b98))
- **tag_items:** fix icon view downloading ([700e7e5](https://bitbucket.org/synetics/i-doit-ui-kit/commits/700e7e58cfc3def62546c2d4961f2c1fac2c195b))

### Features

- **tag_icon:** simplify condition ([4db9f05](https://bitbucket.org/synetics/i-doit-ui-kit/commits/4db9f05add90ef99bbb5891f7c43ecb0e8f4a60a))

# [6.4.0-feat-ID2-3904-tag-icon.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.3.0...v6.4.0-feat-ID2-3904-tag-icon.0) (2022-11-29)

### Features

- **tag_icon:** add view to tags in combobox ([eb87f7a](https://bitbucket.org/synetics/i-doit-ui-kit/commits/eb87f7ada976a64cd8f365fb4dd9f4729ca0e7d0))

# [6.4.0-task-ID2-0000-overflow-classname-popper.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.4.0-task-ID2-0000-overflow-classname-popper.0...v6.4.0-task-ID2-0000-overflow-classname-popper.1) (2022-12-15)

# [6.4.0-task-ID2-0000-overflow-classname-popper.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.3.0...v6.4.0-task-ID2-0000-overflow-classname-popper.0) (2022-12-15)

### Features

- **overflow:** add class prop for OverflowWithTooltip ([87fcff7](https://bitbucket.org/synetics/i-doit-ui-kit/commits/87fcff796cc2586c3eff762a3087f0bac65766a9))

## [6.3.1-bug-ID2-3802-multipulldown-one-click-open.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.3.0...v6.3.1-bug-ID2-3802-multipulldown-one-click-open.0) (2022-12-15)

### Bug Fixes

- **combobox:** fix double click open ([d6fead2](https://bitbucket.org/synetics/i-doit-ui-kit/commits/d6fead2fdd3d36894431756ce77da485392c7079))

# [6.3.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.2.2...v6.3.0) (2022-11-28)

### Features

- **icons:** add time and date time icons ([32d49ee](https://bitbucket.org/synetics/i-doit-ui-kit/commits/32d49eeb4838406f962cf6217a208f61b116d627))

## [6.2.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.2.2-fix-ID2-0000-remove-props.1...v6.2.2) (2022-11-25)

## [6.2.2-fix-ID2-0000-remove-props.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.2.2-fix-ID2-0000-remove-props.0...v6.2.2-fix-ID2-0000-remove-props.1) (2022-11-24)

### Bug Fixes

- **remove_props:** add props for start to build fix ([dea394c](https://bitbucket.org/synetics/i-doit-ui-kit/commits/dea394c9b67c3c5a41bfb99a0d0cb1399ff910c0))

## [6.2.2-fix-ID2-0000-remove-props.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.2.1...v6.2.2-fix-ID2-0000-remove-props.0) (2022-11-24)

### Bug Fixes

- **remove_props:** remove props for build fix ([66d9984](https://bitbucket.org/synetics/i-doit-ui-kit/commits/66d9984966566ec8b28ea4b7ebd55aadb02ab895))

## [6.2.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.2.0...v6.2.1) (2022-11-23)

### Bug Fixes

- **icons:** color for menu and favorite icons ([188c4d7](https://bitbucket.org/synetics/i-doit-ui-kit/commits/188c4d7e7d3d725c55b05b33467ad6a1e6713c92))

# [6.2.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.1.3...v6.2.0) (2022-11-23)

### Features

- **icons:** change standard icon colors ([71af0d9](https://bitbucket.org/synetics/i-doit-ui-kit/commits/71af0d98d932443b627ef7896b24b46b6c0319b4))

## [6.1.3](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.1.2...v6.1.3) (2022-11-22)

### Bug Fixes

- **types:** add children for modal footer and button spacer ([b4ab010](https://bitbucket.org/synetics/i-doit-ui-kit/commits/b4ab010a27a01859559311a6daa96a47b44f9b6a))

## [6.1.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.1.2-fix-tooltips-check.0...v6.1.2) (2022-11-22)

## [6.1.2-fix-tooltips-check.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.1.1...v6.1.2-fix-tooltips-check.0) (2022-11-22)

### Bug Fixes

- **tooltips_check:** task ID2-3773 make release for idoit ([7f0c722](https://bitbucket.org/synetics/i-doit-ui-kit/commits/7f0c722f7f60c30450232b1ce8c5f27c97c05aaa))

## [6.1.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.1.1-fix-ID2-3854-masked-suffix.0...v6.1.1) (2022-11-21)

### Bug Fixes

- **masked_suffix:** adjusted suffix value to password component ([ca7df43](https://bitbucket.org/synetics/i-doit-ui-kit/commits/ca7df4388974ff5483f4a06ff7106bb2ba6bdd55))

## [6.1.1-fix-ID2-3854-masked-suffix.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.1.0...v6.1.1-fix-ID2-3854-masked-suffix.0) (2022-11-16)

### Bug Fixes

- **masked_suffix:** adjusted suffix value to password component ([8ce454c](https://bitbucket.org/synetics/i-doit-ui-kit/commits/8ce454c3051f317451f9747f808004d55bd1fd24))

# [6.1.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.0.3...v6.1.0) (2022-11-15)

### Features

- **icons:** add undo icon ([868ea98](https://bitbucket.org/synetics/i-doit-ui-kit/commits/868ea9828c5341766951b68ef7bf6c49f356d1a2))

## [6.0.3](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.0.1-fix-ID2-3844-keyboard-dnd.10...v6.0.3) (2022-11-12)

### Bug Fixes

- **keyboard_dnd:** adjust package ver ([13c14c1](https://bitbucket.org/synetics/i-doit-ui-kit/commits/13c14c1e83fa7857379fc5ad12727c86723d8065))

## [6.0.1-fix-ID2-3844-keyboard-dnd.10](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.0.1-fix-ID2-3844-keyboard-dnd.9...v6.0.1-fix-ID2-3844-keyboard-dnd.10) (2022-11-12)

### Bug Fixes

- **keyboard_dnd:** remove consoles ([19bb38a](https://bitbucket.org/synetics/i-doit-ui-kit/commits/19bb38ab80a121d464dc1d5590a16f8c09420372))
- **keyboard_dnd:** renew test coverage ([211e260](https://bitbucket.org/synetics/i-doit-ui-kit/commits/211e2600b994a0be568a3dee2ce55101f574d135))

## [6.0.1-fix-ID2-3844-keyboard-dnd.9](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.0.1-fix-ID2-3844-keyboard-dnd.9...v6.0.1-fix-ID2-3844-keyboard-dnd.10) (2022-10-31)

### Bug Fixes

- **keyboard_dnd:** make it scrollable ([d515c61](https://bitbucket.org/synetics/i-doit-ui-kit/commits/d515c610d388229bca1557440b8c6120c99c40ca))

## [6.0.1-fix-ID2-3844-keyboard-dnd.8](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.0.1-fix-ID2-3844-keyboard-dnd.9...v6.0.1-fix-ID2-3844-keyboard-dnd.10) (2022-10-31)

### Bug Fixes

- **keyboard_dnd:** make it scrollable ([e40f72d](https://bitbucket.org/synetics/i-doit-ui-kit/commits/e40f72d0d52c426ec24d167c9fb9913348e958fc))

## [6.0.1-fix-ID2-3844-keyboard-dnd.7](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.0.1-fix-ID2-3844-keyboard-dnd.9...v6.0.1-fix-ID2-3844-keyboard-dnd.10) (2022-10-31)

### Bug Fixes

- **keyboard_dnd:** make it scrollable ([17c290e](https://bitbucket.org/synetics/i-doit-ui-kit/commits/17c290ee5a3202e391bdef7da56a89a2d7a395d7))
- **keyboard_dnd:** make it scrollable ([f58e8aa](https://bitbucket.org/synetics/i-doit-ui-kit/commits/f58e8aa803cd8ab73927ae9fb46a25f02bc1a638))

## [6.0.1-fix-ID2-3844-keyboard-dnd.6](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.0.1-fix-ID2-3844-keyboard-dnd.9...v6.0.1-fix-ID2-3844-keyboard-dnd.10) (2022-10-31)

### Bug Fixes

- **keyboard_dnd:** make it scrollable ([2a5a025](https://bitbucket.org/synetics/i-doit-ui-kit/commits/2a5a025ede239768874c16f43e3a02439de2aefc))

## [6.0.1-fix-ID2-3844-keyboard-dnd.5](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.0.1-fix-ID2-3844-keyboard-dnd.9...v6.0.1-fix-ID2-3844-keyboard-dnd.10) (2022-10-31)

### Bug Fixes

- **keyboard_dnd:** make it scrollable ([e8c1b40](https://bitbucket.org/synetics/i-doit-ui-kit/commits/e8c1b400c6b7029e8b7abc8b31d9c4daf74d36c3))

## [6.0.1-fix-ID2-3844-keyboard-dnd.4](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.0.1-fix-ID2-3844-keyboard-dnd.9...v6.0.1-fix-ID2-3844-keyboard-dnd.10) (2022-10-30)

### Bug Fixes

- **keyboard_dnd:** make it scrollable ([47752e2](https://bitbucket.org/synetics/i-doit-ui-kit/commits/47752e24adf149db39d34f18c6cb00bf08105bf8))

## [6.0.1-fix-ID2-3844-keyboard-dnd.3](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.0.1-fix-ID2-3844-keyboard-dnd.9...v6.0.1-fix-ID2-3844-keyboard-dnd.10) (2022-10-30)

### Bug Fixes

- **keyboard_dnd:** make it scrollable ([e2941cd](https://bitbucket.org/synetics/i-doit-ui-kit/commits/e2941cde1dc471f73e297e6cd50ec1c5fbee368a))

## [6.0.1-fix-ID2-3844-keyboard-dnd.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.0.1-fix-ID2-3844-keyboard-dnd.9...v6.0.1-fix-ID2-3844-keyboard-dnd.10) (2022-10-28)

### Bug Fixes

- **keyboard_dnd:** make it scrollable ([f6ad12a](https://bitbucket.org/synetics/i-doit-ui-kit/commits/f6ad12a9e05372030f5a9a1f0a19454b226865d6))
- **keyboard_dnd:** make it scrollable ([3037c7f](https://bitbucket.org/synetics/i-doit-ui-kit/commits/3037c7f38c03ffee908188fd06ce179a1fb8bcd0))

## [6.0.1-fix-ID2-3844-keyboard-dnd.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.0.1-fix-ID2-3844-keyboard-dnd.9...v6.0.1-fix-ID2-3844-keyboard-dnd.10) (2022-10-27)

### Bug Fixes

- **keyboard_dnd:** make it scrollable ([b6e8442](https://bitbucket.org/synetics/i-doit-ui-kit/commits/b6e8442f7abe6f485bdd9a120955d8cf59eaf57d))
- **keyboard_dnd:** make it scrollable ([10d9202](https://bitbucket.org/synetics/i-doit-ui-kit/commits/10d9202a4addc5f1cdef043acb57238f807354ce))

## [6.0.1-fix-ID2-3844-keyboard-dnd.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.0.1-fix-ID2-3844-keyboard-dnd.9...v6.0.1-fix-ID2-3844-keyboard-dnd.10) (2022-10-27)

### Bug Fixes

- **keyboard_dnd:** make it scrollable ([499ebcb](https://bitbucket.org/synetics/i-doit-ui-kit/commits/499ebcb852f769c1271703d9739e68880e134627))

## [6.0.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.0.1-fix-ID2-3844-keyboard-dnd.9...v6.0.1-fix-ID2-3844-keyboard-dnd.10) (2022-11-09)

## [6.0.1-bug-ID2-0000-fix-tag-event.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.0.1-fix-ID2-3844-keyboard-dnd.9...v6.0.1-fix-ID2-3844-keyboard-dnd.10) (2022-11-09)

## [6.0.1-bug-ID2-0000-fix-tag-event.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.0.1-fix-ID2-3844-keyboard-dnd.9...v6.0.1-fix-ID2-3844-keyboard-dnd.10) (2022-11-09)

## [6.0.1-bug-ID2-0000-fix-tag-event.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.0.1-fix-ID2-3844-keyboard-dnd.9...v6.0.1-fix-ID2-3844-keyboard-dnd.10) (2022-11-08)

### Bug Fixes

- **combobox:** fix delete tags ([19bd71c](https://bitbucket.org/synetics/i-doit-ui-kit/commits/19bd71c347a6e411a126a5f5fda3967650871b3c))

## [6.0.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.0.1-bug-ID2-0000-fix-tag-event.2...v6.0.1) (2022-11-09)

## [6.0.1-bug-ID2-0000-fix-tag-event.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.0.1-bug-ID2-0000-fix-tag-event.1...v6.0.1-bug-ID2-0000-fix-tag-event.2) (2022-11-09)

## [6.0.1-bug-ID2-0000-fix-tag-event.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.0.1-bug-ID2-0000-fix-tag-event.0...v6.0.1-bug-ID2-0000-fix-tag-event.1) (2022-11-09)

## [6.0.1-bug-ID2-0000-fix-tag-event.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.0.0...v6.0.1-bug-ID2-0000-fix-tag-event.0) (2022-11-08)

### Bug Fixes

- **combobox:** fix delete tags ([19bd71c](https://bitbucket.org/synetics/i-doit-ui-kit/commits/19bd71c347a6e411a126a5f5fda3967650871b3c))

## [6.0.1-fix-ID2-3844-keyboard-dnd.9](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.0.1-fix-ID2-3844-keyboard-dnd.8...v6.0.1-fix-ID2-3844-keyboard-dnd.9) (2022-10-31)

### Bug Fixes

- **keyboard_dnd:** make it scrollable ([d515c61](https://bitbucket.org/synetics/i-doit-ui-kit/commits/d515c610d388229bca1557440b8c6120c99c40ca))

## [6.0.1-fix-ID2-3844-keyboard-dnd.8](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.0.1-fix-ID2-3844-keyboard-dnd.7...v6.0.1-fix-ID2-3844-keyboard-dnd.8) (2022-10-31)

### Bug Fixes

- **keyboard_dnd:** make it scrollable ([e40f72d](https://bitbucket.org/synetics/i-doit-ui-kit/commits/e40f72d0d52c426ec24d167c9fb9913348e958fc))

## [6.0.1-fix-ID2-3844-keyboard-dnd.7](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.0.1-fix-ID2-3844-keyboard-dnd.6...v6.0.1-fix-ID2-3844-keyboard-dnd.7) (2022-10-31)

### Bug Fixes

- **keyboard_dnd:** make it scrollable ([17c290e](https://bitbucket.org/synetics/i-doit-ui-kit/commits/17c290ee5a3202e391bdef7da56a89a2d7a395d7))
- **keyboard_dnd:** make it scrollable ([f58e8aa](https://bitbucket.org/synetics/i-doit-ui-kit/commits/f58e8aa803cd8ab73927ae9fb46a25f02bc1a638))

## [6.0.1-fix-ID2-3844-keyboard-dnd.6](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.0.1-fix-ID2-3844-keyboard-dnd.5...v6.0.1-fix-ID2-3844-keyboard-dnd.6) (2022-10-31)

### Bug Fixes

- **keyboard_dnd:** make it scrollable ([2a5a025](https://bitbucket.org/synetics/i-doit-ui-kit/commits/2a5a025ede239768874c16f43e3a02439de2aefc))

## [6.0.1-fix-ID2-3844-keyboard-dnd.5](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.0.1-fix-ID2-3844-keyboard-dnd.4...v6.0.1-fix-ID2-3844-keyboard-dnd.5) (2022-10-31)

### Bug Fixes

- **keyboard_dnd:** make it scrollable ([e8c1b40](https://bitbucket.org/synetics/i-doit-ui-kit/commits/e8c1b400c6b7029e8b7abc8b31d9c4daf74d36c3))

## [6.0.1-fix-ID2-3844-keyboard-dnd.4](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.0.1-fix-ID2-3844-keyboard-dnd.3...v6.0.1-fix-ID2-3844-keyboard-dnd.4) (2022-10-30)

### Bug Fixes

- **keyboard_dnd:** make it scrollable ([47752e2](https://bitbucket.org/synetics/i-doit-ui-kit/commits/47752e24adf149db39d34f18c6cb00bf08105bf8))

## [6.0.1-fix-ID2-3844-keyboard-dnd.3](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.0.1-fix-ID2-3844-keyboard-dnd.2...v6.0.1-fix-ID2-3844-keyboard-dnd.3) (2022-10-30)

### Bug Fixes

- **keyboard_dnd:** make it scrollable ([e2941cd](https://bitbucket.org/synetics/i-doit-ui-kit/commits/e2941cde1dc471f73e297e6cd50ec1c5fbee368a))

## [6.0.1-fix-ID2-3844-keyboard-dnd.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.0.1-fix-ID2-3844-keyboard-dnd.1...v6.0.1-fix-ID2-3844-keyboard-dnd.2) (2022-10-28)

### Bug Fixes

- **keyboard_dnd:** make it scrollable ([f6ad12a](https://bitbucket.org/synetics/i-doit-ui-kit/commits/f6ad12a9e05372030f5a9a1f0a19454b226865d6))
- **keyboard_dnd:** make it scrollable ([3037c7f](https://bitbucket.org/synetics/i-doit-ui-kit/commits/3037c7f38c03ffee908188fd06ce179a1fb8bcd0))

## [6.0.1-fix-ID2-3844-keyboard-dnd.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.0.1-fix-ID2-3844-keyboard-dnd.0...v6.0.1-fix-ID2-3844-keyboard-dnd.1) (2022-10-27)

### Bug Fixes

- **keyboard_dnd:** make it scrollable ([b6e8442](https://bitbucket.org/synetics/i-doit-ui-kit/commits/b6e8442f7abe6f485bdd9a120955d8cf59eaf57d))
- **keyboard_dnd:** make it scrollable ([10d9202](https://bitbucket.org/synetics/i-doit-ui-kit/commits/10d9202a4addc5f1cdef043acb57238f807354ce))

## [6.0.1-fix-ID2-3844-keyboard-dnd.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v6.0.0...v6.0.1-fix-ID2-3844-keyboard-dnd.0) (2022-10-27)

### Bug Fixes

- **keyboard_dnd:** make it scrollable ([499ebcb](https://bitbucket.org/synetics/i-doit-ui-kit/commits/499ebcb852f769c1271703d9739e68880e134627))

# [6.0.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.82.3...v6.0.0) (2022-10-24)

# [6.0.0-task-ID2-3046-use-node-18.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.82.3...v6.0.0) (2022-10-17)

### Features

- **node:** require node 18 and npm 8 ([5cd5d4d](https://bitbucket.org/synetics/i-doit-ui-kit/commits/5cd5d4d64b53eb40fb068244b06a954a047dfb42))

### BREAKING CHANGES

- **node:** drop support of older node and npm versions

# [6.0.0-task-ID2-3046-use-node-18.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.82.2...v6.0.0-task-ID2-3046-use-node-18.0) (2022-10-17)

### Features

- **node:** require node 18 and npm 8 ([5cd5d4d](https://bitbucket.org/synetics/i-doit-ui-kit/commits/5cd5d4d64b53eb40fb068244b06a954a047dfb42))

### BREAKING CHANGES

- **node:** drop support of older node and npm versions

## [5.82.3](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.82.3-bug-ID2-3685-fix-groups.0...v5.82.3) (2022-10-19)

## [5.82.3-bug-ID2-3685-fix-groups.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.82.2...v5.82.3-bug-ID2-3685-fix-groups.0) (2022-10-18)

### Bug Fixes

- **fix_groups:** dropdown layout ([882fa3c](https://bitbucket.org/synetics/i-doit-ui-kit/commits/882fa3c49d7d58a7b2ee971d93ee3194c7781f20))

## [5.82.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.82.2-bug-overflow.0...v5.82.2) (2022-10-17)

## [5.82.2-bug-overflow.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.82.1...v5.82.2-bug-overflow.0) (2022-10-12)

### Bug Fixes

- **overflow:** fix incorrect extension of the module scss file ([f276fab](https://bitbucket.org/synetics/i-doit-ui-kit/commits/f276fabfe65681e69b10af42db987050a1621ab0))

## [5.82.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.82.1-task-ID2-3759-donut-labels.0...v5.82.1) (2022-10-12)

## [5.82.1-task-ID2-3759-donut-labels.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.82.0-task-ID2-3759-donut-labels.3...v5.82.1-task-ID2-3759-donut-labels.0) (2022-10-12)

# [5.82.0-task-ID2-3759-donut-labels.3](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.82.0-task-ID2-3759-donut-labels.2...v5.82.0-task-ID2-3759-donut-labels.3) (2022-10-12)

# [5.82.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.82.0-task-ID2-3759-donut-labels.2...v5.82.0-task-ID2-3759-donut-labels.3) (2022-10-11)

# [5.82.0-feature-ID2-3754-tab-bar.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.82.0-task-ID2-3759-donut-labels.2...v5.82.0-task-ID2-3759-donut-labels.3) (2022-10-07)

### Features

- **tab-bar:** implement tab bar ([e6c381e](https://bitbucket.org/synetics/i-doit-ui-kit/commits/e6c381eff237306aa675265544ced6953a859c9b))

# [5.82.0-task-ID2-3759-donut-labels.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.82.0-task-ID2-3759-donut-labels.1...v5.82.0-task-ID2-3759-donut-labels.2) (2022-10-11)

# [5.82.0-task-ID2-3759-donut-labels.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.82.0-task-ID2-3759-donut-labels.0...v5.82.0-task-ID2-3759-donut-labels.1) (2022-10-10)

# [5.82.0-task-ID2-3759-donut-labels.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.81.0...v5.82.0-task-ID2-3759-donut-labels.0) (2022-10-10)

### Features

- **chart:** add labels to donut chart ([49a1798](https://bitbucket.org/synetics/i-doit-ui-kit/commits/49a1798f7e28d3c3468a1d3a2eb7279c32c377e9))

# [5.82.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.82.0-feature-ID2-3754-tab-bar.0...v5.82.0) (2022-10-11)

# [5.82.0-feature-ID2-3754-tab-bar.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.81.0...v5.82.0-feature-ID2-3754-tab-bar.0) (2022-10-07)

### Features

- **tab-bar:** implement tab bar ([e6c381e](https://bitbucket.org/synetics/i-doit-ui-kit/commits/e6c381eff237306aa675265544ced6953a859c9b))

# [5.81.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.80.1...v5.81.0) (2022-10-04)

# [5.81.0-feat-ID2-3697-add-classname.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.80.1...v5.81.0) (2022-09-29)

### Features

- **add_classname:** adjusted usage example ([e5f001e](https://bitbucket.org/synetics/i-doit-ui-kit/commits/e5f001e880abd4bd705bfec168d73e2f9df479bb))

# [5.81.0-feat-ID2-3697-add-classname.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.80.1...v5.81.0) (2022-09-28)

### Features

- **add_classname:** add container to prevent tooltip distance if it has margins ([9316236](https://bitbucket.org/synetics/i-doit-ui-kit/commits/9316236ab4ee432ec77cf4c7e1bb95b3bf1f13a1))

## [5.80.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.80.1-bug-ID2-3569-click-resizer.2...v5.80.1) (2022-10-04)

## [5.80.1-bug-ID2-3569-click-resizer.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.80.0...v5.80.1-bug-ID2-3569-click-resizer.2) (2022-09-30)

### Bug Fixes

- **click_resizer:** drag over the button ([a84cb8d](https://bitbucket.org/synetics/i-doit-ui-kit/commits/a84cb8d41920f5af208cc285c343a841e5487890))
- **click_resizer:** drag over the button ([1ff932e](https://bitbucket.org/synetics/i-doit-ui-kit/commits/1ff932eefe872ef4250f4a63db716bfd502eea0e))

# [5.80.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.79.0...v5.80.0) (2022-09-28)

### Features

- **icons:** add placeholder and circle disable icons ([d884576](https://bitbucket.org/synetics/i-doit-ui-kit/commits/d88457609fa3efdeb0347860f0ca69c84c410015))

# [5.79.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.78.4...v5.79.0) (2022-09-27)

### Features

- **tile_button:** add tile button for category page ([5efe3a2](https://bitbucket.org/synetics/i-doit-ui-kit/commits/5efe3a255e6888610d28efcbb3e398ead1249875))
- **tile_button:** add tile button for category page ([cef6780](https://bitbucket.org/synetics/i-doit-ui-kit/commits/cef678088a57f588e3564cbc735467ad999f21f4))
- **tile_button:** add tile button for category page ([1b2183d](https://bitbucket.org/synetics/i-doit-ui-kit/commits/1b2183d1cccca0990de0ccfe36ec55eec39857c6))

## [5.78.4](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.78.3...v5.78.4) (2022-09-19)

### Bug Fixes

- **radio_checked:** remove ids from radio ([93f90d9](https://bitbucket.org/synetics/i-doit-ui-kit/commits/93f90d988ed7b3c8089ccfa51f80bd4d34ddac59))

## [5.78.3](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.78.3-bug-ID2-3585-resize-observer.6...v5.78.3) (2022-09-15)

### Bug Fixes

- **resize_observer:** make first element of neede type ([51a75c1](https://bitbucket.org/synetics/i-doit-ui-kit/commits/51a75c1db8cf6ffc58ea5db40e735f773c527a38))

## [5.78.3-bug-ID2-3585-resize-observer.6](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.78.3-bug-ID2-3585-resize-observer.5...v5.78.3-bug-ID2-3585-resize-observer.6) (2022-09-09)

## [5.78.3-bug-ID2-3585-resize-observer.5](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.78.3-bug-ID2-3585-resize-observer.4...v5.78.3-bug-ID2-3585-resize-observer.5) (2022-09-09)

### Bug Fixes

- **resize_observer:** make first element of neede type ([83499bc](https://bitbucket.org/synetics/i-doit-ui-kit/commits/83499bcc4b8fa9392d317d1044bac2e1373b49ca))

## [5.78.3-bug-ID2-3585-resize-observer.4](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.78.3-bug-ID2-3585-resize-observer.3...v5.78.3-bug-ID2-3585-resize-observer.4) (2022-09-09)

### Bug Fixes

- **resize_observer:** make first element of neede type ([521172c](https://bitbucket.org/synetics/i-doit-ui-kit/commits/521172cfc5ff833234911af402f15580768a4850))

## [5.78.3-bug-ID2-3585-resize-observer.3](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.78.3-bug-ID2-3585-resize-observer.2...v5.78.3-bug-ID2-3585-resize-observer.3) (2022-09-09)

### Bug Fixes

- **resize_observer:** add log to identify error ([bce6a6c](https://bitbucket.org/synetics/i-doit-ui-kit/commits/bce6a6c9f5747a13e59bfb411ba9a44122150e87))

## [5.78.3-bug-ID2-3585-resize-observer.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.78.3-bug-ID2-3585-resize-observer.1...v5.78.3-bug-ID2-3585-resize-observer.2) (2022-09-09)

### Bug Fixes

- **resize_observer:** add log to identify error ([acc7aea](https://bitbucket.org/synetics/i-doit-ui-kit/commits/acc7aea96780a9e6406ab2defdfa3d28f1a81296))

## [5.78.3-bug-ID2-3585-resize-observer.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.78.3-bug-ID2-3585-resize-observer.0...v5.78.3-bug-ID2-3585-resize-observer.1) (2022-09-09)

### Bug Fixes

- **resize_observer:** add log to identify error ([9f23111](https://bitbucket.org/synetics/i-doit-ui-kit/commits/9f23111e72ae30b0895d322a05e6b6149eed95c1))

## [5.78.3-bug-ID2-3585-resize-observer.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.78.2...v5.78.3-bug-ID2-3585-resize-observer.0) (2022-09-09)

### Bug Fixes

- **resize_observer:** add log to identify error ([dfa2971](https://bitbucket.org/synetics/i-doit-ui-kit/commits/dfa2971d72efd737890aba84f13515d9bc98d85d))

## [5.78.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.78.2-bug-ID2-0000-add-onClick-SubItem.1...v5.78.2) (2022-09-06)

## [5.78.2-bug-ID2-0000-add-onClick-SubItem.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.78.2-bug-ID2-0000-add-onClick-SubItem.0...v5.78.2-bug-ID2-0000-add-onClick-SubItem.1) (2022-09-06)

## [5.78.2-bug-ID2-0000-add-onClick-SubItem.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.78.1...v5.78.2-bug-ID2-0000-add-onClick-SubItem.0) (2022-09-06)

### Bug Fixes

- **sub-item:** add on click ([c3a761b](https://bitbucket.org/synetics/i-doit-ui-kit/commits/c3a761b1fb413b58c0bfc68fac766901f6c2e0f9))

## [5.78.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.78.1-fix-ID2-3521-adjust-password.1...v5.78.1) (2022-09-05)

## [5.78.1-fix-ID2-3521-adjust-password.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.78.1-fix-ID2-3521-adjust-password.0...v5.78.1-fix-ID2-3521-adjust-password.1) (2022-09-01)

### Bug Fixes

- **adjust_password:** add group classname to password ([71b92ae](https://bitbucket.org/synetics/i-doit-ui-kit/commits/71b92ae01dbf01c47f8061fe533e924b6c81024f))

## [5.78.1-fix-ID2-3521-adjust-password.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.78.0...v5.78.1-fix-ID2-3521-adjust-password.0) (2022-09-01)

### Bug Fixes

- **adjust_password:** add group classname to password ([e348efb](https://bitbucket.org/synetics/i-doit-ui-kit/commits/e348efb3602c063e72c81e5c5cf0e14f46b1dd0e))

# [5.78.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.76.1...v5.78.0) (2022-08-30)

# [5.77.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.76.1...v5.78.0) (2022-08-25)

# [5.77.0-feature-ID2-0000-button-className-tooltip.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.76.1...v5.78.0) (2022-08-23)

# [5.77.0-feature-ID2-0000-button-className-tooltip.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.76.1...v5.78.0) (2022-08-23)

### Features

- **button:** add classname prop for button tooltip ([557225f](https://bitbucket.org/synetics/i-doit-ui-kit/commits/557225f1f1e3cfb3cea64752529eeaff33bf6bba))

# [5.77.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.77.0-feature-ID2-0000-button-className-tooltip.1...v5.77.0) (2022-08-25)

# [5.77.0-feature-ID2-0000-button-className-tooltip.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.77.0-feature-ID2-0000-button-className-tooltip.0...v5.77.0-feature-ID2-0000-button-className-tooltip.1) (2022-08-23)

# [5.77.0-feature-ID2-0000-button-className-tooltip.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.76.0...v5.77.0-feature-ID2-0000-button-className-tooltip.0) (2022-08-23)

## [5.76.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.76.1-feature-ID2-3417-implement-virtual-lists.0...v5.76.1) (2022-08-29)

## [5.76.1-feature-ID2-3417-implement-virtual-lists.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.76.0...v5.76.1-feature-ID2-3417-implement-virtual-lists.0) (2022-08-29)

### Features

- **button:** add classname prop for button tooltip ([557225f](https://bitbucket.org/synetics/i-doit-ui-kit/commits/557225f1f1e3cfb3cea64752529eeaff33bf6bba))

# [5.76.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.75.0...v5.76.0) (2022-08-22)

### Features

- **storybook:** use vite for storybook ([ff60f6c](https://bitbucket.org/synetics/i-doit-ui-kit/commits/ff60f6caf1d6f49dd0a4c5db05403bb652a81fe1))

# [5.75.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.75.0-feature-ID2-3567-create-area-stack.0...v5.75.0) (2022-08-22)

# [5.75.0-feature-ID2-3567-create-area-stack.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.74.1...v5.75.0-feature-ID2-3567-create-area-stack.0) (2022-08-22)

### Features

- **area-stack:** implement area stack chart ([0e30179](https://bitbucket.org/synetics/i-doit-ui-kit/commits/0e301791c362f0aec40a199a5ec6b81b7494cc39))
- **scroll:** create scroll component in svg ([03d9020](https://bitbucket.org/synetics/i-doit-ui-kit/commits/03d9020641bf2b3eae3485c02f623f7b999d740a))

## [5.74.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.74.0...v5.74.1) (2022-08-10)

### Bug Fixes

- **ip-range:** change active state after clear selection ([c868c12](https://bitbucket.org/synetics/i-doit-ui-kit/commits/c868c12053ef87291c413d3322efe09b66d3e184))

# [5.74.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.74.0-feature-list-range.1...v5.74.0) (2022-08-10)

# [5.74.0-feature-list-range.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.74.0-feature-list-range.0...v5.74.0-feature-list-range.1) (2022-08-09)

# [5.74.0-feature-list-range.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.73.0...v5.74.0-feature-list-range.0) (2022-08-09)

# [5.72.0-feature-list-range.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.73.0...v5.74.0-feature-list-range.0) (2022-08-09)

### Bug Fixes

- **scroll-list:** fix imperative hook ([30b0bc3](https://bitbucket.org/synetics/i-doit-ui-kit/commits/30b0bc35ac7c682bdaa685e55e83c120db90b306))

### Features

- **scroll-list:** add scroll list component and stories ([f64596f](https://bitbucket.org/synetics/i-doit-ui-kit/commits/f64596f2f86dd4f3945899c4faa6381c752cd7cb))

# [5.72.0-feature-list-range.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.73.0...v5.74.0-feature-list-range.0) (2022-08-08)

# [5.72.0-feature-list-range.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.73.0...v5.74.0-feature-list-range.0) (2022-08-08)

### Features

- **range-item:** implement range item selection ([220b107](https://bitbucket.org/synetics/i-doit-ui-kit/commits/220b1079c06ec177be5e1b89870beb11e5a0946f))

# [5.73.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.72.0...v5.73.0) (2022-08-09)

# [5.72.0-feature-ip-range.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.72.0...v5.73.0) (2022-08-05)

### Features

- **ip-range:** example of calendar-like range browser ([10d5da9](https://bitbucket.org/synetics/i-doit-ui-kit/commits/10d5da9f7f0a03a0faeeaea33d888491160202b8))

# [5.72.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.71.0...v5.72.0) (2022-08-09)

### Features

- **datetime:** add parent class for dont break styles ([57a68b9](https://bitbucket.org/synetics/i-doit-ui-kit/commits/57a68b972e1740cc0de8579353e415c24a8a1c4f))
- **datetime:** add parent class for dont break styles ([816cffb](https://bitbucket.org/synetics/i-doit-ui-kit/commits/816cffbe5d6ecd4597bcc9c27c361d6928f24fbc))
- **datetime:** adjust calendar formfield with time ([704111b](https://bitbucket.org/synetics/i-doit-ui-kit/commits/704111baa8e80fc9cddc7d2f5474ceeaf0005498))
- **datetime:** fix datetime field changes ([6ccb132](https://bitbucket.org/synetics/i-doit-ui-kit/commits/6ccb13245d709649f5a018dd3e5f441b909c986b))
- **datetime:** fix datetime field changes ([cf4d080](https://bitbucket.org/synetics/i-doit-ui-kit/commits/cf4d08061736857cbd8bae81cc968fb698094fa7))
- **datetime:** fix format checks ([3d8824e](https://bitbucket.org/synetics/i-doit-ui-kit/commits/3d8824e91d32087376e758d088fbbeaba53871a4))
- **datetime:** fix format checks ([3ee649d](https://bitbucket.org/synetics/i-doit-ui-kit/commits/3ee649ddea70430de25a205c5d61448af5efa342))
- **datetime:** fix format checks ([fa5200d](https://bitbucket.org/synetics/i-doit-ui-kit/commits/fa5200de1e4f3ad22db82c3565ef186a76438c07))

## [5.71.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.71.0...v5.71.1) (2022-08-08)

### Bug Fixes

- **multiselect_sort:** fix user sorting ([b14b279](https://bitbucket.org/synetics/i-doit-ui-kit/commits/b14b279a66ce240a98c9119d51d432de09a2d3f1))
- **multiselect_sort:** remove unnesessary check ([0749b8c](https://bitbucket.org/synetics/i-doit-ui-kit/commits/0749b8cc39c2f448d9550c73e965414d30994338))

# [5.71.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.70.0...v5.71.0) (2022-08-05)

# [5.70.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.70.0...v5.71.0) (2022-08-03)

# [5.70.0-task-ID2-1263-adjust-calendar.13](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.70.0...v5.71.0) (2022-08-02)

### Features

- **adjust_calendar:** fix input focus after close ([4aa5515](https://bitbucket.org/synetics/i-doit-ui-kit/commits/4aa55151ab9cc830ab63aa744023c4caeb41c6b2))

# [5.70.0-task-ID2-1263-adjust-calendar.12](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.70.0...v5.71.0) (2022-08-01)

### Features

- **adjust_calendar:** add test for header buttons propagation ([c832ba0](https://bitbucket.org/synetics/i-doit-ui-kit/commits/c832ba0de68f0fb3fd338a56ae9c64105a0b42f9))

# [5.70.0-task-ID2-1263-adjust-calendar.11](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.70.0...v5.71.0) (2022-07-30)

### Features

- **adjust_calendar:** adjust keyboard behavior ([08faa69](https://bitbucket.org/synetics/i-doit-ui-kit/commits/08faa697cbc1b039259d397a2320099fc34d36f6))
- **adjust_calendar:** adjust keyboard behavior ([cf4f179](https://bitbucket.org/synetics/i-doit-ui-kit/commits/cf4f179bb378b25751ed3f73000ade70d176ac7a))

# [5.70.0-feature-range-selection.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.70.0...v5.71.0) (2022-08-03)

### Features

- **range-selection:** implement range selection logic ([3dd8853](https://bitbucket.org/synetics/i-doit-ui-kit/commits/3dd88530ebeb377cc8bee86e2ca525d005ae2421))

# [5.70.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.70.0-task-ID2-1263-adjust-calendar.13...v5.70.0) (2022-08-03)

# [5.70.0-task-ID2-1263-adjust-calendar.13](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.70.0-task-ID2-1263-adjust-calendar.12...v5.70.0-task-ID2-1263-adjust-calendar.13) (2022-08-02)

### Features

- **adjust_calendar:** fix input focus after close ([4aa5515](https://bitbucket.org/synetics/i-doit-ui-kit/commits/4aa55151ab9cc830ab63aa744023c4caeb41c6b2))

# [5.70.0-task-ID2-1263-adjust-calendar.12](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.70.0-task-ID2-1263-adjust-calendar.11...v5.70.0-task-ID2-1263-adjust-calendar.12) (2022-08-01)

### Features

- **adjust_calendar:** add test for header buttons propagation ([c832ba0](https://bitbucket.org/synetics/i-doit-ui-kit/commits/c832ba0de68f0fb3fd338a56ae9c64105a0b42f9))

# [5.70.0-task-ID2-1263-adjust-calendar.11](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.69.1...v5.70.0-task-ID2-1263-adjust-calendar.11) (2022-07-30)

### Features

- **adjust_calendar:** adjust keyboard behavior ([08faa69](https://bitbucket.org/synetics/i-doit-ui-kit/commits/08faa697cbc1b039259d397a2320099fc34d36f6))
- **adjust_calendar:** adjust keyboard behavior ([cf4f179](https://bitbucket.org/synetics/i-doit-ui-kit/commits/cf4f179bb378b25751ed3f73000ade70d176ac7a))

# [5.70.0-task-ID2-1263-adjust-calendar.10](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.70.0-task-ID2-1263-adjust-calendar.4...v5.70.0-task-ID2-1263-adjust-calendar.10) (2022-07-30)

### Features

- **adjust_calendar:** adjust keyboard behavior ([81d4331](https://bitbucket.org/synetics/i-doit-ui-kit/commits/81d4331a604a4bb5daa717dccd297df9926308ec))
- **adjust_calendar:** adjust keyboard behavior ([04e68db](https://bitbucket.org/synetics/i-doit-ui-kit/commits/04e68db70d700b9b408321fa9312489c246bbb5a))

# [5.70.0-task-ID2-1263-adjust-calendar.9](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.70.0-task-ID2-1263-adjust-calendar.8...v5.70.0-task-ID2-1263-adjust-calendar.9) (2022-07-30)

### Features

- **adjust_calendar:** adjust keyboard behavior ([481d523](https://bitbucket.org/synetics/i-doit-ui-kit/commits/481d5232e0792076fcdc74a99ee3fb3fca5e2de2))
- **adjust_calendar:** adjust keyboard behavior ([5370ae3](https://bitbucket.org/synetics/i-doit-ui-kit/commits/5370ae31433a57eb17f3ea426c158862c3be546c))

# [5.70.0-task-ID2-1263-adjust-calendar.8](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.70.0-task-ID2-1263-adjust-calendar.7...v5.70.0-task-ID2-1263-adjust-calendar.8) (2022-07-29)

### Features

- **adjust_calendar:** adjust keyboard behavior ([000ec5a](https://bitbucket.org/synetics/i-doit-ui-kit/commits/000ec5aa429ebb17f975f8821a32d55a06840c14))

# [5.70.0-task-ID2-1263-adjust-calendar.7](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.70.0-task-ID2-1263-adjust-calendar.6...v5.70.0-task-ID2-1263-adjust-calendar.7) (2022-07-29)

### Features

- **adjust_calendar:** adjust keyboard behavior ([d65142d](https://bitbucket.org/synetics/i-doit-ui-kit/commits/d65142da0ceb9b98fa0623a679dedc84ead2cc76))

# [5.70.0-task-ID2-1263-adjust-calendar.6](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.70.0-task-ID2-1263-adjust-calendar.5...v5.70.0-task-ID2-1263-adjust-calendar.6) (2022-07-29)

### Features

- **adjust_calendar:** adjust keyboard behavior ([b081b64](https://bitbucket.org/synetics/i-doit-ui-kit/commits/b081b642620a8ad0c70e4ab031233eaeadc2ee14))

# [5.70.0-task-ID2-1263-adjust-calendar.5](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.70.0-task-ID2-1263-adjust-calendar.4...v5.70.0-task-ID2-1263-adjust-calendar.5) (2022-07-29)

### Features

- **adjust_calendar:** adjust keyboard behavior ([04e68db](https://bitbucket.org/synetics/i-doit-ui-kit/commits/04e68db70d700b9b408321fa9312489c246bbb5a))

# [5.70.0-task-ID2-1263-adjust-calendar.4](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.70.0-task-ID2-1263-adjust-calendar.3...v5.70.0-task-ID2-1263-adjust-calendar.4) (2022-07-29)

### Features

- **adjust_calendar:** adjust keyboard behavior ([b251424](https://bitbucket.org/synetics/i-doit-ui-kit/commits/b2514241fdb0591da6f556c286bb67b15e07b57d))

# [5.70.0-task-ID2-1263-adjust-calendar.3](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.70.0-task-ID2-1263-adjust-calendar.2...v5.70.0-task-ID2-1263-adjust-calendar.3) (2022-07-29)

### Features

- **adjust_calendar:** adjust keyboard behavior ([fdcc2a5](https://bitbucket.org/synetics/i-doit-ui-kit/commits/fdcc2a500cf0d07a977d35ba489e72367f6bae96))

# [5.70.0-task-ID2-1263-adjust-calendar.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.70.0-task-ID2-1263-adjust-calendar.1...v5.70.0-task-ID2-1263-adjust-calendar.2) (2022-07-29)

### Features

- **adjust_calendar:** adjust keyboard behavior ([afeed31](https://bitbucket.org/synetics/i-doit-ui-kit/commits/afeed31e9b177c15973edd315e47a0ff3c621a35))

# [5.70.0-task-ID2-1263-adjust-calendar.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.70.0-task-ID2-1263-adjust-calendar.0...v5.70.0-task-ID2-1263-adjust-calendar.1) (2022-07-29)

### Features

- **adjust_calendar:** adjust keyboard behavior ([378b013](https://bitbucket.org/synetics/i-doit-ui-kit/commits/378b013aef95d75722a54372d1081cca5cdee755))

# [5.70.0-task-ID2-1263-adjust-calendar.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.69.1...v5.70.0-task-ID2-1263-adjust-calendar.0) (2022-07-29)

### Features

- **adjust_calendar:** adjust keyboard behavior ([cf4f179](https://bitbucket.org/synetics/i-doit-ui-kit/commits/cf4f179bb378b25751ed3f73000ade70d176ac7a))

## [5.69.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.69.1-bug-ID2-3460-render-without-overflow.0...v5.69.1) (2022-07-26)

## [5.69.1-bug-ID2-3460-render-without-overflow.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.69.0...v5.69.1-bug-ID2-3460-render-without-overflow.0) (2022-07-25)

### Bug Fixes

- **table:** add the ability to add by condition overflow for header ([5d6e0e3](https://bitbucket.org/synetics/i-doit-ui-kit/commits/5d6e0e3be0d1078c9b4943209aab0f91bf175558))

# [5.69.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.69.0-task-ID2-0000-custom-piece-space.4...v5.69.0) (2022-07-22)

# [5.69.0-task-ID2-0000-custom-piece-space.4](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.69.0-task-ID2-0000-custom-piece-space.3...v5.69.0-task-ID2-0000-custom-piece-space.4) (2022-07-21)

# [5.69.0-task-ID2-0000-custom-piece-space.3](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.69.0-task-ID2-0000-custom-piece-space.2...v5.69.0-task-ID2-0000-custom-piece-space.3) (2022-07-21)

# [5.69.0-task-ID2-0000-custom-piece-space.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.69.0-task-ID2-0000-custom-piece-space.1...v5.69.0-task-ID2-0000-custom-piece-space.2) (2022-07-21)

# [5.69.0-task-ID2-0000-custom-piece-space.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.69.0-task-ID2-0000-custom-piece-space.0...v5.69.0-task-ID2-0000-custom-piece-space.1) (2022-07-21)

# [5.69.0-task-ID2-0000-custom-piece-space.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.68.0...v5.69.0-task-ID2-0000-custom-piece-space.0) (2022-07-21)

### Features

- **chart:** add custom space to pieces of chart ([544304b](https://bitbucket.org/synetics/i-doit-ui-kit/commits/544304bdb0487ad59201272653476766631187aa))

# [5.68.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.67.0...v5.68.0) (2022-07-18)

### Features

- **toolbar:** adjust test for toolbar ([b6d3acc](https://bitbucket.org/synetics/i-doit-ui-kit/commits/b6d3acc4b5096d2358b3480265069e06e6d2a8f9))
- **toolbar:** adjust tests for test coverage ([91fa022](https://bitbucket.org/synetics/i-doit-ui-kit/commits/91fa0223447473a09e4a275110ceb07c1f8668e7))
- **toolbar:** create basic toolbar ([e58d5f0](https://bitbucket.org/synetics/i-doit-ui-kit/commits/e58d5f0f95f3209ba8510ee284ab6c309bd3b43b))
- **toolbar:** fix margins ([be199a2](https://bitbucket.org/synetics/i-doit-ui-kit/commits/be199a2f30986fa72260c53751ccca34d03f5f4e))
- **toolbar:** split section into another component ([b433ce0](https://bitbucket.org/synetics/i-doit-ui-kit/commits/b433ce0ef65b7bb2937ea15edbf7a94c51432a67))
- **toolbar:** split section into another component ([9884015](https://bitbucket.org/synetics/i-doit-ui-kit/commits/9884015b8da56cc37f9a3623f03fdad792c1499e))

# [5.67.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.66.0-feat-ID2-1147-resizable.1...v5.67.0) (2022-07-15)

### Features

- **resize:** add docs to story ([fe9a20b](https://bitbucket.org/synetics/i-doit-ui-kit/commits/fe9a20bac2f918cced4557d01f8a1297f18d880f))
- **resize:** add docs to story ([15b20fa](https://bitbucket.org/synetics/i-doit-ui-kit/commits/15b20fa2f59998d32a86e76eb825158b057f92d2))
- **resize:** add docs to story ([8fdd95d](https://bitbucket.org/synetics/i-doit-ui-kit/commits/8fdd95d05d4df71ad66a6a0f08d4612dbb97be75))
- **resize:** add docs to story ([667821f](https://bitbucket.org/synetics/i-doit-ui-kit/commits/667821f239f4f930e6589bfa001901709483cb62))
- **resize:** add docs to story ([1efab2b](https://bitbucket.org/synetics/i-doit-ui-kit/commits/1efab2bc32a829c5ffea886611140b17cbcf0bb2))
- **resize:** retrieve resize version ([efe3ba6](https://bitbucket.org/synetics/i-doit-ui-kit/commits/efe3ba65427a184a020fd82cfd4b66e5ee58cda3))

# [5.66.0-feat-ID2-1147-resizable.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.66.0-feat-ID2-1147-resizable.1...v5.67.0) (2022-07-13)

### Features

- **resizable:** recommit resizable because of much conflicts ([feeeffd](https://bitbucket.org/synetics/i-doit-ui-kit/commits/feeeffd6844a512d5002a88777adb23aa0e2ef2c))

# [5.66.0-feat-ID2-1147-resizable.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.66.0-feat-ID2-1147-resizable.1...v5.67.0) (2022-07-13)

### Features

- **resizable:** recommit resizable because of much conflicts ([60fa50b](https://bitbucket.org/synetics/i-doit-ui-kit/commits/60fa50b5d3ed005af8f1607d394542fa75257f90))

# [5.66.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.66.0-feat-ID2-1147-resizable.1...v5.67.0) (2022-07-14)

### Features

- **chart:** add custom classes to piece of chart ([b50c9e8](https://bitbucket.org/synetics/i-doit-ui-kit/commits/b50c9e85967e8f59960d8f1cb30d6d997b718305))

# [5.66.0-feat-ID2-1147-resizable.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.66.0-feat-ID2-1147-resizable.0...v5.66.0-feat-ID2-1147-resizable.1) (2022-07-13)

### Features

- **resizable:** recommit resizable because of much conflicts ([feeeffd](https://bitbucket.org/synetics/i-doit-ui-kit/commits/feeeffd6844a512d5002a88777adb23aa0e2ef2c))

# [5.66.0-feat-ID2-1147-resizable.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.65.0...v5.66.0-feat-ID2-1147-resizable.0) (2022-07-13)

### Features

- **resizable:** recommit resizable because of much conflicts ([60fa50b](https://bitbucket.org/synetics/i-doit-ui-kit/commits/60fa50b5d3ed005af8f1607d394542fa75257f90))

# [5.65.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.64.0...v5.65.0) (2022-07-12)

### Bug Fixes

- **show_hint:** add testcase ([d51c8c4](https://bitbucket.org/synetics/i-doit-ui-kit/commits/d51c8c49607090153e27fae9a1e155c38afb5e98))

# [5.61.0-bug-ID2-2943-show-hint.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.64.0...v5.65.0) (2022-06-30)

### Features

- **show_hint:** retrieve changes to not conflict with master ([9b5f3d1](https://bitbucket.org/synetics/i-doit-ui-kit/commits/9b5f3d1021dc40c49fb9bc1bb3b1d15f021b1236))

# [5.64.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.63.0...v5.64.0) (2022-07-12)

### Features

- **time_field:** add basic time field ([9f7fe41](https://bitbucket.org/synetics/i-doit-ui-kit/commits/9f7fe41f128dd7d2bf344796ab32ddeae31c4ef2))
- **time_field:** change cursor to pointer ([50477b6](https://bitbucket.org/synetics/i-doit-ui-kit/commits/50477b6f989963b75d0135b8c8d57a38af5c75f2))
- **time_field:** change cursor to pointer ([92747f6](https://bitbucket.org/synetics/i-doit-ui-kit/commits/92747f6314c452c247e72ae3c216e31bc66c019f))
- **time_field:** simplify code ([7568d56](https://bitbucket.org/synetics/i-doit-ui-kit/commits/7568d565b975dacde6d2d21e5e6b9a80ed11911e))
- **time_field:** simplify code ([f9f2208](https://bitbucket.org/synetics/i-doit-ui-kit/commits/f9f22085148fde8ce0c57316717b25684fa62b17))

# [5.63.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.62.0...v5.63.0) (2022-07-07)

# [5.61.0-task-ID2-3423-donut-chart.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.62.0...v5.63.0) (2022-07-06)

# [5.61.0-task-ID2-3423-donut-chart.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.62.0...v5.63.0) (2022-07-06)

### Features

- **chart:** add donut chart ([7e5fdc1](https://bitbucket.org/synetics/i-doit-ui-kit/commits/7e5fdc175546b6d06de70ca468210eb28f396c2c))

# [5.62.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.61.0...v5.62.0) (2022-07-07)

### Features

- **table:** use full width of the table ([8cc0fd2](https://bitbucket.org/synetics/i-doit-ui-kit/commits/8cc0fd275f907e1a8849933c0c582c7e20de085e))
- **whole_border:** make border for whole table width ([d204e49](https://bitbucket.org/synetics/i-doit-ui-kit/commits/d204e49470de8ff214445bd157e2a9974684729b))
- **whole_border:** make border for whole table width ([3a0c3f0](https://bitbucket.org/synetics/i-doit-ui-kit/commits/3a0c3f02c23b1798a3b0fe045118283a44b25586))
- **whole_border:** nake fake cell to complete whole border width ([25d01b1](https://bitbucket.org/synetics/i-doit-ui-kit/commits/25d01b12075bb1af234c5d1e6e5642683a61177c))
- **whole_border:** remove 100% table width ([848278f](https://bitbucket.org/synetics/i-doit-ui-kit/commits/848278f718a07c037876a3b32bd9230f8ea69774))
- **whole_border:** remove 100% table width ([e9444d1](https://bitbucket.org/synetics/i-doit-ui-kit/commits/e9444d1f8d746634908829da34303350cef69729))

# [5.61.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.60.0...v5.61.0) (2022-07-07)

### Features

- **show_hint:** retrieve changes to not conflict with master ([9b5f3d1](https://bitbucket.org/synetics/i-doit-ui-kit/commits/9b5f3d1021dc40c49fb9bc1bb3b1d15f021b1236))

# [5.60.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.60.0-task-ID2-2288-add-column-icon.0...v5.60.0) (2022-06-27)

# [5.60.0-task-ID2-2288-add-column-icon.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.59.1...v5.60.0-task-ID2-2288-add-column-icon.0) (2022-06-27)

### Features

- **icons:** put add column icon ([2afd6cf](https://bitbucket.org/synetics/i-doit-ui-kit/commits/2afd6cfa801d1b91659bfd8e67390e010cea8b48))

## [5.59.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.59.0...v5.59.1) (2022-06-27)

### Bug Fixes

- **menu_focus:** add arrow action for open ([53db0d4](https://bitbucket.org/synetics/i-doit-ui-kit/commits/53db0d49176195b63195dd124ce8b7bf2cec1798))
- **menu_focus:** add arrow right event listener ([df86888](https://bitbucket.org/synetics/i-doit-ui-kit/commits/df868882576bcaa0e5bd89c9020169b764ccdf79))
- **menu_focus:** add mouse hover interaction ([dbc4caa](https://bitbucket.org/synetics/i-doit-ui-kit/commits/dbc4caaad67e15c640f64d7fb2eedab7e7d55480))
- **menu_focus:** adjust keyboard focus handling on menu ([c411a0f](https://bitbucket.org/synetics/i-doit-ui-kit/commits/c411a0f394aef96d34c4ec0986a68dd274172dfe))
- **menu_focus:** correct tests ([bca9aaa](https://bitbucket.org/synetics/i-doit-ui-kit/commits/bca9aaaf2526fca537579403423c62595bcbb70f))

# [5.59.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.58.0...v5.59.0) (2022-06-21)

### Features

- **context-menu:** implementation of the context menu with usages ([451eed5](https://bitbucket.org/synetics/i-doit-ui-kit/commits/451eed5be4bb8dfaa6904d8c7c8842100883f4ce))

# [5.58.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.57.0...v5.58.0) (2022-06-20)

### Features

- **table_expandable:** add expandable example ([b4786a1](https://bitbucket.org/synetics/i-doit-ui-kit/commits/b4786a1245e57702f8fa7a3ffa08f1395b5fc0fa))
- **table_expandable:** add expandable example ([4bf18df](https://bitbucket.org/synetics/i-doit-ui-kit/commits/4bf18df2bf1607b13faeb4e4a40d83025b07417f))
- **table_expandable:** add expandable example ([f0f06b1](https://bitbucket.org/synetics/i-doit-ui-kit/commits/f0f06b161eca0f7a7fd9605cb9e8b73658fb3a30))
- **table_expandable:** change header styling ([a60708f](https://bitbucket.org/synetics/i-doit-ui-kit/commits/a60708fbda8874745c28cbebae076c6a82ec9de2))
- **table_expandable:** feature fix labels and styles ([419b4ab](https://bitbucket.org/synetics/i-doit-ui-kit/commits/419b4ab178acbf83dd49216183839a215174691d))
- **table_expandable:** remove borders ([d5418b2](https://bitbucket.org/synetics/i-doit-ui-kit/commits/d5418b2653b7c074bb42f36902fbf5cff156ed6e))
- **table_expandable:** remove borders ([9a9a937](https://bitbucket.org/synetics/i-doit-ui-kit/commits/9a9a937c0ed9ae07d9a63990cfca76816ab05506))
- **table_expandable:** remove borders ([1361d0c](https://bitbucket.org/synetics/i-doit-ui-kit/commits/1361d0c8c26500c36b73959d6e15bd9adec8f05b))
- **table_expandable:** remove duplicate class ([09e2bc2](https://bitbucket.org/synetics/i-doit-ui-kit/commits/09e2bc2b5469b5c09720a62160a570f7ec5bd36a))

# [5.57.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.56.0...v5.57.0) (2022-06-10)

### Features

- **color_cell:** add colored cell ti kit ([7810786](https://bitbucket.org/synetics/i-doit-ui-kit/commits/7810786f772812521038ff877674cf4cecaee2f7))
- **color_cell:** add colored cell to kit ([c6be163](https://bitbucket.org/synetics/i-doit-ui-kit/commits/c6be163184b153bc7f6033eb03e715f8e84f9379))
- **color_cell:** add colored cell to kit ([d3b6c31](https://bitbucket.org/synetics/i-doit-ui-kit/commits/d3b6c312e3ce79f24ea2930bac5de0a5f200d3d4))
- **color_cell:** return spacings back due to jumping cell ([2e7febe](https://bitbucket.org/synetics/i-doit-ui-kit/commits/2e7febe917dd1e64d92e31a9b50edc6ef535ee90))

# [5.56.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.55.0...v5.56.0) (2022-06-08)

### Features

- **realtime_search:** searchitem add tests ([b7512fe](https://bitbucket.org/synetics/i-doit-ui-kit/commits/b7512fec41cea58b8fc3a1e3955de18bff40fea3))
- **realtime_search:** searchitem fixes ([53d9d5f](https://bitbucket.org/synetics/i-doit-ui-kit/commits/53d9d5fafcbd2e57585255af5bb0508c0a2cce1e))
- **realtime_search:** searchitem fixes ([e57b3fb](https://bitbucket.org/synetics/i-doit-ui-kit/commits/e57b3fb3faa7f66f0cb9ae8e87e28908d14a105f))
- **realtime_search:** searchitem fixes ([87e0b8a](https://bitbucket.org/synetics/i-doit-ui-kit/commits/87e0b8a991e252586b7a567316e5f4ec3082e553))
- **realtime_search:** test coverage increased ([6176473](https://bitbucket.org/synetics/i-doit-ui-kit/commits/6176473de833320c8392365926962b80e03031ae))
- **realtime_search:** wip add realtime search component ([fd798bc](https://bitbucket.org/synetics/i-doit-ui-kit/commits/fd798bc577039411da668c0eb17dab7839c77010))

# [5.55.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.54.1...v5.55.0) (2022-06-02)

### Features

- **table:** add keyboard controls to selectable columns ([278d827](https://bitbucket.org/synetics/i-doit-ui-kit/commits/278d827917137ec16491fbcb65ac512e622c348c))

## [5.54.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.54.0...v5.54.1) (2022-06-02)

## [5.53.1-task-ID2-3350-menu-pulldown-keyboard-nav-fix.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.54.0...v5.54.1) (2022-05-31)

### Bug Fixes

- allow specific custom trigger as focus context ([d8960c1](https://bitbucket.org/synetics/i-doit-ui-kit/commits/d8960c1623eee4049705862b76fbb55ece68e1b6))

# [5.54.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.53.0...v5.54.0) (2022-06-01)

### Features

- **table:** add selectable columns ([07f77ec](https://bitbucket.org/synetics/i-doit-ui-kit/commits/07f77ecf1c171750887f41a863a2d33238d3388a))

## [5.53.1-task-ID2-3350-menu-pulldown-keyboard-nav-fix.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.53.0...v5.53.1-task-ID2-3350-menu-pulldown-keyboard-nav-fix.0) (2022-05-31)

### Bug Fixes

- allow specific custom trigger as focus context ([d8960c1](https://bitbucket.org/synetics/i-doit-ui-kit/commits/d8960c1623eee4049705862b76fbb55ece68e1b6))

# [5.53.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.53.0-feature-ID2-3109-filter-tree.0...v5.53.0) (2022-05-23)

# [5.53.0-feature-ID2-3109-filter-tree.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.52.1...v5.53.0-feature-ID2-3109-filter-tree.0) (2022-05-19)

### Features

- **filter-tree:** add utility to traverse and filter the tree ([6874d0f](https://bitbucket.org/synetics/i-doit-ui-kit/commits/6874d0fee4a425a2a82d035537ede5c437b50059))

## [5.52.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.52.0...v5.52.1) (2022-05-18)

### Bug Fixes

- **release-it:** tag calculation missed underscore ([cec6dda](https://bitbucket.org/synetics/i-doit-ui-kit/commits/cec6dda85317cd78550fcb1a63439ce8eda32f2b))
- **release-it:** underscore problem fixed in powershell script ([9b996e8](https://bitbucket.org/synetics/i-doit-ui-kit/commits/9b996e8e0ed5c44c9e0ef65baee86aedf6613c68))
- sed directive simplified and merged in single regex ([1c03550](https://bitbucket.org/synetics/i-doit-ui-kit/commits/1c035501fead7b6fdafc65c81fc51b17e5511faa))
- sed for npm tag modified ([efd69da](https://bitbucket.org/synetics/i-doit-ui-kit/commits/efd69da9f2c4a0cab98829dcc801fab3da51ab6c))

# [5.52.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.51.0...v5.52.0) (2022-05-13)

### Features

- **table:** add resize to the columns ([2ef6507](https://bitbucket.org/synetics/i-doit-ui-kit/commits/2ef65070490dd7255440b33e44aa706b45838c34))

# [5.51.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.50.1-task-ID2-3287-correct-sort.1...v5.51.0) (2022-05-13)

### Features

- **correct-sort:** resolve user default order ([fc7d819](https://bitbucket.org/synetics/i-doit-ui-kit/commits/fc7d819ad204f99ea15f5612232453e289018320))

## [5.50.1-task-ID2-3287-correct-sort.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.50.0...v5.50.1-task-ID2-3287-correct-sort.1) (2022-05-12)

### Features

- **correct-sort:** change version ([63874a9](https://bitbucket.org/synetics/i-doit-ui-kit/commits/63874a9e465a8cef476c5c2c9583f785cfa8773f))

# [5.50.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.49.5...v5.50.0) (2022-05-12)

### Features

- **correct_sort:** resolve user default order ([c4bad5f](https://bitbucket.org/synetics/i-doit-ui-kit/commits/c4bad5fe872f21207a1c64f3283eb5877f07d9aa))

## [5.49.5](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.49.5-test-windows-pre-release.0...v5.49.5) (2022-05-11)

## [5.49.5-test-windows-pre-release.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.49.4...v5.49.5-test-windows-pre-release.0) (2022-05-11)

### Bug Fixes

- windows-release-script-edit ([6ef1b50](https://bitbucket.org/synetics/i-doit-ui-kit/commits/6ef1b500a06bc8b88cc74b072a65da5428238ed4))

- fix: windows-release-script-edit (39875da)

## [5.49.3](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.49.2...v5.49.3) (2022-05-11)

### Bug Fixes

- windows-release-script-edit ([d78b287](https://bitbucket.org/synetics/i-doit-ui-kit/commits/d78b287a485ebe555e52d90013dee1cd1ee2a891))

## [5.49.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.49.1...v5.49.2) (2022-05-11)

### Bug Fixes

- postinstall removed from scripts ([de6c6fc](https://bitbucket.org/synetics/i-doit-ui-kit/commits/de6c6fcb606a18673e0c50f6226a09bb7991c25b))

## [5.49.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.49.0...v5.49.1) (2022-05-11)

### Bug Fixes

- publich config added to package.json ([2dff0ef](https://bitbucket.org/synetics/i-doit-ui-kit/commits/2dff0ef11699c8e9dd11cc48ba023134765874dc))

# [5.49.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.48.0...v5.49.0) (2022-05-11)

### Features

- **private-registry:** release-it replaces standard-version ([7524493](https://bitbucket.org/synetics/i-doit-ui-kit/commits/75244931db812176269605a7df55ef6f476eb553))

# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [5.48.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.47.0...v5.48.0) (2022-05-10)

### Features

- **radio_select:** create redio item with single select ([29eb062](https://bitbucket.org/synetics/i-doit-ui-kit/commit/29eb062398bee0d5bdeb63fd373922ad7f588798))

## [5.47.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.46.4...v5.47.0) (2022-05-10)

### Features

- **single-select:** add ability to display only the label as selected item ([5cef8e6](https://bitbucket.org/synetics/i-doit-ui-kit/commit/5cef8e6bf908c4af3525664fb9e3586829ae1fbe))

### [5.46.4](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.46.3...v5.46.4) (2022-05-04)

### Bug Fixes

- **table:** fix navigation after tab press ([ab5f8b9](https://bitbucket.org/synetics/i-doit-ui-kit/commit/ab5f8b9f26950c58fafae8bb35d0dc3aeb97d25d))

### [5.46.3](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.46.2...v5.46.3) (2022-04-26)

### Bug Fixes

- label color changed for better readability ([b7e3d4d](https://bitbucket.org/synetics/i-doit-ui-kit/commit/b7e3d4dc2dc41bedaacdab60feffe5f07f9e2589))

### [5.46.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.46.1...v5.46.2) (2022-04-26)

### Bug Fixes

- **single_select:** fix behavior of single select ([0659227](https://bitbucket.org/synetics/i-doit-ui-kit/commit/0659227bf0bbd4a02242e1b34870e377f7f4043b))
- **single_select:** fix behavior of single select ([eb86c23](https://bitbucket.org/synetics/i-doit-ui-kit/commit/eb86c238bdff452530e30fcecec8dfc4cd1be50d))

### [5.46.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.46.0...v5.46.1) (2022-04-22)

### Bug Fixes

- **multiselect_check:** remove behavior fix for checkbox ([eee6461](https://bitbucket.org/synetics/i-doit-ui-kit/commit/eee64610a6b720e61c120f53891709a2e7e19526))

## [5.46.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.45.8...v5.46.0) (2022-04-20)

### Features

- **table:** adjust keyboard navigation for table with unfocusable cells ([f2c5d5e](https://bitbucket.org/synetics/i-doit-ui-kit/commit/f2c5d5e0f4b306186859060ad2ddc56f43d0bc29))

### Bug Fixes

- **inline_color:** add possibility to pass selector as props ([4ea5c98](https://bitbucket.org/synetics/i-doit-ui-kit/commit/4ea5c9865418e49119f242cb2f128de01d81a1bb))
- **inline_color:** correct modifier for using popper in inline edit fields ([36383d1](https://bitbucket.org/synetics/i-doit-ui-kit/commit/36383d1d6dbd52f6146e402ac96cfcef43c3b870))
- **inline_color:** update behavior and tests ([e7081c3](https://bitbucket.org/synetics/i-doit-ui-kit/commit/e7081c3a04c4633662bce0ef9b1fa83c5feb252d))

### [5.45.9](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.45.7...v5.45.9) (2022-04-19)

### Bug Fixes

- **checkbox_fix:** resolve checkbox click ([059bbe8](https://bitbucket.org/synetics/i-doit-ui-kit/commit/059bbe83b3b5e3c9291edcadb94ee2c8b2a60e47))

### [5.45.8](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.45.6...v5.45.8) (2022-04-11)

### Bug Fixes

- **button:** allowing buttons to be focusable with tooltip in disabled state ([afc135d](https://bitbucket.org/synetics/i-doit-ui-kit/commit/afc135dfde6b49a1c81bfb077bc35c7c11d1ed70))
- **checkbox_fix:** resolve checkbox click ([059bbe8](https://bitbucket.org/synetics/i-doit-ui-kit/commit/059bbe83b3b5e3c9291edcadb94ee2c8b2a60e47))

### [5.45.7](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.45.6...v5.45.7) (2022-04-07)

### Bug Fixes

- **button:** allowing buttons to be focusable with tooltip in disabled state ([afc135d](https://bitbucket.org/synetics/i-doit-ui-kit/commit/afc135dfde6b49a1c81bfb077bc35c7c11d1ed70))

### [5.45.6](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.45.5...v5.45.6) (2022-04-06)

### Bug Fixes

- backspace handling reverted ([bc5c2d0](https://bitbucket.org/synetics/i-doit-ui-kit/commit/bc5c2d02e177b107e4449d94888cfcc8d221d2ef))
- backspace will move focus to next available element like enter ([4ff386e](https://bitbucket.org/synetics/i-doit-ui-kit/commit/4ff386e9a5bc6574424e991f2e51269949d03d9f))
- next focused index calculation adjusted to allow removal of last tag ([331c30f](https://bitbucket.org/synetics/i-doit-ui-kit/commit/331c30f93f1405de7a4e5eacdbea6db89285dcf5))
- removing tags via mouse click will focus form field afterwards ([58124ea](https://bitbucket.org/synetics/i-doit-ui-kit/commit/58124ea62a3a7753b7be8969f965c492407c3986))
- test for useTagItemsFocus fixed ([1b558a4](https://bitbucket.org/synetics/i-doit-ui-kit/commit/1b558a4cdbebf0a76f5e5fd7229005b0b29ea371))
- unnecessary delay for focus removed ([8380457](https://bitbucket.org/synetics/i-doit-ui-kit/commit/838045760a77f80ddbc1174cf51e873c3edb3925))

### [5.45.5](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.45.4...v5.45.5) (2022-04-04)

### Bug Fixes

- **table:** disable sorting for action column ([1447591](https://bitbucket.org/synetics/i-doit-ui-kit/commit/14475911dae25358c4362ec6a5be79f2fbaee7e0))

### [5.45.4](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.45.3...v5.45.4) (2022-03-31)

### Bug Fixes

- **pulldown_overflow:** add neede modifier to overflow ([67aad4b](https://bitbucket.org/synetics/i-doit-ui-kit/commit/67aad4b05d348c9ff17091e5f7f101ea807178da))

### [5.45.3](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.45.2...v5.45.3) (2022-03-28)

### [5.45.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.45.1...v5.45.2) (2022-03-22)

### Bug Fixes

- **fix_tooltip_animation:** add stories and tests for coverage ([b482af0](https://bitbucket.org/synetics/i-doit-ui-kit/commit/b482af05b2c617c16a329c48edf267addc370fc8))
- **fix_tooltip_animation:** add stories and tests for coverage ([eb8a03b](https://bitbucket.org/synetics/i-doit-ui-kit/commit/eb8a03b5141831ad2049a8bbb76d02cd6c932667))
- **fix_tooltip_animation:** tooltip animation fixes ([066a5ff](https://bitbucket.org/synetics/i-doit-ui-kit/commit/066a5fff1e1f030a41882eb4d16c8c0e0f406cd9))

### [5.45.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.45.0...v5.45.1) (2022-03-17)

### Bug Fixes

- **calendar_tooltip:** fix tooltip on open state ([104354a](https://bitbucket.org/synetics/i-doit-ui-kit/commit/104354a253a850188bdecd5030d8426c798a413f))
- **calendar_tooltip:** fix tooltip on open state ([62dfb21](https://bitbucket.org/synetics/i-doit-ui-kit/commit/62dfb218c445b231cdbeb7400c8b02eb394e7180))
- **calendar_tooltip:** fix tooltip on open state ([de7b88d](https://bitbucket.org/synetics/i-doit-ui-kit/commit/de7b88d8a9b76483057148cb315c2d5d172b7791))

## [5.45.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.44.14...v5.45.0) (2022-03-14)

### Features

- **spacer_component:** add container in styles ([7c46f90](https://bitbucket.org/synetics/i-doit-ui-kit/commit/7c46f90f94453ba14026bd2f277d7fd7cea695fb))
- **spacer_component:** add container spacer component for buttons ([4133d5e](https://bitbucket.org/synetics/i-doit-ui-kit/commit/4133d5e48ccabc8316437b94bead32ff1ab69511))
- **spacer_component:** remove scss for story ([28fc69c](https://bitbucket.org/synetics/i-doit-ui-kit/commit/28fc69c4ed3a01f5a1fa571e64a196a93d7ace8e))

### [5.44.14](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.44.13...v5.44.14) (2022-03-10)

### Bug Fixes

- **checkbox_border:** fix click on border ([8f247ee](https://bitbucket.org/synetics/i-doit-ui-kit/commit/8f247ee65c73c636ec0cf1b8dbba1ff339e434a3))

### [5.44.13](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.44.12...v5.44.13) (2022-03-09)

### Bug Fixes

- **fix_popper_position:** change open strategy for other places ([3481e94](https://bitbucket.org/synetics/i-doit-ui-kit/commit/3481e945f74abde8157b82dcb42298e9bfc694ba))
- **fix_popper_position:** fix tests ([bfe88c7](https://bitbucket.org/synetics/i-doit-ui-kit/commit/bfe88c7951e3308662349716d75bc6abe0de80e6))
- **fix_popper_position:** prevent repositioning bug in popper ([e62a27b](https://bitbucket.org/synetics/i-doit-ui-kit/commit/e62a27b1b1eb5d4b03b6daa53e83a566f018a4ba))

### [5.44.12](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.44.11...v5.44.12) (2022-03-03)

### [5.44.11](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.44.10...v5.44.11) (2022-03-03)

### Bug Fixes

- **tooltip:** fix the overflow of the container ([4dfd096](https://bitbucket.org/synetics/i-doit-ui-kit/commit/4dfd0969aff7557b98c1667fb6d3ae907790ff80))

### [5.44.10](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.44.9...v5.44.10) (2022-03-03)

### Bug Fixes

- **tooltip:** fix the word breaking for tooltip ([897e5bc](https://bitbucket.org/synetics/i-doit-ui-kit/commit/897e5bc6485dc0d1b255d061906b252d63805207))

### [5.44.9](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.44.8...v5.44.9) (2022-02-23)

### Bug Fixes

- **form-field:** fix dimensions of field group ([1c69a34](https://bitbucket.org/synetics/i-doit-ui-kit/commit/1c69a34686c73455d2503e914bae47ddb6e2289a))

### [5.44.8](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.44.7...v5.44.8) (2022-02-22)

### Bug Fixes

- **pulldown:** allow sorting of values with numeric values ([2f1f144](https://bitbucket.org/synetics/i-doit-ui-kit/commit/2f1f1447a277ab4f4d809fe06dcfef1e835a3632))

### [5.44.7](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.44.6...v5.44.7) (2022-02-17)

### Bug Fixes

- **tooltip:** fix size problem ([63236f8](https://bitbucket.org/synetics/i-doit-ui-kit/commit/63236f80e789e3884cef417ae546ec63b8dfa09d))

### [5.44.6](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.44.5...v5.44.6) (2022-02-17)

### Bug Fixes

- **close_picker:** fix closepicker bug, adjust tests ([7c3d22d](https://bitbucket.org/synetics/i-doit-ui-kit/commit/7c3d22d603e0c6890f869f9909627357cb05bf7b))

### [5.44.5](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.44.4...v5.44.5) (2022-02-11)

### Bug Fixes

- **calendar:** validate the date on enter press ([23cd9dd](https://bitbucket.org/synetics/i-doit-ui-kit/commit/23cd9dd37fa1033a8e3d8d7452b273a02551bc96))

### [5.44.4](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.44.3...v5.44.4) (2022-02-09)

### Bug Fixes

- **checkboxes:** set check icon z-index to auto ([c16e10d](https://bitbucket.org/synetics/i-doit-ui-kit/commit/c16e10dc4b4f750881278f2465e8bcae33cae2ee))

### [5.44.3](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.44.2...v5.44.3) (2022-02-09)

### Bug Fixes

- **nested-list:** delete group from StoryBook ([6671b73](https://bitbucket.org/synetics/i-doit-ui-kit/commit/6671b739ccf5b2c786fba20c5d01503cff02d24f))

### [5.44.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.44.1...v5.44.2) (2022-02-08)

### Bug Fixes

- **modal:** fix positioning of the modal ([e7108d3](https://bitbucket.org/synetics/i-doit-ui-kit/commit/e7108d35335ac9e28fa635ff39574346fb59cbd5))
- **modal:** move focus trap on the top level ([7f32af6](https://bitbucket.org/synetics/i-doit-ui-kit/commit/7f32af602f4f9b0a00f744ea87f85ed9c9941323))
- **modal:** set the click handler on button, be able to pass the custom label ([ee1cad6](https://bitbucket.org/synetics/i-doit-ui-kit/commit/ee1cad63135d94f94ae8fa366b7f335e53e337b7))

### [5.44.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.44.0...v5.44.1) (2022-02-02)

### Bug Fixes

- **tooltip:** remove max-height ([98385a0](https://bitbucket.org/synetics/i-doit-ui-kit/commit/98385a0481d4adec44fbe4f3dcfdb89841c89dd6))

## [5.44.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.43.0...v5.44.0) (2022-02-02)

### Features

- **focus:** create use-focus-within hook ([d8097bd](https://bitbucket.org/synetics/i-doit-ui-kit/commit/d8097bdb21adad70ebb2cbbde596a02cb54ef1ac))

### Bug Fixes

- **checkbox:** fix clicking on checkbox ([1b1c883](https://bitbucket.org/synetics/i-doit-ui-kit/commit/1b1c88349f0ad39ac8ccacdc6bb40ea117cde157))
- **multi-select:** fix behaviour of multi-select pulldown ([c3249f1](https://bitbucket.org/synetics/i-doit-ui-kit/commit/c3249f1a3da62356cf11e17b6f4a72d95d86be2f))
- **multi-select:** fix focus handling of multi select combobox ([a4f9871](https://bitbucket.org/synetics/i-doit-ui-kit/commit/a4f9871be81bd8facd0e955556d38c9a7c9a04d0))
- **multi-select:** handle escape manually to prevent its propagation ([9894047](https://bitbucket.org/synetics/i-doit-ui-kit/commit/9894047944b52274660f09f39bf9e6efd6a82a27))
- **pulldown:** fix focus management ([a6d9dbf](https://bitbucket.org/synetics/i-doit-ui-kit/commit/a6d9dbf2eb3aba708ee1afd8f083626d76c7ccfd))
- **single-select:** fix focus handling of single select pulldown ([9d3e9ae](https://bitbucket.org/synetics/i-doit-ui-kit/commit/9d3e9ae7e8b8f81190277c061ebaec58627f160a))
- **single-select:** fix focus of single select combobox ([f9dba40](https://bitbucket.org/synetics/i-doit-ui-kit/commit/f9dba40367ab72043be6f2e0c1701c0575cb9319))

## [5.43.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.42.0...v5.43.0) (2022-02-01)

### Features

- **list_item:** Read-only state for list item ([ed3febc](https://bitbucket.org/synetics/i-doit-ui-kit/commit/ed3febc56b965211036c298cb83f1b092682ea64))

## [5.42.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.41.0...v5.42.0) (2022-02-01)

### Features

- **modal_focus:** add focus state to modal component ([250dafb](https://bitbucket.org/synetics/i-doit-ui-kit/commit/250dafbefa71a6f1059f68d0280f06744cebcfb3))
- **modal_focus:** add modal wrapper bottom line ([ab43371](https://bitbucket.org/synetics/i-doit-ui-kit/commit/ab43371edf49f3e905c4f4292c0e1f5adf2e14b3))
- **modal_focus:** add stories and test for user focus option ([c8bc9cd](https://bitbucket.org/synetics/i-doit-ui-kit/commit/c8bc9cd5e1bc46d2ef33d1bf5ffc9bcea5645ff0))
- **modal_focus:** propagate focus options to modal ([a99beaa](https://bitbucket.org/synetics/i-doit-ui-kit/commit/a99beaac5e480b81a1a52fc96812b9318ec389b0))
- **modal_focus:** return focus to button elem ([70453ba](https://bitbucket.org/synetics/i-doit-ui-kit/commit/70453bafd7da1d1b95d0a6afad4abcd57771e613))
- **modal_focus:** return focus to button elem ([7438823](https://bitbucket.org/synetics/i-doit-ui-kit/commit/7438823346a12b8f075e3ab2ecfb8d257e00e0ad))

## [5.41.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.40.0...v5.41.0) (2022-01-31)

### Features

- **button:** add the possibility to set contrast color ([324d0db](https://bitbucket.org/synetics/i-doit-ui-kit/commit/324d0dbe5464a9766f2ab479b245b188e2ee621c))

### [5.40.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.40.0...v5.40.1) (2022-01-31)

## [5.40.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.39.2...v5.40.0) (2022-01-26)

### Features

- **focus_select:** fix focus if no much items ([aa44292](https://bitbucket.org/synetics/i-doit-ui-kit/commit/aa4429288d567c5dbefd599627bd1b1852248bd6))

### Bug Fixes

- **focus_select:** add fix for tag items ([5ce1d87](https://bitbucket.org/synetics/i-doit-ui-kit/commit/5ce1d87aa87027d193ffe7fab30df290a311efdc))
- **focus_select:** focusing two multiply instances ([d9b384b](https://bitbucket.org/synetics/i-doit-ui-kit/commit/d9b384bdcbcd677c350b18c86db90f699e30c7ec))
- **focus_select:** update focus on one field instance ([2cc6b2a](https://bitbucket.org/synetics/i-doit-ui-kit/commit/2cc6b2a9313c1342a09556ecbcc7665da6482e1e))
- update unfocusable columns in table header ([10735bc](https://bitbucket.org/synetics/i-doit-ui-kit/commit/10735bc7a3e24f8b2d0e74e2c3098658d04002d7))

### [5.39.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.39.1...v5.39.2) (2022-01-20)

### Bug Fixes

- **calendar:** fix error handling ([729172c](https://bitbucket.org/synetics/i-doit-ui-kit/commit/729172c1039eda27e2a485465591b8e7b3b731aa))

### [5.39.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.39.0...v5.39.1) (2022-01-17)

### Bug Fixes

- **calendar:** fix popper position ([f040157](https://bitbucket.org/synetics/i-doit-ui-kit/commit/f040157cd435bb58f6c9c760a67217f5ae9a869d))

## [5.39.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.38.5...v5.39.0) (2022-01-12)

### Features

- **selector_item:** add focus state and esc handle ([c52e788](https://bitbucket.org/synetics/i-doit-ui-kit/commit/c52e788efc4bfd0cd7b46b57069b86762fce1113))
- **selector_item:** add selector item to menu ([913be3b](https://bitbucket.org/synetics/i-doit-ui-kit/commit/913be3b670321c8574a3415d2c03d26efaa4e26f))

### [5.38.5](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.38.4...v5.38.5) (2022-01-05)

### Bug Fixes

- **tooltip:** fix modal backdrop styles by using bootstrap z-index variables ([231ede6](https://bitbucket.org/synetics/i-doit-ui-kit/commit/231ede6a23d8d6c4bd079abfdd2023d111314678))
- **tooltip:** fix tooltip styles by using bootstrap z-index variables ([4ff74a5](https://bitbucket.org/synetics/i-doit-ui-kit/commit/4ff74a5534566f69ff886d69dd13a4a0da5e9ce1))

### [5.38.4](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.38.3...v5.38.4) (2022-01-04)

### Bug Fixes

- **text_area:** fix readonly state ([123511f](https://bitbucket.org/synetics/i-doit-ui-kit/commit/123511f1af7d7bea6821090aa15faddbccdf2608))
- **text-area:** fix readonly state ([134e291](https://bitbucket.org/synetics/i-doit-ui-kit/commit/134e2912c3ba0ee8f228c154ca826522273449c6))

### [5.38.3](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.38.2...v5.38.3) (2022-01-03)

### [5.38.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.38.1...v5.38.2) (2021-12-27)

### [5.38.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.38.0...v5.38.1) (2021-12-23)

### Bug Fixes

- **calendar_use:** import styles with component ([9242bcd](https://bitbucket.org/synetics/i-doit-ui-kit/commit/9242bcdc3f65afd5b8eb5bb3e4cdf3bbef2b25b1))
- **datepicker_use:** fix usage component in 3rd party ([5de7f62](https://bitbucket.org/synetics/i-doit-ui-kit/commit/5de7f629d75205a137cd52763e1abde15dfdd0a1))
- **datepicker_use:** fix using styles ([512627d](https://bitbucket.org/synetics/i-doit-ui-kit/commit/512627dc6016a231c2a5d87a32f0e3cbc1608085))

## [5.38.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.37.8...v5.38.0) (2021-12-21)

### Features

- **readonly_screenreader:** fixsingle select pulldown and combobox ([483c747](https://bitbucket.org/synetics/i-doit-ui-kit/commit/483c7476804de29a2eed1a32b2651d375570cf18))

### Bug Fixes

- **readonly_screenreader:** fix focus state for screenreaders ([2ec8f66](https://bitbucket.org/synetics/i-doit-ui-kit/commit/2ec8f660327a291c959323e423dcaa016a325cd8))

### [5.37.8](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.37.7...v5.37.8) (2021-12-15)

### Bug Fixes

- **pulldown:** render menu even if its not open, pass pulldownContainer ([982f80e](https://bitbucket.org/synetics/i-doit-ui-kit/commit/982f80ea9dda05bcd44c9a393523c4654d1206fc))
- **readonly_combobox:** fix disabled ([4d602ef](https://bitbucket.org/synetics/i-doit-ui-kit/commit/4d602ef3b4e66c6c3f9a1afd76f7ddd980f6318f))
- **readonly_combobox:** fixed readonly combobox state ([40c5deb](https://bitbucket.org/synetics/i-doit-ui-kit/commit/40c5deb076594be3625cbef70dc8d836f76906f2))

### [5.37.7](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.37.6...v5.37.7) (2021-12-14)

### Bug Fixes

- **node:** change node version ([7f6f89a](https://bitbucket.org/synetics/i-doit-ui-kit/commit/7f6f89a27dd470fabfa8db17e9f7a45e66ca6a4e))

### [5.37.6](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.37.5...v5.37.6) (2021-12-14)

### Bug Fixes

- **combobox:** fix bugs in multi-select-combobox ([4572c34](https://bitbucket.org/synetics/i-doit-ui-kit/commit/4572c34190e238a84c79add0ce227929d721b626))

### [5.37.5](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.37.4...v5.37.5) (2021-12-10)

### [5.37.4](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.37.3...v5.37.4) (2021-12-09)

### Bug Fixes

- **modal:** add the possibility to pass classes ([aa95ac0](https://bitbucket.org/synetics/i-doit-ui-kit/commit/aa95ac02e77c6e48494bd91193dc692480c024a7))
- **select:** be able to pass container for pulldown ([dc66e7f](https://bitbucket.org/synetics/i-doit-ui-kit/commit/dc66e7f40ccb9ea360a5c087150a3a07c7c01756))

### [5.37.3](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.37.2...v5.37.3) (2021-12-08)

### Bug Fixes

- **readonly_focus:** change input tab priority ([c762dcd](https://bitbucket.org/synetics/i-doit-ui-kit/commit/c762dcdd26c2de6d155b433e9467b3972b596bc0))
- **readonly_focus:** fix wrong readonly state ([86cef66](https://bitbucket.org/synetics/i-doit-ui-kit/commit/86cef668977e9c57206391f789e2fd2150915aa0))

### [5.37.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.37.1...v5.37.2) (2021-12-01)

### Bug Fixes

- **combobox:** open combobox on input ([7c0540c](https://bitbucket.org/synetics/i-doit-ui-kit/commit/7c0540c6a9421fb2e827cf65adc4677b68204e03))

### [5.37.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.37.0...v5.37.1) (2021-12-01)

### Bug Fixes

- **combobox:** fix bugs in single-select-combobox ([1acfb7b](https://bitbucket.org/synetics/i-doit-ui-kit/commit/1acfb7b1a78f5baa1576cfd8fef2e2cee4a08c56))

## [5.37.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.36.1...v5.37.0) (2021-12-01)

### Features

- **add_icon:** add icon for export ready ([9c79b01](https://bitbucket.org/synetics/i-doit-ui-kit/commit/9c79b0134fb06600139666c4e327525acd54bbf0))
- **table-row-view:** add escape handler ([d8a224a](https://bitbucket.org/synetics/i-doit-ui-kit/commit/d8a224a354c455de687b5980d8aad809ea5861ef))
- **table-row-view:** create a row view for table footer ([ac4cf7f](https://bitbucket.org/synetics/i-doit-ui-kit/commit/ac4cf7f2dedd0a49bfe07a7c6b3ec120da47d622))
- **table-row-view:** create a row view for table footer ([a8103c7](https://bitbucket.org/synetics/i-doit-ui-kit/commit/a8103c70582b2f574b9e7a724af5d52c26f7d636))
- **table-row-view:** create a row view for table footer ([b0e51d7](https://bitbucket.org/synetics/i-doit-ui-kit/commit/b0e51d7f0f4a7e2b310f5e75bce44daa903522e1))
- **table-row-view:** edit Props ([409a1a7](https://bitbucket.org/synetics/i-doit-ui-kit/commit/409a1a75fc7997348bdcd6ce1b722aec18439a2f))
- **table-row-view:** edit the pagination and the row view style ([651dda0](https://bitbucket.org/synetics/i-doit-ui-kit/commit/651dda0ab659837ef2ed98aa158d728a28256a11))
- **table-row-view:** edit the test ([c548ad5](https://bitbucket.org/synetics/i-doit-ui-kit/commit/c548ad5e804d87c950c58b8e76fbb6b1f63862e3))
- **table-row-view:** edit the test ([d9e613c](https://bitbucket.org/synetics/i-doit-ui-kit/commit/d9e613c00eba2a0acb552edf3c1e768f5d5bb3ac))

### [5.36.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.36.0...v5.36.1) (2021-11-30)

### Bug Fixes

- **calendar_focus:** not open popper on focus ([f4fbf47](https://bitbucket.org/synetics/i-doit-ui-kit/commit/f4fbf478a80b5a077e302ea7a333fedc3fecd1c8))

## [5.36.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.35.3...v5.36.0) (2021-11-30)

### Features

- **table_select:** use includes, remove comment ([cbd2a7e](https://bitbucket.org/synetics/i-doit-ui-kit/commit/cbd2a7e83f951d7331dac2a84eb5a63328fbe85c))
- **table_select:** use single-select context ([895d237](https://bitbucket.org/synetics/i-doit-ui-kit/commit/895d23782d64a6a9d491d86c7904e6205a0364b6))

### [5.35.3](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.35.2...v5.35.3) (2021-11-30)

### [5.35.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.35.1...v5.35.2) (2021-11-29)

### Bug Fixes

- **pulldown:** focus problem on simultanious update ([a4d6b4b](https://bitbucket.org/synetics/i-doit-ui-kit/commit/a4d6b4b1e5d174933eafc8d98841c69c1b56152f))

### [5.35.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.35.0...v5.35.1) (2021-11-25)

### Bug Fixes

- multiselect behaves uncontrolled and does not react on value changes ([31ad9b4](https://bitbucket.org/synetics/i-doit-ui-kit/commit/31ad9b4fd8964efccc93363131c5b72c320f69a7))
- row selection by clicking fixed in table ([3147a05](https://bitbucket.org/synetics/i-doit-ui-kit/commit/3147a057fa2b20e321e909b65207231d258a2e81))
- selection checkbox will react on first click now ([c43ee69](https://bitbucket.org/synetics/i-doit-ui-kit/commit/c43ee6953f040b9b3850a91834b2bebc3e4c4012))

## [5.35.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.34.3...v5.35.0) (2021-11-25)

### Features

- **calendar_header:** add custom calendar header ([02cde3f](https://bitbucket.org/synetics/i-doit-ui-kit/commit/02cde3f50d7a47b6f7cb3dbeb6cad4270a00444c))
- **calendar_header:** add custom calendar header ([ae86b05](https://bitbucket.org/synetics/i-doit-ui-kit/commit/ae86b054af93b9d67f75d00df4529d14c78dba08))
- **calendar_header:** add custom calendar header ([d291044](https://bitbucket.org/synetics/i-doit-ui-kit/commit/d291044eb0dcc31dd6acc130b13c3870d33b5661))
- **calendar_header:** add custom calendar header ([7575d70](https://bitbucket.org/synetics/i-doit-ui-kit/commit/7575d700fbd0a12e6dc6d913a00ef89224ff342d))
- **calendar_header:** adjust styles, test added, proper null handling ([32ade72](https://bitbucket.org/synetics/i-doit-ui-kit/commit/32ade720ba03add4367144c61e9235843c675d62))
- **calendar_header:** correct test for test instance ([6606ca7](https://bitbucket.org/synetics/i-doit-ui-kit/commit/6606ca73a3b7ffd7b97fe75a03c3916d3e601e9f))
- **calendar_header:** correct test for test instance ([93837b4](https://bitbucket.org/synetics/i-doit-ui-kit/commit/93837b4cf2ba61dca868eba937548bcbe6e1a9c5))
- **calendar_header:** increase story height ([389e411](https://bitbucket.org/synetics/i-doit-ui-kit/commit/389e411ede1244dab013a85ec094f3936794c520))
- **calendar_header:** remove cycle import ([d4897aa](https://bitbucket.org/synetics/i-doit-ui-kit/commit/d4897aadca6a5e6fe2f591a968cd7cb451c2aea7))
- **calendar_header:** remove variable duplication ([e9be91f](https://bitbucket.org/synetics/i-doit-ui-kit/commit/e9be91fba9553eef1738127f2f484e90a4b1dd16))

### [5.34.3](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.34.2...v5.34.3) (2021-11-25)

### Bug Fixes

- **button:** add a 'type' property to the button component ([9323576](https://bitbucket.org/synetics/i-doit-ui-kit/commit/932357625b1de92289c894a82d8b1774304d46d2))

### [5.34.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.34.1...v5.34.2) (2021-11-25)

### Bug Fixes

- **input:** adding 'date' type ([8348595](https://bitbucket.org/synetics/i-doit-ui-kit/commit/83485953d9a1aef947a40e361852876229298bc2))

### [5.34.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.34.0...v5.34.1) (2021-11-24)

### Bug Fixes

- misbehaviour of selection helper fixed if there are no available ids ([1144272](https://bitbucket.org/synetics/i-doit-ui-kit/commit/1144272b6b172ff005b13eb61377352cefad6524))

## [5.34.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.33.2...v5.34.0) (2021-11-16)

### Features

- **table-header-controls:** add keyboard controls to table header ([1436732](https://bitbucket.org/synetics/i-doit-ui-kit/commit/1436732170015619006e7dbd9171354846bf728f))

### [5.33.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.33.1...v5.33.2) (2021-11-12)

### Bug Fixes

- **open_action_popup:** changing event for focus handling ([81a2967](https://bitbucket.org/synetics/i-doit-ui-kit/commit/81a2967a1887b7ad1833a5aaa1a18b9f4df8a11f))
- **open_action_popup:** fix focusable in all places ([de0adfc](https://bitbucket.org/synetics/i-doit-ui-kit/commit/de0adfc37389c151f5cc2e790583c75c52074ccd))
- **open_action_popup:** fix tests coverage ([d795660](https://bitbucket.org/synetics/i-doit-ui-kit/commit/d795660dd4ee5e51fa3d8346ac515bdab925360f))

### [5.33.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.33.0...v5.33.1) (2021-11-12)

## [5.33.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.32.0...v5.33.0) (2021-11-11)

### Features

- **dnd:** initial implementation of drag n drop for lists ([47735fb](https://bitbucket.org/synetics/i-doit-ui-kit/commit/47735fb08aadd6d1f5262a13aac881dd05a341ac))

## [5.32.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.31.1...v5.32.0) (2021-11-09)

### Features

- **combobox-multiselect:** add combobox multiselect ([0da5b3d](https://bitbucket.org/synetics/i-doit-ui-kit/commit/0da5b3d5c933733951bbd9d02fea80edd6e38a61))

### [5.31.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.31.0...v5.31.1) (2021-11-04)

### Bug Fixes

- fieldgroup spacing fix ([6a0b8d1](https://bitbucket.org/synetics/i-doit-ui-kit/commit/6a0b8d19bf3ae83a2585f8683038a7135a727b0f))
- fix spacing of first extra field in group ([c448ba8](https://bitbucket.org/synetics/i-doit-ui-kit/commit/c448ba81530b016fed5cafd9da4e6658709bf00f))
- left margin of fieldgroup and formfield decreased to 12px ([2bc697c](https://bitbucket.org/synetics/i-doit-ui-kit/commit/2bc697c096e149fed378ea6e5de24030fe16bfc5))
- spacing of label and underline fixed ([dca9608](https://bitbucket.org/synetics/i-doit-ui-kit/commit/dca960804594e70233bbafbf53ea069c7393e1b4))

## [5.31.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.30.0...v5.31.0) (2021-11-03)

### Features

- **fix_popper:** calc popper position when it appears ([73f4cbf](https://bitbucket.org/synetics/i-doit-ui-kit/commit/73f4cbf0d82af494f59c253ed5ee0983e73b3783))
- **fix_popper:** calc popper position when it appears ([29a3789](https://bitbucket.org/synetics/i-doit-ui-kit/commit/29a3789673c1aeafb0376046a892d6d08b487783))

## [5.30.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.29.3...v5.30.0) (2021-11-03)

### Features

- **loading-modal:** add loading state in modal ([2d62907](https://bitbucket.org/synetics/i-doit-ui-kit/commit/2d629074c2ee9a9e245ec2079afda48c5e3c9247))

### [5.29.3](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.29.2...v5.29.3) (2021-11-02)

### [5.29.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.29.1...v5.29.2) (2021-10-27)

### [5.29.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.29.0...v5.29.1) (2021-10-26)

## [5.29.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.28.0...v5.29.0) (2021-10-25)

### Features

- **error-modal:** add error state in modal ([58a66d9](https://bitbucket.org/synetics/i-doit-ui-kit/commit/58a66d9fede10f83fe28edb12d78330eea3e5f92))

## [5.28.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.27.1...v5.28.0) (2021-10-22)

### Features

- **multiselect_pulldown:** add icons stories ([9eab162](https://bitbucket.org/synetics/i-doit-ui-kit/commit/9eab1621aefad461d2549151bf1b6ed875c08138))
- **multiselect_pulldown:** add items highlighting ([76eab9d](https://bitbucket.org/synetics/i-doit-ui-kit/commit/76eab9d8d29281c3bd8a2f472e1f9f2ad6a26b67))
- **multiselect_pulldown:** add items highlighting ([17b0822](https://bitbucket.org/synetics/i-doit-ui-kit/commit/17b0822ec00e41afb3b76dc4d944c0ce27970f26))
- **multiselect_pulldown:** add multiselect pulldown ([e0dfe0c](https://bitbucket.org/synetics/i-doit-ui-kit/commit/e0dfe0c6c81032dcb86468bf16ad20a5f15b4b48))
- **multiselect_pulldown:** add multiselect pulldown ([87a6dde](https://bitbucket.org/synetics/i-doit-ui-kit/commit/87a6ddefec1b748e66067e98e7f899d42f89f2a9))
- **multiselect_pulldown:** add multiselect pulldown ([3e09999](https://bitbucket.org/synetics/i-doit-ui-kit/commit/3e0999966ac4eed058dce1b07a65af9ef1661d07))
- **multiselect_pulldown:** add tests coverage improvement ([133a7bf](https://bitbucket.org/synetics/i-doit-ui-kit/commit/133a7bfc67fd5ebeec3199b4c9e6103b9a28c2cc))
- **multiselect_pulldown:** change tests, resolve bug with scroll keyboard ([74592a6](https://bitbucket.org/synetics/i-doit-ui-kit/commit/74592a64a66ac1868aeadd321434876571af5c1d))
- **multiselect_pulldown:** cover pulldown with tests ([5b3d30f](https://bitbucket.org/synetics/i-doit-ui-kit/commit/5b3d30f5f8b0c1eff8d00e1bfc1341eba5ca9674))
- **multiselect_pulldown:** decompose tags component ([ab4d7e7](https://bitbucket.org/synetics/i-doit-ui-kit/commit/ab4d7e7f399f7786d52641150c2c6c540998c54a))
- **multiselect_pulldown:** decompose tags component ([e654010](https://bitbucket.org/synetics/i-doit-ui-kit/commit/e654010a48c66090180d206e9d8f65729b0479c5))
- **multiselect_pulldown:** fix resetting focus index ([b2c04a2](https://bitbucket.org/synetics/i-doit-ui-kit/commit/b2c04a2131629a96a58cb666ab92b8cc429ded62))
- **multiselect_pulldown:** fix resetting focus index ([8da8685](https://bitbucket.org/synetics/i-doit-ui-kit/commit/8da8685c2b2d343a7c9d904d01f21315489bcec2))
- **multiselect_pulldown:** increase code coverage ([ac3fe76](https://bitbucket.org/synetics/i-doit-ui-kit/commit/ac3fe76581ba1ca403e5a548288955fc82fbb112))
- **multiselect_pulldown:** increase code coverage ([7937f59](https://bitbucket.org/synetics/i-doit-ui-kit/commit/7937f59c1ff4b9482d7ec76310f7bc7fef7bd351))
- **multiselect_pulldown:** make pulldown work with selectable items ([b589aab](https://bitbucket.org/synetics/i-doit-ui-kit/commit/b589aab392177d35b1a2c6204c8d96bba5ad1696))
- **multiselect_pulldown:** move tagitem type, adjust stories ([84ce362](https://bitbucket.org/synetics/i-doit-ui-kit/commit/84ce3628331689a7919cda0bd64625741c591543))
- **multiselect_pulldown:** remove missleading operator ([b627a14](https://bitbucket.org/synetics/i-doit-ui-kit/commit/b627a149003a5de5558248e652806ef87eb4240f))
- **multiselect_pulldown:** remove old tags component ([539fbd0](https://bitbucket.org/synetics/i-doit-ui-kit/commit/539fbd0aa2bf145bec84fe51f69642080a1533ac))
- **multiselect_pulldown:** resolved naming and modify tests ([e794d53](https://bitbucket.org/synetics/i-doit-ui-kit/commit/e794d53ab7ed6a193dbc7567fd96a1f4b12f0248))
- **multiselect_pulldown:** retrieve test cases for coverage ([1675eed](https://bitbucket.org/synetics/i-doit-ui-kit/commit/1675eedb06f2f1469b87fbe57dbdf0e409c1b8eb))
- **multiselect_pulldown:** retrieve test cases for coverage ([f3492ab](https://bitbucket.org/synetics/i-doit-ui-kit/commit/f3492ab42a02bf2e193ff120d6b21ddd3190dee3))

### [5.27.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.27.0...v5.27.1) (2021-10-21)

## [5.27.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.26.0...v5.27.0) (2021-10-21)

### Features

- **combobox:** add combobox single select ([12ecc9f](https://bitbucket.org/synetics/i-doit-ui-kit/commit/12ecc9fda17a4fea81d7c8e8ab9628a2e9f373a2))

## [5.26.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.25.0...v5.26.0) (2021-10-20)

### Features

- **nested-list:** add NestedList ([66ff70d](https://bitbucket.org/synetics/i-doit-ui-kit/commit/66ff70d064428fe6181b5c194894501b293e4329))

## [5.25.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.24.1...v5.25.0) (2021-10-19)

### Features

- **banner:** add tests ([3ed4fef](https://bitbucket.org/synetics/i-doit-ui-kit/commit/3ed4fef1649b32aba2874b61d62fea1aced4a896))
- **banner:** create banner component ([9339a0b](https://bitbucket.org/synetics/i-doit-ui-kit/commit/9339a0bf44553f36935d230b93a26f1c7b5b235f))
- **banner:** create banner component ([7bb06a1](https://bitbucket.org/synetics/i-doit-ui-kit/commit/7bb06a11da83b89caa66972dda25621a550f9a6b))
- **banner:** edit the banner style ([eb1e6b5](https://bitbucket.org/synetics/i-doit-ui-kit/commit/eb1e6b5ae8b32b5a1541867a33c2c4570baa4c0f))
- **banner:** edit the banner style ([fb34aec](https://bitbucket.org/synetics/i-doit-ui-kit/commit/fb34aec13eec4cf418a637f40b5c6bb74b455e1e))

### [5.24.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.24.0...v5.24.1) (2021-10-19)

## [5.24.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.23.0...v5.24.0) (2021-10-19)

### Features

- **button-group:** add button-group ([5634333](https://bitbucket.org/synetics/i-doit-ui-kit/commit/5634333205e9020c0294542d5108d5e952f4b262))

## [5.23.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.22.0...v5.23.0) (2021-10-06)

### Features

- **button-active:** add active state to buttons ([c9350d4](https://bitbucket.org/synetics/i-doit-ui-kit/commit/c9350d430158ce60cd9f500430802218763089ae))

## [5.22.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.21.0...v5.22.0) (2021-10-06)

### Features

- **list-item:** implement compound list items ([ee5c961](https://bitbucket.org/synetics/i-doit-ui-kit/commit/ee5c9611b41cf14f9c706ce0d63e917c8aa4654a))
- **list-item:** implement Status and Selector items ([05ff326](https://bitbucket.org/synetics/i-doit-ui-kit/commit/05ff32683ee7922ea7a5b98b5195b200479c51f9))

## [5.21.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.20.0...v5.21.0) (2021-10-06)

### Features

- **adjust_spacing:** make spacing according to design ([f7cf8bc](https://bitbucket.org/synetics/i-doit-ui-kit/commit/f7cf8bcfe0d70bb7cae0640b48a9c95f8a45a821))
- **adjust_spacing:** remove formfield group ([2a2c72b](https://bitbucket.org/synetics/i-doit-ui-kit/commit/2a2c72b234a8285baf7b2a31d6d34762abca1827))
- **adjust_spacing:** remove tgz rar ([750c130](https://bitbucket.org/synetics/i-doit-ui-kit/commit/750c130e83e897cfc1cbdfbbed4751887766609c))
- **adjust_spacing:** remove unnessesary scss ([8597973](https://bitbucket.org/synetics/i-doit-ui-kit/commit/8597973aea04bae531c4a18d80ec07cac18ecab9))
- **adjust_spacing:** remove unnessesary scss ([1ba8d39](https://bitbucket.org/synetics/i-doit-ui-kit/commit/1ba8d397b14ae509592650ae3f0e76500f12616f))
- **adjust_spacing:** remove unnessesary scss ([87ad524](https://bitbucket.org/synetics/i-doit-ui-kit/commit/87ad52448a25f5b6934ef69e9385c01a762f73c0))
- **adjust_spacing:** remove unused scss ([0c7f716](https://bitbucket.org/synetics/i-doit-ui-kit/commit/0c7f716ae66308e06d34ed4ec85db41ee70af5af))

## [5.20.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.19.2...v5.20.0) (2021-09-30)

### Features

- **add_new_icon:** add 6-x point icon ([03d4a78](https://bitbucket.org/synetics/i-doit-ui-kit/commit/03d4a783efe0b7cd854322cab8c07892e5427c0d))

### [5.19.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.19.1...v5.19.2) (2021-09-28)

### Bug Fixes

- react-svg updated to solve loading problems of icons in i-doit 2.x ([e054f6f](https://bitbucket.org/synetics/i-doit-ui-kit/commit/e054f6fb2aeca862e7d4340fec7ecf6b4c5dc7cf))

### [5.19.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.19.0...v5.19.1) (2021-09-27)

### Bug Fixes

- **focus:** fix wrong focus state ([e5b9cb6](https://bitbucket.org/synetics/i-doit-ui-kit/commit/e5b9cb64103fef7ab1dd3da965b7936def787c6a))

## [5.19.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.18.1...v5.19.0) (2021-09-27)

### Features

- **table-footer:** implement basic footer with Pagination ([af01e1d](https://bitbucket.org/synetics/i-doit-ui-kit/commit/af01e1d1708359bdede4097ba823090addecee2d))

### [5.18.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.18.0...v5.18.1) (2021-09-24)

## [5.18.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.17.0...v5.18.0) (2021-09-16)

### Features

- **action:** add action column and interactions with it to table ([93dd3b5](https://bitbucket.org/synetics/i-doit-ui-kit/commit/93dd3b5928cc4ac40a735bb4d98629f60164df61))

## [5.17.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.16.2...v5.17.0) (2021-09-15)

### Features

- **table-multi-selection:** First draft of multi selection capabilities ([c4fb7ad](https://bitbucket.org/synetics/i-doit-ui-kit/commit/c4fb7ad9a9880d6cb6a2e04c940950adbe13b2b1))
- **table-multi-selection:** type problem ignored ([62a3fa5](https://bitbucket.org/synetics/i-doit-ui-kit/commit/62a3fa5208b98ed524a886e5a8f270c06710b55d))
- **table-selector:** hook introduced ([7f4099a](https://bitbucket.org/synetics/i-doit-ui-kit/commit/7f4099ab13b602daa9478295b991ae73dd3ba8bd))
- **use-key-status:** implement use key status hook ([c96943b](https://bitbucket.org/synetics/i-doit-ui-kit/commit/c96943b47aa1f1cf2d14bac572fdd6a2f1eee231))

### [5.16.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.16.1...v5.16.2) (2021-09-08)

### Bug Fixes

- **sorting:** change color for sorted header cell ([bc9c7e5](https://bitbucket.org/synetics/i-doit-ui-kit/commit/bc9c7e58825736f58e7c8d43c12fc8b924a164d2))

### [5.16.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.16.0...v5.16.1) (2021-09-08)

## [5.16.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.15.0...v5.16.0) (2021-09-07)

### Features

- **sorting:** added sorting for table ([86ae6d6](https://bitbucket.org/synetics/i-doit-ui-kit/commit/86ae6d61edaf1bd1ec7d926687672fe7ea05f722))

## [5.15.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.14.0...v5.15.0) (2021-09-07)

### Features

- **single-select:** implement single select using downshift ([a90b9dd](https://bitbucket.org/synetics/i-doit-ui-kit/commit/a90b9dd8fe3fa76e6a1c59802adbaaf73e4dd73d))

## [5.14.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.12.0...v5.14.0) (2021-09-06)

### Features

- **buttonPulldown:** replace icon ([dbab83c](https://bitbucket.org/synetics/i-doit-ui-kit/commit/dbab83cf680192255e634aa79181fbeb409e6986))
- **headline_city:** added right icon ([88da206](https://bitbucket.org/synetics/i-doit-ui-kit/commit/88da20665f2b2b9d04a258b06497c2fbf917b76f))
- **headline_city:** adjusted table with classname prop ([b192071](https://bitbucket.org/synetics/i-doit-ui-kit/commit/b19207108194a4a6e427baf472255cc6f2c7e893))
- **headline_city:** adjusted table with classname prop ([075e2be](https://bitbucket.org/synetics/i-doit-ui-kit/commit/075e2bebb732f0739a6f7097878c43dfa5a6e37e))
- **headline_city:** made utils from adjust helper ([0e3005f](https://bitbucket.org/synetics/i-doit-ui-kit/commit/0e3005f6a0e716f904f8ee01fb5fddb7183206f1))
- **headline_city:** moved styles to right place ([2a76f2b](https://bitbucket.org/synetics/i-doit-ui-kit/commit/2a76f2b07a1f5d790fc391ae3ebbbf0c31cdeab1))
- **headline_city:** remove header border ([bb7896f](https://bitbucket.org/synetics/i-doit-ui-kit/commit/bb7896f48fe956b439286099a7015572797a28c9))
- **headline_city:** return test and footer ([f248a34](https://bitbucket.org/synetics/i-doit-ui-kit/commit/f248a341e0d610fb8453b2d7a14cdcfb1da20a89))
- **headline_city:** rework borders logic to table ([3936e64](https://bitbucket.org/synetics/i-doit-ui-kit/commit/3936e64e6749e9a111a7f51aac9a54d56464f43f))

## [5.13.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.12.0...v5.13.0) (2021-09-06)

### Features

- **headline_city:** added right icon ([88da206](https://bitbucket.org/synetics/i-doit-ui-kit/commit/88da20665f2b2b9d04a258b06497c2fbf917b76f))
- **headline_city:** adjusted table with classname prop ([b192071](https://bitbucket.org/synetics/i-doit-ui-kit/commit/b19207108194a4a6e427baf472255cc6f2c7e893))
- **headline_city:** adjusted table with classname prop ([075e2be](https://bitbucket.org/synetics/i-doit-ui-kit/commit/075e2bebb732f0739a6f7097878c43dfa5a6e37e))
- **headline_city:** made utils from adjust helper ([0e3005f](https://bitbucket.org/synetics/i-doit-ui-kit/commit/0e3005f6a0e716f904f8ee01fb5fddb7183206f1))
- **headline_city:** moved styles to right place ([2a76f2b](https://bitbucket.org/synetics/i-doit-ui-kit/commit/2a76f2b07a1f5d790fc391ae3ebbbf0c31cdeab1))
- **headline_city:** remove header border ([bb7896f](https://bitbucket.org/synetics/i-doit-ui-kit/commit/bb7896f48fe956b439286099a7015572797a28c9))
- **headline_city:** return test and footer ([f248a34](https://bitbucket.org/synetics/i-doit-ui-kit/commit/f248a341e0d610fb8453b2d7a14cdcfb1da20a89))
- **headline_city:** rework borders logic to table ([3936e64](https://bitbucket.org/synetics/i-doit-ui-kit/commit/3936e64e6749e9a111a7f51aac9a54d56464f43f))

## [5.12.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.11.0...v5.12.0) (2021-09-02)

### Features

- initial commit of feature ButtonPulldown ([2e15189](https://bitbucket.org/synetics/i-doit-ui-kit/commit/2e1518953e6364f0c5eb43afec7912444a989d08))

## [5.11.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.10.1...v5.11.0) (2021-08-27)

### Features

- **status_column:** change behavior accordingly ([0fffba3](https://bitbucket.org/synetics/i-doit-ui-kit/commit/0fffba37cec39823df50d6a070db8c9b9d96ea53))
- **status_column:** change behavior accordingly ([6ab1c20](https://bitbucket.org/synetics/i-doit-ui-kit/commit/6ab1c2069c29b1ce6f954ce6909af672dc2dc2d7))
- **status_column:** naming fixes ([8c4776a](https://bitbucket.org/synetics/i-doit-ui-kit/commit/8c4776af6fd1af987a458ab0b39cced99a443e0a))
- **status_column:** not propagate event to cell ([a9c87b4](https://bitbucket.org/synetics/i-doit-ui-kit/commit/a9c87b41820bfb795d47de21efbbfb45f684c334))
- **status_column:** not propagate event to cell ([c4053bf](https://bitbucket.org/synetics/i-doit-ui-kit/commit/c4053bf5de0c535f70a3645b2956a324d02ddffa))
- **status_column:** update table with status column ([3e96a95](https://bitbucket.org/synetics/i-doit-ui-kit/commit/3e96a9588e99b4ab7f580e3e2af79813eec9e9be))
- **website_column:** added timeout for state update ([8150382](https://bitbucket.org/synetics/i-doit-ui-kit/commit/8150382223c480bf14b83c1deab3f83b26db6c8f))

### [5.10.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.10.0...v5.10.1) (2021-08-26)

## [5.10.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.9.0...v5.10.0) (2021-08-26)

### Features

- **adjust_colorpicker:** update tests for colorpicker ([3aae42b](https://bitbucket.org/synetics/i-doit-ui-kit/commit/3aae42b382c926f88f7eb03b30b9f806a8f048cd))
- **color_picker:** add test coverage ([720bb90](https://bitbucket.org/synetics/i-doit-ui-kit/commit/720bb909c0a1b9e412269f36d1dda5f9dcdc25fb))
- **color_picker:** change behaviour of showing flyout of the colorpicker ([8846d20](https://bitbucket.org/synetics/i-doit-ui-kit/commit/8846d20bde4b8f04e55f69ad176846d1d0a2dee7))
- **color_picker:** color picker component added adjusted tests change popper props ([c99be56](https://bitbucket.org/synetics/i-doit-ui-kit/commit/c99be56bb32c28836214d55cf56387a6f56443f9))
- **color_picker:** fix group layout in color picker ([d6ed2e8](https://bitbucket.org/synetics/i-doit-ui-kit/commit/d6ed2e8878faed9d4b060614d921bcd04d300491))
- **color_picker:** redo tests, place picker to controls ([3991d59](https://bitbucket.org/synetics/i-doit-ui-kit/commit/3991d598db7ecd626ea2e09f90ac5cb4603099ed))
- **color_picker:** remove unnessesary comments ([7904864](https://bitbucket.org/synetics/i-doit-ui-kit/commit/7904864c703bc769a052a72a484090d325d47776))

## [5.9.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.8.0...v5.9.0) (2021-08-25)

### Features

- **icon:** adding two new icons and updating icon size story ([64db2bf](https://bitbucket.org/synetics/i-doit-ui-kit/commit/64db2bf8616fc9cb83b918b8bfa8b4e031e65a49))

## [5.8.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.7.2...v5.8.0) (2021-08-23)

### Features

- **website_column:** added timeout for state update ([7619fc3](https://bitbucket.org/synetics/i-doit-ui-kit/commit/7619fc3ac05f10d762aa38f49ad04e9a6ff2a419))
- **website_column:** added website column to table ([06dd0e0](https://bitbucket.org/synetics/i-doit-ui-kit/commit/06dd0e057d61424499d1c76d9235dc9c9fc5c875))
- **website_column:** added website column to table ([10252ed](https://bitbucket.org/synetics/i-doit-ui-kit/commit/10252ed4374aad019b26c285200b44845c6a28c5))
- **website_column:** fix tests ([849fd3d](https://bitbucket.org/synetics/i-doit-ui-kit/commit/849fd3d1102d7518095ba48cbd7765de9110e179))
- **website_column:** fixed overlay toggle ([4ca7327](https://bitbucket.org/synetics/i-doit-ui-kit/commit/4ca7327ff0cd2b0a56acb29405036bd0b65f6258))
- **website_column:** remove unnessesary react ([e151df5](https://bitbucket.org/synetics/i-doit-ui-kit/commit/e151df50f974d5d3f5da9723a642d17c4802ce10))

### [5.7.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.7.1...v5.7.2) (2021-08-23)

### [5.7.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.7.0...v5.7.1) (2021-08-19)

## [5.7.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.6.1...v5.7.0) (2021-08-19)

### Features

- **status_comp:** add text component as child ([4268c3f](https://bitbucket.org/synetics/i-doit-ui-kit/commit/4268c3fed6b1415278ea8b3c82b01963658f00b7))
- **status_comp:** fix ui tooltip ([3203870](https://bitbucket.org/synetics/i-doit-ui-kit/commit/32038708b6e39eb0d5ef3d4422799c05e6f445b5))
- **status_comp:** fix view of default component in table ([1ae117d](https://bitbucket.org/synetics/i-doit-ui-kit/commit/1ae117d8cae5f32ebf97231359ff78f0c428db70))
- **status_comp:** split styling for story ([f25a08c](https://bitbucket.org/synetics/i-doit-ui-kit/commit/f25a08ce23d97986a30d4087a4bdec24c45e2aba))

### [5.6.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.6.0...v5.6.1) (2021-08-17)

### Bug Fixes

- **button:** fixing spinner sizing in button ([d1294dc](https://bitbucket.org/synetics/i-doit-ui-kit/commit/d1294dc8c68189690a7b4f1c0be1a0f9ba3cc65f))

## [5.6.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.5.0...v5.6.0) (2021-08-16)

### Features

- **data table:** implementing keyboard navigation for data table ([1e994b1](https://bitbucket.org/synetics/i-doit-ui-kit/commit/1e994b10ba164d78d5734cbcfebb55a475664a8c))
- **data table:** implementing keyboard navigation hook ([dc20186](https://bitbucket.org/synetics/i-doit-ui-kit/commit/dc201868313140a273a1ae05e9f261fea3c4092b))

## [5.5.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.4.0...v5.5.0) (2021-08-12)

### Features

- **menu:** add base tests for components and hooks ([970eb81](https://bitbucket.org/synetics/i-doit-ui-kit/commit/970eb81c1989255fe6b341a3c0a1e850a7f16048))
- **menu:** fix failing tests ([ff14b22](https://bitbucket.org/synetics/i-doit-ui-kit/commit/ff14b227b922cd64bb64ab8c222f89ebfd893896))
- **menu:** fix selected styles for selected item ([90a0fa7](https://bitbucket.org/synetics/i-doit-ui-kit/commit/90a0fa772c1698aa479f86a02cab15203f94ba5f))
- **menu:** handle the specific behaviour of the items in item instead of in the menu ([291c222](https://bitbucket.org/synetics/i-doit-ui-kit/commit/291c2229b0fd8fc3b44b52ee2c7cc1bfc16603c8))
- **menu:** improve coverage for functions ([05e733d](https://bitbucket.org/synetics/i-doit-ui-kit/commit/05e733dea31086234691191b4ca2a6c4d4b4144e))
- **menu:** move menu related components ([dd4230c](https://bitbucket.org/synetics/i-doit-ui-kit/commit/dd4230c405d4d4a73fda96c133d2c725f3ef5ff7))
- **menu:** refactor styles usage and menu components layout ([fd2bbe8](https://bitbucket.org/synetics/i-doit-ui-kit/commit/fd2bbe88ccc755a44f71ccbd2360e16aa0d44ac5))

## [5.4.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.3.2...v5.4.0) (2021-08-09)

### Features

- **text_area:** moved textarea with types ([880cb47](https://bitbucket.org/synetics/i-doit-ui-kit/commit/880cb47b09fc43cff682ac0781106ebda115760f))
- **text_area:** moved textarea with types ([d7393a4](https://bitbucket.org/synetics/i-doit-ui-kit/commit/d7393a48e7c22ad8154885b4bb4395ed6102d69b))
- **text_area:** moved textarea with types ([d9b895d](https://bitbucket.org/synetics/i-doit-ui-kit/commit/d9b895d656380732149b71a9217318f1692cd674))
- **text_area:** moved textarea with types ([bd11be5](https://bitbucket.org/synetics/i-doit-ui-kit/commit/bd11be527fca79fbc15ac38befc8be7d8b79ff84))

### [5.3.2](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.3.1...v5.3.2) (2021-08-05)

### [5.3.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.3.0...v5.3.1) (2021-08-05)

### Bug Fixes

- **popper:** style definition for zIndex removed ([5520cc9](https://bitbucket.org/synetics/i-doit-ui-kit/commit/5520cc96f987d1b73239ade01ff8246c5504a87e))
- **popper:** switch to styles instead of using classnames for visibility ([f08ee73](https://bitbucket.org/synetics/i-doit-ui-kit/commit/f08ee73f6dd0d874d923bae16a100cc791757be5))

## [5.3.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.2.0...v5.3.0) (2021-08-02)

### Features

- **adjust_tag:** fix stuck focus in close button ([96483c5](https://bitbucket.org/synetics/i-doit-ui-kit/commit/96483c5af949f0fdc115473c2ff8583b6314ab11))
- **adjust_tag:** make tag usable for tags array ([43c7aa3](https://bitbucket.org/synetics/i-doit-ui-kit/commit/43c7aa35a2e3e8f79dbbfa455135ba0fc25c7f17))
- **adjust_tag:** remove conditional ([6fc35f5](https://bitbucket.org/synetics/i-doit-ui-kit/commit/6fc35f50be6708ca1a47a28921f9646b5dbdcde0))
- **calendar:** change handle-key for type-safety ([98d2a14](https://bitbucket.org/synetics/i-doit-ui-kit/commit/98d2a14a411571917fc08f150918538569bea594))
- **calendar:** change handle-key for type-safety ([f35c267](https://bitbucket.org/synetics/i-doit-ui-kit/commit/f35c2673b072ecc694ea21235b4eb0d5d9b2558e))
- **calendar:** change handle-key for type-safety ([06eb0cd](https://bitbucket.org/synetics/i-doit-ui-kit/commit/06eb0cd458ab14a1f6968d47c5ea6b7b026a35ed))
- **calendar:** change test coverage ([c45609c](https://bitbucket.org/synetics/i-doit-ui-kit/commit/c45609c3fa3fc8c4786029834417adfa4a16c4f9))
- **calendar:** fix id retrieve from props / styles from module ([ef81a95](https://bitbucket.org/synetics/i-doit-ui-kit/commit/ef81a95bf56355619926690b2b3cc6f3b9963cec))
- **calendar:** fix usage of component ([3c75d0f](https://bitbucket.org/synetics/i-doit-ui-kit/commit/3c75d0f12183d11445a3b6fc4c2f7c111f02036f))
- **calendar:** move calendar and use ts for it ([ea42d17](https://bitbucket.org/synetics/i-doit-ui-kit/commit/ea42d173155d7ef1586cd9464a7eff18981b06ad))
- **calendar:** update tests for achieve test coverage ([c4a8580](https://bitbucket.org/synetics/i-doit-ui-kit/commit/c4a858047e459d0e9319c3096b1a06d9e40fcd64))
- **calendar:** update tests for achieve test coverage ([5f38185](https://bitbucket.org/synetics/i-doit-ui-kit/commit/5f381856413c6f16efebbc83e733f06b1764090f))
- **calendar:** update tests for achieve test coverage ([349cfaa](https://bitbucket.org/synetics/i-doit-ui-kit/commit/349cfaa6117868905925b436c5f3df4c1e88450d))
- **calendar:** update tests for achieve test coverage ([2ab4108](https://bitbucket.org/synetics/i-doit-ui-kit/commit/2ab4108beb7eae348dae9c168d5306ad36fa40db))
- **calendar:** update tests for achieve test coverage ([be8498d](https://bitbucket.org/synetics/i-doit-ui-kit/commit/be8498d41f9c290b231a908748223f8e9d6ace65))
- **cv_cell:** add alignment ([aa2c053](https://bitbucket.org/synetics/i-doit-ui-kit/commit/aa2c0535c631ecc5b718e75d7fd76bbbe8ea6ddb))
- **cv_cell:** adjusted ui table with cv cell ([1ee8ce8](https://bitbucket.org/synetics/i-doit-ui-kit/commit/1ee8ce8c9e28fc78137447804342bcd392819f5e))
- **cv_cell:** adjusted ui table with cv cell ([982c8fd](https://bitbucket.org/synetics/i-doit-ui-kit/commit/982c8fde1078a958f0f30d926867d3358ef29b84))
- **cv_cell:** adjusted ui table with cv cell ([a59546b](https://bitbucket.org/synetics/i-doit-ui-kit/commit/a59546b965c963e596c4036df3da0858ef969af7))
- **data table:** adding birth time cell data ([1d1b14b](https://bitbucket.org/synetics/i-doit-ui-kit/commit/1d1b14bc372e1c7f0e5dff03739daf1a9e3931e9))
- **data table:** adding icon-text cell type ([3cae012](https://bitbucket.org/synetics/i-doit-ui-kit/commit/3cae012c27cfdf2e5fdfd8c86ded5776a5ba3605))
- **data table:** adding status-text cell type ([8384c33](https://bitbucket.org/synetics/i-doit-ui-kit/commit/8384c3329c2fe75543e8b5f16307637dc6308360))
- **data table:** creating a cell-wrapper component for inner alignment and muting ([7c1b7c2](https://bitbucket.org/synetics/i-doit-ui-kit/commit/7c1b7c2e5e328f9796c333663f1d995a7040ad97))
- **data table:** creating a no-permission component ([d4bde42](https://bitbucket.org/synetics/i-doit-ui-kit/commit/d4bde42e5df9a009e38027073da8d48f2ec858ae))
- **data table:** creating a not-available component ([299d7d6](https://bitbucket.org/synetics/i-doit-ui-kit/commit/299d7d67d93d74397ef4b4902d7cd3396f7b34ca))
- **data table:** creating first simple data table draft ([bb42818](https://bitbucket.org/synetics/i-doit-ui-kit/commit/bb4281857ee0862d9d7310ae80a9f40208553013))
- **data table:** implementing different cell-types for 'react-table' ([a6d0ba4](https://bitbucket.org/synetics/i-doit-ui-kit/commit/a6d0ba46d893365b1f059daffe8e9d00aabc4657))
- **data table:** making text-cell and icon-text-cell types mutable ([f648a6a](https://bitbucket.org/synetics/i-doit-ui-kit/commit/f648a6a1800700fcabf740f5cb5f241f72320ccd))
- **data table:** making use of 'react-table' library, implementing one more example ([6f09b42](https://bitbucket.org/synetics/i-doit-ui-kit/commit/6f09b426baf1783c91a444667ba7e34d964e800f))
- **datetime:** add alignment ([3d088cc](https://bitbucket.org/synetics/i-doit-ui-kit/commit/3d088cc0ba93261059f2c3ae2dff47995359d12d))
- **datetime:** add alignment ([6a5bf06](https://bitbucket.org/synetics/i-doit-ui-kit/commit/6a5bf061f9f64fe413fb050b80148ebe762290a2))
- **datetime:** align cells correctly ([5736462](https://bitbucket.org/synetics/i-doit-ui-kit/commit/57364627c11b55cb0f334905b398c8d811439f04))
- **datetime:** align cells correctly ([4b08efb](https://bitbucket.org/synetics/i-doit-ui-kit/commit/4b08efb74d1cddc5814c900d1a6ef9e37b71d7a6))
- **datetime:** table adjusted with datetime cell ([2ac61ef](https://bitbucket.org/synetics/i-doit-ui-kit/commit/2ac61ef5220ed6ea0db6ba5c9cc37c08d41bc7cc))
- **linked_cell:** adjust ui-kit table with linked cell ([e9bbee7](https://bitbucket.org/synetics/i-doit-ui-kit/commit/e9bbee7aec31339c0d213c3fbc2c025757e554b1))
- **linked_cell:** make cell 100 height for link coverage ([02c27b8](https://bitbucket.org/synetics/i-doit-ui-kit/commit/02c27b8fa8ad6d332b5504e51e2220fc3e52bde3))
- **use_conditional:** enable short cond operator in ui-kit ([f302670](https://bitbucket.org/synetics/i-doit-ui-kit/commit/f302670f9d57567b75cbbb23e9eedf2ea600774d))

## [5.2.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.1.1...v5.2.0) (2021-07-13)

### Features

- **password:** autofocus handling added ([0d0a2e5](https://bitbucket.org/synetics/i-doit-ui-kit/commit/0d0a2e53fcc9285d54619a286f669a885a17ca38))
- **password:** basic implementation of the password field ([9e3e09f](https://bitbucket.org/synetics/i-doit-ui-kit/commit/9e3e09fb0255a1f5306b121643c646d795a5c250))
- **password:** input will handle autofocus now ([b569f95](https://bitbucket.org/synetics/i-doit-ui-kit/commit/b569f95a8de38fc094f4db6e1d71958f91d984ae))
- **password:** missing input properties introduced ([c599188](https://bitbucket.org/synetics/i-doit-ui-kit/commit/c599188791200c996b42e53de43e4e69a41c85a2))
- **password:** missing property "format" and "rtl" added to input ([d1c10c9](https://bitbucket.org/synetics/i-doit-ui-kit/commit/d1c10c98930e8417f0645b1113d6cc1310e9debe))
- **password:** popper test removed because of incompatibilities with ([d823939](https://bitbucket.org/synetics/i-doit-ui-kit/commit/d8239390a1bf5acac6032d58ebfb5962e47ae3f6))
- **password:** stories and tests added ([9a85f80](https://bitbucket.org/synetics/i-doit-ui-kit/commit/9a85f80e0454e3c3a7b17de2f48b3f5b7ee2beed))
- **password:** tooltip index and blur handling fixed ([b5995b4](https://bitbucket.org/synetics/i-doit-ui-kit/commit/b5995b4e9f50332eefb89c0917b9879179fee897))
- **password:** tooltip test for multi level focusing removed ([8121975](https://bitbucket.org/synetics/i-doit-ui-kit/commit/8121975934a6743cd91e9c55d64b907c6bd47c00))
- **password:** visibility labels adjusted ([02ec197](https://bitbucket.org/synetics/i-doit-ui-kit/commit/02ec1975c5ff4fd7e7d2c4b7d470ca21c26733f3))

### [5.1.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.1.0...v5.1.1) (2021-07-12)

### Bug Fixes

- id recalculation of checkbox and radio on each rerender fixed ([497a53a](https://bitbucket.org/synetics/i-doit-ui-kit/commit/497a53ac41c96ffdb3df64fa288562e3d5be34c4))

## [5.1.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.0.1...v5.1.0) (2021-07-02)

### Features

- **tag:** add tests with tooltip ([3baf910](https://bitbucket.org/synetics/i-doit-ui-kit/commit/3baf910c16a18ec2fa13a9fda29246690f7b1f72))
- **tag:** improve codestyle and fix bugs ([a3f8027](https://bitbucket.org/synetics/i-doit-ui-kit/commit/a3f8027d8c29f39c4226bd8d92c13a5da0a6dfa2))
- **tag:** improve unit tests coverage ([7de2090](https://bitbucket.org/synetics/i-doit-ui-kit/commit/7de20900dcf26896d7ca626d5d968b81f7fd7c6d))
- **tag:** transfer and refactor tag component ([2d309e8](https://bitbucket.org/synetics/i-doit-ui-kit/commit/2d309e850362bb3931c5e69cdc24adb53815a450))

### [5.0.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v5.0.0...v5.0.1) (2021-06-24)

### Bug Fixes

- **firefox-border-issue:** border in comoboboxes in firefox removed ([3004b83](https://bitbucket.org/synetics/i-doit-ui-kit/commit/3004b83de8ceafd9734afa38854580905435e171))

## [5.0.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v4.4.0...v5.0.0) (2021-06-23)

### ⚠ BREAKING CHANGES

- **form-field:** changed the structure of the repository
- **form-field:** Use active and disabled props instead of variants to define state of form field

### Features

- **file_upload:** add upload component to ui-kit ([3b7ad1e](https://bitbucket.org/synetics/i-doit-ui-kit/commit/3b7ad1e70f164353094010d990febfbaeb48e0d4))
- **file_upload:** add upload component to ui-kit ([47cacfe](https://bitbucket.org/synetics/i-doit-ui-kit/commit/47cacfe2e5f8be3823acc9d8ab41a276ba2dd3b8))
- **file_upload:** add upload component to ui-kit ([549fcc0](https://bitbucket.org/synetics/i-doit-ui-kit/commit/549fcc0a3650de15d8237d73bc2d49360fdb04b8))
- **file_upload:** add upload component to ui-kit ([b9768ad](https://bitbucket.org/synetics/i-doit-ui-kit/commit/b9768ad42bd243483bb1ec9edfbf75d2aa391d58))
- **file_upload:** add upload component to ui-kit ([45a532b](https://bitbucket.org/synetics/i-doit-ui-kit/commit/45a532b33c425c5b6cdfe62681e23abc1c9fd299))
- **file_upload:** add upload component to ui-kit ([0f86b8c](https://bitbucket.org/synetics/i-doit-ui-kit/commit/0f86b8c81ef17d4b81946fc0b18b65da3b4ace61))
- **file_upload:** add upload component to ui-kit ([48bc7df](https://bitbucket.org/synetics/i-doit-ui-kit/commit/48bc7df1f0cfcbba225712b25ad33dbe78c8e88b))
- **file_upload:** add upload component to ui-kit ([6751598](https://bitbucket.org/synetics/i-doit-ui-kit/commit/675159864194263f19b91225efe21dad53386acc))
- **file_upload:** add upload component to ui-kit ([7794223](https://bitbucket.org/synetics/i-doit-ui-kit/commit/77942237166ab4ccbf61ee4c4e6cea2f85852314))
- **file_upload:** add upload component to ui-kit ([6f8ae67](https://bitbucket.org/synetics/i-doit-ui-kit/commit/6f8ae67b830385eaf629ee5e9bcd8e3a1f166db5))
- **form-field:** create use active hook to retrieve active state ([ecb0ddf](https://bitbucket.org/synetics/i-doit-ui-kit/commit/ecb0ddf5c4265a9f901248f20ef46b76c647c06d))
- **form-field:** decomposition extended ([dcfbd7f](https://bitbucket.org/synetics/i-doit-ui-kit/commit/dcfbd7f2e019f6fd9bdff55b68c844f0f2bb7cc0))
- **form-field:** implement field group ([a109410](https://bitbucket.org/synetics/i-doit-ui-kit/commit/a109410275ec92bca7d8edf024817f0c2d6eade6))
- **form-field:** restructure the repository and implement helpers ([ecbc2c4](https://bitbucket.org/synetics/i-doit-ui-kit/commit/ecbc2c4db787dc59278857bc22788d8e6db93afb))
- **form-field:** test fixed ([abe2228](https://bitbucket.org/synetics/i-doit-ui-kit/commit/abe222875d50908a00238bf12a493b3ca72d5497))
- **form-field:** use programmatic api instead of putting all states via variants ([0ace40c](https://bitbucket.org/synetics/i-doit-ui-kit/commit/0ace40cd3a9239b91a964411c430f16e9f15551d))
- **form-group:** coverage increased ([9040f7f](https://bitbucket.org/synetics/i-doit-ui-kit/commit/9040f7f768b03490297530f06a88a3b436ed6e72))
- **form-group:** test coverage extended ([83c68c4](https://bitbucket.org/synetics/i-doit-ui-kit/commit/83c68c4fb3819cd843ae27b4e2f61a943965ca04))
- **hooks:** create use-context-state hook ([4bf06ef](https://bitbucket.org/synetics/i-doit-ui-kit/commit/4bf06effc865c3702fd5b67a922c55813d879400))

### Bug Fixes

- **accent-color:** accent color and glowing input fixed ([690d669](https://bitbucket.org/synetics/i-doit-ui-kit/commit/690d6696a5a1be21163e9a3fdfcec692e5ccb1f6))

## [4.4.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v4.3.0...v4.4.0) (2021-05-17)

### Features

- **checkbox:** new component Checkbox ([4581cae](https://bitbucket.org/synetics/i-doit-ui-kit/commit/4581cae8038338dc6cd7f3aabc9f1dfcdcb088a7))

## [4.3.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v4.2.0...v4.3.0) (2021-05-12)

### Features

- **portal:** new component Portal ([852784e](https://bitbucket.org/synetics/i-doit-ui-kit/commit/852784ec5bc4da9d4e343a0afbf826fca296ee80))

### Bug Fixes

- **icon-button:** add missing button props ([bd15da2](https://bitbucket.org/synetics/i-doit-ui-kit/commit/bd15da28c6fed6a4564ea310817f5aa71808b5d7))
- **icon-button:** fix background color of the disabled state ([ddc1652](https://bitbucket.org/synetics/i-doit-ui-kit/commit/ddc165272cdac3c686b7b6d6b78bbd5569f5457b))

## [4.2.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v4.1.0...v4.2.0) (2021-05-05)

### Features

- **popper:** new modifiers ([2d6c170](https://bitbucket.org/synetics/i-doit-ui-kit/commit/2d6c170ec68fff901f5fd3d6e067be25bb85f1d5))

## [4.1.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v4.0.1...v4.1.0) (2021-05-04)

### Features

- **form-field:** finalize form field and implement form group ([d5fcd70](https://bitbucket.org/synetics/i-doit-ui-kit/commit/d5fcd705240447eba98655f79e42c74bcd1d91fc))
- **form-field:** implement form-field component ([14df7e4](https://bitbucket.org/synetics/i-doit-ui-kit/commit/14df7e4e66223b29541564a259ec478deebf0090))
- **form-field:** make FormField compound. Added tests ([984cd97](https://bitbucket.org/synetics/i-doit-ui-kit/commit/984cd97bf172a8dfdff23d69bd53bbd623c795ff))

### [4.0.1](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v4.0.0...v4.0.1) (2021-04-27)

### Bug Fixes

- **external-link:** edit the color in hover state ([6a02075](https://bitbucket.org/synetics/i-doit-ui-kit/commit/6a02075b35ef8f475d3f4d618178dad20b8b8bed))
- **headline:** add missing import of abstracts ([53443b5](https://bitbucket.org/synetics/i-doit-ui-kit/commit/53443b556397a18d9ee710375281f705508697f4))
- **headline:** add missing import of abstracts ([2541565](https://bitbucket.org/synetics/i-doit-ui-kit/commit/254156568b867032c1de2246b2d737c63afe6cd0))
- **popper:** use visibility css prop to hide popper initially ([24feb9f](https://bitbucket.org/synetics/i-doit-ui-kit/commit/24feb9fd3087259fddce71898ad6aefa78c2e091))
- **text:** add missing import of abstracts ([59b4e9a](https://bitbucket.org/synetics/i-doit-ui-kit/commit/59b4e9aeeffb2458bc494e674a43f4de47054084))
- **tooltip:** wrong position in scroll container ([0249e98](https://bitbucket.org/synetics/i-doit-ui-kit/commit/0249e982056d33aa6c8d5f366674131389a9868f))

## [4.0.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v2.0.1...v4.0.0) (2021-04-23)

### ⚠ BREAKING CHANGES

We switched back to babel :)

### Features

- **button:** add button component ([0ccf62a](https://bitbucket.org/synetics/i-doit-ui-kit/commit/0ccf62a2d922f613dfb19084f73f6db65d52dd4b))
- **button:** add extra style for spinner ([5eae969](https://bitbucket.org/synetics/i-doit-ui-kit/commit/5eae969917665cc4017b1f3cfcf249f66738980a))
- **button:** add stories and tests ([63a61d2](https://bitbucket.org/synetics/i-doit-ui-kit/commit/63a61d2beea08be44637ee5e82be6366d460b459))
- **button:** clean redundant code ([8383885](https://bitbucket.org/synetics/i-doit-ui-kit/commit/838388521fce441415b5576725f67c1746eef799))
- **button:** fix button color scheme ([c72d28f](https://bitbucket.org/synetics/i-doit-ui-kit/commit/c72d28fb0c08ee677d38327d19d7114d3ca1744f))
- **button:** update button colors ([3278061](https://bitbucket.org/synetics/i-doit-ui-kit/commit/3278061a1b6082974fcb29b5873f86b209ac650a))
- **create-local-context:** add new utility to facilitate context creation ([b20440e](https://bitbucket.org/synetics/i-doit-ui-kit/commit/b20440ed40001e1520d576a6804ab546af167ca5))
- **ExternalLink:** add new component ExternalLink ([a471c2a](https://bitbucket.org/synetics/i-doit-ui-kit/commit/a471c2a0b43efb3f81b3b92317d21ce9d85c9add))
- **ExternalLink:** add new component ExternalLink ([36323ef](https://bitbucket.org/synetics/i-doit-ui-kit/commit/36323efd9dfe523d2a5f08eac5c6c185cba2b5d1))
- **FadeTransition:** new FadeTransition component ([452ebf0](https://bitbucket.org/synetics/i-doit-ui-kit/commit/452ebf0d5e806156d63d5bd2da74a0b9d1529a41))
- **focus-visible:** switch to :focus-visible from :focus ([3d2788d](https://bitbucket.org/synetics/i-doit-ui-kit/commit/3d2788da4b31245e640b054378b061473fd8149c))
- **icon:** add a set of svg icons ([1dc6158](https://bitbucket.org/synetics/i-doit-ui-kit/commit/1dc61585acebbda37115342266a816ab5d7da149))
- **icon:** add Icon component ([aa9d100](https://bitbucket.org/synetics/i-doit-ui-kit/commit/aa9d1006982cd664c4abb6709b3c5163f45429f2))
- **icon:** implement the icon component ([2413dd7](https://bitbucket.org/synetics/i-doit-ui-kit/commit/2413dd7bb4f02d243fb2491673aa7782ee1bf516))
- **icon-button:** add new component IconButton ([a81321a](https://bitbucket.org/synetics/i-doit-ui-kit/commit/a81321a3dd7994f0deb893ea568eb287152cdf87))
- **Popper:** add new Popper component ([7ed411c](https://bitbucket.org/synetics/i-doit-ui-kit/commit/7ed411c9ef102141f81dc8e84ddd932d0fc0f243))
- **radio-button:** add new component RadioButton ([7826d94](https://bitbucket.org/synetics/i-doit-ui-kit/commit/7826d940c1298ca2330792550fb46c7dc9ef5046))
- **section-header:** add new component - SectionHeader ([8f29181](https://bitbucket.org/synetics/i-doit-ui-kit/commit/8f2918133c54d61d2fdf77e56775f15d4cea18d2))
- **slide-transition:** add new component SlideTransition ([08e870c](https://bitbucket.org/synetics/i-doit-ui-kit/commit/08e870ca86eba293fc643043c66c0f0eaea2cbb6))
- **slide-transition:** add new component SlideTransition ([17142cc](https://bitbucket.org/synetics/i-doit-ui-kit/commit/17142cc78357a05a47660f0213a84019a021cb26))
- **tooltip:** added tooltip with over popper functionality ([1764ee6](https://bitbucket.org/synetics/i-doit-ui-kit/commit/1764ee60a1a657c75e5c094f7bac345f5073358a))
- **Tooltip:** add new Tooltip component ([2c5b033](https://bitbucket.org/synetics/i-doit-ui-kit/commit/2c5b033c0ea3a9ea41837cf312616e7373236a95))
- **use-controlled-state:** add new hook useControlledState ([51ea0da](https://bitbucket.org/synetics/i-doit-ui-kit/commit/51ea0da1b67b28a5f36a5c9d18c1f73958949c9b))
- **useCallbackRef:** new useCallbackRef hook ([d7a56be](https://bitbucket.org/synetics/i-doit-ui-kit/commit/d7a56beba33444a9c3006c237f5b70872254e37a))

### Bug Fixes

- **external-link:** edit the classnames ([bb83fe1](https://bitbucket.org/synetics/i-doit-ui-kit/commit/bb83fe172dffbd862aa4cd3c5abbe9d777f9df7d))
- **external-link:** edit the default props ([fad8816](https://bitbucket.org/synetics/i-doit-ui-kit/commit/fad8816e24bdbb479d2d868422bb6d855e8b8764))
- **external-link:** edit the external-link props ([fbb495f](https://bitbucket.org/synetics/i-doit-ui-kit/commit/fbb495fa81e75b9d24dd43a9ee1975bfe5bec1f9))
- **external-link:** edit the externalLink Story ([0bb74a8](https://bitbucket.org/synetics/i-doit-ui-kit/commit/0bb74a8977e4430e9f3179e3358bf1f9eb7ebe72))
- **external-link:** edit the padding in state focus-visible ([3e55458](https://bitbucket.org/synetics/i-doit-ui-kit/commit/3e554580e2cc518f196fdf9bee3b2f3a3d547fdd))
- **external-link:** edit the padding in state focus-visible ([c92d0b8](https://bitbucket.org/synetics/i-doit-ui-kit/commit/c92d0b8a910b9dc5e883ace130ec8a66393af32a))
- **external-link:** switch to focus-visible ([a8c75a7](https://bitbucket.org/synetics/i-doit-ui-kit/commit/a8c75a71ef67610d91986faae4911e6f9c28f732))
- **external-link:** switch to jsx syntax ([5306195](https://bitbucket.org/synetics/i-doit-ui-kit/commit/5306195c21f96bae25462e39ec129155e54cb0a6))
- **icon:** stop event propagation after clicking by an svg icon ([30f64be](https://bitbucket.org/synetics/i-doit-ui-kit/commit/30f64be83e774de2f7f810e14dbffe174d6af4f0))
- **infra:** remove unwanted .babelrc ([6f3777b](https://bitbucket.org/synetics/i-doit-ui-kit/commit/6f3777b336157b6e56ccc9c6fbb03a8a83352f50))

- switch named exports ([381ed20](https://bitbucket.org/synetics/i-doit-ui-kit/commit/381ed20d27ac5578e2f984a76d4ec8425883932e))

### [3.0.1](http://bitbucket.org/synetics/i-doit-ui-kit/compare/v2.0.1...v3.0.1) (2021-04-03)

### Bug Fixes

- remove unwanted .babelrc that leads to ignoring files from node_modules

## [3.0.0](http://bitbucket.org/synetics/i-doit-ui-kit/compare/v2.0.1...v3.0.0) (2021-04-01)

### ⚠ BREAKING CHANGES

- switch named exports
- switch to typescript

Affected components:

- Button.jsx
- Headline.jsx
- Icon.jsx
- Spinner.jsx
- Text.jsx

Affected hooks:

- useClipboard
- useRaf

To migrate the code follow the example below. Please note that we no longer support applications in pure JavaScript.

Before:

```jsx
import Icon from 'i-doit-ui-kit/components/icon/Icon';
```

After:

```jsx
import { Icon } from 'i-doit-ui-kit/components/Icons';
```

or even

```jsx
import { Icon } from 'i-doit-ui-kit';
```

### Bug Fixes

- **icon:** stop event propagation after clicking by an svg icon ([30f64be](http://bitbucket.org/synetics/i-doit-ui-kit/commit/30f64be83e774de2f7f810e14dbffe174d6af4f0))

- switch named exports ([381ed20](http://bitbucket.org/synetics/i-doit-ui-kit/commit/381ed20d27ac5578e2f984a76d4ec8425883932e))

## [2.1.0](http://bitbucket.org/synetics/i-doit-ui-kit/compare/v2.0.1...v2.1.0) (2021-03-16)

### Features

- **icons:** add a set of svg icons ([1dc6158](http://bitbucket.org/synetics/i-doit-ui-kit/commit/1dc61585acebbda37115342266a816ab5d7da149))
- **icon:** add Icon component ([aa9d100](http://bitbucket.org/synetics/i-doit-ui-kit/commit/aa9d1006982cd664c4abb6709b3c5163f45429f2))

### [2.0.1](http://bitbucket.org/synetics/i-doit-ui-kit/compare/v2.0.0...v2.0.1) (2021-03-15)

### Bug Fixes

- **docs:** update react and react-dom versions ([8cb69d5](http://bitbucket.org/synetics/i-doit-ui-kit/commit/8cb69d55a00d8781ea974eb7ce2e712ba0e10c96))

## [2.0.0](http://bitbucket.org/synetics/i-doit-ui-kit/compare/v1.2.0...v2.0.0) (2021-03-09)

No new features and changes in the API. We made this technical release to update our dependencies and switch to react v17.
Since now, the peerDependencies section look like the JSON:

```json
{
  "@babel/cli": "^7.13.10",
  "classnames": "^2.2.6",
  "npm-run-all": "^4.1.5",
  "react": "^17.0.1",
  "react-dom": "^17.0.1"
}
```

### Features

- **spinner:** add Spinner component to handle loading states ([b3cc016](http://bitbucket.org/synetics/i-doit-ui-kit/commit/b3cc016b4331eaeede872a0742e47b0a88a046e6))
- **spinner:** init Spinner component ([db9d714](http://bitbucket.org/synetics/i-doit-ui-kit/commit/db9d7143c43367cc56cd6b1754381968a90a775f))
- **typography:** add prop types and fix types ([2f221a4](http://bitbucket.org/synetics/i-doit-ui-kit/commit/2f221a448c18f6f90c712120466f2ba3d8ef420d))
- **typography:** add tests for headline and text ([2eea098](http://bitbucket.org/synetics/i-doit-ui-kit/commit/2eea0984d021059c5202c3f8f3789ce496442ae1))
- **typography:** delete default props ([7125484](http://bitbucket.org/synetics/i-doit-ui-kit/commit/712548477fdfc6de27fffd4684a54b5642f05490))
- **typography:** fix test fails in text component ([311f174](http://bitbucket.org/synetics/i-doit-ui-kit/commit/311f1741688691e174323d7611eaff882f001191))

## [1.3.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v1.2.0...v1.3.0) (2021-02-23)

### Features

- **spinner:** add Spinner component to handle loading states ([b3cc016](https://bitbucket.org/synetics/i-doit-ui-kit/commit/b3cc016b4331eaeede872a0742e47b0a88a046e6))

## [1.2.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v1.1.0...v1.2.0) (2021-02-19)

### Features

- **typography:** introduce $idoit-path-to-font sass variable ([3b57383](https://bitbucket.org/synetics/i-doit-ui-kit/commit/3b57383eec1f60ca253ff884189d7b03df0da891))

## [1.1.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v1.0.0...v1.1.0) (2021-02-19)

### Features

- **assets:** add Inter font family ([f9eee78](https://bitbucket.org/synetics/i-doit-ui-kit/commit/f9eee78cf2e9e50a1a3bf757a481b2841c5ac2d7))
- **typography:** add bootstrap to the project dependencies ([a779559](https://bitbucket.org/synetics/i-doit-ui-kit/commit/a779559d5dc2193cdebd5f926cd9fc8976d407e2))
- **typography:** add the prefix to variables ([1b8d26f](https://bitbucket.org/synetics/i-doit-ui-kit/commit/1b8d26fe89d7273b629997c2fe0dac267391a190))
- **typography:** adjust typography settings ([c0aca7d](https://bitbucket.org/synetics/i-doit-ui-kit/commit/c0aca7d60dc9c111390a380defa9e189074df7a9))
- **typography:** implement Caption and Overline ([0204833](https://bitbucket.org/synetics/i-doit-ui-kit/commit/0204833415be64800f60afaf9bda0d2643c3207a))
- **typography:** implement Headline component ([a82c7ac](https://bitbucket.org/synetics/i-doit-ui-kit/commit/a82c7ac10cde64c3263e198af39da67c28212805))
- **typography:** implement styles for links ([7726491](https://bitbucket.org/synetics/i-doit-ui-kit/commit/7726491d21ae63d9b511156a0b3fcb7a296a903f))
- **typography:** implement Text component ([726237a](https://bitbucket.org/synetics/i-doit-ui-kit/commit/726237a070383af441bef5d28cb9735be7cf67b7))
- **typography:** set Inter as default font family ([38acefa](https://bitbucket.org/synetics/i-doit-ui-kit/commit/38acefae6b77301ad4e8fc2cf44b16ecbdda8c18))
- **typography:** update styles of links ([2ddfd7a](https://bitbucket.org/synetics/i-doit-ui-kit/commit/2ddfd7afff7fffa5f8517dee0e49476182e847fa))

### Bug Fixes

- **infra:** fix name of eslint script for lintstagedrc ([b441169](https://bitbucket.org/synetics/i-doit-ui-kit/commit/b4411690325e024a47e2deb2889edac8eb0fb238))

## [1.0.0](https://bitbucket.org/synetics/i-doit-ui-kit/compare/v0.1.0...v1.0.0) (2021-02-12)

### ⚠ BREAKING CHANGES

- **colors:** The color $idoit-main-nav has been removed
  and we introduced $idoit-system one to replace it.

To migrate the code follow the example below:

Before:

.className {
color: $idoit-main-nav;
}

After: {
color: $idoit-system;
}

### Features

- **colors:** update the color palette ([a4248be](https://bitbucket.org/synetics/i-doit-ui-kit/commit/a4248be77fea689d2d3c0011e83dfcaa454b6fa5))

### 0.1.0 (2021-02-11)

### Features

- **colors:** consume colors from Sass variables ([92c4648](https://bitbucket.org/synetics/i-doit-ui-kit/commit/92c4648dedad70fc6708d9cf2f1b6948993ac631))